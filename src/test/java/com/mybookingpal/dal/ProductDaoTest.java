package com.mybookingpal.dal;

import com.mybookingpal.dal.server.RazorServer;
import com.mybookingpal.dal.shared.Product;
import com.mybookingpal.shared.dao.ProductDao;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class ProductDaoTest {
	private static ProductDao productDao;

	@BeforeClass
	public static void setup() {
		ApplicationContext context = RazorServer.getContext();
		productDao = context.getBean(ProductDao.class);
	}

	@Ignore
	@Test
	public void readById() {
		Product product = productDao.readById(1235106391);
		assertNotNull(product);
		assertNotNull(product.getId());
	}

	@Ignore
	@Test
	public void update() {
		try {
			Product action = new Product();
			action.setId("1235106391");
			action.setBasePrice(10d);
			productDao.update(action);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Ignore
	@Test
	public void readByExample() {
		Product example = new Product();
		example.setBasePrice(10d);
		List<Product> productList = productDao.readByExample(example);
		assertNotNull(productList);
		assertEquals(1, productList.size());
	}
}
