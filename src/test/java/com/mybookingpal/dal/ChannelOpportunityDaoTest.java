package com.mybookingpal.dal;

import com.mybookingpal.dal.server.RazorServer;
import com.mybookingpal.dal.shared.ChannelOpportunity;
import com.mybookingpal.dal.shared.ChannelProductOpportunity;
import com.mybookingpal.shared.dao.ChannelOpportunityDao;
import com.mybookingpal.shared.dao.ChannelProductOpportunityDao;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ChannelOpportunityDaoTest {
	private static ChannelProductOpportunityDao channelPoductOpportunityDao;
	private static ChannelOpportunityDao channelOpportunitiesDao;

	@BeforeClass
	public static void setup() {
		ApplicationContext context = RazorServer.getContext();
		channelPoductOpportunityDao = context.getBean(ChannelProductOpportunityDao.class);
		channelOpportunitiesDao = context.getBean(ChannelOpportunityDao.class);
	}

	@Ignore
	@Test
	public void readById() {
		ChannelProductOpportunity channelProductOpportunity = channelPoductOpportunityDao.readById(131);
		assertNotNull(channelProductOpportunity);
		assertNotNull(channelProductOpportunity.getProductId());
		assertNotNull(channelProductOpportunity.getOpportunityId());
	}

	@Ignore
	@Test
	public void readByExample() {
		ChannelOpportunity example = new ChannelOpportunity();
		example.setChannelId(276);
		List<ChannelOpportunity> channelOpportunityList = channelOpportunitiesDao.readByExample(example);
		assertNotNull(channelOpportunityList);
		assertTrue(channelOpportunityList.size() > 0);
	}

	@Ignore
	@Test
	public void readByProductId() {
		List<ChannelProductOpportunity> channelOpportunityList = channelPoductOpportunityDao.readByProductId(1235021539);
		assertNotNull(channelOpportunityList);
		assertTrue(channelOpportunityList.size() > 0);
	}

	@Ignore
	@Test
	public void update() {
		try {
			ChannelOpportunity action = new ChannelOpportunity();
			action.setId("131");
			action.setCallToAction("NEW VALUE 2");
			channelOpportunitiesDao.update(action);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
}
