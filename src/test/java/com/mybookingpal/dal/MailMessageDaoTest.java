package com.mybookingpal.dal;

import com.mybookingpal.dal.server.RazorServer;
import com.mybookingpal.dal.shared.messaging.MailMessage;
import com.mybookingpal.shared.dao.MailMessageDao;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.util.List;

public class MailMessageDaoTest {
	private static final Logger LOG = Logger.getLogger(MailMessageDaoTest.class);
	private static MailMessageDao mailMessageDao;

	@BeforeClass
	public static void setup() {
		ApplicationContext context = RazorServer.getContext();
		mailMessageDao = context.getBean(MailMessageDao.class);
	}

	@Ignore
	@Test
	public void testFetchingSettingValue() {
		MailMessage mailMessage = mailMessageDao.readByChannelMessageId(16l);
		LOG.info(mailMessage);
	}

	@Ignore
	@Test
	public void readByExample() {
		MailMessage example = new MailMessage();
		example.setMessage("Hi, just wanted to confirm price and why is it so affordable? is there a catch. ");
		List<MailMessage> mailMessages = mailMessageDao.readByExample(example);
		LOG.info(mailMessages.get(0).getMessage());
	}

}
