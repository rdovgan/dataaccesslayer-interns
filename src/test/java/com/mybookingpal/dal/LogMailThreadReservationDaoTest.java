package com.mybookingpal.dal;

import com.mybookingpal.dal.server.RazorServer;
import com.mybookingpal.dal.shared.LogMailThreadReservation;
import com.mybookingpal.shared.dao.LogMailThreadReservationDao;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.util.List;

import static org.junit.Assert.fail;

public class LogMailThreadReservationDaoTest {
	private static final Logger LOG = Logger.getLogger(LogMailThreadReservationDaoTest.class);
	private static LogMailThreadReservationDao logMailThreadReservationDao;

	@BeforeClass
	public static void setup() {
		ApplicationContext context = RazorServer.getContext();
		logMailThreadReservationDao = context.getBean(LogMailThreadReservationDao.class);
	}

	@Ignore
	@Test
	public void create() {
		try {
			LogMailThreadReservation logMailThreadReservation = new LogMailThreadReservation();
			logMailThreadReservation.setChannelProductId(1);
			logMailThreadReservation.setThreadId(2);
			logMailThreadReservation.setProductId(3);
			logMailThreadReservation.setReservationId(4);
			logMailThreadReservationDao.create(logMailThreadReservation);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Ignore
	@Test
	public void readByExample() {
		try {
			LogMailThreadReservation logMailThreadReservation = new LogMailThreadReservation();
			logMailThreadReservation.setReservationId(4);
			List<LogMailThreadReservation> list = logMailThreadReservationDao.readByExample(logMailThreadReservation);
			LOG.info(list.size());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
}
