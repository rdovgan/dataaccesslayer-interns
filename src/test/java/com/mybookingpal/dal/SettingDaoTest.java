package com.mybookingpal.dal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mybookingpal.shared.dao.SettingDao;

/**
 * @author Danijel
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:com/mybookingpal/dal/config/dao-beans.xml")
public class SettingDaoTest {
	
	@Autowired
	SettingDao settingsDao;
	
	@Test
	public void testFetchingSettingValue() {
		//here can be tested some value fetch from setting table
	}

}
