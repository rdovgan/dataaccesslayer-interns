package com.mybookingpal.dal;

import com.mybookingpal.dataaccesslayer.dao.LocationDao;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:com/mybookingpal/dal/config/dao-beans.xml")
public class LocationDaoTest {

	@Autowired
	private LocationDao locationDao;

	@Test
	public void testLocationDaoUsage() {
		Assert.assertNotNull(locationDao.activeLocations());
	}
}
