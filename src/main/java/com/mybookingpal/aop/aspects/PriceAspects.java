package com.mybookingpal.aop.aspects;

import com.mybookingpal.cache.service.EventAvailabilityCacheService;
import com.mybookingpal.dal.shared.Price;
import com.mybookingpal.dal.utils.GenericList;
import com.mybookingpal.shared.cache.IsCacheable;
import com.mybookingpal.shared.dao.PriceDao;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Aspect
@Component
public class PriceAspects {

	private Logger LOG = Logger.getLogger(PriceAspects.class);

	@Autowired
	private EventAvailabilityCacheService eventAvailabilityCacheService;

	@Autowired
	private PriceDao priceDao;

	@After("(execution(void com.mybookingpal.dal.server.api.PriceMapper.create(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.PriceMapper.update(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.PriceMapper.updateCurrency(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.PriceMapper.cancelversion(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.PriceMapper.cancelversionbypartyid(..))) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(price)")
	public void updatePrice(JoinPoint joinPoint, Price price) {
		updateCache(price);
	}

	@After("(execution(void com.mybookingpal.dal.server.api.PriceMapper.insertList(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.PriceMapper.insertListPrices(..))) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) "
			+ "&& args(prices)")
	public void updatePriceList(JoinPoint joinPoint, List<Price> prices) {
		updatePriceCache(prices);
	}

	@After("execution(void com.mybookingpal.dal.server.api.PriceMapper.cancelpricelist(..)) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) "
			+ "&& args(genericPrices, version)")
	public void cancelpricelist(JoinPoint joinPoint, GenericList<Price> genericPrices, Date version) {
		updatePriceCache(genericPrices);
	}

	@After("execution(void com.mybookingpal.dal.server.api.PriceMapper.insertListWithUpdate(..)) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) "
			+ "&& args(genericPrices)")
	public void insertListWithUpdate(JoinPoint joinPoint, GenericList<Price> genericPrices) {
		updatePriceCache(genericPrices);
	}

	@After("execution(void com.mybookingpal.dal.server.api.PriceMapper.makeFinalByProductId(..)) && @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(productId)")
	public void makeFinalByProductId(JoinPoint joinPoint, String productId) {
		List<Price> pricesByProductId = priceDao.getPricesByProductId(NumberUtils.createInteger(productId));
		updatePriceCache(pricesByProductId);
	}

	@After("execution(void com.mybookingpal.dal.server.api.PriceMapper.updateStateOnFinalPerPm(..)) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(pmId)")
	public void updateStateOnFinalPerPm(JoinPoint joinPoint, String pmId) {

	}

	@After("execution(void com.mybookingpal.dal.server.api.PriceMapper.makePriceFinalByRatePlanId(..)) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(makePriceFinalByRatePlanId)")
	public void makePriceFinalByRatePlanId(JoinPoint joinPoint, Long makePriceFinalByRatePlanId) {
		Price price = priceDao.readPriceByRatePlanId(makePriceFinalByRatePlanId);
		updateCache(price);
	}

	@After("execution(void com.mybookingpal.dal.server.api.PriceMapper.setFinalStateListPrices(..)) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(list)")
	public void setFinalStateListPrices(JoinPoint joinPoint, List<Integer> list) {
		List<Long> ids = Stream.of(joinPoint.getArgs())
				.map(Long.class::cast)
				.collect(Collectors.toList());
		List<Price> prices = priceDao.getPricesByIdList(ids);
		updatePriceCache(prices);
	}

	private void updatePriceCache(List<Price> prices) {
		if (CollectionUtils.isEmpty(prices)) {
			return;
		}
		prices.forEach(this::updateCache);
	}

	@After("execution(void com.mybookingpal.dal.server.api.PriceMapper.deleteByIdList(..)) && @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(prices)")
	public void deleteByIdList(JoinPoint joinPoint, List<Price> prices) {
		if (CollectionUtils.isEmpty(prices)) {
			return;
		}
		prices.forEach(isCacheable -> eventAvailabilityCacheService.removeAvailabilityDataFromCache(isCacheable.getCacheProductId(),
				isCacheable.getFromDate(), isCacheable.getToDate()));
	}

	@After("execution(void com.mybookingpal.dal.server.api.PriceMapper.delete(..)) && @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(priceId)")
	public void delete(JoinPoint joinPoint, String priceId) {
		Optional<Price> priceForDelete = Optional.ofNullable(priceDao.read(priceId));
		if (!priceForDelete.isPresent()) {
			return;
		}
		eventAvailabilityCacheService.removeAvailabilityDataFromCache(priceForDelete.get().getCacheProductId(), priceForDelete.get().getFromDate(),
				priceForDelete.get().getToDate());
	}

	private void updateCache(IsCacheable cache) {
		if (!Optional.ofNullable(cache).isPresent()) {
			return;
		}
		eventAvailabilityCacheService.removeAvailabilityDataFromCache(cache.getCacheProductId(), cache.getFromDate(), cache.getToDate());
		LOG.info(String.format("Add to cache information about product - %d, fromDate - %s, toDate - %s.", cache.getCacheProductId(), cache
				.getFromDate(), cache.getToDate()));
	}
}
