package com.mybookingpal.aop.aspects;

import com.mybookingpal.cache.service.EventAvailabilityCacheService;
import com.mybookingpal.dal.shared.MinStay;
import com.mybookingpal.dal.shared.PropertyMinStay;
import com.mybookingpal.dal.utils.GenericList;
import com.mybookingpal.shared.cache.IsCacheable;
import com.mybookingpal.shared.dao.PropertyMinStayDao;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Aspect
@Component
public class PropertyMinStayAspect {

	private Logger LOG = Logger.getLogger(ReservationAspect.class);

	@Autowired
	private EventAvailabilityCacheService eventAvailabilityCacheService;

	@Autowired
	private PropertyMinStayDao propertyMinStayDao;

	@After("(execution(void com.mybookingpal.dal.server.api.PropertyMinStayMapper.create(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.PropertyMinStayMapper.update(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.PropertyMinStayMapper.delete(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.PropertyMinStayMapper.deleteversion(..)))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(action)")
	public void updateList(JoinPoint joinPoint, MinStay action) {
		updateCache(action);
	}

	@After("execution(void com.mybookingpal.dal.server.api.PropertyMinStayMapper.deleteByProductId(..))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(productId)")
	public void deleteByProductId(JoinPoint joinPoint, String productId) {
		List<PropertyMinStay> propertyMinStays = propertyMinStayDao.readByProductId(NumberUtils.createInteger(productId));
		if (CollectionUtils.isNotEmpty(propertyMinStays)) {
			propertyMinStays.forEach(this::updateCache);
		}
	}

	@After("execution(void com.mybookingpal.dal.server.api.PropertyMinStayMapper.cancelPropertyMinStayList(..))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(propertyMinStays, version)")
	public void cancelPropertyMinStayList(JoinPoint joinPoint, GenericList<PropertyMinStay> propertyMinStays, Date version) {
		if (CollectionUtils.isEmpty(propertyMinStays)) {
			return;
		}
		propertyMinStays.forEach(this::updateCache);
	}

	@After("execution(void com.mybookingpal.dal.server.api.PropertyMinStayMapper.insertList(..))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(propertyMinStayList)")
	public void insertList(JoinPoint joinPoint, GenericList<PropertyMinStay> propertyMinStayList) {
		if (CollectionUtils.isEmpty(propertyMinStayList)) {
			return;
		}
		propertyMinStayList.forEach(this::updateCache);
	}

	@After("execution(void com.mybookingpal.dal.server.api.PropertyMinStayMapper.copyPropertyMinStays(..))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(fromProductId, toProductId)")
	public void copyPropertyMinStays(JoinPoint joinPoint, Integer fromProductId, Integer toProductId) {

	}

	@After("execution(void com.mybookingpal.dal.server.api.PropertyMinStayMapper.cancelPropertyMinStayById(..))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(propertyMinStayId, version)")
	public void cancelPropertyMinStayById(JoinPoint joinPoint, Long propertyMinStayId, Date version) {
		PropertyMinStay propertyMinStay = propertyMinStayDao.read(propertyMinStayId);
		if (Optional.ofNullable(propertyMinStay).isPresent()) {
			updateCache(propertyMinStay);
		}
	}

	@After("execution(void com.mybookingpal.dal.server.api.PropertyMinStayMapper.updateStateOnFinalPerPm(..))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(partyId)")
	public void updateStateOnFinalPerPm(JoinPoint joinPoint, String partyId) {

	}

	@After("execution(void com.mybookingpal.dal.server.api.PropertyMinStayMapper.updateStateOnFinalPerProduct(..))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(productId)")
	public void updateStateOnFinalPerProduct(JoinPoint joinPoint, Integer productId) {
		List<PropertyMinStay> propertyMinStays = propertyMinStayDao.readByProductId(productId);
		if (CollectionUtils.isNotEmpty(propertyMinStays)) {
			propertyMinStays.forEach(this::updateCache);
		}
	}

	private void updateCache(IsCacheable cache) {
		if (!Optional.ofNullable(cache).isPresent()) {
			return;
		}
		eventAvailabilityCacheService.removeAvailabilityDataFromCache(cache.getCacheProductId(), cache.getFromDate(), cache.getToDate());
		LOG.info(String.format("Add to cache information about product - %d, fromDate - %s, toDate - %s.", cache.getCacheProductId(), cache
				.getFromDate(), cache.getToDate()));
	}
}
