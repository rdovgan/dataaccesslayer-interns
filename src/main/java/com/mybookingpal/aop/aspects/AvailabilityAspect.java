package com.mybookingpal.aop.aspects;

import com.mybookingpal.cache.service.EventAvailabilityCacheService;
import com.mybookingpal.dal.shared.AvailabilityCalendarModel;
import com.mybookingpal.shared.cache.IsCacheable;
import com.mybookingpal.shared.dao.AvailabilityCalendarModelDAO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Aspect
@Component
public class AvailabilityAspect {

	private Logger LOG = Logger.getLogger(AvailabilityAspect.class);

	@Autowired
	private AvailabilityCalendarModelDAO availabilityCalendarModelDAO;

	@Autowired
	private EventAvailabilityCacheService eventAvailabilityCacheService;

	@After("(execution(void com.mybookingpal.dal.server.api.AvailabilityCalendarMapper.create(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.AvailabilityCalendarMapper.update(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.AvailabilityCalendarMapper.insertUpdateFromTemplate(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.AvailabilityCalendarMapper.updateVersionInAvilabilityCalenderForProductAndDate(..))) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(availability)")
	public void updateAvailabilityCalendar(JoinPoint joinPoint, AvailabilityCalendarModel availability) {
		updateCache(availability);
	}

	@After("(execution(void com.mybookingpal.dal.server.api.AvailabilityCalendarMapper.insertUpdateFromList(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.AvailabilityCalendarMapper.cancelAvailabilityCalendarlist(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.AvailabilityCalendarMapper.insertAvailabilityCalendarModels(..))) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) "
			+ "&& args(availabilityCalendarModels)")
	public void updatePriceList(JoinPoint joinPoint, List<AvailabilityCalendarModel> availabilityCalendarModels) {
		updateAvailabilityCache(availabilityCalendarModels);
	}
	//BP-28512 Change storing process of availability_calendar
	/*@After("execution(void com.mybookingpal.dal.server.api.AvailabilityCalendarMapper.delete(..)) && @annotation(com.mybookingpal.aop.annotations.UpdateCache) "
			+ "&& args(idList)")
	public void clearAvailabilityCache(JoinPoint joinPoint, List<Long> idList) {
		List<Long> ids = Stream.of(joinPoint.getArgs())
				.map(Long.class::cast)
				.collect(Collectors.toList());
		List<AvailabilityCalendarModel> availabilityCalendarModels = availabilityCalendarModelDAO.readByAvailabilityCalendarIds(ids);
		Stream.of(availabilityCalendarModels)
				.map(IsCacheable.class::cast)
				.forEach(isCacheable -> eventAvailabilityCacheService.removeAvailabilityDataFromCache(isCacheable.getCacheProductId(),
						isCacheable.getFromDate(), isCacheable.getToDate()));
	}*/

	private void updateAvailabilityCache(List<AvailabilityCalendarModel> availabilityCalendarModels) {
		if (CollectionUtils.isEmpty(availabilityCalendarModels)) {
			return;
		}
		availabilityCalendarModels.forEach(this::updateCache);
	}

	private void updateCache(IsCacheable cache) {
		if (!Optional.ofNullable(cache).isPresent()) {
			return;
		}
		eventAvailabilityCacheService.removeAvailabilityDataFromCache(cache.getCacheProductId(), cache.getFromDate(), cache.getToDate());
		LOG.info(String.format("Add to cache information about product - %d, fromDate - %s, toDate - %s.", cache.getCacheProductId(), cache
				.getFromDate(), cache.getToDate()));
	}

	@Around("(execution(java.util.List<com.mybookingpal.dal.shared.AvailabilityCalendarModel> com.mybookingpal.dal.server.api.AvailabilityCalendarMapper"
			+ ".readForPeriodByProductIds(..))"
			+ "&& execution(java.util.List<com.mybookingpal.dal.shared.AvailabilityCalendarModel> com.mybookingpal.dal.server.api.AvailabilityCalendarMapper"
			+ ".readForPeriodForAllStateByProductIds(..))) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.ReadCache) "
			+ "&& args(productIdList, fromDate, toDate)")
	public List<AvailabilityCalendarModel> readForPeriodByProductIds(ProceedingJoinPoint joinPoint, List<Integer> productIdList, Date fromDate, Date toDate) throws Throwable {
		return getAvailabilityCalendarModelsByProductsAndRange(joinPoint, productIdList, fromDate, toDate);
	}

	@Around("(execution(java.util.List<com.mybookingpal.dal.shared.AvailabilityCalendarModel> com.mybookingpal.dal.server.api.AvailabilityCalendarMapper"
			+ ".readForPeriodByProductIdsByVersion(..))"
			+ "&& execution(java.util.List<com.mybookingpal.dal.shared.AvailabilityCalendarModel> com.mybookingpal.dal.server.api.AvailabilityCalendarMapper"
			+ ".readForPeriodForAllStateByProductIdsByVersion(..))) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.ReadCache) "
			+ "&& args(productIdList, fromDate, toDate, version)")
	public List<AvailabilityCalendarModel> readForPeriodByProductIdsByVersion(ProceedingJoinPoint joinPoint, List<Integer> productIdList, Date fromDate, Date
			toDate, Date version) throws Throwable {
		List<AvailabilityCalendarModel> calendarModels = getAvailabilityCalendarModelsByProductsAndRange(joinPoint, productIdList,
				fromDate, toDate);
		return filterCalendarsByVersion(version, calendarModels);
	}

	private List<AvailabilityCalendarModel> filterCalendarsByVersion(Date version, List<AvailabilityCalendarModel> calendarModels) {
		if (!Optional.ofNullable(version).isPresent()) {
			return calendarModels;
		}
		LocalDate localVersion = version.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		return calendarModels.stream().filter(calendar -> {
			LocalDate calendarVersion = version.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			return calendarVersion.isAfter(localVersion) || calendarVersion.isEqual(localVersion);
		}).collect(Collectors.toList());
	}

	private List<AvailabilityCalendarModel> getAvailabilityCalendarModelsByProductsAndRange(ProceedingJoinPoint joinPoint, List<Integer> productIdList,
			Date fromDate, Date toDate) throws Throwable {
		if (CollectionUtils.isEmpty(productIdList) || !Optional.ofNullable(fromDate).isPresent() || !Optional.ofNullable(toDate).isPresent()) {
			return (List<AvailabilityCalendarModel>) joinPoint.proceed();
		}
		LocalDate fromLocalDate = fromDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate toLocalDate = toDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		List<Long> longIds = productIdList.stream().map(Long::valueOf).collect(Collectors.toList());
		List<AvailabilityCalendarModel> availabilityCalendarModels = eventAvailabilityCacheService
				.defineAvailabilityData(longIds, fromLocalDate, toLocalDate);
		return CollectionUtils.isNotEmpty(availabilityCalendarModels) ?
				availabilityCalendarModels :
				(List<AvailabilityCalendarModel>) joinPoint.proceed();
	}

	@Around("execution(java.util.List<com.mybookingpal.dal.shared.AvailabilityCalendarModel> com.mybookingpal.dal.server.api.AvailabilityCalendarMapper.readForPeriodByProductIds(..)) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.ReadCache) "
			+ "&& args(productIdList, fromDate, toDate)")
	public List<AvailabilityCalendarModel> readForPeriodByProductIds(ProceedingJoinPoint joinPoint, List<Integer> productIdList, LocalDate fromDate, LocalDate toDate)
			throws Throwable {
		if (CollectionUtils.isEmpty(productIdList) || !Optional.ofNullable(fromDate).isPresent() || !Optional.ofNullable(toDate).isPresent()) {
			return (List<AvailabilityCalendarModel>) joinPoint.proceed();
		}
		List<Long> longIds = productIdList.stream().map(Long::valueOf).collect(Collectors.toList());
		List<AvailabilityCalendarModel> availabilityCalendarModels = eventAvailabilityCacheService
				.defineAvailabilityData(longIds, fromDate, toDate);
		return CollectionUtils.isNotEmpty(availabilityCalendarModels) ? availabilityCalendarModels : (List<AvailabilityCalendarModel>) joinPoint.proceed();
	}

	@Around("execution(java.util.List<com.mybookingpal.dal.shared.AvailabilityCalendarModel> com.mybookingpal.dal.server.api.AvailabilityCalendarMapper"
			+ ".readByProductIdAndDate(..)) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.ReadCache) "
			+ "&& args(availabilityCalendarModel)")
	public AvailabilityCalendarModel readForPeriodByProductIds(ProceedingJoinPoint joinPoint, AvailabilityCalendarModel availabilityCalendarModel) throws Throwable {
		if (!Optional.ofNullable(availabilityCalendarModel).isPresent()) {
			return (AvailabilityCalendarModel) joinPoint.proceed();
		}
		AvailabilityCalendarModel cache = eventAvailabilityCacheService
				.defineAvailabilityData(availabilityCalendarModel.getProductId(), availabilityCalendarModel.getAvailabilityDate())
				.stream()
				.findFirst()
				.orElse(null);
		return Optional.ofNullable(cache).isPresent() ? cache : (AvailabilityCalendarModel) joinPoint.proceed();
	}
}
