package com.mybookingpal.aop.aspects;

import com.mybookingpal.cache.service.EventAvailabilityCacheService;
import com.mybookingpal.dal.dao.entities.ReservationModel;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.shared.cache.IsCacheable;
import com.mybookingpal.shared.dao.ReservationDao;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Aspect
@Component
public class ReservationAspect {

	private Logger LOG = Logger.getLogger(ReservationAspect.class);

	@Autowired
	private EventAvailabilityCacheService eventAvailabilityCacheService;

	@Autowired
	private ReservationDao reservationDao;

	@After("(execution(void com.mybookingpal.dal.server.api.ReservationMapper.cancelversionbydate(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.ReservationMapper.remove(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.ReservationMapper.create(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.ReservationMapper.update(..))) "
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(reservation)")
	public void updateReservation(JoinPoint joinPoint, ReservationModel reservation) {
		updateCache(reservation);
	}

	@After("(execution(void com.mybookingpal.dal.server.api.ReservationMapper.insertList(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.ReservationMapper.cancelReservations(..)) "
			+ "|| execution(void com.mybookingpal.dal.server.api.ReservationMapper.deleteByIdList(..)))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(reservations)")
	public void updateList(JoinPoint joinPoint, List<ReservationModel> reservations) {
		if (CollectionUtils.isEmpty(reservations)) {
			return;
		}
		reservations.forEach(this::updateCache);
	}

	@After("execution(void com.mybookingpal.dal.server.api.ReservationMapper.deleteDate(..))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(action)")
	public void deleteDate(JoinPoint joinPoint, NameId action) {

	}

	@After("execution(void com.mybookingpal.dal.server.api.ReservationMapper.updateStateOnFinalPerPm(..))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(partyId)")
	public void updateStateOnFinalPerPm(JoinPoint joinPoint, String partyId) {

	}

	@After("execution(void com.mybookingpal.dal.server.api.ReservationMapper.cancelReservationById(..))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(reservationId)")
	public void cancelReservationById(JoinPoint joinPoint, Integer reservationId) {

	}

	@After("execution(void com.mybookingpal.dal.server.api.ReservationMapper.updateVersionById(..))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(id)")
	public void updateVersionById(JoinPoint joinPoint, Long id) {

	}

	@After("execution(void com.mybookingpal.dal.server.api.ReservationMapper.cancelreservationlist(..))"
			+ "&& @annotation(com.mybookingpal.aop.annotations.UpdateCache) && args(reservationIdList, version)")
	public void cancelreservationlist(JoinPoint joinPoint, List<String> reservationIdList, Date version) {
		if (CollectionUtils.isEmpty(reservationIdList)) {
			return;
		}
		List<ReservationModel> reservations = reservationDao
				.readReservations(reservationIdList.stream().map(NumberUtils::createLong).collect(Collectors.toList()));
		reservations.forEach(this::updateCache);
	}

	private void updateCache(IsCacheable cache) {
		if (!Optional.ofNullable(cache).isPresent()) {
			return;
		}
		eventAvailabilityCacheService.removeAvailabilityDataFromCache(cache.getCacheProductId(), cache.getFromDate(), cache.getToDate());
		LOG.info(String.format("Add to cache information about product - %d, fromDate - %s, toDate - %s.", cache.getCacheProductId(), cache
				.getFromDate(), cache.getToDate()));
	}
}
