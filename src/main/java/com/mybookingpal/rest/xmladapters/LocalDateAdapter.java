package com.mybookingpal.rest.xmladapters;

import com.mybookingpal.dal.utils.CommonDateUtils;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;

public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

	@Override
	public LocalDate unmarshal(String date) throws Exception {
		return CommonDateUtils.toLocalDate(date);
	}

	@Override
	public String marshal(LocalDate localDate) throws Exception {
		return CommonDateUtils.format(localDate);
	}
}
