package com.mybookingpal.rest.entity;

import java.io.Serializable;
import java.time.LocalDate;

@Deprecated
public class ProductDate implements Serializable {

	private Integer id;
	private Integer productId;
	private Integer partyId;
	private LocalDate fromDate;
	private LocalDate toDate;

	public ProductDate() {
	}

	public ProductDate(Integer id, Integer productId, Integer partyId, LocalDate fromDate, LocalDate toDate) {
		this.id = id;
		this.productId = productId;
		this.partyId = partyId;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getPartyId() {
		return partyId;
	}

	public void setPartyId(Integer partyId) {
		this.partyId = partyId;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

}