package com.mybookingpal.dal.enums;

public class ReservationConstants {

	public enum State {
		Initial,
		Provisional,
		Reserved,
		Confirmed,
		FullyPaid,
		Briefed,
		Arrived,
		PreDeparture,
		Departed,
		Closed,
		Cancelled,
		Final,
		Failed,
		Inquiry,
		Exception,
		INQUIRY_OK,
		INQUIRY_FAILED,
		Unconfirmed
	}

	public static final String[] STATES = { State.Initial.name(), State.Provisional.name(), State.Reserved.name(), State.Confirmed.name(),
			State.FullyPaid.name(), State.Briefed.name(), State.Arrived.name(), State.PreDeparture.name(), State.Departed.name(), State.Closed.name(),
			State.Cancelled.name(), State.Final.name(), State.Failed.name(), State.Inquiry.name(), State.Unconfirmed.name() };

	public static final String[] ACTIVE_STATES = { State.Provisional.name(), State.Reserved.name(), State.Confirmed.name(), State.FullyPaid.name(),
			State.Briefed.name(), State.Arrived.name() };

	public static final String[] WORKFLOW_STATES = { State.Reserved.name(), State.Confirmed.name(), State.FullyPaid.name(), State.Briefed.name(),
			State.Arrived.name(), State.PreDeparture.name(), State.Departed.name() };

	//Sortable columns
	public static final String DATE = "reservation.date";
	public static final String FROMDATE = "reservation.fromdate";
	public static final String TODATE = "reservation.todate";

	public static final String ARRIVALTIME = "14:00:00";
	public static final String DEPARTURETIME = "10:00:00";
}
