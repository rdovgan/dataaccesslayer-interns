package com.mybookingpal.dal.enums;

public class CommissionConstants {

	public enum CommissionType {
		BpCommission(1), PmIncludedCommission(2), FeeIncludedCommission(3);

		private Integer typeValue;

		CommissionType(Integer typeValue) {
			this.typeValue = typeValue;
		}

		public Integer getTypeValue() {
			return this.typeValue;
		}
	}
}
