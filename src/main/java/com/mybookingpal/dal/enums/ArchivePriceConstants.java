package com.mybookingpal.dal.enums;

public class ArchivePriceConstants {

	public enum UserType {
		Supplier, BookingPal, Channel, PMS
	}

	public enum Name {
		ChannelCommission("CHANNEL_PARTNER_COMMISSION"), BookingPalCommission("BookingPal Commission"), TotalCommissions("Total Commissions"), BpCommission(
				"BP_COMMISSION"), PmsCommission("PMS_COMMISSION"), CcFee("CREDIT_CARD_FEE"), TotalNightlyPrice("Total Nightly Price"), TotalQuote(
				"Total Quote"), TotalBeforeTax("Total Before Tax"), ArchiveGuestCounts("ARCHIVE_GUEST_COUNT"), ArchiveAdultsCounts(
				"ARCHIVE_ADULTS_COUNT"), ArchiveChildrenCounts("ARCHIVE_CHILDREN_COUNT"), ArchivePropertyId("ARCHIVE_PROPERTY_ID"), ArchivePropertyManager(
				"ARCHIVE_PROPERTY_MANAGER"), ArchiveCancellationPolicy("ARCHIVE_CANCELLATION_POLICY"), ArchiveNotes("ARCHIVE_NOTES"), ArchiveCustomerId(
				"ARCHIVE_CUSTOMER_ID"), ArchiveDates("ARCHIVE_DATES"), BpCommissionSetting("BP_COMMISSION_SETTING"), CpCommissionSetting(
				"CP_COMMISSION_SETTING");

		private String value;

		Name(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}
}
