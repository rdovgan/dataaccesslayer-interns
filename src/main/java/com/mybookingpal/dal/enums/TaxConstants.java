package com.mybookingpal.dal.enums;

import java.util.EnumSet;
import java.util.Set;

public class TaxConstants {

	public enum BookingComSupportedTaxes {
		City("City", "", 3, "City Tax"), Tax("Tax", "", 13, "TAX"), Goods("Goods", "Service", 35, "Goods and services tax"),
		VAT("VAT", "Value Added", 36, "VAT (Value Added Tax) **"), Government("Government", "", 46, "Government tax"),
		Spa("Spa", "", 5001, "Spa tax"), Hot_Spring("Hot Spring", "", 5002, "Hot spring tax"), Residential("Residential", "", 5004, "Residential tax"),
		Facilities("Facilities", "Fitness", 5007, "Sauna/fitness facilities tax"), Local("Local", "Council", 5008, "Local council tax");

		private String value;
		private String secondCheckValue;
		private Integer channelCode;
		private String channelName;

		BookingComSupportedTaxes(String value, String secondCheckValue, Integer channelCode, String channelName) {
			this.value = value;
			this.secondCheckValue = secondCheckValue;
			this.channelCode = channelCode;
			this.channelName = channelName;
		}

		public static Set<BookingComSupportedTaxes> getBookingComSupportedTaxes() {
			return EnumSet.of(City, Goods, VAT, Government, Spa, Hot_Spring, Residential, Facilities, Local, Tax);
		}

		public static Set<BookingComSupportedTaxes> getBookingComSupportedTaxesWithoutGeneralTax() {
			return EnumSet.of(City, Goods, VAT, Government, Spa, Hot_Spring, Residential, Facilities, Local);
		}

		public String getValue() {
			return value;
		}

		public String getSecondCheckValue() {
			return secondCheckValue;
		}

		public Integer getChannelCode() {
			return channelCode;
		}

		public String getChannelName() {
			return channelName;
		}
	}
}
