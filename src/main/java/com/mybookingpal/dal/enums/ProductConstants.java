package com.mybookingpal.dal.enums;

import com.mybookingpal.dal.server.util.enums.BookingPalEnums;
import org.apache.commons.collections4.SetUtils;

import java.sql.Time;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ProductConstants {

	@Deprecated
	public static final String INITIAL = "Initial";
	@Deprecated
	public static final String FINAL = "Final";
	@Deprecated
	public static final String CREATED = "Created";
	@Deprecated
	public static final String SUSPENDED = "Suspended";
	@Deprecated
	public static final String PROCESSING = "Processing";
	@Deprecated
	public static final String UNLICENSED = "Unlicensed";
	@Deprecated
	public static final String INCOMPLETE = "Incomplete";

	public static final int NAME_PROPERTY_MAX_CHAR = 150;

	// inquire states
	public static final String USE_API = "Use API";
	public static final String SEND_EMAIL = "Send e-mail";

	// CheckIN, CheckOUT time
	public static final Time DEFAULT_CHECK_IN_TIME = Time.valueOf("10:30:00");
	public static final Time DEFAULT_CHECK_OUT_TIME = Time.valueOf("10:30:00");

	public static final int DEFAULT_PERSON_COUNT = 0;
	public static final int DEFAULT_CHILD_COUNT = 0;
	public static final int DEFAULT_STANDARD_PERSON_COUNT = 0;

	public static final String ACCOMMODATION_SIZE = "Accommodation Size";
	public static final String GRADING = "Grading";
	public static String[] ATTRIBUTE_TYPES = { ACCOMMODATION_SIZE, GRADING };
	public static final Double DEFAULT_SECUIRTY_DEPOSIT = 0.0;
	public static final Double DEFAULT_CLEANING_FEE = 0.0;

	public static final String RF_OPTIMIZATION = "rfOptimization";
	public static final String RT_DISTRIBUTE = "rtDistribute";

	@Deprecated
	public static final String[] STATES = { INITIAL, CREATED, SUSPENDED, PROCESSING, FINAL };

	public enum Servicetype {
		NoService,
		DayofArrival,
		DayofDeparture,
		RefreshDay,
		LinenChange
	}

	public enum Type {
		Accommodation,
		Consumable,
		Inventory,
		Maintenance,
		Marketing
	}

	public enum State {
		Initial("Initial"),
		Final("Final"),
		Created("Created"),
		Suspended("Suspended"),
		Processing("Processing"),
		Unlicensed("Unlicensed"),
		Incomplete("Incomplete");

		private String value;

		State(String state) {
			this.value = state;
		}

		public String getValue() {
			return value;
		}
	}

	public enum MultiUnit {
		OWN,//Hotel
		PRM,//Master
		SGL,//Single unit
		MLT,//Multi unit (room type, parent)
		SUB; //Room (sub parent)

		public static List<String> getMultiUnitRooms() {
			return Arrays.asList(PRM.name(), SGL.name(), SUB.name());
		}
	}

	public enum ProductGroup {
		MULTI_REP,
		MULTI_KEY,
		KEY;

		public static ProductGroup getByName(String groupName) {
			return BookingPalEnums.getEnum(ProductGroup.class, groupName);
		}
	}

	public enum AvailabilityProductType {
		MultiRepOwn("MULTI REP OWN", ProductGroup.MULTI_REP, MultiUnit.OWN, false),
		MultiRepMlt("MULTI REP MLT", ProductGroup.MULTI_REP, MultiUnit.MLT, false),
		MultiKeyOwn("MULTI KEY OWN", ProductGroup.MULTI_KEY, MultiUnit.OWN, false),
		MultiKeyMlt("MULTI KEY MLT", ProductGroup.MULTI_KEY, MultiUnit.MLT, false),
		MultiKeyPrm("MULTI KEY PRM", ProductGroup.MULTI_KEY, MultiUnit.PRM, false),
		MultiKeySub("MULTI KEY SUB", ProductGroup.MULTI_KEY, MultiUnit.SUB, false),
		Single("SGL", ProductGroup.KEY, MultiUnit.SGL, false),
		MultiCluster("MULTI CLUSTER", ProductGroup.MULTI_REP, MultiUnit.MLT, true);

		private String name;
		private ProductGroup group;
		private MultiUnit unit;
		private boolean linked;

		AvailabilityProductType(String name, ProductGroup group, MultiUnit unit, boolean isLinked) {
			this.name = name;
			this.group = group;
			this.unit = unit;
			this.linked = isLinked;
		}

		public String getName() {
			return this.name;
		}

		public ProductGroup getGroup() {
			return this.group;
		}

		public MultiUnit getUnit() {
			return this.unit;
		}

		public boolean isLinked() {
			return this.linked;
		}
	}

	public enum Value {
		Brand,
		City,
		CleaningCost,
		Country,
		DamageFee,
		Elevator, //true if it has an elevator
		Floor,
		HalfBaths,
		InsuranceRate,
		KingBeds,
		Maxrentalprice,
		Minrentalprice,
		PropertyType,
		QueenBeds,
		Region,
		ResortTaxRate,
		SingleBeds,
		SlopeProximity,
		Smoking,
		SquareFeet,
		SquareMetre,
		TaxRate,
		ThreeQuarterBaths,
		Unit,
		YearBuilt
	}

}
