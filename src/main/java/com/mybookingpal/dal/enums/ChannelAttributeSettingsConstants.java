package com.mybookingpal.dal.enums;

import java.util.Arrays;

/**
 * @author Mihailo Spasojevic
 */
public class ChannelAttributeSettingsConstants {

    public enum NativePropertyType {
        HomesAndApartments(0, "Homes / apartments"), Hotels(1, "Hotels");

        private Integer id;
        private String name;

        NativePropertyType(Integer id, String name) {
            this.id = id;
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public static ChannelAttributeSettingsConstants.NativePropertyType getByName(String name) {
            return Arrays.stream(ChannelAttributeSettingsConstants.NativePropertyType.values())
                    .filter(textType -> textType.getId() != null && textType.getName().equals(name)).findFirst().orElse(null);
        }

        public static ChannelAttributeSettingsConstants.NativePropertyType getById(Integer id) {
            return Arrays.stream(ChannelAttributeSettingsConstants.NativePropertyType.values())
                    .filter(textType -> textType.getId() != null && textType.getId().equals(id)).findFirst().orElse(null);
        }
    }

    public enum AcceptsPropertyType {
        HomesAndApartments(0, "Homes / apartments"), Hotels(1, "Hotels"), Both(2, "Both");

        private Integer id;
        private String name;

        AcceptsPropertyType(Integer id, String name) {
            this.id = id;
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public static ChannelAttributeSettingsConstants.AcceptsPropertyType getByName(String name) {
            return Arrays.stream(ChannelAttributeSettingsConstants.AcceptsPropertyType.values())
                    .filter(textType -> textType.getId() != null && textType.getName().equals(name)).findFirst().orElse(null);
        }

        public static ChannelAttributeSettingsConstants.AcceptsPropertyType getById(Integer id) {
            return Arrays.stream(ChannelAttributeSettingsConstants.AcceptsPropertyType.values())
                    .filter(textType -> textType.getId() != null && textType.getId().equals(id)).findFirst().orElse(null);
        }
    }

    public enum BookingType {
        Instant(0, "Instant"), RequestToBook(1, "Request to book"), Both(2, "Both");


        private Integer id;
        private String name;

        BookingType(Integer id, String name) {
            this.id = id;
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public static ChannelAttributeSettingsConstants.BookingType getByName(String name) {
            return Arrays.stream(ChannelAttributeSettingsConstants.BookingType.values())
                    .filter(textType -> textType.getId() != null && textType.getName().equals(name)).findFirst().orElse(null);
        }

        public static ChannelAttributeSettingsConstants.BookingType getById(Integer id) {
            return Arrays.stream(ChannelAttributeSettingsConstants.BookingType.values())
                    .filter(textType -> textType.getId() != null && textType.getId().equals(id)).findFirst().orElse(null);
        }
    }

    public enum RatesAndAvailabilityMapping {
        MapToProperty(0, "Map to property"), MapToRoom(1, "Map to room"), MapToRatePlan(2, "Map to rate plan");

        private Integer id;
        private String name;

        RatesAndAvailabilityMapping(Integer id, String name) {
            this.id = id;
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public static ChannelAttributeSettingsConstants.RatesAndAvailabilityMapping getByName(String name) {
            return Arrays.stream(ChannelAttributeSettingsConstants.RatesAndAvailabilityMapping.values())
                    .filter(textType -> textType.getId() != null && textType.getName().equals(name)).findFirst().orElse(null);
        }

        public static ChannelAttributeSettingsConstants.RatesAndAvailabilityMapping getById(Integer id) {
            return Arrays.stream(ChannelAttributeSettingsConstants.RatesAndAvailabilityMapping.values())
                    .filter(textType -> textType.getId() != null && textType.getId().equals(id)).findFirst().orElse(null);
        }
    }

}
