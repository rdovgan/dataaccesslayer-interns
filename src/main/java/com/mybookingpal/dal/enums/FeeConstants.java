package com.mybookingpal.dal.enums;

import java.util.EnumSet;
import java.util.Set;

public class FeeConstants {
	public enum EntityTypeEnum {
		// PAL - Pay on arrival
		MANDATORY(1, "MANDATORY"), OPTIONAL(2, "OPTIONAL"), MANDATORY_PAL(3, "MANDATORY_PAL"), UNSUPPORTED(0, "UNSUPPORTED_TYPE");

		private Integer value;
		private String name;

		EntityTypeEnum(Integer value, String name) {
			this.value = value;
			this.name = name;
		}

		public Integer getValue() {
			return this.value;
		}

		public String getName() {
			return this.name;
		}

		public static EntityTypeEnum getByInt(int value) {
			switch (value) {
			case 1:
				return MANDATORY;
			case 2:
				return OPTIONAL;
			case 3:
				return MANDATORY_PAL;
			default:
				return UNSUPPORTED;
			}
		}

		public static String getNameByInt(Integer value) {
			EntityTypeEnum feeUnit = null;

			if (value != null) {
				feeUnit = getByInt(value);
			}

			if (feeUnit != null) {
				return feeUnit.getName();
			}

			return "";
		}
	}

	public enum FeeValueType {
		FLAT(1), PERCENT(2);

		private Integer valueType;

		FeeValueType(Integer valueType) {
			this.valueType = valueType;
		}

		public Integer getValue() {
			return this.valueType;
		}

		public static boolean isValueTypeFlat(Integer valueType) {
			return FLAT.valueType.equals(valueType);
		}

		public static boolean isValueTypePercent(Integer valueType) {
			return PERCENT.valueType.equals(valueType);
		}
	}

	public enum FeeUnitEnum {
		PER_STAY(1, "PER_STAY"), PER_DAY(2, "PER_DAY"), PER_PERSON(3, "PER_PERSON"),
		PER_DAY_PER_PERSON(4, "PER_DAY_PER_PERSON"), PER_PERSON_EXTRA(5, "PER_PERSON_EXTRA"),
		PER_DAY_PER_PERSON_EXTRA(6, "PER_DAY_PER_PERSON_EXTRA"), UNSUPPORTED(0, "UNSUPPORTED_TYPE");

		private Integer value;
		private String name;

		FeeUnitEnum(Integer value, String name) {
			this.value = value;
			this.name = name;
		}

		public Integer getValue() {
			return this.value;
		}

		public String getName() {
			return this.name;
		}

		public static FeeUnitEnum getByInt(int value) {
			switch (value) {
			case 1:
				return PER_STAY;
			case 2:
				return PER_DAY;
			case 3:
				return PER_PERSON;
			case 4:
				return PER_DAY_PER_PERSON;
			case 5:
				return PER_PERSON_EXTRA;
			case 6:
				return PER_DAY_PER_PERSON_EXTRA;
			default:
				return UNSUPPORTED;
			}
		}

		public static boolean isPerDayUnit(Integer unit) {
			return PER_DAY.getValue().equals(unit) || PER_DAY_PER_PERSON.getValue().equals(unit) || PER_DAY_PER_PERSON_EXTRA.getValue().equals(unit);
		}

		public static boolean isPerPersonUnit(Integer unit) {
			return PER_PERSON.getValue().equals(unit) || PER_PERSON_EXTRA.getValue().equals(unit) || PER_DAY_PER_PERSON.getValue().equals(unit) || PER_DAY_PER_PERSON_EXTRA.getValue().equals(unit);
		}

		public static String getNameByInt(Integer value) {

			FeeUnitEnum feeUnit = null;

			if (value != null) {
				feeUnit = getByInt(value);
			}

			if (feeUnit != null) {
				return feeUnit.getName();
			}

			return "";
		}
	}

	public enum FeeType {
		UNSUPPORTED(0, "UNSUPPORTED_TYPE"), GENERAL(1, "GENERAL"), PET_FEE(2, "PET_FEE"), DEPOSIT(3, "DEPOSIT");

		private Integer value;
		private String name;

		FeeType(Integer value, String name) {
			this.value = value;
			this.name = name;
		}

		public Integer getValue() {
			return this.value;
		}

		public String getName() {
			return this.name;
		}

		public static FeeType getByInt(Integer value) {
			switch (value) {
			case 1:
				return GENERAL;
			case 2:
				return PET_FEE;
			case 3:
				return DEPOSIT;
			default:
				return UNSUPPORTED;
			}
		}

		public static String getNameByInt(Integer value) {

			FeeType feeType = null;

			if (value != null) {
				feeType = getByInt(value);
			}

			if (feeType != null) {
				return feeType.getName();
			}

			return "";
		}
	}

	public enum BookingComSupportedFees {
		Resort("Resort", "", 12, "Resort fee"), Service("Service", "Charge", 14, "Service charge"),
		Public_transit("Public transit", "Transit", 5005, "Public transit day ticket"),
		Heritage("Heritage", "", 5006, "Heritage charge"), Clean("Clean", "", 5009, "Cleaning fee"), Towel("Towel", "", 5010, "Towel charge"),
		Electricity("Electricity", "", 5011, "Electricity fee"), Bed_linen("Bed linen", "Bed", 5012, "Bed linen fee"), Gas("Gas", "", 5013, "Gas fee"),
		Oil("Oil", "", 5014, "Oil fee"), Wood("Wood", "", 5015, "Wood fee"), Water("Water", "", 5016, "Water usage fee"),
		Transfer("Transfer", "", 5017, "Transfer fee"), Linen("Linen", "", 5018, "Linen package fee"), Heating("Heating", "", 5019, "Heating fee"),
		Air_Conditioning("Air Conditioning", "AC", 5020, "Air conditioning fee"), Kitchen_Linen("Kitchen Linen", "Kitchen", 5021, "Kitchen linen fee"),
		Housekeeping("Housekeeping", "Housekeep", 5022, "Housekeeping fee"),
		Airport_Shuttle("Airport Shuttle", "Airport", 5023, "Airport shuttle fee"), Shuttle_Boat("Shuttle Boat", "", 5024, "Shuttle boat fee"),
		Sea_Plane("Sea Plane", "", 5025, "Sea plane fee"), Ski_pass("Ski pass", "Ski", 5026, "Ski pass"), Final_Cleaning("Final Cleaning", "", 5027, "Final cleaning fee"),
		Wristband("Wristband", "", 5028, "Wristband fee"), Visa("Visa", "process", 5029, "Visa support fee"),
		Water_Park("Water Park", "", 5030, "Water park fee"), Club_Card("Club Card", "", 5031, "Club card fee"), Conservation("Conservation", "", 5032, "Conservation fee"),
		Internet("Internet", "", 5035, "Internet fee"), Parking("Parking", "", 5036, "Parking fee");

		private String value;
		private String secondCheckValue;
		private Integer channelCode;
		private String channelName;

		BookingComSupportedFees(String value, String secondCheckValue, Integer channelCode, String channelName) {
			this.value = value;
			this.secondCheckValue = secondCheckValue;
			this.channelCode = channelCode;
			this.channelName = channelName;
		}

		public static Set<BookingComSupportedFees> getBookingComSupportedFees() {
			return EnumSet
					.of(Resort, Service, Public_transit, Heritage, Clean, Towel, Electricity, Bed_linen, Gas, Oil, Wood,
							Water, Transfer, Linen, Heating, Air_Conditioning, Kitchen_Linen, Housekeeping, Airport_Shuttle, Shuttle_Boat, Sea_Plane, Ski_pass,
							Final_Cleaning, Wristband, Visa, Water_Park, Club_Card, Conservation, Internet, Parking);
		}

		public String getValue() {
			return value;
		}

		public String getSecondCheckValue() {
			return secondCheckValue;
		}

		public Integer getChannelCode() {
			return channelCode;
		}

		public String getChannelName() {
			return channelName;
		}
	}

	public enum GroupedValueAndTaxTypesFees {
		FLAT_TAXABLE, FLAT_NOT_TAXABLE, PERCENT_TAXABLE, PERCENT_NOT_TAXABLE
	}

	public enum AirbnbSupportedFees {
		Pass_Through_Management_Fee("pass_through_management_fee", 1), Cleaning_Fee("cleaning_fee", 2), Pass_Through_Resort_Fee("pass_through_resort_fee",
				3), Pass_Through_Community_Fee("pass_through_community_fee", 4), Pass_Through_Linen_Fee("pass_through_linen_fee", 5);

		private Integer channelCode;
		private String channelName;

		AirbnbSupportedFees(String channelName, Integer channelCode) {
			this.channelCode = channelCode;
			this.channelName = channelName;
		}

		public Integer getChannelCode() {
			return channelCode;
		}

		public String getChannelName() {
			return channelName;
		}
	}
}
