package com.mybookingpal.dal.enums;

import com.mybookingpal.dal.server.util.enums.BookingPalEnums;

public class RestrictionConstants {


	public static final String LOG_MESSAGE_FORMAT = "Process restriction with id %s, type %s, from date %s, to date %s, param %s";
	public static final String PARAMETER_EMPTY_VALUE = "is empty.";

	public static final String ONE_DAY = "24";
	public static final String TWO_DAYS = "48";
	public static final String THREE_DAYS = "72";
	public static final Integer MAX_AGE = 99;
	public static final Integer MIN_AGE = 1;

	public enum RestrictionType implements BookingPalEnums.Valued {

		CHECKIN_RULE_TYPE(1),
		CHECKOUT_RULE_TYPE(2),
		WEEKLY_RULE_TYPE(3),
		CHECKIN_NOT_ALLOWED_RULE_TYPE(4),
		CHECKOUT_NOT_ALLOWED_RULE_TYPE(5),
		ADVANCED_CHECKIN_RULE_TYPE(6),
		MIN_DAYS_BEFORE_RATE_PLAN_ACTIVATION(7),
		MAX_DAYS_BEFORE_RATE_PLAN_ACTIVATION(8),
		MAX_STAY(9),
		MINIMUM_RENTING_AGE(10);

		private final int value;

		RestrictionType(int value) {

			this.value = value;
		}

		@Override
		public String getStringValue() {
			return String.valueOf(this.value);
		}

		@Override
		public int getValue() {
			return value;
		}

		public static RestrictionType getByValue(int value) {
			for (RestrictionType restrictionType : RestrictionType.values()) {
				if (restrictionType.getValue() == value) {
					return restrictionType;
				}
			}
			throw new IllegalArgumentException("RestrictionType has no enum with value " + value);
		}
	}

	public enum State implements BookingPalEnums.Valued {
		CREATED(1), SUSPENDED(2), FINAL(3);

		private Integer value;

		State(Integer value) {
			this.value = value;
		}

		@Override
		public int getValue() {
			return value;
		}

		@Override
		public String getStringValue() {
			throw new UnsupportedOperationException();
		}
	}

}
