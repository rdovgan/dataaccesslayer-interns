package com.mybookingpal.dal.dao.entities;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class AttributeDisplayModel {

	private Long id;
	private String attributeCode;
	private String displayName;
	private String displayCategory;
	private Boolean displayRootLevel;
	private Boolean displayParentLevel;
	private Boolean useOnly;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAttributeCode() {
		return attributeCode;
	}

	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayCategory() {
		return displayCategory;
	}

	public void setDisplayCategory(String displayCategory) {
		this.displayCategory = displayCategory;
	}

	public Boolean getDisplayRootLevel() {
		return displayRootLevel;
	}

	public void setDisplayRootLevel(Boolean displayRootLevel) {
		this.displayRootLevel = displayRootLevel;
	}

	public Boolean getDisplayParentLevel() {
		return displayParentLevel;
	}

	public void setDisplayParentLevel(Boolean displayParentLevel) {
		this.displayParentLevel = displayParentLevel;
	}

	public Boolean getUseOnly() {
		return useOnly;
	}

	public void setUseOnly(Boolean useOnly) {
		this.useOnly = useOnly;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		AttributeDisplayModel that = (AttributeDisplayModel) o;

		return new EqualsBuilder().append(id, that.id)
				.append(attributeCode, that.attributeCode)
				.append(displayName, that.displayName)
				.append(displayCategory, that.displayCategory)
				.append(displayRootLevel, that.displayRootLevel)
				.append(displayParentLevel, that.displayParentLevel)
				.append(useOnly, that.useOnly)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(id)
				.append(attributeCode)
				.append(displayName)
				.append(displayCategory)
				.append(displayRootLevel)
				.append(displayParentLevel)
				.append(useOnly)
				.toHashCode();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("AttributeDisplayModel{");
		sb.append("id=").append(id);
		sb.append(", attributeCode='").append(attributeCode).append('\'');
		sb.append(", displayName='").append(displayName).append('\'');
		sb.append(", displayCategory='").append(displayCategory).append('\'');
		sb.append(", displayRootLevel=").append(displayRootLevel);
		sb.append(", displayParentLevel=").append(displayParentLevel);
		sb.append(", useOnly=").append(useOnly);
		sb.append('}');
		return sb.toString();
	}
}
