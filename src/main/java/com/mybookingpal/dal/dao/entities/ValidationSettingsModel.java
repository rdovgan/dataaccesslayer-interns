package com.mybookingpal.dal.dao.entities;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ValidationSettingsModel {

	/**
	 * Validation type. Enum names should be the same as in type of validation_type.java_enum field in the database.
	 * Id parameter should fit the id of the according validation type in validation_type table.
	 */
	public enum Type {
		/**
		 * At least one fee and one tax assigned to product
		 */
		FEE_AND_TAX(1),
		FEE(2),
		TAX(3);

		private final long id;

		Type(long id) {
			this.id = id;
		}

		public long getId() {
			return id;
		}
	}

	private Long id;
	private Long productId;
	private Long propertyManagerPartyId;
	private Type validationType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getPropertyManagerPartyId() {
		return propertyManagerPartyId;
	}

	public void setPropertyManagerPartyId(Long propertyManagerPartyId) {
		this.propertyManagerPartyId = propertyManagerPartyId;
	}

	public Type getValidationType() {
		return validationType;
	}

	public void setValidationType(Type validationType) {
		this.validationType = validationType;
	}
}
