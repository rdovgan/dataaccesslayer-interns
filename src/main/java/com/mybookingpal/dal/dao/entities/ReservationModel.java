package com.mybookingpal.dal.dao.entities;

import com.mybookingpal.dal.entity.IsReservation;
import com.mybookingpal.shared.cache.IsCacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Component("ReservationModel")
@Scope(value = "prototype")
public class ReservationModel implements IsReservation, IsCacheable {

	protected Integer id;
	protected Integer parentId;
	protected Integer organizationId;
	protected Integer agentId;
	protected Integer customerId;
	protected Integer serviceId;
	protected Integer actorId;
	protected Integer productId;
	protected String altPartyId;
	protected String altId;
	protected Integer financeId;
	protected String name;
	protected String state;
	protected Boolean supplierResDateValidation;
	protected String flat;
	protected String market;
	protected String outcome;
	protected String unit;
	protected String arrivalTime;
	protected String departureTime;
	protected String serviceFrom;
	protected String serviceTo;
	protected LocalDateTime date;
	protected LocalDate fromDate;
	protected LocalDate toDate;
	protected LocalDate dueDate;
	protected LocalDate doneDate;
	protected Double deposit;
	protected Integer adult;
	protected Integer child;
	protected Integer infant;
	protected Integer quantity;
	protected Double price;
	protected Double quote;
	protected Double cost;
	protected Double extra;
	protected String currency;
	protected String cardholder;
	protected String cardNumber;
	protected String cardMonth;
	protected String cardYear;
	protected String cardCode;
	protected String servicePayer;
	protected Boolean termsAccepted;
	protected String notes;
	protected Long groupId;
	protected Date version;

	public ReservationModel() {
	}

	public ReservationModel(ReservationModel reservationModel) {
		super();
		this.id = reservationModel.getId();
		this.parentId = reservationModel.getParentId();
		this.organizationId = reservationModel.getOrganizationId();
		this.agentId = reservationModel.getAgentId();
		this.customerId = reservationModel.getCustomerId();
		this.serviceId = reservationModel.getServiceId();
		this.actorId = reservationModel.getActorId();
		this.productId = reservationModel.getProductId();
		this.altPartyId = reservationModel.getAltPartyId();
		this.altId = reservationModel.getAltId();
		this.financeId = reservationModel.getFinanceId();
		this.name = reservationModel.getName();
		this.state = reservationModel.getState();
		this.supplierResDateValidation = reservationModel.getSupplierResDateValidation();
		this.flat = reservationModel.getFlat();
		this.market = reservationModel.getMarket();
		this.outcome = reservationModel.getOutcome();
		this.unit = reservationModel.getUnit();
		this.arrivalTime = reservationModel.getArrivalTime();
		this.departureTime = reservationModel.getDepartureTime();
		this.serviceFrom = reservationModel.getServiceFrom();
		this.serviceTo = reservationModel.getServiceTo();
		this.date = reservationModel.getDate();
		this.fromDate = reservationModel.getFromDate();
		this.toDate = reservationModel.getToDate();
		this.dueDate = reservationModel.getDueDate();
		this.doneDate = reservationModel.getDoneDate();
		this.deposit = reservationModel.getDeposit();
		this.adult = reservationModel.getAdult();
		this.child = reservationModel.getChild();
		this.infant = reservationModel.getInfant();
		this.quantity = reservationModel.getQuantity();
		this.price = reservationModel.getPrice();
		this.quote = reservationModel.getQuote();
		this.cost = reservationModel.getCost();
		this.extra = reservationModel.getExtra();
		this.currency = reservationModel.getCurrency();
		this.cardholder = reservationModel.getCardholder();
		this.cardNumber = reservationModel.getCardNumber();
		this.cardMonth = reservationModel.getCardMonth();
		this.cardYear = reservationModel.getCardYear();
		this.cardCode = reservationModel.getCardCode();
		this.servicePayer = reservationModel.getServicePayer();
		this.termsAccepted = reservationModel.getTermsAccepted();
		this.notes = reservationModel.getNotes();
		this.groupId = reservationModel.getGroupId();
		this.version = reservationModel.getVersion();
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public Integer getAgentId() {
		return agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getActorId() {
		return actorId;
	}

	public void setActorId(Integer actorId) {
		this.actorId = actorId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getAltPartyId() {
		return altPartyId;
	}

	public void setAltPartyId(String altPartyId) {
		this.altPartyId = altPartyId;
	}

	public String getAltId() {
		return altId;
	}

	public void setAltId(String altId) {
		this.altId = altId;
	}

	public Integer getFinanceId() {
		return financeId;
	}

	public void setFinanceId(Integer financeId) {
		this.financeId = financeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Boolean getSupplierResDateValidation() {
		return supplierResDateValidation;
	}

	public void setSupplierResDateValidation(Boolean supplierResDateValidation) {
		this.supplierResDateValidation = supplierResDateValidation;
	}

	public String getFlat() {
		return flat;
	}

	public void setFlat(String flat) {
		this.flat = flat;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getOutcome() {
		return outcome;
	}

	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getServiceFrom() {
		return serviceFrom;
	}

	public void setServiceFrom(String serviceFrom) {
		this.serviceFrom = serviceFrom;
	}

	public String getServiceTo() {
		return serviceTo;
	}

	public void setServiceTo(String serviceTo) {
		this.serviceTo = serviceTo;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

	public LocalDate getDoneDate() {
		return doneDate;
	}

	public void setDoneDate(LocalDate doneDate) {
		this.doneDate = doneDate;
	}

	public Double getDeposit() {
		return deposit;
	}

	public void setDeposit(Double deposit) {
		this.deposit = deposit;
	}

	public Integer getAdult() {
		return adult == null ? 0 : adult;
	}

	public void setAdult(Integer adult) {
		this.adult = adult;
	}

	public Integer getChild() {
		return child == null ? 0 : child;
	}

	public void setChild(Integer child) {
		this.child = child;
	}

	public Integer getInfant() {
		return infant == null ? 0 : infant;
	}

	public void setInfant(Integer infant) {
		this.infant = infant;
	}

	public Integer getQuantity() {
		return quantity == null ? 1 : quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return price == null ? 0D : price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getQuote() {
		return quote == null ? 0D : quote;
	}

	public void setQuote(Double quote) {
		this.quote = quote;
	}

	public Double getExtra() {
		return extra == null ? 0D : extra;
	}

	public void setExtra(Double extra) {
		this.extra = extra;
	}

	public Double getCost() {
		return cost == null ? 0D : cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCardholder() {
		return cardholder;
	}

	public void setCardholder(String cardholder) {
		this.cardholder = cardholder;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardMonth() {
		return cardMonth;
	}

	public void setCardMonth(String cardMonth) {
		this.cardMonth = cardMonth;
	}

	public String getCardYear() {
		return cardYear;
	}

	public void setCardYear(String cardYear) {
		this.cardYear = cardYear;
	}

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public String getServicePayer() {
		return servicePayer;
	}

	public void setServicePayer(String servicePayer) {
		this.servicePayer = servicePayer;
	}

	public Boolean getTermsAccepted() {
		return termsAccepted;
	}

	public void setTermsAccepted(Boolean termsAccepted) {
		this.termsAccepted = termsAccepted;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	@Override
	public Long getCacheProductId() {
		return Long.valueOf(productId);
	}

}
