package com.mybookingpal.dal.dao.entities;

import com.google.gson.annotations.SerializedName;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PaymentCredentialsModel {
    @SerializedName("name_of_bank")
    private String nameOfBank;
    @SerializedName("account_owner_name")
    private String accountOwnerName;
    @SerializedName("bic")
    private String bic;
    @SerializedName("iban")
    private String iban;
    @SerializedName("billing_authorized")
    private boolean billingAuthorized;
    @SerializedName("authorization_creation_date")
    private String authorizationCreationDate;
    @SerializedName("sepa_invoicing_reference_id")
    private String sepaInvoicingReferenceId;
}
