package com.mybookingpal.dal.dao.entities;


public class LowInventoryRoom {

	private Long id;
	private Long lowInventoryConfigurationId;
	private Integer resortId;
	private String resortName;
	private String roomName;
	private Integer roomId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLowInventoryConfigurationId() {
		return lowInventoryConfigurationId;
	}

	public void setLowInventoryConfigurationId(Long lowInventoryConfigurationId) {
		this.lowInventoryConfigurationId = lowInventoryConfigurationId;
	}

	public Integer getResortId() {
		return resortId;
	}

	public void setResortId(Integer resortId) {
		this.resortId = resortId;
	}

	public String getResortName() {
		return resortName;
	}

	public void setResortName(String resortName) {
		this.resortName = resortName;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

}