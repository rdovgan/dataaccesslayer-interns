package com.mybookingpal.dal.dao.entities;

import java.time.LocalDate;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.rest.GuestDto;
import com.mybookingpal.dal.shared.PendingFailedTransaction;

@Component
@Scope(value = "prototype")
public class FailedTransactionPaymentModel {

	private Long id;
	private Date entryDateTime;
	private String channelReservationId;
	private Long transactionId;
	private PendingFailedTransaction.Status status;
	private Date assignedDateTime;
	private Date resolvedDateTime;
	private Long paymentGatewayId;
	private String reason;
	private Date lastDayToCancel;
	private PendingFailedTransaction.PaymentType paymentType;
	private String comment;
	private PendingFailedTransaction.Actions actions;
	private GuestDto guest;
	private FailedTransactionPaymentAssignee assignee;
	private Long groupedReservationId;
	private Reservation reservation;
	private Property property;
	private Date bookingComNotified;
	private Long millisecondsRemain;
	private Long channelProductId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(Date entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public String getChannelReservationId() {
		return channelReservationId;
	}

	public void setChannelReservationId(String channelReservationId) {
		this.channelReservationId = channelReservationId;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public PendingFailedTransaction.Status getStatus() {
		return status;
	}

	public void setStatus(PendingFailedTransaction.Status status) {
		this.status = status;
	}

	public Date getAssignedDateTime() {
		return assignedDateTime;
	}

	public void setAssignedDateTime(Date assignedDateTime) {
		this.assignedDateTime = assignedDateTime;
	}

	public Date getResolvedDateTime() {
		return resolvedDateTime;
	}

	public void setResolvedDateTime(Date resolvedDateTime) {
		this.resolvedDateTime = resolvedDateTime;
	}

	public Long getPaymentGatewayId() {
		return paymentGatewayId;
	}

	public void setPaymentGatewayId(Long paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getLastDayToCancel() {
		return lastDayToCancel;
	}

	public void setLastDayToCancel(Date lastDayToCancel) {
		this.lastDayToCancel = lastDayToCancel;
	}

	public PendingFailedTransaction.PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PendingFailedTransaction.PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public PendingFailedTransaction.Actions getActions() {
		return actions;
	}

	public void setActions(PendingFailedTransaction.Actions actions) {
		this.actions = actions;
	}

	public GuestDto getGuest() {
		return guest;
	}

	public void setGuest(GuestDto guest) {
		this.guest = guest;
	}

	public FailedTransactionPaymentAssignee getAssignee() {
		return assignee;
	}

	public void setAssignee(FailedTransactionPaymentAssignee assignee) {
		this.assignee = assignee;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	public Property getProperty() {
		return property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public Date getBookingComNotified() {
		return bookingComNotified;
	}

	public void setBookingComNotified(Date bookingComNotified) {
		this.bookingComNotified = bookingComNotified;
	}

	public Long getMillisecondsRemain() {
		return millisecondsRemain;
	}

	public void setMillisecondsRemain(Long millisecondsRemain) {
		this.millisecondsRemain = millisecondsRemain;
	}

	public Long getChannelProductId() {
		return channelProductId;
	}

	public void setChannelProductId(Long channelProductId) {
		this.channelProductId = channelProductId;
	}

	public static class Reservation {

		private String ids;
		private LocalDate createdDate;
		private LocalDate fromDate;
		private LocalDate toDate;
		private String channelName;
		private String channelAbbreviation;

		public String getIds() {
			return ids;
		}

		public void setIds(String ids) {
			this.ids = ids;
		}

		public LocalDate getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(LocalDate createdDate) {
			this.createdDate = createdDate;
		}

		public LocalDate getFromDate() {
			return fromDate;
		}

		public void setFromDate(LocalDate fromDate) {
			this.fromDate = fromDate;
		}

		public LocalDate getToDate() {
			return toDate;
		}

		public void setToDate(LocalDate toDate) {
			this.toDate = toDate;
		}

		public String getChannelName() {
			return channelName;
		}

		public void setChannelName(String channelName) {
			this.channelName = channelName;
		}

		public String getChannelAbbreviation() {
			return channelAbbreviation;
		}

		public void setChannelAbbreviation(String channelAbbreviation) {
			this.channelAbbreviation = channelAbbreviation;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this).append("ids", ids).append("createdDate", createdDate).append("fromDate", fromDate).append("toDate", toDate)
					.append("channelName", channelName).append("channelAbbreviation", channelAbbreviation).toString();
		}
	}

	public static class Property {

		private String ids;
		private String names;
		private Long propertyManagerId;
		private String propertyManagerName;
		private String propertyManagerSystemName;
		private String channelId;

		public String getIds() {
			return ids;
		}

		public void setIds(String ids) {
			this.ids = ids;
		}

		public String getNames() {
			return names;
		}

		public void setNames(String names) {
			this.names = names;
		}

		public Long getPropertyManagerId() {
			return propertyManagerId;
		}

		public void setPropertyManagerId(Long propertyManagerId) {
			this.propertyManagerId = propertyManagerId;
		}

		public String getPropertyManagerName() {
			return propertyManagerName;
		}

		public void setPropertyManagerName(String propertyManagerName) {
			this.propertyManagerName = propertyManagerName;
		}

		public String getPropertyManagerSystemName() {
			return propertyManagerSystemName;
		}

		public void setPropertyManagerSystemName(String propertyManagerSystemName) {
			this.propertyManagerSystemName = propertyManagerSystemName;
		}

		public String getChannelId() {
			return channelId;
		}

		public void setChannelId(String channelId) {
			this.channelId = channelId;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this).append("ids", ids).append("names", names).append("propertyManagerId", propertyManagerId)
					.append("propertyManagerName", propertyManagerName).append("propertyManagerSystemName", propertyManagerSystemName)
					.append("channelId", channelId).toString();
		}
	}

	public Long getGroupedReservationId() {
		return groupedReservationId;
	}

	public void setGroupedReservationId(Long groupReservationId) {
		this.groupedReservationId = groupReservationId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("entryDateTime", entryDateTime).append("channelReservationId", channelReservationId)
				.append("channelProductId", channelProductId).append("transactionId", transactionId).append("status", status)
				.append("assignedDateTime", assignedDateTime).append("resolvedDateTime", resolvedDateTime).append("paymentGatewayId", paymentGatewayId)
				.append("reason", reason).append("lastDayToCancel", lastDayToCancel).append("paymentType", paymentType).append("comment", comment)
				.append("actions", actions).append("guest", guest).append("assignee", assignee).append("groupedReservationId", groupedReservationId)
				.append("reservation", reservation).append("property", property).append("bookingComNotified", bookingComNotified).toString();
	}
}
