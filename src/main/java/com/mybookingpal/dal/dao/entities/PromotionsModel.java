package com.mybookingpal.dal.dao.entities;


import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
@Component
@Scope(value = "prototype")
public class PromotionsModel {
	public enum TypeOfCharge {DECREASE_PERCENT, DECREASE_AMOUNT}

	public enum State {CREATED, FINAL}

	private Long id;
	private Long productId;
	private LocalDate dateFrom;
	private LocalDate dateTo;
	private String name;
	private BigDecimal amount;
	private TypeOfCharge typeOfCharge;
	private LocalDate version;
	private State state;

	public PromotionsModel() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public LocalDate getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(LocalDate dateFrom) {
		this.dateFrom = dateFrom;
	}

	public LocalDate getDateTo() {
		return dateTo;
	}

	public void setDateTo(LocalDate dateTo) {
		this.dateTo = dateTo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public LocalDate getVersion() {
		return version;
	}

	public void setVersion(LocalDate version) {
		this.version = version;
	}

	public TypeOfCharge getTypeOfCharge() {
		return typeOfCharge;
	}

	public void setTypeOfCharge(TypeOfCharge typeOfCharge) {
		this.typeOfCharge = typeOfCharge;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
}
