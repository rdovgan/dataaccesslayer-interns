package com.mybookingpal.dal.elastic;

public class ProductStatusInfo {

	String productId;
	String channelProductId;
	String status;
	String statusCode;
	String endTime;
	String startTime;
	String listingId;
	String request;
	String response;
	String errorMessage;
	String errorResponse;
	String chnalleProductsOnbStatusId;
	
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getChannelProductId() {
		return channelProductId;
	}

	public void setChannelProductId(String channelProductId) {
		this.channelProductId = channelProductId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String errorcode) {
		this.statusCode = errorcode;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getListingId() {
		return listingId;
	}

	public void setListingId(String listingId) {
		this.listingId = listingId;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorResponse() {
		return errorResponse;
	}

	public void setErrorResponse(String errorResponse) {
		this.errorResponse = errorResponse;
	}

	public String getChnalleProductsOnbStatusId() {
		return chnalleProductsOnbStatusId;
	}

	public void setChnalleProductsOnbStatusId(String chnalleProductsOnbStatusId) {
		this.chnalleProductsOnbStatusId = chnalleProductsOnbStatusId;
	}
}
