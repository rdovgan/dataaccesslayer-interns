package com.mybookingpal.dal.elastic;

import lombok.Data;
import org.springframework.context.annotation.Scope;

import java.util.Date;

@Data
@Scope(value = "prototype")
public class ChannelProductOnbStatus {

    public enum Function {
        IMAGES("Images"),STATIC("Static"),DYNAMIC("Dynamic"),OPEN("Open"),CLOSE("Close"),BUILD("Build"), CONNECT ("Connect"), CANCELLATION_POLICY("Cancellation policy"), READY_TO_REVIEW ("Ready to review"), SYNC_TYPE ("Sync type"), CHANNEL_VALIDATION ("Channel validation");

        private String value;

        Function(String value){
            this.value = value;
        }

        public String getValue(){
            return value;
        }

    }

    public enum Status{
        FAILED ("Failed"), SUCCESS ("Success"), IN_PROGRESS ("In Progress");

        private String value;

        Status(String value){
            this.value = value;
        }

        public String getValue(){
            return value;
        }

    }

    private String elasticId;

    private String jobPropertyReportId;

    private int productId;

    private int supplierId;

    private int  channelId;

    private String function;

    private String status;

    private String errorMessage;

    private String warningMessage;

    private String serverHostName;

    private Date createdDate;

    private Date version;

}
