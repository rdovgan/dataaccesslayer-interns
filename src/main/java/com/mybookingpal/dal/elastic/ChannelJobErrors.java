package com.mybookingpal.dal.elastic;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@Scope(value = "prototype")
public class ChannelJobErrors {

    private String elasticId;

    private String supplierId;

    private String propertyId;

    private String listingId;

    private String channelId;

    private String jobName;

    private String serverHostName;

    private String response;

    private String errorMsg;

    private Date created;

    private Date version;

    private int count;

    private boolean isProcessed;

    private String errorStackTrace;

    private boolean isOnboarding;

}
