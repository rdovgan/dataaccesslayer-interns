package com.mybookingpal.dal.elastic;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class JobRepoPmStatus extends JobRepoStatus {

}
