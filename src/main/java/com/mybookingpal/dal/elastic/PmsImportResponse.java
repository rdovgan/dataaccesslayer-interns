package com.mybookingpal.dal.elastic;

import lombok.Data;
import org.springframework.context.annotation.Scope;

import java.util.Date;

@Data
@Scope(value = "prototype")
public class PmsImportResponse {

    public enum Function{
        READ_PRODUCTS ("readProduct"), READ_PRICES("readPrices"), READ_SCHEDULE ("readSchedule"), READ_IMAGES ("readImages"), READ_ADDITIONAL_COST ("readAdditionalCost"), READ_PRODUCT_STATUS ("readProductStatus");

        private String value;

        Function(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    private String elasticId;

    private String parentElasticId;

    private String pmsName;

    private Integer supplierId;

    private Integer productId;

    private String response;

    private String function;

    private String serverHostName;

    private Date createdDate;

}
