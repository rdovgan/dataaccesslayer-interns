package com.mybookingpal.dal.elastic;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope(value = "prototype")
public class KafkaMessageTracker {

	String id;
	Integer channelId;
	String supplierId;
	String productId;
	String topic;
	String status;
	String json;
	String producerHostName;
	String consumerHostName;
	Date startTime;
	Date endTime;
	String fileName;
	String supplierIdAndName;

	public String getSupplierIdAndName() {
		return supplierIdAndName;
	}
	public void setSupplierIdAndName(String supplierIdAndName) {
		this.supplierIdAndName = supplierIdAndName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProducerHostName() {
		return producerHostName;
	}
	public void setProducerHostName(String producerHostName) {
		this.producerHostName = producerHostName;
	}
	public String getConsumerHostName() {
		return consumerHostName;
	}
	public void setConsumerHostName(String consumerHostName) {
		this.consumerHostName = consumerHostName;
	}
}
