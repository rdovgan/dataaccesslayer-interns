package com.mybookingpal.dal.elastic;

import java.util.ArrayList;
import java.util.List;

public class ProductStatusInfos {

	public List<ProductStatusInfo> productList = new ArrayList<>();
	
	public List<ProductStatusInfo> getProductStatusInfos() {
		return productList;
	}

	public void setProductStatusInfos(List<ProductStatusInfo> productStatusInfos) {
		this.productList = productStatusInfos;
	}
	
}
