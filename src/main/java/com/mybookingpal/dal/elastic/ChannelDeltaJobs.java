package com.mybookingpal.dal.elastic;

import lombok.Data;
import org.springframework.context.annotation.Scope;

import java.util.Date;
import java.util.List;

@Data
@Scope(value = "channelDeltaJobs")
public class ChannelDeltaJobs {

    private String elasticId;
    private List<String> supplierIds;
    private List<String> propertyIds;
    private String channelId;
    private String jobName;
    private Integer jobNumber;
    private String serverHostName;
    private String consumerHostName;
    private Date startDate;
    private Date endDate;

}
