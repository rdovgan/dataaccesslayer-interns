package com.mybookingpal.dal.elastic;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class JobRepoPropertyStatus extends JobRepoStatus {

	public static final String JOB_STATUS_IN_PROGRESS = "In Progress";
	public static final String JOB_STATUS_STARTED = "Started";
	public static final String JOB_STATUS_QUEUED = "Queued";
	public static final String JOB_STATUS_FINNISHED = "Finished";
	public static final String JOB_STATUS_COMPLETED = "Completed";
	public static final String JOB_STATUS_FAILED = "Failed";
	public static final String JOB_STATUS_SUCCESS = "Success";
	
	String productId;
	String parentId;
	
	String request;
	String response;
	String elasticId;
	String pmId;
	String pmName;
	String channelName;
	String category = "";
	
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getElasticId() {
		return elasticId;
	}

	public void setElasticId(String elasticId) {
		this.elasticId = elasticId;
	}

	public String getPmId() {
		return pmId;
	}

	public void setPmId(String pmId) {
		this.pmId = pmId;
	}

	public String getPmName() {
		return pmName;
	}

	public void setPmName(String pmName) {
		this.pmName = pmName;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
