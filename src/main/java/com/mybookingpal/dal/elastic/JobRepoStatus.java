package com.mybookingpal.dal.elastic;

import java.util.Date;
import java.util.List;

public class JobRepoStatus {

	String id;
	//@JsonProperty("channel_id")
	Integer channelId;
	//@JsonProperty("supplier_id")
	String supplierId;
	//@JsonProperty("job_name")
	String jobName;
	//@JsonProperty("job_step")
	String jobStep;
	String status;
	// @JsonIgnoreProperties
	Date created;
	//@JsonIgnoreProperties
	Date version;
	String code;
	String errorDescription;
	//@JsonProperty("host_name")
	String hostName;
	Date startTime;
	Date endTime;
	String jobType;
	String partyId;
	String source;
	String environment;
	Date entryTimeDate;
	String emailAddress;
	String statusEmailAddresses;
	String jobRunnedBy;
	List<String> propertyIds;
	ProductStatusInfos productsStatusInfos;
	String progress;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobStep() {
		return jobStep;
	}

	public void setJobStep(String jobStep) {
		this.jobStep = jobStep;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public Date getEntryTimeDate() {
		return entryTimeDate;
	}

	public void setEntryTimeDate(Date entryTimeDate) {
		this.entryTimeDate = entryTimeDate;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getStatusEmailAddresses() {
		return statusEmailAddresses;
	}

	public void setStatusEmailAddresses(String statusEmailAddresses) {
		this.statusEmailAddresses = statusEmailAddresses;
	}

	public List<String> getPropertyIds() {
		return propertyIds;
	}

	public void setPropertyIds(List<String> propertyIds) {
		this.propertyIds = propertyIds;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public ProductStatusInfos getProductsStatusInfos() {
		return productsStatusInfos;
	}

	public void setProductsStatusInfos(ProductStatusInfos productsStatusInfos) {
		this.productsStatusInfos = productsStatusInfos;
	}

	public String getJobRunnedBy() {
		return jobRunnedBy;
	}

	public void setJobRunnedBy(String jobRunnedBy) {
		this.jobRunnedBy = jobRunnedBy;
	}

	public String getProgress() {
		return progress;
	}

	public void setProgress(String progress) {
		this.progress = progress;
	}
}
