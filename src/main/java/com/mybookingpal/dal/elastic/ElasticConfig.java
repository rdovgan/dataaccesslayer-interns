package com.mybookingpal.dal.elastic;

import org.springframework.stereotype.Service;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;

@Service
public class ElasticConfig {

	public JestClient getJestElasticClient() {
		JestClientFactory factory = new JestClientFactory();
		factory.setHttpClientConfig(new HttpClientConfig.Builder("http://search-mbp-channel-reports.mybookingpal.com").multiThreaded(true)
				//Per default this implementation will create no more than 2 concurrent connections per given route
				.defaultMaxTotalConnectionPerRoute(1)
				// and no more 20 connections in total
				.maxTotalConnection(20).build());
		JestClient client = factory.getObject();
		return client;
	}

}
