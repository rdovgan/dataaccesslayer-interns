package com.mybookingpal.dal.json.calendar;

public class CalendarWidgetItem {
	private String date;
	private String state;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
