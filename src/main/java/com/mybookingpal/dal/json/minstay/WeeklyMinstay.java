package com.mybookingpal.dal.json.minstay;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.shared.MinStay;

@Component("WeeklyMinstay")
@Scope(value = "prototype")
public class WeeklyMinstay extends MinStay {
	private Date start;
	private Date end;
	private Integer week;
	
	public WeeklyMinstay(){}
	
	public WeeklyMinstay(MinStay minStay, Date start, Date end, Integer week) {
		this.start = start;
		this.end = end;
		this.week = week;
		this.setId(minStay.getId());
		this.setSupplierid(minStay.getSupplierid());
		this.setProductid(minStay.getProductid());
		this.setFromdate(minStay.getFromdate());
		this.setTodate(minStay.getTodate());
		this.setValue(minStay.getValue());
		this.setVersion(minStay.getVersion());
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Integer getWeek() {
		return week;
	}

	public void setWeek(Integer week) {
		this.week = week;
	}
}
