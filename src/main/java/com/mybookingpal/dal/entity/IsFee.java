package com.mybookingpal.dal.entity;

import java.time.LocalDate;

public interface IsFee {

	//TODO change to wrapper
	int getId();

	String getAltId();

	Integer getFeeType();

	Integer getEntityType();

	String getName();

	Integer getUnit();

	Integer getTaxType();

	LocalDate getFromDate();

	LocalDate getToDate();

	String getCurrency();
	void setCurrency(String currency);

	Double getValue();
	void setValue(Double value);

	Integer getValueType();

	String getOptionValue();

}
