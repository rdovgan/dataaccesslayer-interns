package com.mybookingpal.dal.entity;

import java.time.LocalDate;
import java.util.Date;

public interface IsPrice {

	String getId();

	String getEntityType();

	Integer getEntityId();

	String getName();

	String getType();

	Double getQuantity();

	String getUnit();

	String getCurrency();

	LocalDate getFromDate();

	LocalDate getToDate();

	Double getValue();

}
