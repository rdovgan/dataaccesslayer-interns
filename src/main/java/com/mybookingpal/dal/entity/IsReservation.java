package com.mybookingpal.dal.entity;

import java.time.LocalDate;

public interface IsReservation {

	Integer getId();

	Integer getProductId();

	Integer getOrganizationId();

	Integer getAgentId();

	LocalDate getFromDate();

	LocalDate getToDate();

	Integer getAdult();

	String getCurrency();

}
