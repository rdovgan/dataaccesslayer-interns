package com.mybookingpal.dal.entity;

import com.mybookingpal.dal.enums.ProductConstants;

public interface IsProduct {

	String getId();

	String getParentId();

	String getName();

	Integer getSupplierId();

	String getState();

	String getMultiUnit();

	ProductConstants.ProductGroup getProductGroup();

	String getLocationId();

}
