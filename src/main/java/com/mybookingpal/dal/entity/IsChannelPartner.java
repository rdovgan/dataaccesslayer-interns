package com.mybookingpal.dal.entity;

public interface IsChannelPartner {

	Integer getId();

	Integer getPartyId();

	String getAbbreviation();

}
