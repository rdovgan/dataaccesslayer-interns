package com.mybookingpal.dal.mybatis;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

//Handler is not proper for handler scanner. Need to add default constructor
public class StringEnumTypeHandler<E extends Enum<E>> extends EnumTypeHandler<E> {

	Class<E> type;

	public StringEnumTypeHandler(Class<E> type) {
		super(type);
		this.type = type;
	}

	public void setNonNullParameter(PreparedStatement ps, int i, E parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.name());
	}

	public E getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String value = rs.getString(columnName);
		if (rs.wasNull()) {
			return null;
		}
		E enumValue = null;
		for (E enm : type.getEnumConstants()) {
			if (value.equalsIgnoreCase(enm.name())) {
				enumValue = enm;
				break;
			}
		}
		return enumValue;

	}

	public E getNullableResult(CallableStatement cs, String columnName) throws SQLException {
		String value = cs.getString(columnName);
		if (cs.wasNull()) {
			return null;
		}
		E enumValue = null;
		for (E enm : type.getEnumConstants()) {
			if (value.equalsIgnoreCase(enm.name())) {
				enumValue = enm;
				break;
			}
		}
		return enumValue;
	}

}