package com.mybookingpal.dal.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.mybookingpal.dal.server.util.enums.BookingPalEnums.ResidencyCategoryEnum;

public class ResidencyCategoryEnumHandler extends EnumTypeHandler<ResidencyCategoryEnum> {

	public ResidencyCategoryEnumHandler() {
		super(ResidencyCategoryEnum.class);
	}

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, ResidencyCategoryEnum parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getId());
	}

	@Override
	public ResidencyCategoryEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		Integer value = rs.getInt(columnName);
		return value == null ? null : ResidencyCategoryEnum.getResidencycategoryById(value);
	}

	@Override
	public ResidencyCategoryEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		Integer value = cs.getInt(columnIndex);
		return value == null ? null : ResidencyCategoryEnum.getResidencycategoryById(value);
	}
}
