package com.mybookingpal.dal.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.mybookingpal.dal.shared.LogImportLastRun.LogImportStatusEnum;

/**
 * Handler used to map {@link LogImportStatusEnumHandler} class to String value ready for store to database and vice versa (read to enum from database String).
 * This mapping is configured inside <code>Configurator.xml</code> file into <code>typeHandlers</code> section.
 *
 */
public class LogImportStatusEnumHandler extends EnumTypeHandler<LogImportStatusEnum> {

	@SuppressWarnings("unchecked")
	public LogImportStatusEnumHandler() {
		super(LogImportStatusEnum.class);
	}

	public void setNonNullParameter(PreparedStatement ps, int i, LogImportStatusEnum parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.getValue());
	}

	public LogImportStatusEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String value = rs.getString(columnName);
		return value == null ? null : LogImportStatusEnum.getByString(value);
	}

	public LogImportStatusEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String value = cs.getString(columnIndex);
		return value == null ? null : LogImportStatusEnum.getByString(value);
	}
}
