package com.mybookingpal.dal.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.mybookingpal.dal.shared.FeeTaxRelation.StateFeeTaxEnum;


/**
 * @author Danijel Blagojevic
 *
 */
/**
 * Handler used to map {@link StateFeeTaxEnum} class to Integer value ready for store to database and vice versa (read to enum from database integer).
 * This mapping is configured inside <code>Configurator.xml</code> file into <code>typeHandlers</code> section.
 *
 */
public class StateFeeTaxEnumHandler extends EnumTypeHandler<StateFeeTaxEnum>{

	public StateFeeTaxEnumHandler() {
		super(StateFeeTaxEnum.class);
	}
	
	public void setNonNullParameter(PreparedStatement ps, int i, StateFeeTaxEnum parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getValue());
	}
	
	public StateFeeTaxEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		Integer value = rs.getInt(columnName);
		return value == null ? null : StateFeeTaxEnum.getByInt(value);
	}
	
	public StateFeeTaxEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		Integer value = cs.getInt(columnIndex);
		return value == null ? null : StateFeeTaxEnum.getByInt(value);
	}
}
