package com.mybookingpal.dal.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.StringTypeHandler;

public class StringEncodingTypeHandler extends StringTypeHandler {

	public StringEncodingTypeHandler() {
	}

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType) throws SQLException {
		String[] toEncode = new String[] { (String) parameter };
		String p = StringEscapeUtils.escapeHtml(parameter);
		ps.setString(i, p);
	}

	@Override
	public String getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String resuString = rs.getString(columnName);
		if (resuString != null) {
			resuString = StringEscapeUtils.unescapeHtml(resuString);
		}
		return resuString;
	}

	@Override
	public String getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String resuString = cs.getString(columnIndex);
		if (resuString != null) {
			resuString = StringEscapeUtils.unescapeHtml(resuString);
		}
		return resuString;
	}

}
