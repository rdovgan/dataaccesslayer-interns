package com.mybookingpal.dal.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.mybookingpal.dal.shared.ChannelLogImportLastRun.JobNameEnum;

public class JobNameEnumHandler extends EnumTypeHandler<JobNameEnum> {

	public JobNameEnumHandler() {
		super(JobNameEnum.class);
	}

	public void setNonNullParameter(PreparedStatement ps, int i, JobNameEnum parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.getValue());
	}

	public JobNameEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String value = rs.getString(columnName);
		return value == null ? null : JobNameEnum.getByString(value);
	}

	public JobNameEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String value = cs.getString(columnIndex);
		return value == null ? null : JobNameEnum.getByString(value);
	}
}
