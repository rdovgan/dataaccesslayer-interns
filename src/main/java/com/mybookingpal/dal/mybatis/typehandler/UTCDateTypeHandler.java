package com.mybookingpal.dal.mybatis.typehandler;

import com.mybookingpal.dal.utils.CommonDateUtils;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class UTCDateTypeHandler implements TypeHandler {

	@Override
	public void setParameter(final PreparedStatement ps, final int i, Object timestamp, final JdbcType jdbcType) throws SQLException {
		if (timestamp == null) {
			ps.setNull(i, jdbcType.TYPE_CODE);
		} else {
			ps.setTimestamp(i, (Timestamp) timestamp, getUTCCalendar());
		}
	}

	@Override
	public Date getResult(final ResultSet rs, final String columnName) throws SQLException {
		String timestamp = rs.getString(columnName);
		return CommonDateUtils.parseWithDateTime(timestamp);
	}

	public Object getResult(final ResultSet rs, final int columnIndex) throws SQLException {
		return rs.getTimestamp(columnIndex, getUTCCalendar());
	}

	@Override
	public Object getResult(final CallableStatement cs, final int columnIndex) throws SQLException {
		return cs.getTimestamp(columnIndex, getUTCCalendar());
	}

	private static Calendar getUTCCalendar() {
		return Calendar.getInstance(TimeZone.getTimeZone("UTC"));
	}
}
