package com.mybookingpal.dal.mybatis.typehandler;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JsonTypeHandler extends BaseTypeHandler<JsonObject> {

	@Override
	public void setNonNullParameter(PreparedStatement preparedStatement, int i, JsonObject jsonObject, JdbcType jdbcType) throws SQLException {
		if (jsonObject == null) {
			return;
		}
		String parameterAsString = new Gson().toJson(jsonObject, JsonObject.class);
		preparedStatement.setString(i, parameterAsString);
	}

	@Override
	public JsonObject getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
		String sqlJson = resultSet.getString(columnName);
		if (null != sqlJson) {
			return new Gson().fromJson(sqlJson, JsonObject.class);
		}
		return null;
	}

	@Override
	public JsonObject getNullableResult(ResultSet resultSet, int columnIndex) throws SQLException {
		String sqlJson = resultSet.getString(columnIndex);
		if (null != sqlJson) {
			return new Gson().fromJson(sqlJson, JsonObject.class);
		}
		return null;
	}

	@Override
	public JsonObject getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
		String sqlJson = callableStatement.getString(columnIndex);
		if (null != sqlJson) {
			return new Gson().fromJson(sqlJson, JsonObject.class);
		}
		return null;
	}
}
