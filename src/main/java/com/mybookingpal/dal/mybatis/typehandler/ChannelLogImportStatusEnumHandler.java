package com.mybookingpal.dal.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.mybookingpal.dal.shared.ChannelLogImportLastRun.ChannelLogImportStatusEnum;

public class ChannelLogImportStatusEnumHandler extends EnumTypeHandler<ChannelLogImportStatusEnum> {

	public ChannelLogImportStatusEnumHandler() {
		super(ChannelLogImportStatusEnum.class);
	}

	public void setNonNullParameter(PreparedStatement ps, int i, ChannelLogImportStatusEnum parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.getValue());
	}

	public ChannelLogImportStatusEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String value = rs.getString(columnName);
		return value == null ? null : ChannelLogImportStatusEnum.getByString(value);
	}

	public ChannelLogImportStatusEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String value = cs.getString(columnIndex);
		return value == null ? null : ChannelLogImportStatusEnum.getByString(value);
	}
}

