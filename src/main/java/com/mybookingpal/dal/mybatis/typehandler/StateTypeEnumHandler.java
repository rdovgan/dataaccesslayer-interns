package com.mybookingpal.dal.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.mybookingpal.dal.shared.PropertyMinStay.StateTypeEnum;

/**
 * @author Danijel Blagojevic
 *
 */
/**
 * Handler used to map {@link StateTypeEnum} class to Integer value ready for store to database and vice versa (read to enum from database integer).
 * This mapping is configured inside <code>Configurator.xml</code> file into <code>typeHandlers</code> section.
 *
 */
public class StateTypeEnumHandler extends EnumTypeHandler<StateTypeEnum>{

	public StateTypeEnumHandler() {
		super(StateTypeEnum.class);
	}
	
	public void setNonNullParameter(PreparedStatement ps, int i, StateTypeEnum parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getValue());
	}
	
	public StateTypeEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		Integer value = rs.getInt(columnName);
		return value == null ? null : StateTypeEnum.getByInt(value);
	}
	
	public StateTypeEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		Integer value = cs.getInt(columnIndex);
		return value == null ? null : StateTypeEnum.getByInt(value);
	}
}
