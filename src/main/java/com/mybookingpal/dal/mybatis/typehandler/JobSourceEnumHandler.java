package com.mybookingpal.dal.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.mybookingpal.dal.shared.ChannelJobStatus.JobSourceEnum;


/**
 * @author Danijel Blagojevic
 *
 */
public class JobSourceEnumHandler extends EnumTypeHandler<JobSourceEnum>{

	public JobSourceEnumHandler() {
		super(JobSourceEnum.class);
	}

	public void setNonNullParameter(PreparedStatement ps, int i, JobSourceEnum parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.getValue());
	}

	public JobSourceEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String value = rs.getString(columnName);
		return value == null ? null : JobSourceEnum.getByStr(value);
	}

	public JobSourceEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String value = cs.getString(columnIndex);
		return value == null ? null : JobSourceEnum.getByStr(value);
	}
}
