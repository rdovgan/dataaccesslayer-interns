package com.mybookingpal.dal.mybatis.typehandler;

import com.mybookingpal.dal.mybatis.StringEnumTypeHandler;
import com.mybookingpal.dal.shared.ChannelYieldMap.ChannelYieldMapStateEnum;

/**
 * @author Danijel Blagojevic
 *
 */
public class ChannelYieldMapStateEnumHandler extends StringEnumTypeHandler<ChannelYieldMapStateEnum> {

	public ChannelYieldMapStateEnumHandler() {
		super(ChannelYieldMapStateEnum.class);
	}

	public ChannelYieldMapStateEnumHandler(Class<ChannelYieldMapStateEnum> type) {
		super(type);
	}

}
