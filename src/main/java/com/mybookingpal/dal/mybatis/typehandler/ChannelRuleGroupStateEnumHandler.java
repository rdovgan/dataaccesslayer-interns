package com.mybookingpal.dal.mybatis.typehandler;

import com.mybookingpal.dal.mybatis.StringEnumTypeHandler;
import com.mybookingpal.dal.shared.ChannelRuleGroup.ChannelRuleGroupStateEnum;

/**
 * @author Danijel Blagojevic
 *
 */
public class ChannelRuleGroupStateEnumHandler extends StringEnumTypeHandler<ChannelRuleGroupStateEnum> {

	public ChannelRuleGroupStateEnumHandler() {
		super(ChannelRuleGroupStateEnum.class);
	}

	public ChannelRuleGroupStateEnumHandler(Class<ChannelRuleGroupStateEnum> type) {
		super(type);
	}

}
