package com.mybookingpal.dal.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.mybookingpal.dal.shared.ProductText.TypeEnum;

public class TypeDescriptionEnumHandler extends EnumTypeHandler<TypeEnum>{

	public TypeDescriptionEnumHandler() {
		super(TypeEnum.class);
	}
	
	public void setNonNullParameter(PreparedStatement ps, int i, TypeEnum parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.name());
	}
	
	public TypeEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String value = rs.getString(columnName);
		return value == null ? null : TypeEnum.valueOf(value);
	}
	
	public TypeEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String value = cs.getString(columnIndex);
		return value == null ? null : TypeEnum.valueOf(value);
	}

}
