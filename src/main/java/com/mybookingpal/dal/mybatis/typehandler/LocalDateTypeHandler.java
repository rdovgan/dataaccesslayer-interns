package com.mybookingpal.dal.mybatis.typehandler;

import com.mybookingpal.dal.utils.CommonDateUtils;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;


public class LocalDateTypeHandler extends BaseTypeHandler<LocalDate> {

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, LocalDate parameter, JdbcType jdbcType)
			throws SQLException {
		ps.setString(i, CommonDateUtils.format(parameter));
	}

	@Override
	public LocalDate getNullableResult(ResultSet rs, String columnName) throws SQLException {
		Date date = rs.getDate(columnName);
		if (date == null) {
			return null;
		}
		return getLocalDate(date);
	}

	@Override
	public LocalDate getNullableResult(ResultSet resultSet, int i) throws SQLException {
		return LocalDate.parse(resultSet.getString(i));
	}

	@Override
	public LocalDate getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		Date date = cs.getDate(columnIndex);
		if (date == null) {
			return null;
		}
		return getLocalDate(date);
	}

	private static LocalDate getLocalDate(Date date) {
		if (date != null) {
			return date.toLocalDate();
		}
		return null;
	}
}
