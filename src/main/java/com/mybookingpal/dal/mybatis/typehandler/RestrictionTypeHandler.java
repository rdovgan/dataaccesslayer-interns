package com.mybookingpal.dal.mybatis.typehandler;

import com.mybookingpal.dal.enums.RestrictionConstants;
import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RestrictionTypeHandler extends EnumTypeHandler<RestrictionConstants.RestrictionType> {

	public RestrictionTypeHandler() {
		super(RestrictionConstants.RestrictionType.class);
	}

	public void setNonNullParameter(PreparedStatement ps, int i, RestrictionConstants.RestrictionType parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, parameter.getValue());
	}

	public RestrictionConstants.RestrictionType getNullableResult(ResultSet rs, String columnName) throws SQLException {
		Integer value = rs.getInt(columnName);
		return RestrictionConstants.RestrictionType.getByValue(value);
	}

	public RestrictionConstants.RestrictionType getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		Integer value = cs.getInt(columnIndex);
		return RestrictionConstants.RestrictionType.getByValue(value);
	}
}
