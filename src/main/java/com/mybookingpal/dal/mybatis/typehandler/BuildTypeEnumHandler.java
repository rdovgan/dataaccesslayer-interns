package com.mybookingpal.dal.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.mybookingpal.dal.shared.ChannelProductMap.BuildTypeEnum;

/**
 * @author Danijel
 *
 */
public class BuildTypeEnumHandler extends EnumTypeHandler<BuildTypeEnum> {
	public BuildTypeEnumHandler() {
		super(BuildTypeEnum.class);
	}

	public void setNonNullParameter(PreparedStatement ps, int i, BuildTypeEnum parameter, JdbcType jdbcType)
			throws SQLException {
		ps.setString(i, parameter.getValue());
	}

	public BuildTypeEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String value = rs.getString(columnName);
		return value == null ? null : BuildTypeEnum.getByStr(value);
	}

	public BuildTypeEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String value = cs.getString(columnIndex);
		return value == null ? null : BuildTypeEnum.getByStr(value);
	}
}
