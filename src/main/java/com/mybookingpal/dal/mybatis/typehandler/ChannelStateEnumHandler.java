package com.mybookingpal.dal.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.mybookingpal.dal.shared.ChannelProductMap.ChannelStateEnum;

/**
 * @author Danijel Blagojevic
 *
 */
public class ChannelStateEnumHandler extends EnumTypeHandler<ChannelStateEnum> {

	public ChannelStateEnumHandler() {
		super(ChannelStateEnum.class);
	}

	public void setNonNullParameter(PreparedStatement ps, int i, ChannelStateEnum parameter, JdbcType jdbcType) throws SQLException {
		ps.setInt(i, ((ChannelStateEnum) parameter).getIntegerValue());
	}

	public ChannelStateEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
		Integer value = rs.getInt(columnName);
		return value == null ? null : ChannelStateEnum.getByInt(value);
	}

	public ChannelStateEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		Integer value = cs.getInt(columnIndex);
		return value == null ? null : ChannelStateEnum.getByInt(value);
	}

}
