package com.mybookingpal.dal.shared.action;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.server.ftp.DatesGenerator;

@Component
@Scope(value = "prototype")
public class DatesIdAction {

	private static final int DAY_RANGE = 365;

	private List<Integer> productIds;
	private Date fromDate;
	private Date toDate;
	private Date version;

	public DatesIdAction(Integer productId, Date fromDate, Date toDate) {
		this.productIds = Collections.singletonList(productId);
		this.fromDate = fromDate;
		this.toDate = toDate;
	}

	public DatesIdAction(List<Integer> productIds, Date fromDate, Date toDate) {
		this.productIds = productIds;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}

	public DatesIdAction() {
	}

	public DatesIdAction(List<Integer> productIds) {
		this.productIds = productIds;
		DatesGenerator date = new DatesGenerator(DAY_RANGE);
		this.fromDate = date.getFromDate();
		this.toDate = date.getToDate();
	}

	public List<Integer> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<Integer> productIds) {
		this.productIds = productIds;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

}
