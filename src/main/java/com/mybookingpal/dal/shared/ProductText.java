package com.mybookingpal.dal.shared;

import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ProductText {
	
	private Integer id;
	private String productId;
	private Integer partyId;
	private String language;
	private TypeEnum type;
	private String value;
	private StateEnum state;
	private Date createdDate;
	private Date version;
	
	public ProductText(String language, String value, TypeEnum type) {
		this.language =  language.toUpperCase();
		this.type = type;
		this.value = value;
		this.createdDate = new Date();
	}
	
	public ProductText() {
	}
	
	public enum StateEnum {
		CREATED(2),
		FINAL(3);

		private Integer value;

		StateEnum(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return this.value;
		}

		public static StateEnum getByInt(int value) {
			for (StateEnum v : values()) {
				if (v.value == value) {
					return v;
				}
			}
			return CREATED;
		}

		public static String getNameByInt(Integer value) {
			return getByInt(value).name();
		}
	}
	
	public enum TypeEnum {
		Description,
		ShortDescription,
		HouseRules,
		Name,
		Access,
		Interaction,
		NeighborhoodOverview,
		Transit,
		Notes,
		FinePrint,
		AboutTheProperty;
	}
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public Integer getPartyId() {
		return partyId;
	}
	public void setPartyId(Integer partyId) {
		this.partyId = partyId;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public StateEnum getState() {
		return state;
	}
	public void setState(StateEnum state) {
		this.state = state;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getVersion() {
		return version;
	}
	public void setVersion(Date version) {
		this.version = version;
	}
	public TypeEnum getType() {
		return type;
	}
	public void setType(TypeEnum type) {
		this.type = type;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((language == null) ? 0 : language.hashCode());
		result = prime * result + ((partyId == null) ? 0 : partyId.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductText other = (ProductText) obj;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (partyId == null) {
			if (other.partyId != null)
				return false;
		} else if (!partyId.equals(other.partyId))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (state != other.state)
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		
		return true;
	}
	
	@Override
	public String toString() {
		return "ProductText [productId=" + productId + ", partyId=" + partyId + ", language=" + language + ", type=" + type + ", value=" + value + ", state="
				+ state + ", createdDate=" + createdDate + ", version=" + version + "]";
	}
	
}
