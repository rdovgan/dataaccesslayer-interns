package com.mybookingpal.dal.shared.messaging;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Scope(value = "prototype")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MailMessage {

	public enum State {
		NEW(1), 
		VIEWED(2),
		REPLIED(3),
		ASSIGNED(4),
		FORWARDED(5);

		private Integer value;

		State(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return this.value;
		}

		public static State getByInt(Integer value) {
			for (State v : values()) {
				if (v.value == value) {
					return v;
				}
			}
			return null;
		}

		public static String getNameByInt(Integer value) {
			return getByInt(value).name();
		}
	}

	private Integer id;
	private String message;
	private Integer whoResponded;
	private Integer state;
	private Integer expirationTime;
	private Integer threadId;
	private Date createdDate;
	private Date version;
	private Long channelMessageId;
	private Date channelCreatedDate;


}
