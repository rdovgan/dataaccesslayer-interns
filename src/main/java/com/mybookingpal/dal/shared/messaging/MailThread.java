package com.mybookingpal.dal.shared.messaging;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Scope(value = "prototype")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class MailThread {

	private Integer id;
	private String threadId;
	private String title;
	private Integer productId;
	private Integer reservationId;
	private Integer channelId;
	private Integer supportAgentId;
	private String guestEmail;
	private String channelProductId;
	private String guestId;
	private Date createdDate;
	private Date version;
	private Date fromDate;
	private Date toDate;
	private String guestName;
	private Integer guests;

}
