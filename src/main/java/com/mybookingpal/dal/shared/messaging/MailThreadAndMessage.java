package com.mybookingpal.dal.shared.messaging;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Scope(value = "prototype")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MailThreadAndMessage {

    private Integer id;
    private String threadId;
    private Integer productId;
    private Integer reservationId;
    private Integer channelId;
    private Integer supportAgentId;
    private String guestEmail;
    private Date createdDate;
    private Date version;
    private String channelProductId;
    private String guestId;
    private Date fromDate;
    private Date toDate;
    private String guestName;
    private Integer guests;

    private String message;
    private Integer whoResponded;
    private Integer state;
    private Integer expirationTime;
    private Integer messageThreadId;
    private Date channelCreatedDate;
    private Date messageCreatedDate;
    private Date messageVersion;
    private Long channelMessageId;



}
