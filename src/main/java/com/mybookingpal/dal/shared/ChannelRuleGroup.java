package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Danijel Blagojevic
 *
 */
@Component
@Scope(value = "prototype")
public class ChannelRuleGroup {

	public enum ChannelRuleGroupStateEnum {
		CREATED("Created"), FINAL("Final");

		private String value;

		ChannelRuleGroupStateEnum(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static ChannelRuleGroupStateEnum getByStr(String value) {
			for (ChannelRuleGroupStateEnum v : values()) {
				if (v.value.equals(value)) {
					return v;
				}
			}
			return null;
		}

		public static String getNameByStr(String value) {
			return getByStr(value).name();
		}
	}

	private Integer id;
	private Date fromDate;
	private Date toDate;
	private Integer productId;
	private String ruleId;
	private Integer channelId;
	private ChannelRuleGroupStateEnum state;
	private Date createdDate;
	private Date version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public ChannelRuleGroupStateEnum getState() {
		return state;
	}

	public void setState(ChannelRuleGroupStateEnum state) {
		this.state = state;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

}
