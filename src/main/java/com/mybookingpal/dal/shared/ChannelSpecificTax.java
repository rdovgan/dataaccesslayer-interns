package com.mybookingpal.dal.shared;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Danijel Blagojevic
 *
 */
@Component
@Scope(value = "prototype")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChannelSpecificTax {

    private Integer id;
    private Integer productId;
    private Integer groupId;
    private String name;
    private Double amount;
    /**
     * 0 - "flat"
     * 1 - "percentage"
     */
    private Integer amountType;
    private Date createdDate;
    private Date version;

}
