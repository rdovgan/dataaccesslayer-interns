package com.mybookingpal.dal.shared;

import com.mybookingpal.dal.utils.BigDecimalExt;
import com.mybookingpal.dal.utils.CommonDateUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mybookingpal.dal.server.util.enums.BookingPalEnums;
import com.mybookingpal.dal.utils.BigDecimalExt;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Yield")
@Scope("prototype")
public class Yield extends ModelTable implements DateRange {

	//Entity types
	public static final String PRODUCT = "Product";

	// States
	public static final String INITIAL = "Initial";
	public static final String FINAL = "Final";
	public static final String CREATED = "Created";
	public static final String SUSPENDED = "Suspended";
	public static final String FAILED = "Failed";

	public static final String INCREASE_PERCENT = "Increase Percent";
	public static final String DECREASE_PERCENT = "Decrease Percent";
	public static final String INCREASE_AMOUNT = "Increase Amount";
	public static final String DECREASE_AMOUNT = "Decrease Amount";

	public static final String DATE_RANGE = "Date Range";
	public static final String DAY_OF_WEEK = "Day of Week";
	public static final String GAP_FILLER = "Maximum Gap Filler";
	public static final String EARLY_BIRD = "Early Booking Lead Time";
	public static final String LAST_MINUTE = "Last Minute Lead Time";
	public static final String LENGTH_OF_STAY = "Length of Stay";
	public static final String OCCUPANCY_ABOVE = "Occupancy Above";
	public static final String OCCUPANCY_BELOW = "Occupancy Below";
	public static final String WEEKEND = "Weekend";
	public static final String WEEKLY = "Weekly";
	public static final String STANDARD = "Standard";
	public static final String CHANNEL_MARKUP = "Channel Markup";

	public static final String EARLY_BOOKING_DEAL = "Early Booking Deal";
	public static final String BASIC_PROMO = "Basic Promotion";
	public static final String SAME_DAY_DEAL = "Same Day Deal";
	public static final String FREE_NIGHT_PROMOTION = "Free Night Promotion";
	public static final String LAST_MINUTE_LEAD_PROMOTION = "Last Minute Lead Time";
	public static final String MOBILE_RATE_PROMOTION = "Mobile Rate";
	public static final String BUSINESS_BOOKER_PROMOTION = "Business Booker";
	public static final String GEO_RATE_PROMOTION = "Geo Rate";

	private static final String SUN = "0";
	private static final String MON = "1";
	private static final String TUE = "2";
	private static final String WED = "3";
	private static final String THU = "4";
	private static final String FRI = "5";
	private static final String SAT = "6";
	public static final String[] DAYS = { SUN, MON, TUE, WED, THU, FRI, SAT };

	public static final int DAYS_OF_WEEKEND_SAT_SUN = 0;
	public static final int DAYS_OF_WEEKEND_FRI_SAT = 1;
	public static final int DAYS_OF_WEEKEND_FRI_SAT_SUN = 2;
	public static final int DAYS_OF_WEEKEND_THU_FRI_SAT = 3;
	public static final int DAYS_OF_WEEKEND_THU_FRI_SAT_SUN = 4;

	private static final Map<Integer, List<String>> WEEKEND_DAYS_MAP;

	static {
		WEEKEND_DAYS_MAP = new HashMap<>();
		WEEKEND_DAYS_MAP.put(DAYS_OF_WEEKEND_SAT_SUN, new ArrayList<String>() {
			{
				add(SAT);
				add(SUN);
			}
		});
		WEEKEND_DAYS_MAP.put(DAYS_OF_WEEKEND_FRI_SAT, new ArrayList<String>() {
			{
				add(FRI);
				add(SAT);
			}
		});
	}

	private String entitytype;
	private String entityid;
	private String modifier;
	private Integer param;
	private BigDecimalExt amount;
	private LocalDate fromDate;
	private LocalDate toDate;
	private Date createDate;
	private Date version;
	private BigDecimalExt totalPrice;
	private String yieldName;
	private Integer ratePlanID;
	private Integer minStay;
	private Date bookingFromDate;
	private Date bookingToDate;
	private Integer optionValue;
	private Integer channelID;
	private boolean isInternal;
	private Integer bookEndTime;
	private String excludeDates;
	private Integer targetGroup;
	private String activeDays;
	private String additionalDates;
	private Byte baseRate;
	private Integer channelId;

	public Yield() {
	}

	private Integer bookStartTime;

	public static Map<Integer, List<String>> getWeekendDaysMap() {
		return WEEKEND_DAYS_MAP;
	}

	public Byte getBaseRate() {
		return baseRate;
	}

	public void setBaseRate(Byte baseRate) {
		this.baseRate = baseRate;
	}

	public Integer getBookStartTime() {
		return bookStartTime;
	}

	public void setBookStartTime(Integer bookStartTime) {
		this.bookStartTime = bookStartTime;
	}

	public Integer getBookEndTime() {
		return bookEndTime;
	}

	public void setBookEndTime(Integer bookEndTime) {
		this.bookEndTime = bookEndTime;
	}

	public String getExcludeDates() {
		return excludeDates;
	}

	public void setExcludeDates(String excludeDates) {
		this.excludeDates = excludeDates;
	}

	public Integer getTargetGroup() {
		return targetGroup;
	}

	public void setTargetGroup(Integer targetGroup) {
		this.targetGroup = targetGroup;
	}

	public String getActiveDays() {
		return activeDays;
	}

	public void setActiveDays(String activeDays) {
		this.activeDays = activeDays;
	}

	public String getAdditionalDates() {
		return additionalDates;
	}

	public void setAdditionalDates(String additionalDates) {
		this.additionalDates = additionalDates;
	}

	public Yield(String entitytype, String entityid) {
		this.entitytype = entitytype;
		this.entityid = entityid;
	}

	public Yield(String entitytype, String entityid, LocalDate fromDate, LocalDate todate) {
		this.entitytype = entitytype;
		this.entityid = entityid;
		this.fromDate = fromDate;
		this.toDate = todate;
	}

	public String getEntitytype() {
		return entitytype;
	}

	public void setEntitytype(String entitytype) {
		this.entitytype = entitytype;
	}

	public boolean noEntitytype() {
		return entitytype == null || entitytype.isEmpty();
	}

	public String getEntityid() {
		return entityid;
	}

	public void setEntityid(String entityid) {
		this.entityid = entityid;
	}

	public boolean noEntityid() {
		return entityid == null || entityid.isEmpty();
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public boolean noModifier() {
		return modifier == null || modifier.isEmpty();
	}

	public boolean hasModifier(String modifier) {
		return this.modifier.equalsIgnoreCase(modifier);
	}

	public Integer getParam() {
		return param;
	}

	public void setParam(Integer param) {
		this.param = param;
	}

	public boolean noParam() {
		return param == null || param <= 0;
	}

	public boolean hasParam() {
		return !noParam();
	}

	public boolean hasParam(Integer param) {
		return this.param == param;
	}

	@Deprecated
	public Double defineAmount() {
		return amount != null ? amount.doubleValue() : null;
	}

	@Deprecated
	public void setAmount(Double amount) {
		this.amount = amount != null ? new BigDecimalExt(amount) : null;
	}

	public boolean noAmount() {
		return amount == null || amount.doubleValue() == 0;
	}

	public BigDecimalExt getAmount() {
		return amount;
	}

	public void setAmount(BigDecimalExt amount) {
		this.amount = amount;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public BigDecimalExt getTotalPrice() {
		return totalPrice;
	}

	@Deprecated
	public Double defineTotalPrice() {
		return totalPrice != null ? totalPrice.doubleValue() : null;
	}

	@Deprecated
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice != null ? new BigDecimalExt(totalPrice) : null;
	}

	public void setTotalPrice(BigDecimalExt totalPrice) {
		this.totalPrice = totalPrice;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public boolean noFromdate() {
		return fromDate == null;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public boolean isInternal() {
		return isInternal;
	}

	public void setInternal(boolean isInternal) {
		this.isInternal = isInternal;
	}

	public Integer getRatePlanID() {
		return ratePlanID;
	}

	public void setRatePlanID(Integer ratePlanID) {
		this.ratePlanID = ratePlanID;
	}

	public Integer getMinStay() {
		return minStay;
	}

	public void setMinStay(Integer minStay) {
		this.minStay = minStay;
	}

	public Date getBookingFromDate() {
		return bookingFromDate;
	}

	public void setBookingFromDate(Date bookingFromDate) {
		this.bookingFromDate = bookingFromDate;
	}

	public Date getBookingToDate() {
		return bookingToDate;
	}

	public void setBookingToDate(Date bookingToDate) {
		this.bookingToDate = bookingToDate;
	}

	public Integer getOptionValue() {
		return optionValue;
	}

	public void setOptionValue(Integer optionValue) {
		this.optionValue = optionValue;
	}

	public Integer getOption() {
		return optionValue;
	}

	public Integer getChannelID() {
		return channelID;
	}

	public void setChannelID(Integer channelID) {
		this.channelID = channelID;
	}

	public String getYieldName() {
		return yieldName;
	}

	public void setYieldName(String yieldName) {
		this.yieldName = yieldName;
	}

	public boolean noTodate() {
		return toDate == null;
	}

	public boolean noDate() {
		return noFromdate() || noTodate() || getToDate().isBefore(getFromDate());
	}

	public boolean hasDate() {
		return !noDate();
	}

	public boolean isInDateRange(Date date) {
		return !CommonDateUtils.toLocalDate(date).isBefore(fromDate) && !CommonDateUtils.toLocalDate(date).isAfter(toDate);
	}
	public boolean isInDateRange(LocalDate date) {
		return !date.isBefore(fromDate) && !date.isAfter(toDate);
	}

	public boolean isWeekend(LocalDate date, Integer param) {
		List<String> daysOfWeekend = WEEKEND_DAYS_MAP.get(DAYS_OF_WEEKEND_SAT_SUN);
		if (param != null && WEEKEND_DAYS_MAP.get(param) != null) {
			daysOfWeekend = WEEKEND_DAYS_MAP.get(param);
		}
		for (String day : daysOfWeekend) {
			if (date.getDayOfWeek().getValue() == Integer.parseInt(day)) {
				return true;
			}
		}
		return false;
	}

	public boolean isWeekendDate(LocalDate dt, Integer param) {
		if (param != null && param == Yield.DAYS_OF_WEEKEND_SAT_SUN) {
			// sat and sun have values greater than 5
			return dt.getDayOfWeek().getValue() > 5;
		} else if (param != null && param == Yield.DAYS_OF_WEEKEND_FRI_SAT) {
			// Fri=5 & sat=6
			return dt.getDayOfWeek().getValue() == 5 || dt.getDayOfWeek().getValue() == 6;
		} else if (param != null && param == Yield.DAYS_OF_WEEKEND_FRI_SAT_SUN) {
			// Fri=5 & sat=6 & sun=7
			return dt.getDayOfWeek().getValue() > 4;
		} else if (param != null && param == Yield.DAYS_OF_WEEKEND_THU_FRI_SAT) {
			// Thu=4 & Fri=5 & sat=6
			return dt.getDayOfWeek().getValue() == 4 || dt.getDayOfWeek().getValue() == 5 || dt.getDayOfWeek().getValue() == 6;
		} else if (param != null && param == Yield.DAYS_OF_WEEKEND_THU_FRI_SAT_SUN) {
			// Thu=4 & Fri=5 & sat=6 & Sun=7
			return dt.getDayOfWeek().getValue() > 3;
		}

		return false;
	}

	public boolean isDayOfWeek(Date date) {
		return param == date.getDay();
	}

	public boolean isEarlyBird(Date date) {
		Date deadline = Time.addDuration(new Date(), param, Time.DAY);
		return date.after(deadline);
	}

	public boolean isGapFiller(boolean fillsGap, Integer stay) {
		return fillsGap && stay <= param;
	}

	public boolean isGapFiller() {
		return GAP_FILLER.equalsIgnoreCase(name);
	}

	public boolean isLastMinute(Date date) {
		Date deadline = Time.addDuration(new Date(), param, Time.DAY);
		return date.before(deadline);
	}

	public boolean isLengthOfStay(Integer stay) {
		return param <= stay;
	}

	public boolean isOccupancyAbove(Integer occupancy) {
		return param < occupancy;
	}

	public boolean isOccupancyBelow(Integer occupancy) {
		return param > occupancy;
	}

	public boolean isPercentage() {
		return hasModifier(DECREASE_PERCENT) || hasModifier(INCREASE_PERCENT);
	}

	public Integer getChannelId() {
		return channelID;
	}

	public void setChannelId(Integer channelId) {
		this.channelID = channelId;
	}

	public Double getValue(Double value) {
		if (this.modifier == null) {
			return 0D;
		}
		if (hasModifier(INCREASE_AMOUNT)) {
			return value + defineAmount();
		} else if (hasModifier(DECREASE_AMOUNT)) {
			return value - defineAmount();
		} else if (hasModifier(INCREASE_PERCENT)) {
			return value * (100.0 + defineAmount()) / 100.0;
		} else if (hasModifier(DECREASE_PERCENT)) {
			return value * (100.0 - defineAmount()) / 100.0;
		} else
			return value;
	}

	public BigDecimalExt getPromotionValue(BigDecimalExt value) {
		if (this.modifier == null) {
			return BigDecimalExt.ZERO;
		}
		if (hasModifier(INCREASE_AMOUNT)) {
			return getAmount();
		} else if (hasModifier(DECREASE_AMOUNT)) {
			return getAmount().negate();
		} else if (hasModifier(INCREASE_PERCENT)) {
			return value.multiply(getAmount()).divideByHundred();
		} else if (hasModifier(DECREASE_PERCENT)) {
			return value.negate().multiply(getAmount()).divideByHundred();
		} else {
			return value;
		}
	}

	public BigDecimalExt getValue(BigDecimalExt value) {
		if (this.modifier == null) {
			return BigDecimalExt.ZERO;
		}
		if (hasModifier(INCREASE_AMOUNT)) {
			return value.add(amount);
		} else if (hasModifier(DECREASE_AMOUNT)) {
			return value.subtract(amount);
		} else if (hasModifier(INCREASE_PERCENT)) {
			return BigDecimalExt.ONE_HUNDRED.add(amount).multiply(value).divideByHundred();
		} else if (hasModifier(DECREASE_PERCENT)) {
			return BigDecimalExt.ONE_HUNDRED.subtract(amount).multiply(value).divideByHundred();
		} else {
			return value;
		}
	}

	public boolean isSpecialPromotion() {
		return Yield.isSpecialPromotion(this.getName());
	}

	public static boolean isSpecialPromotion(String name) {
		return BookingPalEnums.YieldType.getSpecialPromotions().contains(BookingPalEnums.getValuedEnum(BookingPalEnums.YieldType.class, name));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Yield [entitytype=");
		builder.append(entitytype);
		builder.append(", entityid=");
		builder.append(entityid);
		builder.append(", modifier=");
		builder.append(modifier);
		builder.append(", param=");
		builder.append(param);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", fromDate=");
		builder.append(fromDate);
		builder.append(", toDate=");
		builder.append(toDate);
		builder.append(", organizationid=");
		builder.append(organizationid);
		builder.append(", status=");
		builder.append(status);
		builder.append(", state=");
		builder.append(state);
		builder.append(", values=");
		builder.append(values);
		builder.append(", attributes=");
		builder.append(attributemap);
		builder.append(", texts=");
		builder.append(texts);
		builder.append(", name=");
		builder.append(name);
		builder.append(", id=");
		builder.append(id);
		builder.append(", yieldName=");
		builder.append(yieldName);
		builder.append(", ratePlanID=");
		builder.append(ratePlanID);
		builder.append(", minStay=");
		builder.append(minStay);
		builder.append(", bookingFromDate=");
		builder.append(bookingFromDate);
		builder.append(", bookingToDate=");
		builder.append(bookingToDate);
		builder.append(", optionValue=");
		builder.append(optionValue);
		builder.append(", channelID=");
		builder.append(channelID);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((entityid == null) ? 0 : entityid.hashCode());
		result = prime * result + ((entitytype == null) ? 0 : entitytype.hashCode());
		result = prime * result + ((fromDate == null) ? 0 : fromDate.hashCode());
		result = prime * result + ((modifier == null) ? 0 : modifier.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((param == null) ? 0 : param.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Yield other = (Yield) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (entityid == null) {
			if (other.entityid != null)
				return false;
		} else if (!entityid.equals(other.entityid))
			return false;
		if (entitytype == null) {
			if (other.entitytype != null)
				return false;
		} else if (!entitytype.equals(other.entitytype))
			return false;
		if (fromDate == null) {
			if (other.fromDate != null)
				return false;
		} else if (!fromDate.equals(other.fromDate))
			return false;
		if (modifier == null) {
			if (other.modifier != null)
				return false;
		} else if (!modifier.equals(other.modifier))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (param == null) {
			if (other.param != null)
				return false;
		} else if (!param.equals(other.param))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (toDate == null) {
			if (other.toDate != null)
				return false;
		} else if (!toDate.equals(other.toDate))
			return false;
		return true;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
