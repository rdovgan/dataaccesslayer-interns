package com.mybookingpal.dal.shared.price;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.shared.Price;

@Component("PriceSearch")
@Scope(value = "prototype")
public class PriceSearch extends Price {

    private List<String> entityTypes;
    private List<String> entityIds;
    
    public PriceSearch() {
    	super();
    }


    public List<String> getEntityTypes() {
        return entityTypes;
    }


    public void setEntityTypes(List<String> entityTypes) {
        this.entityTypes = entityTypes;
    }


    public List<String> getEntityIds() {
        return entityIds;
    }


    public void setEntityIds(List<String> entityIds) {
        this.entityIds = entityIds;
    }

}
