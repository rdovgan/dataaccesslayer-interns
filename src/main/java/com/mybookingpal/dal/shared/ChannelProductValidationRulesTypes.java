package com.mybookingpal.dal.shared;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ChannelProductValidationRulesTypes {

    private Integer id;

    private String name;

    private String defaultValue;

    private String description;

    private LocalDateTime createdDate;

    private LocalDateTime version;

}
