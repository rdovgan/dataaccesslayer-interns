package com.mybookingpal.dal.shared;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//import org.apache.cxf.jaxrs.model.wadl.Description;
//import org.apache.cxf.jaxrs.model.wadl.DocTarget;

//import com.google.gwt.user.client.rpc.IsSerializable;


//@XmlRootElement(name = "keyvalue")
//@Description(value = "Key/value pair", target = DocTarget.RESPONSE)
@Component
@Scope(value = "prototype")
public class KeyValue 
//implements IsSerializable 
{

	private String key;
	private String value;

	public KeyValue() {}

	public KeyValue(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("KeyValue [key=");
		builder.append(key);
		builder.append(", value=");
		builder.append(value);
		builder.append("]");
		return builder.toString();
	}

}
