package com.mybookingpal.dal.shared;


import com.mybookingpal.dal.elastic.ChannelProductOnbStatus;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;

public class ChannelProductStatus {

    public enum Status {
        FAILED(0, "Failed"), SUCCESS(1, "Success"), IN_PROGGRESS(2, "In Progress");

        private int value;

        private String elasticValue;

        Status(int value, String elasticValue) {
            this.value = value;
            this.elasticValue = elasticValue;
        }

        public int getValue() {
            return value;
        }

        public String getElasticValue() {
            return elasticValue;
        }

        public static String findElasticValue(int value){
            return Arrays.stream(Status.values()).filter(status -> status.getValue() == value).findFirst().get().getElasticValue();
        }
    }

    public enum Function {
        OPEN(0, "Open"), CLOSE(1, "Close"), IMAGE(2, "Images"), STATIC(3, "Static"),
        DYNAMIC(4, "Dynamic"), CONNECT(5, "Connect"), BUILD (6, "Build"),
        CANCELLATION (7,"cancellation_policy"), SYNC_TYPE (8,"sync_type"), CHANNEL_VALIDATION(9, "channel_validation");

        private int value;

        private String elasticValue;

        Function(int value, String elasticValue) {
            this.value = value;
            this.elasticValue = elasticValue;
        }

        public int getValue() {
            return value;
        }

        public String getElasticValue() {
            return elasticValue;
        }

        public static String findElasticValue(int value){
            return Arrays.stream(Function.values()).filter(function -> function.getValue() == value).findFirst().get().getElasticValue();
        }

        public static Integer findValue(String value){
            return Arrays.stream(Function.values()).filter(function -> function.getElasticValue().equalsIgnoreCase(value)).findFirst().get().getValue();
        }
    }

    private Integer productId;

    private Integer channelId;

    private Integer status;

    private String errorMessage;

    private String warningMessage;

    private Integer isRead;

    private Integer function;

    private LocalDateTime createdDate;

    private LocalDateTime version;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(String warningMessage) {
        this.warningMessage = warningMessage;
    }

    public Integer getIsRead() {
        return isRead;
    }

    public void setIsRead(Integer isRead) {
        this.isRead = isRead;
    }

    public Integer getFunction() {
        return function;
    }

    public void setFunction(Integer function) {
        this.function = function;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getVersion() {
        return version;
    }

    public void setVersion(LocalDateTime version) {
        this.version = version;
    }

    public ChannelProductOnbStatus fillChannelProductOnbStatus (){
        ChannelProductOnbStatus channelProductOnbStatus = new ChannelProductOnbStatus();
        channelProductOnbStatus.setChannelId(this.getChannelId());
        channelProductOnbStatus.setProductId(this.getProductId());
        channelProductOnbStatus.setErrorMessage(this.getErrorMessage());
        channelProductOnbStatus.setWarningMessage(this.getWarningMessage());
        channelProductOnbStatus.setStatus(Status.findElasticValue(this.getStatus()));
        channelProductOnbStatus.setFunction(String.valueOf(Function.findElasticValue(this.getFunction())));
        if(this.version != null) {
            channelProductOnbStatus.setVersion(Date.from(this.version.atZone(ZoneId.systemDefault()).toInstant()));
        }
        return channelProductOnbStatus;
    }

}
