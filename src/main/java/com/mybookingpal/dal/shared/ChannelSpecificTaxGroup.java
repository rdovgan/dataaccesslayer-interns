package com.mybookingpal.dal.shared;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChannelSpecificTaxGroup {

    private Integer id;
    private Integer propertyManagerId;
    private Date createdDate;

}
