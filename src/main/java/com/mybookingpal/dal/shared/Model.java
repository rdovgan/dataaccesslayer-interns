package com.mybookingpal.dal.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.xml.bind.annotation.XmlTransient;

import com.mybookingpal.utils.entity.NameId;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * The Class Model.
 */
@Component
@Scope(value = "prototype")
public abstract class Model extends com.mybookingpal.utils.entity.NameId {

	public static final String BLANK = "";
	public static final String ZERO = "0";
	public static final String TRUE = "1";
	public static final String DELIMITER = ":";
	public static final String STATE = "state";
	public static final String UNKNOWN = "Unknown";

	protected String organizationid;
	protected String actorid;
	protected String altpartyid;
	protected String altid;
	protected String state;
	protected String options;

	protected int status = 0;

	private Date version; //latest change
	protected HashMap<String, String> values;
	protected HashMap<String, ArrayList<String>> attributemap;
	protected ArrayList<String> imageurls;
	protected HashMap<String, Text> texts;

	/**
	 * Instantiates a new model.
	 */
	public Model() {
	}

	/* (non-Javadoc)
	  * @see com.mybookingpal.shared.api.HasResponse#getStatus()
	  */
	@XmlTransient
	public final int getStatus() {
		return status;
	}

	/* (non-Javadoc)
	 * @see com.mybookingpal.shared.api.HasResponse#setStatus(int)
	 */
	public final void setStatus(int status) {
		this.status = status;
	}

	/**
	 * Gets the organizationid.
	 *
	 * @return the organizationid
	 */
	@XmlTransient
	public final String getOrganizationid() {
		return organizationid;
	}

	/**
	 * Sets the organizationid.
	 *
	 * @param organizationid the new organizationid
	 */
	public final void setOrganizationid(String organizationid) {
		this.organizationid = organizationid;
	}

	public String getActorid() {
		return actorid;
	}

	public void setActorid(String actorid) {
		this.actorid = actorid;
	}

	public String getAltpartyid() {
		return altpartyid;
	}

	public void setAltpartyid(String altpartyid) {
		this.altpartyid = altpartyid;
	}

	public final boolean noAltpartyid() {
		return this.altpartyid == null || altpartyid.isEmpty();
	}

	public final boolean hasAltpartyid() {
		return !noAltpartyid();
	}

	/**
	 * Checks for the alternative party ID.
	 *
	 * @param altpartyid the alternate party ID.
	 * @return true, if successful
	 */
	public final boolean hasAltpartyid(String altpartyid) {
		return this.altpartyid != null && altpartyid != null && this.altpartyid.equalsIgnoreCase(altpartyid);
	}

	public String getAltid() {
		return altid;
	}

	public void setAltid(String altid) {
		this.altid = altid;
	}

	public boolean noAltid() {
		return altid == null || altid.isEmpty();
	}

	/* (non-Javadoc)
	 * @see com.mybookingpal.shared.api.HasState#getState()
	 */
	public final String getState() {
		return state;
	}

	/* (non-Javadoc)
	 * @see com.mybookingpal.shared.api.HasState#setState(java.lang.String)
	 */
	public final void setState(String state) {
		this.state = state;
	}

	/**
	 * Checks if the model is in the state.
	 *
	 * @param state the state.
	 * @return true, the model is in the state.
	 */
	public final boolean hasState(String state) {
		return this.state != null && state != null && this.state.equalsIgnoreCase(state);
	}

	/**
	 * Checks if in state list.
	 *
	 * @param states the state
	 * @return true, if successful
	 */
	public final boolean hasState(String[] states) {
		if (this.state == null || states == null || states.length <= 0) {
			return false;
		}
		for (String state : states) {
			if (state.equalsIgnoreCase(this.state)) {
				return true;
			}
		}
		return false;
	}

	@XmlTransient
	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	@XmlTransient
	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	/**
	 * Gets the value.
	 *
	 * @param key the key
	 * @return the value
	 */
	public final String getValue(String key) {
		if (values == null || values.isEmpty()) {
			return null;
		} else {
			return values.get(key);
		}
	}

	/**
	 * Sets the value.
	 *
	 * @param key the key
	 * @param value the value
	 */
	public final void setValue(String key, String value) {
		if (key == null || key.isEmpty() || value == null) {
			return;
		}
		if (this.values == null) {
			this.values = new HashMap<String, String>();
		}
		values.put(key, value);
	}

	//---------------------------------------------------------------------------------
	// Text functions
	//---------------------------------------------------------------------------------
	/**
	 * Sets the texts.
	 *
	 * @param texts the texts
	 */
	public void setTexts(HashMap<String, Text> texts) {
		this.texts = texts;
	}

	/* (non-Javadoc)
	 * @see com.mybookingpal.shared.api.HasTexts#getTexts()
	 */
	public HashMap<String, Text> getTexts() {
		return texts;
	}

	/**
	 * Gets the specified text.
	 *
	 * @param model the NameIdUtils.Type of model
	 * @param id the id of the model
	 * @param type the Text.Type of the text
	 * @return the text.
	 */
	protected Text getText(NameIdUtils.Type model, String id, Text.Code type) {
		return id == null || id.isEmpty() ? null : getText("", model, id, type);
	}

	protected Text getText(String organizationid, NameIdUtils.Type model, String id) {
		return id == null || id.isEmpty() ? null : getText(organizationid, model, id, null);
	}

	protected Text getText(String organizationid, NameIdUtils.Type model, String id, Text.Code type) {
		if (id == null || id.isEmpty()) {
			return null;
		}
		String textid = organizationid + model.name() + id + (type == null ? "" : type.name());
		if (texts == null) {
			texts = new HashMap<String, Text>();
		}
		if (!texts.containsKey(textid)) {
			texts.put(textid, new Text(textid, name, Text.Type.HTML, new Date(), null, Language.EN));
		}
		return texts.get(textid);
	}

	/**
	 * Sets the text.
	 *
	 * @param id the id
	 * @param value the value
	 */
	protected void setText(NameIdUtils.Type model, String id, Text.Code type, Text value) {
		setText("", model, id, type, value);
	}

	protected void setText(String organizationid, NameIdUtils.Type model, String id, Text value) {
		setText(organizationid, model, id, null, value);
	}

	protected void setText(String organizationid, NameIdUtils.Type model, String id, Text.Code type, Text value) {
		if (value == null) {
			return;
		}
		String textid = organizationid + model.name() + id + (type == null ? "" : type.name());
		value.setId(textid);
		value.setName(name);
		if (texts == null) {
			texts = new HashMap<String, Text>();
		}
		texts.put(textid, value);
	}

	/* (non-Javadoc)
	 * @see com.mybookingpal.shared.NameId#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Model [organizationid=");
		builder.append(organizationid);
		builder.append(", status=");
		builder.append(status);
		builder.append(", name=");
		builder.append(name);
		builder.append(", id=");
		builder.append(id);
		builder.append(", state=");
		builder.append(state);
		builder.append("\nvalues=");
		builder.append(values);
		builder.append("\nattributes=");
		builder.append(attributemap);
		builder.append("]");
		return builder.toString();
	}
	

}
