package com.mybookingpal.dal.shared;

public class MarriottDiscountHmcMap {

	private Integer discountCodeId;
	private Integer partyId;

	public Integer getDiscountCodeId() {
		return discountCodeId;
	}

	public void setDiscountCodeId(Integer discountCodeId) {
		this.discountCodeId = discountCodeId;
	}

	public Integer getPartyId() {
		return partyId;
	}

	public void setPartyId(Integer partyId) {
		this.partyId = partyId;
	}
}
