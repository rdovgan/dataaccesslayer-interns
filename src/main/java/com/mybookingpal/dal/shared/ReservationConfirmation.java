package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

// (id INT NOT NULL auto_increment, ReservationID INT, ConfirmationID INT, ChannelID INT,  PRIMARY KEY  USING BTREE (id));
@Component
@Scope(value = "prototype")
public class ReservationConfirmation {
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((channelId == null) ? 0 : channelId.hashCode());
		result = prime * result + ((confirmationId == null) ? 0 : confirmationId.hashCode());
		result = prime * result + ((reservationId == null) ? 0 : reservationId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReservationConfirmation other = (ReservationConfirmation) obj;
		if (channelId == null) {
			if (other.channelId != null)
				return false;
		} else if (!channelId.equals(other.channelId))
			return false;
		if (confirmationId == null) {
			if (other.confirmationId != null)
				return false;
		} else if (!confirmationId.equals(other.confirmationId))
			return false;
		if (reservationId == null) {
			if (other.reservationId != null)
				return false;
		} else if (!reservationId.equals(other.reservationId))
			return false;
		return true;
	}

	private Integer id;
	private Integer reservationId;
	private String confirmationId;
	private Integer channelId;
	private Long ratePlanId;
	private Date createdDate;

	public int getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}

	public String getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(String confirmationId) {
		this.confirmationId = confirmationId;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	
	public Long getRatePlanId() {
		return ratePlanId;
	}

	public void setRatePlanId(Long ratePlanId) {
		this.ratePlanId = ratePlanId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDaten(Date createdDate) {
		this.createdDate = createdDate;
	}

}