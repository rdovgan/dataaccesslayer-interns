package com.mybookingpal.dal.shared;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

public class LosPrice {
	private Long id;
	private Integer productId;
	private LocalDate checkInDate;
	private Integer losValue;
	private Integer maxGuests;
	private Double amount;
	private String currency;
	private Boolean state;
	private Date createdDate;
	private Date version;

	public enum State {
		Created(true), Final(false);
		private boolean value;

		State(boolean value) {
			this.value = value;
		}

		public boolean value() {
			return value;
		}

		public void setValue(boolean value) {
			this.value = value;
		}

		public static State defineState(Boolean v) {
			return Arrays.stream(State.values()).filter(state -> state.value == v).findFirst()
					.orElseThrow(() -> new IllegalArgumentException(String.valueOf(v)));
		}
	}

	public LosPrice(Long id, Integer productId, LocalDate checkInDate, Integer losValue, Integer maxGuests, Double amount, String currency, Boolean state) {
		this.id = id;
		this.productId = productId;
		this.checkInDate = checkInDate;
		this.losValue = losValue;
		this.maxGuests = maxGuests;
		this.amount = amount;
		this.currency = currency;
		this.state = state;
	}

	public LosPrice(Integer productId, LocalDate checkInDate, Integer losValue, Integer maxGuests, Double amount, String currency) {
		this.productId = productId;
		this.checkInDate = checkInDate;
		this.losValue = losValue;
		this.maxGuests = maxGuests;
		this.amount = amount;
		this.state = true;
		this.currency = currency;
	}

	public LosPrice(Double amount, Integer losValue) {
		this.losValue = losValue;
		this.amount = amount;
	}

	public LosPrice() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public LocalDate getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(LocalDate checkInDate) {
		this.checkInDate = checkInDate;
	}

	public Integer getLosValue() {
		return losValue;
	}

	public void setLosValue(Integer losValue) {
		this.losValue = losValue;
	}

	public Integer getMaxGuests() {
		return maxGuests;
	}

	public void setMaxGuests(Integer maxGuests) {
		this.maxGuests = maxGuests;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		LosPrice losPrice = (LosPrice) o;
		return 	Objects.equals(productId, losPrice.productId) &&
				Objects.equals(checkInDate, losPrice.checkInDate) &&
				Objects.equals(losValue, losPrice.losValue) &&
				Objects.equals(maxGuests, losPrice.maxGuests) &&
				Objects.equals(amount, losPrice.amount) &&
				Objects.equals(currency, losPrice.currency) &&
				Objects.equals(state, losPrice.state);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, productId, checkInDate, losValue, maxGuests, amount, currency, state, createdDate, version);
	}
}
