package com.mybookingpal.dal.shared.image;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.shared.Image;

@Component("ImageSearch")
@Scope(value = "prototype")
public class ImageSearch extends Image {
    
    private List<String> productIds;
    private Integer imagesCount;
    
        
    public List<String> getProductIds() {
        return productIds;
    }
    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }
    public Integer getImagesCount() {
        return imagesCount;
    }
    public void setImagesCount(Integer imagesCount) {
        this.imagesCount = imagesCount;
    } 

}
