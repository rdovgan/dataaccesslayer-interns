package com.mybookingpal.dal.shared.image;

/**
 * @author Danijel Blagojevic
 *
 */
public class ImageUrlTag {

	String imageUrl;
	Integer imageTagId;
	Integer bookingcomId;
	String tagName;

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Integer getImageTagId() {
		return imageTagId;
	}

	public void setImageTagId(Integer imageTagId) {
		this.imageTagId = imageTagId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public Integer getBookingcomId() {
		return bookingcomId;
	}

	public void setBookingcomId(Integer bookingcomId) {
		this.bookingcomId = bookingcomId;
	}
}
