package com.mybookingpal.dal.shared.image;

import java.util.Comparator;

import com.mybookingpal.dal.shared.Image;

public class ImageSortOrderComparator implements Comparator<Object> {

	@Override
	public int compare(Object i1, Object i2) {

		if ((i1 instanceof Image) && (i2 instanceof Image)) {
			if (((Image) i1).getSort() != null && ((Image) i2).getSort() != null) {
				return ((Image) i1).getSort() - ((Image) i2).getSort();
			}
		}
		return -1;
	}

}