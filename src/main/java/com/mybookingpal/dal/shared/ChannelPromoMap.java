package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ChannelPromoMap {
	
	private Integer id;
	private String promoName;
	private String channelId;
	private String channelProductId;
	private String channelRateId;
	private String channelPromoId;
	private String productId;
	private Integer yieldId;
	private Date version;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPromoName() {
		return promoName;
	}
	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getChannelProductId() {
		return channelProductId;
	}
	public void setChannelProductId(String channelProductId) {
		this.channelProductId = channelProductId;
	}
	public String getChannelRateId() {
		return channelRateId;
	}
	public void setChannelRateId(String channelRateId) {
		this.channelRateId = channelRateId;
	}
	public String getChannelPromoId() {
		return channelPromoId;
	}
	public void setChannelPromoId(String channelPromoId) {
		this.channelPromoId = channelPromoId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public Integer getYieldId() {
		return yieldId;
	}
	public void setYieldId(Integer yieldId) {
		this.yieldId = yieldId;
	}
	public Date getVersion() {
		return version;
	}
	public void setVersion(Date version) {
		this.version = version;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ChannelPromoMap [id=");
		builder.append(id);
		builder.append(", \npromoName=");
		builder.append(promoName);
		builder.append(", \nchannelId=");
		builder.append(channelId);
		builder.append(", \nchannelProductId=");
		builder.append(channelProductId);
		builder.append(", \nchannelRateId=");
		builder.append(channelRateId);
		builder.append(", \nchannelPromoId=");
		builder.append(channelPromoId);
		builder.append(", \nproductId=");
		builder.append(productId);
		builder.append(", \nyieldId=");
		builder.append(yieldId);
		return builder.toString();
	}
	
}