package com.mybookingpal.dal.shared.searchReservation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
@Scope(value = "prototype")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SearchReservationModelRequest {

    private Boolean isSortDesc = false;
    private Boolean isReservationId = false;
    private Boolean isChannelId = false;
    private Boolean isPropertyName = false;
    private Boolean isState = false;
    private Boolean isGuest = false;
    private Boolean isCheckIn = false;
    private Boolean isCheckOut = false;
    private Boolean isTotalPrice = false;
    private Boolean isChannelName = false;
    private Boolean isDefault = false;
    private Integer supplierId;
    private String state;
    private LocalDate startDate;
    private LocalDate endDate;
    private String search;
    private Integer searchId;
    private Integer startPagination;
    private Integer limit;

    public enum ColumnSearch {
        ID("Id"),
        CHANNEL_ID("Channel id"),
        PROPERTY("Property"),
        STATE("State"),
        GUEST("Guest"),
        CHECK_IN("Check in"),
        CHECK_OUT("Check out"),
        TOTAL("Total"),
        CHANNEL("Channel");

        private String value;

        ColumnSearch(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }

    public enum Sort {
        DESC("Desc"),
        ASC("Asc");

        private String value;

        Sort(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }

    public SearchReservationModelRequest(ColumnSearch columnSearch, Sort sort, Integer supplierId, String state, LocalDate startDate, LocalDate endDate, String search, Integer searchId, Integer startPagination, Integer limit) {
        this.startPagination = startPagination;
        this.limit = limit;
        this.searchId = searchId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.search = search;
        this.state = state;
        this.supplierId = supplierId;
        if(columnSearch != null){
            switch (columnSearch) {
                case ID:
                    this.isReservationId = true;
                    break;
                case CHANNEL_ID:
                    this.isChannelId = true;
                    break;
                case PROPERTY:
                    this.isPropertyName = true;
                    break;
                case STATE:
                    this.isState = true;
                    break;
                case GUEST:
                    this.isGuest = true;
                    break;
                case CHECK_IN:
                    this.isCheckIn = true;
                    break;
                case CHECK_OUT:
                    this.isCheckOut = true;
                    break;
                case TOTAL:
                    this.isTotalPrice = true;
                    break;
                case CHANNEL:
                    this.isChannelName = true;
                    break;
                default:
                    this.isDefault = true;
            }
        } else {
            this.isDefault = true;
        }
        if(sort != null) {
            switch (sort) {
                case DESC:
                    this.isSortDesc = true;
                    break;
                case ASC:
                    this.isSortDesc = false;
                    break;
            }
        } else {
            this.isSortDesc = true;
        }
    }
}
