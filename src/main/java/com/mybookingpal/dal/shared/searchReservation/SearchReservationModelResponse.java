package com.mybookingpal.dal.shared.searchReservation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SearchReservationModelResponse {

    //reservation
    private Integer reservationId;
    private String reservationState;
    private String checkIn;
    private String checkOut;
    private Double totalPrice;
    //channel
    private String channelName;
    private String channelAbbreviation;
    private Integer channelId;
    private String channelReservationId;
    //property
    private Integer propertyId;
    private String propertyName;
    private String propertyAltId;
    //guest data
    private String state;
    private String firstName;
    private Integer customerId;

}
