package com.mybookingpal.dal.shared.enums;

/**
 * @author Danijel Blagojevic
 *
 */
public enum ReviewStatusAirBnb {

	NEW("new"),
	R4R("ready for review"),
	APPROVED("approved"),
	REJECTED("rejected"),
	DELETED("deleted");

	private String value;

	ReviewStatusAirBnb(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static String getNameByValue(ReviewStatusAirBnb value) {
		for (ReviewStatusAirBnb v : values()) {
			if (v.equals(value)) {
				return v.value;
			}
		}
		return null;
	}
}
