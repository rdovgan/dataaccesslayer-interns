package com.mybookingpal.dal.shared.enums;

public enum AirbnbSyncTypeConstantsEnum {

    SYNC_ALL(1),
    SYNC_RA(0),
    SYNC_UNDECIDED(2);

    private Integer value;

    AirbnbSyncTypeConstantsEnum(Integer value) {this.value = value;}

    public Integer getValue() {return this.value;}

}
