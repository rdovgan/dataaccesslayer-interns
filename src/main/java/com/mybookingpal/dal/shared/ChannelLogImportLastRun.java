package com.mybookingpal.dal.shared;

import java.util.Date;

public class ChannelLogImportLastRun {
	public enum ChannelLogImportStatusEnum {
		RUNNING("Running"), FINISHED("Finished"), FAILED("Failed");

		public static ChannelLogImportStatusEnum getByString(String value) {
			for (ChannelLogImportStatusEnum v : values()) {
				if (v.value == value) {
					return v;
				}
			}
			return RUNNING;
		}

		public static String getNameByString(String value) {
			return getByString(value).name();
		}

		private String value;

		ChannelLogImportStatusEnum(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}
	}

	public enum JobNameEnum {
		UNKNOWN("Unknown"), LISTING("Listing"), LISTING_DESCRIPTION("ListingDescription"), RATES_AND_AVAILABILITY("RatesAndAvailability"), LISTING_PHOTOS(
				"ListingPhotos"), REVIEW_STATE("ReviewState");

		public static JobNameEnum getByString(String value) {
			for (JobNameEnum v : values()) {
				if (v.value == value) {
					return v;
				}
			}
			return UNKNOWN;
		}

		public static String getNameByString(String value) {
			return getByString(value).name();
		}

		private String value;

		JobNameEnum(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

	}

	private int id;
	private String partyId;
	private int channelId;
	private Date startTime;
	private Date endTime;
	private Byte runType;
	private String emailAlert;
	private ChannelLogImportStatusEnum status;
	private JobNameEnum jobName;

	public ChannelLogImportLastRun() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ChannelLogImportLastRun(String partyId, int channelId, Date startTime, Byte runType, JobNameEnum jobNameEnum) {
		this();
		this.partyId = partyId;
		this.channelId = channelId;
		this.startTime = startTime;
		this.endTime = new Date();
		this.runType = runType;
		this.status = ChannelLogImportStatusEnum.RUNNING;
		this.jobName = jobNameEnum;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Byte getRunType() {
		return runType;
	}

	public void setRunType(Byte runType) {
		this.runType = runType;
	}

	public String getEmailAlert() {
		return emailAlert;
	}

	public void setEmailAlert(String emailAlert) {
		this.emailAlert = emailAlert;
	}

	public ChannelLogImportStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ChannelLogImportStatusEnum status) {
		this.status = status;
	}

	public JobNameEnum getJobName() {
		return jobName;
	}

	public void setJobName(JobNameEnum jobName) {
		this.jobName = jobName;
	}

	@Override
	public String toString() {
		return "ChannelLogImportLastRun [id=" + id + ", partyId=" + partyId + ", channelId=" + channelId + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", runType=" + runType + ", emailAlert=" + emailAlert + ", status=" + status + ", jobName=" + jobName + "]";
	}

}
