package com.mybookingpal.dal.shared;

public enum Service {
	
	LOCATION("LocationService"),
	PARTNER ("PartnerService"),
	ATTRIBUTE ("AttributeService"),
	TAX ("TaxService"),
	IMAGE ("ImageService"),
	MONITOR ("MonitorService"),
	CONTRACT ("ContractService");


	private String path = "com.mybookingpal.core.server.";
	private String classname;

	Service(String classname) {
		this.classname = classname;
	}

	public String classname() {
		return path + classname;
	}
}
