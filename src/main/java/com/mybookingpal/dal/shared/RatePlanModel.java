package com.mybookingpal.dal.shared;

import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class RatePlanModel {

	private Long id;
	private String code;
	private String name;
	private State state;
	private Date created;
	private Date version;

	public enum State {
		CREATED, FINAL
	}

	public RatePlanModel() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (!(o instanceof RatePlanModel))
			return false;

		RatePlanModel that = (RatePlanModel) o;

		return new EqualsBuilder().append(getId(), that.getId()).append(getCode(), that.getCode()).append(getName(), that.getName())
				.append(getState(), that.getState()).append(getCreated(), that.getCreated()).append(getVersion(), that.getVersion()).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(getId()).append(getCode()).append(getName()).append(getState()).append(getCreated()).append(getVersion())
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("code", code).append("name", name).append("state", state).append("created", created)
				.append("version", version).toString();
	}
}
