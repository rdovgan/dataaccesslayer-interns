package com.mybookingpal.dal.shared;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;

@Data
@Scope(value = "prototype")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExpediaInactiveReport {

    private Integer id;
    private String propertyName;
    private String state;
    private Integer channelProductId;
    private String supplierId;
    private String pmName;
    private boolean stateCPM;
    private String channelState;
}
