package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("ChannelPrice")
@Scope(value = "prototype")
public class ChannelPrice  {	
	public static final String INITIAL = "Initial";
	public static final String FINAL = "Final";
	public static final String CREATED = "Created";
	public static final String INVOICED = "Invoiced";
	public static final String[] STATES = { INITIAL, CREATED, INVOICED, FINAL };
	
	protected int channelPriceId;
	protected int productID;
	protected String channelProductID;
	protected int channelID;
	protected Date version; 	
	protected String state;
	protected Date created;
	protected Date start_at;
	protected Date end_at;
	protected Double value;
	protected Double weekend_night_value;
	protected String label;
	protected Integer min_stay;
	

	public String getChannelProductID() {
		return channelProductID;
	}
	
	public void setChannelProductID(String channelProductID) {
		this.channelProductID = channelProductID;
	}
	
	public int getChannelID() {
		return channelID;
	}
	
	public void setChannelID(int channelID) {
		this.channelID = channelID;
	}
	
	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	
	public final String getState() {
		return state;
	}
	
	public final void setState(String state) {
		this.state = state;
	}
	
	
	
	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	/**
	 * @return the min_stay
	 */
	public Integer getMin_stay() {
		return min_stay;
	}

	/**
	 * @param min_stay the min_stay to set
	 */
	public void setMin_stay(Integer min_stay) {
		this.min_stay = min_stay;
	}

	/**
	 * @return the channelPriceId
	 */
	public int getChannelPriceId() {
		return channelPriceId;
	}

	/**
	 * @param channelPriceId the channelPriceId to set
	 */
	public void setChannelPriceId(int channelPriceId) {
		this.channelPriceId = channelPriceId;
	}

	/**
	 * @return the productID
	 */
	public int getProductID() {
		return productID;
	}

	/**
	 * @param productID the productID to set
	 */
	public void setProductID(int productID) {
		this.productID = productID;
	}

	/**
	 * @return the start_at
	 */
	public Date getStart_at() {
		return start_at;
	}

	/**
	 * @param start_at the start_at to set
	 */
	public void setStart_at(Date start_at) {
		this.start_at = start_at;
	}

	/**
	 * @return the end_at
	 */
	public Date getEnd_at() {
		return end_at;
	}

	/**
	 * @param end_at the end_at to set
	 */
	public void setEnd_at(Date end_at) {
		this.end_at = end_at;
	}

	/**
	 * @return the value
	 */
	public Double getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Double value) {
		this.value = value;
	}

	/**
	 * @return the weekend_night_value
	 */
	public Double getWeekend_night_value() {
		return weekend_night_value;
	}

	/**
	 * @param weekend_night_value the weekend_night_value to set
	 */
	public void setWeekend_night_value(Double weekend_night_value) {
		this.weekend_night_value = weekend_night_value;
	}
	

}