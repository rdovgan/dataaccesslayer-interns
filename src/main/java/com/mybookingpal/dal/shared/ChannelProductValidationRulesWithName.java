package com.mybookingpal.dal.shared;

import lombok.Data;

@Data
public class ChannelProductValidationRulesWithName {

    private int channelId;

    private String value;

    private String name;

}
