package com.mybookingpal.dal.shared;

import org.springframework.stereotype.Component;

@Component("ProductBedroomBedWithName")
public class ProductBedroomBedWithName extends ProductBedroomBed {
	private String bedName;

	// Can be Living Room or Bedroom. Added for Tripadvisor to specify if the bed is inside or outside of the bedroom
	private String BedRoomType;

	public String getBedName() {
		return bedName;
	}

	public void setBedName(String bedName) {
		this.bedName = bedName;
	}

	public String getBedRoomType() {
		return BedRoomType;
	}

	public void setBedRoomType(String bedRoomType) {
		BedRoomType = bedRoomType;
	}

}
