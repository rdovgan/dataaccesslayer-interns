package com.mybookingpal.dal.shared;

import java.util.ArrayList;
import java.util.Date;

import com.mybookingpal.dal.entity.IsParty;
import com.mybookingpal.utils.entity.NameId;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class Party extends Entity implements IsParty {

	// Preferences
	public enum Preference {LATITUDE, LONGITUDE, PRICEUNIT, ZOOM};

	// Constants
	public static final String WIDGET = "0";
	public static final String ADMINISTRATOR = "1";
	public static final String NO_ACTOR = "2";
	public static final String CBT_LTD_PARTY = "4";
	public static final String ALL_ORGANIZATIONS = "-1";
	public static final String PASSWORD = "password";
	
	//Fields
	public static final String DAYPHONE = "party.dayphone";
	public static final String EMAILADDRESS = "party.emailaddress";
	public static final String MOBILEPHONE = "party.mobilephone";
	public static final String RANK = "party.rank";

	// States
	public static final String INITIAL = "Initial";
	public static final String FINAL = "Final";
	public static final String CREATED = "Created";
	public static final String SUSPENDED = "Suspended";
	
	public static final String[] STATES = {INITIAL, CREATED, SUSPENDED, FINAL};

	public static final String PHONE = "(###)###-####";
	//Date Formats
	public static final String MM_DD_YYYY = "MM/dd/yyyy";
	public static final String DD_MM_YYYY = "dd/MM/yyyy";
	public static final String YYYY_MM_DD = "yyyy/MM/dd";
	public static final String MMDDYYYY = "MM-dd-yyyy";
	public static final String DDMMYYYY = "dd-MM-yyyy";
	public static final String YYYYMMDD = "yyyy-MM-dd";
	public static final String HH_MM = " HH:mm";
	public static final String HH_MM_SS = " HH:mm:ss";

	public static final String[] DATE_FORMATS = {MM_DD_YYYY, DD_MM_YYYY, YYYY_MM_DD, MMDDYYYY, DDMMYYYY, YYYYMMDD};
	
	public static final String[] DEPOSITS = {"% Booking Value", "% Daily Rate", "Amount per Booking", "Amount per Day"};
			
	public static final String[] TIME_INTERVAL = {"0:DAY", "1:DAY", "3:DAY", "5:DAY", "7:DAY", "10:DAY", "2:WEE", "1:MON", "2:MON", "3:MON"};

	public enum Type {Actor, Affiliate, Agent, Customer, Employee, Employer, Jurisdiction, Organization, Owner, Supplier};
	
	public enum Value {Countunit, Currency, Distance, DistanceUnit, Deposit, DepositType, CancelType, CancelTime, CancelRefund, CancelFee, DamageCoverageType,
		DamageInsurance, Interhome, Latitude, Longitude, LookBookCount, Payfull, Payunit, Paytype, Priceunit,
		ProgressActivity, ProgressActivityMax, ProgressAge, ProgressAgeMax, ProgressBrochure, ProgressBrochureMax, 
		ProgressConfirm, ProgressCreator, ProgressCreatorMax, ProgressValue, ProgressValueMax, ZoomLevel};

		/**
		 * Sets the specified string into US phone number format
		 *
		 * @param text the specified string
		 * @return the US phone number
		 */
		public static String setPhoneNumber(String text) {
		    String newtext = text.replaceAll("\\D+", "");
		    if (newtext.length() == 10) {return "(" + text.substring(0, 3) + ") " + text.substring(3, 6) + "-" + text.substring(6, 10);}
		    else {return text;}
		}

		protected String employerid;
		protected String creatorid;
		protected String financeid;
		protected String jurisdictionid;
		protected String oldstate;
		protected String extraname;
		protected String identitynumber;
		protected String taxnumber;
		protected String businessTaxId;
		protected String emailaddress;
		protected String webaddress;
		protected String dayphone;
		protected String nightphone;
		protected String faxphone;
		protected String mobilephone;
		protected String notes;
		protected String country;
		//protected String language;
		protected String formatdate;
		protected String formatphone;
		protected Date birthdate;
		protected Integer rank;
		protected String password;
		protected String postalcode;
		private boolean manager;		
		private boolean agent;
		private String message;
		private String usertype;
		private String xsl; //NB HAS GET&SET = private
		private ArrayList<String> roles;
		private ArrayList<String> types;
		private boolean skipLicense;
		
		protected Address address = new Address("");

		public Party() {super(NameIdUtils.Type.Party.name());}

		public boolean isManager() {
			return manager;
		}

		public void setOrganization(boolean manager) {
			this.manager = manager;
		}

		public boolean isAgent() {
			return agent;
		}

		public void setAgent(boolean agent) {
			this.agent = agent;
		}

		public String getEmployerid() {
			return employerid;
		}

		public void setEmployerid(String parentid) {
			this.employerid = parentid;
		}

		public boolean noEmployerid() {
			return employerid == null || employerid.isEmpty();
		}

		public String getCreatorid() {
			return creatorid;
		}
		
		public void setCreatorid(String creatorid) {
			this.creatorid = creatorid;
		}
		
		public boolean noCreatorid(String creatorid) {
			return this.creatorid == null || creatorid == null || !this.creatorid.equalsIgnoreCase(creatorid);
		}
		
		public boolean hasCreatorid(String creatorid) {
			return !noCreatorid(creatorid);
		}
		
		public String getFinanceid() {
			return financeid;
		}

		public void setFinanceid(String financeid) {
			this.financeid = financeid;
		}

		public boolean noFinanceid() {
			return financeid == null || financeid.isEmpty() || financeid.equals(Model.ZERO);
		}

		public String getJurisdictionid() {
			return jurisdictionid;
		}

		public void setJurisdictionid(String jurisdictionid) {
			this.jurisdictionid = jurisdictionid;
		}

		public String getOldstate() {
			return oldstate;
		}

		public void setOldstate(String oldstate) {
			this.oldstate = oldstate;
		}

		public boolean hasOldstate(String oldstate) {
			return this.oldstate != null && this.oldstate.equals(oldstate);
		}

		public String getFirstName() {
			if (name == null || name.isEmpty()) {
				return null;
			}
			String[] args = name.split(",");
			return args.length > 1 ? args[1].trim() : args[0].trim();
		}

		public boolean noFirstName() {
			return getFirstName() == null || getFirstName().trim().isEmpty();
		}

		public String getFamilyName() {
			if (name == null || name.isEmpty()){
				return null;
			}
			String[] args = name.split(",");
			return args[0].trim();
		}

		public boolean noFamilyName() {
			return getFamilyName() == null || getFamilyName().trim().isEmpty();
		}

		public String getExtraname() {
			return extraname;
		}

		public void setExtraname(String extraname) {
			this.extraname = extraname;
		}

		public String getIdentitynumber() {
			return identitynumber;
		}

		public void setIdentitynumber(String identitynumber) {
			this.identitynumber = identitynumber;
		}

		public String getTaxnumber() {
			return taxnumber;
		}

		public void setTaxnumber(String taxnumber) {
			this.taxnumber = taxnumber;
		}

		public boolean noTaxnumber() {
			return taxnumber == null || taxnumber.trim().isEmpty();
		}

		public boolean hasTaxnumber() {
		return !noTaxnumber();
	}

		public String getBusinessTaxId() {
			return businessTaxId;
		}

		public void setBusinessTaxId(String businessTaxId) {
			this.businessTaxId = businessTaxId;
		}

		public String getPostaladdress() {
			return address.getPostalAddress();
		}

		public void setPostaladdress(String postaladdress) {
			address = new Address(postaladdress);
		}

		public boolean noPostaladdress() {
			return address == null;
		}

		public boolean hasPostaladdress() {
			return !noPostaladdress();
		}

		public void setAddress(String[] address) {
		}
		
		public String getLocalAddress() {
			return address.getAddress();
		}
		
		public void setLocalAddress(String localAddress) {
			address.setAddress(localAddress);
		}
		
		public String getCity() {
			return address.getCity();
		}

		public void setCity(String city) {
			this.address.setCity(city);
		}

		public String getRegion() {
			return address.getState();
		}

		public void setRegion(String region) {
			address.setState(region);
		}

		public String getPostalcode() {
				return postalcode;
		}

		public void setPostalcode(String postalcode) {
			//address.setZip(postalcode);
			this.postalcode = postalcode; 
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public boolean noCountry() {
			return country == null || country.trim().isEmpty();
		}

		public boolean noLanguage() {
			return language == null || language.trim().isEmpty();
		}

		public String getFormatdate() {
			return formatdate;
		}

		public void setFormatdate(String formatdate) {
			this.formatdate = formatdate;
		}

		public String getFormatphone() {
			return formatphone;
		}

		public void setFormatphone(String formatphone) {
			this.formatphone = formatphone;
		}

//		@XmlTransient
		public String getEmailaddresses() {
			return emailaddress;
		}

		public void setEmailaddress(String emailaddress) {
			this.emailaddress = emailaddress;
		}

//		@XmlTransient
		public String getEmailaddress() {
			return this.emailaddress;
		}

		public String getWebaddress() {
			return webaddress;
		}

		public void setWebaddress(String webaddress) {
			this.webaddress = webaddress;
		}

		public String getDayphone() {
			return dayphone;
		}

		public void setDayphone(String dayphone) {
			this.dayphone = dayphone;
		}

		public String getNightphone() {
			return nightphone;
		}

		public void setNightphone(String nightphone) {
			this.nightphone = nightphone;
		}

		public String getFaxphone() {
			return faxphone;
		}

		public void setFaxphone(String faxphone) {
			this.faxphone = faxphone;
		}

		public String getMobilephone() {
			return mobilephone;
		}

		public void setMobilephone(String mobilephone) {
			this.mobilephone = mobilephone;
		}

		public boolean noMobilephone() {
			return mobilephone == null || mobilephone.trim().isEmpty();
		}

		public String getNotes() {
			return notes;
		}

		public void setNotes(String notes) {
			this.notes = notes;
		}

		public Date getBirthdate() {
			return birthdate;
		}

		public void setBirthdate(Date birthdate) {
			this.birthdate = birthdate;
		}

		public Integer getRank() {
			return rank;
		}

		public void setRank(Integer rank) {
			this.rank = rank;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public boolean noPassword() {
			return password == null || password.isEmpty();
		}

		public boolean hasPassword() {
			return !noPassword();
		}

		public ArrayList<String> getRoles() {
			return roles;
		}

		public void setRoles(ArrayList<String> roles) {
			this.roles = roles;
		}

		public boolean noRoles() {
			return roles == null || roles.isEmpty();
		}
		
		public ArrayList<String> getTypes() {
			return types;
		}

		public String getType() {
			return types == null || types.isEmpty() ? null : types.get(0);
		}

		public void setType(String type) {
			if (types == null) {types = new ArrayList<String>();}
			types.add(type);
		}

		public boolean notType(String type) {
			return types == null || types.isEmpty() || type == null || !types.contains(type);
		}

		public void setTypes(ArrayList<String> types) {
			this.types = types;
		}

		public boolean noTypes() {
			return types == null || types.isEmpty();
		}
		
		public boolean hasTypes() {
			return !noTypes();
		}

		public boolean isSkipLicense() {
			return skipLicense;
		}

		public void setSkipLicense(boolean skipLicense) {
			this.skipLicense = skipLicense;
		}
		
		//---------------------------------------------------------------------------------
		// Public text shared among all organizations in several languages
		//---------------------------------------------------------------------------------
		public static String getPublicId(String id) {
			return NameIdUtils.Type.Party.name() + id + Text.Code.Public.name();
		}
		
		public static Text getPublicText(String id, String language) {
			return new Text(NameIdUtils.Type.Party.name() + id + Text.Code.Public.name(), null, Text.Type.HTML, new Date(), null, language);
		}
		
		public Text getPublicText() {
			return getText(NameIdUtils.Type.Party, id, Text.Code.Public);
		}
		
		public void setPublicText(Text value) {
			setText(NameIdUtils.Type.Party, id, Text.Code.Public, value);
		}

		//---------------------------------------------------------------------------------
		// Public text shared among all organizations in several languages
		//---------------------------------------------------------------------------------
		public static String getUrlId(String id) {
			return NameIdUtils.Type.Party.name() + id + Text.Code.Url.name();
		}
		
		public static Text getUrlText(String id, String language) {
			return new Text(NameIdUtils.Type.Party.name() + id + Text.Code.Url.name(), null, Text.Type.HTML, new Date(), null, language);
		}
		
		public Text getUrlText() {
			return getText(NameIdUtils.Type.Party, id, Text.Code.Url);
		}
		
		public void setUrlText(Text value) {
			setText(NameIdUtils.Type.Party, id, Text.Code.Url, value);
		}

		//---------------------------------------------------------------------------------
		// Text is specific to an organization
		//---------------------------------------------------------------------------------
		public static Text getPrivateText(String organizationid, String id, String language) {
			return new Text(organizationid + NameIdUtils.Type.Party.name() + id, language);
		}
		
		public Text getPrivateText(String organizationid){
			return getText(organizationid, NameIdUtils.Type.Party, id);
		}

		public void setPrivateText(String organizationid, Text value){
			setText(organizationid, NameIdUtils.Type.Party, id, value);
		}

		//---------------------------------------------------------------------------------
		// Contract text in HTML format are shared among all organizations in several languages
		//---------------------------------------------------------------------------------
		public final String getContractId(){
			return NameIdUtils.Type.Party.name() + id + Text.Code.Contract.name();
		}

		public static String getContractId(String id){
			return NameIdUtils.Type.Party.name() + id + Text.Code.Contract.name();
		}

		public static Text getContractText(String id, String language){
			return new Text(NameIdUtils.Type.Party.name() + id + Text.Code.Contract.name(), null, Text.Type.HTML, new Date(), null, language);
		}

		public Text getContractText(){
			return getText(NameIdUtils.Type.Party, id, Text.Code.Contract);
		}

		public void setContractText(Text value){
			setText(NameIdUtils.Type.Party, id, Text.Code.Contract, value);
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getUsertype() {
			return usertype;
		}

		public void setUsertype(String usertype) {
			this.usertype = usertype;
		}
		
		public String getStreet() {
			return address.getStreet();
		}
		
		public String getHouseNumber() {
			return address.getHouseNumber();
		}
		
		public String getHouseNumberAddition() {
			return address.getHouseNumberAddition();
		}
		
		//---------------------------------------------------------------------------------
		// Implements HasXsl interface
		//---------------------------------------------------------------------------------
		public String getXsl() {
			return xsl;
		}

		public void setXsl(String xsl) {
			this.xsl = xsl;
		}

		@Override
		public String toString() {
			return "Party [employerid=" + employerid +
					", \ncreatorid=" + creatorid +
					", \nfinanceid=" + financeid +
					", \njurisdictionid=" + jurisdictionid +
					", \noldstate=" + oldstate +
					", \nextraname=" + extraname +
					", \nidentitynumber=" + identitynumber +
					", \ntaxnumber=" + taxnumber +
					", \nemailaddress=" + emailaddress +
					", \nwebaddress=" + webaddress +
					", \ndayphone=" + dayphone +
					", \nnightphone=" + nightphone +
					", \nfaxphone=" + faxphone +
					", \nmobilephone=" + mobilephone +
					", \nnotes=" + notes +
					", \ncountry=" + country +
					", \nlanguage=" + language +
					", \nformatdate=" + formatdate +
					", \nformatphone=" + formatphone +
					", \nbirthdate=" + birthdate +
					", \nrank=" + rank +
					", \npassword=" + password +
					", \nmanager=" + manager +
					", \nagent=" + agent +
					", \nroles=" + roles +
					", \ntypes=" + types +
					", \nentitytype=" + entitytype +
					", \nlocationid=" + locationid +
					", \ncurrency=" + currency +
					", \nunit=" + unit +
					", \nlatitude=" + latitude +
					", \nlongitude=" + longitude +
					", \naltitude=" + altitude +
					", \norganizationid=" + organizationid +
					", \nstatus=" + status +
					", \nstate=" + state +
					", \nattributes=" + attributemap +
					", \nimages=" + imageurls +
					", \nname=" + name +
					", \nusertype=" + usertype +
					", \nid=" + id +
					"]";
		}

}
