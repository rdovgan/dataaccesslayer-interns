package com.mybookingpal.dal.shared;

import java.time.LocalDateTime;

public class ChannelValidationPushObject {

    private int productId;
    private Boolean valid;
    private LocalDateTime lastValidation;
    private String errors;
    private Integer channelId;
    private String key;
    private int supplierId;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        valid = valid;
    }

    public LocalDateTime getLastValidation() {
        return lastValidation;
    }

    public void setLastValidation(LocalDateTime lastValidation) {
        this.lastValidation = lastValidation;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }
}
