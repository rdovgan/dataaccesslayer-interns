package com.mybookingpal.dal.shared;

import com.mybookingpal.dal.shared.api.HasDateRange;

import java.time.LocalDate;
import java.util.Date;

public interface DateRange extends HasDateRange, Cloneable {

	void setFromDate(LocalDate fromDate);

	void setToDate(LocalDate toDate);
	
	Object clone()throws CloneNotSupportedException;
}
