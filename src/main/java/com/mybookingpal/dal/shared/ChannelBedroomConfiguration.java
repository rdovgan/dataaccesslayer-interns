package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.stereotype.Component;

/**
 * @author Danijel Blagojevic
 *
 */
@Component
public class ChannelBedroomConfiguration {

	private Integer id;
	private Integer productId;
	private Integer productBedroomId;
	private Integer channelRoomId;
	private Integer channelId;
	private Date createdDate;
	private Date version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getProductBedroomId() {
		return productBedroomId;
	}

	public void setProductBedroomId(Integer productBedroomId) {
		this.productBedroomId = productBedroomId;
	}

	public Integer getChannelRoomId() {
		return channelRoomId;
	}

	public void setChannelRoomId(Integer channelRoomId) {
		this.channelRoomId = channelRoomId;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((channelId == null) ? 0 : channelId.hashCode());
		result = prime * result + ((channelRoomId == null) ? 0 : channelRoomId.hashCode());
		result = prime * result + ((productBedroomId == null) ? 0 : productBedroomId.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChannelBedroomConfiguration other = (ChannelBedroomConfiguration) obj;
		if (channelId == null) {
			if (other.channelId != null)
				return false;
		} else if (!channelId.equals(other.channelId))
			return false;
		if (channelRoomId == null) {
			if (other.channelRoomId != null)
				return false;
		} else if (!channelRoomId.equals(other.channelRoomId))
			return false;
		if (productBedroomId == null) {
			if (other.productBedroomId != null)
				return false;
		} else if (!productBedroomId.equals(other.productBedroomId))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		return true;
	}
	
	

}
