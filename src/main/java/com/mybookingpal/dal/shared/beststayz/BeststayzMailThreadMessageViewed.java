package com.mybookingpal.dal.shared.beststayz;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BeststayzMailThreadMessageViewed {

	private Long id;
	private Long messageId;
	private Long partyId;
	private Date createdDate;
	private Date version;

	
}
