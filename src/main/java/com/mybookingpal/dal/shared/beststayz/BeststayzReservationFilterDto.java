package com.mybookingpal.dal.shared.beststayz;

import java.util.Date;
import java.util.List;

public class BeststayzReservationFilterDto {
	private String filterDate;
	private String startDate;
	private String endDate;
	private String reservationId;
	private String confirmationId;
	private String propertyId;
	private String channelProductId;
	private String ownerName;
	private String channelId;
	private List<String> mor;
	private List<String> states;
	private Date fromDate;
	private Date toDate;

	public BeststayzReservationFilterDto() {}

	public BeststayzReservationFilterDto(String filterDate, String startDate, String endDate, String reservationId, String confirmationId, String propertyId,
			String channelProductId, String ownerName, String channelId, List<String> mor, List<String> states) {
		this.filterDate = filterDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.reservationId = reservationId;
		this.confirmationId = confirmationId;
		this.propertyId = propertyId;
		this.channelProductId = channelProductId;
		this.ownerName = ownerName;
		this.channelId = channelId;
		this.mor = mor;
		this.states = states;
	}

	public String getFilterDate() {
		return filterDate;
	}

	public void setFilterDate(String filterDate) {
		this.filterDate = filterDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getReservationId() {
		return reservationId;
	}

	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}

	public String getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(String confirmationId) {
		this.confirmationId = confirmationId;
	}

	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	public String getChannelProductId() {
		return channelProductId;
	}

	public void setChannelProductId(String channelProductId) {
		this.channelProductId = channelProductId;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public List<String> getMor() {
		return mor;
	}

	public void setMor(List<String> mor) {
		this.mor = mor;
	}

	public List<String> getStates() {
		return states;
	}

	public void setStates(List<String> states) {
		this.states = states;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}
