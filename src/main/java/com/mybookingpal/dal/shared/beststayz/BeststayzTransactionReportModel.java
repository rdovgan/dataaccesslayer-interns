package com.mybookingpal.dal.shared.beststayz;

import java.util.Date;

public class BeststayzTransactionReportModel {

	private Integer reservationId;
	private String parentId;
	private String confirmationId;
	private Integer productId;
	private Date reservationDate;
	private Date arrivalDate;
	private Date departureDate;
	private String guestName;
	private Integer numberOfGuests;
	private String productName;
	private String reservationCurrency;
	private String productDisplayName;
	private Double reservationTotal;
	private String reservationStatus;
	private String channelAbbreviation;
	private String channelName;
	private Double amount;
	private String paymentInfo;
	private Double bpCommission;
	private Double cpCommission;
	private String partialIin;
	private Date paymentDate;
	private String type;

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public String getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(String confirmationId) {
		this.confirmationId = confirmationId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Date getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public Integer getNumberOfGuests() {
		return numberOfGuests;
	}

	public void setNumberOfGuests(Integer numberOfGuests) {
		this.numberOfGuests = numberOfGuests;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getReservationCurrency() {
		return reservationCurrency;
	}

	public void setReservationCurrency(String reservationCurrency) {
		this.reservationCurrency = reservationCurrency;
	}

	public String getProductDisplayName() {
		return productDisplayName;
	}

	public void setProductDisplayName(String productDisplayName) {
		this.productDisplayName = productDisplayName;
	}

	public Double getReservationTotal() {
		return reservationTotal;
	}

	public void setReservationTotal(Double quote) {
		this.reservationTotal = quote;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public String getChannelAbbreviation() {
		return channelAbbreviation;
	}

	public void setChannelAbbreviation(String channelAbbreviation) {
		this.channelAbbreviation = channelAbbreviation;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double finalAmount) {
		this.amount = finalAmount;
	}

	public String getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(String paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public Double getBpCommission() {
		return bpCommission;
	}

	public void setBpCommission(Double bpCommission) {
		this.bpCommission = bpCommission;
	}

	public Double getCpCommission() {
		return cpCommission;
	}

	public void setCpCommission(Double cpCommission) {
		this.cpCommission = cpCommission;
	}

	public String getPartialIin() {
		return partialIin;
	}

	public void setPartialIin(String partialIin) {
		this.partialIin = partialIin;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
}
