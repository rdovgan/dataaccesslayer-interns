package com.mybookingpal.dal.shared.beststayz;

public class BeststayzReservationByChannel {
	private Integer reservationId;
	private Integer channelId;
	private String abbreviation;

	public BeststayzReservationByChannel() {}

	public BeststayzReservationByChannel(Integer reservationId, Integer channelId, String abbreviation) {
		this.reservationId = reservationId;
		this.channelId = channelId;
		this.abbreviation = abbreviation;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
}
