package com.mybookingpal.dal.shared.beststayz;

import java.util.Date;
import java.util.List;

public class BeststayzTransactionReport {

	private String basedOn;

	private Date fromDate;

	private Date toDate;

	private List<String> channels;

	private String reservationStatus;

	private Integer propertyId;

	private String guestName;

	private Integer reservationId;

	private Integer homeownerId;

	private Boolean useDisplayName;

	private String report;

	public String getBasedOn() {
		return basedOn;
	}

	public void setBasedOn(String basedOn) {
		this.basedOn = basedOn;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<String> getChannels() {
		return channels;
	}

	public void setChannels(List<String> channels) {
		this.channels = channels;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public Integer getHomeownerId() {
		return homeownerId;
	}

	public void setHomeownerId(Integer homeownerId) {
		this.homeownerId = homeownerId;
	}

	public Boolean getUseDisplayName() {
		return useDisplayName;
	}

	public void setUseDisplayName(Boolean useDisplayName) {
		this.useDisplayName = useDisplayName;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}
}
