package com.mybookingpal.dal.shared;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.mybookingpal.utils.entity.NameId;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component("NameId")
@Scope(value = "prototype")
public class NameIdUtils extends com.mybookingpal.utils.entity.NameId {

	public static final String COMMA_DELIMITER = ", ";

	/**
	 * Rounds a double or floating point value to a specified number of decimal places. If the number of decimal places is negative, round to the left of the
	 * decimal place (eg: round(12345.67, -2) returns 12300.00)
	 *
	 * @param value the value to be rounded.
	 * @param places the number of decimal places to round to.
	 * @return value rounded to places decimal places.
	 */
	@Deprecated
	public static double round(double value, int places) {
		if (places < 0) {
			long factor = (long) Math.pow(10, 1 - places);
			long tmp;
			if (value < 0) {
				tmp = -Math.round(-value / factor);
			} else {
				tmp = Math.round(value / factor);
			}
			return (double) tmp * factor;
		} else {
			long factor = (long) Math.pow(10, places);
			value = value * factor;
			long tmp = Math.round(value);
			return (double) tmp / factor;
		}
	}

	@Deprecated
	public static double round(double value) {
		return round(value, 2);
	}

	/**
	 * Gets a comma delimited string from the specified list of string values without quotes.
	 * This is needed for proper work of Mail recipients.
	 *
	 * @param values the specified list of string values.
	 * @return the comma delimited string.
	 */
	public static String getCommaDelimitedString(String[] values) {
		if (values == null || values.length == 0) {
			return StringUtils.EMPTY;
		}
		return getCommaDelimitedString(Arrays.asList(values));
	}

	public static String getCommaDelimitedString(List<String> values) {
		if (CollectionUtils.isEmpty(values)) {
			return StringUtils.EMPTY;
		}
		return values.stream()
				.filter(StringUtils::isNotEmpty)
				.flatMap(s -> Arrays.stream(StringUtils.splitPreserveAllTokens(s, COMMA_DELIMITER)))
				.filter(StringUtils::isNotEmpty)
				.collect(Collectors.joining(COMMA_DELIMITER));
	}
	/**
	 * Gets an array of string values from the specified comma delimited string from the specified.
	 *
	 * @param value the specified comma delimited string.
	 * @return the array of string values.
	 */
	public static ArrayList<String> getCdlist(String value) {
		if (value == null || value.isEmpty()) {
			return null;
		}
		value.replace("'", "");
		String[] items = value.split(",");
		ArrayList<String> values = new ArrayList<String>();
		for (String item : items) {
			values.add(item);
		}
		return values;
	}

	/**
	 * Gets a comma delimited string from the specified list of string values.
	 *
	 * @param values the specified list of string values.
	 * @return the comma delimited string.
	 */
	public static String getCdlist(List<String> values) {
		if (values == null || values.isEmpty()) {
			return EMPTY_LIST;
		}
		StringBuilder sb = new StringBuilder();
		for (String item : values) {
			sb.append("'" + item + "',");
		}
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.length() - 1);
		} else {
			sb.append(EMPTY_LIST);
		}
		return sb.toString();
	}

	/**
	 * Gets a comma delimited string from the specified list of string values without quotes. This is needed for proper work of Mail recipients.
	 *
	 * @param values the specified list of string values.
	 * @return the comma delimited string.
	 */
	public static String getCdlistWithoutQuotes(String[] values) {
		if (values == null || values.length == 0) {
			return EMPTY_LIST;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < values.length; i++) {
			String value = values[i];
			sb.append(value);
			if (i < values.length - 1) {
				sb.append(",");
			}
		}
		return sb.toString();
	}

	/**
	 * Gets a comma delimited string from the specified map of string values.
	 *
	 * @param map the specified map of string values.
	 * @return the comma delimited string.
	 */
	public static String getCdlist(Map<String, List<String>> map) {
		if (map == null || map.isEmpty()) {
			return EMPTY_LIST;
		}
		StringBuilder sb = new StringBuilder();
		for (String key : map.keySet()) {
			for (String item : map.get(key)) {
				sb.append("'" + item + "',");
			}
		}
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.length() - 1);
		} else {
			sb.append(EMPTY_LIST);
		}
		return sb.toString();
	}

	/**
	 * Gets a list of name ID pairs from the specified name and id arrays.
	 *
	 * @param names array of names or labels.
	 * @param ids array of internal representations or IDs.
	 * @return the list of name ID pairs.
	 */
	public static ArrayList<com.mybookingpal.utils.entity.NameId> getList(String[] names, String[] ids) {
		ArrayList<com.mybookingpal.utils.entity.NameId> nameids = new ArrayList<com.mybookingpal.utils.entity.NameId>();
		for (int i = 0; i < ids.length; i++) {
			nameids.add(new com.mybookingpal.utils.entity.NameId(names[i], ids[i]));
		}
		return nameids;
	}

	/**
	 * @return text trimmed to length and/or str
	 * @param text to trim
	 * @param length in characters
	 */
	public static final String trim(String text, String str, int length) {
		return trim(trim(text, str), length);
	}

	/**
	 * Gets the specified text trimmed to length.
	 *
	 * @param text the specified text.
	 * @param length the trimmed length in characters.
	 * @return text trimmed to length.
	 */
	public static final String trim(String text, int length) {
		if (text == null || length == 0) {
			return null;
		} else if (text.length() <= length) {
			return text;
		} else {
			return text.substring(0, length - 1);
		}
	}

	/**
	 * Gets the specified text trimmed to the end text.
	 *
	 * @param text the specified text.
	 * @param end the end text.
	 * @return text trimmed to the end text.
	 */
	public static final String trim(String text, String end) {
		if (text == null) {
			return null;
		}
		if (end == null || end.isEmpty()) {
			return text;
		}
		int endIndex = text.indexOf(end);
		if (endIndex < 0)
			return text;
		else
			return text.substring(0, endIndex);
	}

	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String EMPTY_LIST = "'-1'";

	public enum Type {
		Account,
		Asset,
		Contract,
		Country,
		Currency,
		Design,
		Event,
		Feature,
		Finance,
		Language,
		Lease,
		License,
		Location,
		Mandatory,
		MandatoryPerDay,
		Optional,
		Partner,
		Party,
		Price,
		Product,
		Reservation,
		ResPassThr,
		Rule,
		Task,
		Tax,
		Yield,
		Payment,
		Modification
	}

	public static final String[] TYPES = { Type.Asset.name(), Type.Finance.name(), Type.Party.name(), Type.Product.name() };

	public NameIdUtils() {
	}

	public NameIdUtils(String id) {
		this.name = id;
		this.id = id;
	}

	public NameIdUtils(String name, String id) {
		this.name = name;
		this.id = id;
	}

	public String getName(int length) {
		return NameIdUtils.trim(name, length);
	}

	public boolean noName() {
		return name == null || name.trim().isEmpty();
	}

	public boolean hasName() {
		return !noName();
	}

	public boolean hasName(String nameToCheck, String name) {
		return hasName() && getName().equals(name);
	}

	public void setIds(ArrayList<String> ids) {
		id = NameIdUtils.getCdlist(ids);
	}

	public boolean noId() {
		return id == null || id.isEmpty() || id.equals(Model.ZERO);
	}

	public boolean hasId() {
		return !noId();
	}

	public boolean hasId(String id) {
		return id != null && this.id != null && this.id.equals(id);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NameId [name=");
		builder.append(name);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		NameId nameId = (NameId) o;
		return Objects.equals(name, nameId.getName()) && Objects.equals(id, nameId.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, id);
	}
}
