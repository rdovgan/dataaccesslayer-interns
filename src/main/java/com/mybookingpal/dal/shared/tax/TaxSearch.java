package com.mybookingpal.dal.shared.tax;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.shared.Tax;

@Component("TaxSearch")
@Scope(value = "prototype")
public class TaxSearch extends Tax {

    private List<String> productIds;
    private String searchName;
    private String secondNameSearchParameter;

    public TaxSearch () {

    }

    public TaxSearch(String searchName, String secondNameSearchParameter) {
        this.searchName = searchName;
        this.secondNameSearchParameter = secondNameSearchParameter;
    }

    public List<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public String getSecondNameSearchParameter() {
        return secondNameSearchParameter;
    }

    public void setSecondNameSearchParameter(String secondNameSearchParameter) {
        this.secondNameSearchParameter = secondNameSearchParameter;
    }
}
