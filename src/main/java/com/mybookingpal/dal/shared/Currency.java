package com.mybookingpal.dal.shared;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mybookingpal.utils.entity.NameId;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/** USD US dollar http://webservices.bermilabs.com/exchange/eur/usd
 * JPY Japanese yen http://webservices.bermilabs.com/exchange/eur/jpy
 * BGN Bulgarian lev http://webservices.bermilabs.com/exchange/eur/bgn
 * CZK Czech koruna http://webservices.bermilabs.com/exchange/eur/czk
 * DKK Danish krone http://webservices.bermilabs.com/exchange/eur/dkk
 * EEK Estonian kroon http://webservices.bermilabs.com/exchange/eur/eek
 * GBP Pound sterling http://webservices.bermilabs.com/exchange/eur/gbp
 * HUF Hungarian forint http://webservices.bermilabs.com/exchange/eur/huf
 * LTL Lithuanian litas http://webservices.bermilabs.com/exchange/eur/ltl
 * LVL Latvian lats http://webservices.bermilabs.com/exchange/eur/lvl
 * PLN Polish zloty http://webservices.bermilabs.com/exchange/eur/pln
 * RON New Romanian leu 1 http://webservices.bermilabs.com/exchange/eur/ron
 * SEK Swedish krona http://webservices.bermilabs.com/exchange/eur/sek
 * SKK Slovak koruna http://webservices.bermilabs.com/exchange/eur/skk
 * CHF Swiss franc http://webservices.bermilabs.com/exchange/eur/chf
 * ISK Icelandic krona http://webservices.bermilabs.com/exchange/eur/isk
 * NOK Norwegian krone http://webservices.bermilabs.com/exchange/eur/nok
 * HRK Croatian kuna http://webservices.bermilabs.com/exchange/eur/hrk
 * RUB Russian rouble http://webservices.bermilabs.com/exchange/eur/rub
 * TRY New Turkish lira 2 http://webservices.bermilabs.com/exchange/eur/try
 * AUD Australian dollar http://webservices.bermilabs.com/exchange/eur/aud
 * BRL Brasilian real http://webservices.bermilabs.com/exchange/eur/brl
 * CAD Canadian dollar http://webservices.bermilabs.com/exchange/eur/cad
 * CNY Chinese yuan renminbi http://webservices.bermilabs.com/exchange/eur/cny
 * HKD Hong Kong dollar http://webservices.bermilabs.com/exchange/eur/hkd
 * IDR Indonesian rupiah http://webservices.bermilabs.com/exchange/eur/idr
 * KRW South Korean won http://webservices.bermilabs.com/exchange/eur/krw
 * MXN Mexican peso http://webservices.bermilabs.com/exchange/eur/mxn
 * MYR Malaysian ringgit http://webservices.bermilabs.com/exchange/eur/myr
 * NZD New Zealand dollar http://webservices.bermilabs.com/exchange/eur/nzd
 * PHP Philippine peso http://webservices.bermilabs.com/exchange/eur/php
 * SGD Singapore dollar http://webservices.bermilabs.com/exchange/eur/sgd
 * THB Thai baht http://webservices.bermilabs.com/exchange/eur/thb
 * ZAR South African rand http://webservices.bermilabs.com/exchange/eur/zar
 * ILS Israel Shekel
 */

@Component
@Scope(value = "prototype")
public class Currency
extends Model {
	
	public enum Code {AUD,BGN,BRL,CAD,CHF,CNY,CZK,DKK,EUR,GBP,HKD,HRK,HUF,IDR,INR,ISK,JPY,
		KRW,LTL,LVL,MXN,MYR,NOK,NZD,PHP,PLN,RON,RUB,SEK,SGD,SKK,THB,TRY,USD,ZAR,ILS};

	public static final String[] EXCHANGE_CURRENCIES ={
		Code.AUD.name(), 
		Code.BGN.name(), 
		Code.BRL.name(), 
		Code.CAD.name(), 
		Code.CHF.name(), 
		Code.CNY.name(), 
		Code.CZK.name(), 
		Code.DKK.name(), 
		Code.EUR.name(), 
		Code.GBP.name(), 
		Code.HKD.name(), 
		Code.HRK.name(), 
		Code.HUF.name(), 
		Code.IDR.name(), 
		Code.INR.name(), 
		Code.ISK.name(), 
		Code.JPY.name(), 
		Code.KRW.name(), 
		Code.LTL.name(), 
		Code.LVL.name(), 
		Code.MXN.name(), 
		Code.MYR.name(), 
		Code.NOK.name(), 
		Code.NZD.name(), 
		Code.PHP.name(), 
		Code.PLN.name(), 
		Code.RON.name(), 
		Code.RUB.name(), 
		Code.SEK.name(), 
		Code.SGD.name(), 
		Code.SKK.name(), 
		Code.THB.name(), 
		Code.TRY.name(), 
		Code.USD.name(), 
		Code.ZAR.name(),
		Code.ILS.name()
	};
	
	private static final String[] EXCHANGE_CURRENCY_NAMES = {
			 "Australian Dollar",
			 "Bulgarian Lev",
			 "Brasilian Real",
			 "Canadian Dollar",
			 "Swiss Franc",
			 "Chinese Yuan Renminbi",
			 "Czech Koruna",
			 "Danish Krone",
			 "Euro",
			 "Pound Sterling",
			 "Hong Kong Dollar",
			 "Croatian Kuna",
			 "Hungarian Forint",
			 "Indonesian Rupiah",
			 "Indian Rupee",
			 "Icelandic Krona",
			 "Japanese Yen",
			 "South Korean Won",
			 "Lithuanian Litas",
			 "Latvian Lats",
			 "Mexican Peso",
			 "Malaysian Ringgit",
			 "Norwegian Krone",
			 "New Zealand Dollar",
			 "Philippine Peso",
			 "Polish Zloty",
			 "New Romanian Leu",
			 "Russian Rouble",
			 "Swedish Krona",
			 "Singapore Dollar",
			 "Slovak Koruna",
			 "Thai Baht",
			 "Turkish Lira",
			 "US Dollar",
			 "Rand",
			 "Israel Shekel"
	};

	private static final List<String> convertibleCurrencies = Arrays.asList(EXCHANGE_CURRENCIES);
	public static final boolean isConvertible(String currency){
		return convertibleCurrencies.contains(currency);
	}
	
	public static final ArrayList<NameId> getConvertibleCurrencyNameIds() {return NameIdUtils
			.getList(EXCHANGE_CURRENCY_NAMES, EXCHANGE_CURRENCIES);}

	public static final String[] PRICE_CURRENCIES ={Code.EUR.name(), Code.GBP.name(), Code.USD.name(), Code.JPY.name(), Code.ZAR.name()};

	public static final String CODE = "code";
	public static final String NAME = "name";
	
	private String number;
    private Short decimals;
    private Boolean convertible;
    private Boolean paypal;
    private Boolean jetpay;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Short getDecimals() {
		return decimals;
	}

	public void setDecimals(Short decimals) {
		this.decimals = decimals;
	}

	public Boolean getConvertible() {
		return convertible;
	}

	public void setConvertible(Boolean convertible) {
		this.convertible = convertible;
	}

	public Boolean getPaypal() {
		return paypal;
	}

	public void setPaypal(Boolean paypal) {
		this.paypal = paypal;
	}

	public Boolean getJetpay() {
		return jetpay;
	}

	public void setJetpay(Boolean jetpay) {
		this.jetpay = jetpay;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Currency [number=");
		builder.append(number);
		builder.append(", decimals=");
		builder.append(decimals);
		builder.append(", convertible=");
		builder.append(convertible);
		builder.append(", paypal=");
		builder.append(paypal);
		builder.append(", jetpay=");
		builder.append(jetpay);
		builder.append(", state=");
		builder.append(state);
		builder.append(", name=");
		builder.append(name);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}
}