package com.mybookingpal.dal.shared;

import java.util.ArrayList;
import java.util.List;

import com.mybookingpal.utils.entity.NameId;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class Mail {

	public static final String CONTENT_TYPE_TEXT_PLAIN = "text/plain";
	public static final String CONTENT_TYPE_TEXT_HTML = "text/html";
	public static final String EMPTY_LIST = "'-1'";
	public static final String CONTENT_TYPE_TEXT_HTML_CHARSET_UTF8 = "text/html; charset=utf-8";


	private String subject;
	private String content;
	private String recipients;
	private String ccRecipients;
	private String contentType;
	private int status;
	private static final String SENDER = "noreply@mybookingpal.com";

	public Mail() {
		super();
		contentType = Mail.CONTENT_TYPE_TEXT_PLAIN;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSender() {
		return SENDER;
	}

	public String getRecipients() {
		return recipients;
	}

	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}
	
	public void setRecipients(String... recipients) {
		this.recipients = NameIdUtils.getCdlistWithoutQuotes(recipients);
	}

	public void setRecipients(List<String> recipients) {
		this.recipients = getCdlistWithoutQuotes(recipients);
	}
	
	public String getCcRecipients() {
		return ccRecipients;
	}

	public void setCcRecipients(ArrayList<String> ccRecipients) {
		this.ccRecipients = getCdlistWithoutQuotes(ccRecipients);
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Mail [subject=");
		builder.append(subject);
		builder.append(", content=");
		builder.append(content);
		builder.append(", sender=");
		builder.append(getSender());
		builder.append(", recipients=");
		builder.append(recipients);
		builder.append(", status=");
		builder.append(status);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Gets a comma delimited string from the specified list of string values without quotes. This is needed for proper work of Mail recipients.
	 * 
	 * @param values the specified list of string values.
	 * @return the comma delimited string.
	 */
	public static String getCdlistWithoutQuotes(List<String> values) {
		if (values == null || values.isEmpty()) {
			return EMPTY_LIST;
		}
		StringBuilder sb = new StringBuilder();
		for (String item : values) {
			sb.append(item + ",");
		}
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.length() - 1);
		} else {
			sb.append(EMPTY_LIST);
		}
		return sb.toString();
	}
}
