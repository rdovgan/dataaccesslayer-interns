package com.mybookingpal.dal.shared;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Danijel Blagojevic
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductToLosPrice {

    private Integer productId;
    private boolean isSupported;
    private Date createdDate;
    private Date version;

}
