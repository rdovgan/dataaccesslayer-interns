package com.mybookingpal.dal.shared;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class CommissionSetting {

	private Long id;
	private Integer propertyManagerId;
	private Integer channelPartnerPartyId;
	private Integer productId;
	private Integer setting;
	private Integer type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPropertyManagerId() {
		return propertyManagerId;
	}

	public void setPropertyManagerId(Integer propertyManagerId) {
		this.propertyManagerId = propertyManagerId;
	}

	public Integer getChannelPartnerPartyId() {
		return channelPartnerPartyId;
	}

	public void setChannelPartnerPartyId(Integer channelPartnerPartyId) {
		this.channelPartnerPartyId = channelPartnerPartyId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getSetting() {
		return setting;
	}

	public void setSetting(Integer setting) {
		this.setting = setting;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}
