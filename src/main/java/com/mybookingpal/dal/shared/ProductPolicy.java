package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 * @author Danijel Blagojevic
 *
 */
@Component("ProductPolicy")
@Scope(value = "prototype")
public class ProductPolicy {
	Integer id;
	Integer policyId;
	Integer productId;
	String value;
	Date version;
	ProductPolicyState state;
	
	public enum ProductPolicyState {
		Created,
		Final;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPolicyId() {
		return policyId;
	}
	
	public void setPolicyId(Integer policyId) {
		this.policyId = policyId;
	}
	
	public Integer getProductId() {
		return productId;
	}
	
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public ProductPolicyState getProductPolicyState() {
		return state;
	}

	public void setProductPolicyState(ProductPolicyState productPolicyState) {
		this.state = productPolicyState;
	}

	public Date getVersion() {
		return version;
	}
	
	public void setVersion(Date version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + policyId;
		result = prime * result + productId;
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductPolicy other = (ProductPolicy) obj;
		if (policyId != other.policyId)
			return false;
		if (productId != other.productId)
			return false;
		if (state != other.state)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
}
