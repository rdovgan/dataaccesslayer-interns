package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.stereotype.Component;

/**
 * @author Danijel Blagojevic
 *
 */
@Component("ProductBedroomBed")
public class ProductBedroomBed {

	private Integer id;
	private Integer productId;
	private Integer bedroomId;
	private Integer bedCount;
	private String bedType;
	private Date version;

	public ProductBedroomBed() {
	}

	public ProductBedroomBed(Integer productId, Integer bedroomId, Integer bedCount, String bedType) {
		super();
		this.productId = productId;
		this.bedroomId = bedroomId;
		this.bedCount = bedCount;
		this.bedType = bedType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getBedroomId() {
		return bedroomId;
	}

	public void setBedroomId(Integer bedroomId) {
		this.bedroomId = bedroomId;
	}

	public Integer getBedCount() {
		return bedCount;
	}

	public void setBedCount(Integer bedCount) {
		this.bedCount = bedCount;
	}

	public String getBedType() {
		return bedType;
	}

	public void setBedType(String bedType) {
		this.bedType = bedType;
	}
	
	public Date getVersion() {
		return version;
	}
	
	public void setVersion(Date version) {
		this.version = version;
	}

}
