package com.mybookingpal.dal.shared;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

public class MarriottDiscountCode {

	public enum State {
		Created(1),
		Final(2);

		private Integer id;

		State(Integer id) {
			this.id = id;
		}

		public Integer getId() {
			return id;
		}

		public static State getByName(String name) {
			return Arrays.stream(values()).filter(e -> e.name().equalsIgnoreCase(name)).findFirst().orElse(null);
		}
	}

	public enum Type {
		Percentage(1);

		private Integer id;

		Type(Integer id) {
			this.id = id;
		}

		public Integer getId() {
			return id;
		}

		public static Type getById(Integer id) {
			return Arrays.stream(values()).filter(e -> e.getId().equals(id)).findFirst().orElse(null);
		}

		public static Type getByName(String name) {
			return Arrays.stream(values()).filter(e -> e.name().equalsIgnoreCase(name)).findFirst().orElse(null);
		}
	}

	private Integer id;
	private String discountCode;
	private String channelDiscountId;
	private Integer state;
	private Integer type;
	private Integer discountPolicy;
	private LocalDate startDate;
	private LocalDate endDate;
	private LocalDateTime createdDate;

	public Integer getDiscountPolicy() {
		return discountPolicy;
	}

	public void setDiscountPolicy(Integer discountPolicy) {
		this.discountPolicy = discountPolicy;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	public String getChannelDiscountId() {
		return channelDiscountId;
	}

	public void setChannelDiscountId(String channelDiscountId) {
		this.channelDiscountId = channelDiscountId;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
}
