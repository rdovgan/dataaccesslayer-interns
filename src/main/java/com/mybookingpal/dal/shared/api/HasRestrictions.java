package com.mybookingpal.dal.shared.api;

import com.mybookingpal.dal.shared.Restriction;

import java.util.List;

public interface HasRestrictions {

	List<Restriction> getRestrictions();
}
