package com.mybookingpal.dal.shared.api;


public interface HasProduct {

	IsProductWithTimeZone getProduct();

}
