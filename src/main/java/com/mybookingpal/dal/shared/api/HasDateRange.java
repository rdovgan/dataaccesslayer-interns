package com.mybookingpal.dal.shared.api;

import java.time.LocalDate;

public interface HasDateRange {

	LocalDate getFromDate();

	LocalDate getToDate();
}
