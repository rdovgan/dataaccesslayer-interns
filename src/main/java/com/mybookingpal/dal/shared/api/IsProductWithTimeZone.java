package com.mybookingpal.dal.shared.api;

import com.mybookingpal.dal.entity.IsProduct;
import com.mybookingpal.dal.shared.TimeZone;

import java.sql.Time;

/**
 * Used on refactoring step. Need to remove it in future.
 */
@Deprecated
public interface IsProductWithTimeZone extends IsProduct {

	TimeZone getTimeZone();

	Time getCheckInTime();

}
