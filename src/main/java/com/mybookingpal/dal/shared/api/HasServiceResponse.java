package com.mybookingpal.dal.shared.api;

public interface HasServiceResponse
extends HasService, HasResponse {
	String getId();
}
