package com.mybookingpal.dal.shared.api;

import com.mybookingpal.dal.shared.Product;

import java.time.LocalDate;
import java.util.Date;

public class HasProductAndDateRangeImpl implements HasProductAndDateRange {

	private final Product product;
	private final LocalDate fromDate;
	private final LocalDate toDate;

	public HasProductAndDateRangeImpl(Product product, LocalDate fromDate, LocalDate toDate) {
		this.product = product;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}

	@Override
	public LocalDate getFromDate() {
		return fromDate;
	}

	@Override
	public LocalDate getToDate() {
		return toDate;
	}

	@Override
	public Product getProduct() {
		return product;
	}
}
