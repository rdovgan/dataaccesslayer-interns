package com.mybookingpal.dal.shared.api;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.shared.Price;
import com.mybookingpal.dal.shared.Time;

public interface HasPrice {
	
	String getReservationid();
	Integer getGuestCount();
	
	String getAgentid();
	boolean noAgentid();

	void setCurrency(String currency);
	String getCurrency();
	boolean hasCurrency(String currency);
	
	void setNightlyrate(Double value);
	Double getNightlyrate();
	
	void setPrice(Double value);
	Double getPrice();
	boolean noPrice ();
	
	void setQuote(Double value);
	Double getQuote();
	
	void setExtra(Double value);
	Double getExtra();
	
	void setCost(Double value);
	Double getCost();
	
	void setPriceunit(Boolean total); // per unit = false, total = true
	Boolean getPriceunit();
	
	void setQuantity(Integer quantity);
	Integer getQuantity();
	
	void setSupplierid(String supplierid);
	String getSupplierid();
	void setProductid(String productId);
	String getProductid();
	String getUnit();

	LocalDate getFromDate();
	LocalDate getToDate();
	Double getDuration(Time unit);
	
	void setQuotedetail(List<Price> quotedetail);
	List<Price> getQuotedetail();
//	void addQuotedetail(Boolean id, String name, String type, Double value, String currency);
}
