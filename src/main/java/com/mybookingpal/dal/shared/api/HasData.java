package com.mybookingpal.dal.shared.api;

public interface HasData {
	String getOrganizationid();
	String getActorid();
	String getId();
	String getState();
}
