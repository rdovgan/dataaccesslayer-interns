package com.mybookingpal.dal.shared.api;

import java.util.List;

import com.mybookingpal.utils.entity.NameId;

public interface HasCollision extends HasProductIdAndDateRange {

	Integer getQuantity();

	void addCollisions(List<com.mybookingpal.utils.entity.NameId> collisions);

	void setCollisions(List<NameId> collisions);

	List<NameId> getCollisions();
}
