package com.mybookingpal.dal.shared.api;


public interface HasResponse { // extends IsSerializable Interface for GWT RPC
	void setStatus(int status);
	int getStatus();
}
