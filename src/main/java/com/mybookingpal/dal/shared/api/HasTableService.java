package com.mybookingpal.dal.shared.api;

public interface HasTableService
extends HasTable, HasService {
	String getId();
	void setId(String id);
}
