package com.mybookingpal.dal.shared.api;

public interface HasProductAndDateRange extends HasProduct, HasDateRange {
}
