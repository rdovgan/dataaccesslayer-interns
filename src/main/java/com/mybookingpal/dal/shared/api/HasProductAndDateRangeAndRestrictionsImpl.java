package com.mybookingpal.dal.shared.api;

import com.mybookingpal.dal.shared.Product;
import com.mybookingpal.dal.shared.Restriction;
import com.mybookingpal.dal.utils.CommonDateUtils;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class HasProductAndDateRangeAndRestrictionsImpl implements HasProductAndDateRangeAndRestrictions {
	final private LocalDate fromDate;
	final private LocalDate toDate;
	final private IsProductWithTimeZone product;
	final private List<Restriction> restrictions;

	public HasProductAndDateRangeAndRestrictionsImpl(LocalDate fromDate, LocalDate toDate, Product product, List<Restriction> restrictions) {
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.product = product;
		this.restrictions = restrictions;
	}
	public HasProductAndDateRangeAndRestrictionsImpl(Date fromDate, Date toDate, Product product, List<Restriction> restrictions) {
		this.fromDate = CommonDateUtils.toLocalDate(fromDate);
		this.toDate = CommonDateUtils.toLocalDate(toDate);
		this.product = product;
		this.restrictions = restrictions;
	}

	@Override
	public LocalDate getFromDate() {
		return fromDate;
	}

	@Override
	public LocalDate getToDate() {
		return toDate;
	}

	@Override
	public IsProductWithTimeZone getProduct() {
		return product;
	}

	@Override
	public List<Restriction> getRestrictions() {
		return restrictions;
	}
}
