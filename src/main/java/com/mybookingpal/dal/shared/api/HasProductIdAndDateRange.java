package com.mybookingpal.dal.shared.api;

public interface HasProductIdAndDateRange extends HasDateRange {

	String getId();

	Integer getProductId();

}
