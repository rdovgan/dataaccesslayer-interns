package com.mybookingpal.dal.shared.api;

import java.time.LocalDate;
import java.util.Objects;

public class HasProductIdAndDateRangeImpl implements HasProductIdAndDateRange {

	private Integer productid;
	private String id;
	private LocalDate fromdate;
	private LocalDate todate;

	public HasProductIdAndDateRangeImpl (Integer productId, LocalDate fromDate, LocalDate toDate) {
		this.productid = productId;
		this.fromdate = fromDate;
		this.todate = toDate;
	}

	public HasProductIdAndDateRangeImpl (Integer productId, String id, LocalDate fromDate, LocalDate toDate) {
		this.productid = productId;
		this.id = id;
		this.fromdate = fromDate;
		this.todate = toDate;
	}

	@Override
	public LocalDate getFromDate() {
		return fromdate;
	}

	@Override
	public LocalDate getToDate() {
		return todate;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public Integer getProductId() {
		return productid;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		HasProductIdAndDateRangeImpl that = (HasProductIdAndDateRangeImpl) o;
		return Objects.equals(productid, that.productid) && Objects.equals(id, that.id) && Objects.equals(fromdate, that.fromdate) && Objects
				.equals(todate, that.todate);
	}

	@Override
	public int hashCode() {
		return Objects.hash(productid, id, fromdate, todate);
	}
}

