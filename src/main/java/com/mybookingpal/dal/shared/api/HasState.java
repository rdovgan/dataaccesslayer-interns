package com.mybookingpal.dal.shared.api;

public interface HasState {
//	String INITIAL = "Initial";
//	String FINAL = "Final";
	void setState(String state);
	String getState();	
}