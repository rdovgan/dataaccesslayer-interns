package com.mybookingpal.dal.shared.api;

public interface HasXsl {
	String getXsl();
	void setXsl(String xsl);
}
