package com.mybookingpal.dal.shared.api;

public interface HasProductAndDateRangeAndRestrictions extends HasProductAndDateRange, HasRestrictions {
}
