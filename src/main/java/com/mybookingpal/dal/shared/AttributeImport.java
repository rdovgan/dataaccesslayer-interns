package com.mybookingpal.dal.shared;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Danijel Blagojevic
 *
 */
@Component
@Scope(value = "prototype")
public class AttributeImport {

	private String nameOrCode;
	private Integer quantity;

	public AttributeImport() {
		
	}

	public AttributeImport(String nameOrCode) {
		this.nameOrCode = nameOrCode;
		this.quantity = 1;
	} 
	
	public AttributeImport(String nameOrCode, Integer quantity) {
		this.nameOrCode = nameOrCode;
		this.quantity = quantity;
	}

	public String getNameOrCode() {
		return nameOrCode;
	}

	public void setNameOrCode(String nameOrCode) {
		this.nameOrCode = nameOrCode;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nameOrCode == null) ? 0 : nameOrCode.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttributeImport other = (AttributeImport) obj;
		if (nameOrCode == null) {
			if (other.nameOrCode != null)
				return false;
		} else if (!nameOrCode.equals(other.nameOrCode))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		return true;
	}

}
