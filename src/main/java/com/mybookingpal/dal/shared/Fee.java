package com.mybookingpal.dal.shared;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.entity.IsFee;
import com.mybookingpal.utils.entity.NameId;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Fee")
@Scope(value = "prototype")
@Primary
public class Fee implements IsFee, Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	public Fee() {
		super();
		// Default fee values.
		setEntityType(Fee.MANDATORY);
		setFeeType(Fee.GENERAL);
		setUnit(Fee.NOT_APPLICABLE);
		setValueType(Fee.FLAT);
		setWeight(0);
	}

	public Fee(Price price) {
		this();
		this.name = price.getName();
		this.currency = price.getCurrency();
		this.taxType = price.isTaxable() ? 1 : 2;
		this.value = price.getValue();
		this.valueType = 1;
		if (price.hasEntitytype(NameIdUtils.Type.MandatoryPerDay.name())) {
			this.unit = 2;
		} else {
			this.unit = 1;
		}
		if (price.getType() != null && price.getType().equalsIgnoreCase("Deposit")) {
			this.feeType = 3;
		}
		if (price.hasEntitytype(NameIdUtils.Type.Feature.name())) {
			this.entityType = 2;
		}
		this.fromDate = price.getFromDate();
		this.toDate = price.getToDate();
		this.version = price.getVersion();
		this.weight = 0;
		this.productId = price.getEntityid();
	}

	public final static int UNSUPPORTED = 0;

	//entity types
	public final static int MANDATORY = 1;
	public final static int OPTIONAL = 2;
	public final static int MANDATORY_PAL = 3;

	//fee type
	public final static int GENERAL = 1;
	public final static int PET_FEE = 2;
	public final static int DEPOSIT = 3;

	// states
	public final static int INITIAL = 1;
	public final static int CREATED = 2;
	public final static int FINAL = 3;

	//tax type
	public final static int TAXABLE = 1;
	public final static int NOT_TAXABLE = 2;

	//unit
	public final static int NOT_APPLICABLE = 1;
	public final static int PER_DAY = 2;
	public final static int PER_PERSON = 3;
	public final static int PER_DAY_PER_PERSON = 4;
	public final static int PER_PERSON_EXTRA = 5;
	public final static int PER_DAY_PER_PERSON_EXTRA = 6;

	//value types
	public final static int FLAT = 1;
	public final static int PERCENT = 2;

	private int id;
	private String altId;
	private Integer entityType;
	private Integer feeType;
	private String productId;
	private String partyId;
	private String name;
	private Integer state;
	private String optionValue;
	private Integer taxType;
	private LocalDate fromDate;
	private LocalDate toDate;
	private Integer unit;
	private Double value;
	private Integer valueType;
	private Integer weight;
	private String currency;
	private Date version;
	private Integer updateType;
	private Long ratePlanId;
	private List<String> productList;
	private Integer channelCode;

	public List<String> getProductList() {
		return productList;
	}

	public void setProductList(List<String> productList) {
		this.productList = productList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAltId() {
		return altId;
	}

	public void setAltId(String altId) {
		this.altId = altId;
	}

	public Integer getEntityType() {
		return entityType;
	}

	public boolean isTypeMandatory() {
		return this.getEntityType() == Fee.MANDATORY;
	}

	public boolean isTypeOptional() {
		return this.getEntityType() == Fee.OPTIONAL;
	}

	public void setEntityType(int entityType) {
		this.entityType = entityType;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getState() {
		return state;
	}

	public boolean isStateInitial() {
		return this.getState() == Fee.INITIAL;
	}

	public boolean isStateCreated() {
		return this.getState() == Fee.CREATED;
	}

	public boolean isStateFinal() {
		return this.getState() == Fee.FINAL;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getOptionValue() {
		return optionValue;
	}

	public void setOptionValue(String optionValue) {
		this.optionValue = optionValue;
	}

	public Integer getTaxType() {
		return taxType;
	}

	public boolean isTaxTypeTaxable() {
		return this.getTaxType() == Fee.TAXABLE;
	}

	public boolean isTaxTypeNotTaxable() {
		return this.getTaxType() == Fee.NOT_TAXABLE;
	}

	public void setTaxType(Integer taxType) {
		this.taxType = taxType;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public Integer getUnit() {
		return unit;
	}

	public boolean isUnitNotApplicable() {
		return this.getUnit() == Fee.NOT_APPLICABLE;
	}

	public boolean isUnitPerDay() {
		return this.getUnit() == Fee.PER_DAY;
	}

	public boolean isUnitPerPerson() {
		return this.getUnit() == Fee.PER_PERSON;
	}

	public boolean isUnitPerDayPerPerson() {
		return this.getUnit() == Fee.PER_DAY_PER_PERSON;
	}
	
	public boolean isUnitPerPersonExtra() {
		return this.getUnit() == Fee.PER_PERSON_EXTRA;
	}
	
	public boolean isUnitPerDayPersonExtra() {
		return this.getUnit() == Fee.PER_DAY_PER_PERSON_EXTRA;
	}
	
	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Integer getValueType() {
		return valueType;
	}

	public boolean isValueTypeFlat() {
		return this.getValueType() == null ? false : this.getValueType() == Fee.FLAT;
	}

	public boolean isValueTypePercent() {
		return this.getValueType() == null ? false : this.getValueType() == Fee.PERCENT;
	}

	public void setValueType(Integer valueType) {
		this.valueType = valueType;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public Integer getUpdateType() {
		return updateType;
	}

	public void setUpdateType(Integer updateType) {
		this.updateType = updateType;
	}

	public Integer getFeeType() {
		return feeType;
	}

	public void setFeeType(Integer feeType) {
		this.feeType = feeType;
	}
	
	public Long getRatePlanId() {
		return ratePlanId;
	}

	public void setRatePlanId(Long ratePlanId) {
		this.ratePlanId = ratePlanId;
	}

	public void setEntityType(Integer entityType) {
		this.entityType = entityType;
	}

	public Integer getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(Integer channelCode) {
		this.channelCode = channelCode;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Fee [id=");
		builder.append(id);
		builder.append(", altId=");
		builder.append(altId);
		builder.append(", entityType=");
		builder.append(entityType);
		builder.append(", feeType=");
		builder.append(feeType);
		builder.append(", productId=");
		builder.append(productId);
		builder.append(", partyId=");
		builder.append(partyId);
		builder.append(", name=");
		builder.append(name);
		builder.append(", state=");
		builder.append(state);
		builder.append(", optionValue=");
		builder.append(optionValue);
		builder.append(", taxType=");
		builder.append(taxType);
		builder.append(", fromDate=");
		builder.append(fromDate);
		builder.append(", toDate=");
		builder.append(toDate);
		builder.append(", unit=");
		builder.append(unit);
		builder.append(", value=");
		builder.append(value);
		builder.append(", valueType=");
		builder.append(valueType);
		builder.append(", weight=");
		builder.append(weight);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", version=");
		builder.append(version);
		builder.append(", updateType=");
		builder.append(updateType);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((altId == null) ? 0 : altId.hashCode());
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
		result = prime * result + ((feeType == null) ? 0 : feeType.hashCode());
		result = prime * result + ((fromDate == null) ? 0 : fromDate.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((optionValue == null) ? 0 : optionValue.hashCode());
		result = prime * result + ((partyId == null) ? 0 : partyId.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((taxType == null) ? 0 : taxType.hashCode());
		result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		long temp;
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((valueType == null) ? 0 : valueType.hashCode());
		result = prime * result + ((weight == null) ? 0 : weight.hashCode());
		result = prime * result + ((updateType == null) ? 0 : updateType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Fee))
			return false;
		Fee other = (Fee) obj;
		if (altId == null) {
			if (other.altId != null)
				return false;
		} else if (!altId.equals(other.altId))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (entityType == null) {
			if (other.entityType != null)
				return false;
		} else if (!entityType.equals(other.entityType))
			return false;
		if (feeType == null) {
			if (other.feeType != null)
				return false;
		} else if (!feeType.equals(other.feeType))
			return false;
		if (fromDate == null) {
			if (other.fromDate != null)
				return false;
		} else if (!fromDate.equals(other.fromDate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (optionValue == null) {
			if (other.optionValue != null)
				return false;
		} else if (!optionValue.equals(other.optionValue))
			return false;
		if (partyId == null) {
			if (other.partyId != null)
				return false;
		} else if (!partyId.equals(other.partyId))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (taxType == null) {
			if (other.taxType != null)
				return false;
		} else if (!taxType.equals(other.taxType))
			return false;
		if (toDate == null) {
			if (other.toDate != null)
				return false;
		} else if (!toDate.equals(other.toDate))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (Double.doubleToLongBits(value) != Double.doubleToLongBits(other.value))
			return false;
		if (valueType == null) {
			if (other.valueType != null)
				return false;
		} else if (!valueType.equals(other.valueType))
			return false;
		if (weight == null) {
			if (other.weight != null)
				return false;
		} else if (!weight.equals(other.weight))
			return false;
		if (updateType == null) {
			if (other.updateType != null)
				return false;
		} else if (!updateType.equals(other.updateType))
			return false;
		return true;
	}

	public boolean equalsWithoutDates(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Fee))
			return false;
		Fee other = (Fee) obj;
		if (altId == null) {
			if (other.altId != null)
				return false;
		} else if (!altId.equals(other.altId))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (entityType == null) {
			if (other.entityType != null)
				return false;
		} else if (!entityType.equals(other.entityType))
			return false;
		if (feeType == null) {
			if (other.feeType != null)
				return false;
		} else if (!feeType.equals(other.feeType))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (optionValue == null) {
			if (other.optionValue != null)
				return false;
		} else if (!optionValue.equals(other.optionValue))
			return false;
		if (partyId == null) {
			if (other.partyId != null)
				return false;
		} else if (!partyId.equals(other.partyId))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (taxType == null) {
			if (other.taxType != null)
				return false;
		} else if (!taxType.equals(other.taxType))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (Double.doubleToLongBits(value) != Double.doubleToLongBits(other.value))
			return false;
		if (valueType == null) {
			if (other.valueType != null)
				return false;
		} else if (!valueType.equals(other.valueType))
			return false;
		if (weight == null) {
			if (other.weight != null)
				return false;
		} else if (!weight.equals(other.weight))
			return false;
		if (updateType == null) {
			if (other.updateType != null)
				return false;
		} else if (!updateType.equals(other.updateType))
			return false;
		return true;
	}

	public boolean equalsWithoutDatesAndValue(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Fee))
			return false;
		Fee other = (Fee) obj;
		if (altId == null) {
			if (other.altId != null)
				return false;
		} else if (!altId.equals(other.altId))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (entityType == null) {
			if (other.entityType != null)
				return false;
		} else if (!entityType.equals(other.entityType))
			return false;
		if (feeType == null) {
			if (other.feeType != null)
				return false;
		} else if (!feeType.equals(other.feeType))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (optionValue == null) {
			if (other.optionValue != null)
				return false;
		} else if (!optionValue.equals(other.optionValue))
			return false;
		if (partyId == null) {
			if (other.partyId != null)
				return false;
		} else if (!partyId.equals(other.partyId))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (taxType == null) {
			if (other.taxType != null)
				return false;
		} else if (!taxType.equals(other.taxType))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (valueType == null) {
			if (other.valueType != null)
				return false;
		} else if (!valueType.equals(other.valueType))
			return false;
		if (weight == null) {
			if (other.weight != null)
				return false;
		} else if (!weight.equals(other.weight))
			return false;
		if (updateType == null) {
			if (other.updateType != null)
				return false;
		} else if (!updateType.equals(other.updateType))
			return false;
		return true;
	}

	@Override
	public Fee clone() {
		try {
			return (Fee) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
}