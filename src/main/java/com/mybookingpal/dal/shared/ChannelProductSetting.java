package com.mybookingpal.dal.shared;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChannelProductSetting {
	private Integer productId;
	private Integer channelId;
	private Integer type;
	private String value;
	private Date createdDate;
	private Date version;

	public ChannelProductSetting(Integer productId, Integer channelId, Integer type) {
		this.productId = productId;
		this.channelId = channelId;
		this.type = type;
	}
}
