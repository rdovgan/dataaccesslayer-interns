package com.mybookingpal.dal.shared;

import java.util.Date;

import javax.xml.bind.annotation.XmlTransient;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class Process extends Model {
	public static final String STATE = "state";

	protected String parentid;
    protected String locationid;
	protected String parentname;
	protected String actorname;
	protected String process;
	protected String activity;
    protected String notes;
	protected Date duedate;
    protected Date date;
    protected Date donedate;

	public String getResetid() {
		if (hasProcess(Task.Type.Service.name())) {return parentid;}
		else if (hasProcess(Task.Type.Maintenance.name())) {return parentid;}
		else {return id;}
	}

	@XmlTransient
	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	@XmlTransient
	public String getLocationid() {
		return locationid;
	}

	public void setLocationid(String locationid) {
		this.locationid = locationid;
	}

	@XmlTransient
	public String getParentname() {
		return parentname;
	}

	public void setParentname(String parentname) {
		this.parentname = parentname;
	}

	public String getActorname() {
		return actorname;
	}

	public void setActorname(String actor) {
		this.actorname = actor;
	}

	@XmlTransient
	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public boolean hasProcess(String process) {
		return this.process != null && process != null && this.process.equalsIgnoreCase(process);
	}

	@XmlTransient
	public String getActivity() {
		return activity;
	}

	public void setActivity(String parenttype) {
		this.activity = parenttype;
	}

	public String getNotes() {
		return notes;
	}

	public String getNotes(int length) {
		return NameIdUtils.trim(notes, length);
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDuedate() {
		return duedate;
	}

	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}

	public Date getDonedate() {
		return donedate;
	}

	public void setDonedate(Date donedate) {
		this.donedate = donedate;
	}
	
	public void setDone() {
		this.donedate = new Date();
	}

	public boolean isDone() {
		return (donedate != null);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Process [parentid=");
		builder.append(parentid);
		builder.append(", actorid=");
		builder.append(actorid);
		builder.append(", locationid=");
		builder.append(locationid);
		builder.append(", parentname=");
		builder.append(parentname);
		builder.append(", actorname=");
		builder.append(actorname);
		builder.append(", process=");
		builder.append(process);
		builder.append(", activity=");
		builder.append(activity);
		builder.append(", notes=");
		builder.append(notes);
		builder.append(", duedate=");
		builder.append(duedate);
		builder.append(", date=");
		builder.append(date);
		builder.append(", donedate=");
		builder.append(donedate);
		builder.append(", organizationid=");
		builder.append(organizationid);
		builder.append(", status=");
		builder.append(status);
		builder.append(", state=");
		builder.append(state);
		builder.append(", values=");
		builder.append(values);
		builder.append(", attributes=");
		builder.append(attributemap);
		builder.append(", texts=");
		builder.append(texts);
		builder.append(", name=");
		builder.append(name);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
