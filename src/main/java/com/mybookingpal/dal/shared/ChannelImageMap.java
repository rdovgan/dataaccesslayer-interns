package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ChannelImageMap {
	
	private Integer id;
	private String channelImageId;
	private String channelId;
	private String channelProductId;
	private String channelRoomId;
	private String productId;
	private Integer imageId;
	private Boolean status;
	private Date version;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getChannelImageId() {
		return channelImageId;
	}
	public void setChannelImageId(String channelImageId) {
		this.channelImageId = channelImageId;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getChannelProductId() {
		return channelProductId;
	}
	public void setChannelProductId(String channelProductId) {
		this.channelProductId = channelProductId;
	}
	public String getChannelRoomId() {
		return channelRoomId;
	}
	public void setChannelRoomId(String channelRoomId) {
		this.channelRoomId = channelRoomId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public Integer getImageId() {
		return imageId;
	}
	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Date getVersion() {
		return version;
	}
	public void setVersion(Date version) {
		this.version = version;
	}
	
	
	
	
}