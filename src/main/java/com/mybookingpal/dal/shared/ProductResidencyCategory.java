package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.server.util.enums.BookingPalEnums;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Getter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Component
@Scope(value = "prototype")
public class ProductResidencyCategory {

	private Integer productId;
	private BookingPalEnums.ResidencyCategoryEnum type;
	private Integer managedBy;
	private Date createdDate;
	private Date version;
	
}
