package com.mybookingpal.dal.shared;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Component
@Scope(value = "prototype")
public class ChannelOpportunity {

	public enum ImplementationType {
		TOGGLE("TOGGLE"), REDIRECT("REDIRECT");

		private String value;

		ImplementationType(String value) {
			this.value = value;
		}

		public String getValue() {return value;}
	}

	private String id;
	private Integer channelId;
	private String altId;
	private String title;
	private String description;
	private String implementationType;
	private String instruction;
	private String category;
	private String callToAction;
	private Date createdDate;
	private Date version;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public String getAltId() {
		return altId;
	}

	public void setAltId(String altId) {
		this.altId = altId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImplementationType() {
		return implementationType;
	}

	public void setImplementationType(String implementationType) {
		this.implementationType = implementationType;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public String getCallToAction() {
		return callToAction;
	}

	public void setCallToAction(String callToAction) {
		this.callToAction = callToAction;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof ChannelOpportunity))
			return false;
		ChannelOpportunity that = (ChannelOpportunity) o;
		return Objects.equals(getChannelId(), that.getChannelId()) && Objects.equals(getAltId(), that.getAltId()) && Objects.equals(getTitle(), that.getTitle())
				&& Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getImplementationType(), that.getImplementationType()) && Objects
				.equals(getInstruction(), that.getInstruction()) && Objects.equals(getCallToAction(), that.getCallToAction());
	}

	@Override public int hashCode() {
		return Objects.hash(getChannelId(), getAltId(), getTitle(), getDescription(), getImplementationType(), getInstruction(), getCallToAction());
	}

	@Override
	public String toString() {
		return "ChannelOpportunity{" +
				"id='" +
				id +
				'\'' +
				", channelId=" +
				channelId +
				", altId='" +
				altId +
				'\'' +
				", title='" +
				title +
				'\'' +
				", description='" +
				description +
				'\'' +
				", implementationType='" +
				implementationType +
				'\'' +
				", instruction='" +
				instruction +
				'\'' +
				", callToAction='" +
				callToAction +
				'\'' +
				", createdDate=" +
				createdDate +
				", version=" +
				version +
				'}';
	}
}
