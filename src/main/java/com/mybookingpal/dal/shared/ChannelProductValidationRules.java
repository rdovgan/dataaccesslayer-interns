package com.mybookingpal.dal.shared;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ChannelProductValidationRules {

    private int channelId;

    private int type;

    private String value;

    private LocalDateTime createdDate;

    private LocalDateTime version;

}
