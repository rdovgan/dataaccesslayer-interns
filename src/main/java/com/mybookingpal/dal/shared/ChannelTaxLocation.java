package com.mybookingpal.dal.shared;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class ChannelTaxLocation {
	private Integer id;
	private Integer channelID;
	private Integer locationID;
	private Integer taxID1;
	private Integer taxID2;
	private Integer taxID3;
	private Integer taxID4;
	private Integer taxID5;
	private String zipCode9;
	private String state;

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getChannelID() {
		return channelID;
	}
	public void setChannelID(Integer channelID) {
		this.channelID = channelID;
	}
	public Integer getLocationID() {
		return locationID;
	}
	public void setLocationID(Integer locationID) {
		this.locationID = locationID;
	}
	public Integer getTaxID1() {
		return taxID1;
	}
	public void setTaxID1(Integer taxID1) {
		this.taxID1 = taxID1;
	}
	public Integer getTaxID2() {
		return taxID2;
	}
	public void setTaxID2(Integer taxID2) {
		this.taxID2 = taxID2;
	}
	public Integer getTaxID3() {
		return taxID3;
	}
	public void setTaxID3(Integer taxID3) {
		this.taxID3 = taxID3;
	}
	public Integer getTaxID4() {
		return taxID4;
	}
	public void setTaxID4(Integer taxID4) {
		this.taxID4 = taxID4;
	}
	public Integer getTaxID5() {
		return taxID5;
	}
	public void setTaxID5(Integer taxID5) {
		this.taxID5 = taxID5;
	}
	public String getZipCode9() {
		return zipCode9;
	}
	public void setZipCode9(String zipCode9) {
		this.zipCode9 = zipCode9;
	}
	
	
}
