package com.mybookingpal.dal.shared;

import com.mybookingpal.dal.utils.BigDecimalExt;

import java.util.Date;

public class ChannelReservationTransaction {

    private Integer id;
    private Integer reservationId;
    private Integer channelPartnerId;
    private String confirmationId;
    private BigDecimalExt quote;
    private BigDecimalExt npnr;
    private BigDecimalExt taxes;
    private BigDecimalExt fees;
    private BigDecimalExt commissions;
    private String currency;
    private String state;
    private Date created;
    private Date version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReservationId() {
        return reservationId;
    }

    public void setReservationId(Integer reservationId) {
        this.reservationId = reservationId;
    }

    public Integer getChannelPartnerId() {
        return channelPartnerId;
    }

    public void setChannelPartnerId(Integer channelPartnerId) {
        this.channelPartnerId = channelPartnerId;
    }

    public String getConfirmationId() {
        return confirmationId;
    }

    public void setConfirmationId(String confirmationId) {
        this.confirmationId = confirmationId;
    }

    public BigDecimalExt getQuote() {
        return quote;
    }

    public void setQuote(BigDecimalExt quote) {
        this.quote = quote;
    }

    public BigDecimalExt getNpnr() {
        return npnr;
    }

    public void setNpnr(BigDecimalExt npnr) {
        this.npnr = npnr;
    }

    public BigDecimalExt getTaxes() {
        return taxes;
    }

    public void setTaxes(BigDecimalExt taxes) {
        this.taxes = taxes;
    }

    public BigDecimalExt getFees() {
        return fees;
    }

    public void setFees(BigDecimalExt fees) {
        this.fees = fees;
    }

    public BigDecimalExt getCommissions() {
        return commissions;
    }

    public void setCommissions(BigDecimalExt commissions) {
        this.commissions = commissions;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getVersion() {
        return version;
    }

    public void setVersion(Date version) {
        this.version = version;
    }

}
