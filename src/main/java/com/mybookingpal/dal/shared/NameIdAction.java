package com.mybookingpal.dal.shared;

import java.util.Date;
import java.util.List;

import com.mybookingpal.utils.entity.NameId;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("NameIdAction")
@Scope(value = "prototype")
public class NameIdAction {

	private boolean suggested;
	private int numrows = Integer.MAX_VALUE;
	private int offsetrows = 0;
	protected String organizationid;
	protected String id;
	protected String name = Model.BLANK;
	protected String state;
	protected String type;
	protected String ids;
	protected String productids;
	protected Date version;
	protected Double rank;
	protected String supplierid;
	protected String parentId;
	protected List<String> rootIds;
	protected int channelId;

	public NameIdAction() {
	}

	public NameIdAction(String ids, int numrows, int offsetrows) {
		this.numrows = numrows;
		this.offsetrows = offsetrows;
		this.ids = ids;
	}

	public NameIdAction(String type) {
		this.type = type;
	}

	public void setRootIds(List<String> rootIds) {
		// TODO Auto-generated method stub
		this.rootIds = rootIds;
	}

	public List<String> getRootIds() {
		return rootIds;
	}

	public String getOrganizationid() {
		return organizationid;
	}

	public void setOrganizationid(String organizationid) {
		this.organizationid = organizationid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean noType() {
		return type == null || type.isEmpty();
	}

	public boolean hasType() {
		return !noType();
	}

	public String getWildtype() {
		return '%' + type + '%';
	}

	public void setIds(List<String> ids) {
		this.ids = (ids == null || ids.isEmpty()) ? null : NameIdUtils.getCdlist(ids);
	}

	public String getIds() {
		return ids;
	}

	public String getProductids() {
		return productids;
	}

	public void setProductids(List<String> productids) {
		this.productids = (productids == null || productids.isEmpty()) ? null : NameIdUtils.getCdlist(productids);

	}

	public boolean isSuggested() {
		return suggested;
	}

	public void setSuggested(boolean suggested) {
		this.suggested = suggested;
	}

	public int getNumrows() {
		return numrows;
	}

	public void setNumrows(int numrows) {
		this.numrows = numrows;
	}

	public int getOffsetrows() {
		return offsetrows;
	}

	public void setOffsetrows(int offsetrows) {
		this.offsetrows = offsetrows;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public Double getRank() {
		return rank;
	}

	public void setRank(Double rank) {
		this.rank = rank;
	}

	public String getSupplierid() {
		return supplierid;
	}

	public void setSupplierid(String supplierid) {
		this.supplierid = supplierid;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NameIdAction [service=");

		builder.append(", suggested=");
		builder.append(suggested);
		builder.append(", numrows=");
		builder.append(numrows);
		builder.append(", offsetrows=");
		builder.append(offsetrows);
		builder.append(", organizationid=");
		builder.append(organizationid);
		builder.append(", id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", state=");
		builder.append(state);
		builder.append(", type=");
		builder.append(type);
		builder.append(", ids=");
		builder.append(ids);
		builder.append(", version=");
		builder.append(version);
		builder.append(", rank=");
		builder.append(rank);
		builder.append(", service()=");

		builder.append("]");
		return builder.toString();
	}
}