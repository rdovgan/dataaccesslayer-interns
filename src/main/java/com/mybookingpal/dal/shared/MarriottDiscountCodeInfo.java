package com.mybookingpal.dal.shared;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class MarriottDiscountCodeInfo {

	private Integer id;
	private String discountCode;
	private String channelDiscountId;
	private Integer state;
	private Integer type;
	private LocalDate startDate;
	private LocalDate endDate;
	private LocalDateTime createdDate;
	private List<MarriottDiscountMap> discounts;
	private List<MarriottDiscountHmcMap> hmcs;
	private List<MarriottBlackoutDatesMap> blackoutDates;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	public String getChannelDiscountId() {
		return channelDiscountId;
	}

	public void setChannelDiscountId(String channelDiscountId) {
		this.channelDiscountId = channelDiscountId;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public List<MarriottDiscountMap> getDiscounts() {
		return discounts;
	}

	public void setDiscounts(List<MarriottDiscountMap> discounts) {
		this.discounts = discounts;
	}

	public List<MarriottDiscountHmcMap> getHmcs() {
		return hmcs;
	}

	public void setHmcs(List<MarriottDiscountHmcMap> hmcs) {
		this.hmcs = hmcs;
	}

	public List<MarriottBlackoutDatesMap> getBlackoutDates() {
		return blackoutDates;
	}

	public void setBlackoutDates(List<MarriottBlackoutDatesMap> blackoutDates) {
		this.blackoutDates = blackoutDates;
	}
}
