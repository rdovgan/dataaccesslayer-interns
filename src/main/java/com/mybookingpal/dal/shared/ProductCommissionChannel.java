package com.mybookingpal.dal.shared;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ProductCommissionChannel {
	
	public enum StatusEnum {
		INACTIVE(0),
		ACTIVE(1);

		Integer status;
		StatusEnum(Integer statusValue) {
			status = statusValue;
		}

		public Integer getIntegerValue(){
			return status;
		}

		public static StatusEnum getByInt(Integer value) {
			for (StatusEnum v : values()) {
				if (v.status.equals(value)) {
					return v;
				}
			}
			return null;
		}
	}

	private Integer productId;
	private Integer channelPartnerId;
	private Double commissionDelta;
	private Integer status;
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getChannelPartnerId() {
		return channelPartnerId;
	}
	public void setChannelPartnerId(Integer channelPartnerId) {
		this.channelPartnerId = channelPartnerId;
	}
	public Double getCommissionDelta() {
		return commissionDelta;
	}
	public void setCommissionDelta(Double commissionDelta) {
		this.commissionDelta = commissionDelta;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
