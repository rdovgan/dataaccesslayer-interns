package com.mybookingpal.dal.shared;

public class MarriottDiscountMap {

	private Integer discountCodeId;
	private Double minAdr;
	private Double maxAdr;
	private Double discount;
	private Double channelContribution;
	private Double pmContribution;

	public MarriottDiscountMap() {
	}

	public MarriottDiscountMap(Double minAdr, Double maxAdr, Double discount, Double channelContribution, Double pmContribution) {
		this.minAdr = minAdr;
		this.maxAdr = maxAdr;
		this.discount = discount;
		this.channelContribution = channelContribution;
		this.pmContribution = pmContribution;
	}

	public Double getPmContribution() {
		return pmContribution;
	}

	public void setPmContribution(Double pmContribution) {
		this.pmContribution = pmContribution;
	}

	public Integer getDiscountCodeId() {
		return discountCodeId;
	}

	public void setDiscountCodeId(Integer discountCodeId) {
		this.discountCodeId = discountCodeId;
	}

	public Double getMinAdr() {
		return minAdr;
	}

	public void setMinAdr(Double minAdr) {
		this.minAdr = minAdr;
	}

	public Double getMaxAdr() {
		return maxAdr;
	}

	public void setMaxAdr(Double maxAdr) {
		this.maxAdr = maxAdr;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getChannelContribution() {
		return channelContribution;
	}

	public void setChannelContribution(Double channelContribution) {
		this.channelContribution = channelContribution;
	}
}
