package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Danijel Blagojevic
 *
 */
@Component
@Scope(value = "prototype")
public class FeeTaxRelation {

	public enum StateFeeTaxEnum {
		CREATED(2),
		FINAL(3);

		private Integer value;

		StateFeeTaxEnum(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return this.value;
		}

		public static StateFeeTaxEnum getByInt(int value) {
			for (StateFeeTaxEnum v : values()) {
				if (v.value.equals(value)) {
					return v;
				}
			}
			return CREATED;
		}

		public static String getNameByInt(Integer value) {
			return getByInt(value).name();
		}
	}

	private Integer id;
	private Integer feeId;
	private Integer taxId;
	private StateFeeTaxEnum state;
	private Date version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFeeId() {
		return feeId;
	}

	public void setFeeId(Integer feeId) {
		this.feeId = feeId;
	}

	public Integer getTaxId() {
		return taxId;
	}

	public void setTaxId(Integer taxId) {
		this.taxId = taxId;
	}

	public StateFeeTaxEnum getState() {
		return state;
	}

	public void setState(StateFeeTaxEnum state) {
		this.state = state;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((feeId == null) ? 0 : feeId.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((taxId == null) ? 0 : taxId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FeeTaxRelation other = (FeeTaxRelation) obj;
		if (feeId == null) {
			if (other.feeId != null)
				return false;
		} else if (!feeId.equals(other.feeId))
			return false;
		if (state != other.state)
			return false;
		if (taxId == null) {
			if (other.taxId != null)
				return false;
		} else if (!taxId.equals(other.taxId))
			return false;
		return true;
	}

}
