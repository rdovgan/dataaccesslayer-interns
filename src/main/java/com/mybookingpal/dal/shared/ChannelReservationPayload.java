package com.mybookingpal.dal.shared;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mybookingpal.dal.utils.BigDecimalExt;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ChannelReservationPayload {

	private Integer id;
	private Integer reservationId;
	@XmlElement(name = "channel_partner_id")
	@JsonProperty("channel_partner_id")
	private Integer channelPartnerId;
	private String confirmationId;
	private String payload;
	private BigDecimalExt quote;
	private BigDecimalExt npnr;
	private BigDecimalExt taxes;
	private BigDecimalExt fees;
	private BigDecimalExt yields;
	private BigDecimalExt commissions;
	private Date version;
	private String currency;
	private Integer channelId;

	public Integer getChannelPartnerId() {
		return channelPartnerId;
	}

	public void setChannelPartnerId(Integer channelPartnerId) {
		this.channelPartnerId = channelPartnerId;
	}
	
	
	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public String getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(String confirmationId) {
		this.confirmationId = confirmationId;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public BigDecimalExt getQuote() {
		return quote;
	}

	public void setQuote(BigDecimalExt quote) {
		this.quote = quote;
	}

	public BigDecimalExt getNpnr() {
		return npnr;
	}

	public void setNpnr(BigDecimalExt npnr) {
		this.npnr = npnr;
	}

	public BigDecimalExt getTaxes() {
		return taxes;
	}

	public void setTaxes(BigDecimalExt taxes) {
		this.taxes = taxes;
	}

	public BigDecimalExt getFees() {
		return fees;
	}

	public void setFees(BigDecimalExt fees) {
		this.fees = fees;
	}

	public BigDecimalExt getYields() {
		return yields;
	}

	public void setYields(BigDecimalExt yields) {
		this.yields = yields;
	}

	public BigDecimalExt getCommissions() {
		return commissions;
	}

	public void setCommissions(BigDecimalExt commissions) {
		this.commissions = commissions;
	}

}
