package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope(value = "prototype")
public class ChannelJobStatus {
	
	public enum JobSourceEnum {
		UI("UI"),
		SERVER("Server"),
		DELTA("Delta"),
		PMS_TRIGGER("PMS Trigger"),
		UI_TRIGGER("UI Trigger"),
		NOTIFICATION("Notification");
		
		private String value;

		JobSourceEnum(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static JobSourceEnum getByStr(String value) {
			for (JobSourceEnum v : values()) {
				if (v.value.equalsIgnoreCase(value)) {
					return v;
				}
			}
			return null;
		}

		public static String getNameByStr(String value) {
			return getByStr(value).name();
		}
	}

	private Integer id;
	private String channelId;
	private String supplierId;
	private String productId;
	private String jobName;
	private String jobStep;
	private JobSourceEnum jobSource;
	private String status;
	private Date createdDate;
	private Date updatedDate;
	private Date version;
	private String error;
	private Date startDate;
	private Date endDate;
	
	
	
	public Date getVersion() {
		return version;
	}
	public void setVersion(Date version) {
		this.version = version;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	
	public JobSourceEnum getJobSource() {
		return jobSource;
	}

	public void setJobSource(JobSourceEnum jobSource) {
		this.jobSource = jobSource;
	}

	public String getJobStep() {
		return jobStep;
	}
	public void setJobStep(String jobStep) {
		this.jobStep = jobStep;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	
}
