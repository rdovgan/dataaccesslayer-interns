package com.mybookingpal.dal.shared;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ChannelProductMap {

	public enum ChannelStateEnum {
		CHANNEL_STATE_INACTIVE(0),
		CHANNEL_STATE_ACTIVE(1),
		CHANNEL_STATE_SUSPENDED(2),
		CHANNEL_STATE_REMOVED(3),
		CHANNEL_STATE_REJECTED(4),
		CHANNEL_STATE_SWITCHOVER(5),
		CHANNEL_STATE_REMOVED_HA(6),
		CHANNEL_STATE_AUTHENTICATION_FAILED(7);

		Integer channelState;

		ChannelStateEnum(Integer channelStateValue) {
			channelState = channelStateValue;
		}

		public Integer getIntegerValue() {
			return channelState;
		}

		public static ChannelStateEnum getByInt(Integer value) {
			for (ChannelStateEnum v : values()) {
				if (v.channelState.equals(value)) {
					return v;
				}
			}
			return null;
		}
	}

	public enum BuildTypeEnum {
		A("A"), // API
		S("S"), // Switchover
		B("B"); // Bulk

		private String value;

		BuildTypeEnum(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static BuildTypeEnum getByStr(String value) {
			for (BuildTypeEnum v : values()) {
				if (v.value.equals(value)) {
					return v;
				}
			}
			return null;
		}

		public static String getNameByStr(String value) {
			return getByStr(value).name();
		}
	}

	public enum PortalState {
		New(0),
		Pending(1),
		Approved(2),
		Listed(3),
		Delisted(4),
		TemporarilyRejected(5),
		PermanentlyRejected(6),
		NotDestributed(7);

		private Integer id;

		PortalState(Integer id) {
			this.id = id;
		}

		public Integer getId() {
			return id;
		}
	}

	private Long id;
	private String productId;
	private String channelProductId;
	private String channelRoomId;
	private Integer channelId;
	private String channelRateId;
	private Integer offsetrows;
	private Integer numrows;
	private BuildTypeEnum buildType;
	private String reviewStatus;
	private String reviewResult;
	private String ratePlanType;
	private boolean losPriceEnable;
	private Integer portalState;
	private Integer portalPriorState;
	private LocalDateTime portalApprovedDate;
	private LocalDateTime portalRejectedDate;
	private String portalRejectedReason;
	private String channelPropertyType;
	private Integer syncType;
	private Date created;
	private Date version;


	// CS: list of product ids
	// used in the in clause of the query
	private List<String> list;
	private Boolean state = true;
	private ChannelStateEnum channelState = ChannelStateEnum.CHANNEL_STATE_INACTIVE;// 0-Inactive 1-Active 2-Suspended(availability is blocked but still listed)

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	public String getProductId() {
		return productId;
	}

	public void setProductId(String bpProductId) {
		this.productId = bpProductId;
	}

	public String getChannelProductId() {
		return channelProductId;
	}

	public void setChannelProductId(String channelProductId) {
		this.channelProductId = channelProductId;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getChannelRoomId() {
		return channelRoomId;
	}

	public void setChannelRoomId(String channelRoomId) {
		this.channelRoomId = channelRoomId;
	}

	public String getChannelRateId() {
		return channelRateId;
	}

	public void setChannelRateId(String channelRateId) {
		this.channelRateId = channelRateId;
	}

	public Integer getOffsetrows() {
		return offsetrows;
	}

	public void setOffsetrows(Integer offsetrows) {
		this.offsetrows = offsetrows;
	}

	public Integer getNumrows() {
		return numrows;
	}

	public void setNumrows(Integer numrows) {
		this.numrows = numrows;
	}

	public Boolean isState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public ChannelStateEnum getChannelState() {
		return channelState;
	}

	public void setChannelState(ChannelStateEnum channelState) {
		this.channelState = channelState;
	}

	public BuildTypeEnum getBuildType() {
		return buildType;
	}

	public void setBuildType(BuildTypeEnum a) {
		this.buildType = a;
	}

	public String getReviewStatus() {
		return reviewStatus;
	}

	public void setReviewStatus(String reviewStatus) {
		this.reviewStatus = reviewStatus;
	}

	public String getReviewResult() {
		return reviewResult;
	}

	public void setReviewResult(String reviewResult) {
		this.reviewResult = reviewResult;
	}

	public String getRatePlanType() {
		return ratePlanType;
	}

	public void setRatePlanType(String ratePlanType) {
		this.ratePlanType = ratePlanType;
	}

	public String getChannelPropertyType() {
		return channelPropertyType;
	}

	public void setChannelPropertyType(String channelPropertyType) {
		this.channelPropertyType = channelPropertyType;
	}

	public Integer getSyncType() {
		return syncType;
	}

	public void setSyncType(Integer syncType) {
		this.syncType = syncType;
	}
	
	public boolean equals(Object obj) {
		return (obj instanceof ChannelProductMap && this.getProductId().equals(((ChannelProductMap) obj).getProductId())
				&& this.getChannelId().equals(((ChannelProductMap) obj).getChannelId()));
	}

	public int hashCode() {
		return (this.getProductId().hashCode() + this.getChannelId().hashCode());
	}
	
	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public boolean isLosPriceEnable() {
		return losPriceEnable;
	}

	public void setLosPriceEnable(boolean losPriceEnable) {
		this.losPriceEnable = losPriceEnable;
	}

	public Integer getPortalState() {
		return portalState;
	}

	public void setPortalState(Integer portalState) {
		this.portalState = portalState;
	}

	public Integer getPortalPriorState() {
		return portalPriorState;
	}

	public void setPortalPriorState(Integer portalPriorState) {
		this.portalPriorState = portalPriorState;
	}

	public LocalDateTime getPortalApprovedDate() {
		return portalApprovedDate;
	}

	public void setPortalApprovedDate(LocalDateTime portalApprovedDate) {
		this.portalApprovedDate = portalApprovedDate;
	}

	public LocalDateTime getPortalRejectedDate() {
		return portalRejectedDate;
	}

	public void setPortalRejectedDate(LocalDateTime portalRejectedDate) {
		this.portalRejectedDate = portalRejectedDate;
	}

	public String getPortalRejectedReason() {
		return portalRejectedReason;
	}

	public void setPortalRejectedReason(String portalRejectedReason) {
		this.portalRejectedReason = portalRejectedReason;
	}

	public String toString() {
		return ("Product Id: " + this.getProductId() + " Channel Id " + this.getChannelId() + " Channel Product Id " + this.getChannelProductId()
				+ " Channel Room Id " + this.getChannelRoomId() + " Channel Rate Id " + this.getChannelRateId() + " Channel State " + this.getChannelState()
				+ " State " + this.isState() + " Los price enable " + this.isLosPriceEnable());
	}

}