package com.mybookingpal.dal.shared;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Danijel Blagojevic
 *
 */
@Component
@Scope(value = "prototype")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChannelSpecificTaxGroupRelation {

    private Integer groupId;
    private Integer channelId;
    private Date createdDate;

}
