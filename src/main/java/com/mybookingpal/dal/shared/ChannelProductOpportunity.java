package com.mybookingpal.dal.shared;

import java.util.Date;

public class ChannelProductOpportunity {
	private Integer id;
	private Integer productId;
	private Integer opportunityId;
	private String url;
	private String supplierReply;
	private Date createdDate;
	private Date version;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getOpportunityId() {
		return opportunityId;
	}

	public void setOpportunityId(Integer opportunityId) {
		this.opportunityId = opportunityId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSupplierReply() {
		return supplierReply;
	}

	public void setSupplierReply(String supplierReply) {
		this.supplierReply = supplierReply;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}
}
