package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Danijel Blagojevic
 *
 */
@Component
@Scope(value = "prototype")
public class ChannelYieldMap {

	public enum ChannelYieldMapStateEnum {
		CREATED("Created"),
		FINAL("Final");

		private String value;

		ChannelYieldMapStateEnum(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static ChannelYieldMapStateEnum getByStr(String value) {
			for (ChannelYieldMapStateEnum v : values()) {
				if (v.value.equals(value)) {
					return v;
				}
			}
			return null;
		}

		public static String getNameByStr(String value) {
			return getByStr(value).name();
		}
	}

	private Integer id;
	private String ruleId;
	private Integer yieldId;
	private Integer channelId;
	private ChannelYieldMapStateEnum state;
	private Date createdDate;
	private Date version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	public Integer getYieldId() {
		return yieldId;
	}

	public void setYieldId(Integer yieldId) {
		this.yieldId = yieldId;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public ChannelYieldMapStateEnum getState() {
		return state;
	}

	public void setState(ChannelYieldMapStateEnum state) {
		this.state = state;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

}
