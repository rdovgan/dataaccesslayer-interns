package com.mybookingpal.dal.shared;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.server.enumerations.multiunit.PaymentType;
import com.mybookingpal.dal.server.enumerations.multiunit.ValueType;

/**
 * Entity for payment_details table
 */
@Component
@Scope("prototype")
public class PaymentDetails {

	private Long id;
	private Integer propertyManagerPartyId;
	private Integer productId;
	private PaymentType paymentType;
	private ValueType valueType;
	private BigDecimal firstPayment;
	private Integer balanceDueDaysFromArrival;
	private Date created;
	private Date version;
	private State state;

	public enum State {
		CREATED, FINAL
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPropertyManagerPartyId() {
		return propertyManagerPartyId;
	}

	public void setPropertyManagerPartyId(Integer propertyManagerPartyId) {
		this.propertyManagerPartyId = propertyManagerPartyId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public ValueType getValueType() {
		return valueType;
	}

	public void setValueType(ValueType valueType) {
		this.valueType = valueType;
	}

	public BigDecimal getFirstPayment() {
		return firstPayment;
	}

	public void setFirstPayment(BigDecimal firstPayment) {
		this.firstPayment = firstPayment;
	}

	public Integer getBalanceDueDaysFromArrival() {
		return balanceDueDaysFromArrival;
	}

	public void setBalanceDueDaysFromArrival(Integer balanceDueDaysFromArrival) {
		this.balanceDueDaysFromArrival = balanceDueDaysFromArrival;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
}
