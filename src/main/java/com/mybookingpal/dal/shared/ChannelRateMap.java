package com.mybookingpal.dal.shared;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ChannelRateMap extends ModelTable  {
    private String rateName;
    private String channelRateId;
    private String priceId;
    private String yieldId;
    public String getRateName() {
        return rateName;
    }
    public void setRateName(String rateName) {
        this.rateName = rateName;
    }
    public String getChannelRateId() {
        return channelRateId;
    }
    public void setChannelRateId(String channelRateId) {
        this.channelRateId = channelRateId;
    }
    
    public String getPriceId() {
        return priceId;
    }
    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }
    public String getYieldId() {
        return yieldId;
    }
    public void setYieldId(String yieldId) {
        this.yieldId = yieldId;
    }
//    @Override
//    public Service service() {
//        return null;
//    }
    
}
