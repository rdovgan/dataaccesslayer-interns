package com.mybookingpal.dal.shared;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class Relation {
	//TODO: convert to enum
	public static final String PRODUCT_ATTRIBUTE = "Product Attribute";
	public static final String PRODUCT_TAX = "PRODUCT_TAX";

	private String link;
	private String headid;
	private String lineid;

	public Relation() {
	}

	public void init(String link, String headid, String lineid) {
		this.link = link;
		this.headid = headid;
		this.lineid = lineid;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getHeadid() {
		return headid;
	}

	public void setHeadid(String headid) {
		this.headid = headid;
	}

	public String getLineid() {
		return lineid;
	}

	public void setLineid(String lineid) {
		this.lineid = lineid;
	}

	public boolean noHeadid() {
		return headid == null || headid.isEmpty();
	}

	public boolean noLineid() {
		return lineid == null || lineid.isEmpty();
	}

}
