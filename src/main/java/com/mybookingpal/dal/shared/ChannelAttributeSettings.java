package com.mybookingpal.dal.shared;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Date;

/**
 * @author Mihailo Spasojevic
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChannelAttributeSettings {

    private Integer channelPartnerId;

    private Integer nativePropertyType;

    private Integer acceptsPropertyType;

    private Integer ratesAndAvailabilityMapping;

    private Integer ratesQuantity;

    private Integer bookingType;

    private Integer minimumProperties;

    private Boolean supportSynchronization;

    private Date createdDate;

    private Date version;


}
