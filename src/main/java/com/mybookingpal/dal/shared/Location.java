package com.mybookingpal.dal.shared;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.mybookingpal.dataaccesslayer.entity.LocationModel;
import com.mybookingpal.shared.location.LocationType;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//import com.google.gwt.maps.client.base.LatLng;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias( value = "location")
@XmlRootElement(name = "location")
@XmlAccessorType(XmlAccessType.NONE)
@Component
@Scope(value = "prototype")
public class Location extends Model {

	public enum State {
		INITIAL ("Initial"),
		FINAL ("Final"),
		CREATED ("Created"),
		CONFLICT ("Conflict"),
		SUSPENDED ("Suspended"),
		DEPRECATED ("Deprecated");

		private String name;

		State(String name) {
			this.name = name;
		}

		public String getName() {
			return this.name;
		}
	}

	public class GeoLocation {

	}

	public static final String INITIAL = "Initial";
	public static final String FINAL = "Final";
	public static final String CREATED = "Created";
	public static final String CONFLICT = "Conflict";
	public static final String SUSPENDED = "Suspended";
	public static final String DEPRECATED = "Deprecated";
	public static final String[] STATES = { INITIAL, CREATED, SUSPENDED, DEPRECATED, FINAL };

	private static final double getRandom(double min, double max) {
		return min + (Math.random() * (max - min));
	}

	public Location () {}

	public Location(int id, String code, String name, String country, String region, double latitude, double longitude) {
		super();
		this.id = String.valueOf(id);
		this.code = code;
		this.name = name;
		this.country = country;
		this.region = region;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public Location(LocationModel model) {
		this.id = model.getId() != null ? String.valueOf(model.getId()) : null;
		this.name = model.getName();
		this.state = model.getState();
		this.gname = model.getGName();
		this.code = model.getCode();
		this.country = model.getCountry();
		this.region = model.getRegion();
		this.adminarea_lvl_1 = model.getAdminAreaLvl1();
		this.adminarea_lvl_2 = model.getAdminAreaLvl2();
		this.area = model.getArea();
		this.locationtype = model.getLocationType();
		this.iata = model.getIata();
		this.notes = model.getNotes();
		this.codeinterhome = model.getCodeInterHome();
		this.coderentalsunited = model.getCodeRentalsUnited();
		this.latitude = model.getLatitude();
		this.longitude = model.getLongitude();
		this.altitude = model.getAltitude();
		this.parentid = model.getParentId();
		this.zipCode = model.getZipCode();
		this.timeZoneId = model.getTimeZoneId();
	}

	public Service service() {return Service.LOCATION;}

	private String gname;
	private String code;
	private String country;
	private String region;
	@XStreamAlias( value = "region" )
	private String adminarea_lvl_1;
	@XStreamAlias( value = "district")
	private String adminarea_lvl_2;
	private String area;
	private String locationtype;
	private String iata;
	private String notes;
	private String codeinterhome;
	private String coderentalsunited;
	private Double latitude;
	private Double longitude;
	private Double altitude;
	@XStreamOmitField
	private Integer parentid;
	
	@XStreamOmitField
	private String zipCode;
	private Integer timeZoneId;

	public Integer getTimeZoneId() {
		return timeZoneId;
	}

	/**
	 * Sets MyBookingPal's time zone ID to the location. It can be gotten from {@link TimeZone#getId()}
	 *
	 * @param timeZoneId MyBookingPal's time zone ID
	 */
	public void setTimeZoneId(Integer timeZoneId) {
		this.timeZoneId = timeZoneId;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@XmlElement(name = "country")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public boolean noRegion() {
		return region == null || region.isEmpty();
	}
	
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Integer getParentID() {
		return parentid;
	}

	public void setParentID(Integer parentid) {
		this.parentid = parentid;
	}

	public String getIata() {
		return iata;
	}

	public void setIata(String iata) {
		this.iata = iata;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCodeinterhome() {
		return codeinterhome;
	}

	public void setCodeinterhome(String codeinterhome) {
		this.codeinterhome = codeinterhome;
	}
	
	public String getCoderentalsunited() {
		return coderentalsunited;
	}

	public void setCoderentalsunited(String coderentalsunited) {
		this.coderentalsunited = coderentalsunited;
	}
	
	@XmlElement(name = "latitude")
	public Double getLatitude() {
		return latitude;
	}

	public Double getLatitudeNearby(double min, double max) {
		return latitude == null || latitude == 0.0 ? null : latitude + getRandom(min, max);
	}

	public Double getLatitudeNearby() {
		return getLatitudeNearby(-0.015, 0.015);
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@XmlElement(name = "longitude")
	public Double getLongitude() {
		return longitude;
	}

	public Double getLongitudeNearby(double min, double max) {
		return longitude == null || longitude == 0.0 ? null : longitude + getRandom(min, max);
	}

	public Double getLongitudeNearby() {
		return getLongitudeNearby(-0.015, 0.015);
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	
	public boolean noLatLng() {
		return latitude == null || latitude < -90.0 || latitude > 90.0
				|| longitude == null || longitude < -180.0 || longitude > 180.0; 
	}

	public boolean hasLatLng() {
		return !noLatLng();
	}
	
	public boolean zeroLatLng() {
		return latitude == 0.0 && longitude == 0.0;
	}
	
	@XmlElement(name = "region")
	public String getAdminarea_lvl_1() {
		return adminarea_lvl_1;
	}

	
	public void setAdminarea_lvl_1(String adminarea_lvl_1) {
		this.adminarea_lvl_1 = adminarea_lvl_1;
	}
	
	@XmlElement(name = "district")
	public String getAdminarea_lvl_2() {
		return adminarea_lvl_2;
	}

	public void setAdminarea_lvl_2(String adminarea_lvl_2) {
		this.adminarea_lvl_2 = adminarea_lvl_2;
	}

	public String getGname() {
		return gname;
	}

	public void setGname(String gname) {
		this.gname = gname;
	}

	public String getLocationtype() {
		return locationtype;
	}

	public void setLocationtype(String locationtype) {
		this.locationtype = locationtype;
	}

	public boolean isLocationTypeLocality() {
		if(getLocationtype() == null) {
			return false;
		}
		return getLocationtype().equals(LocationType.LOCALITY.getName());
	}

	public boolean isLocationTypeSublocality() {
		if(getLocationtype() == null) {
			return false;
		}
		return getLocationtype().equals(LocationType.SUBLOCALITY.getName());
	}

	public boolean isLocationTypeCountry() {
		if(getLocationtype() == null) {
			return false;
		}
		return getLocationtype().equals(LocationType.COUNTRY.getName());
	}

	public boolean isStateConflict() {
		return State.CONFLICT.getName().equalsIgnoreCase(getState());
	}

	public boolean isStateCreated() {
		return State.CREATED.getName().equalsIgnoreCase(getState());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Location [code=");
		builder.append(code);
		builder.append(", country=");
		builder.append(country);
		builder.append(", region=");
		builder.append(region);
		builder.append(", adminarea_lvl_1=");
		builder.append(adminarea_lvl_1);
		builder.append(", adminarea_lvl_2=");
		builder.append(adminarea_lvl_2);
		builder.append(", area=");
		builder.append(area);
		builder.append(", locationtype=");
		builder.append(locationtype);
		builder.append(", iata=");
		builder.append(iata);
		builder.append(", notes=");
		builder.append(notes);
		builder.append(", codeinterhome=");
		builder.append(codeinterhome);
		builder.append(", coderentalsunited=");
		builder.append(coderentalsunited);
		builder.append(", latitude=");
		builder.append(latitude);
		builder.append(", longitude=");
		builder.append(longitude);
		builder.append(", altitude=");
		builder.append(altitude);
		builder.append(", organizationid=");
		builder.append(organizationid);
		builder.append(", status=");
		builder.append(status);
		builder.append(", state=");
		builder.append(state);
		builder.append(", values=");
		builder.append(values);
		builder.append(", attributes=");
		builder.append(attributemap);
		builder.append(", texts=");
		builder.append(texts);
		builder.append(", name=");
		builder.append(name);
		builder.append(", gname=");
		builder.append(gname);
		builder.append(", parentid=");
		builder.append(parentid);
		builder.append(", id=");
		builder.append(id);
		builder.append(", service()=");
		builder.append(service());
		builder.append("]");
		return builder.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Location)) return false;

		Location location = (Location) o;

		if (getGname() != null ? !getGname().equals(location.getGname()) : location.getGname() != null) return false;
		if (getCode() != null ? !getCode().equals(location.getCode()) : location.getCode() != null) return false;
		if (getCountry() != null ? !getCountry().equals(location.getCountry()) : location.getCountry() != null) return false;
		if (getRegion() != null ? !getRegion().equals(location.getRegion()) : location.getRegion() != null) return false;
		if (getAdminarea_lvl_1() != null ? !getAdminarea_lvl_1().equals(location.getAdminarea_lvl_1()) : location.getAdminarea_lvl_1() != null) return false;
		if (getAdminarea_lvl_2() != null ? !getAdminarea_lvl_2().equals(location.getAdminarea_lvl_2()) : location.getAdminarea_lvl_2() != null) return false;
		if (getArea() != null ? !getArea().equals(location.getArea()) : location.getArea() != null) return false;
		if (getLocationtype() != null ? !getLocationtype().equals(location.getLocationtype()) : location.getLocationtype() != null) return false;
		if (getIata() != null ? !getIata().equals(location.getIata()) : location.getIata() != null) return false;
		if (getNotes() != null ? !getNotes().equals(location.getNotes()) : location.getNotes() != null) return false;
		if (getCodeinterhome() != null ? !getCodeinterhome().equals(location.getCodeinterhome()) : location.getCodeinterhome() != null) return false;
		if (getCoderentalsunited() != null ? !getCoderentalsunited().equals(location.getCoderentalsunited()) : location.getCoderentalsunited() != null)
			return false;
		if (getLatitude() != null ? !getLatitude().equals(location.getLatitude()) : location.getLatitude() != null) return false;
		if (getLongitude() != null ? !getLongitude().equals(location.getLongitude()) : location.getLongitude() != null) return false;
		if (getAltitude() != null ? !getAltitude().equals(location.getAltitude()) : location.getAltitude() != null) return false;
		if (parentid != null ? !parentid.equals(location.parentid) : location.parentid != null) return false;
		return getZipCode() != null ? getZipCode().equals(location.getZipCode()) : location.getZipCode() == null;

	}

	@Override
	public int hashCode() {
		int result = getGname() != null ? getGname().hashCode() : 0;
		result = 31 * result + (getCode() != null ? getCode().hashCode() : 0);
		result = 31 * result + (getCountry() != null ? getCountry().hashCode() : 0);
		result = 31 * result + (getRegion() != null ? getRegion().hashCode() : 0);
		result = 31 * result + (getAdminarea_lvl_1() != null ? getAdminarea_lvl_1().hashCode() : 0);
		result = 31 * result + (getAdminarea_lvl_2() != null ? getAdminarea_lvl_2().hashCode() : 0);
		result = 31 * result + (getArea() != null ? getArea().hashCode() : 0);
		result = 31 * result + (getLocationtype() != null ? getLocationtype().hashCode() : 0);
		result = 31 * result + (getIata() != null ? getIata().hashCode() : 0);
		result = 31 * result + (getNotes() != null ? getNotes().hashCode() : 0);
		result = 31 * result + (getCodeinterhome() != null ? getCodeinterhome().hashCode() : 0);
		result = 31 * result + (getCoderentalsunited() != null ? getCoderentalsunited().hashCode() : 0);
		result = 31 * result + (getLatitude() != null ? getLatitude().hashCode() : 0);
		result = 31 * result + (getLongitude() != null ? getLongitude().hashCode() : 0);
		result = 31 * result + (getAltitude() != null ? getAltitude().hashCode() : 0);
		result = 31 * result + (parentid != null ? parentid.hashCode() : 0);
		result = 31 * result + (getZipCode() != null ? getZipCode().hashCode() : 0);
		return result;
	}

	public final void setState(State state) {
		setState(state.getName());
	}

}