package com.mybookingpal.dal.shared;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ChannelProductYieldMap extends ModelTable{
	private String productId;
	private Integer channelId;
	private String yieldRuleName;
	private String channelRateId;
	private Integer maxStay;
	public static final String LOS7="LOS7";
	public static final String LOS30="LOS30";
	public static final String LOS28 = "LOS28";
	public static final String WEEKEND="weekend";//weekend rates
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	public String getYieldRuleName() {
		return yieldRuleName;
	}
	public void setYieldRuleName(String yieldRuleName) {
		this.yieldRuleName = yieldRuleName;
	}
	public String getChannelRateId() {
		return channelRateId;
	}
	public void setChannelRateId(String channelRateId) {
		this.channelRateId = channelRateId;
	}
	public Integer getMaxStay() {
		return maxStay;
	}
	public void setMaxStay(Integer maxStay) {
		this.maxStay = maxStay;
	}
	
	public String toString() {
		return ("Product Id: " + this.getProductId() 
				+ " Channel Id " + this.getChannelId()
				+ " Channel Yield Name " + this.getYieldRuleName()
				+ " Channel Rate Id " + this.getChannelRateId());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((channelId == null) ? 0 : channelId.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((yieldRuleName == null) ? 0 : yieldRuleName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChannelProductYieldMap other = (ChannelProductYieldMap) obj;
		if (channelId == null) {
			if (other.channelId != null)
				return false;
		} else if (!channelId.equals(other.channelId))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (yieldRuleName == null) {
			if (other.yieldRuleName != null)
				return false;
		} else if (!yieldRuleName.equals(other.yieldRuleName))
			return false;
		return true;
	}
	
	

}
