package com.mybookingpal.dal.shared;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Danijel Blagojevic
 *
 */
@Component
@Scope(value = "prototype")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChannelListingInformation {

    private Integer id;
    private Integer productId;
    private Integer supplierId;
    private Integer channelSupplierId;
    private Integer channelId;
    private String channelProductId;
    private String channelProductName;
    private double latitude;
    private double longitude;
    private Date created;
    private String channelStatus;
    private String syncType;
    private Date version;
}


