package com.mybookingpal.dal.shared;

import java.time.LocalDate;
import java.util.Date;

import com.mybookingpal.shared.cache.IsCacheable;
import com.mybookingpal.dal.utils.CommonDateUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("MinStay")
@Scope(value = "prototype")
public class MinStay implements IsCacheable{

	private Long id;
	private String supplierid;
	private String productid;
	private LocalDate fromdate;
	private LocalDate todate;
	private Integer value;
	private Date version;
	private PropertyMinStay.StateTypeEnum state;

	public MinStay() {
		super();
	}
	
	public MinStay(String supplierid, String productid, LocalDate fromDate, LocalDate toDate, Integer value) {
		super();
		this.supplierid = supplierid;
		this.productid = productid;
		this.fromdate = fromDate;
		this.todate = toDate;
		this.value = value;
	}

	public MinStay(String supplierid, String productid, LocalDate fromDate, LocalDate toDate, Integer value, Date version, PropertyMinStay.StateTypeEnum state) {
		this.supplierid = supplierid;
		this.productid = productid;
		this.fromdate = fromDate;
		this.todate = toDate;
		this.value = value;
		this.version = version;
		this.state = state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSupplierid() {
		return supplierid;
	}

	public void setSupplierid(String supplierid) {
		this.supplierid = supplierid;
	}

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	@Deprecated
	public Date getFromdate() {
		return CommonDateUtils.toDate(fromdate);
	}

	public LocalDate getFromDate() {
		return fromdate;
	}

	@Deprecated
	public void setFromdate(Date fromdate) {
		this.fromdate = CommonDateUtils.toLocalDate(fromdate);
	}

	public void setFromDate(LocalDate fromdate) {
		this.fromdate = fromdate;
	}

	@Deprecated
	public Date getTodate() {
		return CommonDateUtils.toDate(todate);
	}

	public LocalDate getToDate() {
		return todate;
	}

	@Deprecated
	public void setTodate(Date toDate) {
		this.todate = CommonDateUtils.toLocalDate(toDate);
	}
	public void setToDate(LocalDate toDate) {
		this.todate = toDate;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public PropertyMinStay.StateTypeEnum getState() {
		return state;
	}

	public void setState(PropertyMinStay.StateTypeEnum state) {
		this.state = state;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MinStay [id=");
		builder.append(id);
		builder.append(", supplierid=");
		builder.append(supplierid);
		builder.append(", productid=");
		builder.append(productid);
		builder.append(", fromdate=");
		builder.append(fromdate);
		builder.append(", todate=");
		builder.append(todate);
		builder.append(", value=");
		builder.append(value);
		builder.append(", version=");
		builder.append(version);
		builder.append(", state=");
		builder.append(state);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public Long getCacheProductId() {
		return NumberUtils.createLong(productid);
	}

}
