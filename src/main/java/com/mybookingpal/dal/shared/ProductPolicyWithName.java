package com.mybookingpal.dal.shared;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Danijel Blagojevic
 *
 */
@Component
@Scope(value = "prototype")
public class ProductPolicyWithName extends ProductPolicy {
	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
