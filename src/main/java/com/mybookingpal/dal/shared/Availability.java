/**
 * 
 */
package com.mybookingpal.dal.shared;

/**
 * Enum for Property availability status available/unavailable
 * 
 * @author Nibodha-Aruna [May 20, 2015-2:21:59 PM]
 * 
 */
public enum Availability {

	AVAILABLE("default"), UNAVAILABLE("unavailable"), DEFAULT("default");

	private Availability(String availability) {
		name = availability;
	}

	private String name;

	public String getName() {
		return name;
	}
}
