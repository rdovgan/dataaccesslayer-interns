package com.mybookingpal.dal.shared;

import com.mybookingpal.dal.utils.BigDecimalExt;

import java.time.LocalDate;

public class QuoteDetail {

	private String userType;
	private String entity;
	private Integer entityId;
	private String type;
	private Integer typeId;
	private Integer typeAltId;
	private String typeChannelAltId;
	private String name;
	private BigDecimalExt amount;
	private String currency;
	private LocalDate fromDate;
	private LocalDate toDate;
	private String promotion;
	private Boolean included;
	private String unit;
	private BigDecimalExt percentage;

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Integer getTypeAltId() {
		return typeAltId;
	}

	public void setTypeAltId(Integer typeAltId) {
		this.typeAltId = typeAltId;
	}

	public String getTypeChannelAltId() {
		return typeChannelAltId;
	}

	public void setTypeChannelAltId(String typeChannelAltId) {
		this.typeChannelAltId = typeChannelAltId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimalExt getAmount() {
		return amount;
	}

	public void setAmount(BigDecimalExt amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public String getPromotion() {
		return promotion;
	}

	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}

	public Boolean getIncluded() {
		return included;
	}

	public void setIncluded(Boolean included) {
		this.included = included;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public BigDecimalExt getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimalExt percentage) {
		this.percentage = percentage;
	}
}
