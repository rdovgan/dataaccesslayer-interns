package com.mybookingpal.dal.shared.yield;

import com.mybookingpal.dal.shared.Yield;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Component("YieldSearch")
@Scope("prototype")
public class YieldSearch extends Yield {

	private List<String> entityTypes;
	private List<String> entityIds;

	public YieldSearch() {
		super();
	}

	public YieldSearch(String entityType, String entityId) {
		super(entityType, entityId);
	}

	public YieldSearch(String entityType, String entityId, LocalDate fromDate, LocalDate toDate) {
		super(entityType, entityId, fromDate, toDate);
	}

	public List<String> getEntityTypes() {
		return entityTypes;
	}

	public void setEntityTypes(List<String> entityTypes) {
		this.entityTypes = entityTypes;
	}

	public List<String> getEntityIds() {
		return entityIds;
	}

	public void setEntityIds(List<String> entityIds) {
		this.entityIds = entityIds;
	}

}
