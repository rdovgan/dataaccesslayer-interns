package com.mybookingpal.dal.shared;

import java.time.LocalDate;
import java.util.Date;

import com.mybookingpal.dal.utils.CommonDateUtils;
import com.mybookingpal.shared.cache.IsCacheable;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("PropertyMinStay")
@Scope(value = "prototype")
public class PropertyMinStay implements DateRange, IsCacheable {

	public enum StateTypeEnum {
		INITIAL(1),
		CREATED(2),
		FINAL(3);

		private Integer value;

		StateTypeEnum(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return this.value;
		}

		public static StateTypeEnum getByInt(int value) {
			for (StateTypeEnum v : values()) {
				if (v.value.equals(value)) {
					return v;
				}
			}
			return CREATED;
		}

		public static String getNameByInt(Integer value) {
			return getByInt(value).name();
		}
	}

	private Long id;
	private String supplierID;
	private String productID;
	private LocalDate fromDate;
	private LocalDate toDate;
	private Integer value;
	private Date version;
	private StateTypeEnum state;

	public PropertyMinStay() {
	}

	public PropertyMinStay(PropertyMinStay propertyMinStay) {
		this.id = propertyMinStay.id;
		this.supplierID = propertyMinStay.supplierID;
		this.productID = propertyMinStay.productID;
		this.fromDate = propertyMinStay.fromDate;
		this.toDate = propertyMinStay.toDate;
		this.value = propertyMinStay.value;
		this.version = propertyMinStay.version;
		this.state = propertyMinStay.state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSupplierID() {
		return supplierID;
	}

	public void setSupplierID(String supplierID) {
		this.supplierID = supplierID;
	}

	public String getProductID() {
		return productID;
	}

	public void setProductID(String productID) {
		this.productID = productID;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public StateTypeEnum getState() {
		return state;
	}

	public void setState(StateTypeEnum state) {
		this.state = state;
	}

	@Override
	public Long getCacheProductId() {
		return NumberUtils.createLong(productID);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fromDate == null) ? 0 : fromDate.hashCode());
		result = prime * result + ((productID == null) ? 0 : productID.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((supplierID == null) ? 0 : supplierID.hashCode());
		result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropertyMinStay other = (PropertyMinStay) obj;
		if (fromDate == null) {
			if (other.fromDate != null)
				return false;
		} else if (!fromDate.equals(other.fromDate))
			return false;
		if (productID == null) {
			if (other.productID != null)
				return false;
		} else if (!productID.equals(other.productID))
			return false;
		if (state != other.state)
			return false;
		if (supplierID == null) {
			if (other.supplierID != null)
				return false;
		} else if (!supplierID.equals(other.supplierID))
			return false;
		if (toDate == null) {
			if (other.toDate != null)
				return false;
		} else if (!toDate.equals(other.toDate))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public String toString() {
		return "PropertyMinStay [id=" + id + ", supplierID=" + supplierID + ", productID=" + productID + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", value=" + value + ", version=" + version + ", state=" + state + "]";
	}

}
