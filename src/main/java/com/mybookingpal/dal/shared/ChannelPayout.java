package com.mybookingpal.dal.shared;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
@Component
@Scope(value = "prototype")
public class ChannelPayout {
	private Integer id;
	private Integer channelId;
	private String channelProductId;
	private String confirmationCode;
	private String hostCurrency;
	private String expectedPayoutAmount;
	private String totalPaidAmount;
	private Date confirmedAt;
	private Date confirmedAtMinusOne;
	private Date confirmedAtPlusOne;
	private Date fromDate;
	private Date toDate;
	private String basePrice;
	private String cancellationHostFee;
	private String cancellationPayout;
	private String hostFee;
	private String cleaningFee;
	private String occupancyTaxAmountPaidtoHost;
	private String transientOccupancyTaxPaidAmount;
	
	private static DateTimeFormatter dateTimeFormatter1 = DateTimeFormat
			.forPattern("yyy-MM-dd");
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	public String getChannelProductId() {
		return channelProductId;
	}
	public void setChannelProductId(String channelProductId) {
		this.channelProductId = channelProductId;
	}
	public String getConfirmationCode() {
		return confirmationCode;
	}
	public void setConfirmationCode(String confirmationCode) {
		this.confirmationCode = confirmationCode;
	}
	public String getHostCurrency() {
		return hostCurrency;
	}
	public void setHostCurrency(String hostCurrency) {
		this.hostCurrency = hostCurrency;
	}
	public String getExpectedPayoutAmount() {
		return expectedPayoutAmount;
	}
	public void setExpectedPayoutAmount(String expectedPayoutAmount) {
		this.expectedPayoutAmount = expectedPayoutAmount;
	}
	public String getTotalPaidAmount() {
		return totalPaidAmount;
	}
	public void setTotalPaidAmount(String totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}
	public Date getConfirmedAt() {
		return confirmedAt;
	}
	public void setConfirmedAt(Date confirmedAt) {
		this.confirmedAt = confirmedAt;
		this.setConfirmedAtMinusOne(dateTimeFormatter1.parseDateTime(dateTimeFormatter1.print(new DateTime(confirmedAt))).toDate());
		this.setConfirmedAtPlusOne(dateTimeFormatter1.parseDateTime(dateTimeFormatter1.print(new DateTime(confirmedAt).plusDays(1))).toDate());
	}
	public Date getConfirmedAtMinusOne() {
		return confirmedAtMinusOne;
	}
	public void setConfirmedAtMinusOne(Date confirmedAtMinusOne) {
		this.confirmedAtMinusOne = confirmedAtMinusOne;
	}
	
	public Date getConfirmedAtPlusOne() {
		return confirmedAtPlusOne;
	}
	public void setConfirmedAtPlusOne(Date confirmedAtPlusOne) {
		this.confirmedAtPlusOne = confirmedAtPlusOne;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(String basePrice) {
		this.basePrice = basePrice;
	}
	public String getCancellationHostFee() {
		return cancellationHostFee;
	}
	public void setCancellationHostFee(String cancellationHostFee) {
		this.cancellationHostFee = cancellationHostFee;
	}
	public String getCancellationPayout() {
		return cancellationPayout;
	}
	public void setCancellationPayout(String cancellationPayout) {
		this.cancellationPayout = cancellationPayout;
	}
	public String getHostFee() {
		return hostFee;
	}
	public void setHostFee(String hostFee) {
		this.hostFee = hostFee;
	}	
	public String getCleaningFee() {
		return cleaningFee;
	}
	public void setCleaningFee(String cleaningFee) {
		this.cleaningFee = cleaningFee;
	}
	public String getOccupancyTaxAmountPaidtoHost() {
		return occupancyTaxAmountPaidtoHost;
	}
	public void setOccupancyTaxAmountPaidtoHost(String occupancyTaxAmountPaidtoHost) {
		this.occupancyTaxAmountPaidtoHost = occupancyTaxAmountPaidtoHost;
	}
	public String getTransientOccupancyTaxPaidAmount() {
		return transientOccupancyTaxPaidAmount;
	}
	public void setTransientOccupancyTaxPaidAmount(String transientOccupancyTaxPaidAmount) {
		this.transientOccupancyTaxPaidAmount = transientOccupancyTaxPaidAmount;
	}	
		
}
