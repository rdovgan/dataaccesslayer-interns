package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ChannelPromoStatus {

	private Integer id;
	private Integer channelId;
	private Integer promoId;
	private boolean availabilityStatus;
	private boolean expiredStatus;
	private Date version;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getVersion() {
		return version;
	}
	public void setVersion(Date version) {
		this.version = version;
	}
	public boolean isAvailabilityStatus() {
		return availabilityStatus;
	}
	public void setAvailabilityStatus(boolean availabilityStatus) {
		this.availabilityStatus = availabilityStatus;
	}
	public boolean isExpiredStatus() {
		return expiredStatus;
	}
	public void setExpiredStatus(boolean expiredStatus) {
		this.expiredStatus = expiredStatus;
	}
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	public Integer getPromoId() {
		return promoId;
	}
	public void setPromoId(Integer promoId) {
		this.promoId = promoId;
	}
}
