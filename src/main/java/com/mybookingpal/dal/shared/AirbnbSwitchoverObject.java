package com.mybookingpal.dal.shared;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;

@Data
@Scope(value =  "prototype")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AirbnbSwitchoverObject {

    private int propertyId;
    private int channelProductId;
    private String syncType;

}
