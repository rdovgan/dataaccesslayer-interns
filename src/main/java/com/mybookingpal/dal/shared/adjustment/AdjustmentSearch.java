package com.mybookingpal.dal.shared.adjustment;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.shared.Adjustment;

@Component
@Scope(value = "prototype")
public class AdjustmentSearch extends Adjustment {
    
    private List<String> productIds;

    public List<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }       

}
