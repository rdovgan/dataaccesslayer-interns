package com.mybookingpal.dal.shared;

public class TimeZone {
	private Integer id;
	private String name;

	public TimeZone() {

	}

	public TimeZone(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "TimeZone:" + " id " + getId() + " name " + getName();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof TimeZone)) return false;

		TimeZone timeZone = (TimeZone) o;

		if (getId() != null ? !getId().equals(timeZone.getId()) : timeZone.getId() != null) return false;
		return getName() != null ? getName().equals(timeZone.getName()) : timeZone.getName() == null;

	}

	@Override
	public int hashCode() {
		int result = getId() != null ? getId().hashCode() : 0;
		result = 31 * result + (getName() != null ? getName().hashCode() : 0);
		return result;
	}
}
