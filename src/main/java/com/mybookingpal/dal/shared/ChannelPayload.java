package com.mybookingpal.dal.shared;

import javax.xml.bind.annotation.XmlElement;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mybookingpal.dal.utils.BigDecimalExt;

@Component
@Scope(value = "prototype")
public class ChannelPayload {

	private Integer id;

	@XmlElement(name = "reservation_id")
	@JsonProperty("reservation_id")
	private Integer reservationId;

	@XmlElement(name = "confirmation_id")
	@JsonProperty("confirmation_id")
	private String confirmationId;

	private String payload;

	private BigDecimalExt quote;

	private BigDecimalExt npnr;

	@XmlElement(name = "taxes")
	@JsonProperty("taxes")
	private BigDecimalExt taxes;

	@XmlElement(name = "fees")
	@JsonProperty("fees")
	private BigDecimalExt fees;

	@XmlElement(name = "yields")
	@JsonProperty("yields")
	private BigDecimalExt yields;

	@XmlElement(name = "commissions")
	@JsonProperty("commissions")
	private BigDecimalExt commissions;

	private String currency;

	public ChannelPayload() {

	}

	public ChannelPayload(Integer id, Integer reservationId, String confirmationId, String payload, BigDecimalExt quote, BigDecimalExt npnr,
			BigDecimalExt taxes, BigDecimalExt fees, BigDecimalExt yields, BigDecimalExt commissions, String currency) {
		super();
		this.id = id;
		this.reservationId = reservationId;
		this.confirmationId = confirmationId;
		this.payload = payload;
		this.quote = quote;
		this.npnr = npnr;
		this.taxes = taxes;
		this.fees = fees;
		this.yields = yields;
		this.commissions = commissions;
		this.currency = currency;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public String getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(String confirmationId) {
		this.confirmationId = confirmationId;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public BigDecimalExt getQuote() {
		return quote;
	}

	public void setQuote(BigDecimalExt quote) {
		this.quote = quote;
	}

	public BigDecimalExt getNpnr() {
		return npnr;
	}

	public void setNpnr(BigDecimalExt npnr) {
		this.npnr = npnr;
	}

	public BigDecimalExt getTaxes() {
		return taxes;
	}

	public void setTaxes(BigDecimalExt taxes) {
		this.taxes = taxes;
	}

	public BigDecimalExt getFees() {
		return fees;
	}

	public void setFees(BigDecimalExt fees) {
		this.fees = fees;
	}

	public BigDecimalExt getYields() {
		return yields;
	}

	public void setYields(BigDecimalExt yields) {
		this.yields = yields;
	}

	public BigDecimalExt getCommissions() {
		return commissions;
	}

	public void setCommissions(BigDecimalExt commissions) {
		this.commissions = commissions;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
