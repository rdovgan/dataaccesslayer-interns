package com.mybookingpal.dal.shared.fee;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.shared.Fee;

@Component(value = "FeeSearch")
@Scope(value = "prototype")
public class FeeSearch extends Fee {

    private List<Integer> entityTypes;
    private List<String> productIds;
    private String searchName;
    private String secondNameSearchParameter;

    public FeeSearch () {

    }

    public FeeSearch(String searchName, String secondNameSearchParameter) {
        this.searchName = searchName;
        this.secondNameSearchParameter = secondNameSearchParameter;
    }
    
    public List<Integer> getEntityTypes() {
        return entityTypes;
    }
    public void setEntityTypes(List<Integer> entityTypes) {
        this.entityTypes = entityTypes;
    }
    public List<String> getProductIds() {
        return productIds;
    }
    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public String getSecondNameSearchParameter() {
        return secondNameSearchParameter;
    }

    public void setSecondNameSearchParameter(String secondNameSearchParameter) {
        this.secondNameSearchParameter = secondNameSearchParameter;
    }
}
