package com.mybookingpal.dal.shared;

import java.time.LocalDate;

public class IdDateDto {

	private Integer id;
	private LocalDate date;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

}
