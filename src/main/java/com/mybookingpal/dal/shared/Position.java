package com.mybookingpal.dal.shared;

public class Position {

    public static final String[] STATES = { Location.CREATED};

    protected Double latitude;
    protected Double longitude;
    private Double altitude;
    private int status;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public boolean noLatLng() {
        return latitude == null || longitude == null; // || altitude == null;
    }

    public String getState() {
        return Location.CREATED;
    }

    public void setState(String state) {
        //this.state = state;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Position [latitude=");
        builder.append(latitude);
        builder.append(", longitude=");
        builder.append(longitude);
        builder.append(", altitude=");
        builder.append(altitude);
        builder.append(", status=");
        builder.append(status);
        builder.append("]");
        return builder.toString();
    }
}
