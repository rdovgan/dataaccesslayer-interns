package com.mybookingpal.dal.shared;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;



/**
 * @author Danijel Blagojevic
 *
 */
@Component("ProductBedroom")
public class ProductBedroom {

	private static final Logger LOG = Logger.getLogger(ProductBedroom.class);

	private Integer id;
	private Integer productId;
	private Integer beds;
	private Integer guests;
	private Type type;
	private Boolean privateBathroom;

	public enum Type {
		BEDROOM("Bedroom"),
		LIVING_ROOM("Living Room"),
		BATHROOM("Bathroom");

		String type;

		Type(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}

		public static Type getByValue(String typeValue) {
			String str = typeValue.trim();
			for (Type type : values()) {
				if (type.getType().equals(str)) {
					return type;
				}
			}
			LOG.error("Attempted to get an  com.mybookingpal.dal.shared.ProductBedroom.Type that does not exist: " + typeValue + " returning null");
			return null;
		}
	}

	public ProductBedroom() {

	}

	public ProductBedroom(Integer productId, Integer beds, Integer guests, Type type, Boolean privateBathroom) {
		super();
		this.productId = productId;
		this.beds = beds;
		this.guests = guests;
		this.type = type;
		if (privateBathroom != null) {
			this.privateBathroom = privateBathroom;
		} else {
			this.privateBathroom = false;
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getBeds() {
		return beds;
	}

	public void setBeds(Integer beds) {
		this.beds = beds;
	}

	public Integer getGuests() {
		return guests;
	}

	public void setGuests(Integer guests) {
		this.guests = guests;
	}

	public String getType() {
		if (type != null) {
			return type.getType();
		}
		return null;
	}

	public void setType(String type) {
		this.type = Type.getByValue(type);
	}

	public Boolean getPrivateBathroom() {
		return privateBathroom;
	}

	public void setPrivateBathroom(Boolean privateBathroom) {
		this.privateBathroom = privateBathroom;
	}

}
