package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ChannelPartyToProduct {
	private Integer id;
	private String productId;
	private String partyId;
	private String supplierId;
	private String channelProductId;
	private Date version;
	private Date createdDate;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getChannelProductId() {
		return channelProductId;
	}
	public void setChannelProductId(String channelProductId) {
		this.channelProductId = channelProductId;
	}
	public Date getVersion() {
		return version;
	}
	public void setVersion(Date version) {
		this.version = version;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ChannelPartyToProduct that = (ChannelPartyToProduct) o;

		if (!id.equals(that.id))
			return false;
		if (!productId.equals(that.productId))
			return false;
		if (!partyId.equals(that.partyId))
			return false;
		if (!supplierId.equals(that.supplierId))
			return false;
		return true;
	}

	@Override public String toString() {
		return "ChannelPartyToProduct{" +
				"id=" + id +
				", productId='" + productId + '\'' +
				", partyId='" + partyId + '\'' +
				", supplierId='" + supplierId + '\'' +
				", channelProductId='" + channelProductId + '\'' +
				", version=" + version +
				", createdDate=" + createdDate +
				'}';
	}
}