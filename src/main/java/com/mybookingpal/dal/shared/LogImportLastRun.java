package com.mybookingpal.dal.shared;

import java.util.Date;

public class LogImportLastRun {
	
	public enum ImportTypeEnum {
		UNKNOWN(0),
		PRODUCTS(1),
		IMAGES(2),
		PRICES(3),
		SCHEDULES(4),
		ALERTS(5),
		ADDITION_COSTS(6),
		DESCRIPTIONS(7),
		SPECIALS(8),
		VALIDATION(9),
		ALL(10),
		RESTRICTIONS(11),
		SCHEDULE_DELTA(12),
		PRICES_DELTA(13),
		ADDITION_COSTS_DELTA(14);

		public static ImportTypeEnum getByInt(int value) {
			for (ImportTypeEnum v : values()) {
				if (v.value == value) {
					return v;
				}
			}
			return UNKNOWN;
		}

		public static String getNameByInt(Integer value) {
			return getByInt(value).name();
		}

		private Integer value;

		ImportTypeEnum(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return this.value;
		}
	}
	
	public enum LogImportStatusEnum{
		RUNNING("Running"), FINISHED("Finished"), FAILED("Failed");
		
		public static LogImportStatusEnum getByString(String value) {
			for (LogImportStatusEnum v : values()) {
				if (v.value == value) {
					return v;
				}
			}
			return RUNNING;
		}
		
		public static String getNameByString(String value) {
			return getByString(value).name();
		}
		
		private String value;
		
		LogImportStatusEnum(String value){
			this.value = value;
		}
		
		public String getValue() {
			return this.value;
		}
	}

	private int id;
	private String partyId;
	private String productId;
	private ImportTypeEnum importType;
	private Date startTime;
	private Date endTime;
	private Date effectiveTime;
	private Byte runType;
	private String emailAlert;
	private LogImportStatusEnum status;

	public LogImportLastRun() {
		super();
		// Default values
		setProductId("0");
	}

	public LogImportLastRun(String partyId, ImportTypeEnum importType, Date startTime, Byte runType) {
		this();
		this.partyId = partyId;
		this.importType = importType;
		this.startTime = startTime;
		this.endTime = new Date();
		this.runType = runType;
		this.status = LogImportStatusEnum.RUNNING;
	}

	public Date getEffectiveTime() {
		return effectiveTime;
	}
	
	public Date getEndTime() {
		return endTime;
	}

	public int getId() {
		return id;
	}

	public ImportTypeEnum getImportType() {
		return importType;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getProductId() {
		return productId;
	}

	public Byte getRunType() {
		return runType;
	}

	public Date getStartTime() {
		return startTime;
	}

	public String getEmailAlert() {
		return emailAlert;
	}

	public LogImportStatusEnum getStatus() {
		return status;
	}

	public void setEffectiveTime(Date effectiveTime) {
		this.effectiveTime = effectiveTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setImportType(ImportTypeEnum importType) {
		this.importType = importType;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public void setRunType(Byte runType) {
		this.runType = runType;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public void setEmailAlert(String emailAlert) {
		this.emailAlert = emailAlert;
	}

	public void setStatus(LogImportStatusEnum status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "LogImportLastRun [id=" + id + ", partyId=" + partyId + ", productId=" + productId + ", importType=" + importType + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", effectiveTime=" + effectiveTime + ", emailAlert=" + emailAlert + ", status=" + status + "]";
	}

}