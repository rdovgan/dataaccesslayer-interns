package com.mybookingpal.dal.shared;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

import com.mybookingpal.dal.entity.IsProduct;
import com.mybookingpal.dal.enums.ProductConstants;
import com.mybookingpal.dal.shared.api.IsProductWithTimeZone;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
//TODO remove #Entity extending
public class Product extends Entity implements IsProduct, IsProductWithTimeZone {

	private String altsupplierid; 
	
	private String partofid;
	private String ownerid;
	private String supplierid;
	private String unspsc;
	private String code;
	private String webaddress;
	private String physicaladdress;
	private String tax;
	private String servicedays; //service weekdays Sum = 0
	
	private Integer bed;
	private Integer room;
	private Integer livingPlace;
	private Double bathroom;
	private Integer toilet;
	private Integer floor;
	private String space;
	private SpaceUnitEnum spaceUnit ;
	private Integer quantity;
	private Integer person;
	private Integer standardPerson;
	private Integer child;
	private Integer infant;
	private Integer rating;
	private Integer linenchange; //frequency of linen change in days
	private Integer refresh; //frequency of refresh in days
	private Double commission; // manager's commission
	private Double discount; // standard agent's discount
	private Double securitydeposit; // property manager securitydeposit
	private Double cleaningfee; // property manager cleaning fee
	private Boolean useonepricerow; //if property have more prices for one starting date. Also in that case date and toDate in price table must be used for start and end reservation date.
	private String inquireState;
	private Integer linkedId;
	
	private String pmAbbrev;
	private String pmsAbbrev;

	private Date builtDate;
	private Date renovatingDate;
	private Date rentingDate;
	private Boolean hostLocation;

	private Boolean displayaddress=true;
	private Time checkInTime;
	private Time checkOutTime;
	private String displayname;
	private Boolean usedisplayname;
	private Integer addressComponentsId;
	private TimeZone timeZone;
	private String parentId;
	private String multiUnit;
	private ProductConstants.ProductGroup productGroup;
	private String phone;
	private String taxNumber;
	private Date touristLicenseExpiryDate;
	private Integer bookingSetting;
	private Double basePrice; // A nightly base price for a property when no date is specified.

	public Date getTouristLicenseExpiryDate() {
		return touristLicenseExpiryDate;
	}

	public void setTouristLicenseExpiryDate(Date touristLicenseExpiryDate) {
		this.touristLicenseExpiryDate = touristLicenseExpiryDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone zoneId) {
		this.timeZone = zoneId;
	}
	
	public Integer getBookingSetting() {
        return bookingSetting;
    }

    public void setBookingSetting(Integer bookingSetting) {
        this.bookingSetting = bookingSetting;
    }

	/**
	 * @return the displayaddress
	 */
	public Boolean getDisplayaddress() {
		return displayaddress;
	}

	/**
	 * @param displayaddress the displayaddress to set
	 */
	public void setDisplayaddress(Boolean displayaddress) {
		this.displayaddress = displayaddress;
	}

	public Double getCleaningfee() {
		return cleaningfee != null ? cleaningfee : 0.0;
	}

	public void setCleaningfee(Double cleaningfee) {
		this.cleaningfee = cleaningfee;
	}


	public void setAltSupplierId(String altsupplierid){
		this.altsupplierid = altsupplierid;
	}
	public String getAltSupplierId(){
		return altsupplierid;
	}
	

	private Boolean assignedtomanager;
	public Boolean getAssignedtomanagerValue() {
		// CS: always return boolean value, null is false
		Boolean value = false;
		value = assignedtomanager != null ? assignedtomanager: false;
		return value;
	}

	public Boolean getAssignedtomanager(){
		return assignedtomanager;
	}

	public void setAssignedtomanager(Boolean assignedtomanager) {
		this.assignedtomanager = assignedtomanager;
	}
	
	private Double ownerdiscount; // special owner's discount
	private Double rank; // product rank, negative if off line
	private Boolean dynamicpricingenabled;
	private ArrayList<com.mybookingpal.utils.entity.NameId> files;
	
	private String productImageRootLocation;

	public Product() {
		super(NameIdUtils.Type.Product.name());
		setRank(0.);
		setUseonepricerow(false);
		setPerson(ProductConstants.DEFAULT_PERSON_COUNT);
		setChild(ProductConstants.DEFAULT_CHILD_COUNT);
		setInquireState(ProductConstants.SEND_EMAIL);
		setCheckInTime(ProductConstants.DEFAULT_CHECK_IN_TIME);
		setCheckOutTime(ProductConstants.DEFAULT_CHECK_OUT_TIME);
		setStandardPerson(ProductConstants.DEFAULT_STANDARD_PERSON_COUNT);
	}


	public boolean isValidState() {return hasState(ProductConstants.STATES);}
	
	public boolean notValidState() {return !isValidState();}
	
	public boolean isActiveAndDistributed() {
		return (hasState(ProductConstants.CREATED) && assignedtomanager);
	}
	
	public String getPartofid() {
		return partofid;
	}

	public void setPartofid(String partofid) {
		this.partofid = partofid;
	}

	public boolean noPartofid() {
		return partofid == null || partofid.isEmpty();
	}
	
	public boolean hasPartofid() {
		return !noPartofid();
	}

	public String getOwnerid() {
		return ownerid;
	}

	public void setOwnerid(String ownerid) {
		this.ownerid = ownerid;
	}

	public boolean noOwnerid() {
		return ownerid == null || ownerid.trim().isEmpty();
	}
	
	public boolean hasOwnerid() {
		return !noOwnerid();
	}

	public boolean hasOwnerid(String ownerid) {
		return this.ownerid == null ? false : this.ownerid.equalsIgnoreCase(ownerid);
	}

	public String getSupplierid() {
		return supplierid;
	}

	public Integer getSupplierId() {
		return NumberUtils.createInteger(supplierid);
	}

	public void setSupplierid(String supplierid) {
		this.supplierid = supplierid;
	}

	public boolean noSupplierid() {
		return supplierid == null || supplierid.trim().isEmpty();
	}
	
	public boolean hasSupplierid() {
		return !noSupplierid();
	}

	public boolean hasSupplierid(String supplierid) {
		return this.supplierid == null ? false : this.supplierid.equalsIgnoreCase(supplierid);
	}

	public String getUnspsc() {
		return unspsc;
	}

	public void setUnspsc(String unspsc) {
		this.unspsc = unspsc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getWebaddress() {
		return webaddress;
	}

	public void setWebaddress(String webaddress) {
		this.webaddress = webaddress;
	}

	public String getPhysicaladdress() {
		return physicaladdress;
	}

	public void setPhysicaladdress(String physicaladdress) {
		this.physicaladdress = physicaladdress;
	}

	public String[] getAddress() {
		return physicaladdress == null ? null : physicaladdress.split("[\\r\\n]+");
	}
	
	public String getFormattedAddress() {
		return physicaladdress == null ? null : physicaladdress.replaceAll("[\\t\\n\\r]+",", ");
	}

	public void setAddress(String[] address) {
		//
	}
	
	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getServicedays() {
		return servicedays;
	}

	public void setServicedays(String servicedays) {
		this.servicedays = servicedays;
	}

	public boolean noServicedays() {
		return servicedays == null || servicedays.equals("0000000");
	}
	
	public boolean hasServicedays() {
		return !noServicedays();
	}

	public String getLocationId() {
		return locationid;
	}

	public void setLocationId(String locationId) {
		super.locationid = locationId;
	}
	
	public Integer getBed() {
		if(bed != null){
			return bed;
		}else{
			return 0;
		}
	}

	public void setBed(Integer bed) {
		this.bed = bed;
	}

	public Integer getRoom() {
		return room;
	}

	public void setRoom(Integer room) {
		this.room = room;
	}


	public Double getBathroom() {
		return bathroom;
	}

	public void setBathroom(Double bathroom) {
		this.bathroom = bathroom;
	}

	public Integer getToilet() {
		return toilet;
	}

	public void setToilet(Integer toilet) {
		this.toilet = toilet;
	}
	
	public Integer getFloor() {
		return floor;
	}

	public void setFloor(Integer floor) {
		this.floor = floor;
	}
	
	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public boolean noQuantity() {
		return quantity == null || quantity <= 1;
	}
	
	public boolean hasQuantity() {
		return !noQuantity();
	}
	
	public Integer getPerson() {
		return person;
	}

	public void setPerson(Integer person) {
		this.person = person;
	}

	public Integer getAdult() {
		return person;
	}

	public void setAdult(Integer person) {
		this.person = person;
	}
	
	public Integer getStandardPerson() {
		return standardPerson;
	}

	public void setStandardPerson(Integer standardPerson) {
		this.standardPerson = standardPerson;
	}

	public Integer getChild() {
		return child;
	}

	public void setChild(Integer child) {
		this.child = child;
	}

	public Integer getInfant() {
		return infant;
	}

	public void setInfant(Integer infant) {
		this.infant = infant;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Integer getLinenchange() {
		return linenchange;
	}

	public void setLinenchange(Integer linenchange) {
		this.linenchange = linenchange;
	}

	public Integer getRefresh() {
		return refresh;
	}

	public void setRefresh(Integer refresh) {
		this.refresh = refresh;
	}

	public Double getCommissionValue() {
		return (commission == null) ? 0 : commission; 
	}
	
	public Double getCommission() {
		return  commission; 
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}
	
	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getOwnerdiscount() {
		return ownerdiscount;
	}

	public void setOwnerdiscount(Double ownerdiscount) {
		this.ownerdiscount = ownerdiscount;
	}

	public Double getRank() {
		return rank;
	}

	public void setRank(Double rank) {
		this.rank = rank;
	}

	public boolean noRank() {
		return rank == null || rank < 0.0;
	}
	
	public boolean hasRank() {
		return !noRank();
	}

	
	public Boolean getDynamicpricingenabled() {
		return dynamicpricingenabled;
	}

	public void setDynamicpricingenabled(Boolean dynamicpricingenabled) {
		this.dynamicpricingenabled = dynamicpricingenabled;
	}

	public Boolean getUseonepricerow() {
		return useonepricerow;
	}

	public void setUseonepricerow(Boolean useonepricerow) {
		this.useonepricerow = useonepricerow;
	}
	
	public Boolean IsUseonepricerow() {
		return (useonepricerow == null || !useonepricerow) ? false : true;
	}
	
	
	public ArrayList<com.mybookingpal.utils.entity.NameId> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<com.mybookingpal.utils.entity.NameId> files) {
		this.files = files;
	}
	
	public Double getSecuritydeposit() {
		return securitydeposit;
	}

	public void setSecuritydeposit(Double securitydeposit) {
		this.securitydeposit = securitydeposit;
	}

	// ---------------------------------------------------------------------------------
	// Private text is specific to an organization
	// ---------------------------------------------------------------------------------
	//NOT A GETTER SO DO NOT ANNOTATE
	public static String getPrivateId(String organizationid, String id) {
		return organizationid + NameIdUtils.Type.Product.name() + id;
	}

	
	public String getPrivateLabel() {
		return "Private Notes"; // make multilingual
	}

	public Text getPrivateText(String organizationid) {
		return getText(organizationid, NameIdUtils.Type.Product, id);
	}

	public void setPrivateText(String organizationid, Text value) {
		setText(organizationid, NameIdUtils.Type.Product, id, value);
	}

	// ---------------------------------------------------------------------------------
	// Public text 
	// ---------------------------------------------------------------------------------
	public String getPublicId() {
		return id == null || id.isEmpty() ? null : NameIdUtils.Type.Product.name() + id + Text.Code.Public.name();
	}

	public String getPublicLabel() {
		return "Description"; // make multilingual
	}

	
	public Text getPublicText() {
		return getText(NameIdUtils.Type.Product, id, Text.Code.Public);
	}

	public void setPublicText(Text value) {
		setText(NameIdUtils.Type.Product, id, Text.Code.Public, value);
	}

	// ---------------------------------------------------------------------------------
	// Inside text 
	// ---------------------------------------------------------------------------------
	public String getInsideId() {
		return id == null || id.isEmpty() ? null : NameIdUtils.Type.Product.name() + id + Text.Code.Inside.name();
	}

	// ---------------------------------------------------------------------------------
	// Outside text 
	// ---------------------------------------------------------------------------------
	public String getOutsideId() {
		return id == null || id.isEmpty() ? null : NameIdUtils.Type.Product.name() + id + Text.Code.Outside.name();
	}

	// ---------------------------------------------------------------------------------
	// Product check in text
	// ---------------------------------------------------------------------------------
	public String getCheckinId() {
		return id == null || id.isEmpty() ? null : NameIdUtils.Type.Product.name() + id + Text.Code.Checkin.name();
	}

	public String getCheckinLabel() {
		return "Check In Instructions"; // make multilingual
	}

	
	public Text getCheckinText() {
		return getText(NameIdUtils.Type.Product, id, Text.Code.Checkin);
	}

	public void setCheckinText(Text value) {
		setText(NameIdUtils.Type.Product, id, Text.Code.Checkin, value);
	}

	// ---------------------------------------------------------------------------------
	// Product contents text
	// ---------------------------------------------------------------------------------
	public String getContentsId() {
		return id == null || id.isEmpty() ? null : NameIdUtils.Type.Product.name() + id + Text.Code.Contents.name();
	}

	public String getContentsLabel() {
		return "What's Included"; // make multilingual
	}

	
	public Text getContentsText() {
		return getText(NameIdUtils.Type.Product, id, Text.Code.Contents);
	}

	public void setContentsText(Text value) {
		setText(NameIdUtils.Type.Product, id, Text.Code.Contents, value);
	}

	// ---------------------------------------------------------------------------------
	// Product options text
	// ---------------------------------------------------------------------------------
	public String getOptionsId() {
		return id == null || id.isEmpty() ? null : NameIdUtils.Type.Product.name() + id + Text.Code.Options.name();
	}

	public String getOptionsLabel() {
		return "Available Options"; // make multilingual
	}

	
	public Text getOptionsText() {
		return getText(NameIdUtils.Type.Product, id, Text.Code.Options);
	}

	public void setOptionsText(Text value) {
		setText(NameIdUtils.Type.Product, id, Text.Code.Options, value);
	}


	//---------------------------------------------------------------------------------
	// Conditions of use text 
	//---------------------------------------------------------------------------------
	public final String getConditionsId() {
		return id == null || id.isEmpty() ? null : NameIdUtils.Type.Product.name() + id + Text.Code.Condition.name();
	}

	public String getConditionsLabel() {
		return "Guest Conditions"; // make multilingual
	}

	
	public Text getConditionsText() {
		return getText(NameIdUtils.Type.Product, id, Text.Code.Condition);
	}

	public void setConditionsText(Text value){
		setText(NameIdUtils.Type.Product, id, Text.Code.Condition, value);
	}
	
	//---------------------------------------------------------------------------------
	// Specials of use text 
	//---------------------------------------------------------------------------------
	public final String getSpecialsId() {
		return id == null || id.isEmpty() ? null : NameIdUtils.Type.Product.name() + id + Text.Code.Special.name();
	}
	
	public String getSpecialsLabel() {
		return "Special cases"; // make multilingual
	}
	
	
	public Text getSpecialsText() {
		return getText(NameIdUtils.Type.Product, id, Text.Code.Special);
	}
	
	public void setSpecialsText(Text value){
		setText(NameIdUtils.Type.Product, id, Text.Code.Special, value);
	}

	//---------------------------------------------------------------------------------
	// Service text 
	//---------------------------------------------------------------------------------
	public static final String getServiceId(String id){
		return id == null || id.isEmpty() ? null : NameIdUtils.Type.Product.name() + id + Text.Code.Service.name();
	}

	public String getServiceLabel() {
		return "Service Description"; // make multilingual
	}

	
	public Text getServiceText(){
		return getText(NameIdUtils.Type.Product, id, Text.Code.Service);
	}

	public void setServiceText(Text value, String type){
		setText(NameIdUtils.Type.Product, id, Text.Code.Service, value);
	}

	public String getPmAbbrev() {
		return pmAbbrev;
	}

	public void setPmAbbrev(String pmAbbrev) {
		this.pmAbbrev = pmAbbrev;
	}

	public String getPmsAbbrev() {
		return pmsAbbrev;
	}

	public void setPmsAbbrev(String pmsAbbrev) {
		this.pmsAbbrev = pmsAbbrev;
	}
	
	public String getProductImageRootLocation() {
		return productImageRootLocation;
	}

	public void setProductImageRootLocation(String productImageRootLocation) {
		this.productImageRootLocation = productImageRootLocation;
	}

	
	public String getInquireState() {
		return inquireState;
	}

	public void setInquireState(String inquireState) {
		this.inquireState = inquireState;
	}

	
	public Time getCheckInTime() {
	    return checkInTime;
	}

	public void setCheckInTime(Time checkInTime) {
	    this.checkInTime = checkInTime;
	}

	
	public Time getCheckOutTime() {
	    return checkOutTime;
	}

	public void setCheckOutTime(Time checkOutTime) {
	    this.checkOutTime = checkOutTime;
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public Boolean isUsedisplayname() {
		return usedisplayname;
	}

	public void setUseDisplayName(Boolean usedisplayname) {
		this.usedisplayname = usedisplayname;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getMultiUnit() {
		return multiUnit;
	}

	public void setMultiUnit(String multiUnit) {
		this.multiUnit = multiUnit;
	}
	
	public Integer getLinkedId() {
		return linkedId;
	}

	public void setLinkedId(Integer linkedId) {
		this.linkedId = linkedId;
	}

	@Override
	public String getName() {
		String actualName = this.name;
		if (isUsedisplayname()!=null && isUsedisplayname()  && getDisplayname() != null && !"".equals(getDisplayname())) {
			actualName = this.displayname;
		}
		
		return actualName;
	}
	
	public String getOriginalName() {
		return this.name;
	}

	public Integer getAddressComponentsId() {
		return addressComponentsId;
	}

	public void setAddressComponentsId(Integer addressComponentsId) {
		this.addressComponentsId = addressComponentsId;
	}

	public enum SpaceUnitEnum {
		SQ_M("m2"), SQ_FT("ft2");

		private String value;

		SpaceUnitEnum(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static SpaceUnitEnum getByStr(String value) {
			for (SpaceUnitEnum v : values()) {
				if (v.value.equals(value)) {
					return v;
				}
			}
			return null;
		}

		public static String getNameByStr(String value) {
			return getByStr(value).name();
		}
	}

	public SpaceUnitEnum getSpaceUnit() {
		return spaceUnit;
	}

	public void setSpaceUnit(SpaceUnitEnum spaceUnit) {
		this.spaceUnit = spaceUnit;
	}

	public ProductConstants.ProductGroup getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(ProductConstants.ProductGroup productGroup) {
		this.productGroup = productGroup;
	}

	public Date getBuiltDate() {
		return builtDate;
	}

	public void setBuiltDate(Date builtDate) {
		this.builtDate = builtDate;
	}

	public Date getRenovatingDate() {
		return renovatingDate;
	}

	public Double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(Double basePrice) {
		this.basePrice = basePrice;
	}

	public void setRenovatingDate(Date renovatingDate) {
		this.renovatingDate = renovatingDate;
	}

	public Date getRentingDate() {
		return rentingDate;
	}

	public void setRentingDate(Date rentingDate) {
		this.rentingDate = rentingDate;
	}

	public Boolean getHostLocation() {
		return hostLocation;
	}

	public void setHostLocation(Boolean hostLocation) {
		this.hostLocation = hostLocation;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((altsupplierid == null) ? 0 : altsupplierid.hashCode());
        result = prime * result + ((assignedtomanager == null) ? 0 : assignedtomanager.hashCode());
        result = prime * result + ((displayname == null) ? 0 : displayname.hashCode());
        result = prime * result + ((multiUnit == null) ? 0 : multiUnit.hashCode());
        result = prime * result + ((ownerid == null) ? 0 : ownerid.hashCode());
        result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
        result = prime * result + ((person == null) ? 0 : person.hashCode());
        result = prime * result + ((physicaladdress == null) ? 0 : physicaladdress.hashCode());
        result = prime * result + ((pmAbbrev == null) ? 0 : pmAbbrev.hashCode());
        result = prime * result + ((pmsAbbrev == null) ? 0 : pmsAbbrev.hashCode());
        result = prime * result + ((productGroup == null) ? 0 : productGroup.hashCode());
        result = prime * result + ((room == null) ? 0 : room.hashCode());
        result = prime * result + ((standardPerson == null) ? 0 : standardPerson.hashCode());
        result = prime * result + ((supplierid == null) ? 0 : supplierid.hashCode());
        result = prime * result + ((usedisplayname == null) ? 0 : usedisplayname.hashCode());
        result = prime * result + ((useonepricerow == null) ? 0 : useonepricerow.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Product other = (Product) obj;
        if (altsupplierid == null) {
            if (other.altsupplierid != null)
                return false;
        } else if (!altsupplierid.equals(other.altsupplierid))
            return false;
        if (assignedtomanager == null) {
            if (other.assignedtomanager != null)
                return false;
        } else if (!assignedtomanager.equals(other.assignedtomanager))
            return false;
        if (displayname == null) {
            if (other.displayname != null)
                return false;
        } else if (!displayname.equals(other.displayname))
            return false;
        if (multiUnit == null) {
            if (other.multiUnit != null)
                return false;
        } else if (!multiUnit.equals(other.multiUnit))
            return false;
        if (ownerid == null) {
            if (other.ownerid != null)
                return false;
        } else if (!ownerid.equals(other.ownerid))
            return false;
        if (parentId == null) {
            if (other.parentId != null)
                return false;
        } else if (!parentId.equals(other.parentId))
            return false;
        if (person == null) {
            if (other.person != null)
                return false;
        } else if (!person.equals(other.person))
            return false;
        if (physicaladdress == null) {
            if (other.physicaladdress != null)
                return false;
        } else if (!physicaladdress.equals(other.physicaladdress))
            return false;
        if (pmAbbrev == null) {
            if (other.pmAbbrev != null)
                return false;
        } else if (!pmAbbrev.equals(other.pmAbbrev))
            return false;
        if (pmsAbbrev == null) {
            if (other.pmsAbbrev != null)
                return false;
        } else if (!pmsAbbrev.equals(other.pmsAbbrev))
            return false;
        if (productGroup == null) {
            if (other.productGroup != null)
                return false;
        } else if (!productGroup.equals(other.productGroup))
            return false;
        if (room == null) {
            if (other.room != null)
                return false;
        } else if (!room.equals(other.room))
            return false;
        if (standardPerson == null) {
            if (other.standardPerson != null)
                return false;
        } else if (!standardPerson.equals(other.standardPerson))
            return false;
        if (supplierid == null) {
            if (other.supplierid != null)
                return false;
        } else if (!supplierid.equals(other.supplierid))
            return false;
        if (usedisplayname == null) {
            if (other.usedisplayname != null)
                return false;
        } else if (!usedisplayname.equals(other.usedisplayname))
            return false;
        if (useonepricerow == null) {
            if (other.useonepricerow != null)
                return false;
        } else if (!useonepricerow.equals(other.useonepricerow))
            return false;
        return true;
    }


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [partofid=");
		builder.append(partofid);
		builder.append(", \nownerid=");
		builder.append(ownerid);
		builder.append(", \nsupplierid=");
		builder.append(supplierid);
		builder.append(", \nunspsc=");
		builder.append(unspsc);
		builder.append(", \ncode=");
		builder.append(code);
		builder.append(", \nwebaddress=");
		builder.append(webaddress);
		builder.append(", \nphysicaladdress=");
		builder.append(physicaladdress);
		builder.append(", \ntax=");
		builder.append(tax);
		builder.append(", \nservicedays=");
		builder.append(servicedays);
		builder.append(", \nroom=");
		builder.append(room);
		builder.append(", \nbathroom=");
		builder.append(bathroom);
		builder.append(", \ntoilet=");
		builder.append(toilet);
		builder.append(", \nfloor=");
		builder.append(floor);
		builder.append(", \nquantity=");
		builder.append(quantity);
		builder.append(", \nperson=");
		builder.append(person);
		builder.append(", \nchild=");
		builder.append(child);
		builder.append(", \ninfant=");
		builder.append(infant);
		builder.append(", \nrating=");
		builder.append(rating);
		builder.append(", \nlinenchange=");
		builder.append(linenchange);
		builder.append(", \nrefresh=");
		builder.append(refresh);
		builder.append(", \ncommission=");
		builder.append(commission);
		builder.append(", \ndiscount=");
		builder.append(discount);
		builder.append(", \nownerdiscount=");
		builder.append(ownerdiscount);
		builder.append(", \nrank=");
		builder.append(rank);
		builder.append(", \ndynamicpricingenabled=");
		builder.append(dynamicpricingenabled);
		builder.append(", \nuseonepricerow=");
		builder.append(useonepricerow);
		builder.append(", \nfiles=");
		builder.append(files);
		builder.append(", \ntype=");
		builder.append(type);
		builder.append(", \nentitytype=");
		builder.append(entitytype);
		builder.append(", \nlocationid=");
		builder.append(locationid);
		builder.append(", \ncurrency=");
		builder.append(currency);
		builder.append(", \nunit=");
		builder.append(unit);
		builder.append(", \nlatitude=");
		builder.append(latitude);
		builder.append(", \nlongitude=");
		builder.append(longitude);
		builder.append(", \naltitude=");
		builder.append(altitude);
		builder.append(", \ntaxes=");
		builder.append(taxes);
		builder.append(", \norganizationid=");
		builder.append(organizationid);
		builder.append(", \nstatus=");
		builder.append(status);
		builder.append(", \nstate=");
		builder.append(state);
		builder.append(", \nvalues=");
		builder.append(values);
		builder.append(", \nattributes=");
		builder.append(attributemap);
		builder.append(", \ntexts=");
		builder.append(texts);
		builder.append(", \nimages=");
		builder.append(imageurls);
		builder.append(", \nname=");
		builder.append(name);
		builder.append(", \ndisplayname=");
		builder.append(displayname);
		builder.append(", \nusedisplayname=");
		builder.append(usedisplayname);
		builder.append(", \nlinkedid=");
		builder.append(linkedId);
		builder.append(", \nid=");
		builder.append(id);
		builder.append(", \nhostLocation=");
		builder.append(hostLocation);
		builder.append(", \nbuiltDate=");
		builder.append(builtDate);
		builder.append(", \nrenovatingDate=");
		builder.append(renovatingDate);
		builder.append(", \nrentingDate=");
		builder.append(rentingDate);
		builder.append(", \nbasePrice=");
		builder.append(basePrice);
		builder.append("]");
		return builder.toString();
	}

	public boolean isMultiUnitParent() {
		return ProductConstants.MultiUnit.MLT.name().equals(getMultiUnit()) && (ProductConstants.ProductGroup.MULTI_REP.equals(getProductGroup())
				|| ProductConstants.ProductGroup.MULTI_KEY.equals(getProductGroup()));
	}

	public boolean isProductMultiUnit() {
		return isMultiUnitKeyLevel() || isMultiUnitRepLevel();
	}

	public boolean isMultiUnitKeyLevel() {
		return ProductConstants.ProductGroup.MULTI_KEY.equals(getProductGroup());
	}

	public boolean isMultiUnitRepLevel() {
		return ProductConstants.ProductGroup.MULTI_REP.equals(getProductGroup());
	}

	public Integer getLivingPlace() {
		return livingPlace;
	}

	public void setLivingPlace(Integer livingPlace) {
		this.livingPlace = livingPlace;
	}
}