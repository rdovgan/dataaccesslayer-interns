package com.mybookingpal.dal.shared;

import java.util.Date;

public class IdVersion {
    private String id;
    private Date version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getVersion() {
        return version;
    }

    public void setVersion(Date version) {
        this.version = version;
    }

}
