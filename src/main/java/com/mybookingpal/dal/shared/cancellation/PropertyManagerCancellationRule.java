package com.mybookingpal.dal.shared.cancellation;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("propertyManagerCancellationRule")
public class PropertyManagerCancellationRule {

	public PropertyManagerCancellationRule() {
		super();
	}

	@XStreamOmitField
	protected Integer id;
	@XStreamOmitField
	protected Integer propertyManagerId; 			// ID of PM in Party table   
	protected Integer cancellationRefund;			// Refund value (%) that traveler receive in case cancellation.   
	protected Double cancellationTransactionFee; 	// Fee value that traveler pay for the transaction cancellation.   
	protected Integer cancellationRuleType;
	protected Integer cancellationDate; // Period (days count) at what traveler can cancel reservation if cancelationType=2;
	private Long ratePlanId;
	private Integer productId;
	private Integer cancellationNights;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getCancellationNights() {
		return cancellationNights;
	}

	public void setCancellationNights(Integer cancellationNights) {
		this.cancellationNights = cancellationNights;
	}

	public Integer getPropertyManagerId() {
		return propertyManagerId;
	}

	public void setPropertyManagerId(Integer propertyManagerId) {
		this.propertyManagerId = propertyManagerId;
	}

	public Integer getCancellationRefund() {
		return cancellationRefund;
	}

	public void setCancellationRefund(Integer cancellationRefund) {
		this.cancellationRefund = cancellationRefund;
	}

	public Double getCancellationTransactionFee() {
		return cancellationTransactionFee;
	}

	public void setCancellationTransactionFee(Double cancellationTransactionFee) {
		this.cancellationTransactionFee = cancellationTransactionFee;
	}

	public Integer getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(Integer cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public Long getRatePlanId() {
		return ratePlanId;
	}

	public void setRatePlanId(Long ratePlanId) {
		this.ratePlanId = ratePlanId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getCancellationRuleType() {
		return cancellationRuleType;
	}

	public void setCancellationRuleType(Integer cancellationRuleType) {
		this.cancellationRuleType = cancellationRuleType;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("PropertyManagerCancellationRules [id=");
		sb.append(id);
		sb.append(", \npropertyManagerId=");
		sb.append(propertyManagerId);
		sb.append(", \ncancelationRefund=");
		sb.append(cancellationRefund);
		sb.append(", \ncancelationTransactionFee=");
		sb.append(cancellationTransactionFee);
		sb.append(", \ncancellationRuleType=");
		sb.append(cancellationRuleType);
		sb.append(", \nratePlanId=");
		sb.append(ratePlanId);
		sb.append(", \nproductId=");
		sb.append(productId);

		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cancellationDate == null) ? 0 : cancellationDate.hashCode());
		result = prime * result + ((cancellationNights == null) ? 0 : cancellationNights.hashCode());
		result = prime * result + ((cancellationRefund == null) ? 0 : cancellationRefund.hashCode());
		result = prime * result + ((cancellationRuleType == null) ? 0 : cancellationRuleType.hashCode());
		result = prime * result + ((cancellationTransactionFee == null) ? 0 : cancellationTransactionFee.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((propertyManagerId == null) ? 0 : propertyManagerId.hashCode());
		result = prime * result + ((ratePlanId == null) ? 0 : ratePlanId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropertyManagerCancellationRule other = (PropertyManagerCancellationRule) obj;
		if (cancellationDate == null) {
			if (other.cancellationDate != null)
				return false;
		} else if (!cancellationDate.equals(other.cancellationDate))
			return false;
		if (cancellationNights == null) {
			if (other.cancellationNights != null)
				return false;
		} else if (!cancellationNights.equals(other.cancellationNights))
			return false;
		if (cancellationRefund == null) {
			if (other.cancellationRefund != null)
				return false;
		} else if (!cancellationRefund.equals(other.cancellationRefund))
			return false;
		if (cancellationRuleType == null) {
			if (other.cancellationRuleType != null)
				return false;
		} else if (!cancellationRuleType.equals(other.cancellationRuleType))
			return false;
		if (cancellationTransactionFee == null) {
			if (other.cancellationTransactionFee != null)
				return false;
		} else if (!cancellationTransactionFee.equals(other.cancellationTransactionFee))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (propertyManagerId == null) {
			if (other.propertyManagerId != null)
				return false;
		} else if (!propertyManagerId.equals(other.propertyManagerId))
			return false;
		if (ratePlanId == null) {
			if (other.ratePlanId != null)
				return false;
		} else if (!ratePlanId.equals(other.ratePlanId))
			return false;
		return true;
	}
	
	

}
