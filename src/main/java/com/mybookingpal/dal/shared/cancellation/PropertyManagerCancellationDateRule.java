package com.mybookingpal.dal.shared.cancellation;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class PropertyManagerCancellationDateRule extends PropertyManagerCancellationRule {
	
	public PropertyManagerCancellationDateRule() {
		super();
	}
	
	public PropertyManagerCancellationDateRule(Integer propertyManagerId, Integer cancellationDate, Integer cancellationRefund,
			Double cancellationTransactionFee) {
		super();
		setPropertyManagerId(propertyManagerId);
		setCancellationDate(cancellationDate);
		setCancellationRefund(cancellationRefund);
		setCancellationTransactionFee(cancellationTransactionFee);
	}
	
	public Integer getCancellationRuleType() {
		return 1;
	}
}
