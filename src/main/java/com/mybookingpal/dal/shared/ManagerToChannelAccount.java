package com.mybookingpal.dal.shared;

import java.io.Serializable;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ManagerToChannelAccount  implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer supplierID;
	private Integer channelID;
	private String authToken;
	private Date createdDate = new Date();
	private Date version = new Date();
	private boolean state = false;
	private String credentials;
	private String paymentCredentials;

	public ManagerToChannelAccount() {
	}

	public ManagerToChannelAccount(Integer channelID) {
		super();
		this.channelID = channelID;
	}

	/**
	 * @param id
	 * @param supplierID
	 * @param channelID
	 * @param authToken
	 */
	public ManagerToChannelAccount(Integer supplierID, Integer channelID, String authToken) {
		super();
		this.supplierID = supplierID;
		this.channelID = channelID;
		this.authToken = authToken;
	}

	public ManagerToChannelAccount(Integer supplierID, Integer channelID) {
		super();
		this.supplierID = supplierID;
		this.channelID = channelID;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the supplierID
	 */
	public Integer getSupplierID() {
		return supplierID;
	}

	/**
	 * @param supplierID the supplierID to set
	 */
	public void setSupplierID(Integer supplierID) {
		this.supplierID = supplierID;
	}

	/**
	 * @return the channelID
	 */
	public Integer getChannelID() {
		return channelID;
	}

	/**
	 * @param channelID the channelID to set
	 */
	public void setChannelID(Integer channelID) {
		this.channelID = channelID;
	}

	/**
	 * @return the authToken
	 */
	public String getAuthToken() {
		return authToken;
	}

	/**
	 * @param authToken the authToken to set
	 */
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the version
	 */
	public Date getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Date version) {
		this.version = version;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public String getCredentials() {
		return credentials;
	}

	public void setCredentials(String credentials) {
		this.credentials = credentials;
	}

	public String getPaymentCredentials() {
		return paymentCredentials;
	}

	public void setPaymentCredentials(String paymentCredentials) {
		this.paymentCredentials = paymentCredentials;
	}
}
