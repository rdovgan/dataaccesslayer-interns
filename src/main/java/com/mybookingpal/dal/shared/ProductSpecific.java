package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
@Scope(value = "prototype")
public class ProductSpecific {
	
	public enum BDCPropertyTaxInfo {
		PropertyRegisteredInVcs, PropertyHasVat, PropertyDeclaresRevenue, PropertyTaxCategory
	}

	public enum OrganizationType {
		private_person, professional_company
	}

	private Integer id;
	private Integer partyId;
	private Integer productId;
	private Integer channelId;
	private Integer nameId;
	private String value;
	private Date createdDate;
	private Date version;

	//additional setter to get the ids from myBatis when inserting a list
	public void setStringId(String id) {
		this.id = id == null ? null : Integer.decode(id);
	}
}
