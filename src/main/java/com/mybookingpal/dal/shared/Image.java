package com.mybookingpal.dal.shared;

import javax.xml.bind.annotation.XmlRootElement;

import com.mybookingpal.utils.entity.NameId;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@XmlRootElement(name="text")
@Component("Image")
@Scope(value = "prototype")
public class Image extends ModelTable {

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null)
				? 0
				: name.hashCode());
		result = prime * result + ((productId == null)
				? 0
				: productId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Image other = (Image) obj;
		if (name == null) {
			if (other.name != null) return false;
		} else if (!name.equals(other.name)) return false;
		if (productId == null) {
			if (other.productId != null) return false;
		} else if (!productId.equals(other.productId)) return false;
		return true;
	}

	public enum Code {Checkin, Contents, Condition, Contract, Contact, File, Inside, Options, Outside, Pictures, Private, Public, Service, Url}

	public static Code getCode(String type) {
		if (type == null || type.isEmpty()) {
			return Code.Public;
		}
		try {
			return Code.valueOf(type);
		} catch (Throwable x) {
			return Code.Public;
		}
	}

	public enum State {Initial, Created, Final}
	public enum Type {Hosted, Linked, Blob}
	public enum Size {REGULAR, STANDARD, THUMB}
	/**
	 * Checks if the URL is a valid image file type, otherwise false.
	 * 
	 * @param url the URL of the file.
	 * @return true, if a valid image file type, otherwise false.
	 */
	public static boolean isImageFile(String url){
		if (url == null) {
			return false;
		}
		String filename = url.trim().toLowerCase();
		return filename.endsWith(".jpg")
				|| filename.endsWith(".jpeg")
				|| filename.endsWith(".bmp")
				|| filename.endsWith(".gif")
				|| filename.endsWith(".png");
	}

	/**
	 * Checks if the URL is not a valid image file type, otherwise false.
	 * 
	 * @param url the URL of the file.
	 * @return true, if not a valid image file type, otherwise false.
	 */
	public static boolean notImageFile(String url) {
		return !isImageFile(url);
	}

	/**
	 * Gets the extension type of the specified filename.
	 * 
	 * @param filename the specified filename.
	 */
	public static String getExtension(String filename) {
		String[] args = filename.split("\\.");
		return args[1].toLowerCase();
	}

	/*
	* Get the specified image name for UI imported images
	*/
	public static String trimUIName(String value) {
		if (value == null || value.lastIndexOf('/') <= 0) {
			return (value);
		}
		return value.substring(value.lastIndexOf('/') + 1, value.length());
	}

	/**
	 * Gets a string with any HTML mark up removed.
	 * 
	 * @param html the text from which HTML mark up is to be removed.
	 * @return text without mark up.
	 * @see <pre>http://developer.ean.com/faqs/Development</pre>
	 * TODO:
		xmlText=Replace(xmlText,"&amp;lt;","&lt;")
		xmlText=Replace(xmlText,"&amp;gt;","&gt;")
		xmlText=Replace(xmlText,"&amp;apos;","&apos;")
		xmlText=Replace(xmlText,"&amp;#x0A","")
		xmlText=Replace(xmlText,"&amp;#x0D","")
		xmlText=Replace(xmlText,"&#x0D","")
		xmlText=Replace(xmlText,"&#x0A","")
		xmlText=Replace(xmlText,"&amp;#x09","")
		xmlText=Replace(xmlText,"&amp;amp;amp;","&amp;")
		xmlText=Replace(xmlText,"&lt;br&gt;","<br />")
	 */
	public static String stripHTML(String html){
		if (html == null || html.isEmpty()) {
			return html;
		}
		html = html.replaceAll("&nbsp;", " ");
		html = html.replaceAll("&amp;lt;", "&lt;");
		html = html.replaceAll("&amp;gt;", "&gt;");
		html = html.replaceAll("&amp;apos;", "&apos;");
		html = html.replaceAll("&amp;#x0A", "");
		html = html.replaceAll("&amp;#x0D", "");
		html = html.replaceAll("&#x0D", "");
		html = html.replaceAll("&#x0A", "");
		html = html.replaceAll("&amp;#x09", "");
		html = html.replaceAll("&amp;amp;amp;", "&amp;");
		html = html.replaceAll("&lt;br&gt;", "<br />");
		return html.replaceAll("\\<.*?>", ""); // alt return html.replaceAll("\\<.*?\\>", "");
	}

	//protected int status = 0;
	protected Integer id;
	protected String name;
	protected String oldName;
	//protected String state;
	protected String type;
	protected String notes;
	protected String language;
	protected Integer productId;
	protected byte[] data;
	protected String url;
	// this url is for hosted images
	private String urlMBP;
	protected Boolean standard = false;
	protected Boolean thumbnail = false;
	// 1 - image has type (e.g. "Main Image", "Exterior", "Interior" picture etc.)
	// 2 - default number for images which do not have any type.
	protected Integer sort;
	private Integer height;
	private Integer width;
	private byte[] imageData;
	private String tags;
	private boolean isValid;
	
	public Image() {}

	public Image(Integer id, String language) {
		this.id = id;
		this.language = language;
	}

	public Image(Integer id, String name, Type type, String notes, String language) {
		this.id = id;
		this.name = name;
		this.state = State.Created.name();
		this.type = type == null ? null : type.name().toLowerCase();
		this.notes = notes;
		this.language = language;
	}
	
	public Image(String name, Integer productId, Type type, String notes, String language, String url) {
		this.name = name;
		this.state = State.Created.name();
		this.type = type == null ? null : type.name().toLowerCase();
		this.notes = notes;
		this.language = language;
		this.productId = productId;
		this.url = url;
	}
	
	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? " " : name;
	}

	public String getOldName() {
		return oldName;
	}

	public void setOldName(String oldName) {
		this.oldName = oldName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrlMBP() {
		return urlMBP;
	}

	public void setUrlMBP(String urlMBP) {
		this.urlMBP = urlMBP;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getName(int length) {
		return NameIdUtils.trim(name, ",", length);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type.toLowerCase();
	}

	public void setType(Type type) {
		this.type = type == null ? null : type.name();
	}

	public boolean notType(Type type) {
		return this.type == null || type == null || !this.type.equalsIgnoreCase(type.name());
	}

	public boolean hasType(Type type) {
		return !notType(type);
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes == null ? " " : notes;
	}

	public String getPlainText() {
		if (notes == null) {
			return "";
		}
		return notes.replaceAll("\\<.*?>", "");
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language == null ? "EN" : language;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public Boolean isStandard() {
		return standard;
	}

	public void setStandard(Boolean standard) {
		this.standard = standard;
	}

	public Boolean isThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(Boolean thumbnail) {
		this.thumbnail = thumbnail;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}
	
	public byte[] getImageData() {
		return imageData;
	}

	public void setImageData(byte[] imageData) {
		this.imageData = imageData;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "Image [" +
				"status=" + status +
				", id=" + id +
				", name=" + name +
				", oldName=" + oldName +
				", state=" + state +
				", type=" + type +
				", notes=" + notes +
				", language=" + language +
				", productId=" + productId +
				", url=" + url +
				", urlMBP=" + urlMBP +
				", standard=" + standard +
				", thumbnail=" + thumbnail +
				", sort=" + sort +
				", height=" + height +
				", width=" + width +
				", tags=" + tags +
				']';
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public void setId(String id) {
		if (id != null) {
			this.id = Integer.valueOf(id);
		}
	}

	@Override
	public String getId() {
		return (id != null)
				? id.toString()
				: null;
	}
}
