package com.mybookingpal.dal.shared;

public class ChannelProductRate {
	public String getChannelProductId() {
		return channelProductId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public String getStarndardRateId() {
		return starndardRateId;
	}

	public String getWeeklyRateId() {
		return weeklyRateId;
	}

	public String getWeekendRateId() {
		return weekendRateId;
	}

	public String getMonthlyRateId() {
		return monthlyRateId;
	}

	public String getBpProductId() {
		return bpProductId;
	}

	public String getRoomId() {
		return roomId;
	}

	private String channelProductId;
	private String propertyName;
	private String starndardRateId;
	private String weeklyRateId;
	private String weekendRateId;
	private String monthlyRateId;

	private String bpProductId;
	private String roomId;

	ChannelProductRate(String channelProductId, String propertyName, String standardRateId, String weeklyRateId,
			String weekendRateId, String monthlyRateId) {
		this.channelProductId = channelProductId;
		this.propertyName = propertyName;
		this.starndardRateId = standardRateId;
		this.weeklyRateId = weeklyRateId;
		this.weekendRateId = weekendRateId;
		this.monthlyRateId = monthlyRateId;

	}

	public ChannelProductRate(String channelProductId, String propertyName, String starndardRateId,
			String weeklyRateId, String weekendRateId, String monthlyRateId, String bpProductId, String roomId) {
		super();
		this.channelProductId = channelProductId;
		this.propertyName = propertyName;
		this.starndardRateId = starndardRateId;
		this.weeklyRateId = weeklyRateId;
		this.weekendRateId = weekendRateId;
		this.monthlyRateId = monthlyRateId;
		this.bpProductId = bpProductId;
		this.roomId = roomId;
	}

}
