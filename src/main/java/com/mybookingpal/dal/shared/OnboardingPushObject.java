package com.mybookingpal.dal.shared;

public class OnboardingPushObject {

    public enum JobType{
        OPEN,CLOSE,UPDATE_STATIC_DATA,RATES_AND_AVAILABILITY,BUILD,READY_TO_CHECK,READY_TO_REVIEW,IMAGE, SYNC_OPPORTUNITY, REPLY_OPPORTUNITY, CANCELLATION_POLICIES
    }

    private Integer channelId;

    private Integer supplierId;

    private Integer productId;

    private boolean success;

    private JobType jobType;

    private String error;

    private String elasticId;

    private String key;

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getElasticId() {
        return elasticId;
    }

    public void setElasticId(String elasticId) {
        this.elasticId = elasticId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public JobType getJobType() {
        return jobType;
    }
}
