package com.mybookingpal.dal.shared;

import java.util.Date;

import javax.xml.bind.annotation.XmlTransient;

import com.mybookingpal.utils.entity.NameId;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class Text {
	
	protected int status = 0;
	protected String id;
	protected String name;
	protected String state;
	protected String type;
	protected String notes;
	protected String language;
	protected Date date;
	protected byte[] data;
	private Date version; //latest change

	public enum Code {
		Checkin, Contents, Condition, Contract, Contact, File, Inside, Options, Outside, Pictures, Private, Public, Service, Url, Special
	}
	
	public enum State {Initial, Created, Final}
	public enum Type {Blob, File, Image, ImageBlob, PublicFile, Text, HTML}

	public Text() {}

	public Text(String id, String language) {
		this.id = id;
		this.language = language;
	}

	public Text(String id, String name, Type type, Date date, String notes, String language) {
		this.id = id;
		this.name = name;
		this.state = State.Created.name();
		this.type = type == null ? null : type.name();
		this.date = date;
		this.notes = notes;
		this.language = language;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean noId() {
		return id == null || id.isEmpty() || id.equals(Model.ZERO);
	}

	public boolean hasId() {
		return !noId();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? " " : name;
	}

	public String getName(int length) {
		return NameIdUtils.trim(name, ",", length);
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setType(Type type) {
		this.type = type == null ? null : type.name();
	}

	public boolean notType(Type type) {
		return this.type == null || type == null || !this.type.equalsIgnoreCase(type.name());
	}

	public boolean hasType(Type type) {
		return !notType(type);
	}

	@XmlTransient
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public boolean noStatus() {
		return status == 0;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes == null ? " " : notes;
	}

	public boolean noNotes() {
		return notes == null || notes.trim().isEmpty();
	}

	public String getPlainText() {
		if (notes == null){return "";}
		return notes.replaceAll("\\<.*?>","");
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language == null ? "EN" : language;
	}

	public boolean noLanguage() {
		return language == null || language.isEmpty();
	}

	public boolean hasLanguage(String language) {
		return this.language != null && this.language.equalsIgnoreCase(language);
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date == null ? new Date() : date;
	}

	public boolean noDate() {
		return date == null;
	}

	public byte[] getData() {
		return data;
	}

	public String getDataString() {
		if (data == null) {return null;}
		String value = new String();
		for (byte datum : data) {value += (char)datum;}
		return value;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public boolean noData(long min, long max) {
		return data == null || data.length < min || data.length > max;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Text [status=");
		builder.append(status);
		builder.append(", id=");
		builder.append(id);
		builder.append(", language=");
		builder.append(language);
		builder.append(", name=");
		builder.append(name);
		builder.append(", type=");
		builder.append(type);
		builder.append(", state=");
		builder.append(state);
		builder.append(", date=");
		builder.append(date);
		builder.append(", notes=");
		builder.append(notes);
//		builder.append(", data=");
//		builder.append(Arrays.toString(data));
		builder.append("]");
		return builder.toString();
	}
}
