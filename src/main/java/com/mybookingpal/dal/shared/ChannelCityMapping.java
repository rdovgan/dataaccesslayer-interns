package com.mybookingpal.dal.shared;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ChannelCityMapping {
	private String id;
	// BookingPal location ID from location table
	private String locationId;
	// Channel city name
	private String cityName;
	// Channel ID
	private String channelId;
	// Channel city code
	private String channelCityCode;
	// ISO Code
	private String countryCode;
	private String continent;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getChannelCityCode() {
		return channelCityCode;
	}

	public void setChannelCityCode(String channelCityCode) {
		this.channelCityCode = channelCityCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	@Override
	public String toString() {
		return "{\"ChannelCityMapping\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"locationId\":\"" + locationId + "\""
				+ ", \"cityName\":\"" + cityName + "\""
				+ ", \"channelId\":\"" + channelId + "\""
				+ ", \"channelCityCode\":\"" + channelCityCode + "\""
				+ ", \"countryCode\":\"" + countryCode + "\""
				+ ", \"continent\":\"" + continent + "\""
				+ "}}";
	}
}
