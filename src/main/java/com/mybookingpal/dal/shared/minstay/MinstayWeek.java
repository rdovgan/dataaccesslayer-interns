package com.mybookingpal.dal.shared.minstay;

import java.time.LocalDate;
import java.util.Date;

import com.mybookingpal.rest.xmladapters.LocalDateAdapter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.shared.MinStay;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Component("MinstayWeek")
@Scope(value = "prototype")
public class MinstayWeek extends MinStay {
	private LocalDate fromDate;
	private LocalDate toDate;
	private String entityId;

	@XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateAdapter.class)
	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateAdapter.class)
	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}
}
