package com.mybookingpal.dal.shared.minstay;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.shared.MinStay;

@Component("MinstaySearch")
@Scope(value = "prototype")
public class MinstaySearch extends MinStay {
    
    private List<String> productIds;

    public List<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }
            
}
