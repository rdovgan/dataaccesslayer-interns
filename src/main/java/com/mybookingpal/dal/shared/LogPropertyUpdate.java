package com.mybookingpal.dal.shared;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 * @author Danijel Blagojevic
 *
 */
@Component
@Scope(value = "prototype")
public class LogPropertyUpdate {

	private Integer id;
	private String productId;
	private Integer partyId;
	private UpdateTypeEnum updateType;
	private Date createdDate;
	private Date version;

	public LogPropertyUpdate() {
	}

	public LogPropertyUpdate(UpdateTypeEnum updateType) {
		this.updateType = updateType;
		this.createdDate = new Date();
	}

	public enum UpdateTypeEnum {
		Product,
		Attributes,
		Images,
		Description,
		Prices,
		MinStay,
		Restrictions,
		Fees,
		Taxes,
		Yields,
		ProductPerson,
		ProductCancelled,
		AddressComponents,
		ProductActivated;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Integer getPartyId() {
		return partyId;
	}

	public void setPartyId(Integer partyId) {
		this.partyId = partyId;
	}

	public UpdateTypeEnum getUpdateType() {
		return updateType;
	}

	public void setUpdateType(UpdateTypeEnum updateType) {
		this.updateType = updateType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "LogPropertyUpdate [id=" + id + ", productId=" + productId + ", partyId=" + partyId + ", updateType=" + updateType + ", createdDate="
				+ createdDate + ", version=" + version + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((partyId == null) ? 0 : partyId.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((updateType == null) ? 0 : updateType.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogPropertyUpdate other = (LogPropertyUpdate) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (partyId == null) {
			if (other.partyId != null)
				return false;
		} else if (!partyId.equals(other.partyId))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (updateType != other.updateType)
			return false;
		return true;
	}

}
