package com.mybookingpal.dal.shared;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ChannelProductValidationHistory {

    private Integer id;

    private Integer productId;

    private Integer channelId;

    private boolean isValid;

    private String errors;

    private LocalDateTime createdDate;

}
