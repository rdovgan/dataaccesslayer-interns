package com.mybookingpal.dal.shared.marriott;

import java.util.Date;
import java.util.List;

public class ChannelPortalFilterDto {

	private String filterDate;
	private Date startDate;
	private Date endDate;
	private Date checkInStartDate;
	private Date checkInEndDate;
	private Integer reservationId;
	private String confirmationId;
	private Integer customerId;
	private Integer propertyId;
	private String propertyName;
	private String guestName;
	private String HMCName;
	private List<String> types;
	private List<String> states;
	private List<String> reservationType;
	private Integer page;
	private Integer limit;
	private String sortingBy;
	private String sortingType;
	private Integer limitFrom;
	private String discountCode;
	private Integer channelId;

	public ChannelPortalFilterDto() {

	}

	public String getFilterDate() {
		return filterDate;
	}

	public void setFilterDate(String filterDate) {
		this.filterDate = filterDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public Date getCheckInStartDate() {
		return checkInStartDate;
	}

	public void setCheckInStartDate(Date checkInStartDate) {
		this.checkInStartDate = checkInStartDate;
	}

	public Date getCheckInEndDate() {
		return checkInEndDate;
	}

	public void setCheckInEndDate(Date checkInEndDate) {
		this.checkInEndDate = checkInEndDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public String getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(String confirmationId) {
		this.confirmationId = confirmationId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getHMCName() {
		return HMCName;
	}

	public void setHMCName(String HMCName) {
		this.HMCName = HMCName;
	}

	public List<String> getTypes() {
		return types;
	}

	public void setTypes(List<String> types) {
		this.types = types;
	}

	public List<String> getStates() {
		return states;
	}

	public void setStates(List<String> states) {
		this.states = states;
	}

	public List<String> getReservationType() {
		return reservationType;
	}

	public void setReservationType(List<String> reservationType) {
		this.reservationType = reservationType;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getSortingBy() {
		return sortingBy;
	}

	public void setSortingBy(String sortingBy) {
		this.sortingBy = sortingBy;
	}

	public String getSortingType() {
		return sortingType;
	}

	public void setSortingType(String sortingType) {
		this.sortingType = sortingType;
	}

	public Integer getLimitFrom() {
		return limitFrom;
	}

	public void setLimitFrom(Integer limitFrom) {
		this.limitFrom = limitFrom;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
}