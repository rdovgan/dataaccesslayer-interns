package com.mybookingpal.dal.shared.marriott;

import java.util.Date;

public class PaymentAndStayInfo {

	private String propertyImageUrl;
	private Integer bedrooms;
	private Integer adult;
	private Integer child;
	private String propertyName;
	private Date checkInDate;
	private Date checkOutDate;
	private String reservationConfirmationId;
	private String primaryGuest;
	private Double totalForStay;
	private Double amountChargedAtBooking;
	private Double amountChargedAtLastDayToCancel;
	private Date lastDayToCancel;
	private String currency;
	private String pmName;
	private String pmPhone;
	private String homesCancellationPolicy;
	private String city;
	private String country;
	private String currencySymbol;

	public String getPropertyImageUrl() {
		return propertyImageUrl;
	}

	public void setPropertyImageUrl(String propertyImageUrl) {
		this.propertyImageUrl = propertyImageUrl;
	}

	public Integer getBedrooms() {
		return bedrooms;
	}

	public void setBedrooms(Integer bedrooms) {
		this.bedrooms = bedrooms;
	}

	public Integer getAdult() {
		return adult;
	}

	public void setAdult(Integer adult) {
		this.adult = adult;
	}

	public Integer getChild() {
		return child;
	}

	public void setChild(Integer child) {
		this.child = child;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public Date getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}

	public Date getCheckOutDate() {
		return checkOutDate;
	}

	public void setCheckOutDate(Date checkOutDate) {
		this.checkOutDate = checkOutDate;
	}

	public String getReservationConfirmationId() {
		return reservationConfirmationId;
	}

	public void setReservationConfirmationId(String reservationConfirmationId) {
		this.reservationConfirmationId = reservationConfirmationId;
	}

	public String getPrimaryGuest() {
		return primaryGuest;
	}

	public void setPrimaryGuest(String primaryGuest) {
		this.primaryGuest = primaryGuest;
	}

	public Double getTotalForStay() {
		return totalForStay;
	}

	public void setTotalForStay(Double totalForStay) {
		this.totalForStay = totalForStay;
	}

	public Double getAmountChargedAtBooking() {
		return amountChargedAtBooking;
	}

	public void setAmountChargedAtBooking(Double amountChargedAtBooking) {
		this.amountChargedAtBooking = amountChargedAtBooking;
	}

	public Double getAmountChargedAtLastDayToCancel() {
		return amountChargedAtLastDayToCancel;
	}

	public void setAmountChargedAtLastDayToCancel(Double amountChargedAtLastDayToCancel) {
		this.amountChargedAtLastDayToCancel = amountChargedAtLastDayToCancel;
	}

	public Date getLastDayToCancel() {
		return lastDayToCancel;
	}

	public void setLastDayToCancel(Date lastDayToCancel) {
		this.lastDayToCancel = lastDayToCancel;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPmName() {
		return pmName;
	}

	public void setPmName(String pmName) {
		this.pmName = pmName;
	}

	public String getPmPhone() {
		return pmPhone;
	}

	public void setPmPhone(String pmPhone) {
		this.pmPhone = pmPhone;
	}

	public String getHomesCancellationPolicy() {
		return homesCancellationPolicy;
	}

	public void setHomesCancellationPolicy(String homesCancellationPolicy) {
		this.homesCancellationPolicy = homesCancellationPolicy;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}
}
