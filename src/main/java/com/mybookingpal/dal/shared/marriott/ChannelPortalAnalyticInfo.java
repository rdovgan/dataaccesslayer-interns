package com.mybookingpal.dal.shared.marriott;

import java.time.LocalDate;

public class ChannelPortalAnalyticInfo {

	private Integer reservationId;
	private LocalDate date;
	private LocalDate fromDate;
	private LocalDate toDate;
	private String currency;
	private Double quote;
	private Double price;
	private Double netRevenue;

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Double getQuote() {
		return quote;
	}

	public void setQuote(Double quote) {
		this.quote = quote;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getNetRevenue() {
		return netRevenue;
	}

	public void setNetRevenue(Double netRevenue) {
		this.netRevenue = netRevenue;
	}
}
