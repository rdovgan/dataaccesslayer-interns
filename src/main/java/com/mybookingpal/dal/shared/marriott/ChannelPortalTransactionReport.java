package com.mybookingpal.dal.shared.marriott;

import java.util.Date;

public class ChannelPortalTransactionReport {

	private Date date;
	private Long transactionId;
	private String customerName;
	private Integer bookingPalReservationId;
	private String channelReservationId;
	private Integer productId;
	private String channelProductId;
	private String propertyName;
	private String reservationState;
	private String paymentStatus;
	private String hmcName;
	private Double currentTransactionAmount;
	private Double currentTransactionUsd;
	private String currency;
	private Double totalCommission;
	private Double totalAmount;
	private Double totalAmountUsd;
	private Date invoiceDate;
	private String chargeType;
	private Double totalFees;
	private Double totalTaxes;
	private Double price;
	private String discountCode;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Integer getBookingPalReservationId() {
		return bookingPalReservationId;
	}

	public void setBookingPalReservationId(Integer bookingPalReservationId) {
		this.bookingPalReservationId = bookingPalReservationId;
	}

	public String getChannelReservationId() {
		return channelReservationId;
	}

	public void setChannelReservationId(String channelReservationId) {
		this.channelReservationId = channelReservationId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getChannelProductId() {
		return channelProductId;
	}

	public void setChannelProductId(String channelProductId) {
		this.channelProductId = channelProductId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getReservationState() {
		return reservationState;
	}

	public void setReservationState(String reservationState) {
		this.reservationState = reservationState;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getHmcName() {
		return hmcName;
	}

	public void setHmcName(String hmcName) {
		this.hmcName = hmcName;
	}

	public Double getCurrentTransactionAmount() {
		return currentTransactionAmount;
	}

	public void setCurrentTransactionAmount(Double currentTransactionAmount) {
		this.currentTransactionAmount = currentTransactionAmount;
	}

	public Double getCurrentTransactionUsd() {
		return currentTransactionUsd;
	}

	public void setCurrentTransactionUsd(Double currentTransactionUsd) {
		this.currentTransactionUsd = currentTransactionUsd;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Double getTotalCommission() {
		return totalCommission;
	}

	public void setTotalCommission(Double totalCommission) {
		this.totalCommission = totalCommission;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getTotalAmountUsd() {
		return totalAmountUsd;
	}

	public void setTotalAmountUsd(Double totalAmountUsd) {
		this.totalAmountUsd = totalAmountUsd;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public Double getTotalFees() {
		return totalFees;
	}

	public void setTotalFees(Double totalFees) {
		this.totalFees = totalFees;
	}

	public Double getTotalTaxes() {
		return totalTaxes;
	}

	public void setTotalTaxes(Double totalTaxes) {
		this.totalTaxes = totalTaxes;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}
}
