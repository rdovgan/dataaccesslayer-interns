package com.mybookingpal.dal.shared.marriott;

import java.time.LocalDate;
import java.util.List;

public class ChannelPortalAnalyticRequest {

	private String dateType;
	private LocalDate fromDate;
	private LocalDate toDate;
	private String hmcName;
	private String propertyName;
	private List<String> states;
	private Integer channelId;

	public String getDateType() {
		return dateType;
	}

	public void setDateType(String dateType) {
		this.dateType = dateType;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public String getHmcName() {
		return hmcName;
	}

	public void setHmcName(String hmcName) {
		this.hmcName = hmcName;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public List<String> getStates() {
		return states;
	}

	public void setStates(List<String> states) {
		this.states = states;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

}
