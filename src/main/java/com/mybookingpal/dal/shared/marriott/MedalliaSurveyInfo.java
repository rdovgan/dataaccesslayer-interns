package com.mybookingpal.dal.shared.marriott;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class MedalliaSurveyInfo {

	private Integer reservationId;
	private String confirmationId;
	private Integer productId;
	private Integer channelId;
	private Integer customerId;
	private String firstName;
	private String lastName;
	private String email;
	private String rewardLevel;
	private String encryptedKey;
	private LocalDateTime reservationDate;
	private LocalDate checkIn;
	private LocalDate checkOut;
	private String specificGatewayInfo;
	private String hmcName;
	private String reservationPayload;

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public String getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(String confirmationId) {
		this.confirmationId = confirmationId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRewardLevel() {
		return rewardLevel;
	}

	public void setRewardLevel(String rewardLevel) {
		this.rewardLevel = rewardLevel;
	}

	public String getEncryptedKey() {
		return encryptedKey;
	}

	public void setEncryptedKey(String encryptedKey) {
		this.encryptedKey = encryptedKey;
	}

	public LocalDateTime getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(LocalDateTime reservationDate) {
		this.reservationDate = reservationDate;
	}

	public LocalDate getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(LocalDate checkIn) {
		this.checkIn = checkIn;
	}

	public LocalDate getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(LocalDate checkOut) {
		this.checkOut = checkOut;
	}

	public String getSpecificGatewayInfo() {
		return specificGatewayInfo;
	}

	public void setSpecificGatewayInfo(String specificGatewayInfo) {
		this.specificGatewayInfo = specificGatewayInfo;
	}

	public String getHmcName() {
		return hmcName;
	}

	public void setHmcName(String hmcName) {
		this.hmcName = hmcName;
	}

	public String getReservationPayload() {
		return reservationPayload;
	}

	public void setReservationPayload(String reservationPayload) {
		this.reservationPayload = reservationPayload;
	}
}
