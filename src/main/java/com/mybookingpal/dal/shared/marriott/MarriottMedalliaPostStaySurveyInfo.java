package com.mybookingpal.dal.shared.marriott;

public class MarriottMedalliaPostStaySurveyInfo {

	private Integer reservationId;
	private Integer productId;
	private String productName;
	private String rewardLevel;
	private String rewardNumber;
	private String customerEmail;
	private String customerFirstName;
	private String customerLastName;
	private String encryptedKey;
	private Integer hmcId;
	private String hmcName;
	private String hmcEmail;
	private String hmcLogo;

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getRewardLevel() {
		return rewardLevel;
	}

	public void setRewardLevel(String rewardLevel) {
		this.rewardLevel = rewardLevel;
	}

	public String getRewardNumber() {
		return rewardNumber;
	}

	public void setRewardNumber(String rewardNumber) {
		this.rewardNumber = rewardNumber;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	public String getEncryptedKey() {
		return encryptedKey;
	}

	public void setEncryptedKey(String encryptedKey) {
		this.encryptedKey = encryptedKey;
	}

	public Integer getHmcId() {
		return hmcId;
	}

	public void setHmcId(Integer hmcId) {
		this.hmcId = hmcId;
	}

	public String getHmcName() {
		return hmcName;
	}

	public void setHmcName(String hmcName) {
		this.hmcName = hmcName;
	}

	public String getHmcEmail() {
		return hmcEmail;
	}

	public void setHmcEmail(String hmcEmail) {
		this.hmcEmail = hmcEmail;
	}

	public String getHmcLogo() {
		return hmcLogo;
	}

	public void setHmcLogo(String hmcLogo) {
		this.hmcLogo = hmcLogo;
	}
}
