package com.mybookingpal.dal.shared.marriott;

import java.util.Date;

public class ChannelPortalReconciliationReport {

	private Integer hmcId;
	private String hmcName;
	private String propertyName;
	private String channelReservationId;
	private Integer bookingPalReservationId;
	private String guestName;
	private Date reservationDate;
	private Date checkInDate;
	private Date checkOutDate;
	private String reservationStatus;
	private String bookingCurrency;
	private Double channelCommission;
	private Date reportDate;

	public Integer getHmcId() {
		return hmcId;
	}

	public void setHmcId(Integer hmcId) {
		this.hmcId = hmcId;
	}

	public String getHmcName() {
		return hmcName;
	}

	public void setHmcName(String hmcName) {
		this.hmcName = hmcName;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getChannelReservationId() {
		return channelReservationId;
	}

	public void setChannelReservationId(String channelReservationId) {
		this.channelReservationId = channelReservationId;
	}

	public Integer getBookingPalReservationId() {
		return bookingPalReservationId;
	}

	public void setBookingPalReservationId(Integer bookingPalReservationId) {
		this.bookingPalReservationId = bookingPalReservationId;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public Date getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}

	public Date getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}

	public Date getCheckOutDate() {
		return checkOutDate;
	}

	public void setCheckOutDate(Date checkOutDate) {
		this.checkOutDate = checkOutDate;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public String getBookingCurrency() {
		return bookingCurrency;
	}

	public void setBookingCurrency(String bookingCurrency) {
		this.bookingCurrency = bookingCurrency;
	}

	public Double getChannelCommission() {
		return channelCommission;
	}

	public void setChannelCommission(Double channelCommission) {
		this.channelCommission = channelCommission;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
}
