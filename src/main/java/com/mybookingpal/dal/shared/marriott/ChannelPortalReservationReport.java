package com.mybookingpal.dal.shared.marriott;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class ChannelPortalReservationReport {

	private Integer reservationId;
	private String confirmationId;
	private Integer customerId;
	private LocalDateTime reservationDate;
	private LocalDate surveyDate;
	private Integer propertyId;
	private String propertyName;
	private LocalDate fromDate;
	private LocalTime arrivalTime;
	private LocalDate toDate;
	private LocalTime departureTime;
	private String customerName;
	private String state;
	private String HMCName;
	private Long pointsUsed;
	private Integer pointsStatus;
	private String redemptionStatus;
	private Integer numberOfBedrooms;
	private Double price;
	private Double totalFees;
	private Double totalTaxes;
	private String loyaltyLevel;
	private String currency;
	private LocalDate lastDayToCancel;
	private Integer hmcId;
	private Double totalAmount;
	private Long pointsEarned;
	private LocalDateTime reservationCancellationDate;
	private Double channelCommission;
	private String discountCode;

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public String getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(String confirmationId) {
		this.confirmationId = confirmationId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public LocalDateTime getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(LocalDateTime reservationDate) {
		this.reservationDate = reservationDate;
	}

	public LocalDate getSurveyDate() {
		return surveyDate;
	}

	public void setSurveyDate(LocalDate surveyDate) {
		this.surveyDate = surveyDate;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalTime getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(LocalTime arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public LocalTime getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(LocalTime departureTime) {
		this.departureTime = departureTime;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getHMCName() {
		return HMCName;
	}

	public void setHMCName(String HMCName) {
		this.HMCName = HMCName;
	}

	public Long getPointsUsed() {
		return pointsUsed;
	}

	public void setPointsUsed(Long pointsUsed) {
		this.pointsUsed = pointsUsed;
	}

	public Integer getPointsStatus() {
		return pointsStatus;
	}

	public void setPointsStatus(Integer pointsStatus) {
		this.pointsStatus = pointsStatus;
	}

	public String getRedemptionStatus() {
		return redemptionStatus;
	}

	public void setRedemptionStatus(String redemptionStatus) {
		this.redemptionStatus = redemptionStatus;
	}

	public Integer getNumberOfBedrooms() {
		return numberOfBedrooms;
	}

	public void setNumberOfBedrooms(Integer numberOfBedrooms) {
		this.numberOfBedrooms = numberOfBedrooms;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getTotalFees() {
		return totalFees;
	}

	public void setTotalFees(Double totalFees) {
		this.totalFees = totalFees;
	}

	public Double getTotalTaxes() {
		return totalTaxes;
	}

	public void setTotalTaxes(Double totalTaxes) {
		this.totalTaxes = totalTaxes;
	}

	public String getLoyaltyLevel() {
		return loyaltyLevel;
	}

	public void setLoyaltyLevel(String loyaltyLevel) {
		this.loyaltyLevel = loyaltyLevel;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public LocalDate getLastDayToCancel() {
		return lastDayToCancel;
	}

	public void setLastDayToCancel(LocalDate lastDayToCancel) {
		this.lastDayToCancel = lastDayToCancel;
	}

	public Integer getHmcId() {
		return hmcId;
	}

	public void setHmcId(Integer hmcId) {
		this.hmcId = hmcId;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Long getPointsEarned() {
		return pointsEarned;
	}

	public void setPointsEarned(Long pointsEarned) {
		this.pointsEarned = pointsEarned;
	}

	public LocalDateTime getReservationCancellationDate() {
		return reservationCancellationDate;
	}

	public void setReservationCancellationDate(LocalDateTime reservationCancellationDate) {
		this.reservationCancellationDate = reservationCancellationDate;
	}

	public Double getChannelCommission() {
		return channelCommission;
	}

	public void setChannelCommission(Double channelCommission) {
		this.channelCommission = channelCommission;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}
}

