package com.mybookingpal.dal.shared.marriott;

import java.util.Date;

public class ChannelPortalPointsRedemptionReport {

	private Integer hmcId;
	private String hmcName;
	private String propertyName;
	private String guestName;
	private Date createdDate;
	private Date checkInDate;
	private Date checkOutDate;
	private Integer bookingPalReservationId;
	private String channelReservationId;
	private String reservationStatus;
	private String reservationType;
	private String bookingCurrency;
	private String clientRewardLevel;
	private String clientRewardNumber;
	private Long pointsEarned;
	private Long pointsRedeemed;
	private Date lastDayToCancel;
	private String clientRewardEmail;
	private Date invoiceDate;
	private Double totalQuote;
	private String encryptedKey;
	private Integer customerId;
	private Integer propertyId;
	private Double totalAmount;
	private Integer pointsStatus;
	private Integer numberOfNights;
	private Double nightlyRate;
	private Double fees;
	private Double taxes;
	private String pmsReservationId;
	private Date cancellationDate;
	private Double commission;
	private Double bpCommission;

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public Date getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}

	public Date getCheckOutDate() {
		return checkOutDate;
	}

	public void setCheckOutDate(Date checkOutDate) {
		this.checkOutDate = checkOutDate;
	}

	public Integer getBookingPalReservationId() {
		return bookingPalReservationId;
	}

	public void setBookingPalReservationId(Integer bookingPalReservationId) {
		this.bookingPalReservationId = bookingPalReservationId;
	}

	public String getChannelReservationId() {
		return channelReservationId;
	}

	public void setChannelReservationId(String channelReservationId) {
		this.channelReservationId = channelReservationId;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public String getReservationType() {
		return reservationType;
	}

	public void setReservationType(String reservationType) {
		this.reservationType = reservationType;
	}

	public String getBookingCurrency() {
		return bookingCurrency;
	}

	public void setBookingCurrency(String bookingCurrency) {
		this.bookingCurrency = bookingCurrency;
	}

	public String getClientRewardLevel() {
		return clientRewardLevel;
	}

	public void setClientRewardLevel(String clientRewardLevel) {
		this.clientRewardLevel = clientRewardLevel;
	}

	public Long getPointsEarned() {
		return pointsEarned;
	}

	public void setPointsEarned(Long pointsEarned) {
		this.pointsEarned = pointsEarned;
	}

	public Long getPointsRedeemed() {
		return pointsRedeemed;
	}

	public void setPointsRedeemed(Long pointsRedeemed) {
		this.pointsRedeemed = pointsRedeemed;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastDayToCancel() {
		return lastDayToCancel;
	}

	public void setLastDayToCancel(Date lastDayToCancel) {
		this.lastDayToCancel = lastDayToCancel;
	}

	public Integer getHmcId() {
		return hmcId;
	}

	public void setHmcId(Integer hmcId) {
		this.hmcId = hmcId;
	}

	public String getHmcName() {
		return hmcName;
	}

	public void setHmcName(String hmcName) {
		this.hmcName = hmcName;
	}

	public String getClientRewardNumber() {
		return clientRewardNumber;
	}

	public void setClientRewardNumber(String clientRewardNumber) {
		this.clientRewardNumber = clientRewardNumber;
	}

	public String getClientRewardEmail() {
		return clientRewardEmail;
	}

	public void setClientRewardEmail(String clientRewardEmail) {
		this.clientRewardEmail = clientRewardEmail;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getEncryptedKey() {
		return encryptedKey;
	}

	public void setEncryptedKey(String encryptedKey) {
		this.encryptedKey = encryptedKey;
	}

	public Double getTotalQuote() {
		return totalQuote;
	}

	public void setTotalQuote(Double totalQuote) {
		this.totalQuote = totalQuote;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getPointsStatus() {
		return pointsStatus;
	}

	public void setPointsStatus(Integer pointsStatus) {
		this.pointsStatus = pointsStatus;
	}

	public Integer getNumberOfNights() {
		return numberOfNights;
	}

	public void setNumberOfNights(Integer numberOfNights) {
		this.numberOfNights = numberOfNights;
	}

	public Double getNightlyRate() {
		return nightlyRate;
	}

	public void setNightlyRate(Double nightlyRate) {
		this.nightlyRate = nightlyRate;
	}

	public Double getFees() {
		return fees;
	}

	public void setFees(Double fees) {
		this.fees = fees;
	}

	public Double getTaxes() {
		return taxes;
	}

	public void setTaxes(Double taxes) {
		this.taxes = taxes;
	}

	public String getPmsReservationId() {
		return pmsReservationId;
	}

	public void setPmsReservationId(String pmsReservationId) {
		this.pmsReservationId = pmsReservationId;
	}

	public Date getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(Date cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public Double getCommission() {
		return commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public Double getBpCommission() {
		return bpCommission;
	}

	public void setBpCommission(Double bpCommission) {
		this.bpCommission = bpCommission;
	}
}