package com.mybookingpal.dal.shared.marriott;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;

public class GuestDecryptedInfoDto {

	@XmlElement(name = "guest_name", nillable = true)
	@JsonProperty("guest_name")
	private String guestName;

	@XmlElement(name = "client_reward_number", nillable = true)
	@JsonProperty("client_reward_number")
	private String clientRewardNumber;

	@XmlElement(name = "client_reward_email", nillable = true)
	@JsonProperty("client_reward_email")
	private String clientRewardEmail;

	@JsonIgnore
	private String encryptedKey;

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getClientRewardNumber() {
		return clientRewardNumber;
	}

	public void setClientRewardNumber(String clientRewardNumber) {
		this.clientRewardNumber = clientRewardNumber;
	}

	public String getClientRewardEmail() {
		return clientRewardEmail;
	}

	public void setClientRewardEmail(String clientRewardEmail) {
		this.clientRewardEmail = clientRewardEmail;
	}

	public String getEncryptedKey() {
		return encryptedKey;
	}

	public void setEncryptedKey(String encryptedKey) {
		this.encryptedKey = encryptedKey;
	}
}
