package com.mybookingpal.dal.shared.marriott;

import java.util.Date;

public class ChannelPortalPayoutReport {

	private Integer hmcId;
	private String hmcName;
	private String propertyName;
	private String guestName;
	private Date checkInDate;
	private Date checkOutDate;
	private Integer bookingPalReservationId;
	private String channelReservationId;
	private String reservationStatus;
	private String transactionType;
	private String bookingCurrency;
	private Double totalQuote;
	private Double marriottCommision;
	private Double creditCardFeeSum;
	private Double bpCommission;
	private Double pmsCommision;
	private Double creditCardFeeValue;
	private Date lastDayToCancel;

	public Integer getHmcId() {
		return hmcId;
	}

	public void setHmcId(Integer hmcId) {
		this.hmcId = hmcId;
	}

	public String getHmcName() {
		return hmcName;
	}

	public void setHmcName(String hmcName) {
		this.hmcName = hmcName;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public Date getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}

	public Date getCheckOutDate() {
		return checkOutDate;
	}

	public void setCheckOutDate(Date checkOutDate) {
		this.checkOutDate = checkOutDate;
	}

	public Integer getBookingPalReservationId() {
		return bookingPalReservationId;
	}

	public void setBookingPalReservationId(Integer bookingPalReservationId) {
		this.bookingPalReservationId = bookingPalReservationId;
	}

	public String getChannelReservationId() {
		return channelReservationId;
	}

	public void setChannelReservationId(String channelReservationId) {
		this.channelReservationId = channelReservationId;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getBookingCurrency() {
		return bookingCurrency;
	}

	public void setBookingCurrency(String bookingCurrency) {
		this.bookingCurrency = bookingCurrency;
	}

	public Double getTotalQuote() {
		return totalQuote;
	}

	public void setTotalQuote(Double totalQuote) {
		this.totalQuote = totalQuote;
	}

	public Double getMarriottCommision() {
		return marriottCommision;
	}

	public void setMarriottCommision(Double marriottCommision) {
		this.marriottCommision = marriottCommision;
	}

	public Double getCreditCardFeeSum() {
		return creditCardFeeSum;
	}

	public void setCreditCardFeeSum(Double creditCardFeeSum) {
		this.creditCardFeeSum = creditCardFeeSum;
	}

	public Double getBpCommission() {
		return bpCommission;
	}

	public void setBpCommission(Double bpCommission) {
		this.bpCommission = bpCommission;
	}

	public Double getPmsCommision() {
		return pmsCommision;
	}

	public void setPmsCommision(Double pmsCommision) {
		this.pmsCommision = pmsCommision;
	}

	public Date getLastDayToCancel() {
		return lastDayToCancel;
	}

	public void setLastDayToCancel(Date lastDayToCancel) {
		this.lastDayToCancel = lastDayToCancel;
	}

	public Double getCreditCardFeeValue() {
		return creditCardFeeValue;
	}

	public void setCreditCardFeeValue(Double creditCardFeeValue) {
		this.creditCardFeeValue = creditCardFeeValue;
	}
}
