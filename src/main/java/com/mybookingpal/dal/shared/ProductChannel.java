package com.mybookingpal.dal.shared;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductChannel extends Product {

	private String channelProductId;
	private String parentChannelProductId;
	private ChannelProductMap.ChannelStateEnum channelState;

}

