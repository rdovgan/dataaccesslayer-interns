package com.mybookingpal.dal.shared;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

import com.mybookingpal.dal.enums.RestrictionConstants;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Restriction")
@Scope(value = "prototype")
@Primary
public class Restriction implements DateRange {

	private Integer id;
	private Integer productId;
	private Integer partyId;
	private Integer state;
	private LocalDate fromDate;
	private LocalDate toDate;
	private RestrictionConstants.RestrictionType ruleType;
	private String param;
	private Date createdDate;
	private Date version;
	private Long ratePlanId;

	public Restriction() {
		super();
	}

	public Restriction(Integer productId, int partyId, int state, LocalDate fromDate, LocalDate toDate, int ruleType, String param, Date createdDate) {
		super();
		this.productId = productId;
		this.partyId = partyId;
		this.state = state;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.ruleType = RestrictionConstants.RestrictionType.getByValue(ruleType);
		this.param = param;
		this.createdDate = createdDate;
	}

	public Restriction(Restriction restriction) {
		this.id = restriction.getId();
		this.productId = restriction.getProductId();
		this.partyId = restriction.getPartyId();
		this.state = restriction.getState();
		this.fromDate = restriction.getFromDate();
		this.toDate = restriction.getToDate();
		this.ruleType = restriction.getRuleType();
		this.param = restriction.getParam();
		this.createdDate = restriction.getCreatedDate();
		this.version = restriction.getVersion();
		this.ratePlanId = restriction.getRatePlanId();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getPartyId() {
		return partyId;
	}

	public void setPartyId(Integer partyId) {
		this.partyId = partyId;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public RestrictionConstants.RestrictionType getRuleType() {
		return ruleType;
	}

	public void setRuleType(RestrictionConstants.RestrictionType ruleType) {
		this.ruleType = ruleType;
	}

	@Deprecated
	//Need to add MyBatis type handler
	public void setRuleType(Integer ruleType) {
		this.ruleType = RestrictionConstants.RestrictionType.getByValue(ruleType);
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}

	public Long getRatePlanId() {
		return ratePlanId;
	}

	public void setRatePlanId(Long ratePlanId) {
		this.ratePlanId = ratePlanId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Restriction that = (Restriction) o;
		return Objects.equals(productId, that.productId) && Objects.equals(partyId, that.partyId) && Objects.equals(state, that.state) && Objects
				.equals(fromDate, that.fromDate) && Objects.equals(toDate, that.toDate) && ruleType == that.ruleType && Objects.equals(param, that.param)
				&& Objects.equals(ratePlanId, that.ratePlanId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(productId, partyId, state, fromDate, toDate, ruleType, param, ratePlanId);
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public String toString() {
		return "Restriction [id=" + id + ", productId=" + productId + ", partyId=" + partyId + ", state=" + state + ", fromDate=" + fromDate + ", toDate="
				+ toDate + ", ruleType=" + ruleType + ", param=" + param + ", createdDate=" + createdDate + ", version=" + version + ", ratePlanId="
				+ ratePlanId + "]";
	}

}