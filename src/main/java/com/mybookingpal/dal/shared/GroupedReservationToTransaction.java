package com.mybookingpal.dal.shared;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class GroupedReservationToTransaction {

	private Long id;
	private Long groupedReservationId;
	private Long transactionId;
	private TransactionType transactionType;

	public enum TransactionType {
		PAYMENT_TRANSACTION, PENDING_TRANSACTION
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGroupedReservationId() {
		return groupedReservationId;
	}

	public void setGroupedReservationId(Long groupedReservationId) {
		this.groupedReservationId = groupedReservationId;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
}
