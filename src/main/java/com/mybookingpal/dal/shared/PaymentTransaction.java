package com.mybookingpal.dal.shared;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Component
@Scope(value = "prototype")
public class PaymentTransaction {

	public enum TransactionState {
		Initial, Accepted, Failed, Declined, Cancelled
	}

	protected Long id;
	protected Date createDate;
	protected int serverId;
	protected Integer reservationId;
	protected Integer pmsConfirmationId;
	protected int paymentMethod;
	protected String gatewayTransactionId;
	protected Integer gatewayId;
	protected Integer fundsHolder;
	protected String partialIin;
	protected String status;
	protected String message;
	protected Integer partnerId;
	protected int supplierId;
	protected Date chargeDate;
	protected Date invoiceDate;
	protected String currency;
	protected BigDecimal totalCommission;
	protected BigDecimal partnerPayment;
	protected BigDecimal totalBookingpalPayment;
	protected BigDecimal finalAmount;
	protected BigDecimal finalAmountUsd;
	protected BigDecimal totalAmount;
	protected BigDecimal totalAmountUsd;
	protected BigDecimal creditCardFee;
	protected String chargeType;
	protected boolean cancelled;
	protected boolean netRate;

	// net rate commissions
	protected BigDecimal pmsPayment;
	protected BigDecimal pmCommissionValue;

	protected BigDecimal additionalCommissionValue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public int getServerId() {
		return serverId;
	}

	public void setServerId(int serverId) {
		this.serverId = serverId;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public Integer getPmsConfirmationId() {
		return pmsConfirmationId;
	}

	public void setPmsConfirmationId(Integer pmsConfirmationId) {
		this.pmsConfirmationId = pmsConfirmationId;
	}

	public int getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(int paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getGatewayTransactionId() {
		return gatewayTransactionId;
	}

	public void setGatewayTransactionId(String gatewayTransactionId) {
		this.gatewayTransactionId = gatewayTransactionId;
	}

	public Integer getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(Integer gatewayId) {
		this.gatewayId = gatewayId;
	}

	public Integer getFundsHolder() {
		return fundsHolder;
	}

	public void setFundsHolder(Integer fundsHolder) {
		this.fundsHolder = fundsHolder;
	}

	public String getPartialIin() {
		return partialIin;
	}

	public void setPartialIin(String partialIin) {
		this.partialIin = partialIin;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}

	public int getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	public Date getChargeDate() {
		return chargeDate;
	}

	public void setChargeDate(Date chargeDate) {
		this.chargeDate = chargeDate;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	@JsonIgnore
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = new BigDecimal(totalAmount, MathContext.DECIMAL64);
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getTotalCommission() {
		return totalCommission;
	}

	@JsonIgnore
	public void setTotalCommission(BigDecimal totalCommission) {
		this.totalCommission = totalCommission;
	}

	public void setTotalCommission(double totalCommission) {
		this.totalCommission = new BigDecimal(totalCommission, MathContext.DECIMAL64);
	}

	public BigDecimal getPartnerPayment() {
		return partnerPayment;
	}

	@JsonIgnore
	public void setPartnerPayment(BigDecimal partnerPayment) {
		this.partnerPayment = partnerPayment;
	}

	public void setPartnerPayment(double partnerPayment) {
		this.partnerPayment = new BigDecimal(partnerPayment, MathContext.DECIMAL64);
	}

	public BigDecimal getTotalBookingpalPayment() {
		return totalBookingpalPayment;
	}

	@JsonIgnore
	public void setTotalBookingpalPayment(BigDecimal totalBookingpalPayment) {
		this.totalBookingpalPayment = totalBookingpalPayment;
	}

	public void setTotalBookingpalPayment(double totalBookingpalPayment) {
		this.totalBookingpalPayment = new BigDecimal(totalBookingpalPayment, MathContext.DECIMAL64);
	}

	public BigDecimal getFinalAmount() {
		return finalAmount;
	}

	@JsonIgnore
	public void setFinalAmount(BigDecimal finalAmount) {
		this.finalAmount = finalAmount;
	}

	public void setFinalAmount(double finalAmount) {
		this.finalAmount = new BigDecimal(finalAmount, MathContext.DECIMAL64);
	}

	public BigDecimal getFinalAmountUsd() {
		return finalAmountUsd;
	}

	@JsonIgnore
	public void setFinalAmountUsd(BigDecimal finalAmountUsd) {
		this.finalAmountUsd = finalAmountUsd;
	}

	public void setFinalAmountUsd(double finalAmountUsd) {
		this.finalAmountUsd = new BigDecimal(finalAmountUsd, MathContext.DECIMAL64);
	}

	public BigDecimal getTotalAmountUsd() {
		return totalAmountUsd;
	}

	@JsonIgnore
	public void setTotalAmountUsd(BigDecimal totalAmountUsd) {
		this.totalAmountUsd = totalAmountUsd;
	}

	public void setTotalAmountUsd(double totalAmountUsd) {
		this.totalAmountUsd = new BigDecimal(totalAmountUsd, MathContext.DECIMAL64);
	}

	public BigDecimal getCreditCardFee() {
		return creditCardFee;
	}

	@JsonIgnore
	public void setCreditCardFee(BigDecimal creditCardFee) {
		this.creditCardFee = creditCardFee;
	}

	public void setCreditCardFee(Double creditCardFee) {
		this.creditCardFee = new BigDecimal(creditCardFee, MathContext.DECIMAL64);
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public boolean isNetRate() {
		return netRate;
	}

	public void setNetRate(boolean netRate) {
		this.netRate = netRate;
	}

	public BigDecimal getPmsPayment() {
		return pmsPayment;
	}

	@JsonIgnore
	public void setPmsPayment(BigDecimal pmsPayment) {
		this.pmsPayment = pmsPayment;
	}

	public void setPmsPayment(double pmsPayment) {
		this.pmsPayment = new BigDecimal(pmsPayment, MathContext.DECIMAL64);
	}

	public BigDecimal getPmCommissionValue() {
		return pmCommissionValue;
	}

	@JsonIgnore
	public void setPmCommissionValue(BigDecimal pmCommissionValue) {
		this.pmCommissionValue = pmCommissionValue;
	}

	public void setPmCommissionValue(double pmCommissionValue) {
		this.pmCommissionValue = new BigDecimal(pmCommissionValue, MathContext.DECIMAL64);
	}

	public BigDecimal getAdditionalCommissionValue() {
		return additionalCommissionValue;
	}

	@JsonIgnore
	public void setAdditionalCommissionValue(BigDecimal additionalCommissionValue) {
		this.additionalCommissionValue = additionalCommissionValue;
	}

	public void setAdditionalCommissionValue(double additionalCommissionValue) {
		this.additionalCommissionValue = new BigDecimal(additionalCommissionValue, MathContext.DECIMAL64);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PaymentTransaction [id=");
		builder.append(id);
		builder.append(", createDate=");
		builder.append(createDate);
		builder.append(", serverId=");
		builder.append(serverId);
		builder.append(", reservationId=");
		builder.append(reservationId);
		builder.append(", pmsConfirmationId=");
		builder.append(pmsConfirmationId);
		builder.append(", paymentMethod=");
		builder.append(paymentMethod);
		builder.append(", gatewayTransactionId=");
		builder.append(gatewayTransactionId);
		builder.append(", gatewayId=");
		builder.append(gatewayId);
		builder.append(", fundsHolder=");
		builder.append(fundsHolder);
		builder.append(", partialIin=");
		builder.append(partialIin);
		builder.append(", status=");
		builder.append(status);
		builder.append(", message=");
		builder.append(message);
		builder.append(", partnerId=");
		builder.append(partnerId);
		builder.append(", supplierId=");
		builder.append(supplierId);
		builder.append(", chargeDate=");
		builder.append(chargeDate);
		builder.append(", totalAmount=");
		builder.append(totalAmount);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", totalCommission=");
		builder.append(totalCommission);
		builder.append(", partnerPayment=");
		builder.append(partnerPayment);
		builder.append(", totalBookingpalPayment=");
		builder.append(totalBookingpalPayment);
		builder.append(", finalAmount=");
		builder.append(finalAmount);
		builder.append(", creditCardFee=");
		builder.append(creditCardFee);
		builder.append(", chargeType=");
		builder.append(chargeType);
		builder.append(", netRate=");
		builder.append(netRate);
		builder.append("]");
		return builder.toString();
	}
}
