package com.mybookingpal.dal.shared;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class Setting implements Comparable<Setting> {
    
	private Integer id;
	private String name;
	private String value;
	private String description;
	private Integer parentId;
	private Date version;
	
	public Setting() {
		super();
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Date getVersion() {
		return version;
	}

	public void setVersion(Date version) {
		this.version = version;
	}
	
	@Override
	public String toString() {
		String stringObject = getName() + ":" + getValue() + "\n";
		return stringObject;
	}

	@Override
	public int compareTo(Setting o) {
		if(getId() > o.getId()) {
			return 1;
		} else if (getId() < o.getId()) {
			return -1;
		}
		return 0;
	}
	
	@Override
	public int hashCode() {
		byte[] id = this.id == null ? new byte[]{0} : String.valueOf(this.id).getBytes();
		byte[] name = this.name == null ? new byte[]{0} : String.valueOf(this.name).getBytes();
		byte[] value = this.value == null ? new byte[]{0} : String.valueOf(this.value).getBytes();
		byte[] description = this.description == null ? new byte[]{0} : String.valueOf(this.description).getBytes();
		byte[] parentId = this.parentId == null ? new byte[]{0} : String.valueOf(this.parentId).getBytes();
		byte[] version = this.version == null ? new byte[]{0} : String.valueOf(this.version).getBytes();
		return hashBytes(id) + hashBytes(name) + hashBytes(value) + hashBytes(description) + hashBytes(parentId) + hashBytes(version);
	}
	
	private int hashBytes(byte[] byteArray) {
		int result = 0;
		IntBuffer intBuf = ByteBuffer.wrap(byteArray).order(ByteOrder.BIG_ENDIAN).asIntBuffer();
		int[] array = new int[intBuf.remaining()];
		IntBuffer integerBuffer = intBuf.get(array);
		while (integerBuffer.hasRemaining()) {
			result += integerBuffer.get();
		}
		
		return result;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof Setting) {
			Setting toCompare = (Setting) o;
			return toCompare.getId().equals(getId());
		} else {
			return false;
		}
	}
}
