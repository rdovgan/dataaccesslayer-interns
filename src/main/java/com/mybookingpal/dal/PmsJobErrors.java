package com.mybookingpal.dal;

import lombok.Data;
import org.springframework.context.annotation.Scope;

import java.util.Date;

@Data
@Scope(value = "prototype")
public class PmsJobErrors {

    private String elasticId;

    private String pmsAbb;

    private String supplierId;

    private String propertyId;

    private String functionName;

    private String serverHostName;

    private String errorMsg;

    private Date created;

    private Date version;

    private int count;

    private boolean isProcessed;

    private String errorStackTrace;
}
