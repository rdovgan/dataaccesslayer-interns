/**
 * 
 */
package com.mybookingpal.dal.rest.channel.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.shared.Price;

/**
 * DTO for Price
 * 
 * @author Nibodha-Aruna [Jun 3, 2015-3:30:17 PM]
 * 
 */
@Component
@Scope("prototype")
public class NormalizedPriceDTO {
	private List<Price> availablePriceList = new ArrayList<Price>();
	private List<Price> unAvailablePriceList = new ArrayList<Price>();

	/**
	 * @return the availablePriceList
	 */
	public List<Price> getAvailablePriceList() {
		return availablePriceList;
	}

	/**
	 * @param availablePriceList the availablePriceList to set
	 */
	public void setAvailablePriceList(List<Price> availablePriceList) {
		this.availablePriceList = availablePriceList;
	}

	/**
	 * @return the unAvailablePriceList
	 */
	public List<Price> getUnAvailablePriceList() {
		return unAvailablePriceList;
	}

	/**
	 * @param unAvailablePriceList the unAvailablePriceList to set
	 */
	public void setUnAvailablePriceList(List<Price> unAvailablePriceList) {
		this.unAvailablePriceList = unAvailablePriceList;
	}

}
