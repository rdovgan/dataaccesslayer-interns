/**
 * 
 */
package com.mybookingpal.dal.rest.channel.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.shared.Yield;

/**
 * Yield DTO
 * 
 * @author Nibodha-Aruna [Jun 3, 2015-3:34:07 PM]
 * 
 */
@Component
@Scope("prototype")
public class NormalizedYieldDTO {

	private List<Yield> yieldList = new ArrayList<Yield>();

	/**
	 * @return the yieldList
	 */
	public List<Yield> getYieldList() {
		return yieldList;
	}

	/**
	 * @param yieldList
	 *            the yieldList to set
	 */
	public void setYieldList(List<Yield> yieldList) {
		this.yieldList = yieldList;
	}

}
