package com.mybookingpal.dal.rest.common.utils;

import com.mybookingpal.config.RazorConfig;
import com.mybookingpal.dal.rest.channel.model.NormalizedPriceDTO;
import com.mybookingpal.dal.shared.Price;
import com.mybookingpal.dal.shared.Time;
import com.mybookingpal.dal.shared.Yield;
import com.mybookingpal.dal.utils.CommonDateUtils;
import com.sun.istack.Nullable;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.DurationFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CommonUtils {
	@Autowired
	RazorConfig razorConfig;
	private static final Logger LOG = Logger.getLogger(CommonUtils.class.getName());

	public static final String RHOF_PM_ID = "724471";
	public static final String VACASA_PM_ID = "4915961";
	public static final String SUPPLIER_API_KEY = "Io!@2*WM#W|9bSA";
	public static final String SUPPLIER_API = "SupplierApi";
	public static final String SUPPLIER_API_TEST = "SupplierApiTest";
	public static final String SWITCHOVER_THROUGH_FILE = "FileSwitchover";

	public enum SupplierApiTypes{
		SUPPLIER_API ("SupplierApi"), SUPPLIER_API_TEST("SupplierApiTest"), SUPPLIER_API_DEMO ("SupplierApiDemo");

		String value;

		SupplierApiTypes(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

	}

	/**
	 * Returns date in "YYYY-MM-DD (eg 1997-07-16)" format
	 */
	public String convertDateTOIOSFormat(Date date) {
		return DateFormatUtils.ISO_DATE_FORMAT.format(date);
	}

	/**
	 * Accepts List of sorted(by date) yields. Clubs consecutive yield to range yield , if totalPrice is same
	 */
	public List<Yield> getYieldNormalized(List<Yield> yields) {
		final List<Yield> normalizedYieldList = new ArrayList<Yield>();
		if (yields.size() < 2) {
			return yields;
		}
		Yield previous = yields.get(0);
		Yield current;
		boolean start_startSame, end_endSame, end_startSame, end_startSuccessive, end_endSuccessive, values_Same, same_yieldName;
		for (int itr = 1; itr < yields.size(); itr++) {
			current = yields.get(itr);
			// compare two consecutive date
			start_startSame = CommonDateUtils.isTheSameDates(previous.getFromDate(), current.getFromDate()); // if, Same start date
			end_endSame = CommonDateUtils.isTheSameDates(previous.getToDate(), current.getToDate()); // if, Same end date
			end_startSame = CommonDateUtils.isTheSameDates(previous.getToDate(), current.getFromDate()); // if, previous endDate is same as
			// startDate of current
			end_startSuccessive = CommonDateUtils.isTheSameDates(previous.getToDate().plusDays(1), current.getFromDate()); // if, previous and current yield having
			// successive start dates
			end_endSuccessive = (previous.getToDate().compareTo(current.getToDate()) > 0); // if, previous and current price
																							// having successive end dates
			values_Same = Double.compare(previous.getTotalPrice().doubleValue(), current.getTotalPrice().doubleValue()) == 0;
			same_yieldName = previous.getName().equalsIgnoreCase(current.getName()); // Checking Yield types
			if (start_startSame && end_endSame && same_yieldName) { // same/duplicate date range
				previous = current;
			} else if (start_startSame && !end_endSame) { // date range having
															// same startDate
															// and diff endDate
				if (end_endSuccessive && same_yieldName)
					previous = current;
			} else if (!start_startSame && (end_startSame || end_startSuccessive) && values_Same && same_yieldName) {
				previous.setToDate(current.getToDate());
			} else {
				normalizedYieldList.add(previous);
				previous = current;
			}
		}
		normalizedYieldList.add(previous);
		return normalizedYieldList;
	}

	/**
	 * Accepts List of sorted(by date) prices Clubs consecutive price to range price , if price are same
	 */
	public NormalizedPriceDTO getPriceNormalized(List<Price> prices) {
		final NormalizedPriceDTO normalizedPriceDTO = new NormalizedPriceDTO();
//		NormalizedPriceDTO normalizedPriceDTO = RazorServer.getContext().getBean(NormalizedPriceDTO.class);

		if (prices.size() < 2) {
			normalizedPriceDTO.setAvailablePriceList(prices);
			return normalizedPriceDTO;
		}
		Price previous = prices.get(0);
		Price current;
		boolean start_startSame, end_endSame, end_startSame, end_startSuccessive, end_endSuccessive, values_Same;
		List<Price> availablePriceList = new ArrayList<Price>(prices.size());
		List<Price> missingPriceList = new ArrayList<Price>(prices.size());
		for (int itr = 1; itr < prices.size(); itr++) {
			current = prices.get(itr);
			// compare two consecutive date
			start_startSame = DateUtils.isSameDay(previous.getDate(), current.getDate()); // if, Same start date
			end_endSame = DateUtils.isSameDay(previous.getTodate(), current.getTodate()); // if, Same end date
			end_startSame = DateUtils.isSameDay(previous.getTodate(), current.getDate()); // if, previous endDate is same as
																							// startDate of current
			end_startSuccessive = DateUtils.isSameDay(Time.addDuration(previous.getTodate(), 1, Time.DAY), current.getDate()); // if, previous and current price having
																																// successive start dates
			end_endSuccessive = (previous.getTodate().compareTo(current.getTodate()) > 0); // if, previous and current price
																							// having successive end dates
			values_Same = Double.compare(previous.getValue(), current.getValue()) == 0;
			if (start_startSame && end_endSame) { // same/duplicate date range
				previous = current;
			} else if (start_startSame && !end_endSame) { // date range having
															// same startDate
															// and diff endDate
				if (end_endSuccessive)
					previous = current;
				else {
					//SN:Added for FPLOS style of pricing
					availablePriceList.add(previous);
					previous = current;

				}

			} else if (!start_startSame && (end_startSame || end_startSuccessive) && values_Same) {
				previous.setTodate(current.getTodate());
			} else {
				availablePriceList.add(previous);
				if (!(end_startSame || end_startSuccessive)) {
					missingPriceList.add(getPrice(Time.addDuration(previous.getTodate(), 1, Time.DAY), Time.addDuration(current.getDate(), -1, Time.DAY), 0));
				}
				previous = current;
			}
		}
		availablePriceList.add(previous);

		//normalizedPriceDTO.setAvailablePriceList(availablePriceList);SN:Commenting normalized price since it may cause problems for identifying minStay
		normalizedPriceDTO.setAvailablePriceList(prices);
		normalizedPriceDTO.setUnAvailablePriceList(missingPriceList);
		return normalizedPriceDTO;
	}

	/**
	 * Supporting method
	 */
	private Price getPrice(Date date, Date todate, double value) {
		Price price = new Price();
		price.setDate(date);
		price.setTodate(todate);
		price.setValue(value);
		return price;
	}

	public List<Date> generateDatesBetweenTwoDates(Date date1, Date date2) {
		DateTime dateTime1 = new DateTime(date1);
		DateTime dateTime2 = new DateTime(date2);
		List<Date> listDates = new ArrayList<Date>();
		int diff = Days.daysBetween(dateTime1, dateTime2).getDays() + 1;
		for (int i = 0; i < diff; i++) {
			DateTime dt = dateTime1.withFieldAdded(DurationFieldType.days(), i);
			listDates.add(dt.toDate());
		}
		return listDates;
	}

	public static <K, V> void put(Map<K, List<V>> listMap, K key, V value) {
		List<V> values = listMap.get(key);
		if (values == null) {
			values = new ArrayList<>();
			listMap.put(key, values);
		}
		values.add(value);
	}
	
	public <K, V> void putInMap(Map<K, Map<K, V>> map, K key, K keyForValue, V value) {
		if (map != null) {
			Map<K, V> values = map.get(key);
			if (values == null) {
				values = new HashMap<>();
				map.put(key, values);
			}
			values.put(keyForValue, value);
		}
	}
	
	public List<String> splitProductListByJobNumber(Integer jobNumber, @Nullable List<String> productsIds, @Nullable String supplierId, String channelId) {
		if (StringUtils.isEmpty(supplierId) || CollectionUtils.isEmpty(productsIds)) {
			LOG.warn("Product list or supplier ID is null or empty.");
			return null;
		} else if (jobNumber == null){
			LOG.warn("Job number is null. List will not be splitted!");
			return productsIds;
		} else {
			LOG.info("Number of products before splitting: " + productsIds.size());
			String splitNumberS = StringUtils.EMPTY;
			if (RHOF_PM_ID.equals(supplierId)) {
				LOG.info("Spliting RHOF products.");
				splitNumberS = razorConfig.getRhofSplitSize(channelId);
			} else if (VACASA_PM_ID.equals(supplierId)) {
				LOG.info("Spliting Vacasa products.");
				if (StringUtils.isNotBlank(channelId)) {
					if (Integer.parseInt(channelId) == razorConfig.getBookingChannelId()) {
						splitNumberS = razorConfig.getBatchSizeForBookingVacasaMultiunit();
					} else if (razorConfig.getExpediaChannelId_For_PM_MOR().equals(channelId)) {
						splitNumberS = razorConfig.getBatchSizeForExpediaVacasaLos();
					}
				} else {
					LOG.info("Channel id is null.");
				}
			}
			if (StringUtils.isNotBlank(splitNumberS)) {
				int splitNumber = Integer.parseInt(splitNumberS);
				int listSize = productsIds.size();
				int splitSize = listSize / splitNumber;
				List<String> subList = new ArrayList<>();
				if (jobNumber <= 0 || jobNumber > splitNumber) {
					LOG.warn("Job number " + jobNumber + " is not capable for spliting logic for supplier " + supplierId + ". List will not be splitted!");
					return productsIds;
				} else if (jobNumber != splitNumber) {
					subList = productsIds.subList(splitSize * (jobNumber - 1), (splitSize * jobNumber));
				} else {
					subList = productsIds.subList(splitSize * (jobNumber - 1), productsIds.size());
				}
				LOG.info("Number of products for job number " + jobNumber + " is " + subList.size());
				return subList;
			} else {
				LOG.warn("There isn't split size in setting table. List will not be splitted!");
				return productsIds;
			}
		}
	}
}
