package com.mybookingpal.dal.rest;

import java.util.Date;

public class ScheduleItem {
	private String id;
	private String name;
	private String state;
	private String reservation;
	private Date date;

	public ScheduleItem(){}

	public ScheduleItem(
			String id,
			String name,
			String state,
			String reservation,
			Date date) {
		this.id = id;
		this.name = name;
		this.state = state;
		this.reservation = reservation;
		this.date = date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean noId() {
		return getId() == null || getId().isEmpty();
	}

	public boolean notId(String id) {
		return this.id == null || id == null || !this.id.equals(id);
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReservation() {
		return reservation;
	}

	public void setReservation(String reservation) {
		this.reservation = reservation;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean notSameCell(ScheduleItem item) {
		return item == null || notId(item.getId()) || date.getDay() != item.getDate().getDay();
	}

}
