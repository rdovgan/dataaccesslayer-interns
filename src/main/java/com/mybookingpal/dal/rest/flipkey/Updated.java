package com.mybookingpal.dal.rest.flipkey;

import java.util.Date;

public class Updated {
	public String pi;
	public Date lu;

	public String getPi() {
		return pi;
	}

	public void setPi(String pi) {
		this.pi = pi;
	}

	public Date getLu() {
		return lu;
	}

	public void setLu(Date lu) {
		this.lu = lu;
	}
}
