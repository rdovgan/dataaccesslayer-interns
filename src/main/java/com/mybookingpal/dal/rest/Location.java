package com.mybookingpal.dal.rest;

import com.mybookingpal.dal.shared.api.HasXsl;

import javax.xml.bind.annotation.XmlRootElement;

@Deprecated
//use com.mybookingpal.dataaccesslayer.entity.LocationXsl
@XmlRootElement(name = "location")
public class Location
		implements HasXsl {
	private String locationid;
	private String country;
	private String code;
	private String name;
	private String state;
	private String subdivision;
	private String type;
	private String date;
	private String iata;
	private String coordinates;
	private String remarks;
	private Double latitude;
	private Double longitude;
	private Double altitude;
	private String message;
	private String xsl; //NB HAS GET&SET = private

	public Location() {}

	public Location(String message) {
		this.message = message;
	}

	public String getLocationid() {
		return locationid;
	}

	public void setLocationid(String locationid) {
		this.locationid = locationid;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubdivision() {
		return subdivision;
	}

	public void setSubdivision(String subdivision) {
		this.subdivision = subdivision;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getIata() {
		return iata;
	}

	public void setIata(String iata) {
		this.iata = iata;
	}

	public String getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public boolean noCoordinate() {
		return latitude == null || longitude == null; // || altitude == null;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public boolean hasState(String state) {
		return this.state != null && this.state.equals(state);
	}

	public String getId(){
		return locationid;
	}

	public void setId(String id){
		locationid = id;
	}

	public boolean noId() {
		return getId() == null || getId().isEmpty();
	}

	public boolean hasId() {
		return !noId();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getXsl() {
		return xsl;
	}

	public void setXsl(String xsl) {
		this.xsl = xsl;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Location [locationid=");
		builder.append(locationid);
		builder.append(", country=");
		builder.append(country);
		builder.append(", code=");
		builder.append(code);
		builder.append(", name=");
		builder.append(name);
		builder.append(", state=");
		builder.append(state);
		builder.append(", subdivision=");
		builder.append(subdivision);
		builder.append(", type=");
		builder.append(type);
		builder.append(", date=");
		builder.append(date);
		builder.append(", iata=");
		builder.append(iata);
		builder.append(", coordinates=");
		builder.append(coordinates);
		builder.append(", remarks=");
		builder.append(remarks);
		builder.append(", latitude=");
		builder.append(latitude);
		builder.append(", longitude=");
		builder.append(longitude);
		builder.append(", altitude=");
		builder.append(altitude);
		builder.append(", xsl=");
		builder.append(xsl);
		builder.append("]");
		return builder.toString();
	}
}