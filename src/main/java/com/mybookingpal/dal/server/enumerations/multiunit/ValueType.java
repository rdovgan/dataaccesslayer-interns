package com.mybookingpal.dal.server.enumerations.multiunit;

public enum ValueType {
	PERCENTAGE,
	FLAT,
	NIGHTS
}
