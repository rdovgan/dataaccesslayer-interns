package com.mybookingpal.dal.server.enumerations.multiunit;

public enum PaymentType {
	FULL,
	SPLIT
}
