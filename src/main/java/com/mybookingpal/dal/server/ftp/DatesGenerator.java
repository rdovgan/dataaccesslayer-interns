package com.mybookingpal.dal.server.ftp;

import java.util.Calendar;
import java.util.Date;

import com.mybookingpal.dal.utils.CommonDateUtils;

public class DatesGenerator {

	private Date fromDate;
	private Date toDate;

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getFromDateString() {
		return CommonDateUtils.convertToString(fromDate);
	}

	public String getToDateString() {
		return CommonDateUtils.convertToString(toDate);
	}

	public DatesGenerator(int days) {
		super();
		setToodayDate();
		addDaysToDate(days);
	}

	private void setToodayDate() {
		fromDate = new Date();
	}

	private void addDaysToDate(int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fromDate);
		calendar.add(Calendar.DATE, days);
		toDate = calendar.getTime();
	}
}
