package com.mybookingpal.dal.server;

import java.io.Reader;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import org.apache.log4j.Logger;
import com.mybookingpal.config.RazorConfig;

/**
 * The Class RazorServer is the server side for Razor GWT client.
 * <p>
 * A request is dispatched by the execute method to its Service according to the signature of its action. This allows services to be compact and closely allied
 * to the data table(s) that persist their instances. Each service has a single instance accessed via its static getInstance() method. The required service
 * method is dynamically resolved by the value of its action and is invoked. The value returned by the service method forms the response sent back to the
 * client.
 * <p>
 * The class also has a scheduler based on the UNIX cron command. This is used to schedule the execution of periodic housekeeping tasks.
 */
@Service
public final class RazorServer {
  private static final Logger LOG = Logger.getLogger(RazorServer.class.getName());
	private static SqlSessionFactory sqlMapper = null;
	
	private static ApplicationContext ctx;

	/**
	 * This method is no more called. We should call the getContext() method to get beans from the spring container.
	 * 
	 * Opens the database SQL session using the parameters in the specified configuration file. The database can be tuned using these parameters - but lazy
	 * loading may NOT be used. Connections are drawn from and returned to the pool as soon as possible. Ensure that service methods execute with minimum
	 * latency - long running tasks should run in forked threads.
	 *
	 * @return the current SQL session.
	 */
		public static SqlSession openSession() {
			LOG.debug("openSession " + sqlMapper);
			if (sqlMapper == null) {
				try {
					String resource = "com/mybookingpal/dal/server/api/Configuration.xml";
					Reader reader = Resources.getResourceAsReader(resource);
					sqlMapper = new SqlSessionFactoryBuilder().build(reader, RazorConfig.getDBEnvironmentId());
					LOG.debug("Using Configuration: "+RazorConfig.getDBEnvironmentId());
					LOG.debug("openSession " + sqlMapper);
				}
				catch (Throwable x) {
					LOG.error(x.getMessage());
					x.printStackTrace();
				}
			}
			return sqlMapper.openSession();
		}

	public static ApplicationContext getContext() {
		if (ctx == null) {
			LOG.debug("Initiazlizing context in Razor Server...");
			ctx = new ClassPathXmlApplicationContext("classpath:com/mybookingpal/dal/config/dao-beans.xml");
			return ctx;
		} else {
			return ctx;
		}
	}

	public static ApplicationContext getContextOnlyForMappers() {
		if (ctx == null) {
			LOG.debug("Initiazlizing context in Razor Server...");
			ctx = new ClassPathXmlApplicationContext("classpath:com/mybookingpal/dal/config/dao-beans-mappers-only.xml");
			return ctx;
		} else {
			return ctx;
		}
	}
	
	public String getHostName() 
	{
		String hostname = ""; 
		try {
			hostname =  InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} //finally{return hostname;}
		return hostname;
	}

}
