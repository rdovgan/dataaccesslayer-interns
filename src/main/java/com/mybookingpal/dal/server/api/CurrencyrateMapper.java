package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.Currencyrate;

public interface CurrencyrateMapper
extends AbstractMapper<Currencyrate> {
	Currencyrate readbyexample(Currencyrate currencyrate);
}
