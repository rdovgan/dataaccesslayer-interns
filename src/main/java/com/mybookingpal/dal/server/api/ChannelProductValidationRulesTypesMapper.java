package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelProductValidationRulesTypes;

public interface ChannelProductValidationRulesTypesMapper {

    void create (ChannelProductValidationRulesTypes channelProductValidationRulesTypes);

}
