package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.Text;

import java.util.List;

public interface TextMapper {
	Text readbyexample(Text action);
	Text readByID(String id);

	List<Text> readallbyid(String id);
}
