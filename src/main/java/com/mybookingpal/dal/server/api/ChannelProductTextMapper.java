package com.mybookingpal.dal.server.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ChannelProductText;


/**
 * @author Danijel Blagojevic
 *
 */
public interface ChannelProductTextMapper {

	List<ChannelProductText> getByProductId(Integer productId);
	List<ChannelProductText> getByProductIdAndChannelId(ChannelProductText channelProductText);
	void insertList(@Param("list") List<ChannelProductText> channelProductTextList);
	void update(ChannelProductText channelProductTextList);
	void deleteList(List<ChannelProductText> idList);
	String getHeadlineTextByProductIdAndPartyForChannel(ChannelProductText channelProductText);
	List<ChannelProductText> getHeadlineTextsByProductIdAndPartyForChannel(ChannelProductText channelProductText);
	String getShortDescriptionProductIdAndPartyForChannel(ChannelProductText channelProductText);
	String getDescriptionByProductIdAndPartyForChannel(ChannelProductText channelProductText);
	String getDisplayNameByProductIdAndPartyForChannel(ChannelProductText channelProductText);
	List<ChannelProductText> getShortDescriptionsByProductIdAndPartyForChannel(ChannelProductText channelProductText);
	List<ChannelProductText> getNameByTextType(ChannelProductText channelProductText);
}
