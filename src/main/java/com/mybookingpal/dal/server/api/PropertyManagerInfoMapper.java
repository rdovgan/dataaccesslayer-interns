package com.mybookingpal.dal.server.api;

import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.shared.PropertyManagerInfo;
import org.apache.ibatis.annotations.Param;

public interface PropertyManagerInfoMapper {

	void create(PropertyManagerInfo property);
	PropertyManagerInfo readbypmid(Integer pmID);
//	List<PropertyManagerInfo> list();
//	void update(PropertyManagerInfo item);
//	void updatebypmid(PropertyManagerInfo item);
	List<PropertyManagerInfo> listbyids(List<String> partyids);
//	String getTermsLinkByPMid(String pm_id);
	void updatebypmid(PropertyManagerInfo item);

	void updateFeeNetRate(@Param("propertyManagerId")Integer pmId, @Param("isNetRate")Boolean isNetRate);
	
	PropertyManagerInfo readbyvirtualpmid(Integer pmID);

    Date getMaxVersionForHACommission(@Param("channelPartnerId") Integer channelPartnerId, @Param("propertyManagerId")Integer pmId);
}
