package com.mybookingpal.dal.server.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.dao.entities.ReservationModel;
import com.mybookingpal.dal.shared.LocalDateRange;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.Restriction;
import com.mybookingpal.dal.shared.RoomPeriodChange;

public interface RestrictionMapper {
	
	void create(Restriction restriction);
	Restriction read(int id);
	void delete(int id);
	void deletebyproductid(int id);
	List<Restriction> readbyreservation(ReservationModel reservation);
	List<Restriction> reaByStateAndProduct(Restriction restriction);
	List<Restriction> readbyproductandstate(Restriction restriction);
	void cancelrestrictionlist(ArrayList<Integer> restrictionIdList);
	List<Restriction> readByProductIdAndDateRange(Restriction restriction);
	List<Restriction> readBySupplierIdAndDateRange(Restriction restriction);
	List<Restriction> readByProductIdAndDateRangeAndRuleType(Restriction restriction);
	List<Restriction> readBySupplierIdAndDateRangeOnlyFinal(Restriction restriction);
	void insertListWithUpdate(@Param("list") List<Restriction> restrictions);
	List<Restriction> readByEntityIdAndEntityTypeOnlyFinal(Restriction restriction);
	List<Restriction> readByEntityIdAndEntityType(Restriction restriction);
	List<Restriction> readByEntityIdAndEntityTypeAllState(Restriction restriction);
	List<Restriction> readByEntityIdAndStateAndVersion(Restriction restriction);
	List<Restriction> getProductsForRestrictionByVersion(Restriction restriction);
	Restriction readByPartyIdAndRuleType(@Param("partyId") Integer partyId, @Param("ruleType") Integer ruleType);
	List<Restriction> readCancelledByProductIdAndDateRange(Restriction restriction);
	Restriction readByProductIdAndRuleType(@Param("productId") Integer productId, @Param("ruleType") Integer ruleType);
	List<Restriction> readByPartyIdAndRuleTypeWithProducts(@Param("partyId") Integer partyId, @Param("ruleType") Integer ruleType);
	Restriction getByEntityIdAndNameAndRuleTypeAndRatePlanId(Restriction restriction);
	List<RoomPeriodChange> readRestrictionForPeriodForAllStateBySupplierIdByVersion(@Param("supplierId") String supplierId,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("version") Date version, @Param("channelId") Integer channelId);

	List<Restriction> readAllCreatedForProductBetweenDates(@Param("productId") Integer productId, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);
	List<Restriction> readCreatedRestrictionsByRatePlanIdAndRuleType(@Param("ratePlanId") Long ratePlanId, @Param("rules") List<Integer> rules);

	List<Restriction> getByProductIdOrPartyIdAndMinMaxAdvDaysRuleTypes(Restriction restriction);

	List<Restriction> readRestrictionByProductIdAndDates(@Param("productId") Integer productId, @Param("list") List<LocalDateRange> dates);

}