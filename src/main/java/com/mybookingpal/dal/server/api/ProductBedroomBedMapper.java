package com.mybookingpal.dal.server.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ProductBedroomBed;
import com.mybookingpal.dal.shared.ProductBedroomBedWithName;


/**
 * @author Danijel Blagojevic
 *
 */
public interface ProductBedroomBedMapper extends AbstractMapper<ProductBedroomBed> {
	void deleteByIdList(@Param("list") List<Integer> productBedroomBedIds);
	void deleteByProductBedroomIdList(@Param("list") List<Integer> productBedroomIds);
	void deleteById(Integer id);
	void insertUpdateFromList(@Param("list") List<ProductBedroomBed> models);
	List<ProductBedroomBed> readListOfBedroomBedsWithoutNames(Integer roomId);
	List<ProductBedroomBedWithName> readListOfBedroomBeds(Integer roomId);
	ProductBedroomBed read(Integer id);
	void create(ProductBedroomBed action);
	void deleteByRoomId(Integer roomId);
	List<ProductBedroomBed> readByProductIdList(@Param("list") List<Integer> productIdList);
	void deleteByProductId(Integer productId);
	List<ProductBedroomBed> getAllByProductId(Integer productId);
	List<ProductBedroomBedWithName> getAllByProductIdGroupByType(Integer productId);
	List<ProductBedroomBed> getAllByBedroomId(Integer bedroomId);
    Integer readNumberOfBedsForProduct(String productId);
    List<ProductBedroomBedWithName> getAllByProductIdGroupByBedroomIDAndType(Integer productId);
}
