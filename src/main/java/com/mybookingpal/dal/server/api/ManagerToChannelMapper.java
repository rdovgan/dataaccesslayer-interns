package com.mybookingpal.dal.server.api;

import java.util.ArrayList;
import java.util.List;

import com.mybookingpal.dal.shared.ChannelDistribution;
import com.mybookingpal.dal.shared.ManagerToChannel;
import com.mybookingpal.dal.shared.ProductAction;
import com.mybookingpal.utils.entity.NameId;
import org.apache.ibatis.annotations.Param;

public interface ManagerToChannelMapper {
	
	ArrayList<ManagerToChannel> listAllChannelPropertyManagers ();
	ArrayList<ChannelDistribution> propertyManagersDistributingToChannels ();
	ArrayList<ManagerToChannel> listPropertyManagersDistributedChannel(String channel_id);
	ArrayList<String> listPropertyManagersNotDistributedChannel(String channel_id);
	List<String> justPropertyManagersDistributedChannel (String channel_id);
	List<String> listPMNotDistributedToChannelString (List<String> supplierIds);
	List<ManagerToChannel> listFeed();
	ManagerToChannel getByPmIDAndChannelID(NameId nameId);
	ManagerToChannel getByPmIDAndChannel(NameId nameId);
	List<ManagerToChannel> readAll();
	List<Integer> getChannelPartnerIdsByPmId(String id);
	List<Integer> listDistributedPMIdsByAbbreviation(String abbreviation);
	List<String> justPMCreatedOrNotChangedInAWhile(ProductAction channel_id);
	void updateFeeNetRateByPropertyManagerAndChannelPartner(@Param("propertyManagerId") Integer pmId, @Param("channelPartnerId") Integer cpId, @Param("isNetRate") Boolean netRate);
	List<ManagerToChannel> findSupplierForExpedia(Integer supplierId);
}
