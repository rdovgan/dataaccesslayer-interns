package com.mybookingpal.dal.server.api;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ChannelPromoMap;

public interface ChannelPromoMapMapper {

	void createPromoMap(ChannelPromoMap channelPromoMap);

	void updatePromoMapByPromoId(ChannelPromoMap channelPromoMap);

	 List<ChannelPromoMap> readPromoMapsByYieldId(Integer yieldId);
	 
	 ChannelPromoMap readPromoMap(Integer yieldId);

	ChannelPromoMap readPromoMapByPromoId(String channelPromoId);
	
	List<ChannelPromoMap> readPromoMapByChannelProductIdChannelId(ChannelPromoMap channelPromoMap);

	void deleteByProductIdChannelIdAndYieldId(ChannelPromoMap channelPromoMap);

	List<ChannelPromoMap> readByProductIdChannelIdAndYieldId(ChannelPromoMap channelPromoMap);

	void  deleteByChannelIdProductIdChannelRateId(ChannelPromoMap channelPromoMap);
	
	public List<ChannelPromoMap> readChannelPromoMapByYieldId(int yieldId);
	
	public List<ChannelPromoMap> readChannelPromoMapForAvailabilityStatus(@Param("channelId") int channelId,
			@Param("fromdate") Date fromdate, @Param("todate") Date todate, @Param("entitytype") String entitytype);
	
	public List<ChannelPromoMap> readChannelPromoMapForExpiredStatus(@Param("fromdate") Date fromdate, 
			@Param("todate") Date todate, @Param("entitytype") String entitytype);
	
	public List<ChannelPromoMap> readChannelPromoMapByProductIdWithAvailabilityStatus(@Param("channelId") int channelId,
			@Param("fromdate") Date fromdate, @Param("todate") Date todate, @Param("entitytype") String entitytype, 
			@Param("productIds") List<String> productIds);
	
	public List<ChannelPromoMap> readChannelPromoMapByProductIdWithExpiredStatus(@Param("fromdate") Date fromdate, 
			@Param("todate") Date todate, @Param("entitytype") String entitytype, @Param("productIds") List<String> productIds);
	
	public List<String> readPromotionApplicableChannelPromoMap(@Param("channelId") int channelId,
			@Param("fromdate") Date fromdate, @Param("todate") Date todate, @Param("entitytype") String entitytype);
	
	void update(ChannelPromoMap channelPromoMap);

	void delete(String id);
}
