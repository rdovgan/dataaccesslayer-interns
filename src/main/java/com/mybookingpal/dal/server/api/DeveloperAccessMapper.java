package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.DeveloperAccess;

public interface DeveloperAccessMapper {

	void create(DeveloperAccess developerAccess);

	void updateById(DeveloperAccess developerAccess);

	void updateByPartyId(DeveloperAccess developerAccess);

	void deleteByPartyId(Integer partyId);

	DeveloperAccess readByPartyId(Integer partyId);

	DeveloperAccess readByApiKey(String apiKey);

	DeveloperAccess readByEmail(String email);
}
