package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelReservationTransaction;

import java.util.List;

public interface ChannelReservationTransactionMapper {

    void create(ChannelReservationTransaction channelReservationTranscation);
    void update(ChannelReservationTransaction channelReservationTranscation);
    ChannelReservationTransaction readByReservationId(Integer reservationId);
    ChannelReservationTransaction readByConfirmationId(String confirmationId);
    List<ChannelReservationTransaction> findAllChannelReservationPayloadsWithReservationIdZero();

}