package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelSpecificTaxGroupRelation;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ChannelSpecificTaxGroupRelationMapper {

    List<ChannelSpecificTaxGroupRelation> getByGroupIdsAndChannelId(@Param("list") List<Integer> groupIds, @Param("channelId") Integer channelId);
}
