package com.mybookingpal.dal.server.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ChannelYieldMap;

/**
 * @author Danijel Blagojevic
 *
 */
public interface ChannelYieldMapMapper {

	void create(ChannelYieldMap channelYieldMap);
	List<ChannelYieldMap> getActiveRuleGroupsByRuleId(String ruleId);
	void update(ChannelYieldMap channelYieldMap);
	List<ChannelYieldMap> getActiveByYieldId(Integer yieldId);
	ChannelYieldMap getActiveByYieldIdAndRuleId(@Param("yieldId") Integer yieldId, @Param("ruleId") String ruleId);
}
