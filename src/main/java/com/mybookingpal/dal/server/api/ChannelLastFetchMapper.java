package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelLastFetch;
import com.mybookingpal.dal.shared.IdVersion;

import java.util.List;

public interface ChannelLastFetchMapper {
	void update(ChannelLastFetch action);
	ChannelLastFetch read(ChannelLastFetch action);
	void create(ChannelLastFetch action);
	List<IdVersion> getLastfetch(List<Integer> productIds);
	void deleteByProductId(String productId);
	List<IdVersion> getMaxRateVersions(List<Integer> productIds);
}
