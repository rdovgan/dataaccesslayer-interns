package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ProductResidencyCategory;

public interface ProductResidencyCategoryMapper  extends AbstractMapper<ProductResidencyCategory>{
	
	ProductResidencyCategory readByProductId(Integer productId);

	void create(ProductResidencyCategory productResidencyCategory);

	void update(ProductResidencyCategory productResidencyCategory);

}
