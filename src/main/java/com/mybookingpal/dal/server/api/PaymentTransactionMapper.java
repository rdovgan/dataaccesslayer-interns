package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.NameIdAction;
import com.mybookingpal.dal.shared.PaymentTransaction;

public interface PaymentTransactionMapper {
	void create(PaymentTransaction paymentTransaction);
	PaymentTransaction read(int id);
	void update(PaymentTransaction paymentTransaction);
	void delete(int id);
	List<PaymentTransaction> transactionToProcessRefund();
	List<PaymentTransaction> readByReservationId(int reservationId);
	List<PaymentTransaction> readAcceptedTransactionsByReservationId(int reservationId);
	PaymentTransaction getFailedTransactionByTransactionId(Long transactionId);
	PaymentTransaction getInitialTransactionByReservationId(int reservationId);
	List<PaymentTransaction> listLast24hours(NameIdAction action);
	PaymentTransaction readLastFailedTransactionForGroupedReservation(Long groupedReservationId);
	List<PaymentTransaction> readTransactionsByIds(List<Long> transactionIds);
	List<PaymentTransaction> readAllTransactionsByIds(List<Long> transactionIds);

}
