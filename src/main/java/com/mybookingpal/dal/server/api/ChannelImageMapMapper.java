package com.mybookingpal.dal.server.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ChannelImageMap;

public interface ChannelImageMapMapper {

	void createImageMap(ChannelImageMap channelImageMap);

	List<ChannelImageMap> readImageMap(Integer imageId);

	ChannelImageMap readImageMapByImageId(ChannelImageMap channelImageMap);
	
	ChannelImageMap readImageMapByImageIdRoomId(ChannelImageMap channelImageMap);

	void updateChannelImageId(ChannelImageMap channelImageMap);
	
	void updateChannelImageMap(ChannelImageMap channelImageMap);

	void deleteById (@Param("id") String id);

	void deleteImageByImageId(ChannelImageMap channelImageMap);

	void updateStateByChannelImageIdChannelProductId(ChannelImageMap channelImageMap);

	void updateStateByChannelImageIdsChannelProductId(@Param("channelProductId") String channelProductId, @Param("list")  List<String> channelImageIds);

	List<Integer> getPropertiesForUploadForChannel (@Param("channelId") Integer channelId);

	List<ChannelImageMap> readImageMapsByChannelProductIdAndChannelImageIdAndChannelId(ChannelImageMap channelImageMap);

	List<ChannelImageMap> readImageMapByProductIdChannelId(ChannelImageMap channelImageMap);
	
	List<ChannelImageMap> readAllImageMapByProductIdChannelId(ChannelImageMap channelImageMap);

	ChannelImageMap readImageMapByProductIdChannelIdRoomIdImageId(ChannelImageMap channelImageMap);
	
	List<ChannelImageMap> readImageMapWithoutChannelImage (ChannelImageMap channelImageMap);
	
	List<ChannelImageMap> readImageMapInactive (ChannelImageMap channelImageMap);
	
	List<ChannelImageMap> readAllImageMapByChannelProductIdRoomIdChannelId (ChannelImageMap channelImageMap);

	List<ChannelImageMap> readImageMapActive (ChannelImageMap channelImageMap);

}
