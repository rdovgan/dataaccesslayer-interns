package com.mybookingpal.dal.server.api;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.Customer;

import java.util.List;

public interface CustomerMapper {

	void createCustomer(Customer customer);

	void updateCustomer(Customer customer);

	Customer readCustomer(@Param("id") Integer id);

	Customer readCustomerByReservationId(@Param("reservationId") Integer reservationId);

	List<String> readMarriottCustomerNames();

	List<Customer> readDecryptedCustomers(@Param("ids") List<Integer> ids);
}