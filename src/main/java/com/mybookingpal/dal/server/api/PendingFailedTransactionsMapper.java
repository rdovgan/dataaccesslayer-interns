package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.dao.entities.FailedTransactionPaymentModel;
import com.mybookingpal.dal.shared.PendingFailedTransaction;
import com.mybookingpal.shared.dao.FailedTransactionPaymentRequest;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PendingFailedTransactionsMapper {

	List<FailedTransactionPaymentModel> readAllFailedTransactionPayments(@Param("request") FailedTransactionPaymentRequest request);

	void update(PendingFailedTransaction model);

	PendingFailedTransaction read(Long id);

	PendingFailedTransaction readPaymentsByTransactionId(Long transactionId);

	List<PendingFailedTransaction> readPaymentsByReservationId(Long reservationId);

	void create(PendingFailedTransaction pendingFailedTransaction);

	PendingFailedTransaction readActiveTransactionByGroupedReservationId(Long id);

	void markAsEmailSentToAgent(Long id);
	
	List<PendingFailedTransaction> readAllActiveFailedPaymentReservationsByChannelId(Integer id);
	
	
}
