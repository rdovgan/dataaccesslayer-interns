package com.mybookingpal.dal.server.api;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import com.mybookingpal.aop.annotations.UpdateCache;
import com.mybookingpal.dal.annotation.SqlUpdateMarker;
import com.mybookingpal.dal.json.price.PriceWidgetItem;
import com.mybookingpal.dal.shared.IdVersion;
import com.mybookingpal.dal.shared.LocalDateRange;
import com.mybookingpal.dal.shared.PriceTable;
import com.mybookingpal.dal.soap.ota.server.OtaRate;
import com.mybookingpal.dal.utils.GenericList;
import com.mybookingpal.rest.entity.ProductDate;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.Price;
import com.mybookingpal.dal.shared.RoomPeriodChange;
import com.mybookingpal.dal.shared.action.DatesIdAction;
import com.mybookingpal.dal.shared.price.PriceSearch;

public interface PriceMapper {

	Price read(String id);

//	@UpdateCache
	void deleteByIdList(List<Price> idList);

	Price altidfromdate(Price action);

	Price exists(Price action);

	Price existsAltID(Price action);

//	@UpdateCache
	void cancelversion(Price action);

//	@UpdateCache
	void cancelversionbypartyid(Price action);

	@SqlUpdateMarker
//	@UpdateCache
	void cancelpricelist(@Param("list") GenericList<Price> priceIdList, @Param("version") Date version);

	Price readbydate(Price action);

	Price readExactMatch(Price action);

	Integer readMaxMinStayByPeriod(Price action);

	Integer readArrivalMinStay(Price action);

	Price getcheckinprice(List<String> priceids);

	List<Price> readbytype(Price action);

	List<Price> entityfeature(Price action);

	Price readperdiemtax(Price action);

	List<Price> readsplitpriceperiods(Price action);

	Date maxVersionForProduct(Price action);

	Date maxDate(Price action);

	Price getpropertydetailcheckinprice(Price action);

	Date maxToDate(Price action);

	Integer countValidPricesForProduct(Price price);

	void copyPrices(@Param("fromProductId") Integer fromProductId, @Param("toProductId") Integer toProductId);

	List<Price> readByEntityIdAndEntityTypeStateByVersion(Price price);

	Double getPriceForRange(Price price);

	List<Price> readByEntityIdAndState(String entityId);

	List<Price> getProductsForPriceByVersion(Price price);


	// API Search
	List<Price> readbydateandids(PriceSearch action);

	List<Price> readRatePlanPricesByDateAndIds(PriceSearch action);

	//REST
	List<Price> restread(Price action);

	List<Price> restreadLimitedRows(Price action);

	//OTA
	List<OtaRate> otarates(OtaRate action);

	//JSON
	List<PriceWidgetItem> pricewidget(PriceTable action);

	List<PriceWidgetItem> featurewidget(PriceTable action);

	List<Price> existsPrice(Price action);

	//XML Writer
//	@UpdateCache
	void delete(String id);

	List<Price> readByEntityId(Price price);

	Price readByEntityIdAndSupplierId(Price price);

	List<Price> readByEntityIdWithFinal(Price price);

	List<Price> readByEntityIdAndEntityType(Price price);

	List<Price> readByEntityIdAndEntityTypeWithFinal(Price price);

	List<Price> readByEntityIdAndEntityTypeOnlyFinal(Price price);
	
	List<Price> findFinalStateByEntityId(String entityId);

	Price readByFromDate(Price price);

	List<Price> readContractFee(Price price);

	Price readContractFees(Price price);

	List<Price> readFeesAndFeatures(Price price);

	//@UpdateCache
	void insertList(@Param("list") List<Price> prices);

	@SqlUpdateMarker
	//@UpdateCache
	void insertListWithUpdate(@Param("list") GenericList<Price> prices);

	List<Price> readByEntityIdAndDate(Price price);

	@SqlUpdateMarker
//	@UpdateCache
	void create(Price action);

	@SqlUpdateMarker
//	@UpdateCache
	void update(Price action);

	//xmls generation block
	List<Price> readByProductIdAndDateRangeForOnePriceRowProducts(DatesIdAction action);

	List<Price> readByProductIdAndDateRange(DatesIdAction action);

	// CS: Added to get all the fees (man and opt) for razor products
	List<Price> readMandatoryFees(Price price);

	List<Price> readOptionalFees(Price price);

//	@UpdateCache
	void makeFinalByProductId(String productId);

	List<Price> readFinal(int limit);

	List<Price> getAllByEntityIdOrderByDateAsc(Price price);

//	@UpdateCache
	void updateStateOnFinalPerPm(String partyId);

	Price getAllByEntityIdAndNameAndRatePlanId(Price price);

	Price getAllByEntityIdAndNameAndRatePlanIdAndDate(Price price);

	/**
	 * This method is specific in the way that it checks last change for all prices, even which are in Final state.
	 */
	Date getLastChangeSinceLastFetch(@Param("productId") String productId, @Param("lastFetch") String lastFetch);

	List<Price> listPriceByEntityAndLastVersion(Integer entityId);

	Price readPriceByRatePlanId(Long ratePlanId);

//	@UpdateCache
	void makePriceFinalByRatePlanId(Long makePriceFinalByRatePlanId);

	Price readPriceById(Integer priceId);

	Price readPriceForBaseRatePlan(@Param("productId") String productId, @Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate);

	List<Price> readListPricesForBaseRatePlan(@Param("productId") String productId, @Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate);

	List<Price> readPricesByProductIdAndDateRange(@Param("productId") Integer productId, @Param("fromDate") LocalDate fromDate,
			@Param("toDate") LocalDate toDate);

	List<Price> readByProductIdWithRatePlanId(@Param("productId") String productId);

	Price getLowestActivePriceForProduct(Price action);

	List<RoomPeriodChange> readPriceForPeriodForAllStateBySupplierIdByVersion(@Param("supplierId") String supplierId, @Param("productId") String productId,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("version") Date version, @Param("channelId") Integer channelId);

	List<IdVersion> getMaxRateVersions(List<Integer> productIds);

	List<Price> readAllPricesByProductIdAndDateRange(@Param("productId") Integer productId, @Param("fromDate") LocalDate fromDate,
			@Param("toDate") LocalDate toDate);

//	@UpdateCache
	void insertListPrices(@Param("list") List<Price> prices);

//	@UpdateCache
	void setFinalStateListPrices(@Param("ids") List<Integer> list);

	List<Price> getPricesByIdList(@Param("ids") List<Long> list);

	List<Price> getPricesByProductId(@Param("productId") Integer productId);

	List<ProductDate> findByVersionAndMonthPeriod(@Param("version") LocalDateTime version, @Param("versionInterval") int versionInterval,
			@Param("priceInterval") int priceInterval);

	List<Price> readPriceDiscrepancyByProductIdAndDateRange(@Param("productId") Integer productId, @Param("fromDate") LocalDate fromDate,
			@Param("toDate") LocalDate toDate);

//	@UpdateCache
	void updateCurrency(Price price);

	Price getContractFees(Price price);

	List<Price> getFeesAndFeatures(Price price);

	List<Price> readPriceByProductIdAndDates(@Param("productId") Integer productId, @Param("list") List<LocalDateRange> dates);

	List<Price> readAllPricesByProductListAndDateRange(@Param("list") List<Integer> list, @Param("fromDate") LocalDate fromDate,
			@Param("toDate") LocalDate toDate);
}