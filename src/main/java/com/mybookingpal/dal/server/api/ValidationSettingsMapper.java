package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.dao.entities.ValidationSettingsModel;

public interface ValidationSettingsMapper {

	void create(ValidationSettingsModel validationSettingsModel);
	List<ValidationSettingsModel> readAllForProduct(Long productId);
	List<ValidationSettingsModel> readAllForPropertyManager(Long propertyManagerPartyId);
}
