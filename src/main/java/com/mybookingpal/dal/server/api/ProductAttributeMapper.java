package com.mybookingpal.dal.server.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.shared.IdVersion;
import com.mybookingpal.shared.dto.KeyCollection;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ProductAttribute;



public interface ProductAttributeMapper {
	void create(ProductAttribute productAttribute);
	ArrayList<String> readAttributesByProductId(Integer productId);
	Integer getAttributesCountByProductIdAndAttributeId(ProductAttribute productAttribute);
	List<ProductAttribute> getAllByProductId(String productId);

	List<String> productIds(ProductAttribute productAttribute);
	List<String> attributeIds(ProductAttribute productAttribute);
	ProductAttribute readByProductIdAndAttribute(@Param("productId") Integer productId, @Param("attribute") String attribute);
	List<ProductAttribute> getProductAttributesFromLastTimeStamp(@Param("productId") String productId, @Param("version") Date version);
	void insertList(List<ProductAttribute> productAttributeList);
	void deleteRoomCodesByProductId(Integer productId);
	void deleteHotelCodesByProductId(Integer productId);
	void delete(ProductAttribute productAttribute);
	List<ProductAttributeWithName> getAllWithNamesByProductId(String productId);
	void updateQuantity(ProductAttribute productAttribute);
	ProductAttribute getByProductIdAndAttributeId(ProductAttribute productAttribute);
	void deleteByProductId(String productId);
	List<String> readAttributeByProductId(Integer id);
	void deleteByProductIdAndAttribute(@Param("productId")String productId, @Param("attribute") String attribute);
	List<ProductAttribute> readAttributesByProductIds(List<String> productIds);
	List<ProductAttribute> readAttributesStartWithPctForProducts(List<String> productIds);
	List<String> readAllAttributesBeginWithPrefixForProduct(@Param("productId") Integer productId, @Param("prefix") String prefix);
	List<ProductAttributeWithName> readAttributesDisplayNameForProduct(Integer productId);
	List<IdVersion> getLastUpdateByProductIds(@Param("productIds") List<Integer> productIds);
	KeyCollection getKeyCollection(@Param("productId") Integer productId);

}
