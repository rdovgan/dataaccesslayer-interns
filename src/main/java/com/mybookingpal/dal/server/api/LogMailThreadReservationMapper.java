package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.LogMailThreadReservation;

import java.util.List;

public interface LogMailThreadReservationMapper {

	void create(LogMailThreadReservation logMailThreadReservation);

	List<LogMailThreadReservation> readByExample(LogMailThreadReservation example);
}
