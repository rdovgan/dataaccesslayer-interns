package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelCityMapping;

import java.util.List;

/**
 * @see ChannelCityMapping
 */
public interface ChannelCityMappingMapper {
	void create(ChannelCityMapping action);

	void update(ChannelCityMapping action);

	List<ChannelCityMapping> readByExample(ChannelCityMapping action);

	ChannelCityMapping readByChannelIdAndLocationId(ChannelCityMapping channelCityMapping);
}
