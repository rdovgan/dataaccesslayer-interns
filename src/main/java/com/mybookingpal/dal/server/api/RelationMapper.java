package com.mybookingpal.dal.server.api;

import java.util.ArrayList;

import com.mybookingpal.dal.shared.Relation;

public interface RelationMapper {
	ArrayList<String> lineids(Relation relation);
	ArrayList<Relation> list(Relation relation);
	ArrayList<String> headids(Relation relation);


}
