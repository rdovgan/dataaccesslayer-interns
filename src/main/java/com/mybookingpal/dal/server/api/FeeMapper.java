package com.mybookingpal.dal.server.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.shared.LocalDateRange;
import com.mybookingpal.dal.shared.RoomPeriodChange;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.Fee;
import com.mybookingpal.dal.shared.NameIdCountOffset;
import com.mybookingpal.dal.shared.fee.FeeSearch;

public interface FeeMapper {
	void create (Fee action);
	void update (Fee action);
	Fee exists(Fee action);
	Fee equalExists(Fee action);
	Fee read(int id);
	void cancelversion(Fee action);
	void cancelfeelist(List<Integer> feeIdList);
	void insertList(@Param("list") List<Fee> fees);
	void insertListWithUpdate(@Param("list") List<Fee> fees);
	ArrayList<Fee> readbyproductandstate(Fee action);
	ArrayList<Fee> readAllUniqueFeesForAProduct(Fee action);
	ArrayList<Fee> readUniquebyproductandstate(Fee action);
	ArrayList<Fee> readUniquebyproductandstateandmaxval(Fee action);
	ArrayList<Fee> readUniquebyproductandstateandminperson(Fee action);
	
	List<Fee> readbydateandproduct(Fee action);
	List<Fee> readspecialbydateandproduct(Fee action);
	List<Fee> listbyproductids(FeeSearch action);
	ArrayList<Fee> readbyProductandStateandValueTypeandTaxType(Fee action);
	
	ArrayList<Fee> readByChannelPage(NameIdCountOffset action);
	ArrayList<Fee> readAll(int channelPartnerId);
	
	ArrayList<Fee> readbyproductandunit(Fee action);
	ArrayList<Fee> readbyproductanddifrentunit(Fee action);

	List<RoomPeriodChange> readFeesForPeriodForAllStateBySupplierIdByVersion(@Param("supplierId") String supplierId,
		@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("version") Date version, @Param("channelId") Integer channelId);

	List<Fee> getAllFeesByProductAndVersion(Fee fee);
	List<Fee> getAllEntityTypeFeesByProductAndVersion(Fee fee);
	List<Fee> getMandatoryAndOnArrivalFeesByProduct(Fee fee);
	Fee readFeeByRatePlanId(Long ratePlanId);
	ArrayList<Fee> readbyproductandunitformultipleproperties(Fee action);
	List<Fee> getExtraPersonFeeByProductAndVersion(Fee fee);

	List<Fee> readByProductIds(@Param("productIds") List<String> productIds, @Param("state") Integer state);

	ArrayList<Fee> readUniqExtraPersonFee(Fee action);
	ArrayList<Fee> readUniqFeesWithoutExtraPersonFee(Fee action);
	ArrayList<Fee> readUniqExtraPersonFeeForKey(Fee action);
	ArrayList<Fee> readUniqFeesWithoutExtraPersonFeeForKey(Fee action);
	ArrayList<Fee> readAllMandatoryFeeswithoutExtraPersonFee(Fee action);
	ArrayList<Fee> readMandatoryFeesForProduct(Fee action);
	List<String> getUpdatedFees(@Param("days") int daysBack);

	List<Fee> readFeeByProductIdAndDates(@Param("productId") Integer productId, @Param("list") List<LocalDateRange> dates);

	List<Fee> readFeeByProductIdAndDatesAndNameTemplate(@Param("productId") Integer productId, @Param("dates") List<LocalDateRange> dates,
			@Param("templates") List<FeeSearch> searchFees);

	Fee getSecurityDepositFee(@Param("productId") Integer productId);
}