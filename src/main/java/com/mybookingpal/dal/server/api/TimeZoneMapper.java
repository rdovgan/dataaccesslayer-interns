package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.NameIdAction;
import com.mybookingpal.dal.shared.TimeZone;

import java.util.List;

public interface TimeZoneMapper {
	List<TimeZone> readAll(NameIdAction action);
	void delete(TimeZone timeZone);
	void create(TimeZone timeZone);
	void update(TimeZone timeZone);
	List<TimeZone> readByExample(TimeZone timeZone);
	TimeZone read(int id);
}
