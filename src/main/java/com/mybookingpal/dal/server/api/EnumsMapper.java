package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.Enums;

public interface EnumsMapper {
	
	Integer getIdByValue(String value);
	List<Enums> readByField(String fied);

}
