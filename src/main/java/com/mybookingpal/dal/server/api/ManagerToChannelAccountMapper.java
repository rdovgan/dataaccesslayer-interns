package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.ProductAction;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ManagerToChannelAccount;

public interface ManagerToChannelAccountMapper {

	List<ManagerToChannelAccount> readByPropertyManagerAndChannelPartner(ManagerToChannelAccount managerToChannelAccount);

	List<ManagerToChannelAccount> findActiveAccountsByChannelPartner(ManagerToChannelAccount managerToChannelAccount);

	List<ManagerToChannelAccount> justPMCreatedOrSuspendedNotChangedInAWhile(ProductAction productAction);
	
	List<ManagerToChannelAccount> findActiveAccountsWithMltRepProperties(@Param("list")List<Integer>listOfAllSupplierIds, @Param("channelId") Integer channelId);

	void update(ManagerToChannelAccount managerToChannelAccount);

	void create(ManagerToChannelAccount managerToChannelAccount);

	void updateState(ManagerToChannelAccount managerToChannelAccount);

	List<ManagerToChannelAccount> findByPropertyManagerAndChannelPartnerOrderByVersion(ManagerToChannelAccount managerToChannelAccount);

	List<ManagerToChannelAccount> readByPropertyManagerAndChannelPartnerWithoutState(ManagerToChannelAccount managerToChannelAccount);

	List<String> getAllActiveAPMIdsForChannel(@Param("channelID") Integer channelID);

	List<ManagerToChannelAccount> findByActivePropertyManager(@Param("supplierId") Integer supplierId);
	
	List<ManagerToChannelAccount> getlAllActiveVirtualAccountsForSupplier(@Param("supplierId") Integer supplierId);

	String getPaymentCredentialsBySupplierIdAndChannelId(ManagerToChannelAccount managerToChannelAccount);

	List<ManagerToChannelAccount> findSupplierForExpedia(String supplierId);
}
