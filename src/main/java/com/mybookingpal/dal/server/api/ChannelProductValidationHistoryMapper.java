package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelProductValidationHistory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelProductValidationHistoryMapper {

    void create (ChannelProductValidationHistory channelProductValidationHistory);

    ChannelProductValidationHistory getChannelProductValidationHistoryByProductIdAndChannelId (@Param("productId") Integer productId, @Param("channelId") Integer channelId);

    List<ChannelProductValidationHistory> getChannelProductValidationHistoryByProductIdsAndChannelId (@Param("productIds") List<Integer> productIds, @Param("channelId") Integer channelId);


}
