package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelAttributeMapping;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelAttributeMappingMapper {

	List<ChannelAttributeMapping> readbyCodes(@Param("list") List<String> attributeCodes, @Param("channelId") int channelId);

	void create(ChannelAttributeMapping channelAttributeMapping);

	List<ChannelAttributeMapping> readByExample(ChannelAttributeMapping example);

	List<ChannelAttributeMapping> readExistingListByCodesAndChannelId(@Param("list") List<String> attributeCodes, @Param("channelId") Integer channelId);

}
