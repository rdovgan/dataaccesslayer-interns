package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.ChannelPartyToProduct;

public interface ChannelPartyToProductMapper {
	void createChannelPartyToProduct(ChannelPartyToProduct tempChannelPartyToProduct);

	void updateChannelProductId(ChannelPartyToProduct tempChannelPartyToProduct);

	ChannelPartyToProduct readByProductId(String productId);

	ChannelPartyToProduct readByProductId(ChannelPartyToProduct tempChannelPartyToProduct);

	List<ChannelPartyToProduct> readByPartyId(ChannelPartyToProduct tempChannelPartyToProduct);

	List<String> readBySupplierId(ChannelPartyToProduct tempChannelPartyToProduct);

	List<String> readProductIdByPartyId(ChannelPartyToProduct tempChannelPartyToProduct);

	List<String> readByProductIdAndPartyId(ChannelPartyToProduct tempChannelPartyToProduct);
	
	List<String> readSupplierIdByPartyId(String supplierId);
	
	List<String> getVirtualAccountsForPM();
	
	String getVirtualAccountsForPMBySupplierId(ChannelPartyToProduct tempChannelPartyToProduct);
}
