package com.mybookingpal.dal.server.api;

import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;

public interface TimelineMapper {

	void createDays(@Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate);

	void dropDays();

}
