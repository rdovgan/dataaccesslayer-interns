package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.ChannelProductYieldMap;

public interface ChannelProductYieldMapMapper {
	// CS: change the return type to list
	public List<ChannelProductYieldMap> getChannelRateId(ChannelProductYieldMap channelProductYieldMap);

	void create(ChannelProductYieldMap action);

	void update(ChannelProductYieldMap action);

	void deleteByProductId(ChannelProductYieldMap action);
	
	void deleteById(String id);
	
	void deleteByChannelIdProductIdChannelRateId(ChannelProductYieldMap channelProductYieldMap);
	
	public ChannelProductYieldMap getChannelRateName(ChannelProductYieldMap channelProductYieldMap);

	public ChannelProductYieldMap getYieldRuleNameByChannelRateId(ChannelProductYieldMap channelProductYieldMap);
	
	public List<ChannelProductYieldMap> getPromoRatesByChannelIdProductId(ChannelProductYieldMap channelProductYieldMap);
	
	public List<ChannelProductYieldMap> getByProductId(ChannelProductYieldMap channelProductYieldMap);
}
