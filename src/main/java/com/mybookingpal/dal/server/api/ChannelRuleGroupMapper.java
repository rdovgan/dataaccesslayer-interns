package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.ChannelRuleGroup;

/**
 * @author Danijel Blagojevic
 *
 */
public interface ChannelRuleGroupMapper {

	void create(ChannelRuleGroup channelRuleGroup);
	ChannelRuleGroup getByRuleId(String ruleId);
	List<ChannelRuleGroup> getActiveRuleGroupsByProductId(Integer productId);
	void update(ChannelRuleGroup channelRuleGroup);
}
