package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelPrice;

public interface ChannelPriceMapper extends AbstractMapper<ChannelPrice> {

	void create(ChannelPrice action);

	void update(ChannelPrice action);

	void delete(ChannelPrice action);

}