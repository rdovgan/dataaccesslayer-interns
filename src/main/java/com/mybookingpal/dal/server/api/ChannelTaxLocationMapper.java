package com.mybookingpal.dal.server.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ChannelTaxLocation;

public interface ChannelTaxLocationMapper {
	public void createAirbnbTaxLocation(ChannelTaxLocation channelTaxLocation);
	public void createAirbnbTaxLocationList(@Param("list") List<ChannelTaxLocation> channelTaxLocationList);
	public List<ChannelTaxLocation> readByLocationIDAndChannelID(ChannelTaxLocation channelTaxLocation);
	public void updateByLocationIDAndChannelID(ChannelTaxLocation channelTaxLocation);
	public List<ChannelTaxLocation> readByZipCode9AndChannelID(ChannelTaxLocation channelTaxLocation);
	public ChannelTaxLocation readByZipCode9(@Param("zipcode") String zipcode, @Param("channelId") Integer channelId);
	public void updateAirbnbTaxLocationByID(ChannelTaxLocation channelTaxLocation);
	public List<ChannelTaxLocation> readAllIdWithZipCode9();
	public void delete(@Param("id") Integer id);
	ChannelTaxLocation readByZipCode9AndChannelIDTax(ChannelTaxLocation channelTaxLocation);
}
