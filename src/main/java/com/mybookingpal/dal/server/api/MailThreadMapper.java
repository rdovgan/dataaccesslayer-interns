package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.messaging.MailThreadAndMessage;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.messaging.MailThread;

import java.util.List;

public interface MailThreadMapper {

	void create(MailThread action);
	void update(MailThread action);
	MailThread readByThreadId(@Param("threadId") String threadId);
	MailThread readByReservationId(@Param("reservationId") String reservationId);
	MailThread readById(@Param("id") Integer id);
	List<MailThreadAndMessage> getThreadAndMessageByPartyId(@Param("partyId") String partyId, @Param("paginationLimit") Integer pagination,
															@Param("paginationPage") Integer paginationPage, @Param("type") String type);
	List<Integer> readAllThreadIdByPartyId(@Param("partyId") String partyId);
	MailThread readByMessageId(@Param("messageId") String messageId);
	MailThread readByGuestIdAndChannelProductId(@Param("channelProductId") String channelProductId, @Param("channelGuestId") String channelGuestId);
	MailThread readByThreadIdAndPmId(@Param("id") Integer id, @Param("partyId") Integer partyId);
	MailThread readByExample(MailThread mailThread);
	void updateVersion(MailThread mailThread);

}
