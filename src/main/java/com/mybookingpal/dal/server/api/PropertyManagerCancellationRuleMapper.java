package com.mybookingpal.dal.server.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.cancellation.PropertyManagerCancellationDateRule;
import com.mybookingpal.dal.shared.cancellation.PropertyManagerCancellationNightsRule;
import com.mybookingpal.dal.shared.cancellation.PropertyManagerCancellationRule;

public interface PropertyManagerCancellationRuleMapper {
	List<PropertyManagerCancellationDateRule> readdatebypmid(Integer propertyManagerId);

	List<PropertyManagerCancellationRule> readbypmid(Integer propertyManagerId);

	List<PropertyManagerCancellationRule> readCancellationsByRatePlanId(Long ratePlanId);

	List<PropertyManagerCancellationNightsRule> readByChannelIdSupplierId(@Param("supplierId") Integer supplierId, @Param("list") List<Integer> channelIds);
	List<PropertyManagerCancellationNightsRule> readByChannelIdProductId(@Param("productId") Integer productId, @Param("list") List<Integer> channelIds);
	
	void create(PropertyManagerCancellationRule rule);
	PropertyManagerCancellationRule read(Integer id);
	
	void update(PropertyManagerCancellationRule rule);

	List<PropertyManagerCancellationRule> readByPmId(String pmId);
}
