package com.mybookingpal.dal.server.api;

import java.util.Date;
import java.util.List;

import com.mybookingpal.aop.annotations.UpdateCache;
import com.mybookingpal.dal.annotation.SqlUpdateMarker;
import com.mybookingpal.dal.shared.LocalDateRange;
import com.mybookingpal.dal.utils.GenericList;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.json.minstay.WeeklyMinstay;
import com.mybookingpal.dal.shared.MinStay;
import com.mybookingpal.dal.shared.PropertyMinStay;
import com.mybookingpal.dal.shared.RoomPeriodChange;
import com.mybookingpal.dal.shared.action.DatesIdAction;
import com.mybookingpal.dal.shared.minstay.MinstaySearch;
import com.mybookingpal.dal.shared.minstay.MinstayWeek;

public interface PropertyMinStayMapper {

	@SqlUpdateMarker
//	@UpdateCache
	void create(MinStay action);

	PropertyMinStay read(Long id);

	MinStay read(MinStay action);

	List<PropertyMinStay> readByIds(@Param("ids") List<Long> minStayIds);

	@SqlUpdateMarker
//	@UpdateCache
	void update(MinStay action);

//	@UpdateCache
	void delete(MinStay action);

	MinStay exists(MinStay action);

	List<PropertyMinStay> readByProductId(int productId);

	List<PropertyMinStay> readListByProductsId(@Param("ids") List<Integer> productId);

	MinStay readByExample(MinStay action);

//	@UpdateCache
	void deleteByProductId(String productId);

	List<PropertyMinStay> getAllByProductIdAndSupplierId(PropertyMinStay action);

//	@UpdateCache
	void cancelPropertyMinStayList(@Param("list") GenericList<PropertyMinStay> propertyMinStays, @Param("version") Date version);

	@SqlUpdateMarker
//	@UpdateCache
	void insertList(@Param("list") GenericList<PropertyMinStay> propertyMinStayList);

	List<PropertyMinStay> getAllActiveByProductId(@Param("propertyMinStay") PropertyMinStay propertyMinStay);

//	@UpdateCache
	void copyPropertyMinStays(@Param("fromProductId") Integer fromProductId, @Param("toProductId") Integer toProductId);

//	@UpdateCache
	void updateStateOnFinalPerPm(String partyId);

//	@UpdateCache
	void updateStateOnFinalPerProduct(Integer productId);

	Integer readMaxMinStay(MinStay action);

	Integer readMinStay(MinStay action);

	List<PropertyMinStay> readMinStayByIdAndDate(MinStay action);

	List<PropertyMinStay> readMinStayByProductIdAndDate(MinStay action);

//	@UpdateCache
	void cancelPropertyMinStayById(@Param("propertyMinStayId") Long propertyMinStayId, @Param("version") Date version);

	List<PropertyMinStay> readByExamples(@Param("list") List<PropertyMinStay> list);

	List<WeeklyMinstay> getMinstayByWeeksFeed(DatesIdAction action);

	List<WeeklyMinstay> getMinstayByWeeks(MinstayWeek action);

//	@UpdateCache
	void deleteversion(MinStay action);

	List<PropertyMinStay> readByProductIdAndDateRange(DatesIdAction action);

	List<MinStay> selectbyproductids(MinstaySearch minstaySearch);

	List<PropertyMinStay> readByEntityIdAndEntityTypeOnlyFinal(PropertyMinStay propertyMinStay);

	List<PropertyMinStay> readByEntityIdAndEntityType(PropertyMinStay propertyMinStay);

	List<PropertyMinStay> readByEntityIdAndStateCreatedAndVersion(PropertyMinStay propertyMinStay);

	List<PropertyMinStay> readByEntityIdAndStateFinalAndVersion(PropertyMinStay propertyMinStay);

	List<PropertyMinStay> readByEntityIdAndAllStateAndVersion(PropertyMinStay propertyMinStay);

	List<PropertyMinStay> getProductsForPropertyMinStayByVersion(PropertyMinStay propertyMinStay);

	List<RoomPeriodChange> readPropertyMinStayForPeriodForAllStateBySupplierIdByVersion(@Param("supplierId") String supplierId,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("version") Date version, @Param("channelId") Integer channelId);

	List<PropertyMinStay> readCancelledByProductIdAndDateRange(DatesIdAction action);

	List<PropertyMinStay> getAllActiveByProductIdWithoutDateRange(@Param("productID") String productId);

	List<PropertyMinStay> getByProductId(@Param("productId") Integer productId);

	List<PropertyMinStay> readMinStayByProductIdAndDates(@Param("productId") Integer productId, @Param("list") List<LocalDateRange> dates);
}
