package com.mybookingpal.dal.server.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ProductPolicy;
import com.mybookingpal.dal.shared.ProductPolicyWithName;


/**
 * @author Danijel Blagojevic
 *
 */
public interface ProductPolicyMapper {

	List<ProductPolicy> getByProductId(String productId);
	ArrayList<ProductPolicy> getActiveByProductId(String productId);
	List<ProductPolicy> getByProductIdAndPolicyId(String productId, String policyId);
	List<ProductPolicyWithName> getByProductIdWithName(String productId);
	void insertList(@Param("list") List<ProductPolicy> propertyMinStayList);
	List<ProductPolicy> getProductPoliciesFromLastTimeStamp (@Param("productId") String productId, @Param("version") Date version);
	void cancelList(@Param("list") List<ProductPolicy> propertyMinStays, @Param("version") Date version);
	List<ProductPolicy> getAcitvePetPolicyByProductId(Integer productId);
}
