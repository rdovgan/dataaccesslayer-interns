package com.mybookingpal.dal.server.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.shared.LocalDateRange;
import com.mybookingpal.dal.shared.Price;
import com.mybookingpal.dal.shared.RoomPeriodChange;
import com.mybookingpal.dal.shared.Tax;
import com.mybookingpal.dal.shared.api.HasTableService;
import com.mybookingpal.dal.shared.tax.TaxSearch;
import org.apache.ibatis.annotations.Param;

public interface TaxMapper extends AbstractMapper<Tax> {
	Tax readByProductId(String productId);
	Tax exists(Tax action);
	void cancelversion(Tax action); 
	void canceltaxlist(List<String> taxIdList);
	ArrayList<Tax> taxdetail(Price action);
	ArrayList<Tax> taxbyrelation(Price action);
	Integer count(HasTableService action);
	ArrayList<Tax> list(HasTableService action);
	ArrayList<Tax> readbyproductid(Tax action);
	List<Tax> readbyproductidString(String productId);
	ArrayList<Tax> readAll(int channelPartnerId);
	List<Tax> listbyproductids(TaxSearch action);
	List<Tax> listbyrelationsproductids(TaxSearch action);
	List<Tax> readbyAmountAndName(Tax action);
	Tax getActiveChannelChargedTaxByAmountAndName(Tax action);

	List<RoomPeriodChange> readTaxesForPeriodForAllStateBySupplierIdByVersion(@Param("supplierId") String supplierId,
		@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("version") Date version, @Param("channelId") Integer channelId);

	List<Tax> getAllTaxesByProductAndVersion(Tax action);
	List<Tax> getAllTaxesByRelationForProductAndVersion(Price action);

	List<Tax> readByProductIds(@Param("productIds") List<String> productIds, @Param("state") String state);

	void create(Tax action);
	List<Tax> readAllActive();

	List<Tax> readTaxByProductId(@Param("productId") Integer productId);

	List<Tax> readTaxByProductIdAndDatesAndNameTemplate(@Param("productId") Integer productId, @Param("templates") List<TaxSearch> searchTaxes);
}