package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.ChannelPartner;
import org.apache.ibatis.annotations.Param;

public interface ChannelPartnerMapper {

	ChannelPartner read(Integer id);

	List<ChannelPartner> list();

	void delete(Integer id);

	void create(ChannelPartner action);

	void update(ChannelPartner partner);

	ChannelPartner readBySupplierId(int supplierId);

	ChannelPartner readByPartyId(int partyId);

	List<String> readRelatedManagersByPartyId(int partyId);

	List<Integer> ManagerByChannelList(int channelID);

	// XMLs generation 
	List<ChannelPartner> getListChannelPartnerIdsForXMLs();

	List<Integer> readByAbbreviationChannelIDs(@Param("abbreviation") String abbreviation);

	List<ChannelPartner> readListByAbbreviation(@Param("abbreviation") String abbreviation);

	List<Integer> readChannelIdsByAbbreviation(@Param("abbreviation") String abbreviation);

	ChannelPartner readByAbbreviation(@Param("abbreviation") String abbreviation);

	ChannelPartner readByProductIdForExpedia(@Param("productId") Integer productId);
}
