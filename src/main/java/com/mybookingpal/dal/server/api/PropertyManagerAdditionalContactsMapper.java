package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.PropertyManagerAdditionalContacts;
import org.apache.ibatis.annotations.Param;

public interface PropertyManagerAdditionalContactsMapper {
	List<PropertyManagerAdditionalContacts> getByIdAndType(PropertyManagerAdditionalContacts propertyManagerAdditionalContacts);

	List<PropertyManagerAdditionalContacts> getByPmId(int pmId);

	void insertList(List<PropertyManagerAdditionalContacts> additionalContactsList);

	void deleteList(List<PropertyManagerAdditionalContacts> additionalContactsList);

	void deleteListByPmId(int pmId);

	List<PropertyManagerAdditionalContacts> getByPropertyId(PropertyManagerAdditionalContacts propertyManagerAdditionalContacts);

	List<PropertyManagerAdditionalContacts> getByPropertyIdAndType(PropertyManagerAdditionalContacts propertyManagerAdditionalContacts);

	List<PropertyManagerAdditionalContacts> getByPmIdOrPropertyId(PropertyManagerAdditionalContacts propertyManagerAdditionalContacts);

	List<PropertyManagerAdditionalContacts> getByPmIdOrPropertyIds(@Param("pmId")Integer pmId, @Param("productIds")List<Integer> productIds);

}
