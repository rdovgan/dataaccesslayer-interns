package com.mybookingpal.dal.server.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ChannelBedroomConfiguration;

/**
 * @author Danijel Blagojevic
 *
 */
public interface ChannelBedroomConfigurationMapper extends AbstractMapper<ChannelBedroomConfiguration>{

	void create(ChannelBedroomConfiguration channelBedroomConfiguration);
	void delete(Integer id);
	List<ChannelBedroomConfiguration> getByProductId(Integer productId);
	List<ChannelBedroomConfiguration> getByProductIdAndProductBedroomIds(@Param("productId") Integer productId,
			@Param("bedroomIds") List<Integer> productBedroomIds);
	List<Integer> getByChannelRoomId(Integer channelRoomId) ;
}
