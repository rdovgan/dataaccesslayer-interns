package com.mybookingpal.dal.server.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.MarriottBlackoutDatesMap;
import com.mybookingpal.dal.shared.MarriottDiscountCode;
import com.mybookingpal.dal.shared.MarriottDiscountCodeInfo;
import com.mybookingpal.dal.shared.MarriottDiscountHmcMap;
import com.mybookingpal.dal.shared.MarriottDiscountMap;

public interface MarriottDiscountCodesMapper {

	MarriottDiscountCode readDiscountCode(@Param("id") Integer id);

	MarriottDiscountCode readDiscountCodeByName(@Param("discountCode") String discountCode);

	void createDiscountCode(@Param("discountCode") MarriottDiscountCode discountCode);

	void insertDiscountList(@Param("discountCodeId") Integer discountCodeId, @Param("discounts") List<MarriottDiscountMap> discounts);

	void insertHmcList(@Param("discountCodeId") Integer discountCodeId, @Param("hmcs") List<Integer> hmcs);

	void insertBlackOutDatesList(@Param("discountCodeId") Integer discountCodeId, @Param("blackoutDates") List<MarriottBlackoutDatesMap> blackoutDates);

	List<MarriottDiscountMap> readDiscountList(@Param("discountCodeId") Integer discountCodeId);

	List<MarriottDiscountHmcMap> readHmcList(@Param("discountCodeId") Integer discountCodeId);

	List<MarriottBlackoutDatesMap> readBlackOutDatesList(@Param("discountCodeId") Integer discountCodeId);

	void moveToFinal(@Param("id") Integer id);
	
	void updateChannelDiscountId(@Param("discountCode") MarriottDiscountCode discountCode);

	List<MarriottDiscountCodeInfo> getDiscountCodesInfo();
}
