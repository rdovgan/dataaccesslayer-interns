package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelListingInformation;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ChannelListingInformationMapper {

    void insertList(@Param("list") List<ChannelListingInformation> channelListingsInformation);
    List<ChannelListingInformation> getChannelListingInfoBySupplierAndChannel(@Param("supplierId") Integer supplierId, @Param("channelId") Integer channelId);
    ChannelListingInformation getChannelListingInfoBySupplierAndChannelProduct(@Param("supplierId")Integer supplierId, @Param("channelProductId")Integer channelProductId);
    void update(ChannelListingInformation channelListingInformation);
    void delete(@Param("supplierId") Integer supplierId);
}
