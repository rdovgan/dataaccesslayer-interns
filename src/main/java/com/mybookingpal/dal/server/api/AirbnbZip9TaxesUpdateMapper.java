/**
 * 
 */
package com.mybookingpal.dal.server.api;

/**
 * @author Filip Pankovic
 *
 */
public interface AirbnbZip9TaxesUpdateMapper {
	void create(String zip9UpdateList);
}
