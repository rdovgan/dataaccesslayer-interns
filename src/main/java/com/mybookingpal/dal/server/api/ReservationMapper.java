package com.mybookingpal.dal.server.api;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.mybookingpal.dal.dao.entities.ReservationModel;
import com.mybookingpal.dal.rest.Download;
import com.mybookingpal.dal.rest.ScheduleItem;
import com.mybookingpal.dal.server.job.dto.PartnerPaymentArchiveDto;
import com.mybookingpal.dal.shared.CancelledReservation;
import com.mybookingpal.dal.shared.IdVersion;
import com.mybookingpal.dal.shared.searchReservation.SearchReservationModelRequest;
import com.mybookingpal.dal.shared.searchReservation.SearchReservationModelResponse;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.dal.shared.Yield;
import com.mybookingpal.dal.shared.action.DatesIdAction;
import com.mybookingpal.dal.shared.api.HasProductIdAndDateRange;
import com.mybookingpal.dal.shared.beststayz.BeststayzReservationByChannel;
import com.mybookingpal.dal.shared.beststayz.BeststayzReservationFilterDto;
import com.mybookingpal.dal.shared.beststayz.BeststayzReservationReport;
import com.mybookingpal.dal.shared.beststayz.BeststayzTransactionReportModel;
import com.mybookingpal.dal.shared.creditcard.CreditCardInformation;
import com.mybookingpal.dal.shared.marriott.GuestDecryptedInfoDto;
import com.mybookingpal.dal.shared.marriott.ChannelPortalReservationReport;
import com.mybookingpal.dal.shared.marriott.ChannelPortalAnalyticInfo;
import com.mybookingpal.dal.shared.marriott.ChannelPortalAnalyticRequest;
import com.mybookingpal.dal.shared.marriott.ChannelPortalFilterDto;
import com.mybookingpal.dal.shared.marriott.ChannelPortalGuestFolioInfo;
import com.mybookingpal.dal.shared.marriott.MarriottMedalliaPostStaySurveyInfo;
import com.mybookingpal.dal.shared.marriott.ChannelPortalPayoutReport;
import com.mybookingpal.dal.shared.marriott.ChannelPortalPointsRedemptionReport;
import com.mybookingpal.dal.shared.marriott.ChannelPortalReconciliationReport;
import com.mybookingpal.dal.shared.marriott.ChannelPortalTransactionReport;
import com.mybookingpal.dal.shared.marriott.MedalliaGuestReservationInfo;
import com.mybookingpal.dal.shared.marriott.MedalliaSurveyInfo;
import com.mybookingpal.dal.shared.marriott.PaymentAndStayInfo;
import com.mybookingpal.dal.shared.marriott.PostStayEmailInfoDto;
import com.mybookingpal.dal.shared.marriott.ReservationDatesAvailability;
import com.mybookingpal.dal.soap.ota.server.OtaRoomStay;
import com.mybookingpal.rest.entity.ProductDate;
import com.mybookingpal.shared.dto.BeststayzTransactionReportDto;
import com.mybookingpal.shared.dto.ReservationFilterDto;
import com.mybookingpal.shared.dto.ReservationReport;
import com.mybookingpal.shared.dto.ReservationReportGropedByParam;
import com.mybookingpal.shared.dto.ReservationReportRequestDto;
import com.mybookingpal.shared.dto.ReservationYearsInfo;
import com.mybookingpal.shared.dto.StripeReconcilationReportDto;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.json.Parameter;
import com.mybookingpal.dal.rest.response.CalendarElement;
import com.mybookingpal.dal.shared.RoomPeriodChange;

public interface ReservationMapper {
//TODO need to check fields name in queries

	ReservationModel read(Integer id);

	ReservationModel offlineread(Integer reservationid);

	List<ReservationModel> exists(ReservationModel action);

//	@UpdateCache
	void cancelversionbydate(ReservationModel action);

//	@UpdateCache
	void cancelreservationlist(@Param("list") List<String> reservationIdList, @Param("version") Date version);

	Integer minAvailable(ReservationModel action); // Minimum units available during reservation period

	Integer occupancy(Yield yield); //Occupancy

	Integer next(Yield yield); //Next

	Integer previous(Yield yield); //Previous

	Integer readreservationcount(Integer organizationID);

	List<String> donestates(Integer reservationid);

	List<com.mybookingpal.utils.entity.NameId> parentcollisions(ReservationModel reservation);

	List<NameId> childcollisions(ReservationModel reservation);

	List<OtaRoomStay> hotelavailability(ReservationModel reservation); //HotelAvailRS

	List<OtaRoomStay> hotelavailabilities(ReservationModel reservation); //HotelAvailRS isSummary

	//REST SQL queries
	Boolean available(ReservationModel action);

	List<ScheduleItem> locationschedule(ReservationModel action);

	List<ScheduleItem> productschedule(ReservationModel action);

	ReservationModel readbyforeignid(String id); //RESTReservationByForeignid

	ReservationModel readbyname(ReservationModel action); //RESTReservationidExists

	ReservationModel readbyorganization(ReservationModel action);

	List<Download> download(String id); //RESTDownload

	List<Download> downloads(Integer id); //RESTDownload

	List<Download> uploads(Integer id); //RESTUploads

	List<com.mybookingpal.dal.rest.flipkey.History> flipkeyhistory(); //FlipKeyHistory

	List<com.mybookingpal.dal.rest.flipkey.Updated> flipkeyproperties(); //FlipKeyProperties

	com.mybookingpal.dal.rest.flipkey.Location flipkeylocation(String locationid); //FlipKeyRead

	List<com.mybookingpal.dal.rest.flipkey.ScheduleItem> flipkeyavailability(); //FlipKeyAvailability

	List<com.mybookingpal.dal.rest.flipkey.ScheduleItem> flipkeyavailabilityproduct(String productid); //FlipKeyAvailabilityProduct

	List<com.mybookingpal.dal.rest.flipkey.ScheduleItem> flipkeyreservation(com.mybookingpal.dal.rest.flipkey.ScheduleItem action); //FlipKeyReservation

	List<com.mybookingpal.dal.json.calendar.CalendarWidgetItem> calendarwidget(com.mybookingpal.dal.json.Parameter action);

	List<com.mybookingpal.dal.json.reservation.ReservationWidgetItem> listbyparentid(String parentid);

	com.mybookingpal.dal.json.reservation.ReservationWidgetItem readwidget(com.mybookingpal.dal.json.Parameter action);

//	@UpdateCache
	void remove(ReservationModel action);

	//Foreign SQL queries
	ReservationModel altread(com.mybookingpal.utils.entity.NameId action);

	List<ReservationModel> altreadforchannel(ReservationModel action);

	List<ReservationModel> closedDatesForProperty(ReservationModel action);

	List<ReservationModel> findMultipleBookingsForPropertyByDates(ReservationModel action);

	List<java.util.Date> reservedDateForProperty(Integer productid); //to fetch all bookedDate of the property

	List<ReservationModel> productActiveReservations(Integer productId);

	/**
	 * Get reservations from API which are in state Exception (previously Closed state, this are reservation where CustomerID and AgentID are empty)
	 */
	List<ReservationModel> productExceptionApiReservations(Integer productId);

	List<ReservationModel> getReservationsByListAltId(@Param("productId") Integer productId, @Param("altIds") List<String> altId);

	List<ReservationModel> productApiReserveredDates(ReservationModel action);

//	@UpdateCache
	void deleteDate(com.mybookingpal.utils.entity.NameId action); ///to remove cancelled bookingDate from DB

	List<ReservationModel> readActiveBasedOnTime(ReservationModel tmp);

	List<ReservationModel> readCancelledOverlappingDatesBasedOnTimeBySupplier(ReservationModel tmp);

	//@SqlUpdateMarker
//	@UpdateCache
	void create(ReservationModel action);

	//@SqlUpdateMarker
	//@UpdateCache
	void insertList(@Param("list") List<ReservationModel> reservations);

	//@SqlUpdateMarker
//	@UpdateCache
	void update(ReservationModel action);

//	@UpdateCache
	void updateVersionById(@Param("id") Long id);

	List<ReservationModel> getByCPAndVersion(ReservationModel action);

	List<ReservationModel> readClosed(int limit);

//	@UpdateCache
	void deleteByIdList(List<ReservationModel> reservations);

	List<ReservationModel> maxVersionForEachProductInList(List<String> productIdList);

	List<Integer> getReservationIds();

	List<ReservationModel> readByExample(ReservationModel reservation);

	List<ReservationModel> getRealReservations(Integer productId);

	List<ReservationModel> getRealReservationsByPm(Integer altPartyId);

	Integer countRealReservationsByPm(Integer altPartyId);

	List<ReservationModel> getRealReservationsWithPages(@Param("altPartyId") Integer altPartyId, @Param("startPagination") Integer startPagination, @Param("limit") Integer limit);

	Integer countSearchRealReservations(SearchReservationModelRequest searchReservationModelRequest);

	List<SearchReservationModelResponse> searchRealReservations(SearchReservationModelRequest searchReservationModelRequest);

//	@UpdateCache
	void updateStateOnFinalPerPm(String partyId);

	List<ReservationModel> getAllBookedOrBlockedReservationsByDate(@Param("productId") Integer productId, @Param("fromDate") LocalDate fromDate,
			@Param("toDate") LocalDate toDate);

	List<CancelledReservation> getAllReservationByDateAndOrganizationID(@Param("channelPartnerId") Integer channelPartnerId, @Param("fromDate") Date fromDate,
			@Param("toDate") Date toDate);

	List<CancelledReservation> getAllCancelledReservationByDateAndOrganizationID(@Param("channelPartnerId") Integer channelPartnerId,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	List<ReservationModel> getBlockedReservationByDate(@Param("productId") Integer productId, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

//	@UpdateCache
	void cancelReservations(@Param("reservations") List<ReservationModel> reservations);

	List<ReservationModel> readByGroupReservationId(Long groupedReservationId);

	ReservationModel readFirstFromGroup(@Param("id") String id);

	List<ReservationModel> readAllReservations(ReservationModel tmp);

	List<ReservationModel> getReservationsByAltId(@Param("productId") Integer productId, @Param("altId") String altId);

	List<ReservationModel> readReservations(@Param("reservations") List<Long> reservations);

	List<ReservationModel> getReservationForGoogleImportJob();

	List<ReservationModel> readReservationByReconciliationStatus(@Param("status") Integer status, @Param("name") String name,
			@Param("chSupressInvoiceMail") Boolean chSupressInvoiceMail);

	List<CreditCardInformation> getCreditCardInfo(@Param("reservationIds") List<Integer> reservationIds);

	ReservationModel readReservationByDatesAndChannelProductId(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("productId") String productId,
			@Param("confirmationId") String confirmationId);

//	@UpdateCache
	void cancelReservationById(@Param("reservationId") Integer reservationId);

	List<ReservationModel> readReservationsByChannelConfirmationId(String confirmationId);

	List<ReservationModel> readReservationsByChannelConfirmationIdAndChannelId(@Param("confirmationId") String confirmationId,
			@Param("channelId") Integer channelId);

	List<ProductDate> readProductDatesByProductIdsDatesAndState(@Param("productIds") List<Integer> productIds, @Param("fromDate") LocalDate fromDate,
			@Param("toDate") LocalDate toDate, @Param("state") String state);

	List<ReservationModel> findAllReservationsForYear2018ByChannelWhichAreNotInCRP(@Param("agentId") Integer agentId, @Param("channelId") Integer channelId);

	List<ReservationModel> findAllReservationsForYear2018ByChannel(@Param("agentId") Integer agentId);

	List<PartnerPaymentArchiveDto> reservationToArchivePartnerPaymentMethod();

	List<ReservationModel> readAllBeststayzReservationByExcludeState(@Param("list") List<String> list);

	List<BeststayzReservationByChannel> readAllBeststayzReservationByChannel();

	List<BeststayzReservationReport> readAllBeststayzReservation(@Param("filter") BeststayzReservationFilterDto reservationFilter);

	List<ReservationModel> getAllUnconfirmedReservationsForProduct(@Param("productId") Integer productId);

	List<ReservationModel> getByPmAndVersionInterval(@Param("organizationID") String pmId, @Param("abbreviation") String abbreviation, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	//JSON SQL queries
	ReservationModel offlineReservationRead(Integer reservationid);

	List<CalendarElement> calendarelement(Parameter action);

	List<ReservationModel> cancelledDatesForPropertyid(ReservationModel action);

	List<ReservationModel> readCancelledBasedOnTimeBySupplier(ReservationModel tmp);

	List<ReservationModel> readInquiryOlderThenTime(ReservationModel tmp);

	List<ReservationModel> readActiveBasedOnTimeBySupplier(ReservationModel tmp);

	List<ReservationModel> reserveredDatesForPropertyid(ReservationModel action);

	List<ReservationModel> readClosedReservationsForCancelledDates(ReservationModel tmp);

	ReservationModel readMaxMinClosedReservationsForCancelledDates(ReservationModel tmp);

	List<ReservationModel> readReservationsForADateRange(ReservationModel tmp);

	List<ReservationModel> getSchedule(Integer productId);

	List<ReservationModel> reservedDatesForMultipleProperties(@Param("listId") List<Integer> productIdList, @Param("fromdate") Date fromDate,
			@Param("todate") Date toDate);

	List<RoomPeriodChange> readReservationForPeriodForAllStateBySupplierIdByVersion(@Param("supplierId") String supplierId, @Param("fromDate") Date fromDate,
			@Param("toDate") Date toDate, @Param("version") Date version, @Param("channelId") Integer channelId);

	List<ReservationModel> readBasedOnTime(String lastFetch);

	List<ReservationModel> listReservationsByPropertyidAndVersion(ReservationModel tmp);

	List<ReservationModel> readReservationByAgentAndAltId(ReservationModel action);

	// added for airbnb reservation retrieval job
	List<ReservationModel> readActiveBasedOnTimeBySupplierAndChannel(ReservationModel tmp);

	// added for airbnb reservation retrieval job
	List<ReservationModel> readActiveBasedOnTimeBySupplierAndChannelWithoutProductId(ReservationModel tmp);

	List<ReservationModel> findAllActiveKeysOfMLTByReservationVersion(@Param("parentId") String parentId, @Param("version") Date version);

	ReservationModel collides(ReservationModel action);

	List<IdVersion> getMaxAvailabilityVersions(@Param("productIds") List<Integer> productIds, @Param("channelIds") Set<Integer> channelIds);

	List<IdVersion> getMaxAvailabilityVersionsWithoutMlts(@Param("productIds") List<Integer> productIds, @Param("channelIds") Set<Integer> channelIds);

	List<DatesIdAction> collisionItems(HasProductIdAndDateRange hasProductIdAndDateRange);

	List<ReservationModel> collisionItemsForDateRange(HasProductIdAndDateRange hasProductIdAndDateRange);

	List<com.mybookingpal.utils.entity.NameId> collisions(HasProductIdAndDateRange reservationModel); //Collide

	Integer getAvailableRoomsCountForMltKeyRoomsByDay(@Param("productList") List<Integer> subProducts, @Param("day") LocalDate fromDate);

	List<ReservationReportRequestDto> getReportDataByDateAndSearchTypeAndProductId(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate,
			@Param("productID") Integer productId, @Param("pmId") Integer pmId, @Param("searchType") String searchType);

	List<ReservationReportGropedByParam> getReportDataByDateAndSearchTypeAndProductIdGroupByDateAndChannel(@Param("fromDate") Date fromDate,
			@Param("toDate") Date toDate, @Param("productID") Integer productId, @Param("pmId") Integer pmId, @Param("searchType") String searchType,
			@Param("range") String range);

	List<ReservationReportGropedByParam> getReportDataByDateAndSearchTypeAndProductIdGroupByDate(@Param("fromDate") Date fromDate,
			@Param("toDate") Date toDate, @Param("productID") Integer productId, @Param("pmId") Integer pmId, @Param("searchType") String searchType,
			@Param("range") String range);

	List<ReservationReport> readAllReservationByFilter(@Param("filter") ReservationFilterDto reservationFilter);

	List<ReservationYearsInfo> getReservationYears(@Param("pmId") Integer pmId);

	List<BeststayzTransactionReportModel> getTransactionReport(BeststayzTransactionReportDto beststayzTransactionReport);

	List<BeststayzTransactionReportModel> getTransactionReportForExpedia(BeststayzTransactionReportDto beststayzTransactionReport);

	ReservationModel getChannelPortalReservation(@Param("reservationId") Integer reservationId, @Param("channelId") Integer channelId);

	List<ChannelPortalReservationReport> getChannelPortalReservations(@Param("filter") ChannelPortalFilterDto filter);

	List<ReservationDatesAvailability> getReservationsByProductIdAndDates(@Param("productId") Integer productId, @Param("fromDate") Date fromDate, @Param("toDate")
			Date toDate);

	List<ChannelPortalTransactionReport> getChannelPortalTransactionReport(@Param("filter") ChannelPortalFilterDto filter);

	Integer getCountOfChannelPortalTransactionForReport(@Param("filter") ChannelPortalFilterDto filter);

	List<Integer> getChannelPortalReservationForReconciliationReport(@Param("channelId") Integer channelId);

	List<ChannelPortalReconciliationReport> getChannelPortalReconciliationReport(@Param("reservationIds") List<Integer> reservationIds,
			@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate, @Param("page") Integer page, @Param("limit") Integer limit);

	Integer getCountOfChannelPortalReconciliationForReport(@Param("reservationIds") List<Integer> reservationIds, @Param("startDate") LocalDate startDate,
			@Param("endDate") LocalDate endDate);

	List<ChannelPortalPayoutReport> getChannelPortalPayoutReport(@Param("filter") ChannelPortalFilterDto filter);

	List<ChannelPortalPointsRedemptionReport> getChannelPortalPointsRedemptionReport(@Param("filter") ChannelPortalFilterDto filter);

	Integer getCountOfChannelPortalPointsRedemptionReport(@Param("filter") ChannelPortalFilterDto filter);

	Integer getCountOfChannelPortalPayoutReport(@Param("filter") ChannelPortalFilterDto filter);

	GuestDecryptedInfoDto getPointsRedemptionInfoByReservationId(Integer reservationId);

	List<ReservationModel> readAllReservationsWithException(@Param("filter") ReservationFilterDto filter);

	PaymentAndStayInfo readAllPaymentAndStayInfo(@Param("reservationId") Integer reservationId, @Param("productId") Integer productId);

	List<Long> checkAndGetReservationIdsByChannel(@Param("reservationIds") List<Long> reservationIds, @Param("channelId") Integer channelId);

	List<PostStayEmailInfoDto> getInfoForPostStayEmails();

	MedalliaGuestReservationInfo getGuestReservationDataForMedallia(@Param("reservationId") Integer reservationId);

	List<StripeReconcilationReportDto> getAllReservationsPaidViaStripeByDateAndPmId(@Param("filter") ReservationFilterDto filter);

	List<ChannelPortalAnalyticInfo> getChannelPortalAnalyticReportInfo(@Param("request") ChannelPortalAnalyticRequest request);

	List<MedalliaSurveyInfo> getMedalliaSurveyLinksInfo(@Param("ids") List<Integer> ids);

	ChannelPortalGuestFolioInfo getChannelPortalGuestFolioInfo(@Param("reservationId") Integer reservationId);

	List<MarriottMedalliaPostStaySurveyInfo> getReservationsInfoForMedalliaPostStaySurvey();

	List<ReservationModel> getChannelReservations(@Param("agentId") Integer agentId);

	void cancelClosedReservations(@Param("productId") Integer productId);
}
