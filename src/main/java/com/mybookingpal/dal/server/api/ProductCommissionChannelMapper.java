package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.IdVersion;
import com.mybookingpal.dal.shared.ProductCommissionChannel;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

public interface ProductCommissionChannelMapper {
	
	ProductCommissionChannel readActiveByProductAndChannelPartner(ProductCommissionChannel tempProductCommissionChannel);

    List<IdVersion> getMaxVersionForHACommissionPerProduct(@Param("channelId")Integer channelId, @Param("channelPartnerId") Integer channelPartnerId,
                                                           @Param("productIds") List<Integer> productIds);
}
