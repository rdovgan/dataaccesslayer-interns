package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.messaging.MailThread;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.messaging.MailMessage;

import java.util.List;

public interface MailMessageMapper {

	void create(MailMessage action);
	void update(MailMessage action);
	List<MailMessage> readByThreadId(@Param("threadId") String threadId);
	MailMessage readByThreadIdAndMaxVersion(@Param("threadId") String threadId);
	MailMessage readByMessageId(@Param("messageId") String messageId);
	MailMessage readByChannelMessageId(@Param("channelMessageId") Long channelMessageId);
	List<MailMessage> readByExample(MailMessage example);
	List<MailMessage> readNotViewedMessages();
	List<MailMessage> readByThreadIdAndState(@Param("threadId") String threadId, @Param("state") Integer state);
	MailMessage readLastMessageFromGuestInThread(@Param("threadId") String threadId, @Param("guestId") Integer guestId);
	MailMessage readReservationConfirmedMessage(@Param("threadId") String threadId);
}
