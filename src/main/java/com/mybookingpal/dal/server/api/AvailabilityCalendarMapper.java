package com.mybookingpal.dal.server.api;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.mybookingpal.aop.annotations.ReadCache;
import com.mybookingpal.aop.annotations.UpdateCache;
import com.mybookingpal.dal.dao.entities.LowInventoryRoom;
import com.mybookingpal.dal.shared.AvailabilityCalendarSimple;
import com.mybookingpal.dal.shared.availability.AvailabilityRoomDto;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.AvailabilityCalendarModel;

public interface AvailabilityCalendarMapper {

	AvailabilityCalendarModel read(Long id);

//	@UpdateCache
	void create(AvailabilityCalendarModel availabilityCalendarModel);

//	@UpdateCache
	void update(AvailabilityCalendarModel availabilityCalendarModel);

//	@UpdateCache
	void delete(@Param("listId") List<Long> idList);

//	@ReadCache
	List<AvailabilityCalendarModel> readForPeriodByProductIds(@Param("listId") List<Integer> productIdList,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

//	@ReadCache
	List<AvailabilityCalendarModel> readForPeriodByProductIds(@Param("listId") List<Integer> productIdList, @Param("fromDate") LocalDate fromDate,
			@Param("toDate") LocalDate toDate);

//	@ReadCache
	List<AvailabilityCalendarModel> readForPeriodForAllStateByProductIds(@Param("listId") List<Integer> productIdList,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

//	@ReadCache
	List<AvailabilityCalendarModel> readForPeriodByProductIdsByVersion(@Param("listId") List<Integer> productIdList,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate,@Param("version") Date version);

//	@ReadCache
	List<AvailabilityCalendarModel> readForPeriodForAllStateByProductIdsByVersion(@Param("listId") List<Integer> productIdList,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate,@Param("version") Date version);

	List<AvailabilityCalendarSimple> readForPeriodForAllStateBySupplierIdByVersion(@Param("supplierId") String supplierId,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("version") Date version, @Param("channelId") Integer channelId);

//	@UpdateCache
	void insertUpdateFromList(@Param("list") List<AvailabilityCalendarModel> models);

//	@UpdateCache
	void cancelAvailabilityCalendarlist(@Param("list") List<AvailabilityCalendarModel> models);

//	@UpdateCache
	void deleteAvailabilityCalendarlist(@Param("list") List<AvailabilityCalendarModel> models);

//	@UpdateCache
	void insertUpdateFromTemplate(@Param("templateModel") AvailabilityCalendarModel templateModel);

	List<AvailabilityRoomDto> getCalendarByPropertyManagerForReport(Long pmId);

	List<AvailabilityRoomDto> readAvailabilityCalendarByRoomConfiguration(@Param("rooms") List<LowInventoryRoom> rooms,
			@Param("configurationId") Long configurationId, @Param("pmId") Integer pmId, @Param("noUnitCount") Integer noUnitCount);

//	@UpdateCache
	void insertAvailabilityCalendarModels(
			@Param("availabilityCalendarModelList") List<AvailabilityCalendarModel> availabilityCalendarModelList);

//	@ReadCache
	AvailabilityCalendarModel readByProductIdAndDate(AvailabilityCalendarModel availabilityCalendarModel);

//	@UpdateCache
	void updateVersionInAvilabilityCalenderForProductAndDate(AvailabilityCalendarModel availabilityCalendarModel);
	
	void saveAvailabilityCalendarHistory(@Param("list") List<AvailabilityCalendarModel> models);

	Integer getAvailableCountForProduct(@Param("productId") String productId);

    List<AvailabilityCalendarModel> readCreatedByProductIds(Integer pmId);

	AvailabilityCalendarModel getLowestActiveBasePriceForProduct(@Param("productId") String productId, @Param("dateForCheck") Date dateForCheck);
	//BP-28512 Change storing process of availability_calendar
	/*List<AvailabilityCalendarModel> readByAvailabilityCalendarIds(@Param("list") List<Long> ids);*/
}
