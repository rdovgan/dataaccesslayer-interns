package com.mybookingpal.dal.server.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.cancellation.ChannelCancellationRule;

public interface ChannelCancellationRuleMapper {
	void create(ChannelCancellationRule cancellationRule);

	ChannelCancellationRule read(Integer id);

	void update(ChannelCancellationRule cancellationRule);

	void delete(Integer id);

	List<ChannelCancellationRule> readByChannelId(Integer channelId);

	List<ChannelCancellationRule> readByPmId(Integer pmId);

	void deleteByChannelId(Integer channelId);

	List<ChannelCancellationRule> readCancellationsByPmIdAndChannelPartner(@Param("pmId") Integer pmId,
			@Param("channelPartnerIds") List<Integer> channelPartnerIds);

	void deleteAllByIds(List<Integer> listIds);

	List<ChannelCancellationRule> readCancellationsByProductIdAndChannelPartner(@Param("productId")Integer productId, @Param("channelPartnerIds") List<Integer> channelPartnerIds);

	List<ChannelCancellationRule> readByProductId(Integer productId);
	
	List<ChannelCancellationRule> readCancellationsByPmIdOrProductIdAndChannelId(ChannelCancellationRule cancellationRule);
	
	List<String> getPropertyIdsByPmIdAndChannelId(@Param("pmId")Integer pmId, @Param("list") List<Integer> channelIds);
	
}