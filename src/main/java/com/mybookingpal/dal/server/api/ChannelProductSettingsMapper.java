package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelProductSetting;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelProductSettingsMapper {

	ChannelProductSetting readByProductIdAndChannelIdAndType(ChannelProductSetting channelProductSetting);
	List<ChannelProductSetting> read(ChannelProductSetting example);
	void delete(ChannelProductSetting channelProductSetting);
	void update(ChannelProductSetting channelProductSetting);
	void create(ChannelProductSetting action);
	void insertList(@Param("list") List<ChannelProductSetting> channelProductSettings );
}
