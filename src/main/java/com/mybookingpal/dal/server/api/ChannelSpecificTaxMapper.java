package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelSpecificTax;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ChannelSpecificTaxMapper {

    void insertNewTax(ChannelSpecificTax channelSpecificTax);
    List<ChannelSpecificTax> getByProductId(@Param("productId") Integer productId);
    List<ChannelSpecificTax> getByProductIdAndVersion(@Param("productId") Integer productId, @Param("version") Date version);
}
