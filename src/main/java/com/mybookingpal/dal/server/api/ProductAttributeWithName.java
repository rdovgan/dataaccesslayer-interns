package com.mybookingpal.dal.server.api;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.shared.ProductAttribute;

@Component
@Scope(value = "prototype")
public class ProductAttributeWithName extends ProductAttribute {

	private String displayName;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	public String toString() {
		final StringBuilder sb = new StringBuilder("ProductAttributeWithName{");
		sb.append("displayName='").append(displayName).append('\'');
		sb.append(super.toString());
		sb.append('}');
		return sb.toString();
	}
}
