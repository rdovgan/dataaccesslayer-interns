package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelOpportunity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelOpportunityMapper {
	ChannelOpportunity readById(Integer id);

	List<ChannelOpportunity> readByExample(ChannelOpportunity example);

	void update(ChannelOpportunity channelOpportunity);

	void create(ChannelOpportunity action);

	List<ChannelOpportunity> readByChannelId(@Param("channelId") Integer channelId);

	List<String> readAltIds();

	List<ChannelOpportunity> readByIdList(List<Integer> idList);

	List<ChannelOpportunity> readDistinctOpportunities();
}
