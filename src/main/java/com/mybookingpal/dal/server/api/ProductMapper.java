package com.mybookingpal.dal.server.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.shared.IdVersion;
import com.mybookingpal.dal.shared.ProductChannel;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.NameIdAction;
import com.mybookingpal.dal.shared.Product;
import com.mybookingpal.dal.shared.ProductAction;

public interface ProductMapper {

	Product read(String id);

	Product readById(Integer productId);

	Product getProductAvoidNPE(Integer id);

	/**
	 * @deprecated use {@link #readProductCollectionByIdCollection(List)} instead of this
	 */
	@Deprecated
	ArrayList<Product> productlistbyids(List<String> productids);

	List<Product> readProductCollectionByIdCollection(List<Integer> productIdCollection);

	ArrayList<String> inactiveProductIdListByOwnerForChannelByVersion(ProductAction supplierId);

	ArrayList<String> inactiveProductIdListByPartyForChannelByVersion(ProductAction supplierId);

	ArrayList<String> inactiveProductIdListByPartyForChannel(NameIdAction supplierId);

	List<Product> readAllSupplier(NameIdAction nameIdAction);

	List<Product> readAllActiveProperties(NameIdAction nameIdAction);

	List<Product> readAllActiveOrSuspendedProperties(NameIdAction nameIdAction);

	ArrayList<String> activeProductIdListBySupplier(NameIdAction supplierId);

	ArrayList<String> activeSingleUnitProductIdListBySupplier(NameIdAction supplierId);

	ArrayList<String> activeMultiUnitSubProductIdListBySupplier(NameIdAction supplierId);

	ArrayList<String> activeMultiUnitKeyRootProductIdListBySupplier(NameIdAction supplierId);

	ArrayList<String> allMultiUnitKeyRootProductIdListBySupplier(NameIdAction supplierId);

	ArrayList<String> activeMultiUnitRepRootProductIdListBySupplier(NameIdAction supplierId);

	List<Product> activeMultiUnitRepRootProductListBySupplier(String supplierId);

	List<Product> activeMultiUnitProductsByRootId(String hotelId);

	ArrayList<String> activeMultiUnitProductIdListByParentId(NameIdAction supplierId);

	ArrayList<String> activeProductIdListBySupplierForChannel(ProductAction supplier);
	
	List<String> activeProductIdListBySupplierForChannelWithoutOWN(ProductAction supplier);
	
	ArrayList<String> activeSGLProductIdListBySupplierForChannel(ProductAction supplier);

	ArrayList<String> allProductIdListBySupplierForChannel(ProductAction supplier);

	ArrayList<String> activeProductIdListByOwnerForChannel(ProductAction supplierId);

	ArrayList<String> inactiveProductIdListBySupplierForChannel(NameIdAction supplierId);

	ArrayList<String> inactiveProductIdListByOwnerForChannel(NameIdAction supplierId);

	ArrayList<String> activeProductIdListByPartyForChannel(ProductAction supplierId);

	// ArrayList<String> activeProductIdListBySupplierByCountryCode(ProductAction supplierId);
	ArrayList<String> activeAndInactiveProductIdListBySupplier(NameIdAction supplierId);

	// //SN:This will return properties with Prefix "LL_".This is the criteria specific to Apartments Apart
	ArrayList<String> activeProductIdListBySupplierForApartmentsApart(NameIdAction supplierId);

	ArrayList<String> activeProductIdListByOwnerForChannelByVersion(ProductAction supplierId);

	ArrayList<String> activeProductIdListByPartyForChannelByVersion(ProductAction supplierId);

	ArrayList<String> allMultiUnitKeyProductIdListByRootId(NameIdAction supplierId);

	// // author: cshah
	// // returns final or suspended properties
	ArrayList<String> inactiveOrUndistributedProductIdListBySupplier(NameIdAction supplierId);
	
	List<String> inactiveOrUndistributedProductIdListBySupplierWithoutOWN(NameIdAction supplierId);

	ArrayList<String> inactiveProductIdListBySupplierByVersion(ProductAction supplierId);
	
	ArrayList<String> inactiveSGLProductIdListBySupplierByVersion(ProductAction supplierId);

	ArrayList<String> activeProductIdListBySupplierByVersion(ProductAction supplierId);
	
	ArrayList<String> activeSGLProductIdListBySupplierByVersion(ProductAction supplierId);

	List<Product> readByExample(Product product);
	// Product readByAltId(String altId);
	List<Product> fetchAllMasterForASupplier(String supplierId);

	List<Product> getProductsByParentId(String masterId);

	List<String> getProductIdsByParentIds(@Param("listId") List<String> productIdList);

	List<Product> getProductsByParentIds(@Param("listId") List<String> productIdList);

	List<Product> fetchAllParentForASupplier(String supplierId);

	List<Product> findAllFinalizedMLTsForASupplierByVersion(ProductAction supplier);

	List<Product> fetchAllFinalizedParentForASupplierByVersion(ProductAction supplier);

	List<Product> findAllReactivatedParentsForASupplierByVersion(ProductAction supplier);

	List<Product> findAllFinalizedKeysForMLTByVersion(ProductAction productAction);

	List<Product> fetchAllCreatedChildByParentIdByVersion(ProductAction productAction);

	List<Product> fetchAllCreatedMasterBySupplierIdByVersion(ProductAction productAction);

	List<Product> getProductByName(@Param("hotelName") String hotelName);

	List<String> getAllProductsByPartyForChannelFromChannelParty(NameIdAction supplierId);

	List<String> getAllProductsBySupplierForChannel(NameIdAction supplierId);

	Product getProductFromLastTimeStamp(Product product);

	List<String> getOWNPropertiesBySupplierId(String supplierId);

	void update(Product product);

	void updatePhone(Product product);

	List<String> fetchActiveMultiUnitKeyProductListByIds(@Param("list") List<String> products);

	List<String> fetchActiveMultiUnitRepProductListByIds(@Param("list") List<String> products);

	List<Product> getFinilizedMlts(ProductAction productAction);
	
	List<Product> getFinilizedMltsByMLTList(ProductAction productAction);

	List<Product> fetchAllCreatedMasterForOwnListByVersion(ProductAction productAction);
	
	List<Product> fetchAllCreatedMasterByVersion(ProductAction productAction);

	List<String> findKeyProductsForMlt(String parentId);

	List<Product> inactiveProductListBySupplier(NameIdAction supplierId);
	
	List<String> activeMuliUnitProductIdListBySupplier(NameIdAction supplierId);
	
	ArrayList<String> allSinglePropertiesIdListBySupplier(NameIdAction supplierId);

	List<Product> rootProducts(String supplierId);

	List<Product> allRootProducts(@Param("supplierId") String supplierId, @Param("version") Date version);

	List<Product> allRootProductsFromChannelProductMap(@Param("supplierId") String supplierId, @Param("channelId") Integer channelId, @Param("version") Date version);
	
	List<String> allOWNProductsFromChannelProductMap(@Param("supplierId") String supplierId, @Param("channelId") Integer channelId);
	
	List<String> allMLTProductsFromChannelProductMap(@Param("supplierId") String supplierId, @Param("channelId") Integer channelId);

	List<Product> distributedRootProducts(@Param("supplierId") String supplierId, @Param("channelId") Integer channelId);

	List<Product> readRootProducts(@Param("list") List<String> productIds);

	List<Product> readAllActivePropertiesByPropertyIds(List<String> productIds);
	
	List<Product> readAllPropertiesByPropertyIds(List<String> productIds);
	
	List<Product> readAllActivePropertiesByPmIds(List<String> pmIds);
	
	void markAsFinal(Integer id);

	List<IdVersion> getMaxVersions(List<Integer> productIds);

    List<Integer> getSubProductIds(Integer parentProductId);

	List<ProductChannel> getMappedProducts(@Param("abbreviation") String abbreviation, @Param("pmId") Integer pmId,
		@Param("multiUnitCodes") List<String> multiUnits);

	List<ProductChannel> getMappedProductsForHA(@Param("abbreviation") String abbreviation, @Param("pmId") Integer pmId,
												@Param("multiUnitCodes") List<String> multiUnits);

	ProductChannel getMbpProductWithMTCCheck(@Param("productId") String productId, @Param("abbreviation") String abbreviation);

	List<Product> getAllAvailableRoomsForRoot(Long rootId);
	
	Product readByAltIdAndSupplierId(@Param("altId") String altId, @Param("supplierId") String supplierId);
	
	List<Integer> getAllNewProductsByVersionForChannelPortal(ProductAction productAction);	
	
	Integer getBoookingSettingForProduct(Integer id);
	
	List<String> getFlagsForProduct(Integer id);

	List<String> activePRMAndSUBProductIdListByParentId(ProductAction parentId);

	List<String> unDistributedMLTProductIdListBySupplier(ProductAction productAction);

	List<String> allInactivePRMAndSUBProductIdListByParentId(ProductAction parentId);

	List<Product> readMarriottProductsForGoogleChannel(@Param("marriottChannelId") Integer marriottChannelId);
}
