package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelAttributeSettings;
import org.apache.ibatis.annotations.Param;

/**
 * @author Mihailo Spasojevic
 */
public interface ChannelAttributeSettingsMapper {

    void create(ChannelAttributeSettings channelAttributeSettings);

    ChannelAttributeSettings readByChannelPartnerId(@Param("channelPartnerId") Integer channelPartnerId);

    void deleteByChannelPartnerId(@Param("channelPartnerId") Integer channelPartnerId);

    void update(ChannelAttributeSettings channelAttributeSettings);

}
