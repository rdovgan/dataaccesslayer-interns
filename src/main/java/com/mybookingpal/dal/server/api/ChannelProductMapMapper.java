package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.IdVersion;
import java.util.ArrayList;
import java.util.List;

import com.mybookingpal.dal.shared.ExpediaInactiveReport;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ChannelProductMap;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.shared.dto.ProductZipCode9DTO;

public interface ChannelProductMapMapper {
	public void update(ChannelProductMap tempChannelProductMap);
	public ChannelProductMap readBPProduct(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> readAllBPProduct(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> readAllBPProductWithOutState(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> readAllBPProductNoLimit(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> readAllBPProductNoLimitNoState(ChannelProductMap tempChannelProductMap);
	Integer readChannelProductID(ChannelProductMap tempChannelProductMap);
	ArrayList<Integer> readProductIDs(Integer channelId);
	void createProductMap(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> findByBPProductIDForTriggerEvent(String productID);
	public ChannelProductMap findByBPProductAndChannelId(ChannelProductMap tempChannelProductMap);
	public ChannelProductMap findDuplicateByBPProductAndChannelId(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> findDuplicatesByChannelId(ChannelProductMap channelProductMap);
	public List<ChannelProductMap> findCompleteDuplicateListByProductAndChannelId(ChannelProductMap channelProductMap);
	public ChannelProductMap findByBPProductAndChannelIdForNonAirbnb(ChannelProductMap tempChannelProductMap);
	public int findCountByProductIdAndChannelId(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> findActiveRoomsOfHotel(ChannelProductMap tempChannelProductMap);
	public ChannelProductMap findByListingAndChannelIdForNonAirbnb(ChannelProductMap tempChannelProductMap);
	List<ChannelProductMap> findAllMLTbyParentAndChannelId(@Param("parentId") Integer parentId,@Param("channelId") String channelId);
	// CS: lookup properties by their ids
	public List<ChannelProductMap> findByBPProductsAndChannelId(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> findByProductsForExpedia(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> findInActiveListingsByProductsAndChannelId(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> findByBPProductsAndChannelIdAndState(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> findByBPProductsAndChannelIdAndStateAndRoomID(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> findByBPProductsAndChannelIdAndStateAndRoomIDWithoutRoomID(ChannelProductMap tempChannelProductMap);
	public ChannelProductMap findByProductIdAndChannelId(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap>  findChannelProductsMapWithoutConsideringChannelState(ChannelProductMap tempChannelProductMap);
	public ChannelProductMap findByBPProductChannelIdAndChannelId(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> findUnavailableProductsByChannelId(int channelId);
	public int findCountOfActivePropertiesForAChannel(ChannelProductMap tempChannelProductMap);
	public void suspendProducts(List<String> productIds);
	public void activateProducts(List<String> productIds);
	public List<ChannelProductMap> findByChannelProductIDAndChannelID(ChannelProductMap tempChannelProductMap);
	public List<ChannelProductMap> findByChannelProductIDAndChannelIDWithRoomIdNotNull(ChannelProductMap tempChannelProductMap);
	public void suspendProduct(ChannelProductMap tempChannelProductMap);
	void deleteByProductId(String productId);
	public List<ChannelProductMap> findByProductId(String productID);
	public List<Integer> findChannelIdsBySupplierId(String supplierId);
	
	public void updateChannelRateId(ChannelProductMap channelProductMap);
	public List<ChannelProductMap> findActiveKeyChannelProductsByChannelIdAndSupplierId(com.mybookingpal.utils.entity.NameId nameId);
	public List<ChannelProductMap> findInActiveKeyChannelProductsByChannelIdAndSupplierId(NameId nameId);
	public List<ChannelProductMap> findAllKeyChannelProductsByChannelIdAndSupplierId(com.mybookingpal.utils.entity.NameId nameId);
	public List<ChannelProductMap> findSuspendedKeyChannelProductsByChannelIdAndSupplierId(com.mybookingpal.utils.entity.NameId nameId);
	
	public List<ChannelProductMap> findActiveMLTChannelProductsByChannelIdAndSupplierId(com.mybookingpal.utils.entity.NameId nameId);
	public List<ChannelProductMap> findInActiveMLTChannelProductsByChannelIdAndSupplierId(NameId nameId);
	public List<ChannelProductMap> findAllMLTChannelProductsByChannelIdAndSupplierId(com.mybookingpal.utils.entity.NameId nameId);
	public List<ChannelProductMap> findSuspendedMLTChannelProductsByChannelIdAndSupplierId(com.mybookingpal.utils.entity.NameId nameId);
	
	List<String> productIdsBySupplier(@Param("supplierId") String supplierId,@Param("channelId") String channelId);
	List<ChannelProductMap> getBySupplierIdAndChannelId(@Param("supplierId") String supplierId,@Param("channelId") String channelId);
	/*public List<ChannelProductMap> findByReport(ChannelProductMap tempChannelProductMap);*/
	List<String> getAllChannelProductIdsForChannel(@Param("list") List<String> channelProductIds, @Param("channelId") Integer channelId);
	
	List<ChannelProductMap> getAllProductByChannel(@Param("channelId") Integer channelId);
	List<ChannelProductMap> getAllProductBy2Channel(@Param("channelId1") Integer channelId1,@Param("channelId2") Integer channelId2);
	List<ChannelProductMap> getAllProductPerPmListByChannel(@Param("list") List<String> pmList, @Param("channelId") Integer channelId);
	
	public ChannelProductMap readByProductIdChannelProductIdChannelRoomId(ChannelProductMap tempChannelProductMap);
	public ChannelProductMap findByProductIdChannelIdStateAndChannelState(ChannelProductMap tempChannelProductMap);
	public void blockChannelProducts(List<String> productIds) ;
	public ChannelProductMap findByProductIdAndChannelIds(@Param("productId") String productId, @Param("list") List<Integer> channelIds);
	public ChannelProductMap findByBPProductIdAndChannelIdAndState(ChannelProductMap tempChannelProductMap);
	public ChannelProductMap findByChannelProductIdChannelIdAndRateId(ChannelProductMap channelProductMap);
	public List<ChannelProductMap> getAllProductByPmAndByChannel(@Param("supplierId") String supplierId, @Param("list") List<Integer> channelIds);
	public List<ChannelProductMap> getAllActiveProductByPmAndByChannel(@Param("supplierId") String supplierId, @Param("list") List<Integer> channelIds);
	public ChannelProductMap getActiveChannelProductMapByProductByChannel(@Param("productId")String productId, @Param("channelId")Integer channelId);
	public List<Integer> getChannelIdsByProductId(@Param("productId") String productId);
	public List<Integer> getChannelIdsBySupplierId(@Param("supplierId") String supplierId);
	public List<String> findDistributedProductsOnChannel(@Param("supplierId") String supplierID, @Param("channelID") String channelID);
	public List<String> getAllChannelProductIdsForChannelId(@Param("channelID")  Integer channelId);
	public List<ChannelProductMap> findByListingAndChannelIDdisregardingState(ChannelProductMap channelProductMap);
	List<ChannelProductMap> findActiveListingByChannelId(ChannelProductMap channelProductMap);
	public ChannelProductMap findByBPProductAndChannelForExpedia(ChannelProductMap channelProductMap);
	public List<ChannelProductMap> findByChannelIdsWithProductIdChannelProductIdRoomIdRateplanIdNullAndProductIdNotNull(@Param("list") List<Integer> channelIds);
	public List<ChannelProductMap> findByProductIdsChannelIdsWithChannelProductIdNotNullAndRateplanIdNull(@Param("productIds") List<String> productIds, @Param("list") List<Integer> channelIds);
	public List<ChannelProductMap> findByProductIdsChannelIdsWithChannelProductIdRoomIdNotNull(@Param("productIds") List<String> productIds, @Param("list") List<Integer> channelIds);
	public List<ChannelProductMap> findByProductIdsChannelIdsWithChannelProductIdRoomIdRateplanIdNotNull(@Param("productIds") List<String> productIds, @Param("list") List<Integer> channelIds);
	public List<ChannelProductMap> findAllChannelProductsByChannelIdAndSupplierId(NameId action);
	public List<String> getProductsOnChannelForZipCodes(ProductZipCode9DTO pzc);
	
	List<ChannelProductMap> getAllActiveProductsByChannel(@Param("channelId") Integer channelId);
	List<String> getActiveSupplierIdsByChannel(@Param("channelId") Integer channelId);

	public List<ChannelProductMap> findByChannelIdsProductIds(@Param("productIds") List<String> productIds, @Param("list") List<Integer> channelIds);
	public List<ChannelProductMap> findByProductIdsChannelIdsWithChannelProductIdRoomIdNull(@Param("productIds") List<String> productIds, @Param("list") List<Integer> channelIds) ;

	List<ChannelProductMap> findByBPProductAndChannelIdWithoutStates(ChannelProductMap tempChannelProductMap);
	public List<String> findAllMLTChannelProductsByChannelIdAndParentList(@Param("ownList") List<String> ownList, @Param("channelId") String channelId);
	public String findReviewStatusByProductID(@Param("productId") Integer productId,  @Param("channelId") Integer channelId);
	public List<ChannelProductMap> findAllSwithoverPropertiesBySupplierID(@Param("supplierId")Integer supplierId, @Param("channelId") Integer channelId);
	ChannelProductMap findByProductIdAndChannelAbbreviation(@Param("productId") Integer productId, @Param("abbreviation") String abbreviation);
	List<ChannelProductMap> readAllBPProductNoLimitNoStateForSupplier(ChannelProductMap tempChannelProductMap);
	boolean isLosPriceEnable(@Param("productId") String productId, @Param("channelId") String channelID);
	Integer findChannelProductIdByProductOwnerIdAndChannelId(ChannelProductMap channelProductMap);
	void setLosPriceEnable(@Param("productId") String productId, @Param("channelId") String channelID, @Param("losPriceEnable") boolean losPriceEnable);
	List<ChannelProductMap> findByReviewStatusAndChannelIDForSupplierApi(@Param("reviewStatus") String reviewStatus, @Param("channelId") Integer channelId);
	List<ExpediaInactiveReport> allPropertiesOnExpedia();
	List<IdVersion> getMaxVersions(@Param("productIds")List<Integer> productIds);

	ChannelProductMap readById(@Param("id") Long id);

	ChannelProductMap readByProductIdAndChannelId(@Param("productId") Integer productId, @Param("channelId") Integer channelId);

	ChannelProductMap readByChannelProductIdAndChannelId(@Param("channelProductId") String channelProductId, @Param("channelId") Integer channelId);

	void updateWithoutNullCheck(ChannelProductMap channelProductMap);

	void create(ChannelProductMap channelProductMap);

	void delete(ChannelProductMap channelProductMap);
}

