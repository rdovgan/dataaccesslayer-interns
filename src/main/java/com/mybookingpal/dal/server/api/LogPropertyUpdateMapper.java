package com.mybookingpal.dal.server.api;

import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.shared.IdVersion;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.LogPropertyUpdate;


/**
 * @author Danijel Blagojevic
 *
 */
public interface LogPropertyUpdateMapper {
	LogPropertyUpdate getLogPropertyUpdateAttributesFromLastTimeStamp(@Param("productId") String productId, @Param("version") Date version);

    List<LogPropertyUpdate> getLastUpdatesByProductIds(@Param("productIds") List<Integer> productIds,
                                                       @Param("updateTypes") List<LogPropertyUpdate.UpdateTypeEnum> updateTypes);

    List<IdVersion> getLastUpdatesByProducts(@Param("productIds") List<Integer> productIds,
                                             @Param("updateTypes") List<LogPropertyUpdate.UpdateTypeEnum> updateTypes);
}
