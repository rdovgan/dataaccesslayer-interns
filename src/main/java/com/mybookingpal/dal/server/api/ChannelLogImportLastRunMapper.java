package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelLogImportLastRun;

public interface ChannelLogImportLastRunMapper {
	void create(ChannelLogImportLastRun action);

	ChannelLogImportLastRun read(Integer logId);

	void update(ChannelLogImportLastRun action);

	ChannelLogImportLastRun getLastRunByParty(ChannelLogImportLastRun action);
}
