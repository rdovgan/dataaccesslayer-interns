package com.mybookingpal.dal.server.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.FeeTaxRelation;

/**
 * @author Danijel Blagojevic
 *
 */
public interface FeeTaxRelationMapper {
	List<FeeTaxRelation> getAllByProductIdAndPartyId(@Param("productId") String productId, @Param("partyId") String partyId);
	void insertList(@Param("list") List<FeeTaxRelation> feeTaxReleationList);
	void cancelFeeTaxRelationList (List<Integer> feeTaxRelationIds);
	List<FeeTaxRelation> getByProductId(String productId);
	List<FeeTaxRelation> getByFeeId(Integer feeId);
	void copyFeesNotInRelation(@Param("fromProductId") Integer fromProductId, @Param("toProductId") Integer toProductId);
	void copyTaxesNotInRelation(@Param("fromProductId") Integer fromProductId, @Param("toProductId") Integer toProductId);
	void deleteByProductId(String productId);
	void create(FeeTaxRelation feeTaxRelation);

    List<FeeTaxRelation> readByProductIds(@Param("productIds") List<String> productIds, @Param("state") Integer state);

    List<FeeTaxRelation> readByTaxesIds(@Param("taxIds") List<Integer> taxIds);
}
