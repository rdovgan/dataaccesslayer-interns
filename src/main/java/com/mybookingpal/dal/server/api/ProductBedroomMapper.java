package com.mybookingpal.dal.server.api;
/**
 * @author Danijel Blagojevic
 * 
 * 
 */

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ProductBedroom;

public interface ProductBedroomMapper extends AbstractMapper<ProductBedroom> {
	void deleteByIdList(@Param("list") List<Integer> productBedroomIds);
	void deleteById(Integer id);
	void insertUpdate(ProductBedroom action);
    void create(ProductBedroom action);
	List<ProductBedroom> readListOfBedrooms(Integer productId);
	ProductBedroom read(Integer id);

	List<ProductBedroom> readByProductIdList(@Param("list") List<Integer> productIdList);
	Integer getRoomsNumberByProductIdAndType(ProductBedroom action);
}
