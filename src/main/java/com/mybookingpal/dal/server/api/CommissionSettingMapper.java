package com.mybookingpal.dal.server.api;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.server.util.enums.CommissionSettingEnum;
import com.mybookingpal.dal.shared.CommissionSetting;

public interface CommissionSettingMapper {

	void create(CommissionSetting commissionSetting);

	CommissionSetting read(Long id);

	CommissionSetting readBpCommissionByPmIdAndCpPartyId(CommissionSetting commissionSetting);

	CommissionSetting readBpCommissionByPmIdAndChannelPartyId(@Param("propertyManagerId") Integer pmId, @Param("channelPartnerPartyId") Integer channelId);

	CommissionSetting readChannelCommissionByPmIdChannelPartyIdAndProductId(@Param("propertyManagerId") Integer pmId, @Param("channelPartnerPartyId") Integer channelId,
			@Param("productId") Integer productId);

	void changeCommissionSetting(@Param("commissionSetting") CommissionSettingEnum commissionSettingEnum, @Param("propertyManagerId") Integer partyId,
			@Param("channelPartnerPartyId") Integer channelPartnerPartyId);

}
