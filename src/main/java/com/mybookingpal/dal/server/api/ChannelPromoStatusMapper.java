package com.mybookingpal.dal.server.api;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ChannelPromoStatus;

public interface ChannelPromoStatusMapper{

	public ChannelPromoStatus readChannelPromoStatusByPromoId(ChannelPromoStatus channelPromoStatus);
	public void create(ChannelPromoStatus channelPromoStatus);
	public void update(ChannelPromoStatus channelPromoStatus);
	public Date getMaxVersionOfChannelPromoStatus(@Param("channelId") int channelId);
	public List<ChannelPromoStatus> readChannelPromoStatusByAvailabilityStatus(ChannelPromoStatus channelPromoStatus);
	public List<ChannelPromoStatus> readChannelPromoStatusByExpiredStatus(ChannelPromoStatus channelPromoStatus);
}
