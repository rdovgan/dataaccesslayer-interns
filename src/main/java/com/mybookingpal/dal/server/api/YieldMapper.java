package com.mybookingpal.dal.server.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.shared.LocalDateRange;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.dal.shared.Yield;
import com.mybookingpal.dal.shared.RoomPeriodChange;
import com.mybookingpal.dal.shared.yield.YieldSearch;
import org.apache.ibatis.annotations.Param;


public interface YieldMapper {
	void create(Yield action);
	void update(Yield action);
	void copy(NameId oldnew);
	Integer countbyentity(Yield action);
	ArrayList<Yield> listbyentity(Yield action);	
	ArrayList<Yield> maximumgapfillers();
	Yield exists(Yield action);
	ArrayList<Yield> readByProductState(Yield action);
	void insertList(ArrayList<Yield> yieldList);
	void cancelYieldList(List<String> yieldIdList);
	void cancelversion(Yield action);
	ArrayList<Yield> listByEntityAndDate(Yield action);
	ArrayList<Yield> listByEntityAndDateWithFinal(Yield action);
	ArrayList<Yield> listbyproductids(YieldSearch action);	
	ArrayList<Yield> readByEntityIdAndEntityTypeOnlyFinal(Yield yield);
	ArrayList<Yield> readByEntityIdAndEntityType(Yield yield);
	Yield read(String id);
	Yield readByIdAndstate(Yield action);
	ArrayList<Yield> readByEntityIdAndEntityTypeStateByVersion(Yield yield);
	List<Yield> getProductYields(Yield yield);
	ArrayList<Yield> getProductsForYieldByVersion(Yield yield);

	List<RoomPeriodChange> readYieldsForPeriodForAllStateBySupplierIdByVersion(@Param("supplierId") String supplierId,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("version") Date version, @Param("channelId") Integer channelId);

	ArrayList<Yield> readInActiveByEntityAndVersion(Yield action);
	List<Yield> readByParamAndEntityAndBookingFromDate(Yield action);
	ArrayList<Yield> getPromoYieldWithinGivenDateRange(Yield yield);
	ArrayList<Yield> getPromoYieldByVersionByProduct(Yield yield);
	ArrayList<Yield> getAllPromoYieldsByVersion(Yield yield);
	ArrayList<Yield> getAllPromotionYieldsByVersionAndByProduct(Yield yield);
	ArrayList<Yield> getExpiredPromoYield(Yield yield);
	ArrayList<Yield> getExpiredPromoYieldByVersion(Yield yield);
	ArrayList<Yield> listByEntityVersionAndDateWithoutPromotion(Yield yield);
	List<Yield> getYieldsInInitialState(Yield yield);
	List<Yield> getYieldsWithBookingDates(Yield action);
	ArrayList<Yield> getAllPromoYieldsByVersionAndState(Yield action);

	List<Yield> readForPeriodByProductIds(@Param("list") List<String> productIds, @Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);

	List<Integer> readProductIdListForSpecifiedNames(@Param("list") List<String> yieldNameList, @Param("pmid") Integer pmid);

	List<String> getProductsWithNightlyYieldsWithBookingInterval(@Param("productIds") List<Integer> productIds, @Param("names") List<String> nightlyYieldTypes);

	List<Yield> readYieldByEntityIdEntityTypeAndDates(@Param("entityId") Integer entityId, @Param("entityType") String entityType, @Param("list") List<LocalDateRange> dates);
}
