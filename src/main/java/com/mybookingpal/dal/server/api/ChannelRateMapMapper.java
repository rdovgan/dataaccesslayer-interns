package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.ChannelRateMap;

public interface ChannelRateMapMapper{

	
	public List<ChannelRateMap> readChannelRateMap(ChannelRateMap tempChannelRateMap);
	public List<ChannelRateMap> readChannelRateMapByYieldId(ChannelRateMap tempChannelRateMap);
	public ChannelRateMap exists(ChannelRateMap channelRateMap);
	void create(ChannelRateMap action);
    void update(ChannelRateMap action);

}
