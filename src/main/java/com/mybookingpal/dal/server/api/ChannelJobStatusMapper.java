package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelJobStatus;

public interface ChannelJobStatusMapper { 

	void createChannelJobStatus(ChannelJobStatus channelJobStatus);
	void updateChannelJobStatus(ChannelJobStatus channelJobStatus);
	ChannelJobStatus readChannelJobStatus(ChannelJobStatus channelJobStatus);
}
