package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.IdDateDto;
import com.mybookingpal.dal.shared.IdVersion;
import com.mybookingpal.dal.shared.LocalDateRange;
import com.mybookingpal.dal.shared.LosPrice;
import com.mybookingpal.dal.shared.ProductToLosPrice;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface LosPriceMapper {

	List<LosPrice> readByProductId(@Param("productId") Integer productId);

	List<LosPrice> readById(List<Long> ids);

	List<LosPrice> readByProductIdAndCheckInDate(@Param("productId") Integer productId, @Param("checkInDate") Date checkInDate);
	List<LosPrice> readByProductIdAndCheckInAndMaxGuest(@Param("productId") Integer productId, @Param("checkInDate") LocalDate checkInDate, @Param("maxGuests") Integer maxGuests);
	LosPrice readByParamsAndClosestMaxGuest(@Param("productId") Integer productId, @Param("checkInDate") Date checkInDate, @Param("los") Integer los, @Param("maxGuests") Integer maxGuests);

	List<LosPrice> readByProductIdAndCheckinAndClosestMaxGuest(@Param("productId") Integer productId, @Param("checkInDate") LocalDate checkInDate, @Param("numberOfQuests") Integer numberOfQuests);

	Boolean isSupported(@Param("productId") Integer productId);

	Boolean isSupport(@Param("productId") Integer productId);

	void setIsSupported(@Param("productId") Integer productId, @Param("isSupported") boolean isSupported);

	void insertList(@Param("list") List<LosPrice> losPriceList);

	void update(LosPrice losPrice);

	void insertOrUpdateList(@Param("list") List<LosPrice> losPrice);

	void setToFinal(@Param("list") List<Long> listToFinal);

	void copyToArchiveTable(@Param("list") List<Long> idsToArchive);

	void deleteByIds(@Param("list") List<Long> idsToDelete);

	List<LosPrice> readByProductIdAndDates(@Param("productId") Integer productId,  @Param("list") List<LocalDateRange> dates);

	List<Integer> readSupportedProductIdsWithActiveLosPricesByLocationId(@Param("locationId") Integer locationId, @Param("fromDate") LocalDate fromDate,
			@Param("toDate") LocalDate toDate);

	List<LocalDate> countDatesInLosPricesByDateRang(@Param("productId") Integer productId,  @Param("list") List<LocalDateRange> dates);

	LocalDateTime getProductToLosVersion(Integer productId);

	List<IdVersion> getMaxVersionProductToLosVersion(@Param("productIds")List<Integer> productIds);

	List<LosPrice> getByProductIdAndVersion(@Param("productId") Integer productId, @Param("version") LocalDateTime version);

	List<LosPrice> getHistoryByProductIdAndVersion (@Param("productId") Integer productId, @Param("version") LocalDateTime version);

	void copyLosPrices(@Param("fromProductId") Integer var1, @Param("toProductId") Integer var2);

	void makeFinalByProductId(String productId);

	void copyToArchiveTableByProductId(String productId);

	void deleteByProductId(String productId);

	List<ProductToLosPrice> getProductToLosByProductIds(List<Integer> productIds);

	ProductToLosPrice getProductToLosByProductId (Integer productId);

	List<IdVersion> getMaxRateVersions(@Param("productIds")List<Integer> productIds);

	List<IdVersion> getMaxAvailabilityVersions(@Param("productIds") List<Integer> productIds, @Param("channelId") Integer channelId);

	void deleteSupportedLosPriceRecord (@Param("productId") Integer productId);

	List<LosPrice> readMinLosPriceForNext90Days(@Param("productId") Integer productId);

	List<Integer> checkLosPriceEnableForProduct(@Param("productIds") List<Integer> productIds);

	List<IdDateDto> countDatesInLosPricesByDateRangForProducts(@Param("productIds") List<Integer> productIds, @Param("range") LocalDateRange range);

}
