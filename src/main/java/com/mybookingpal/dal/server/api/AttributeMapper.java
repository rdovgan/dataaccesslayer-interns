package com.mybookingpal.dal.server.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.Amenities;
import com.mybookingpal.dal.shared.Attribute;

public interface AttributeMapper {
	ArrayList<String> readAttribute(List<String> relation);
	Attribute read(Attribute action);
	List<Amenities> getAmenitiesById(@Param("productId") Integer productId);

}
