package com.mybookingpal.dal.server.api.beststayz;


import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.beststayz.BeststayzMailThreadMessageViewed;

import java.util.List;

public interface BeststayzMailThreadMessageViewedMapper {

	void create(BeststayzMailThreadMessageViewed action);
	void insertList(@Param("list") List<BeststayzMailThreadMessageViewed> list);
	BeststayzMailThreadMessageViewed readByMessageIdAndUserType(@Param("messageId") Long messageId, @Param("userType") String userType);
}
