package com.mybookingpal.dal.server.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.Commission;

public interface CommissionMapper {

	void create(Commission action);

	Commission read(Integer id);

	void update(Commission action);

	void updateCommissionByTypeAndPmId(Commission action);

	void delete(Integer id);

	ArrayList<Commission> readCommissionsListByPm(Integer pmId);

	ArrayList<Commission> readBpCommissionsForPm(Integer pmId);

	List<Commission> readBpSeasonalCommissionsForPmInDateRange(Commission commission);

	List<Commission> readPmIncludedCommissionsForPmInDateRange(@Param("partyId") Integer partyId, @Param("toPartyId") Integer toPartyId,
			@Param("bookingDate") String bookingDate);

	List<Commission> readPmIncludedCommissionsForPm(@Param("partyId") Integer partyId, @Param("toPartyId") Integer toPartyId);

	List<Commission> readCommissionsForPmInDateRange(@Param("partyId") Integer partyId, @Param("toPartyId") Integer toPartyId,
			@Param("bookingDate") Date bookingDate, @Param("type") Integer type);

	List<Commission> readCommissions(@Param("partyId") Integer partyId, @Param("toPartyId") Integer toPartyId, @Param("type") Integer type);

	List<Commission> readCommissionListByPmAndChannelPartnerPartyId(@Param("pmId") Integer propertyManagerId,
			@Param("channelPartnerPartyId") Integer channelPartnerPartyId);
}
