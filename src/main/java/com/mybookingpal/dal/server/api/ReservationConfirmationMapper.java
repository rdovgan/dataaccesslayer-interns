package com.mybookingpal.dal.server.api;

import java.util.List;
import java.util.Map;

import com.mybookingpal.dal.shared.ReservationConfirmation;

public interface ReservationConfirmationMapper {

	void create(ReservationConfirmation action);
	ReservationConfirmation read(String id);
	ReservationConfirmation readByReservationId(Integer id);
	Integer readLatestReservationIdByConfirmationId(String id);
	ReservationConfirmation readOldReservationIdByConfirmationId(String id);
	List<ReservationConfirmation> list();
	List<ReservationConfirmation>  readByChannelIdConfirmationId(ReservationConfirmation action);
	List<ReservationConfirmation>  readByChannelIdAndCreatedDate(ReservationConfirmation action);
	List<ReservationConfirmation>  readDuplicatesByChannelIdAndCreatedDate(ReservationConfirmation action);
	List<ReservationConfirmation> readByConfirmationId(String confirmationI);
	List<ReservationConfirmation>  findByChannelIdAndConfirmationIdLike(ReservationConfirmation action);
	void update(ReservationConfirmation action);
	void delete(String id);
	void deleteByProductId(String productId);
	Map<String, String> readLatestReservationIdAndProductIdByConfirmationId(String id);
}