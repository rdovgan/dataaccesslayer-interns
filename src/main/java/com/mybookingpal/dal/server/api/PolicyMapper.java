package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.Policy;

/**
 * @author Danijel Blagojevic
 *
 */
public interface PolicyMapper {
	
	List<Policy> listAll();
	String readId(String name);

}
