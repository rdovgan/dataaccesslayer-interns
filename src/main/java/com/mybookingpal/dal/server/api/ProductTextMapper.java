package com.mybookingpal.dal.server.api;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ProductText;

public interface ProductTextMapper {
	List<ProductText> getByProductId(Integer productId);
	List<ProductText> getProductTextFromLastTimeStamp(@Param("productId") String productId, @Param("version") Date version);
}
