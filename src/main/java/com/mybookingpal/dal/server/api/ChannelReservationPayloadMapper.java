package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelReservationPayload;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelReservationPayloadMapper {

	void create(ChannelReservationPayload channelReservationPayload);

	void update(ChannelReservationPayload channelReservationPayload);

	void updateItem(ChannelReservationPayload channelReservationPayload);

	ChannelReservationPayload readChannelReservationPayloadByReservationId(int reservationId);

	ChannelReservationPayload readChannelReservationPayloadByConfirmationId(String confirmationId);

	List<ChannelReservationPayload> readMissedChannelReservationPayloadByChannelId(Integer channelId);

	void createChannelReservationPayload(ChannelReservationPayload channelReservationPayload);

	//List<ChannelPayloadsWithAdditionalInfoDto> readChannelReservationPayloadsWithAdditionalInfo(@Param("fromDate") LocalDate fromDate,
	//		@Param("toDate") LocalDate toDate);

	List<ChannelReservationPayload> findAllChannelReservationPayloadsForYear2018ByChannel(@Param("agentId") Integer agentId,
			@Param("channelId") Integer channelId, @Param("includeCancel") boolean includeCancel);

	//List<ChannelPayloadsWithAdditionalInfoDto> readChannelReservationPayloadsWithAdditionalInfoByReservationId(@Param("reservationId") Long reservationId);

	List<ChannelReservationPayload> findAllChannelReservationPayloadsWithReservationIdZero();
}
