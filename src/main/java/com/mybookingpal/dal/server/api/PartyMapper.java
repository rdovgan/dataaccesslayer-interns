package com.mybookingpal.dal.server.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.Party;

public interface PartyMapper {

	Party read(String id);

	ArrayList<Party> partylistbyids(List<String> partyids);

	List<Integer> listChannelID(Integer pmId);

	void insertChannelPatner(@Param("pmid") Integer managerId, @Param("channelid") Integer channelId);

	List<String> getAllPartyIdsPerPMSAndChannel(@Param("organizationId") String organizationId, @Param("channelId") Integer channelId);

	Party readActiveParty(String id);

	Integer getPartyIdByEmployerId(@Param("channelId") Integer channelId);

	void create(Party action);

}
