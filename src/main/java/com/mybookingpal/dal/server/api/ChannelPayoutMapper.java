package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.ChannelPayout;

public interface ChannelPayoutMapper {
	public void createChannelPayout(ChannelPayout tempChannelPayout);
	public List<ChannelPayout> fetchChannelPayout(ChannelPayout tempChannelPayout);
	public List<ChannelPayout> getChannelPayoutsByConfirmationCode(ChannelPayout tempChannelPayout);
	public List<ChannelPayout> fetchChannelPayoutForDailyReport(ChannelPayout tempChannelPayout);
	public List<ChannelPayout> fetchChannelPayoutForSelectedDates(ChannelPayout tempChannelPayout);
	public List<ChannelPayout> fetchChannelPayoutForLastFetch(ChannelPayout tempChannelPayout);
	public Integer getDuplicateChannelPayoutCount(ChannelPayout tempChannelPayout);
}
