package com.mybookingpal.dal.server.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.ChannelRatePlanMap;

public interface ChannelRatePlanMapMapper {

	void create(ChannelRatePlanMap ratePlanMap);

	ChannelRatePlanMap readByChannelIdAndRatePlanIdAndProductIdWithStateNotFinal(ChannelRatePlanMap ratePlanMap);

	void updateState(ChannelRatePlanMap ratePlanMap);

	void update(ChannelRatePlanMap ratePlanMap);

	List<ChannelRatePlanMap> readByProductIdChannelId(ChannelRatePlanMap ratePlanMap);

	ChannelRatePlanMap findByProductIdAndChannelIdsAndRateplanIdNull(@Param("productId") String productId, @Param("list") List<Integer> channelIds);

	List<ChannelRatePlanMap> getAllProductByPmAndByChannel(@Param("supplierId") String supplierId, @Param("list") List<Integer> channelIds);

	List<Integer> getChannelIdsByProductId(@Param("productId") String productId);

	List<Integer> getChannelIdsBySupplierId(@Param("supplierId") String supplierId);
	
	ChannelRatePlanMap findByProductIdsChannelIdsWithChannelProductIdRoomIdNotNullRateplanIdNull(@Param("productIds") List<String> productIds, @Param("list") List<Integer> channelIds);

	List<ChannelRatePlanMap> findByProductIdsChannelIds(@Param("productIds") List<String>  productIds,  @Param("list") List<Integer> channelIds);

	ChannelRatePlanMap findByChannelProductIdAndRateId(ChannelRatePlanMap channelRatePlanMap);
}
