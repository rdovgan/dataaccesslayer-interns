package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.Items;
import com.mybookingpal.dal.shared.RateItem;

import java.util.List;

public interface ItemsMapper {

	List<Items> getAllItems();

	List<Items> readItemsByRatePlanId(Long ratePlanId);

	void create(Items item);

	void createRateItem(RateItem rateItem);

	void createRateItemList(List<RateItem> rateItems);

	void delete(Items item);

	void deleteRateItem(RateItem rateItem);

	void deleteAllItemsByRatePlanId(Long ratePlanId);
}
