package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelProductStatus;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelProductStatusMapper {

    void create(ChannelProductStatus channelProductStatus);

    ChannelProductStatus getChannelProductStatusByProductIdAndChannelIdAndFunction(@Param("productId") Integer productId, @Param("channelId") Integer channelId, @Param("function") Integer function);

    void update(ChannelProductStatus channelProductStatus);

    List<ChannelProductStatus> getChannelProductStatusByProductIdAndChannelIdAndFunctionList(@Param("productIds") List<Integer> productIds, @Param("channelId") Integer channelId, @Param("functions") List<Integer> functions);

    List<ChannelProductStatus> getInProgressChannelProductStatusByProductIdAndChannelId(@Param("productIds") List<Integer> productIds, @Param("channelId") Integer channelId);

    List<ChannelProductStatus> getLatestChannelProductStatusByProductIdAndChannelIdAndFunctions(@Param("productIds") List<Integer> productIds, @Param("channelId") Integer channelId, @Param("functions") List<Integer> functions);

    List<ChannelProductStatus> getChannelProductStatusByProductIdsAndChannelIdsAndFunctionList(@Param("productIds") List<Integer> productIds, @Param("channelIds") List<Integer> channelId, @Param("functions") List<Integer> functions);

    List<ChannelProductStatus> getChannelProductStatusForExpedia(@Param("productId") Integer productId, @Param("function") Integer function);
}
