package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.Setting;

public interface SettingMapper {
	void create(Setting setting);

	Setting read(Integer id);

	Setting readbyname(String name);

	void update(Setting setting);
	
	void updateValue(Setting setting);

	void delete(Integer id);

	List<Setting> list();
	
	List<String> readEmailList(String keyName);
}
