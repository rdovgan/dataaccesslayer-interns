package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelProductValidationRules;
import com.mybookingpal.dal.shared.ChannelProductValidationRulesWithName;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelProductValidationRulesMapper {

    void create (ChannelProductValidationRules channelProductValidationRules);

    List<ChannelProductValidationRulesWithName> getAllRulesForChannel (@Param("channelId") Integer channelId);

}
