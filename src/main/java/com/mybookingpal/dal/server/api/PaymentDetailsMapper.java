package com.mybookingpal.dal.server.api;


import java.util.List;

import com.mybookingpal.dal.shared.PaymentDetails;

public interface PaymentDetailsMapper {

	void create(PaymentDetails paymentDetails);

	PaymentDetails read(long id);

	void update(PaymentDetails paymentDetails);

	List<PaymentDetails> readByProductIdWithStateCreated(int productId);

	List<PaymentDetails> readByPropertyManagerIdWithStateCreated(int propertyManagerId);
}
