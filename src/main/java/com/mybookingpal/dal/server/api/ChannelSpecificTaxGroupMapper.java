package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelSpecificTaxGroup;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelSpecificTaxGroupMapper {

    List<ChannelSpecificTaxGroup> getByPropertyManagerId(@Param("propertyManagerId") Integer propertyManagerId);
    void insertNewPropertyManager(Integer newPropertyManager);
}
