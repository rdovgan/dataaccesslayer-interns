package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.RatePlanModel;
import org.apache.ibatis.annotations.Param;

public interface RatePlanMapper {

	void create(RatePlanModel ratePlanModel);

	RatePlanModel read(Long id);

	void update(RatePlanModel ratePlanModel);

	void deleteById(Long ratePlanId);

	RatePlanModel readByRateCode(String rateCode);

	List<RatePlanModel> getRatePlansForProductId(Long productId);

	List<RatePlanModel> listByProductIds(@Param("productIds") List<Long> productIds);
}
