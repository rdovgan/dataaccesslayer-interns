package com.mybookingpal.dal.server.api;

import java.util.ArrayList;
import java.util.List;

import com.mybookingpal.dal.shared.Adjustment;
import com.mybookingpal.dal.shared.action.DatesIdAction;
import com.mybookingpal.dal.shared.adjustment.AdjustmentSearch;
import com.mybookingpal.dal.shared.api.HasPrice;

public interface AdjustmentMapper {

	void create(Adjustment action);
	void update(Adjustment action);
	Adjustment exists(Adjustment action);
	Adjustment read(int id);
	Adjustment readbyexample(Adjustment action);
	Adjustment readbyreservation(HasPrice action);
	void cancelversion(Adjustment action);
	void canceladjustmentlist(List<String> ajustmentIdList);
	Double getfixprice(Adjustment action);
	ArrayList<Adjustment> readbyproductandstate(Adjustment action);
	ArrayList<Adjustment> listcreatedbyproductids(AdjustmentSearch action);
	List<Adjustment> readByProductIdAndDateRange(DatesIdAction action);
	
}
