package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.dao.entities.AttributeDisplayModel;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.dao.entities.AttributeDisplayWithProduct;

import java.util.List;

public interface AttributeDisplayMapper {

	List<AttributeDisplayWithProduct> readAttributesForRoot(@Param("productId") Long productId);

	List<AttributeDisplayWithProduct> readAttributesForRoom(@Param("productIds") List<Long> productIds, @Param("updateType") Boolean updateType);

	List<AttributeDisplayWithProduct> readAttributes(@Param("productIds") List<Long> productIds, @Param("updateType") Boolean updateType);

	List<AttributeDisplayWithProduct> readAllPossibleAttributes();

	List<AttributeDisplayWithProduct> readAttributesForResort(@Param("productIds") List<Long> productIds, @Param("updateType") Boolean updateType);

	List<AttributeDisplayWithProduct> readDefiningAttributesNotIncludeCurrentForRoot(List<String> activeAttributes);

	List<AttributeDisplayWithProduct> readDefiningAttributesNotIncludeCurrentForRoom(List<String> activeAttributes);

	List<AttributeDisplayWithProduct> readDefiningAttributeForRoot();

	List<AttributeDisplayWithProduct> getAmenityByDisplayCategory(@Param("categoryType") String categoryType);

	List<AttributeDisplayWithProduct> readDefiningAttributesForRoom();

	List<AttributeDisplayWithProduct> readAllProductAttributes(@Param("productId") Long productId);

	List<AttributeDisplayWithProduct> readProductsAttributesWithCategories(@Param("productIds") List<Long> productIds, @Param("categoryType") String categoryType);

	List<AttributeDisplayModel> readAttributeDisplaysByAttributeCodes (@Param("attributeCodes") List<String> attributeCodes);

	List<AttributeDisplayWithProduct> readUniqueChildrenAttributes(@Param("ids") List<Long> productIds, @Param("size") Integer size);

	List<AttributeDisplayWithProduct> readBedTypeProductAttributes(@Param("productId") Long productId, @Param("categoryType") String categoryType);

	List<AttributeDisplayWithProduct> readAllPossibleBedTypes(@Param("categoryType") String categoryType);
}
