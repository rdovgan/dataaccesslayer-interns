package com.mybookingpal.dal.server.api;

import java.util.List;

import com.mybookingpal.dal.shared.ProductSpecific;

public interface ProductSpecificMapper {

	void create(ProductSpecific productSpecific);
	void update(ProductSpecific productSpecific);
	void createList(List<ProductSpecific> list);
	void updateList(List<ProductSpecific> list);
	List<ProductSpecific> readByProductId(int productId);
	List<ProductSpecific> readByPartyId(int partyId);
}
