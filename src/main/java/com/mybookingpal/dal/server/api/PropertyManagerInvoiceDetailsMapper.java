package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.PropertyManagerInvoiceDetails;

/**
 * @author Danijel Blagojevic
 *
 */
public interface PropertyManagerInvoiceDetailsMapper {

	PropertyManagerInvoiceDetails getByPmId(Integer pmId);
	void create(PropertyManagerInvoiceDetails propertyManagerInvoiceDetails);
	void update(PropertyManagerInvoiceDetails propertyManagerInvoiceDetails);
}
