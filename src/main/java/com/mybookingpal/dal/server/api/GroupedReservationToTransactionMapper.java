package com.mybookingpal.dal.server.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.GroupedReservationToTransaction;

public interface GroupedReservationToTransactionMapper {

	void create(GroupedReservationToTransaction groupedReservation);

	GroupedReservationToTransaction read(Long id);

	String readStateOfLastPaymentTransactionByGroupReservationId(Long groupedReservationId);

	GroupedReservationToTransaction readGroupedReservationWithPendingTransactionByGroupedReservationId(Long groupedReservationId);
	
	GroupedReservationToTransaction readByTransactionIdAndType(@Param("transactionId") Long transactionId, @Param("transactionType") GroupedReservationToTransaction.TransactionType transactionType);

	List<Long> readTransactionIdsByGroupedReservationIdAndType(@Param("groupedReservationId") Long groupedReservationId, @Param("type") GroupedReservationToTransaction.TransactionType type);

	GroupedReservationToTransaction readLastGroupedReservationTransactionByGroupIdAndType(@Param("reservationId") String reservationId, @Param("type") GroupedReservationToTransaction.TransactionType type);

	GroupedReservationToTransaction readGroupedReservationByTransactionTypeAndTransactionId(@Param("transactionId") Long transactionId, @Param("type") GroupedReservationToTransaction.TransactionType transactionType);
}
