package com.mybookingpal.dal.server.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.Image;
import com.mybookingpal.dal.shared.NameIdAction;
import com.mybookingpal.dal.shared.Product;
import com.mybookingpal.dal.shared.image.ImageSearch;
import com.mybookingpal.dal.shared.image.ImageTag;

public interface ImageMapper extends AbstractMapper<Image> {

	ArrayList<Image> imagesbyproductidsortorder(NameIdAction action);
	List<ImageTag> readAllImageTags();
	List<ImageSearch> listByProductIds(ImageSearch action);

	List<Image> imagesByProductId(String id);
	List<ImageTag> readImageTags(Integer id);
	List<Image> getImagesFromLastTimeStamp(@Param("productId") String productId, @Param("version") Date version);
	void createImageTag(ImageTag imageTag);
	void updateImageTag(ImageTag imageTag);
	ImageTag readImageTag(String imageTagId);
	List<Image> imagesByProductIdRegardlessOfState(String productId);
}