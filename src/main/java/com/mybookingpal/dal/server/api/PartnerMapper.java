package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.Partner;

import java.util.List;

public interface PartnerMapper extends AbstractMapper<Partner> {

	Partner exists(String partyid);
	Partner readByPartyId(Integer partyid);
	Partner getParentByPartyId(String partyid);
	List<Partner> readAllByOrganizationId(Integer organizationid);
	Partner readAllByAbbrevation(String abbrevation);
}
