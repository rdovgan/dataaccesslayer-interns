package com.mybookingpal.dal.server.api;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.cancellation.ChannelCancellationRule;
import com.mybookingpal.dal.shared.cancellation.ChannelSpecificCancellationRule;

public interface CancellationRuleMapMapper {

	void create(ChannelSpecificCancellationRule rule);

	ChannelSpecificCancellationRule read(Integer id);

	List<ChannelSpecificCancellationRule> readAll();

	String readChannelAbbreviation(Integer id);

	void update(ChannelSpecificCancellationRule rule);

	List<ChannelSpecificCancellationRule> readByPmName(String name);

	List<ChannelSpecificCancellationRule> getAllCancellationRules();

	List<ChannelSpecificCancellationRule> readChannelSpecificCancellationRulesByIds(List<Integer> cancellationIds);
	
	List<ChannelSpecificCancellationRule> readCancellationsByPmIdOrProductIdAndChannelId(ChannelCancellationRule cancellationRule);

	ChannelSpecificCancellationRule readByChannelCancellationMapId(@Param("channelCancellationMapId") String channelCancellationMapId);

	List<ChannelSpecificCancellationRule> readByChannelCancellationMapPerChannelAbbreviation(@Param("channelAbbreviation") String channelAbbreviation);

}
