package com.mybookingpal.dal.server.api;

import com.mybookingpal.dal.shared.ChannelProductOpportunity;
import com.mybookingpal.shared.dto.ChannelProductOpportunityDto;
import com.mybookingpal.shared.dto.OpportunityFilterDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChannelProductOpportunityMapper {

	ChannelProductOpportunity readById(Integer id);

	List<ChannelProductOpportunity> readByExample(ChannelProductOpportunity example);

	void update(ChannelProductOpportunity channelProductOpportunity);

	void create(ChannelProductOpportunity action);

	List<ChannelProductOpportunity> readByProductId(@Param("productId") Integer productId);

	List<ChannelProductOpportunityDto> readByFilters(@Param("filter") OpportunityFilterDto opportunityFilter);

	List<ChannelProductOpportunityDto> readForCSVByFilters(@Param("filter") OpportunityFilterDto opportunityFilter);

	Integer readNumberByFilters(@Param("filter") OpportunityFilterDto opportunityFilter);

	List<ChannelProductOpportunity> readAllByChannelId(@Param("channelId") Integer channelId);

	void deleteChannelProductOpportunity(Integer id);

	List<ChannelProductOpportunity> readByOpportunityId(@Param("opportunityId") Integer opportunityId);

	List<ChannelProductOpportunity> readProductByOpportunityId(@Param("opportunityId") Integer opportunityId,@Param("productIds") List<Integer> productIds, @Param("paginationLimit") Integer pagination,
															   @Param("paginationPage") Integer paginationPage);

	List<ChannelProductOpportunity> readPropertiesByOpportunityId(@Param("opportunityId") Integer opportunityId);

}
