package com.mybookingpal.dal.server.api;

public interface AbstractMapper<T> {

	void create(T action);
	T read(String id);
	void update(T action);
	void write(T action);


}
