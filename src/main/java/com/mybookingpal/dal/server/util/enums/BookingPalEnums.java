package com.mybookingpal.dal.server.util.enums;

import com.mybookingpal.dal.utils.BigDecimalExt;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class BookingPalEnums {

	public interface Valued {
		int getValue();

		String getStringValue();
	}

	public enum YieldType implements Valued {
		DATE_RANGE("Date Range"),
		DAY_OF_WEEK("Day of Week"),
		GAP_FILLER("Maximum Gap Filler"),
		EARLY_BIRD("Early Booking Lead Time"),
		LAST_MINUTE("Last Minute Lead Time"),
		LENGTH_OF_STAY("Length of Stay"),
		OCCUPANCY_ABOVE("Occupancy Above"),
		OCCUPANCY_BELOW("Occupancy Below"),
		WEEKEND("Weekend"),
		WEEKLY("Weekly"),

		BASE("Basic Promotion"),
		SAME_DAY_DEAL("Same Day Deal"),
		EARLY_BOOKING_DEAL("Early Booking Deal"),
		FREE_NIGHTS("Free Nights"),
		SEASONAL_ADJUSTMENT("Season adjustment"),
		STAYED_AT_LEAST_X_DAYS("Long-term stay adjustment"),
		BOOKED_WITHIN_AT_MOST_X_DAYS("Last-minute discount"),
		BOOKED_BEYOND_AT_LEAST_X_DAYS("Booking ahead discount"),
		MOBILE_RATE("Mobile rate"),
		BUSINESS_BOOKER("Business booker"),
		GEO_RATE("Geo rate"),
		CHANNEL_MARKUP("Channel Markup");

		private String value;

		YieldType(String value) {
			this.value = value;
		}

		@Override
		public String getStringValue() {
			return this.value;
		}

		@Override
		public int getValue() {
			throw new UnsupportedOperationException();
		}

		public static Set<YieldType> getSpecialPromotions() {
			return EnumSet.of(BASE, SAME_DAY_DEAL, EARLY_BOOKING_DEAL, FREE_NIGHTS);
		}

		public static Set<YieldType> getGeneralPromotions() {
			return EnumSet.of(DATE_RANGE, WEEKEND, WEEKLY, DAY_OF_WEEK, LENGTH_OF_STAY, LAST_MINUTE, EARLY_BIRD, OCCUPANCY_ABOVE, OCCUPANCY_BELOW);
		}

		public static Set<YieldType> getYields() {
			return EnumSet.of(WEEKEND, DAY_OF_WEEK, GAP_FILLER, LENGTH_OF_STAY, OCCUPANCY_ABOVE, OCCUPANCY_BELOW);
		}

		public static Set<YieldType> getPMSSupportedYields() {
			return EnumSet.of(WEEKEND, DAY_OF_WEEK, DATE_RANGE, EARLY_BIRD, LAST_MINUTE, GAP_FILLER, LENGTH_OF_STAY, OCCUPANCY_ABOVE, OCCUPANCY_BELOW);
		}

		public static Set<YieldType> getPromotions() { return EnumSet.of(BASE, SAME_DAY_DEAL, EARLY_BOOKING_DEAL, FREE_NIGHTS, SEASONAL_ADJUSTMENT, STAYED_AT_LEAST_X_DAYS, BOOKED_WITHIN_AT_MOST_X_DAYS, BOOKED_BEYOND_AT_LEAST_X_DAYS, MOBILE_RATE, GEO_RATE, BUSINESS_BOOKER);}

		public static Set<YieldType> getPromotionsForRackRate() { return EnumSet.of(BASE, SAME_DAY_DEAL, EARLY_BOOKING_DEAL, FREE_NIGHTS, WEEKEND, LENGTH_OF_STAY);}

		public static Set<YieldType> getAIRBNBPromotions() { return EnumSet.of(SEASONAL_ADJUSTMENT, STAYED_AT_LEAST_X_DAYS, BOOKED_WITHIN_AT_MOST_X_DAYS, BOOKED_BEYOND_AT_LEAST_X_DAYS);}
	}


	public enum Channel implements Valued {
		BOOKINGCOM("Booking.com", "BKG"),
		AIRBNB("AirBnb", "ABB"),
		AIRBNB_STAGING("AirBnb", "ABBStaging"),
		EXPEDIA("Expedia", "EXP"),
		EXPEDIA_HC("Expedia Hotel Collect", "EXP_HC"),
		EXPEDIA_HC_OLD("Expedia Hotel Collect Old", "EXP_HC_OLD"),
		HOMEAWAY("HomeAway", "HAC"),
		THIRDHOME("ThirdHome", "TRH"),
		AGODA("Agoda", "AGD"),
		TRIP_ADVISOR("Trip advisor", "TRA"),
		GOOGLE("Google", "GGL"),
		MARRIOTT("Marriott", "MRT"),
		SECRA("Secra", "SCR"),
		FEWO("Fewo", "FEWO"),
		INNTOPIA("Inntopia","INP"),
		BOOKINGPAL("Bookingpal", "BP"),
		ALLEGIANT_AIR("Allegiant Air", "AAY"),
		HYATT("Hyatt", "HYATT"),
		TEST_CHANNEL("Test Channel", "TCA"),
		HVMI_MARRIOT("HVMI Marriott", "HVMI"),
		RENTALZ("Rentalz", "RETZ"),
		SMILING_HOUSE("Smiling House", "SGH"),
		SMOKY_MOUNTAIN("Smoky Mountain", "SMK"),
		THE_BACH("The BACH", "BACH"),
		PLUM_GUIDE("Plum Guide", "PLMG"),
		BEACH_HOUSE("BeachHouse", "BHS"),
		RENTBUTTON("RentButton", "RNTZ"),
		QUINTESS_COLLECTIONS("Quintess Collections", "QUIN"),
		MOUNTAIN_SKI_TRIPS("Mountain Ski Trips", "MSR"),
		MONAKER_BOOKING("Monaker Booking", "MNB");

		private String name;
		private String value;

		Channel(String name, String value) {
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		@Override
		public String getStringValue() {
			return this.value;
		}

		@Override
		public int getValue() {
			throw new UnsupportedOperationException();
		}

		public static Set<Channel> getChannelAbbreviations() { return EnumSet.of(BOOKINGCOM, AIRBNB, EXPEDIA, EXPEDIA_HC, EXPEDIA_HC_OLD, HOMEAWAY, GOOGLE, INNTOPIA);}

		public static Set<Channel> getExpediaAbbreviations() { return EnumSet.of(EXPEDIA, EXPEDIA_HC, EXPEDIA_HC_OLD);}

		public static Set<Channel> getChannels() {
			return EnumSet.of(BOOKINGCOM, AIRBNB, EXPEDIA, HOMEAWAY, AGODA, TRIP_ADVISOR, GOOGLE, INNTOPIA);
		}

		public static List<String> getByAbb(List<String> abb) {
			return Arrays.stream(Channel.values()).filter(channel -> abb.contains(channel.value)).map(Channel::name).collect(Collectors.toList());
		}

		public static Set<Channel> getSupportedDifferentDisplayNamesChannel() {return EnumSet.of(AIRBNB, BOOKINGCOM, EXPEDIA, HOMEAWAY, TRIP_ADVISOR, MARRIOTT);}

		public static Set<Channel> getChannelsForGuidelinesProducts() {
			return EnumSet.of(MARRIOTT);
		}

		public static Set<Channel> getChannelsSupportedChannelCommissionOnProductLevel() {
			return EnumSet.of(BOOKINGCOM, EXPEDIA, EXPEDIA_HC, AIRBNB, TRIP_ADVISOR, HOMEAWAY);
		}

		public static Set<Channel> getSupportedChannelSpecificTax() {
			return EnumSet.of(BOOKINGCOM, EXPEDIA, EXPEDIA_HC, AIRBNB, HOMEAWAY);
		}

		public static Set<Channel> getChannelsUseChannelApi() {
			return EnumSet.of(ALLEGIANT_AIR, HYATT, TEST_CHANNEL, HVMI_MARRIOT, RENTALZ, SMILING_HOUSE, SMOKY_MOUNTAIN, THE_BACH, PLUM_GUIDE, BEACH_HOUSE,
					RENTBUTTON, QUINTESS_COLLECTIONS, MOUNTAIN_SKI_TRIPS, MONAKER_BOOKING);
		}

	}

	public enum TypeOfCharge implements Valued {
		DECREASE_PERCENT("Decrease Percent"),
		DECREASE_AMOUNT("Decrease Amount");
		private String value;

		TypeOfCharge(String value) {
			this.value = value;
		}

		@Override
		public String getStringValue() {
			return this.value;
		}

		@Override
		public int getValue() {
			throw new UnsupportedOperationException();
		}
	}

	public enum YieldsTypeOfCharge implements Valued {
		DECREASE_PERCENT("Decrease Percent"),
		DECREASE_AMOUNT("Decrease Amount"),
		INCREASE_PERCENT("Increase Percent"),
		INCREASE_AMOUNT("Increase Amount");
		private String value;

		YieldsTypeOfCharge(String value) {
			this.value = value;
		}

		@Override
		public String getStringValue() {
			return this.value;
		}

		@Override
		public int getValue() {
			throw new UnsupportedOperationException();
		}

		public static Set<YieldsTypeOfCharge> getTypeValues() {
			return EnumSet.of(DECREASE_PERCENT, DECREASE_AMOUNT, INCREASE_PERCENT, INCREASE_AMOUNT);
		}
	}

	public enum ChannelNotificationEnum implements Valued {
		PRODUCT_ACTIVATION("/webmessages/multiunit/activate/"),
		PRODUCT_CREATE("/webmessages/multiunit/create/"),
		PRODUCT_UPDATE("/webmessages/multiunit/update/"),
		PRODUCT_DEACTIVATION("/webmessages/multiunit/deactivate/"),
		PRODUCT_PHOTO_CREATE("/webmessages/multiunit/photos/create/"),
		PRODUCT_PHOTO_UPDATE("/webmessages/multiunit/photos/update/"),
		CALENDAR_UPDATE("/webmessages/multiunit/calendar/update/"),
		YIELD_UPDATE("/webmessages/multiunit/yields/update/"),
		YIELD_PROMOTION_BOOKING_CREATE("/webmessages/bookingpromotion/create/v2/"),
		YIELD_PROMOTION_BOOKING_UPDATE("/webmessages/bookingpromotion/update/v2/"),
		YIELD_PROMOTION_BOOKING_ACTIVATE("/webmessages/bookingpromotion/activate/v2/"),
		YIELD_PROMOTION_BOOKING_INACTIVE("/webmessages/bookingpromotion/inactivate/v2/"),
		FEE_UPDATE("/webmessages/multiunit/fees/update/"),
		FEE_UPATE("/webmessages/bookingpromo/update"),
		FEE_NET_RATE("/webmessages/rate/update/"),
		TAX_UPDATE("/webmessages/multiunit/taxes/update/"),
		RATE_PLAN("/webmessages/rateplan/product/{productId}/channel/{channelId}/rateplan/{ratePlanId}"),
		RESTRICTION_UPDATE("/webmessages/restriction/update/version/"),
		CHANNEL_CANCELLATION_CREATE("/webmessages/cancellation/channelId/"),
		LINKED_PRODUCT_AVAILABILITY_CALENDAR("/webmessages/multiunitkeytorep/availabilitycalendar/create/productid/"),
		YIELD_PROMOTION_AIRBNB_CREATE("/webmessages/airbnb/promo/create/productId/%d/supplierId/%d/channelId/%d/yieldIds/%s/environment/%s"),
		YIELD_PROMOTION_AIRBNB_DELETE("/webmessages/airbnb/promo/delete/productId/%d/supplierId/%d/channelId/%d/yieldIds/%s/environment/%s"),
		CHANNEL_FEE_TAX("/webmessages/channel/feetax/supplierId/%d/version/%s/channelId/%d"),
		CHANNEL_ACTIVATION_DEACTIVATION("/webmessages/channel/activationdeactivation/supplierId/%d/version/%s/channelId/%d"),
		CHANNEL_CREATE_OR_UPDATE_LISTING("/webmessages/channel/createorupdatelisting/supplierId/%d/version/%s/channelId/%d"),
		CHANNEL_STATIC_DATA("/webmessages/channel/updatestaticdata/supplierId/%d/version/%s/channelId/%d"),
		CHANNEL_CREATE_OR_UPDATE_IMAGE("/webmessages/channel/createorupdateimage/supplierId/%d/version/%s/channelId/%d"),
		CHANNEL_DELETE_LISTING("/webmessages/channel/deletelisting/supplierId/%d/version/%s/channelId/%d"),
		CHANNEL_MAX_PERSON("/webmessages/channel/maxperson/supplierId/%d/version/%s/channelId/%d"),
		CHANNEL_RESTRICTION("/webmessages/channel/restriction/supplierId/%d/version/%s/channelId/%d"),
		CHANNEL_RESERVATION("/webmessages/channel/reservation/supplierId/%d/version/%s/channelId/%d"),
		CHANNEL_RATE("/webmessages/channel/rate/supplierId/%d/version/%s/channelId/%d"),
		CHANNEL_RATE_UPDATE("/webmessages/rate/update/"),
		CHANNEL_CALENDAR("/webmessages/channel/calendar/supplierId/%d/version/%s/channelId/%d"),
		COMMISSION_ON_PRODUCT_LEVEL("/webmessages/property/update/channel-specific-rates-and-availability"),
		ACTIVATION_DEACTIVATION("/webmessages/activationdeactivation/productId/%d"),
		CHANNEL_SPECIFIC_TAX("/webmessages/property/update/channel-specific-tax"),
		CHANNEL_DISCOUNT_CODE("/webmessages/channel/discount/discountCode/%s/channelId/%d");

		private String value;

		ChannelNotificationEnum(String value) {
			this.value = value;
		}

		@Override
		public String getStringValue() {
			return this.value;
		}

		@Override
		public int getValue() {
			throw new UnsupportedOperationException();
		}

		public static Set<ChannelNotificationEnum> getChannelNotification() {
			return EnumSet.of(
					CHANNEL_FEE_TAX,
					CHANNEL_ACTIVATION_DEACTIVATION,
					CHANNEL_CREATE_OR_UPDATE_LISTING,
					CHANNEL_STATIC_DATA,
					CHANNEL_CREATE_OR_UPDATE_IMAGE,
					CHANNEL_DELETE_LISTING,
					CHANNEL_MAX_PERSON,
					CHANNEL_RESTRICTION,
					CHANNEL_RESERVATION,
					CHANNEL_RATE,
					CHANNEL_CALENDAR
			);
		}
	}

	public enum ChannelNotificationType implements Valued {
		UPDATE("Update"), DELETE("delete"), CREATE("Create");

		private String value;

		ChannelNotificationType(String value) {
			this.value = value;
		}

		@Override
		public String getStringValue() {
			return this.value;
		}

		@Override
		public int getValue() {
			throw new UnsupportedOperationException();
		}

	}

	public enum PriceType implements Valued {
		LOCAL(0), LIVE(1);
		private int value;

		PriceType(Integer value) {
			this.value = value;
		}

		@Override
		public int getValue() {
			return this.value;
		}

		@Override
		public String getStringValue() {
			throw new UnsupportedOperationException();
		}
	}

	;

	public static <E extends Enum<E>> E getValuedEnum(Class<E> clazz, int value) {
		for (E en : EnumSet.allOf(clazz)) {
			if (((Valued) en).getValue() == value) {
				return en;
			}
		}
		throw new IllegalArgumentException("Can not get correct Enum for class " + clazz + " with value " + value);
	}

	public static <E extends Enum<E>> E getValuedEnum(Class<E> clazz, String value) {
		for (E en : EnumSet.allOf(clazz)) {
			if (((Valued) en).getStringValue().equalsIgnoreCase(value)) {
				return en;
			}
		}
		throw new IllegalArgumentException("Can not get correct Enum for class " + clazz + " with value " + value);
	}

	public static <E extends Enum<E>> E getEnum(Class<E> clazz, String name) {
		for (E en : EnumSet.allOf(clazz)) {
			if (en.name().equalsIgnoreCase(name)) {
				return en;
			}
		}
		throw new IllegalArgumentException("Can not get correct Enum for class " + clazz + " with value " + name);
	}

	public static <E extends Enum<E>> E getValuedEnumNullSafe(Class<E> clazz, String value) {
		for (E en : EnumSet.allOf(clazz)) {
			if (((Valued) en).getStringValue().equalsIgnoreCase(value)) {
				return en;
			}
		}
		return null;
	}

	public static <E extends Enum<E>> E getValuedEnumNullSafe(Class<E> clazz, int value) {
		for (E en : EnumSet.allOf(clazz)) {
			if (((Valued) en).getValue() == value) {
				return en;
			}
		}
		return null;
	}

	public enum EntityType implements Valued {
		//TODO Add necessary type in near future.
		RATE_PLAN("Rate Plan"),
		PRICE("Price"),
		RESTRICTION("Restriction"),
		CANCELLATION_RULE("Cancellation rule"),
		RATE_ITEMS("Rate plan items"),
		PRODUCT("Product"),
		RESERVATION("Reservation"),
		TAX("Tax"),
		FEE("Fee"),
		YIELD("Yield"),
		COMMISSION("Commission"),
		INFO("Info"),
		PROCESS("Process");

		EntityType(String value) {
			this.value = value;
		}

		private String value;

		@Override
		public int getValue() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getStringValue() {
			return value;
		}
	}

	public enum AttributeDisplayCategory {
		ACTIVITIES("Activities"), OUTDOOR_VIEW("Outdoor & view"), FOOD_DRINK("Food and Drink"), SHOPS("Shops"),
		SERVICES_EXTRAS("Services & extras"), POOL_SPA("Pool and Spa"), TRANSPORTATION("Transportation"),
		FRONT_DESK("Front Desk Services"), MISCELLANEOUS("Miscellaneous"), ROOM(	"Room amenities"), COMMON_AREAS("Common areas"),
		ENTARTAIMENT_FAMILY("Entertainment and Family Services"), CLEANING("Cleaning Services"), BUSINESS("Business Facilities"),
		ACCESSIBILITY("Accessibility"), BATHROOM("Bathroom"), MEDIA("Media & technology"), POLICY("Policy"),
		PROPERTY_TYPE("Property Type"), BED_TYPE("Bed Type"), ROOM_TYPE("Room type"), KEY_COLLECTION("Key Collection");

		private String value;

		AttributeDisplayCategory(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public enum PendingPayoutStatus implements Valued {
		INITIAL(1, "Initial"), PENDING(2, "Pending"), APPROVED(3, "Approved"), CANCELLED(4, "Cancelled"), NOT_RECONCILED(5, "Not reconciled"), NOT_SHOW(6, "Not show"), ALREADY_APPROVED(7, "Already approved");

		private int value;
		private String name;

		PendingPayoutStatus(Integer value, String name) {
			this.value = value;
			this.name = name;
		}

		@Override
		public String getStringValue() {
			return name;
		}

		@Override
		public int getValue() {
			return value;
		}

		public static Set<PendingPayoutStatus> getTypeValues() {
			return EnumSet.of(INITIAL, PENDING, APPROVED, CANCELLED, NOT_RECONCILED, NOT_SHOW, ALREADY_APPROVED);
		}
	}

	public enum BestStayzMessagingStatus implements Valued {
		NEW(1, "New"), VIEWED(2, "Viewed"), ASSIGNED(4, "Assigned"), REPLIED(3, "Replied"), FORWARDED(5, "Forwarded"), UNASSIGNED(6, "Unassigned");

		private int value;
		private String name;

		BestStayzMessagingStatus(Integer value, String name) {
			this.value = value;
			this.name = name;
		}

		@Override
		public String getStringValue() {
			return name;
		}

		@Override
		public int getValue() {
			return value;
		}

		public static Set<BestStayzMessagingStatus> getTypeValues() {
			return EnumSet.of(NEW, VIEWED, ASSIGNED, REPLIED, FORWARDED, UNASSIGNED);
		}
	}

	public enum BestStayzHomeownerDocument implements Valued {
		DRIVER_LICENSE(1), IDENTIFICATION_CARD(2);

		private int value;

		BestStayzHomeownerDocument(Integer value) {
			this.value = value;
		}

		@Override
		public String getStringValue() {
			return String.valueOf(value);
		}

		@Override
		public int getValue() {
			return value;
		}

		public static Set<BestStayzHomeownerDocument> getTypeValues() {
			return EnumSet.of(DRIVER_LICENSE, IDENTIFICATION_CARD);
		}
	}

	public enum ChannelProductSettingsType implements Valued{
		PASS_THROUGH_TAXES_COLLECTION_TYPE(1),
		PM_CONFIRMATION_TO_USE_NEW_TAX_API(2);

		private int value;

		ChannelProductSettingsType(Integer value) {
			this.value = value;
		}

		@Override
		public int getValue() {
			return value;
		}

		@Override
		public String getStringValue() {
			throw new UnsupportedOperationException();
		}

		public static Set<ChannelProductSettingsType> getTypeValues() {
			return EnumSet.of(PASS_THROUGH_TAXES_COLLECTION_TYPE, PM_CONFIRMATION_TO_USE_NEW_TAX_API);
		}
	}


	public enum BestStayzHomeownerVerifyStatus implements Valued {
		PENDING(1, "Pending verification"), APPROVED(2, "Reviewed - Approved"), REJECTED(3, "Reviewed - Rejected");

		private int value;
		private String name;

		BestStayzHomeownerVerifyStatus(Integer value, String name) {
			this.value = value;
			this.name = name;
		}

		@Override
		public String getStringValue() {
			return name;
		}

		@Override
		public int getValue() {
			return value;
		}

		public static Set<BestStayzHomeownerVerifyStatus> getTypeValues() {
			return EnumSet.of(PENDING, APPROVED, REJECTED);
		}
	}

	public enum ChannelAffiliateStatus implements Valued {
		CREATED(1, "Created"), CANCELLED(2, "Cancelled");

		private int value;
		private String name;

		ChannelAffiliateStatus(Integer value, String name) {
			this.value = value;
			this.name = name;
		}

		@Override
		public String getStringValue() {
			return name;
		}

		@Override
		public int getValue() {
			return value;
		}

		public static Set<ChannelAffiliateStatus> getTypeValues() {
			return EnumSet.of(CREATED, CANCELLED);
		}
	}

	public enum FailedPaymentSupportLetterType implements Valued {
		BALANCE_PAYMENT("balancePayment"),
		CANCELLED_RESERVATION("cancelledReservation"),
		ACCEPTED_PAYMENT("acceptedPayment");

		private String value;

		FailedPaymentSupportLetterType(String value) {
			this.value = value;
		}

		@Override
		public String getStringValue() {
			return this.value;
		}

		@Override
		public int getValue() {
			throw new UnsupportedOperationException();
		}
	}


	public enum  PaymentMethodType implements Valued {
		MAIL(1, "mail"), PAYPAL(2, "paypal"), BANK(3, "bank"), WIRE(4, "wire");

		private int value;
		private String name;

		PaymentMethodType(Integer value, String name) {
			this.value = value;
			this.name = name;
		}

		@Override
		public String getStringValue() {
			return name;
		}

		@Override
		public int getValue() {
			return value;
		}

		public static Set<PaymentMethodType> getTypeValues() {
			return EnumSet.of(PAYPAL, BANK, WIRE, MAIL);
		}
	}

	public enum PMConfirmationToUseTaxEligibility implements Valued {
		NOT_CONFIRMED(0), CONFIRMED(1);

		private int value;

		PMConfirmationToUseTaxEligibility(Integer value) {
			this.value = value;
		}

		@Override public int getValue() {
			return value;
		}

		@Override public String getStringValue() {
			throw new UnsupportedOperationException();
		}

		public static Set<PMConfirmationToUseTaxEligibility> getTypeValues() {
			return EnumSet.of(NOT_CONFIRMED, CONFIRMED);
		}

	}

	public enum AirbnbTaxEligibilityLevel implements Valued{
		INELIGIBLE("INELIGIBLE", 1),
		NO_AIRBNB_COLLECTED_TAX("NO_AIRBNB_COLLECTED_TAX", 2),
		OVERRIDE_AIRBNB_COLLECTED_TAX("OVERRIDE_AIRBNB_COLLECTED_TAX",3),
		STACKED_AIRBNB_COLLECTED_TAX("STACKED_AIRBNB_COLLECTED_TAX",4);

		private int code;
		private String airbnbName;


		AirbnbTaxEligibilityLevel(String airbnbName,  Integer code) {
			this.airbnbName = airbnbName;
			this.code = code;
		}

		@Override public int getValue() {
			return code;
		}

		@Override public String getStringValue() {
			return airbnbName;
		}

		public static Set<AirbnbTaxEligibilityLevel> getTypeValues() {
			return EnumSet.of(INELIGIBLE, NO_AIRBNB_COLLECTED_TAX, OVERRIDE_AIRBNB_COLLECTED_TAX, STACKED_AIRBNB_COLLECTED_TAX);
		}

	}

	public enum  ZendeskCustomFieldEnum implements Valued {
		PM_ID("25496826", "PM ID"), PROPERTY_ID("25496846", "Property ID"), MOR("360011104471", "MOR");

		private int value;
		private String name;
		private String fieldId;

		ZendeskCustomFieldEnum(String fieldId, String name) {
			this.fieldId = fieldId;
			this.name = name;
		}

		@Override
		public String getStringValue() {
			return name;
		}

		@Override
		public int getValue() {
			return BigDecimalExt.ZERO.intValue();
		}

		public String getFieldId() {
			return fieldId;
		}
	}

	public enum BestStayzHomeownerAgent implements Valued {
		ONBOARDING_PERSON("OnboardingPerson"),
		ACCOUNT_MANAGER("AccountManager"),
		REGIONAL_MANAGER("RegionalManager"),
		SALES_REP("SalesRep");

		private String value;

		BestStayzHomeownerAgent(String value) {
			this.value = value;
		}

		@Override
		public int getValue() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getStringValue() {
			return value;
		}
	}

	public enum BestStayzHomeownerStep implements Valued {
		PRODUCT_CREATION("1"),
		TERMS_AND_CONDITIONS("2"),
		EMAIL_CONFIRMATION("3"),
		ALL("all");

		private String value;

		BestStayzHomeownerStep(String value) {
			this.value = value;
		}

		@Override
		public int getValue() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getStringValue() {
			return value;
		}

		public static Set<BestStayzHomeownerStep> getAllStep() {
			return EnumSet.of(PRODUCT_CREATION, TERMS_AND_CONDITIONS, EMAIL_CONFIRMATION);
		}
	}

	public enum  ProductBedroomTypes implements Valued {
		BEDROOM("Bedroom"), LIVING_ROOM("Living Room");

		private String value;

		ProductBedroomTypes(String value) {
			this.value = value;
		}

		@Override
		public String getStringValue() {
			return value;
		}

		@Override
		public int getValue() {
			throw new UnsupportedOperationException();
		}
	}

	public enum  EntityTypeUserNotification implements Valued {
		RESERVATION(1, "reservation"), PRODUCT(2, "product");

		private int value;
		private String name;

		EntityTypeUserNotification(Integer value, String name) {
			this.value = value;
			this.name = name;
		}

		@Override
		public String getStringValue() {
			return name;
		}

		@Override
		public int getValue() {
			return value;
		}
	}

	public enum MessageUserNotification implements Valued {
		NEW_RESERVATION("New %s reservation of %s for %s - %s from %s."),
		MODIFIED_RESERVATION("Reservation %s for %s was modified."),
		CANCELLED_RESERVATION("Reservation %s for %s was cancelled."),
		PRODUCT_STATUS_CHANGED("Product %s state was changed from %s -> %s. Reason: %s");


		private String name;

		MessageUserNotification(String name) {
			this.name = name;
		}

		@Override
		public String getStringValue() {
			return name;
		}

		@Override
		public int getValue() {
			throw new UnsupportedOperationException();
		}
	}

	public enum ProductState implements Valued {
		INITIAL("Initial"),
		ON_HOLD("OnHold"),
		INCOMPLETE("Incomplete"),
		COMPLETE("Complete"),
		DISTRIBUTED("Distributed"),
		ARCHIVED("Archived"),
		INREVIEW("InReview");

		private String value;

		ProductState(String value) {
			this.value = value;
		}

		@Override
		public int getValue() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getStringValue() {
			return value;
		}
	}

	public enum ReservationImportType implements Valued {
		Link(1, "Link"), File(2, "File");

		private int value;
		private String name;

		ReservationImportType(int value, String name) {
			this.value = value;
			this.name = name;
		}

		@Override
		public int getValue() {
			return value;
		}

		@Override
		public String getStringValue() {
			return name;
		}
	}

	public enum LogEmailEntityIdEnum {
		Reservation(1, "Reservation"), GroupReservation(2, "Group reservation");

		private Integer value;
		private String name;

		LogEmailEntityIdEnum(Integer value, String name) {
			this.value = value;
			this.name = name;
		}

		public Integer getValue() {
			return this.value;
		}

		public String getName() {
			return this.name;
		}

		public static LogEmailEntityIdEnum getByInt(Integer value) {
			switch (value) {
			case 1:
				return Reservation;
			case 2:
				return GroupReservation;
			default:
				return null;
			}
		}
	}

	public enum HomeownerPhotoType implements Valued {
		ORIGINAL(1, "Original"), TEMPORARY(2, "Temporary");

		private int value;
		private String name;

		HomeownerPhotoType(int value, String name) {
			this.value = value;
			this.name = name;
		}

		@Override
		public int getValue() {
			return value;
		}

		@Override
		public String getStringValue() {
			return name;
		}

		public static Set<HomeownerPhotoType> getTypeValues() {
			return EnumSet.of(ORIGINAL, TEMPORARY);
		}
	}

	public enum ResidencyCategoryEnum {
		PRIMARY_RESIDENCE(0, "primary_residence"), SECONDARY_RESIDENCE(1, "secondary_residence"), NON_RESIDENTIAL(2,"non_residential");

		private Integer id;
		private String name;

		ResidencyCategoryEnum(Integer id, String name) {
			this.id = id;
			this.name = name;
		}

		public Integer getId() {
			return this.id;
		}

		public String getStringValue() {
			return this.name;
		}

		public static Set<ResidencyCategoryEnum> getTypeValues() {
			return EnumSet.of(PRIMARY_RESIDENCE, SECONDARY_RESIDENCE, NON_RESIDENTIAL);
		}

		public static ResidencyCategoryEnum getByString(String value) {
			return Arrays.stream(ResidencyCategoryEnum.values())
					.filter(residencyCategoryEnum -> residencyCategoryEnum.getStringValue() != null && residencyCategoryEnum.getStringValue().equals(value))
					.findFirst().orElse(null);
		}

		public static ResidencyCategoryEnum getResidencycategoryById(Integer id) {
			return Arrays.stream(ResidencyCategoryEnum.values())
					.filter(residencyCategoryEnum -> residencyCategoryEnum.getId() != null && residencyCategoryEnum.getId().equals(id)).findFirst()
					.orElse(null);
		}

	}

	public enum PostalAddressPart {
		City("city"), Address("address"), State("state");

		PostalAddressPart(String name) {
			this.name = name;
		}

		private String name;

		public String getName() {
			return name;
		}
	}

	public enum PortalType {
		MARRIOTT(0), PM_PORTAL(1), CHANNEL_PORTAL(2);

		PortalType(Integer id) {
			this.id = id;
		}

		private Integer id;

		public Integer getId() {
			return id;
		}
	}

	public enum PromotionTargetChannel {
		ALGERIA_POS(1, "algeria_pos"), ARGENTINA_POS(2, "argentina_pos"), AUSTRALIA_POS(3, "australia_pos"), BELARUS_POS(4, "belarus_pos"),
		BRAZIL_POS(5, "brazil_pos"), CANADA_POS(6, "canada_pos"), CHILE_POS(7, "chile_pos"), CHINA_POS(8, "china_pos"), COLOMBIA_POS(9,"colombia_pos"),
		DOMESTIC_POS(10, "domestic_pos"), EU_POS(11, "eu_pos"), HONG_CONG_POS(12, "hong_kong_pos"), INDIA_POS(13, "india_pos"),
		INDONESIA_POS(14, "indonesia_pos"), INTERNATIONAL_POS(15, "international_pos"), IRAN_POS(16, "iran_pos"), ISRAEL_POS(17, "israel_pos"),
		JAPAN_POS(18, "japan_pos"), KAZAKHSTAN_POS(19, "kazakhstan_pos"), KUWAIT_POS(20, "kuwait_pos"), MALAYSIA_POS(21, "malaysia_pos"),
		MEXICO_POS(22, "mexico_pos"), NEW_ZEALAND_POS(23, "new_zealand_pos"), OMAN_POS(24, "oman_pos"), PAKISTAN_POS(25, "pakistan_pos"),
		PERU_POS(26, "peru_pos"), PHILIPPINES_POS(27, "philippines_pos"), QATAR_POS(28, "qatar_pos"), RUSSIA_POS(29, "russia_pos"),
		SAUDI_ARABIA_POS(30, "saudi_arabia_pos"), SINGAPORE_POS(31, "singapore_pos"), SOUTH_AFRICA_POS(32, "south_africa_pos"),
		SOUTH_KOREA_POS(33, "south_korea_pos"), SWITZERLAND_POS(34, "switzerland_pos"), TAIWAN_POS(35, "taiwan_pos"),
		THAILAND_POS(36, "thailand_pos"), TRINIDAD_TOBAGO_POS(37, "trinidad_&_tobago_pos"), TURKEY_POS(38, "turkey_pos"),
		UKRAINE_POS(39, "ukraine_pos"), UNITED_ARAB_EMIRATES_POS(40, "united_arab_emirates_pos"), UNITED_STATES_POS(41, "united_states_pos"),
		VIETNAM_POS(42, "vietnam_pos"), APP(43, "app"), ALL(44, "all");

		private Integer id;
		private String name;

		PromotionTargetChannel(Integer id, String name) {
			this.id = id;
			this.name = name;
		}

		public Integer getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public static PromotionTargetChannel getById(Integer id) {
			return Arrays.stream(PromotionTargetChannel.values()).filter(targetChannel -> targetChannel.getId().equals(id)).findFirst().orElse(null);
		}

		public static PromotionTargetChannel getByName(String name) {
			return Arrays.stream(PromotionTargetChannel.values()).filter(targetChannel -> targetChannel.getName().equals(name)).findFirst().orElse(null);
		}
	}

	public enum ReservationLogActivity {
		CREATE("Creation"),
		MODIFY("Modification"),
		PAYMENT_PROCESS("PaymentProcess"),
		CANCEL("Cancellation");

		ReservationLogActivity(String name) {
			this.name = name;
		}

		private String name;

		public String getName() {
			return name;
		}
	}
}
