package com.mybookingpal.dal.server.util;

public class ExceptionUtils {
	public static void throwExceptionIfNull(Object value, String name) {
		if (value == null) {
			throw new IllegalArgumentException(name + " is null");
		}
	}
}
