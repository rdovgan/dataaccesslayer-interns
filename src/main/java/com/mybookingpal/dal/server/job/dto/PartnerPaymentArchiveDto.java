package com.mybookingpal.dal.server.job.dto;

public class PartnerPaymentArchiveDto {
	private int reservationId;
	private String partnerPaymentModel;
	private String agentUserType;

	public int getReservationId() {
		return reservationId;
	}

	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}

	public String getPartnerPaymentModel() {
		return partnerPaymentModel;
	}

	public void setPartnerPaymentModel(String partnerPaymentModel) {
		this.partnerPaymentModel = partnerPaymentModel;
	}

	public String getAgentUserType() {
		return agentUserType;
	}

	public void setAgentUserType(String agentUserType) {
		this.agentUserType = agentUserType;
	}
}
