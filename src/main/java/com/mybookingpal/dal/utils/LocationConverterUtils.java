package com.mybookingpal.dal.utils;

import com.mybookingpal.dal.shared.Location;
import com.mybookingpal.dataaccesslayer.entity.LocationModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Scope("prototype")
public class LocationConverterUtils {
	
	public Location convert(LocationModel model) {
		if (model == null) {
			return null;
		}
		return new Location(model);
	}
	
	public LocationModel convert(Location location) {
		if (location == null) {
			return null;
		}
		LocationModel model = new LocationModel();
		model.setId(NumberUtils.createInteger(location.getId()));
		model.setName(location.getName());
		model.setState(location.getState());
		model.setGName(location.getGname());
		model.setCode(location.getCode());
		model.setCountry(location.getCountry());
		model.setRegion(location.getRegion());
		model.setAdminAreaLvl1(location.getAdminarea_lvl_1());
		model.setAdminAreaLvl2(location.getAdminarea_lvl_2());
		model.setArea(location.getArea());
		model.setLocationType(location.getLocationtype());
		model.setIata(location.getIata());
		model.setNotes(location.getNotes());
		model.setCodeInterHome(location.getCodeinterhome());
		model.setCodeRentalsUnited(location.getCoderentalsunited());
		model.setLatitude(location.getLatitude());
		model.setLongitude(location.getLongitude());
		model.setAltitude(location.getAltitude());
		model.setParentId(location.getParentID());
		model.setZipCode(location.getZipCode());
		model.setTimeZoneId(location.getTimeZoneId());
		return model;
	}

	public List<Location> convert(List<LocationModel> list) {
		if (CollectionUtils.isEmpty(list)) {
			return new ArrayList<>();
		}
		return list.stream().map(this::convert).collect(Collectors.toList());
	}
	
}
