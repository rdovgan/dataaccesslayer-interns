package com.mybookingpal.dal.utils;

import com.mybookingpal.dal.shared.NameIdUtils;
import com.mybookingpal.dal.shared.Yield;
import com.mybookingpal.utils.entity.NameId;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
@Scope("prototype")
public class YieldUtils {

	public BigDecimalExt getWeekendOrLengthOfStayOrDateRangeAmountYieldsValue(List<Yield> yieldRules, BigDecimalExt rackRate, LocalDate checkInDate, Integer stay) {
		return yieldRules.stream().filter(yield -> (yield.getName().contains(Yield.WEEKEND) && yield.isWeekendDate(checkInDate, yield.getParam())) || (
				Yield.LENGTH_OF_STAY.equalsIgnoreCase(yield.getName()) && yield.isLengthOfStay(stay)) || (Yield.DATE_RANGE.equalsIgnoreCase(yield.getName()) && yield.isInDateRange(checkInDate)))
				.map(y -> y.getValue(rackRate).subtract(rackRate)).reduce(BigDecimalExt.ZERO, BigDecimalExt::add);
	}

	public List<Yield> defineWeekendAndDateRangeYields(List<Yield> yieldRules, LocalDate checkInDate, Predicate<Yield> predicate) {
		return yieldRules.stream().filter(predicate)
				.filter(yield -> !checkInDate.isBefore(yield.getFromDate()) && !checkInDate.isAfter(yield.getToDate()))
				.filter(yield -> yield.getName().contains(Yield.WEEKEND) || yield.getName().contains(Yield.DATE_RANGE)).collect(Collectors.toList());
	}

	public List<Yield> defineLosYields(List<Yield> yieldRules, LocalDate checkInDate, Predicate<Yield> predicate, Integer stay) {
		Integer maxParamValue = yieldRules.stream().filter(yield -> yield.getName().contains(Yield.LENGTH_OF_STAY))
				.filter(yield -> !checkInDate.isBefore(yield.getFromDate()) && !checkInDate.isAfter(yield.getToDate()))
				.filter(yield -> yield.getParam() <= stay).max(Comparator.comparingInt(Yield::getParam)).map(Yield::getParam).orElse(0);
		return yieldRules.stream().filter(yield -> yield.getName().contains(Yield.LENGTH_OF_STAY))
				.filter(yield -> yield.getParam() != null && yield.getParam() != null && yield.getParam() <= stay).filter(yield -> maxParamValue.equals(yield.getParam())).filter(predicate)
				.filter(yield -> !checkInDate.isBefore(yield.getFromDate()) && !checkInDate.isAfter(yield.getToDate())).collect(Collectors.toList());
	}

	public Yield defineChannelMarkupYield(List<Yield> yieldRules, LocalDate checkInDate) {
		Optional<Yield> productMarkup = defineChannelMarkupByType(yieldRules, checkInDate, NameIdUtils.Type.Product.name());
		return productMarkup.orElse(defineChannelMarkupByType(yieldRules, checkInDate, NameIdUtils.Type.Party.name()).orElse(null));
	}

	private Optional<Yield> defineChannelMarkupByType(List<Yield> yieldRules, LocalDate checkInDate, String type) {
		return yieldRules
				.stream()
				.filter(y -> !checkInDate.isBefore(y.getFromDate()) && !checkInDate.isAfter(y.getToDate()))
				.filter(y -> Yield.CHANNEL_MARKUP.equalsIgnoreCase(y.getName()))
				.filter(y -> type.equalsIgnoreCase(y.getEntitytype()))
				.findFirst();
	}

	public Predicate<Yield> getYieldTypesPredicate(List<String> yieldTypes) {
		return yield -> yieldTypes.stream().anyMatch(yieldType -> yield.getName().contains(yieldType));
	}

	public Predicate<Yield> getFlatYieldPredicate(String increaseAmount, String decreaseAmount) {
		return yield -> yield.hasModifier(increaseAmount) || yield.hasModifier(decreaseAmount);
	}

	public Predicate<Yield> getPercentYieldPredicate(String increasePercent, String decreasePercent) {
		return yield -> yield.hasModifier(increasePercent) || yield.hasModifier(decreasePercent);
	}

	public String getWeekendOrLengthOfStayYieldsIds(List<Yield> yieldRules, LocalDate localDate, Integer stay) {
		return yieldRules.stream().filter(y -> !localDate.isBefore(y.getFromDate()) && !localDate.isAfter(y.getToDate()))
				.filter(getYieldTypesPredicate(Arrays.asList(Yield.WEEKEND, Yield.LENGTH_OF_STAY)))
				.filter(yield -> yield.isWeekend(localDate, yield.getParam()) || (Yield.LENGTH_OF_STAY.equalsIgnoreCase(yield.getName()) && yield.isLengthOfStay(stay)))
				.map(NameId::getId).collect(Collectors.joining(", "));
	}

	public boolean checkDayOnPromotionDateRangeWithIncludedAndExcludedDays(LocalDate day, Yield promotion) {
		if (promotion == null || day == null) {
			return false;
		}
		if (promotion.getAdditionalDates() == null && promotion.getExcludeDates() == null) {
			return promotion.isInDateRange(CommonDateUtils.toDate(day));
		}
		List<LocalDate> includedDays = defineIncludedDays(promotion);
		return includedDays.contains(day);
	}

	private List<LocalDate> defineIncludedDays(Yield promotion) {
		List<LocalDate> daysList = CommonDateUtils.getLocalDateRange(promotion.getFromDate(), promotion.getToDate());
		List<LocalDate> additionalDates = listLocalDatesFromString(promotion.getAdditionalDates());
		List<LocalDate> excludedDates = listLocalDatesFromString(promotion.getExcludeDates());
		if (CollectionUtils.isEmpty(additionalDates) && CollectionUtils.isEmpty(excludedDates)) {
			return daysList;
		}
		daysList.addAll(additionalDates);
		daysList = daysList.stream().filter(day -> !excludedDates.contains(day)).collect(Collectors.toList());
		return daysList;
	}

	private List<LocalDate> listLocalDatesFromString(String dates) {
		try {
			return Arrays.stream(StringUtils.split(dates, ";")).map(CommonDateUtils::toLocalDate).collect(Collectors.toList());
		} catch (Exception ignored) {
			return new ArrayList<>();
		}
	}

	public List<Yield> filterYields(List<Yield> yieldRules, Integer channelId) {
		List<Yield> filteredYields = new ArrayList<>();
		for (Yield yield : yieldRules) {
			if (yield.getChannelID() == null || yield.getChannelID() == 0 || channelId.equals(yield.getChannelID())) {
				filteredYields.add(yield);
			}
		}
		return filteredYields;
	}
}
