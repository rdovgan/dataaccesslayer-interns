package com.mybookingpal.dal.utils;

import java.util.ArrayList;
import java.util.Collection;

/**
 * List class to overcome type erasure
 *
 * @param <T>
 */
public class GenericList<T> extends ArrayList<T> {
	private Class<T> genericType;

	public GenericList(Class<T> c) {
		this.genericType = c;
	}

	public GenericList(Collection<T> collection, Class<T> cls) {
		super(collection);
		this.genericType = cls;
	}

	public Class<T> getGenericType() {
		return genericType;
	}
}
