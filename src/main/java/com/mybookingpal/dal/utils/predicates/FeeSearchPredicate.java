package com.mybookingpal.dal.utils.predicates;

import com.mybookingpal.dal.shared.Fee;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;
import java.util.function.Predicate;

@Component
@Scope("prototype")
public class FeeSearchPredicate {

	public Predicate<Fee> getPredicate(Fee action, boolean special) {
		return byEntityType(action.getEntityType())
				.and(byProductId(NumberUtils.createInteger(action.getProductId())))
				.and(byPartyId(NumberUtils.createInteger(action.getPartyId())))
				.and(byFromDate(action.getFromDate()))
				.and(byToDate(action.getToDate()))
				.and(byTaxType(action.getTaxType()))
				.and(byUnit(action.getUnit()))
				.and(byName(action.getName()))
				.and(byRatePlan(action.getRatePlanId()))
				.and(byWeightAndSpecial(action.getWeight(), special));
	}

	private Predicate<Fee> isTaxable() {
		return f -> f.getTaxType() == Fee.TAXABLE;
	}

	private Predicate<Fee> byTaxType(Integer taxType) {
		return f -> taxType == null || taxType.equals(f.getTaxType());
	}

	private Predicate<Fee> byWeightAndSpecial(Integer weight, boolean special) {
		return f -> weight == null || f.getWeight() == null || (special ? (f.getWeight() > 0) : (weight.equals(f.getWeight())));
	}

	private Predicate<Fee> byPartyId(Integer partyId) {
		return f -> partyId == null || partyId.equals(f.getPartyId());
	}

	private Predicate<Fee> byFromDate(LocalDate fromDate) {
		return f -> fromDate == null || !fromDate.isAfter(f.getToDate());
	}

	private Predicate<Fee> byToDate(LocalDate toDate) {
		return f -> toDate == null || toDate.isAfter(f.getFromDate());
	}

	private Predicate<Fee> byEntityType(int entityType) {
		return f -> f.getEntityType() == entityType;
	}

	private Predicate<Fee> byUnit(Integer unit) {
		return f -> unit == null || unit.equals(f.getUnit());
	}

	private Predicate<Fee> byName(String name) {
		return f -> name == null || name.equalsIgnoreCase(f.getName());
	}

	private Predicate<Fee> byProductId(Integer productId) {
		return f -> productId == null || productId.equals(f.getProductId());
	}

	private Predicate<Fee> hasRatePlan() {
		return f -> f.getRatePlanId() != null;
	}

	private Predicate<Fee> byRatePlan(Long ratePlanId) {
		return f -> Objects.equals(ratePlanId, f.getRatePlanId());
	}

}
