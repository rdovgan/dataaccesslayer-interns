package com.mybookingpal.dal.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DateFormatUtils {

	private static final ThreadLocal<DateFormat> TIME_FORMAT = ThreadLocal.withInitial(() -> new SimpleDateFormat("HH:mm"));
	private static final ThreadLocal<DateFormat> DATE_FORMAT = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd"));
	private static final ThreadLocal<DateFormat> DATE_TIME_FORMAT = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

	public static DateFormat getTimeFormat() {
		return TIME_FORMAT.get();
	}

	public static DateFormat getDateFormat() {
		return DATE_FORMAT.get();
	}

	public static DateFormat getDateTimeFormat() {
		return DATE_TIME_FORMAT.get();
	}

}
