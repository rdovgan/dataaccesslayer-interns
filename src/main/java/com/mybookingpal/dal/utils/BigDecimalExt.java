package com.mybookingpal.dal.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import org.springframework.stereotype.Service;

/**
 * 
 * @author jinalshah
 * Commented out @Service tab because of compilation error 
 * java.beans.IntrospectionException: Type mismatch between indexed and non-indexed methods: null - public 
 */

public class BigDecimalExt extends BigDecimal {

	public static final BigDecimalExt ZERO = new BigDecimalExt(BigDecimal.ZERO);
	public static final BigDecimalExt ONE = new BigDecimalExt(BigDecimal.ONE);
	public static final BigDecimalExt TEN = new BigDecimalExt(BigDecimal.TEN);
	public static final BigDecimalExt ONE_HUNDRED = new BigDecimalExt(100);

	public BigDecimalExt() {
		super(BigInteger.ZERO);
	}

	public BigDecimalExt(BigDecimal value) {
		super(value.toPlainString());
	}

	public BigDecimalExt(String val) {
		super(val);
	}

	public BigDecimalExt(Integer value) {
		super(value);
	}
	public BigDecimalExt(Double value) {
		super(value, MathContext.DECIMAL64);
	}

	public BigDecimalExt(long val) {
		super(val, MathContext.DECIMAL64);
	}

	public BigDecimalExt divideByHundred() {
		return new BigDecimalExt(this.divide(ONE_HUNDRED));
	}

	public BigDecimalExt divideToIntegralValue(BigDecimal divisor) {
		return new BigDecimalExt(super.divideToIntegralValue(divisor));
	}

	public BigDecimalExt multiplyByHundred() {
		return this.multiply(ONE_HUNDRED);
	}

	public BigDecimalExt round() {
		return new BigDecimalExt(super.setScale(2, ROUND_HALF_UP));
	}

	public BigDecimalExt roundUp() {
		return new BigDecimalExt(super.setScale(2, ROUND_UP));
	}

	public BigDecimalExt roundHalfUp() {
		return new BigDecimalExt(super.setScale(2, ROUND_HALF_UP));
	}

	@Override
	public BigDecimalExt setScale(int newScale, int roundingMode) {
		return new BigDecimalExt(super.setScale(newScale, roundingMode));
	}

	@Override
	public BigDecimalExt setScale(int newScale, RoundingMode roundingMode) {
		return new BigDecimalExt(super.setScale(newScale, roundingMode));
	}

	@Override
	public BigDecimalExt add(BigDecimal augend) {
		return new BigDecimalExt(super.add(augend, MathContext.DECIMAL64));
	}

	@Override
	public BigDecimalExt subtract(BigDecimal subtrahend) {
		return new BigDecimalExt(super.subtract(subtrahend, MathContext.DECIMAL64));
	}

	@Override
	public BigDecimalExt multiply(BigDecimal multiplicand) {
		return new BigDecimalExt(super.multiply(multiplicand, MathContext.DECIMAL64));
	}

	@Override
	public BigDecimalExt divide(BigDecimal divisor) {
		return new BigDecimalExt(super.divide(divisor, MathContext.DECIMAL64));
	}

	@Override
	public BigDecimalExt divide(BigDecimal divisor, RoundingMode roundingMode) {
		return new BigDecimalExt(super.divide(divisor, roundingMode));
	}

	@Override
	public BigDecimalExt min(BigDecimal value) {
		return new BigDecimalExt(super.min(value));
	}

	@Override
	public BigDecimalExt max(BigDecimal value) {
		return new BigDecimalExt(super.max(value));
	}

	@Override
	public BigDecimalExt negate() {
		return new BigDecimalExt(super.negate());
	}

	@Override
	public BigDecimalExt abs() {
		return new BigDecimalExt(super.abs());
	}

	public boolean compareWithDelta(BigDecimal value) {
		BigDecimal x = this.setScale(3, RoundingMode.HALF_UP);
		return x.compareTo(value.setScale(3, RoundingMode.HALF_UP)) == 0;
	}

	public boolean greaterThanZero() {
		return compareTo(ZERO) > 0;
	}

	public boolean lessThanZero() {
		return compareTo(ZERO) < 0;
	}

	public boolean greaterThan(BigDecimalExt value) {
		return compareTo(value) > 0;
	}

	public boolean lessThan(BigDecimalExt value) {
		return compareTo(value) < 0;
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
