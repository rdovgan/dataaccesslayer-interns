package com.mybookingpal.dal.utils;

import java.math.BigDecimal;

public class AbstractUtils {

	protected Long setLongOrNull(Object value) {
		return value != null ? Long.parseLong(value.toString()) : null;
	}

	protected String setStringOrNull(Object value) {
		return value != null ? value.toString() : null;
	}

	protected Integer setIntegerOrNull(Object value) {
		return value != null ? Integer.parseInt(value.toString()) : null;
	}

	protected Double setDoubleOrNull(Object value) {
		return value != null ? Double.valueOf(value.toString()) : null;
	}

	protected BigDecimal setBigDecimalOrNull(Object value) {
		return value != null ? new BigDecimal(value.toString()) : null;
	}

}
