package com.mybookingpal.dal.utils;

import com.mybookingpal.dal.shared.api.HasDateRange;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import reactor.util.annotation.Nullable;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.Optional;

import static java.time.temporal.TemporalAdjusters.lastDayOfYear;

public class CommonDateUtils {

	@Deprecated
	/**
	 * Use {@link DateFormatUtils#getDateFormat()} as thread safe solution
	 */
	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	public static final String YYYY_MM_DD_HH_MIN_SEC = "yyyy-MM-dd HH:mm:ss";
	public static final String YYYY_MM_DD_HH_MIN = "yyyy-MM-dd HH:mm";
	public static final String YYYY_MM_DD_HH_MIN_SEC_MSEC = "yyyy-MM-dd HH:mm:ss.S";
	public static final String MMMM_DD = "MMMM dd";
	public static final String MMMM_YYYY = "MMMM yyyy";
	public static final String MMM_D_YYYY = "MMM dd, yyyy";
	public static final String MMMM_D_YYYY = "MMMM dd, yyyy";
	public static final String E_MMMM_D_YYYY = "E, MMMM dd, yyyy";
	public static final String HH_MIN_SEC = "HH:mm:ss";
	public static final String YYYY_MM_DD_HH_MM_AA = "yyyy-MM-dd hh:mm aa";
	public static final String E_DD_MMM_YYYY_HH_MM_SS_Z = "E, dd MMM yyyy HH:mm:ss Z";
	public static final String EEE_MMM_DD_HH_MM_SS_Z_YYYY = "EEE MMM dd HH:mm:ss Z yyyy";
	public static final String EEE_MMMM_DD_YYYY = "EEE, MMMM dd, yyyy";
	public static final String EEEE_MMMM_DD_YYYY = "EEEE, MMMM d, yyyy";
	public static final String MMMM_DD_YYYY = "MMMM d, yyyy";
	public static final String K_MM_A = "K:mma";
	public static final String VERSION_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String yyyyMMdd ="yyyyMMdd";
	public static final String M_DD_YY = "M/dd/yy";
	public static final SimpleDateFormat SDF_YYYY_MM_DD = new SimpleDateFormat(YYYY_MM_DD);
	public static final SimpleDateFormat DATE_FORMAT_WITH_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final String HOURS_AND_MINUTES = "HH:mm";
	public static final DateFormat TIME_FORMAT = new SimpleDateFormat(HOURS_AND_MINUTES);
	public static final DateFormat TIME_FORMAT_PM = new SimpleDateFormat("hh:mm aa");
	public static final java.time.format.DateTimeFormatter DATE_TIME_FORMATTER = java.time.format.DateTimeFormatter.ofPattern(YYYY_MM_DD);
	public static final java.time.format.DateTimeFormatter LOCAL_DATE_TIME_FORMATTER = java.time.format.DateTimeFormatter.ofPattern(YYYY_MM_DD_HH_MIN_SEC_MSEC);
	public static final java.time.format.DateTimeFormatter LOCAL_DATE_TIME_FORMATTER_WITHOUT_MSEC = java.time.format.DateTimeFormatter
			.ofPattern(YYYY_MM_DD_HH_MIN_SEC);

	public static final String MIN_DATE_STRING = "1970-01-01";
	public static final LocalDate MIN_LOCAL_DATE = toLocalDate(MIN_DATE_STRING);
	public static final Date MIN_DATE = parseWithDate(MIN_DATE_STRING);
	public static final String MAX_DATE_STRING = "3014-05-17";
	public static final LocalDate MAX_LOCAL_DATE = toLocalDate(MAX_DATE_STRING);
	public static final Date MAX_DATE = parseWithDate(MAX_DATE_STRING);

	public static final String UNITED_STATES_DATE_FORMAT = "MM/dd/yyyy";
	public static final java.time.format.DateTimeFormatter UNITED_STATES_DATE_FORMATTER =
			java.time.format.DateTimeFormatter.ofPattern(UNITED_STATES_DATE_FORMAT);
	public static final SimpleDateFormat SyyyyMMdd = new SimpleDateFormat(yyyyMMdd);
	public static final int HOURS_PER_DAY = 24;
	public static final int HOURS_PM = 12;

	/**
	 * Returns a list of LocalDate range. Params can be swapped.
	 */
	public static List<LocalDate> getLocalDateRange(LocalDate fromDate, LocalDate toDate) {
		int days = Math.abs((int)fromDate.until(toDate, ChronoUnit.DAYS));
		LocalDate startDate = fromDate.isBefore(toDate) ? fromDate : toDate;
		return Stream.iterate(startDate, d -> d.plusDays(1)).limit(days).collect(Collectors.toList());
	}

	public static List<LocalDate> getLocalDateRangeIncludingLastDay(LocalDate startDate, LocalDate endDate) {
		long numOfDaysBetween = CommonDateUtils.getDaysBetweenIncludingEnds(startDate, endDate);
		return IntStream.iterate(0, i -> i + 1).limit(numOfDaysBetween).mapToObj(startDate::plusDays).collect(Collectors.toList());
	}

	public static Set<Date> getDates(String startDt, String endDt) {
		return getDates(startDt, endDt, DateTimeFormat.forPattern(YYYY_MM_DD));
	}

	public static <T> Set<T> getDates(String startDt, String endDt, Class<T> type) {
		return getDates(startDt, endDt, DateTimeFormat.forPattern(YYYY_MM_DD), type);
	}

	public static Set<Date> getDates(String startDt, String endDt, org.joda.time.format.DateTimeFormatter dateTimeFormatter) {
		return getDates(dateTimeFormatter.parseDateTime(startDt), dateTimeFormatter.parseDateTime(endDt));
	}

	public static <T> Set<T> getDates(String startDt, String endDt, org.joda.time.format.DateTimeFormatter dateTimeFormatter, Class<T> type) {
		return getDates(dateTimeFormatter.parseDateTime(startDt), dateTimeFormatter.parseDateTime(endDt), type);
	}

	public static Set<Date> getDates(Date startDt, Date endDt) {
		return getDates(new DateTime(startDt), new DateTime(endDt));
	}

	public static <T> Set<T> getDates(Date startDt, Date endDt, Class<T> type) {
		return getDates(new DateTime(startDt), new DateTime(endDt), type);
	}

	public static Set<Date> getDates(DateTime start, DateTime end) {
		return getDates(start, end, Date.class);
	}

	@SuppressWarnings("unchecked")
	public static <T> Set<T> getDates(DateTime start, DateTime end, Class<T> type) {
		Set<T> bookedDates = new HashSet<T>();
		org.joda.time.format.DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(YYYY_MM_DD);
		if (type == DateTime.class) {
			for (DateTime itr = start; itr.isBefore(end); itr = itr.plusDays(1)) {
				bookedDates.add((T) itr);
			}
			bookedDates.add((T) end);
		} else if (type == Date.class) {
			for (DateTime itr = start; itr.isBefore(end); itr = itr.plusDays(1)) {
				bookedDates.add((T) itr.toDate());
			}
			bookedDates.add((T) end.toDate());
		} else if (type == String.class) {
			for (DateTime itr = start; itr.isBefore(end); itr = itr.plusDays(1)) {
				bookedDates.add((T) dateTimeFormatter.print(itr));
			}
			bookedDates.add((T) dateTimeFormatter.print(end));
		}
		return bookedDates;
	}

	public static List<Date> getDatesAsList(String startDt, String endDt) {
		org.joda.time.format.DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(YYYY_MM_DD);
		return getDatesAsList(dateTimeFormatter.parseDateTime(startDt), dateTimeFormatter.parseDateTime((endDt)), Date.class);
	}

	public static List<Date> getDatesAsList(Date startDt, Date endDt) {
		return getDatesAsList(new DateTime(startDt), new DateTime(endDt), Date.class);
	}

	public static <T> List<T> getDatesAsList(DateTime start, DateTime end, Class<T> type) {
		List<T> bookedDates = new ArrayList<T>();
		org.joda.time.format.DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(YYYY_MM_DD);
		if (type == DateTime.class) {
			for (DateTime itr = start; itr.isBefore(end); itr = itr.plusDays(1)) {
				bookedDates.add((T) itr);
			}
			bookedDates.add((T) end);
		} else if (type == Date.class) {
			for (DateTime itr = start; itr.isBefore(end); itr = itr.plusDays(1)) {
				bookedDates.add((T) itr.toDate());
			}
			bookedDates.add((T) end.toDate());
		} else if (type == String.class) {
			for (DateTime itr = start; itr.isBefore(end); itr = itr.plusDays(1)) {
				bookedDates.add((T) dateTimeFormatter.print(itr));
			}
			bookedDates.add((T) dateTimeFormatter.print(end));
		}
		return bookedDates;
	}

	public static long daysBetween(Date startDate, Date endDate) {
		long difference = endDate.getTime() - startDate.getTime();
		return TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);
	}

	public static long hoursBetween(Date startDate, Date endDate) {
		long difference = endDate.getTime() - startDate.getTime();
		return TimeUnit.HOURS.convert(difference, TimeUnit.MILLISECONDS);
	}

	public static long hoursBetween(LocalDateTime startDate, LocalDateTime endDate) {
		return ChronoUnit.HOURS.between(startDate, endDate);
	}

	public static long daysBetween(LocalDate startDate, LocalDate endDate) {
		return ChronoUnit.DAYS.between(startDate, endDate);
	}

	public static long getDurationByUnit(LocalDate fromDate, LocalDate toDate, ChronoUnit unit) {
		return unit == null ? 0L : unit.between(fromDate, toDate);
	}

	public static XMLGregorianCalendar getXMLDateFormat(Date date) throws Exception {
		return getXMLDateFormat(date, new GregorianCalendar());
	}

	public static XMLGregorianCalendar getXMLDateFormat(Date date, GregorianCalendar gcal) throws Exception {
		gcal.setTime(date);
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);

	}

	public static XMLGregorianCalendar getXMLDateFormat(String date) throws Exception {
		return getXMLDateFormat(SDF_YYYY_MM_DD.parse(date));
	}

	public static XMLGregorianCalendar getXMLCurrentDate() throws Exception {
		GregorianCalendar gcal = new GregorianCalendar();
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);
	}

	public static XMLGregorianCalendar toXMLGregorianCalendarDateOnly(Date date) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		XMLGregorianCalendar xc = null;
		try {
			xc = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH) + 1, gc.get(Calendar.DAY_OF_MONTH),
					DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return xc;
	}

	public static XMLGregorianCalendar toXMLGregorianCalendarWithoutTimeZone(Date date) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		XMLGregorianCalendar xc = null;
		try {
			xc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			xc.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
			xc.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return xc;
	}

	public static XMLGregorianCalendar toXMLGregorianCalendarWithoutMilliseconds(Date date) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		XMLGregorianCalendar xc = null;
		try {
			xc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			xc.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return xc;
	}

	public static String format(Date date, String format) {
		return new SimpleDateFormat(format).format(date);
	}

	public static String format(Date date) {
		return DateFormatUtils.getDateFormat().format(date);
	}

	public static String format(LocalDateTime localDateTime, String format) {
		return localDateTime.format(java.time.format.DateTimeFormatter.ofPattern(format));
	}

	public static String format(LocalDate localDate, String format) {
		java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern(format);
		return formatter.format(localDate);
	}

	public static String format(LocalTime localTime, String format) {
		java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern(format);
		return formatter.format(localTime);
	}

	@Deprecated
	/**
		Use {@link #format(Date)} instead
	 */
	public static String convertToString(Date date) {
		return format(date);
	}

	@Deprecated
	public static String convertToString(LocalDate date) {
		return format(date, YYYY_MM_DD);
	}

	public static String maxDateOfCurrentMonthToString() {
		LocalDate convertedDate = LocalDate.now();
		convertedDate = convertedDate.withDayOfMonth(convertedDate.getMonth().length(convertedDate.isLeapYear()));
		return convertedDate.format(DATE_TIME_FORMATTER);
	}

	public static String maxDateOfCurrentYearToString() {
		LocalDate convertedDate = LocalDate.now().with(lastDayOfYear());
		return convertedDate.format(DATE_TIME_FORMATTER);
	}

	public static String timeString(Date date) {
		return format(date, HH_MIN_SEC);
	}

	public static Date parse(String date, String format) {
		try {
			if (StringUtils.isBlank(date)) {
				return null;
			}

			return new SimpleDateFormat(format).parse(date);
		} catch (ParseException e) {
			return new Date();
		}
	}

	public static Date parseWithDateTime(String dateTimeWithString) {
		try {
			return DateFormatUtils.getDateTimeFormat().parse(dateTimeWithString);
		} catch (Exception e) {
			return parseWithDateTimeNotThreadSafe(dateTimeWithString);
		}
	}

	public static Date parseWithDate(String date) {
		return parse(date, YYYY_MM_DD);
	}

	public static Date parseWithDateRaw(String date) throws ParseException {
		return new SimpleDateFormat(YYYY_MM_DD).parse(date);
	}

	public static Date parseWithDateAndLenient(String date, Boolean lenient) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(YYYY_MM_DD);
		formatter.setLenient(lenient);
		return formatter.parse(date);
	}

	public static Date parseWithTime(String date) {
		return parse(date, HH_MIN_SEC);
	}

	public static String formatWithDateTime(Date date) {
		return convertToString(date) + "T" + timeString(date);
	}

	public static Date parseWithDateTimeNotThreadSafe(String date) {
		String raw = date.replace("T", " ");
		return parse(raw, YYYY_MM_DD + " " + HH_MIN_SEC);
	}

	public static boolean overlap(Date start1, Date end1, Date start2, Date end2) {
		return start1.getTime() <= end2.getTime() && start2.getTime() <= end1.getTime();
	}

	public static boolean notStrictOverlap(Date start1, Date end1, Date start2, Date end2) {
		return start1.getTime() < end2.getTime() && start2.getTime() < end1.getTime();
	}

	public static Date toDate(LocalDateTime localDateTime) {
		return localDateTime == null ? new Date() : toDate(localDateTime.atZone(ZoneId.systemDefault()));
	}

	public static Date minusDays(Date date, long days) {
		return toDate(CommonDateUtils.toLocalDate(date).minusDays(days));
	}

	public static Date plusDays(Date date, int days) {
		return toDate(toLocalDate(date).plusDays(days));
	}

	public static Date plusMonths(Date date, long months) {
		return toDate(toLocalDate(date).plusMonths(months));
	}

	public static Date max(Date... dates) {
		return Collections.max(Arrays.asList(dates));
	}

	public static LocalDate max(LocalDate... dates) {
		return Collections.max(Arrays.asList(dates));
	}

	public static Date min(Date... dates) {
		return Collections.min(Arrays.asList(dates));
	}

	public static LocalDate min(LocalDate... dates) {
		return Collections.min(Arrays.asList(dates));
	}

	public static Date defineCurrentDateAtStartOfDay() {
		LocalDateTime currentDateAtStartOfDay = toLocalDate(new Date()).atStartOfDay();

		return toDate(currentDateAtStartOfDay);
	}

	public static Date parseDateInIsoFormat(String dateInString) throws ParseException {
		final boolean isLenientOldValue = SDF_YYYY_MM_DD.isLenient();
		SDF_YYYY_MM_DD.setLenient(false);
		final Date date = SDF_YYYY_MM_DD.parse(dateInString);
		SDF_YYYY_MM_DD.setLenient(isLenientOldValue);
		return date;
	}

	public static String makeReadableFormat(LocalDateTime dateTime) {
		return toUnitedStatesFormat(dateTime.toLocalDate()) + " at " + toIsoLocalTimeFormat(dateTime.toLocalTime());
	}

	public static List<Date> toDates(List<LocalDate> localDates) {
		if (localDates == null) {
			return null;
		}
		return localDates.stream().map(CommonDateUtils::toDate).collect(Collectors.toList());
	}

	private static class DateComparator implements Comparator<Date> {
		protected static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

		@Override
		public int compare(Date d1, Date d2) {
			return DATE_FORMAT.format(d1).compareTo(DATE_FORMAT.format(d2));
		}
	}

	public static DateComparator DATE_COMPARATOR = new DateComparator();

	/**
	 * @param date is string representation of date
	 * @return reference to XMLGregorianCalendar object that holding that date inside
	 * @throws DatatypeConfigurationException if XMLGregorianCalendar can not be created
	 */
	public static XMLGregorianCalendar getXMLGregorianCalendar(Date date) {
		try {
			String dateInString = CommonDateUtils.format(date);
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(dateInString);
		} catch (DatatypeConfigurationException e) {
			throw new RuntimeException("Date \"" + date + "\" can not be converted to XMLGregorianCalendar.", e);
		}
	}

	public static XMLGregorianCalendar dateToXMLGregorianCalendar(Date date) {
		try {
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(date);
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException e) {
			throw new RuntimeException("Date \"" + date + "\" can not be converted to XMLGregorianCalendar.", e);
		}
	}

	public static Date getDate(XMLGregorianCalendar xmlGregorianCalendar) {
		if (xmlGregorianCalendar == null) {
			throw new IllegalArgumentException("xmlGregorianCalendar is null");
		}
		String dateInString = "" + xmlGregorianCalendar.getYear() + "-" + xmlGregorianCalendar.getMonth() + "-" + xmlGregorianCalendar.getDay();
		return parseWithDate(dateInString);
	}

	/**
	 * Defines difference between two dates in specified time unit
	 *
	 * @param beginDate - begin date
	 * @param endDate - end date
	 * @param timeUnit - time unit in which difference is required
	 * @return difference between two dates in timeUnit
	 */
	public static long defineDifferenceBetweenDates(Date beginDate, Date endDate, TimeUnit timeUnit) {
		long differenceInMilliseconds = endDate.getTime() - beginDate.getTime();
		return timeUnit.convert(differenceInMilliseconds, TimeUnit.MILLISECONDS);
	}

	/**
	 * Creates {@link LocalDate} from the date part of {@link Date}
	 */
	public static LocalDate toLocalDate(Date date) {
		if (null == date) {
			return null;
		}
		return new java.sql.Date(date.getTime()).toLocalDate();
	}

	public static Date toDate(LocalDate localDate) {
		if (null == localDate) {
			return null;
		}
		return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public static Date toDate(ZonedDateTime zonedDateTime) {
		return Date.from(zonedDateTime.toInstant());
	}

	public static String format(LocalDate localDate) {
		return DATE_TIME_FORMATTER.format(localDate);
	}

	public static String format(LocalDateTime localDateTime) {
		return LOCAL_DATE_TIME_FORMATTER.format(localDateTime);
	}

	public static LocalDate toLocalDate(@Nullable String date) {
		if (StringUtils.isBlank(date)) {
			return null;
		}
		return LocalDate.parse(date, DATE_TIME_FORMATTER);
	}

	public static String toUnitedStatesFormat(LocalDate localDate) {
		return UNITED_STATES_DATE_FORMATTER.format(localDate);
	}

	public static String toIsoLocalTimeFormat(LocalTime localTime) {
		return localTime.format(java.time.format.DateTimeFormatter.ofPattern(HH_MIN_SEC));
	}

	public static LocalDateTime toLocalDateTime(String date) {
		return LocalDateTime.parse(date, LOCAL_DATE_TIME_FORMATTER);
	}

	public static Date defineMinDate() {
		return MIN_DATE;
	}

	public static Date defineMaxDate() {
		return MAX_DATE;
	}

	public static long defineNumberOfDaysBetweenIncludingLastDay(LocalDate fromDate, LocalDate toDate) {
		return defineNumberOfDaysBetweenExcludingLastDay(fromDate, toDate) + 1;
	}

	public static long defineNumberOfDaysBetweenExcludingLastDay(LocalDate fromDate, LocalDate toDate) {
		return ChronoUnit.DAYS.between(fromDate, toDate);
	}

	public static long defineNumberOfDaysBetweenExcludingLastDay(Date fromDate, Date toDate) {
		return defineNumberOfDaysBetweenExcludingLastDay(toLocalDate(fromDate), toLocalDate(toDate));
	}

	public static LocalDateTime toLocalDateTime(Date date) {
		if (date == null) {
			return null;
		}
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	public static String formatDuration(long milliseconds, String format) {
		return DurationFormatUtils.formatDuration(milliseconds, format);
	}

	public static LocalDateTime setDateTime(Date date, Time time) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if (time != null) {
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			int month = calendar.get(Calendar.MONTH);
			int year = calendar.get(Calendar.YEAR);
			calendar.setTime(time);
			calendar.set(Calendar.DAY_OF_MONTH, day);
			calendar.set(Calendar.MONTH, month);
			calendar.set(Calendar.YEAR, year);
		}
		return toLocalDateTime(calendar.getTime());
	}

	public static boolean isBeforeOrEquals(Date date1, Date date2) {
		return date1 != null && date2 != null && (date1.before(date2) || date1.equals(date2));
	}

	public static boolean isAfterOrEquals(Date date1, Date date2) {
		return date1 != null && date2 != null && (date1.after(date2) || date1.equals(date2));
	}

	public static LocalDateTime getLocalDateTimeFromDate(Date date) {
		Instant instant = date.toInstant();
		return instant.atZone(ZoneOffset.UTC).toLocalDateTime();
	}

	public static LocalDate getLocalDateFromDate(Date date) {
		Instant instant = date.toInstant();
		return instant.atZone(ZoneOffset.UTC).toLocalDate();
	}

	public static boolean isTheSameDates(Date date, Date comparedTo) {
		return date != null && comparedTo != null && DATE_COMPARATOR.compare(date, comparedTo) == 0;
	}

	public static boolean isTheSameDates(LocalDate date, LocalDate comparedTo) {
		return date != null && date.equals(comparedTo);
	}

	public static String stringFromListDates(List<Date> dates) {
		if (dates == null) {
			return null;
		}
		return dates.stream().map(CommonDateUtils::convertToString).collect(Collectors.joining(";"));
	}

	public static Predicate<HasDateRange> getSameDatesPredicate(HasDateRange second) {
		return first -> isTheSameDates(first.getToDate(), second.getToDate())
				&& isTheSameDates(first.getFromDate(), second.getFromDate());
	}

	/**
	 * Convert a string to "hh:mm" if it is possible.
	 * @param time can be like "1", "1:", "1:1", "01:", "01:1", "1:1", "1:10", "12:"
	 * @return Returns formatted time string if the conversion is possible. Return the same string if the conversion is not possible.
	 */
	public static String convertToTimeFormat(String time) {
		if (StringUtils.isBlank(time)) {
			return time;
		} else {
			String t = time.trim();
			String h;
			String m;
			if (t.contains(":")) {
				h = StringUtils.substringBefore(t, ":");
				m = StringUtils.substringAfter(t, ":");
				if (!StringUtils.isNumeric(h)) {
					return time;
				}
			} else {
				h = t;
				m = "00";
			}
			if (h.length() == 1) {
				h = "0" + h;
			}
			if (StringUtils.isBlank(m)) {
				m = "00";
			} else if (m.length() == 1) {
				m = "0" + m;
			}
			return h + ":" + m;
		}
	}

	public static long getDaysBetweenIncludingEnds(LocalDate fromDate, LocalDate toDate) {
		return ChronoUnit.DAYS.between(fromDate, toDate) + 1;
	}

	/** Makes a calendar time to 00:00*/
	public static Calendar normalizeCalendar(Calendar cal) {
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 0, 0, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}

	/** Makes a dates time to 00:00*/
	public static Date normalizeDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		normalizeCalendar(cal);
		return cal.getTime();
	}

}
