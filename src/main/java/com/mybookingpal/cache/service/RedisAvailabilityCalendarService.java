package com.mybookingpal.cache.service;

import com.mybookingpal.cache.dto.FullAvailabilityItemForCaching;
import com.mybookingpal.redis.dto.RedisCacheException;
import com.mybookingpal.cache.utils.RedisAvailabilityUtils;
import com.mybookingpal.cache.utils.RedisAvailabilityObjectSerializer;
import com.mybookingpal.config.RazorConfig;
import com.mybookingpal.dal.shared.AvailabilityCalendarModel;
import com.mybookingpal.dal.utils.CommonDateUtils;
import com.mybookingpal.redis.client.RedisClientManager;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class RedisAvailabilityCalendarService {

	private static final Logger LOG = LoggerFactory.getLogger(RedisAvailabilityCalendarService.class.getName());

	@Autowired
	protected RedisAvailabilityUtils redisAvailabilityUtils;

	public void storeAvailabilityDataToCache(List<AvailabilityCalendarModel> availabilityCalendarModels) {
		try {
			if (!RedisClientManager.getRedisClient(RazorConfig.isProduction()).isAvailable()) {
				return;
			}
			availabilityCalendarModels.forEach(this::storeAvailabilityDataToCache);
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
	}

	private void storeAvailabilityDataToCache(AvailabilityCalendarModel availabilityCalendarModel) {
		String key = redisAvailabilityUtils.defineKeyForAvailabilityCalendar(availabilityCalendarModel);
		try {
			RedisClientManager.getRedisClient(RazorConfig.isProduction())
					.storeValueToRedisHash(key, CommonDateUtils.format(availabilityCalendarModel.getAvailabilityDate()),
							RedisAvailabilityObjectSerializer.serializeObjectToJson(new FullAvailabilityItemForCaching(availabilityCalendarModel)));
		} catch (RedisCacheException e) {
			throw new RuntimeException("Unable to store data to cache");
		}
	}

	/**
	 * Generate key with field and get data from cache
	 *
	 * @return deserialized data from cache if it present, or else `null` value
	 */
	public AvailabilityCalendarModel getAvailabilityDataFromCache(Long productId, LocalDate date) throws RuntimeException {
		String keyInCache = redisAvailabilityUtils.defineKeyForAvailabilityCalendar(productId);
		String fieldInCache = CommonDateUtils.format(date);
		String availabilityCalendarInString = RedisClientManager.getRedisClient(RazorConfig.isProduction()).getValueFromRedisHash(keyInCache, fieldInCache);
		if (StringUtils.isEmpty(availabilityCalendarInString)) {
			throw new RuntimeException("There is no any records in cache");
		}
		try {
			return RedisAvailabilityObjectSerializer.extractAvailabilityCalendarFromJson(productId, date, availabilityCalendarInString);
		} catch (RedisCacheException e) {
			RedisClientManager.getRedisClient(RazorConfig.isProduction()).clearValueInCache(keyInCache, fieldInCache);
			return null;
		}
	}

	/**
	 * Generate key with field and get data from cache
	 *
	 * @return deserialized data from cache if it present, or else `null` value
	 */
	public List<AvailabilityCalendarModel> getAvailabilityDataListFromCache(Long productId, List<LocalDate> dates) throws RuntimeException {
		if (CollectionUtils.isEmpty(dates)) {
			return new ArrayList<>();
		}
		String keyInCache = redisAvailabilityUtils.defineKeyForAvailabilityCalendar(productId);
		Map<LocalDate, String> fullCachedAvailabilityByProduct = RedisClientManager.getRedisClient(RazorConfig.isProduction()).getValueFromRedisHash(keyInCache)
				.entrySet().stream().sorted(Map.Entry.comparingByKey()).collect(Collectors
						.toMap(entry -> CommonDateUtils.toLocalDate(entry.getKey()), Map.Entry::getValue, (oldValue, newValue) -> oldValue,
								LinkedHashMap::new)); //TODO need to sort on cache side
		if (!fullCachedAvailabilityByProduct.keySet().containsAll(dates)) {
			throw new RuntimeException("Some Records are missing in cache");
		}
		Map<LocalDate, String> availabilityCalendars = fullCachedAvailabilityByProduct.entrySet().parallelStream()
				.filter(entry -> dates.contains(entry.getKey())).filter(entry -> StringUtils.isNotEmpty(entry.getValue()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		if (availabilityCalendars.size() < dates.size()) {
			throw new RuntimeException("Some Records are missing in cache");
		}
		return availabilityCalendars.entrySet().parallelStream().map(entry -> parseAvailabilityCalendar(productId, entry)).collect(Collectors.toList());

	}

	private AvailabilityCalendarModel parseAvailabilityCalendar(Long productId, Map.Entry<LocalDate, String> entry) {
		try {
			return RedisAvailabilityObjectSerializer.extractAvailabilityCalendarFromJson(productId, entry.getKey(), entry.getValue());
		} catch (RedisCacheException e) {
			RedisClientManager.getRedisClient(RazorConfig.isProduction()).clearValueInCache(String.valueOf(productId), CommonDateUtils.format(entry.getKey()));
			throw new RuntimeException("Error on parsing cache value");
		}
	}

	/**
	 * Uses to get data from cache, using dates and product ID
	 *
	 * @return Availability calendar items for product and dates
	 */
	public List<AvailabilityCalendarModel> getAvailabilityDataFromCache(Long productId, LocalDate fromDate, LocalDate toDate) {
		if (!RedisClientManager.getRedisClient(RazorConfig.isProduction()).isAvailable()) {
			return new ArrayList<>();
		}
		try {
			List<LocalDate> dateRange = CommonDateUtils.getLocalDateRange(fromDate, toDate);
			return getAvailabilityDataListFromCache(productId, dateRange);
		} catch (Exception r) {
			return new ArrayList<>();
		}
	}

	/**
	 * Get list of availability calendars by product ID and list of dates. If count of result items is less then count of dates, we should return empty array.
	 */
	public List<AvailabilityCalendarModel> getAvailabilityDataFromCache(Long productId, List<LocalDate> dates) {
		try {
			//TODO need to check if data is full
			return dates.stream().map(date -> getAvailabilityDataFromCache(productId, date)).filter(Objects::nonNull).collect(Collectors.toList());
		} catch (Exception r) {
			return new ArrayList<>();
		}
	}

	public void removeAvailabilityDataFromCache(Long productId, LocalDate fromDate, LocalDate toDate) {
		if (RedisClientManager.getRedisClient(RazorConfig.isProduction()).isAvailable()) {
			String keyInCache = redisAvailabilityUtils.defineKeyForAvailabilityCalendar(productId);
			List<LocalDate> dateRange = CommonDateUtils.getLocalDateRange(fromDate, toDate);
			dateRange.parallelStream().forEach(day -> removeAvailabilityDataFromCache(day, keyInCache));
		}
	}

	public void removeAvailabilityDataFromCache(Long productId, LocalDate date) {
		if (RedisClientManager.getRedisClient(RazorConfig.isProduction()).isAvailable()) {
			String keyInCache = redisAvailabilityUtils.defineKeyForAvailabilityCalendar(productId);
			removeAvailabilityDataFromCache(date, keyInCache);
		}
	}

	private void removeAvailabilityDataFromCache(LocalDate day, String keyInCache) {
		String fieldInCache = CommonDateUtils.format(day);
		RedisClientManager.getRedisClient(RazorConfig.isProduction()).clearValueInCache(keyInCache, fieldInCache);
	}
}
