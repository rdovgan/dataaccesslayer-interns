package com.mybookingpal.cache.service;

import com.mybookingpal.dal.shared.AvailabilityCalendarModel;
import com.mybookingpal.dal.utils.BigDecimalExt;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public interface AvailabilityCalendarService {

	Integer DEFAULT_MINSTAY = 1;
	BigDecimalExt DEFAULT_BASE_PRICE = BigDecimalExt.ZERO;

	default AvailabilityCalendarModel initializeAvailabilityCalendar(Long productId, LocalDate date) {
		AvailabilityCalendarModel availabilityCalendar = new AvailabilityCalendarModel();
		availabilityCalendar.setAvailabilityDate(date);
		availabilityCalendar.setProductId(productId);
		availabilityCalendar.setAvailableCount(0);
		availabilityCalendar.setBpAvailableCount(0);
		availabilityCalendar.setUnitsBooked(0);
		availabilityCalendar.setThreshold(0);
		availabilityCalendar.setStopSell(false);
		availabilityCalendar.setCloseToArrival(false);
		availabilityCalendar.setCloseToDeparture(false);
		availabilityCalendar.setMinStay(DEFAULT_MINSTAY);
		availabilityCalendar.setBasePrice(DEFAULT_BASE_PRICE);
		return availabilityCalendar;
	}

	List<AvailabilityCalendarModel> getListByPeriodOrderedByDate(List<Long> productIds, LocalDate fromDate, LocalDate toDate);

	default List<AvailabilityCalendarModel> getCachedAvailabilityCalendarByProductIdsAndDateRange(List<Long> productIds, LocalDate fromDate, LocalDate toDate) {
		return new ArrayList<>();
	}
}