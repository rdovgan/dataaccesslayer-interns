package com.mybookingpal.cache.service;

import com.mybookingpal.dal.enums.ProductConstants;
import com.mybookingpal.dal.shared.AvailabilityCalendarModel;
import com.mybookingpal.dal.shared.Product;
import com.mybookingpal.shared.dao.AvailabilityCalendarModelDAO;
import com.mybookingpal.shared.dao.ProductDao;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventAvailabilityCacheService {

	private static final Logger LOG = LoggerFactory.getLogger(EventAvailabilityCacheService.class.getName());

	@Autowired
	private ProductDao productDao;
	@Autowired
	private RedisAvailabilityCalendarService redisAvailabilityCalendarService;
	@Autowired
	private RedisMultiKeyAvailabilityCalendar redisMultiKeyAvailabilityCalendar;
	@Autowired
	private AvailabilityCalendarModelDAO availabilityCalendarModelDAO;

	public void removeAvailabilityDataFromCache(Long productId, LocalDate fromDate, LocalDate toDate) {
		try {
			redisAvailabilityCalendarService.removeAvailabilityDataFromCache(defineProductId(productId), fromDate, toDate);
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
	}

	public void removeAvailabilityDataFromCache(Long productId, LocalDate date) {
		try {
			redisAvailabilityCalendarService.removeAvailabilityDataFromCache(defineProductId(productId), date);
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
	}

	/**
	 * Define availability calendar list for product IDs and date range
	 */
	public List<AvailabilityCalendarModel> defineAvailabilityData(List<Long> productIds, LocalDate fromDate, LocalDate toDate) {
		//TODO disabled Redis due future release
		if (true || CollectionUtils.isEmpty(productIds) || ProductConstants.ProductGroup.MULTI_REP.toString().equals(productDao.read(String.valueOf(productIds.stream().findFirst().get())).getProductGroup())) {
			return new ArrayList<>();
		}
		return productIds.stream().flatMap(productId -> defineAvailabilityData(productId, fromDate, toDate).stream()).collect(Collectors.toList());
	}

	/**
	 * Define availability calendar list for product and date range. We should return empty list for MultiRep products because we got StackOverflow on aspect level. {@link com.mybookingpal.aop.aspects.AvailabilityAspect}
	 */
	public List<AvailabilityCalendarModel> defineAvailabilityData(Long productId, LocalDate fromDate, LocalDate toDate) {
		try {
			//TODO disabled Redis due future release
			if (true || ProductConstants.ProductGroup.MULTI_REP.toString().equals(productDao.read(String.valueOf(productId)).getProductGroup())) {
				return new ArrayList<>();
			}
			List<AvailabilityCalendarModel> availabilityCalendarItemsFromCache = redisAvailabilityCalendarService.getAvailabilityDataFromCache(productId, fromDate, toDate);
			if (CollectionUtils.isEmpty(availabilityCalendarItemsFromCache)) {
				return redisMultiKeyAvailabilityCalendar.defineAvailabilityCalendarsForProduct(fromDate, toDate, productId);
			}
			return availabilityCalendarItemsFromCache;
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return new ArrayList<>();
		}
	}

	public List<AvailabilityCalendarModel> defineAvailabilityData(Long productId, LocalDate date) {
		try {
			//TODO disabled Redis due future release
			if (true) {
				return new ArrayList<>();
			}
			return redisMultiKeyAvailabilityCalendar.defineAvailabilityCalendarsForProduct(date, date.plusDays(1), productId);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return new ArrayList<>();
		}
	}

	private Long defineProductId(Long productId) throws Exception {
		Product product = productDao.read(String.valueOf(productId));
		if (product == null) {
			throw new Exception("Invalid product ID");
		}
		if (!ProductConstants.MultiUnit.MLT.name().equals(product.getMultiUnit()) && product.getParentId() != null) {
			return Long.valueOf(product.getParentId());
		}
		return NumberUtils.createLong(product.getId());
	}

}
