package com.mybookingpal.cache.service;

import com.mybookingpal.dal.shared.AvailabilityCalendarModel;
import com.mybookingpal.dal.shared.Price;
import com.mybookingpal.dal.shared.Product;
import com.mybookingpal.dal.shared.PropertyMinStay;
import com.mybookingpal.dal.shared.action.DatesIdAction;
import com.mybookingpal.dal.shared.api.HasProductIdAndDateRangeImpl;
import com.mybookingpal.dal.utils.BigDecimalExt;
import com.mybookingpal.dal.utils.CommonDateUtils;
import com.mybookingpal.shared.dao.PriceDao;
import com.mybookingpal.shared.dao.ProductDao;
import com.mybookingpal.shared.dao.PropertyMinStayDao;
import com.mybookingpal.shared.dao.ReservationDao;
import com.mybookingpal.utils.entity.NameId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class MultiKeyAvailabilityCalendarService implements AvailabilityCalendarService {

	private static final Logger LOG = LoggerFactory.getLogger(MultiKeyAvailabilityCalendarService.class.getName());

	public static final int MIN_ROOM_COUNT_FOR_AVAILABILITY = 1;

	@Autowired
	private PriceDao priceDao;
	@Autowired
	private PropertyMinStayDao propertyMinStayDao;
	@Autowired
	private ReservationDao reservationDao;
	@Autowired
	protected ProductDao productDao;

	private List<AvailabilityCalendarModel> initializeAvailabilityCalendarList(Long productId, LocalDate fromDate, LocalDate toDate) {
		List<LocalDate> datesList = CommonDateUtils.getLocalDateRange(fromDate, toDate);
		return datesList.parallelStream().map(date -> initializeAvailabilityCalendar(productId, date)).collect(Collectors.toList());
	}

	private List<AvailabilityCalendarModel> defineBasePrices(List<AvailabilityCalendarModel> availabilityCalendars, Long productId, LocalDate fromDate,
			LocalDate toDate) {
		List<Price> productPrices = priceDao.readByProductIdAndDateRange(productId.intValue(), fromDate, toDate);
		return availabilityCalendars.parallelStream()
				.map(availabilityCalendar -> definePriceAndMinStayForAvailabilityCalendar(productPrices, availabilityCalendar)).collect(Collectors.toList());
	}

	private AvailabilityCalendarModel definePriceAndMinStayForAvailabilityCalendar(List<Price> productPrices, AvailabilityCalendarModel availabilityCalendar) {
		Price currentPrice = getPriceForCurrentDay(productPrices, CommonDateUtils.toDate(availabilityCalendar.getAvailabilityDate()));
		if (currentPrice != null) {
			changePriceAndMinStayForAvailabilityCalendar(availabilityCalendar, currentPrice);
		}
		return availabilityCalendar;
	}

	private void changePriceAndMinStayForAvailabilityCalendar(AvailabilityCalendarModel availabilityCalendar, Price currentPrice) {
		availabilityCalendar.setBasePrice(new BigDecimalExt(currentPrice.getValue()));
		availabilityCalendar.setMinStay(currentPrice.getMinStay());
	}

	private Price getPriceForCurrentDay(List<Price> prices, Date day) {
		LocalDate currentDay = CommonDateUtils.toLocalDate(day);
		return prices.parallelStream().filter(isCurrentDayInPriceRange(currentDay)).findFirst().orElse(null);
	}

	private Predicate<Price> isCurrentDayInPriceRange(LocalDate currentDay) {
		return p -> (CommonDateUtils.toLocalDate(p.getDate()).isBefore(currentDay) && CommonDateUtils.toLocalDate(p.getTodate()).isAfter(currentDay)
				|| CommonDateUtils.toLocalDate(p.getDate()).isEqual(currentDay) || CommonDateUtils.toLocalDate(p.getTodate()).isEqual(currentDay));
	}

	private List<AvailabilityCalendarModel> defineMinStays(List<AvailabilityCalendarModel> availabilityCalendars, Long productId, LocalDate fromDate,
			LocalDate toDate) {
		DatesIdAction datesIdAction = new DatesIdAction(productId.intValue(), CommonDateUtils.toDate(fromDate), CommonDateUtils.toDate(toDate));
		List<PropertyMinStay> propertyMinStays = propertyMinStayDao.readByProductIdAndDateRange(datesIdAction);
		return availabilityCalendars.parallelStream().map(availabilityCalendar -> defineMinStayForAvailabilityCalendar(propertyMinStays, availabilityCalendar))
				.collect(Collectors.toList());
	}

	private AvailabilityCalendarModel defineMinStayForAvailabilityCalendar(List<PropertyMinStay> propertyMinStays,
			AvailabilityCalendarModel availabilityCalendar) {
		PropertyMinStay propertyMinStay = getPropertyMinStayForCurrentDay(propertyMinStays, availabilityCalendar.getAvailabilityDate());
		if (propertyMinStay != null) {
			availabilityCalendar.setMinStay(propertyMinStay.getValue());
		}
		return availabilityCalendar;
	}

	private PropertyMinStay getPropertyMinStayForCurrentDay(List<PropertyMinStay> propertyMinStays, LocalDate day) {
		return propertyMinStays.parallelStream().filter(p -> p.getFromDate().isBefore(day) && p.getToDate().isAfter(day)).findFirst().orElse(null);
	}

	protected void defineAvailableCount(List<AvailabilityCalendarModel> availabilityCalendars, Long productId, LocalDate fromDate, LocalDate toDate) {
		List<Product> subProducts = new ArrayList<>(productDao.getAllAvailableRoomsForRoot(productId));
		long availableRoomsCount = subProducts.stream().filter(subProduct -> isNoCollisionsForProduct(subProduct, fromDate, toDate)).count();
		if (availableRoomsCount > 0) {
			addAvailableRoomsCount(availabilityCalendars, availableRoomsCount);
		}
	}

	protected List<AvailabilityCalendarModel> addAvailableRoomsCount(List<AvailabilityCalendarModel> availabilityCalendars, long availableRoomsCount) {
		return availabilityCalendars.parallelStream()
				.map(availabilityCalendar -> defineAvailableRoomsCountForAvailabilityCalendar(availabilityCalendar, (int) availableRoomsCount))
				.collect(Collectors.toList());
	}

	protected AvailabilityCalendarModel defineAvailableRoomsCountForAvailabilityCalendar(AvailabilityCalendarModel availabilityCalendar,
			int availableRoomsCount) {
		availabilityCalendar.setAvailableCount(availabilityCalendar.getAvailableCount() + availableRoomsCount);
		availabilityCalendar.setBpAvailableCount(availabilityCalendar.getBpAvailableCount() + availableRoomsCount);
		return availabilityCalendar;
	}

	protected boolean isNoCollisionsForProduct(Product product, LocalDate fromDate, LocalDate toDate) {
		List<NameId> collisions = reservationDao.getCollisions(new HasProductIdAndDateRangeImpl(Integer.valueOf(product.getId()), fromDate, toDate));
		return collisions == null || collisions.isEmpty();
	}

	@Override
	public List<AvailabilityCalendarModel> getListByPeriodOrderedByDate(List<Long> productIds, LocalDate fromDate, LocalDate toDate) {
		try {
			LocalDate tempToDate = toDate.minusDays(1);
			if (productIds == null || productIds.isEmpty()) {
				throw new Exception("Invalid product number");
			}
			List<AvailabilityCalendarModel> calendarList = productIds.stream()
					.flatMap(productId -> defineAvailabilityCalendarsForProduct(fromDate, tempToDate, productId).stream()).collect(Collectors.toList());
			return checkAvailabilityForAllPeriod(calendarList, productIds);
		} catch (Exception e) {
			return new ArrayList<>();
		}
	}

	private List<AvailabilityCalendarModel> checkAvailabilityForAllPeriod(List<AvailabilityCalendarModel> calendarList, List<Long> productIds) {
		List<AvailabilityCalendarModel> availableCalendarModels = new ArrayList<>();
		for (Long productId : productIds) {
			List<AvailabilityCalendarModel> availabilityCalendarsByProductId = getAvailabilityCalendarModelStreamByProductId(calendarList, productId)
					.collect(Collectors.toList());
			if (getAvailabilityCalendarModelStreamByProductId(calendarList, productId).noneMatch(this::isNotAvailableDay)) {
				availableCalendarModels.addAll(availabilityCalendarsByProductId);
			} else {
				LOG.info("There are no available calendars for product " + productId);
			}
		}
		return availableCalendarModels;
	}

	private Stream<AvailabilityCalendarModel> getAvailabilityCalendarModelStreamByProductId(List<AvailabilityCalendarModel> calendarList, Long productId) {
		return calendarList.parallelStream().filter(ac -> productId.equals(ac.getProductId()));
	}

	private boolean isNotAvailableDay(AvailabilityCalendarModel availabilityCalendarModel) {
		return availabilityCalendarModel.getBpAvailableCount() < MIN_ROOM_COUNT_FOR_AVAILABILITY || availabilityCalendarModel.getBasePrice() == null
				|| availabilityCalendarModel.getBasePrice().compareTo(BigDecimal.ZERO) == 0;
	}

	public List<AvailabilityCalendarModel> defineAvailabilityCalendarsForProduct(LocalDate fromDate, LocalDate toDate, Long productId) {
		List<AvailabilityCalendarModel> availabilityCalendars = initializeAvailabilityCalendarList(productId, fromDate, toDate);
		defineBasePrices(availabilityCalendars, productId, fromDate, toDate); //TODO add collection overriding
		defineMinStays(availabilityCalendars, productId, fromDate, toDate);
		defineAvailableCount(availabilityCalendars, productId, fromDate, toDate);
		return availabilityCalendars;
	}
}
