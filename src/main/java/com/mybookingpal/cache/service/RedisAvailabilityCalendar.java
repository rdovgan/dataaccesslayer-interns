package com.mybookingpal.cache.service;

import com.mybookingpal.dal.shared.AvailabilityCalendarModel;

import java.util.List;

public interface RedisAvailabilityCalendar {

	void defineAndStoreAvailableRoomsCount(AvailabilityCalendarModel availabilityCalendarModel, List<Integer> subProducts);

}