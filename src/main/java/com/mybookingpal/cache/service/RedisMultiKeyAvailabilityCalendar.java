package com.mybookingpal.cache.service;

import com.mybookingpal.cache.utils.RedisAvailabilityUtils;
import com.mybookingpal.dal.shared.AvailabilityCalendarModel;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
//TODO need to refactor this service
public class RedisMultiKeyAvailabilityCalendar extends MultiKeyAvailabilityCalendarService implements RedisAvailabilityCalendar {

	@Autowired
	protected RedisAvailabilityUtils redisAvailabilityUtils;
	@Autowired
	private RedisAvailabilityCalendarService redisAvailabilityCalendarService;
	@Autowired
	private EventAvailabilityCacheService eventAvailabilityCacheService;

	@Override
	public List<AvailabilityCalendarModel> defineAvailabilityCalendarsForProduct(LocalDate fromDate, LocalDate toDate, Long productId) {
		List<AvailabilityCalendarModel> availabilityCalendars = super.defineAvailabilityCalendarsForProduct(fromDate, toDate, productId);
		redisAvailabilityCalendarService.storeAvailabilityDataToCache(availabilityCalendars);
		return availabilityCalendars;
	}

	@Override
	//TODO Need to remove this method, because current service shouldn't call cache directly. Also, we should check usages with MBP module
	public List<AvailabilityCalendarModel> getCachedAvailabilityCalendarByProductIdsAndDateRange(List<Long> productIds, LocalDate fromDate, LocalDate toDate) {
		return productIds.stream().flatMap(productId -> redisAvailabilityCalendarService.getAvailabilityDataFromCache(productId, fromDate, toDate).stream())
				.collect(Collectors.toList());
	}

	@Override
	protected void defineAvailableCount(List<AvailabilityCalendarModel> availabilityCalendars, Long productId, LocalDate fromDate, LocalDate toDate) {
		List<Integer> subProducts = productDao.getAllAvailableRoomsForRoot(productId).stream().filter(Objects::nonNull).map(product -> Integer.valueOf(product.getId()))
				.collect(Collectors.toList());
		availabilityCalendars.forEach(availabilityCalendarModel -> defineAndStoreAvailableRoomsCount(availabilityCalendarModel, subProducts));
	}

	@Override
	public void defineAndStoreAvailableRoomsCount(AvailabilityCalendarModel availabilityCalendarModel, List<Integer> subProducts) {
		if (CollectionUtils.isNotEmpty(subProducts)) {
			int availableRoomsCount = redisAvailabilityUtils.defineReservedRoomsCount(subProducts, availabilityCalendarModel.getAvailabilityDate());
			defineAvailableRoomsCountForAvailabilityCalendar(availabilityCalendarModel, availableRoomsCount);
		}
	}

	@Override
	public List<AvailabilityCalendarModel> getListByPeriodOrderedByDate(List<Long> productIds, LocalDate fromDate, LocalDate toDate) {
		return productIds.stream().flatMap(productId -> getAvailabilityCalendarByProductAndByDates(productId, fromDate, toDate.minusDays(1)).stream())
				.collect(Collectors.toList());
	}

	private List<AvailabilityCalendarModel> getAvailabilityCalendarByProductAndByDates(Long productId, LocalDate fromDate, LocalDate toDate) {
		return eventAvailabilityCacheService.defineAvailabilityData(productId, fromDate, toDate);
	}
}
