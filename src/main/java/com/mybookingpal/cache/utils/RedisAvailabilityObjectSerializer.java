package com.mybookingpal.cache.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.mybookingpal.cache.dto.FullAvailabilityItemForCaching;
import com.mybookingpal.redis.dto.RedisCacheException;
import com.mybookingpal.dal.shared.AvailabilityCalendarModel;
import com.mybookingpal.redis.utils.RedisObjectSerializer;
import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;

public class RedisAvailabilityObjectSerializer extends RedisObjectSerializer {

	private static final Logger LOG = Logger.getLogger(RedisAvailabilityObjectSerializer.class);

	private static ObjectMapper objectMapper = new ObjectMapper();

	static {
		objectMapper.registerModule(new SimpleModule());
	}

	public static String serializeAvailabilityCalendarModel(AvailabilityCalendarModel availabilityCalendarModel) {
		try {
			ByteArrayOutputStream bo = new ByteArrayOutputStream();
			ObjectOutputStream so = new ObjectOutputStream(bo);
			so.writeObject(availabilityCalendarModel);
			so.flush();
			return bo.toString();
		} catch (Exception e) {
			LOG.debug("Can't serialize Availability calendar for Redis caching");
			return null;
		}
	}

	public static AvailabilityCalendarModel deserializeAvailabilityCalendarModel(String stringAvailabilityCalendarModel) {
		try {
			byte b[] = stringAvailabilityCalendarModel.getBytes();
			ByteArrayInputStream bi = new ByteArrayInputStream(b);
			ObjectInputStream si = new ObjectInputStream(bi);
			return (AvailabilityCalendarModel) si.readObject();
		} catch (Exception e) {
			LOG.debug("Can't deserialize Availability calendar from Redis cache");
			return null;
		}
	}

	public static AvailabilityCalendarModel extractAvailabilityCalendarFromJson(Long productId, LocalDate date, String stringAvailabilityCalendar)
			throws RedisCacheException {
		FullAvailabilityItemForCaching availabilityCalendarModelDto = deserializeObjectFromJson(stringAvailabilityCalendar,
				FullAvailabilityItemForCaching.class);
		if (availabilityCalendarModelDto != null) {
			return defineAvailabilityModelForCaching(availabilityCalendarModelDto, productId, date);
		}
		return null;
	}

	private static AvailabilityCalendarModel defineAvailabilityModelForCaching(FullAvailabilityItemForCaching availabilityCalendarModelDto, Long productId,
			LocalDate availabilityDate) {
		AvailabilityCalendarModel availabilityCalendarModel = new AvailabilityCalendarModel();

		availabilityCalendarModel.setProductId(productId);
		availabilityCalendarModel.setAvailabilityDate(availabilityDate);
		availabilityCalendarModel.setMinStay(availabilityCalendarModelDto.getMinStay());
		availabilityCalendarModel.setMaxStay(availabilityCalendarModelDto.getMaxStay());
		availabilityCalendarModel.setBpAvailableCount(availabilityCalendarModelDto.getBpAvailableCount());
		availabilityCalendarModel.setBasePrice(availabilityCalendarModelDto.getBasePrice());

		return availabilityCalendarModel;
	}
}
