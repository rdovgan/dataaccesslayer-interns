package com.mybookingpal.cache.utils;

import com.mybookingpal.cache.dto.FullAvailabilityItemForCaching;
import com.mybookingpal.redis.dto.RedisCacheException;
import com.mybookingpal.redis.dto.RedisKey;
import com.mybookingpal.dal.shared.AvailabilityCalendarModel;
import com.mybookingpal.shared.dao.ReservationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RedisAvailabilityUtils {

	@Autowired
	private ReservationDao reservationDao;

	public String convertAvailabilityCalendarToString(AvailabilityCalendarModel availabilityCalendarModel) {
		try {
			return RedisAvailabilityObjectSerializer.serializeObjectToJson(new FullAvailabilityItemForCaching(availabilityCalendarModel));
		} catch (RedisCacheException e) {
			throw new RuntimeException("Unable to store data to cache");
		}
	}

	public List<String> convertAvailabilityCalendarsListToString(List<AvailabilityCalendarModel> availabilityCalendarModels) {
		return availabilityCalendarModels.parallelStream().map(this::convertAvailabilityCalendarToString).collect(Collectors.toList());
	}

	public String defineKeyForAvailabilityCalendar(AvailabilityCalendarModel availabilityCalendar) {
		return defineKeyForAvailabilityCalendar(availabilityCalendar.getProductId());
	}

	public String defineKeyForAvailabilityCalendar(Long productId) {
		return String.format(RedisKey.AvailabilityCalendar.getKeyFormat(), productId);
	}

	public int defineReservedRoomsCount(List<Integer> subProducts, LocalDate day) {
		int availableRoomsCount = subProducts.size() - reservationDao.getAvailableRoomsCountForMltKeyRoomsByDay(subProducts, day);
		return availableRoomsCount < 0 ? 0 : availableRoomsCount;
	}

}
