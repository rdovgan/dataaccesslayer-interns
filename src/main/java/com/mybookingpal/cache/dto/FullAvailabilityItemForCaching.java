package com.mybookingpal.cache.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mybookingpal.dal.shared.AvailabilityCalendarModel;
import com.mybookingpal.dal.utils.BigDecimalExt;

import javax.xml.bind.annotation.XmlElement;

public class FullAvailabilityItemForCaching {

	@XmlElement(name = "min")
	@JsonProperty("min")
	private Integer minStay = 1;

	@XmlElement(name = "max")
	@JsonProperty("max")
	private Integer maxStay = 0;

	@XmlElement(name = "rooms")
	@JsonProperty("rooms")
	private Integer bpAvailableCount = 0;

	@XmlElement(name = "price")
	@JsonProperty("price")
	private BigDecimalExt basePrice = BigDecimalExt.ZERO;

	public FullAvailabilityItemForCaching() {
	}

	public FullAvailabilityItemForCaching(AvailabilityCalendarModel availabilityCalendarModel) {
		this.minStay = availabilityCalendarModel.getMinStay();
		this.maxStay = availabilityCalendarModel.getMaxStay();
		this.bpAvailableCount = availabilityCalendarModel.getBpAvailableCount();
		this.basePrice = availabilityCalendarModel.getBasePrice();
	}

	public AvailabilityCalendarModel convertToAvailabilityCalendarModel() {
		AvailabilityCalendarModel availabilityCalendarModel = new AvailabilityCalendarModel();
		availabilityCalendarModel.setMinStay(this.minStay);
		availabilityCalendarModel.setMaxStay(this.maxStay);
		availabilityCalendarModel.setBpAvailableCount(this.bpAvailableCount);
		availabilityCalendarModel.setBasePrice(this.basePrice);
		return availabilityCalendarModel;
	}

	public Integer getMinStay() {
		return minStay;
	}

	public void setMinStay(Integer minStay) {
		this.minStay = minStay;
	}

	public Integer getMaxStay() {
		return maxStay;
	}

	public void setMaxStay(Integer maxStay) {
		this.maxStay = maxStay;
	}

	public Integer getBpAvailableCount() {
		return bpAvailableCount;
	}

	public void setBpAvailableCount(Integer bpAvailableCount) {
		this.bpAvailableCount = bpAvailableCount;
	}

	public BigDecimalExt getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BigDecimalExt basePrice) {
		this.basePrice = basePrice;
	}
}
