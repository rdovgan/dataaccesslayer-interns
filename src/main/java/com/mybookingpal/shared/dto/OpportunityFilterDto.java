package com.mybookingpal.shared.dto;

public class OpportunityFilterDto {

	private Integer productId;
	private Integer channelProductId;
	private Integer pmId;
	private Integer pmsId;
	private String altId;
	private Integer page;
	private Integer limit;
	private Integer limitFrom;
	private String sortingBy;
	private String sortingType;

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getChannelProductId() {
		return channelProductId;
	}

	public void setChannelProductId(Integer channelProductId) {
		this.channelProductId = channelProductId;
	}

	public Integer getPmId() {
		return pmId;
	}

	public void setPmId(Integer pmId) {
		this.pmId = pmId;
	}

	public Integer getPmsId() {
		return pmsId;
	}

	public void setPmsId(Integer pmsId) {
		this.pmsId = pmsId;
	}

	public String getAltId() {
		return altId;
	}

	public void setAltId(String altId) {
		this.altId = altId;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getLimitFrom() {
		return limitFrom;
	}

	public void setLimitFrom(Integer limitFrom) {
		this.limitFrom = limitFrom;
	}

	public String getSortingBy() {
		return sortingBy;
	}

	public void setSortingBy(String sortingBy) {
		this.sortingBy = sortingBy;
	}

	public String getSortingType() {
		return sortingType;
	}

	public void setSortingType(String sortingType) {
		this.sortingType = sortingType;
	}

	@Override public String toString() {
		return "OpportunityFilterDto{" + "productId=" + productId + ", channelProductId=" + channelProductId + ", pmId=" + pmId + ", pmsId=" + pmsId
				+ ", altId='" + altId + '\'' + ", page=" + page + ", limit=" + limit + ", limitFrom=" + limitFrom + ", sortingBy='" + sortingBy + '\''
				+ ", sortingType='" + sortingType + '\'' + '}';
	}
}
