package com.mybookingpal.shared.dto;

import java.util.Date;

public class StripeReconcilationReportDto {
	private Integer id;
	private Integer organizationId;
	private Integer agentId;
	private Integer customerId;
	private Integer productId;
	private String state;
	private Date fromDate;
	private Date toDate;
	private Double quote;
	private String chargeType;
	private Date chargeDate;
	private String gatewayTransactionId;
	private Integer gatewayId;
	private String currency;
	private Double totalAmount;
	private Double totalAmountUsd;
	private Double partnerPayment;
	private Double pmsCommissionValue;
	private Double totalBookingpalPayment;
	private String gatewayAccountId;
	private String customerName;
	private String pmName;
	private String channelName;
	private String partnerPaymentModel;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public Integer getAgentId() {
		return agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Double getQuote() {
		return quote;
	}

	public void setQuote(Double quote) {
		this.quote = quote;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public Date getChargeDate() {
		return chargeDate;
	}

	public void setChargeDate(Date chargeDate) {
		this.chargeDate = chargeDate;
	}

	public String getGatewayTransactionId() {
		return gatewayTransactionId;
	}

	public void setGatewayTransactionId(String gatewayTransactionId) {
		this.gatewayTransactionId = gatewayTransactionId;
	}

	public Integer getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(Integer gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getTotalAmountUsd() {
		return totalAmountUsd;
	}

	public void setTotalAmountUsd(Double totalAmountUsd) {
		this.totalAmountUsd = totalAmountUsd;
	}

	public Double getPartnerPayment() {
		return partnerPayment;
	}

	public void setPartnerPayment(Double partnerPayment) {
		this.partnerPayment = partnerPayment;
	}

	public Double getPmsCommissionValue() {
		return pmsCommissionValue;
	}

	public void setPmsCommissionValue(Double pmsCommissionValue) {
		this.pmsCommissionValue = pmsCommissionValue;
	}

	public Double getTotalBookingpalPayment() {
		return totalBookingpalPayment;
	}

	public void setTotalBookingpalPayment(Double totalBookingpalPayment) {
		this.totalBookingpalPayment = totalBookingpalPayment;
	}

	public String getGatewayAccountId() {
		return gatewayAccountId;
	}

	public void setGatewayAccountId(String gatewayAccountId) {
		this.gatewayAccountId = gatewayAccountId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPmName() {
		return pmName;
	}

	public void setPmName(String pmName) {
		this.pmName = pmName;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getPartnerPaymentModel() {
		return partnerPaymentModel;
	}

	public void setPartnerPaymentModel(String partnerPaymentModel) {
		this.partnerPaymentModel = partnerPaymentModel;
	}
}
