package com.mybookingpal.shared.dto;

import java.util.Date;

public class ReservationReportRequestDto {

	private Date reservationDate;

	private String channelAbbreviation;

	private Double finalAmount;

	private String averageDailyRate;

	public ReservationReportRequestDto(Date reservationDate, String channelAbbreviation, Double finalAmount, String averageDailyRate) {
		this.reservationDate = reservationDate;
		this.channelAbbreviation = channelAbbreviation;
		this.finalAmount = finalAmount;
		this.averageDailyRate = averageDailyRate;
	}

	public ReservationReportRequestDto() {
	}

	public Date getReservationDate() {

		return reservationDate;
	}

	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}

	public String getChannelAbbreviation() {
		return channelAbbreviation;
	}

	public void setChannelAbbreviation(String channelAbbreviation) {
		this.channelAbbreviation = channelAbbreviation;
	}

	public Double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(Double finalAmount) {
		this.finalAmount = finalAmount;
	}

	public String getAverageDailyRate() {
		return averageDailyRate;
	}

	public void setAverageDailyRate(String averageDailyRate) {
		this.averageDailyRate = averageDailyRate;
	}
}
