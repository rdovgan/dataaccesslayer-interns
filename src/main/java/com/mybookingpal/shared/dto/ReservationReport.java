package com.mybookingpal.shared.dto;

import java.time.LocalDate;

public class ReservationReport {

	private Integer reservationId;
	private LocalDate reservationDate;
	private LocalDate arrivalDate;
	private String guestName;
	private String productName;
	private Double reservationTotal;
	private String status;
	private String channelName;
	private String channelAbbreviation;
	private String confirmationId;
	private LocalDate departureDate;
	private String productDisplayName;
	private Integer productId;
	private Integer numberOfGuests;

	public ReservationReport() {

	}

	public ReservationReport(Integer reservationId, LocalDate reservationDate, LocalDate arrivalDate, String guestName, String productName,
			Double reservationTotal, String status, String channelName, String channelAbbreviation, String confirmationId, LocalDate departureDate,
			String productDisplayName, Integer productId, Integer numberOfGuests) {
		this.reservationId = reservationId;
		this.reservationDate = reservationDate;
		this.arrivalDate = arrivalDate;
		this.guestName = guestName;
		this.productName = productName;
		this.reservationTotal = reservationTotal;
		this.status = status;
		this.channelName = channelName;
		this.channelAbbreviation = channelAbbreviation;
		this.confirmationId = confirmationId;
		this.departureDate = departureDate;
		this.productDisplayName = productDisplayName;
		this.productId = productId;
		this.numberOfGuests = numberOfGuests;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public LocalDate getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(LocalDate reservationDate) {
		this.reservationDate = reservationDate;
	}

	public LocalDate getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(LocalDate arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getReservationTotal() {
		return reservationTotal;
	}

	public void setReservationTotal(Double reservationTotal) {
		this.reservationTotal = reservationTotal;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelAbbreviation() {
		return channelAbbreviation;
	}

	public void setChannelAbbreviation(String channelAbbreviation) {
		this.channelAbbreviation = channelAbbreviation;
	}

	public String getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(String confirmationId) {
		this.confirmationId = confirmationId;
	}

	public LocalDate getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(LocalDate departureDate) {
		this.departureDate = departureDate;
	}

	public String getProductDisplayName() {
		return productDisplayName;
	}

	public void setProductDisplayName(String productDisplayName) {
		this.productDisplayName = productDisplayName;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getNumberOfGuests() {
		return numberOfGuests;
	}

	public void setNumberOfGuests(Integer numberOfGuests) {
		this.numberOfGuests = numberOfGuests;
	}
}
