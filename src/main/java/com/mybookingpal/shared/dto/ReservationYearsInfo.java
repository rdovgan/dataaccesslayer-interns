package com.mybookingpal.shared.dto;

public class ReservationYearsInfo {

	private String reservationYear;
	private String arrivalYear;
	private String departureYear;

	public ReservationYearsInfo() {

	}

	public String getReservationYear() {
		return reservationYear;
	}

	public void setReservationYear(String reservationYear) {
		this.reservationYear = reservationYear;
	}

	public String getArrivalYear() {
		return arrivalYear;
	}

	public void setArrivalYear(String arrivalYear) {
		this.arrivalYear = arrivalYear;
	}

	public String getDepartureYear() {
		return departureYear;
	}

	public void setDepartureYear(String departureYear) {
		this.departureYear = departureYear;
	}
}
