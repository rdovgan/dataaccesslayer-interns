package com.mybookingpal.shared.dto;


public class ReservationReportGropedByParam  {

	private Integer countReservations;

	private Double sumGross;

	private Double avgSumGross;

	private String groupTime;

	private String channelAbbreviation;

	private Double avgSumDailyRate;

	private Integer countDay;

	public Integer getCountReservations() {
		return countReservations;
	}

	public void setCountReservations(Integer countReservations) {
		this.countReservations = countReservations;
	}

	public ReservationReportGropedByParam(Integer countReservations, Double sumGross, Double avgSumGross, String groupTime, String channelAbbreviation,
			Double finalAmount, Integer countDay) {
		this.countReservations = countReservations;
		this.sumGross = sumGross;
		this.avgSumGross = avgSumGross;
		this.groupTime = groupTime;
		this.channelAbbreviation = channelAbbreviation;
		this.avgSumDailyRate = finalAmount;
		this.countDay = countDay;
	}

	public String getGroupTime() {
		return groupTime;
	}

	public void setGroupTime(String groupTime) {
		this.groupTime = groupTime;
	}

	public ReservationReportGropedByParam() {
	}

	public String getChannelAbbreviation() {
		return channelAbbreviation;
	}

	public void setChannelAbbreviation(String channelAbbreviation) {
		this.channelAbbreviation = channelAbbreviation;
	}

	public Double getAvgSumDailyRate() {
		return avgSumDailyRate;
	}

	public void setAvgSumDailyRate(Double avgSumDailyRate) {
		this.avgSumDailyRate = avgSumDailyRate;
	}

	public Double getSumGross() {
		return sumGross;
	}

	public void setSumGross(Double sumGross) {
		this.sumGross = sumGross;
	}

	public Double getAvgSumGross() {
		return avgSumGross;
	}

	public void setAvgSumGross(Double avgSumGross) {
		this.avgSumGross = avgSumGross;
	}

	public Integer getCountDay() {
		return countDay;
	}

	public void setCountDay(Integer countDay) {
		this.countDay = countDay;
	}
}
