package com.mybookingpal.shared.dto;

public class ChannelProductOpportunityDto {

	private Integer productId;
	private Integer channelProductId;
	private Integer pmId;
	private String pmName;
	private Integer pmsId;
	private String pmsName;
	private Integer numberOfPassed;
	private Integer numberOfOpportunities;
	private String opportunityId;
	private String opportunityName;
	private String actionType;
	private String redirectUrl;


	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getChannelProductId() {
		return channelProductId;
	}

	public void setChannelProductId(Integer channelProductId) {
		this.channelProductId = channelProductId;
	}

	public Integer getPmId() {
		return pmId;
	}

	public void setPmId(Integer pmId) {
		this.pmId = pmId;
	}

	public String getPmName() {
		return pmName;
	}

	public void setPmName(String pmName) {
		this.pmName = pmName;
	}

	public Integer getPmsId() {
		return pmsId;
	}

	public void setPmsId(Integer pmsId) {
		this.pmsId = pmsId;
	}

	public String getPmsName() {
		return pmsName;
	}

	public void setPmsName(String pmsName) {
		this.pmsName = pmsName;
	}

	public Integer getNumberOfPassed() {
		return numberOfPassed;
	}

	public void setNumberOfPassed(Integer numberOfPassed) {
		this.numberOfPassed = numberOfPassed;
	}

	public Integer getNumberOfOpportunities() {
		return numberOfOpportunities;
	}

	public void setNumberOfOpportunities(Integer numberOfOpportunities) {
		this.numberOfOpportunities = numberOfOpportunities;
	}

	public String getOpportunityId() {
		return opportunityId;
	}

	public void setOpportunityId(String opportunityId) {
		this.opportunityId = opportunityId;
	}

	public String getOpportunityName() {
		return opportunityName;
	}

	public void setOpportunityName(String opportunityName) {
		this.opportunityName = opportunityName;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	@Override public String toString() {
		return "ChannelProductOpportunityDto{" + "productId=" + productId + ", channelProductId=" + channelProductId + ", pmId=" + pmId + ", pmName='" + pmName
				+ '\'' + ", pmsId=" + pmsId + ", pmsName='" + pmsName + '\'' + ", numberOfPassed=" + numberOfPassed + ", numberOfOpportunities="
				+ numberOfOpportunities + ", opportunityId='" + opportunityId + '\'' + ", opportunityName='" + opportunityName + '\'' + ", actionType='"
				+ actionType + '\'' + ", redirectUrl='" + redirectUrl + '\'' + '}';
	}
}
