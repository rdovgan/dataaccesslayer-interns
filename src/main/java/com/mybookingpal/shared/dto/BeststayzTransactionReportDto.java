package com.mybookingpal.shared.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Date;
import java.util.List;

@SuppressWarnings("SpellCheckingInspection")
@XmlAccessorType(XmlAccessType.NONE)
public class BeststayzTransactionReportDto {

	@XmlElement(name = "based_on", nillable = true)
	@JsonProperty("based_on")
	private String basedOn;

	@XmlElement(name = "from_date", nillable = true)
	@JsonProperty("from_date")
	private Date fromDate;

	@XmlElement(name = "to_date", nillable = true)
	@JsonProperty("to_date")
	private Date toDate;

	@XmlElement(name = "channels", nillable = true)
	@JsonProperty("channels")
	private List<String> channels;

	@XmlElement(name = "reservation_status", nillable = true)
	@JsonProperty("reservation_status")
	private String reservationStatus;

	@XmlElement(name = "property", nillable = true)
	@JsonProperty("property")
	private Integer propertyId;

	@XmlElement(name = "guest_name", nillable = true)
	@JsonProperty("guest_name")
	private String guestName;

	@XmlElement(name = "reservation_id", nillable = true)
	@JsonProperty("reservation_id")
	private Integer reservationId;

	@XmlElement(name = "homeowner_id", nillable = true)
	@JsonProperty("homeowner_id")
	private Integer homeownerId;

	@XmlElement(name = "use_display_name", nillable = true)
	@JsonProperty("use_display_name")
	private Boolean useDisplayName;

	@XmlElement(name = "report", nillable = true)
	@JsonProperty("report")
	private String report;

	public String getBasedOn() {
		return basedOn;
	}

	public void setBasedOn(String basedOn) {
		this.basedOn = basedOn;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<String> getChannels() {
		return channels;
	}

	public void setChannels(List<String> channels) {
		this.channels = channels;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public Integer getReservationId() {
		return reservationId;
	}

	public void setReservationId(Integer reservationId) {
		this.reservationId = reservationId;
	}

	public Boolean getUseDisplayName() {
		return useDisplayName;
	}

	public void setUseDisplayName(Boolean useDisplayName) {
		this.useDisplayName = useDisplayName;
	}

	public Integer getHomeownerId() {
		return homeownerId;
	}

	public void setHomeownerId(Integer homeownerId) {
		this.homeownerId = homeownerId;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}
}
