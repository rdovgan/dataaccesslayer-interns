package com.mybookingpal.shared.dto;

/**
 * @author lackanovic
 *
 */
public class ProductZipCode9DTO {
	
	private Integer channelID;
	private String zipCodes;
	private String supplierID;

	public Integer getChannelID() {
		return channelID;
	}
	public void setChannelID(Integer channelID) {
		this.channelID = channelID;
	}
	public String getZipCodes() {
		return zipCodes;
	}
	public void setZipCodes(String zipCodes) {
		this.zipCodes = zipCodes;
	}
	public String getSupplierID() {
		return supplierID;
	}
	public void setSupplierID(String supplierID) {
		this.supplierID = supplierID;
	}
}
