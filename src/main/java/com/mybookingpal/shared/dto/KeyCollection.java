package com.mybookingpal.shared.dto;

import com.google.gson.JsonObject;

public class KeyCollection {

	private Integer id;

	private Integer productId;

	private String checkInMethod;

	private JsonObject additionCheckInDto;

	public KeyCollection() {

	}

	public KeyCollection(Integer productId, String checkInMethod, JsonObject additionCheckInDto) {

		this.productId = productId;

		this.checkInMethod = checkInMethod;

		this.additionCheckInDto = additionCheckInDto;

	}

	public Integer getId() {

		return id;

	}

	public void setId(Integer id) {

		this.id = id;

	}

	public Integer getProductId() {

		return productId;

	}

	public void setProductId(Integer productId) {

		this.productId = productId;

	}

	public String getCheckInMethod() {

		return checkInMethod;

	}

	public void setCheckInMethod(String checkInMethod) {

		this.checkInMethod = checkInMethod;

	}

	public JsonObject getAdditionCheckInDto() {

		return additionCheckInDto;

	}

	public void setAdditionCheckInDto(JsonObject additionCheckInDto) {

		this.additionCheckInDto = additionCheckInDto;

	}

}

