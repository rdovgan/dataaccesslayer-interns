package com.mybookingpal.shared.dto;

import com.fasterxml.jackson.annotation.JsonProperty;



import javax.xml.bind.annotation.XmlElement;


public class AdditionCheckInDto {


	@XmlElement(name = "how", nillable = true)

	@JsonProperty("how")

	private String how;


	@XmlElement(name = "when", nillable = true)

	@JsonProperty("when")

	private String when;


	public String getHow() {

		return how;

	}


	public void setHow(String how) {

		this.how = how;

	}


	public String getWhen() {

		return when;

	}


	public void setWhen(String when) {

		this.when = when;

	}


	@Override

	public String toString() {

		return "{" + "\"how\":\"" + how + "\", \"when\":\"" + when + "\"}";

	}

}

