package com.mybookingpal.shared.dto;


import com.mybookingpal.dal.shared.AvailabilityCalendarModel;
import com.mybookingpal.dal.utils.BigDecimalExt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;
import java.time.LocalDate;

@XmlAccessorType(XmlAccessType.FIELD)
public class AvailabilityCalendarDTO {

	@XmlElement(name = "product_id")
	private Long productId;

	@XmlElement(name = "availability_date")
	//@XmlJavaTypeAdapter(type = Date.class, value = LocalDateAdapter.class)
	private LocalDate availabilityDate;

	@XmlElement(name = "threshold")
	private Integer threshold;

	@XmlElement(name = "stop_sell")
	private Boolean stopSell;

	@XmlElement(name = "min_stay")
	private Integer minStay;

	@XmlElement(name = "close_to_arrival")
	private Boolean closeToArrival;

	@XmlElement(name = "close_to_departure")
	private Boolean closeToDeparture;

	@XmlElement(name = "available_count")
	private Integer availableCount;

	@XmlElement(name = "units_booked")
	private Integer unitsBooked;

	@XmlElement(name = "max_stay")
	private Integer maxStay;

	@XmlElement(name = "base_price")
	private BigDecimalExt basePrice;

	@XmlElement(name = "state")
	private AvailabilityCalendarModel.State state;

	public AvailabilityCalendarDTO() {
	}

	public AvailabilityCalendarDTO(AvailabilityCalendarModel availabilityCalendarModel) {
		this.productId = availabilityCalendarModel.getProductId();
		this.availabilityDate = availabilityCalendarModel.getAvailabilityDate();
		this.threshold = availabilityCalendarModel.getThreshold();
		this.stopSell = availabilityCalendarModel.isStopSell();
		this.minStay = availabilityCalendarModel.getMinStay();
		this.closeToArrival = availabilityCalendarModel.isCloseToArrival();
		this.closeToDeparture = availabilityCalendarModel.isCloseToDeparture();
		this.availableCount = availabilityCalendarModel.getAvailableCount();
		this.unitsBooked = availabilityCalendarModel.getUnitsBooked();
		this.basePrice = availabilityCalendarModel.getBasePrice();
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public LocalDate getAvailabilityDate() {
		return availabilityDate;
	}

	public void setAvailabilityDate(LocalDate availabilityDate) {
		this.availabilityDate = availabilityDate;
	}

	public Integer getThreshold() {
		return threshold;
	}

	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}

	public Boolean getStopSell() {
		return stopSell;
	}

	public void setStopSell(Boolean stopSell) {
		this.stopSell = stopSell;
	}

	public Integer getMinStay() {
		return minStay;
	}

	public void setMinStay(Integer minStay) {
		this.minStay = minStay;
	}

	public Boolean getCloseToArrival() {
		return closeToArrival;
	}

	public void setCloseToArrival(Boolean closeToArrival) {
		this.closeToArrival = closeToArrival;
	}

	public Boolean getCloseToDeparture() {
		return closeToDeparture;
	}

	public void setCloseToDeparture(Boolean closeToDeparture) {
		this.closeToDeparture = closeToDeparture;
	}

	public Integer getAvailableCount() {
		return availableCount;
	}

	public void setAvailableCount(Integer availableCount) {
		this.availableCount = availableCount;
	}

	public Integer getUnitsBooked() {
		return unitsBooked;
	}

	public void setUnitsBooked(Integer unitsBooked) {
		this.unitsBooked = unitsBooked;
	}

	public BigDecimalExt getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BigDecimalExt basePrice) {
		this.basePrice = basePrice;
	}

	public Integer getMaxStay() {
		return maxStay;
	}

	public void setMaxStay(Integer maxStay) {
		this.maxStay = maxStay;
	}

	public AvailabilityCalendarModel.State getState() {
		return state;
	}

	public void setState(AvailabilityCalendarModel.State state) {
		this.state = state;
	}
}
