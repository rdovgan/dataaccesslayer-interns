package com.mybookingpal.shared.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;

public class KeyCollectionDto {

	@XmlElement(name = "check_in_method", nillable = true)
	@JsonProperty("check_in_method")
	private String checkInMethod;

	@XmlElement(name = "additional_info", nillable = true)
	@JsonProperty("additional_info")
	private AdditionCheckInDto additionCheckInDto;

	public KeyCollectionDto() {
	}

	public KeyCollectionDto(String checkInMethod, AdditionCheckInDto additionCheckInDto) {
		this.checkInMethod = checkInMethod;
		this.additionCheckInDto = additionCheckInDto;
	}

	public String getCheckInMethod() {
		return checkInMethod;
	}

	public void setCheckInMethod(String checkInMethod) {
		this.checkInMethod = checkInMethod;
	}

	public AdditionCheckInDto getAdditionCheckInDto() {
		return additionCheckInDto;
	}

	public void setAdditionCheckInDto(AdditionCheckInDto additionCheckInDto) {
		this.additionCheckInDto = additionCheckInDto;
	}
}
