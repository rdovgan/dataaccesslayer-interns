package com.mybookingpal.shared.dto;

import java.util.Date;
import java.util.List;

public class ReservationFilterDto {

	private Integer id;
	private Integer pmId;
	private String filterDate;
	private String startDate;
	private String endDate;
	private List<String> channels;
	private List<String> statuses;
	private List<String> properties;
	private Boolean useDisplayName;
	private String search;
	private String guestName;
	private String reservationId;
	private String email;
	private Date fromDate;
	private Date toDate;
	private List<Integer> pmIds;

	public ReservationFilterDto() {

	}

	public ReservationFilterDto(String filterDate, String startDate, String endDate, List<String> channels, List<String> statuses, List<String> properties,
			Boolean useDisplayName, String search) {
		this.filterDate = filterDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.channels = channels;
		this.statuses = statuses;
		this.properties = properties;
		this.useDisplayName = useDisplayName;
		this.search = search;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPmId() {
		return pmId;
	}

	public void setPmId(Integer pmId) {
		this.pmId = pmId;
	}

	public String getFilterDate() {
		return filterDate;
	}

	public void setFilterDate(String filterDate) {
		this.filterDate = filterDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public List<String> getChannels() {
		return channels;
	}

	public void setChannels(List<String> channels) {
		this.channels = channels;
	}

	public List<String> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<String> statuses) {
		this.statuses = statuses;
	}

	public List<String> getProperties() {
		return properties;
	}

	public void setProperties(List<String> properties) {
		this.properties = properties;
	}

	public Boolean getUseDisplayName() {
		return useDisplayName;
	}

	public void setUseDisplayName(Boolean useDisplayName) {
		this.useDisplayName = useDisplayName;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getReservationId() {
		return reservationId;
	}

	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<Integer> getPmIds() {
		return pmIds;
	}

	public void setPmIds(List<Integer> pmIds) {
		this.pmIds = pmIds;
	}
}
