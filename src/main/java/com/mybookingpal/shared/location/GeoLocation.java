package com.mybookingpal.shared.location;

import org.springframework.beans.BeanUtils;

import com.mybookingpal.dal.shared.Location;

public class GeoLocation extends Location {

	private String zipCode;

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public GeoLocation() {
	}

	public Location clone(Location location) {
		BeanUtils.copyProperties(location, this);
		return this;
	}
}
