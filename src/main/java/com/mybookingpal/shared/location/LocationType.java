package com.mybookingpal.shared.location;

/**
 * Location types that are available in location table
 */
public enum LocationType {
	COUNTRY("country"),LOCALITY("locality"), SUBLOCALITY("sublocality");
	private final String name;

	LocationType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
