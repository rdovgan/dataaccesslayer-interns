package com.mybookingpal.shared.cache;

import java.time.LocalDate;

public interface IsCacheable {

	Long getCacheProductId();
	LocalDate getFromDate();
	LocalDate getToDate();
}
