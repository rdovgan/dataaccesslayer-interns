package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelProductValidationRulesTypesMapper;
import com.mybookingpal.dal.shared.ChannelProductValidationRulesTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ChannelProductValidationRulesTypesDao {

    @Autowired
    ChannelProductValidationRulesTypesMapper channelProductValidationRulesTypesMapper;

    public void create (ChannelProductValidationRulesTypes channelProductValidationRulesTypes) {
        channelProductValidationRulesTypesMapper.create(channelProductValidationRulesTypes);
    }

}
