package com.mybookingpal.shared.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ReservationConfirmationMapper;
import com.mybookingpal.dal.shared.ReservationConfirmation;

@Repository
public class ReservationConfirmationDao {
	
	@Autowired
	ReservationConfirmationMapper reservationConfirmationMapper;
	
	public void create(ReservationConfirmation action){
		reservationConfirmationMapper.create(action);
	}

	public ReservationConfirmation read(String id){
		return reservationConfirmationMapper.read(id);
	}
	public ReservationConfirmation readByReservationId(Integer id){
		return reservationConfirmationMapper.readByReservationId(id);
	}
	public Integer readLatestReservationIdByConfirmationId(String id){
		return reservationConfirmationMapper.readLatestReservationIdByConfirmationId(id);
	}

	public List<ReservationConfirmation> list(){
		return reservationConfirmationMapper.list();
	}
	public List<ReservationConfirmation>  readByChannelIdConfirmationId(ReservationConfirmation action){
		return reservationConfirmationMapper.readByChannelIdConfirmationId(action);
	}
	public List<ReservationConfirmation>  readByChannelIdAndCreatedDate(ReservationConfirmation action){
		return reservationConfirmationMapper.readByChannelIdAndCreatedDate(action);
	}
	public List<ReservationConfirmation>  readDuplicatesByChannelIdAndCreatedDate(ReservationConfirmation action){
		return reservationConfirmationMapper.readDuplicatesByChannelIdAndCreatedDate(action);
	}
	public List<ReservationConfirmation>  readByConfirmationId(String confirmationId){
		return reservationConfirmationMapper.readByConfirmationId(confirmationId);
	}
	public List<ReservationConfirmation>  findByChannelIdAndConfirmationIdLike(ReservationConfirmation action){
		return reservationConfirmationMapper.findByChannelIdAndConfirmationIdLike(action);
	}

	public void update(ReservationConfirmation action){
		reservationConfirmationMapper.update(action);
	}

	public void delete(String id){
		reservationConfirmationMapper.delete(id);
	}
	
	public void deleteByProductId(String productId){
		reservationConfirmationMapper.deleteByProductId(productId);
	}

	public ReservationConfirmation readOldReservationIdByConfirmationId(String id){
		return reservationConfirmationMapper.readOldReservationIdByConfirmationId(id);
	}
	
	public List<ReservationConfirmation> getListByChannelIdAndConfirmationId(ReservationConfirmation action) {
		return reservationConfirmationMapper.readByChannelIdConfirmationId(action);
	}

	public Map<String, String> readLatestReservationIdAndProductIdByConfirmationId(String id) {
		return reservationConfirmationMapper.readLatestReservationIdAndProductIdByConfirmationId(id);
	}
}
