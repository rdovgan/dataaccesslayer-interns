package com.mybookingpal.shared.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.CommissionSettingMapper;
import com.mybookingpal.dal.server.util.enums.CommissionSettingEnum;
import com.mybookingpal.dal.shared.CommissionSetting;

@Repository
public class CommissionSettingDao {

	@Autowired
	CommissionSettingMapper commissionSettingMapper;

	private final Class<CommissionSettingMapper> COMMISSION_SETTING_MAPPER = CommissionSettingMapper.class;

	public void create(CommissionSetting commission) {
		commissionSettingMapper.create(commission);
	}

	public void changeCommissionSetting(CommissionSettingEnum commissionSetting, Integer pmId, Integer channelPartnerPartyId) {
		commissionSettingMapper.changeCommissionSetting(commissionSetting, pmId, channelPartnerPartyId);
	}

	public CommissionSetting readBpCommissionByPmIdAndCpPartyId(CommissionSetting commissionSetting) {
		return commissionSettingMapper.readBpCommissionByPmIdAndCpPartyId(commissionSetting);
	}

	public CommissionSetting readBpCommissionByPmIdAndChannelPartyId(Integer pmId, Integer channelId) {
		return commissionSettingMapper.readBpCommissionByPmIdAndChannelPartyId(pmId, channelId);
	}

	public CommissionSetting readChannelCommissionByPmIdChannelPartyIdAndProductId(Integer pmId, Integer channelId, Integer productId) {
		return commissionSettingMapper.readChannelCommissionByPmIdChannelPartyIdAndProductId(pmId, channelId, productId);
	}

}
