package com.mybookingpal.shared.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelImageMapMapper;
import com.mybookingpal.dal.shared.ChannelImageMap;

@Repository
public class ChannelImageMapDAO {

	private static final Logger LOG = Logger.getLogger(ChannelPromoMapDAO.class);

	@Autowired
	private ChannelImageMapMapper mapper;

	@Autowired
	ApplicationContext ctx;

	public void createImageMap(ChannelImageMap channelImageMap) {
		mapper.createImageMap(channelImageMap);
	}

	public List<ChannelImageMap> readImageMap(Integer imageId) {
		return mapper.readImageMap(imageId);
	}

	public ChannelImageMap readImageMapByImageId(ChannelImageMap channelImageMap) {
		return mapper.readImageMapByImageId(channelImageMap);
	}
	
	public ChannelImageMap readImageMapByImageIdRoomId(ChannelImageMap channelImageMap) {
		return mapper.readImageMapByImageIdRoomId(channelImageMap);
	}

	public void updateChannelImageId(ChannelImageMap channelImageMap) {
		mapper.updateChannelImageId(channelImageMap);
	}
	
	public void updateChannelImageMap(ChannelImageMap channelImageMap) {
		mapper.updateChannelImageMap(channelImageMap);
	}

	public void updateStateByChannelImageIdChannelProductId(ChannelImageMap channelImageMap) {
		mapper.updateStateByChannelImageIdChannelProductId(channelImageMap);
	}

	public void deleteById(String id) {
		mapper.deleteById(id);
	}

	public void deleteImageByImageId(ChannelImageMap channelImageMap) {
		mapper.deleteImageByImageId(channelImageMap);
	}

	public List<Integer> getPropertiesForUploadForChannel(Integer channelId) {
		return mapper.getPropertiesForUploadForChannel(channelId);
	}

	public List<ChannelImageMap> readImageMapsByChannelProductIdChannelImageIdChannelId(ChannelImageMap channelImageMap) {
		return mapper.readImageMapsByChannelProductIdAndChannelImageIdAndChannelId(channelImageMap);
	}

	public List<ChannelImageMap> readImageMapByProductIdChannelId(ChannelImageMap channelImageMap) {
		return mapper.readImageMapByProductIdChannelId(channelImageMap);
	}
	
	public List<ChannelImageMap> readAllImageMapByProductIdChannelId(ChannelImageMap channelImageMap) {
		return mapper.readAllImageMapByProductIdChannelId(channelImageMap);
	}

	public void updateStateByChannelImageIdsChannelProductId(String channelProductId, List<String> channelImageIds) {
		mapper.updateStateByChannelImageIdsChannelProductId(channelProductId, channelImageIds);
	}
	
	public ChannelImageMap readImageMapByProductIdChannelIdRoomIdImageId(ChannelImageMap channelImageMap){
		return mapper.readImageMapByProductIdChannelIdRoomIdImageId(channelImageMap);
	}
	
	public List<ChannelImageMap> readImageMapWithoutChannelImage(ChannelImageMap channelImageMap){
		return mapper.readImageMapWithoutChannelImage(channelImageMap);
	}
	
	public List<ChannelImageMap> readImageMapInactive(ChannelImageMap channelImageMap){
		return mapper.readImageMapInactive(channelImageMap);
	}
	
	public List<ChannelImageMap> readAllImageMapByChannelProductIdRoomIdChannelId(ChannelImageMap channelImageMap){
		return mapper.readAllImageMapByChannelProductIdRoomIdChannelId(channelImageMap);
	}

	public List<ChannelImageMap> readImageMapActive(ChannelImageMap channelImageMap) {
		return mapper.readImageMapActive(channelImageMap);
	}
	

}
