package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelAttributeSettingsMapper;
import com.mybookingpal.dal.shared.ChannelAttributeSettings;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Mihailo Spasojevic
 */
@Repository
public class ChannelAttributeSettingsDao {

    private static final Logger LOG = Logger.getLogger(ChannelAttributeSettingsDao.class);

    @Autowired
    private ChannelAttributeSettingsMapper mapper;

    public void create(ChannelAttributeSettings channelAttributeSettings) {
        mapper.create(channelAttributeSettings);

    }


    public ChannelAttributeSettings readByChannelPartnerId(Integer channelPartnerId) {
        return mapper.readByChannelPartnerId(channelPartnerId);
    }

    public void update(ChannelAttributeSettings channelAttributeSettings) {
        mapper.update(channelAttributeSettings);
    }

}
