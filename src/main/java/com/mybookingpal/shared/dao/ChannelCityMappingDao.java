package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelCityMappingMapper;
import com.mybookingpal.dal.shared.ChannelCityMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ChannelCityMappingDao {

	@Autowired
	private ChannelCityMappingMapper cityMappingMapper;

	public void create(ChannelCityMapping channelCityMapping) {
		cityMappingMapper.create(channelCityMapping);
	}

	public List<ChannelCityMapping> readByExample(ChannelCityMapping action) {
		return cityMappingMapper.readByExample(action);
	}

	public ChannelCityMapping readByChannelIdAndLocationId(String channelId, String locationId) {
		ChannelCityMapping channelCityMapping = new ChannelCityMapping();
		channelCityMapping.setChannelId(channelId);
		channelCityMapping.setLocationId(locationId);
		return cityMappingMapper.readByChannelIdAndLocationId(channelCityMapping);
	}
}
