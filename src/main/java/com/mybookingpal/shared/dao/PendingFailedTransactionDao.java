package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.dao.entities.FailedTransactionPaymentModel;
import com.mybookingpal.dal.server.api.PendingFailedTransactionsMapper;
import com.mybookingpal.dal.shared.PendingFailedTransaction;

@Repository
public class PendingFailedTransactionDao {
	
//	@Autowired
//	PendingFailedTransaction pendingFailedTransaction;
	@Autowired
	PendingFailedTransactionsMapper pendingFailedTransactionsMapper;

	public PendingFailedTransaction readActiveTransactionByGroupedReservationId(Long id) {
		return pendingFailedTransactionsMapper.readActiveTransactionByGroupedReservationId(id);
	}

	public List<FailedTransactionPaymentModel> readAllFailedTransactionPayments(FailedTransactionPaymentRequest request) {
		return pendingFailedTransactionsMapper.readAllFailedTransactionPayments(request);
		//return null;
	}

	public void markAsEmailSentToAgent(Long id) {
		pendingFailedTransactionsMapper.markAsEmailSentToAgent(id);
	}

	public PendingFailedTransaction read(Long id) {
		return pendingFailedTransactionsMapper.read(id);
	}

	public void update(PendingFailedTransaction model) {
		pendingFailedTransactionsMapper.update(model);
	}

	public List<PendingFailedTransaction> readPaymentsByReservationId(Long reservationId) {
		return pendingFailedTransactionsMapper.readPaymentsByReservationId(reservationId);
	}
	
	public List<PendingFailedTransaction> readAllActiveFailedPaymentReservationsByChannelId(Integer channelId) {
		return pendingFailedTransactionsMapper.readAllActiveFailedPaymentReservationsByChannelId(channelId);
	}
	
	
}
