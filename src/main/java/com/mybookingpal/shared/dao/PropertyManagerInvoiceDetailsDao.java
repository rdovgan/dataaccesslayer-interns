package com.mybookingpal.shared.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.PropertyManagerInvoiceDetailsMapper;
import com.mybookingpal.dal.shared.PropertyManagerInvoiceDetails;

/**
 * @author Danijel Blagojevic
 *
 */
@Repository
public class PropertyManagerInvoiceDetailsDao {

	@Autowired
	PropertyManagerInvoiceDetailsMapper propertyManagerInvoiceDetailsMapper;
	
	public PropertyManagerInvoiceDetails getByPmId(Integer pmId) {
		return propertyManagerInvoiceDetailsMapper.getByPmId(pmId);
	}

	public void create(PropertyManagerInvoiceDetails propertyManagerInvoiceDetails) {
		propertyManagerInvoiceDetailsMapper.create(propertyManagerInvoiceDetails);
	}

	public void update(PropertyManagerInvoiceDetails propertyManagerInvoiceDetails) {
		propertyManagerInvoiceDetailsMapper.update(propertyManagerInvoiceDetails);
	}
}
