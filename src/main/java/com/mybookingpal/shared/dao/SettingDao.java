package com.mybookingpal.shared.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.config.RazorConfig;
import com.mybookingpal.dal.server.api.SettingMapper;
import com.mybookingpal.dal.shared.Setting;

@Repository
public class SettingDao {

	private static final Logger LOG = Logger.getLogger(SettingDao.class.getName());

	@Autowired
	SettingMapper settingMapper;

	public void create(Setting setting) {
		settingMapper.create(setting);
	}

	public Setting read(Integer id) {
		return settingMapper.read(id);
	}

	public Setting readbyname(String name) {
		return settingMapper.readbyname(name);
	}

	public void update(Setting setting) {
		settingMapper.update(setting);
	}
	
	public void updateValue(Setting setting) {
		settingMapper.updateValue(setting);
	}

	public void delete(Integer id) {
		settingMapper.delete(id);
	}

	public List<Setting> list() {
		return settingMapper.list();
	}

	public List<String> readEmailList(String keyName) {
		return settingMapper.readEmailList(keyName);
	}

	public void addValueToList(String value, String param) {
		Setting values = readbyname(param);
		Integer settingId = values.getId();
		if (!values.getValue().contains(value)) {
			LOG.info("Existing values in setting list " + values.getValue());
			LOG.info("Adding the new value " + value  +" to setting list for " + param);
			String pmsList = values.getValue() + "," + value;
			Setting setting = new Setting();
			setting.setId(settingId);
			setting.setValue(pmsList);
			update(setting);
		}
	}
}
