package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.dao.entities.ValidationSettingsModel;
import com.mybookingpal.dal.server.api.ValidationSettingsMapper;

@Repository
public class ValidationSettingsDao {

	@Autowired
	ValidationSettingsMapper validationSettingsMapper;

	public List<ValidationSettingsModel> readAllForProduct(Long productId) {
		return validationSettingsMapper.readAllForProduct(productId);
	}

	public List<ValidationSettingsModel> readAllForPropertyManager(Long propertyManagerPartyId) {
		return validationSettingsMapper.readAllForPropertyManager(propertyManagerPartyId);
	}
}
