package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.PaymentDetailsMapper;
import com.mybookingpal.dal.shared.PaymentDetails;

@Repository
public class PaymentDetailsDao {
	
	@Autowired
	PaymentDetailsMapper paymentDetailsMapper;

	public void create(PaymentDetails paymentDetails) {
		paymentDetailsMapper.create(paymentDetails);
	}

	public List<PaymentDetails> readByProductIdWithStateCreated(int productId) {
		return paymentDetailsMapper.readByProductIdWithStateCreated(productId);
	}

	public void update(PaymentDetails paymentDetails) {
		paymentDetailsMapper.update(paymentDetails);
	}

	public List<PaymentDetails> readByPropertyManagerIdWithStateCreated(int propertyManagerId) {
		return paymentDetailsMapper.readByPropertyManagerIdWithStateCreated(propertyManagerId);
	}
}
