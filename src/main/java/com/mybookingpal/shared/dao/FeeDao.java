package com.mybookingpal.shared.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.shared.LocalDateRange;
import com.mybookingpal.dal.shared.RoomPeriodChange;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.FeeMapper;
import com.mybookingpal.dal.shared.Fee;
import com.mybookingpal.dal.shared.NameIdCountOffset;
import com.mybookingpal.dal.shared.fee.FeeSearch;

@Repository
public class FeeDao {

	@Autowired
	private FeeMapper feeMapper;

	public void create(Fee action) {
		feeMapper.create(action);
	}

	public void update(Fee action) {
		feeMapper.update(action);
	}

	public Fee exists(Fee action) {
		return feeMapper.exists(action);
	}

	public Fee equalExists(Fee action) {
		return feeMapper.equalExists(action);
	}

	public Fee read(int id) {
		return feeMapper.read(id);
	}

	public void cancelversion(Fee action) {
		feeMapper.cancelversion(action);
	}

	public void cancelfeelist(List<Integer> feeIdList) {
		feeMapper.cancelfeelist(feeIdList);
	}

	public void insertList(@Param("list") List<Fee> fees) {
		feeMapper.insertList(fees);
	}

	public void insertListWithUpdate(@Param("list") List<Fee> fees) {
		feeMapper.insertListWithUpdate(fees);
	}

	public ArrayList<Fee> readbyproductandstate(Fee action) {
		return feeMapper.readbyproductandstate(action);
	}
	
	public ArrayList<Fee> readAllUniqueFeesForAProduct(Fee action) {
		return feeMapper.readAllUniqueFeesForAProduct(action);
	}
	
	public ArrayList<Fee> readUniquebyproductandstate(Fee action) {
		return feeMapper.readUniquebyproductandstate(action);
	}

	public List<Fee> readbydateandproduct(Fee action) {
		return feeMapper.readbydateandproduct(action);
	}

	public List<Fee> readspecialbydateandproduct(Fee action) {
		return feeMapper.readspecialbydateandproduct(action);
	}

	public List<Fee> listbyproductids(FeeSearch action) {
		return feeMapper.listbyproductids(action);
	}

	public ArrayList<Fee> readbyProductandStateandValueTypeandTaxType(Fee action) {
		return feeMapper.readbyProductandStateandValueTypeandTaxType(action);
	}

	public ArrayList<Fee> readByChannelPage(NameIdCountOffset action) {
		return feeMapper.readByChannelPage(action);
	}

	public ArrayList<Fee> readAll(int channelPartnerId) {
		return feeMapper.readAll(channelPartnerId);
	}

	public ArrayList<Fee> readbyproductandunit(Fee action) {
		return feeMapper.readbyproductandunit(action);
	}

	public ArrayList<Fee> readbyproductanddifrentunit(Fee action) {
		return feeMapper.readbyproductanddifrentunit(action);
	}
	
	public ArrayList<Fee> readUniquebyproductandstateandmaxval(Fee action){
		return feeMapper.readUniquebyproductandstateandmaxval(action);
	}
	
	public ArrayList<Fee> readUniquebyproductandstateandminperson(Fee action){
		return feeMapper.readUniquebyproductandstateandminperson(action);
	}

	public List<RoomPeriodChange> readFeesForPeriodForAllStateBySupplierIdByVersion(String supplierId, Date fromDate, Date toDate, Date lastFetch, Integer channelId) {
		return feeMapper.readFeesForPeriodForAllStateBySupplierIdByVersion(supplierId, fromDate, toDate, lastFetch, channelId);
	}

	public List<Fee> getAllFeesByProductAndVersion(Fee action) {
		return feeMapper.getAllFeesByProductAndVersion(action);
	}
	
	public List<Fee> getAllEntityTypeFeesByProductAndVersion(Fee action) {
		return feeMapper.getAllEntityTypeFeesByProductAndVersion(action);
	}
	
	public List<Fee> getMandatoryAndOnArrivalFeesByProduct(Fee action) {
		return feeMapper.getMandatoryAndOnArrivalFeesByProduct(action);
	}
	
	public Fee readFeeByRatePlanId(Long ratePlanId) {
		return feeMapper.readFeeByRatePlanId(ratePlanId);
	}
	
	public ArrayList<Fee> readbyproductandunitformultipleproperties(Fee action) {
		return feeMapper.readbyproductandunitformultipleproperties(action);
	}
	
	public List<Fee> getExtraPersonFeeByProductAndVersion(Fee fee){
		return feeMapper.getExtraPersonFeeByProductAndVersion(fee);
	}
	
	public ArrayList<Fee> readUniqFeesWithoutExtraPersonFee(Fee action){
		return feeMapper.readUniqFeesWithoutExtraPersonFee(action);
	}
	
	public ArrayList<Fee> readUniqExtraPersonFee(Fee action){
		return feeMapper.readUniqExtraPersonFee(action);
	}
	
	public ArrayList<Fee> readUniqFeesWithoutExtraPersonFeeForKey(Fee action){
		return feeMapper.readUniqFeesWithoutExtraPersonFeeForKey(action);
	}
	
	public List<Fee> readAllMandatoryFeeswithoutExtraPersonFee(Fee action){
		return feeMapper.readAllMandatoryFeeswithoutExtraPersonFee(action);
	}
	
	public ArrayList<Fee> readUniqExtraPersonFeeForKey(Fee action){
		return feeMapper.readUniqExtraPersonFeeForKey(action);
	}

	public List<Fee> readByProductIds(List<String> productIds, int state) {
		return feeMapper.readByProductIds(productIds, state);
	}

	public List<String> getUpdatedFees(int daysBack) {
		return feeMapper.getUpdatedFees(daysBack);
	}
	
	public ArrayList<Fee> readMandatoryFeesForProduct(Fee action){
		return feeMapper.readMandatoryFeesForProduct(action);
	}

	public List<Fee> readFeeByProductIdAndDates(Integer productId, List<LocalDateRange> dates) {
		return feeMapper.readFeeByProductIdAndDates(productId, dates);
	}

	public List<Fee> readFeeByProductIdAndDatesAndNameTemplate(Integer productId, List<LocalDateRange> dates, List<FeeSearch> searchFees) {
		return feeMapper.readFeeByProductIdAndDatesAndNameTemplate(productId, dates, searchFees);
	}

	public Fee getSecurityDepositFee(Integer productId) {
		return feeMapper.getSecurityDepositFee(productId);
	}
}
