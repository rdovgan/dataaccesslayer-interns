package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelProductMapMapper;
import com.mybookingpal.dal.shared.ChannelPartyToProduct;
import com.mybookingpal.dal.shared.ChannelProductMap;
import com.mybookingpal.dal.shared.ExpediaInactiveReport;
import com.mybookingpal.dal.shared.IdVersion;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.shared.dto.ProductZipCode9DTO;
import org.apache.ibatis.annotations.Param;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ChannelProductMapDAO {

	private static final Logger LOG = Logger.getLogger(ChannelProductMapDAO.class);

	@Autowired
	private ChannelProductMapMapper mapper;

	@Autowired
	private ChannelPartyToProductDao channelPartyToProductDao;

	public Integer readChannelProductID(ChannelProductMap tempChannelProductMap) {
		return mapper.readChannelProductID(tempChannelProductMap);
	}

	public ChannelProductMap findById(ChannelProductMap channelProductMap) {
		return mapper.findByBPProductAndChannelIdForNonAirbnb(channelProductMap);
	}

	public List<ChannelProductMap> readAllBPProductWithOutState(ChannelProductMap channelProductMap) {
		return mapper.readAllBPProductWithOutState(channelProductMap);
	}

	public List<ChannelProductMap> readAllBPProduct(ChannelProductMap channelProductMap) {
		return mapper.readAllBPProduct(channelProductMap);
	}

	public ChannelProductMap findByBPProductAndChannelId(ChannelProductMap tempChannelProductMap) {
		return mapper.findByBPProductAndChannelId(tempChannelProductMap);
	}

	public void createProductMap(ChannelProductMap tempChannelProductMap) {
		if (tempChannelProductMap.getPortalState() == null) {
			tempChannelProductMap.setPortalState(ChannelProductMap.PortalState.NotDestributed.getId());
		}
		mapper.createProductMap(tempChannelProductMap);
	}

	//	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ChannelProductMap tempChannelProductMap) {
		mapper.update(tempChannelProductMap);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void transactionalUpdate(ChannelProductMap tempChannelProductMap) {
		mapper.update(tempChannelProductMap);
	}

	public List<Integer> findChannelIdsBySupplierId(String supplierId) {
		return mapper.findChannelIdsBySupplierId(supplierId);
	}

	public List<ChannelProductMap> findByBPProductsAndChannelId(ChannelProductMap tempChannelProductMap) {
		return mapper.findByBPProductsAndChannelId(tempChannelProductMap);
	}
	
	public List<ChannelProductMap> findByProductsForExpedia(ChannelProductMap tempChannelProductMap) {
		return mapper.findByProductsForExpedia(tempChannelProductMap);
	}

	public List<ChannelProductMap> readAllBPProductNoLimitNoState(ChannelProductMap channelProductMap) {
		return mapper.readAllBPProductNoLimitNoState(channelProductMap);
	}

	public ChannelProductMap findByProductIdAndChannelId(ChannelProductMap tempChannelProductMap) {
		return mapper.findByProductIdAndChannelId(tempChannelProductMap);
	}

	/**
	 * This method looks up channel product map by product and channel id.
	 *
	 * @param sqlSession
	 * @param productId
	 * @param channelId
	 * @return
	 */
	public ChannelProductMap findByProductIdAndChannelId(String productId, int channelId) {

		ChannelProductMap channelProductMap = new ChannelProductMap();
		channelProductMap.setChannelId(channelId);
		channelProductMap.setProductId(productId);
		return mapper.findByProductIdAndChannelId(channelProductMap);
	}

	public int findCountByProductIdAndChannelIdWithoutState(String productId, int channelId) {

		ChannelProductMap channelProductMap = new ChannelProductMap();
		channelProductMap.setChannelId(channelId);
		channelProductMap.setProductId(productId);
		channelProductMap.setState(null);
		return mapper.findCountByProductIdAndChannelId(channelProductMap);
	}

	public int findCountByProductIdAndChannelIDWithState(String productId, int channelId) {
		ChannelProductMap channelProductMap = new ChannelProductMap();
		channelProductMap.setChannelId(channelId);
		channelProductMap.setProductId(productId);
		channelProductMap.setState(true);
		return mapper.findCountByProductIdAndChannelId(channelProductMap);
	}

	/**
	 * This method looks up inactive channel product map list by product id list and channel id. It's currently being used to find inactive channel map listings
	 * for active properties
	 *
	 * @param sqlSession
	 * @param productId
	 * @param channelId
	 * @return
	 */
	public List<ChannelProductMap> findInActiveListingsByProductsAndChannelId(List<String> productIds, int channelId) {

		if (productIds == null || productIds.isEmpty()) {
			return null;
		}

		ChannelProductMap channelProductMap = new ChannelProductMap();
		channelProductMap.setChannelId(channelId);
		channelProductMap.setList(productIds);
		return mapper.findInActiveListingsByProductsAndChannelId(channelProductMap);
	}

	public ChannelProductMap findByListingIdAndChannelId(String listingId, int channelId) {

		ChannelProductMap channelProductMap = new ChannelProductMap();
		channelProductMap.setChannelId(channelId);
		channelProductMap.setChannelProductId(listingId);
		channelProductMap.setState(true);
		return mapper.findByListingAndChannelIdForNonAirbnb(channelProductMap);
	}

	/**
	 * Create new ChannelPartyToProducts. It will not update ChannelPartyToProduct(s) and just show some logs about already exists items. and new one has
	 * different PartyId LOG a warn.
	 *
	 * @param channelPartyToProductList The List of ChannelPartyToProduct to create.
	 */
	public void createProductChannelPartyToProduct(List<ChannelPartyToProduct> channelPartyToProductList) {
		for (ChannelPartyToProduct channelPartyToProduct : channelPartyToProductList) {
			// ChannelPartyToProductMapper channelPartyToProductMapper =
			// sqlSession.getMapper(ChannelPartyToProductMapper.class);
			// ChannelPartyToProduct foundChannelPartyToProduct =
			// channelPartyToProductMapper.readByProductId(channelPartyToProduct);
			ChannelPartyToProduct foundChannelPartyToProduct = channelPartyToProductDao.readByProductId(channelPartyToProduct);
			if (foundChannelPartyToProduct == null) {
				channelPartyToProductDao.createChannelPartyToProduct(channelPartyToProduct);
			}
			else {
				if (!foundChannelPartyToProduct.equals(channelPartyToProduct)) {
					LOG.warn("ChannelPartyToProduct exists and can not be updated." + "\n" + "Current item: " + channelPartyToProduct + "\n" + "Found item: "
							+ foundChannelPartyToProduct);
				}
			}
		}
	}

	public List<ChannelProductMap> findByBPProductsAndChannelIdAndStateAndRoomIDWithoutRoomID(ChannelProductMap tempChannelProductMap) {
		return mapper.findByBPProductsAndChannelIdAndStateAndRoomIDWithoutRoomID(tempChannelProductMap);
	}

	public List<ChannelProductMap> findByBPProductsAndChannelIdAndStateAndRoomID(ChannelProductMap tempChannelProductMap) {
		return mapper.findByBPProductsAndChannelIdAndStateAndRoomID(tempChannelProductMap);
	}

	public ChannelProductMap readBPProduct(ChannelProductMap tempChannelProductMap) {
		return mapper.readBPProduct(tempChannelProductMap);
	}

	public List<ChannelProductMap> findDuplicateByProductAndChannelId(String productId, int channelId) {

		List<ChannelProductMap> listchannelProductMap = new ArrayList<ChannelProductMap>();
		// final SqlSession sqlSession = RazorServer.openSession("uat"); //
		// replace this

		ChannelProductMap channelProductMap = new ChannelProductMap();
		channelProductMap.setChannelId(channelId);
		channelProductMap.setProductId(productId);
		ChannelProductMap channelProduct = mapper.findDuplicateByBPProductAndChannelId(channelProductMap);
		listchannelProductMap.add(channelProduct);

		return listchannelProductMap;
	}
	
	public List<ChannelProductMap> findCompleteDuplicateListByProductAndChannelId(String productId, int channelId) {
		ChannelProductMap channelProductMap = new ChannelProductMap();
		channelProductMap.setProductId(productId);
		channelProductMap.setChannelId(channelId);
		return mapper.findCompleteDuplicateListByProductAndChannelId(channelProductMap);
	}
	
	public List<ChannelProductMap> findDuplicatesByChannelId(int channelId) {
		ChannelProductMap channelProductMap = new ChannelProductMap();
		channelProductMap.setChannelId(channelId);
		return mapper.findDuplicatesByChannelId(channelProductMap);
	}

	public List<ChannelProductMap> findByProductsAndChannelIdAndState(List<String> productIds, Integer channelId, boolean open) {
		List<ChannelProductMap> listchannelProductMap = new ArrayList<ChannelProductMap>();

		try {
			ChannelProductMap channelProductMap = new ChannelProductMap();
			channelProductMap.setChannelId(channelId);
			channelProductMap.setList(productIds);
			channelProductMap.setState(open);
			if (open) {
				channelProductMap.setChannelState(ChannelProductMap.ChannelStateEnum.CHANNEL_STATE_ACTIVE);
			}
			else {
				channelProductMap.setChannelState(ChannelProductMap.ChannelStateEnum.CHANNEL_STATE_SUSPENDED);
			}
			LOG.info("channelId :" + channelId);
			LOG.info("productIds :" + productIds);
			LOG.info("open :" + open);
			listchannelProductMap = mapper.findByBPProductsAndChannelIdAndState(channelProductMap);
			LOG.info("listchannelProductMap :" + listchannelProductMap.size());
		} catch (Throwable x) {
			LOG.error(x);
		}
		return listchannelProductMap;
	}

	public List<String> findByProductsAndChannelIdAndStateForVacasa(List<String> productIds, Integer channelId, boolean open) {
		List<ChannelProductMap> listchannelProductMap = new ArrayList<ChannelProductMap>();
		List<String> listchannelProducts = new ArrayList<String>();

		try {
			ChannelProductMap channelProductMap = new ChannelProductMap();
			channelProductMap.setChannelId(channelId);
			channelProductMap.setList(productIds);
			channelProductMap.setState(open);
			if (open) {
				channelProductMap.setChannelState(ChannelProductMap.ChannelStateEnum.CHANNEL_STATE_ACTIVE);
			}
			else {
				channelProductMap.setChannelState(ChannelProductMap.ChannelStateEnum.CHANNEL_STATE_SUSPENDED);
			}
			LOG.info("channelId :" + channelId);
			LOG.info("productIds :" + productIds);
			LOG.info("open :" + open);
			listchannelProductMap = mapper.findByBPProductsAndChannelIdAndState(channelProductMap);
			for (ChannelProductMap listchannelProductMapObject : listchannelProductMap) {
				listchannelProducts.add(listchannelProductMapObject.getProductId());
			}

			LOG.info("listchannelProductMap :" + listchannelProducts.size());
		} catch (Throwable x) {
			LOG.error(x);
		}
		return listchannelProducts;
	}

	public List<ChannelProductMap> readAllProductFromChannelProductMapperNoLimit(Integer channelId, boolean state) {
		List<ChannelProductMap> listchannelProductMap = null;
		try {
			System.out.println("channelId " + channelId);
			ChannelProductMap tempChannelProductMap = new ChannelProductMap();
			tempChannelProductMap.setChannelId(channelId);
			tempChannelProductMap.setState(state);
			listchannelProductMap = mapper.readAllBPProductNoLimit(tempChannelProductMap);

		} catch (Throwable x) {
			LOG.error(x);

		}
		return listchannelProductMap;
	}

	public ChannelProductMap findByBPProductChannelIdAndChannelId(ChannelProductMap tempChannelProductMap) {
		return mapper.findByBPProductChannelIdAndChannelId(tempChannelProductMap);
	}

	public ChannelProductMap findByListingAndChannelIdForNonAirbnb(ChannelProductMap tempChannelProductMap) {
		return mapper.findByListingAndChannelIdForNonAirbnb(tempChannelProductMap);
	}

	public List<ChannelProductMap> findInActiveListingsByProductsAndChannelId(ChannelProductMap tempChannelProductMap) {
		return mapper.findInActiveListingsByProductsAndChannelId(tempChannelProductMap);
	}

	public ChannelProductMap findByBPProductAndChannelIdForNonAirbnb(ChannelProductMap tempChannelProductMap) {
		return mapper.findByBPProductAndChannelIdForNonAirbnb(tempChannelProductMap);
	}

	public List<ChannelProductMap> findByChannelProductIDAndChannelID(ChannelProductMap tempChannelProductMap) {
		return mapper.findByChannelProductIDAndChannelID(tempChannelProductMap);
	}

	public List<ChannelProductMap> findByChannelProductIDAndChannelIDWithRoomIdNotNull(ChannelProductMap tempChannelProductMap) {
		return mapper.findByChannelProductIDAndChannelIDWithRoomIdNotNull(tempChannelProductMap);
	}

	public List<ChannelProductMap> findActiveKeyChannelProductsByChannelIdAndSupplierId(NameId action) {
		return mapper.findActiveKeyChannelProductsByChannelIdAndSupplierId(action);
	}

	public List<ChannelProductMap> findInActiveKeyChannelProductsByChannelIdAndSupplierId(com.mybookingpal.utils.entity.NameId action) {
		return mapper.findInActiveKeyChannelProductsByChannelIdAndSupplierId(action);
	}

	public List<ChannelProductMap> findAllKeyChannelProductsByChannelIdAndSupplierId(NameId action) {
		return mapper.findAllKeyChannelProductsByChannelIdAndSupplierId(action);
	}

	public List<ChannelProductMap> findSuspendedKeyChannelProductsByChannelIdAndSupplierId(NameId action) {
		return mapper.findSuspendedKeyChannelProductsByChannelIdAndSupplierId(action);
	}

	public List<ChannelProductMap> findActiveMLTChannelProductsByChannelIdAndSupplierId(com.mybookingpal.utils.entity.NameId action) {
		return mapper.findActiveMLTChannelProductsByChannelIdAndSupplierId(action);
	}

	public List<ChannelProductMap> findInActiveMLTChannelProductsByChannelIdAndSupplierId(NameId action) {
		return mapper.findInActiveKeyChannelProductsByChannelIdAndSupplierId(action);
	}

	public List<ChannelProductMap> findAllMLTChannelProductsByChannelIdAndSupplierId(NameId action) {
		return mapper.findAllMLTChannelProductsByChannelIdAndSupplierId(action);
	}

	public List<ChannelProductMap> findSuspendedMLTChannelProductsByChannelIdAndSupplierId(com.mybookingpal.utils.entity.NameId action) {
		return mapper.findSuspendedKeyChannelProductsByChannelIdAndSupplierId(action);
	}

	public void suspendProducts(List<String> productIds) {
		mapper.suspendProducts(productIds);
	}

	public void activateProducts(List<String> productIds) {
		mapper.activateProducts(productIds);
	}

	public ChannelProductMap findDuplicateByBPProductAndChannelId(ChannelProductMap tempChannelProductMap) {
		return mapper.findDuplicateByBPProductAndChannelId(tempChannelProductMap);
	}

	public void updateChannelRateId(ChannelProductMap channelProductMap) {
		mapper.updateChannelRateId(channelProductMap);
	}

	public List<String> productIdsBySupplier(String supplierId, String channelId) {
		return mapper.productIdsBySupplier(supplierId, channelId);
	}

	public List<String> getAllChannelProductIdsForChannel(List<String> channelProductIds, Integer channelId) {
		return mapper.getAllChannelProductIdsForChannel(channelProductIds, channelId);
	}

	public List<ChannelProductMap> getAllProductByChannel(Integer channelId) {
		return mapper.getAllProductByChannel(channelId);
	}

	public List<ChannelProductMap> getAllProductBy2Channel(Integer channelId1, Integer channelId2) {
		return mapper.getAllProductBy2Channel(channelId1, channelId2);
	}

	public List<ChannelProductMap> getAllProductPerPmListByChannel(List<String> pmList, Integer channelId) {
		return mapper.getAllProductPerPmListByChannel(pmList, channelId);
	}

	public ChannelProductMap readByProductIdChannelProductIdChannelRoomId(ChannelProductMap tempChannelProductMap) {
		return mapper.readByProductIdChannelProductIdChannelRoomId(tempChannelProductMap);
	}

	public ChannelProductMap findByProductIdAndChannelIds(String productId, List<Integer> channelIds) {
		return mapper.findByProductIdAndChannelIds(productId, channelIds);
	}

	public ChannelProductMap findByProductIdChannelIdStateAndChannelState(String productId, int channelId, Boolean state, Integer channelState) {

		ChannelProductMap channelProductMap = new ChannelProductMap();
		channelProductMap.setChannelId(channelId);
		channelProductMap.setProductId(productId);
		channelProductMap.setState(state);
		channelProductMap.setChannelState(ChannelProductMap.ChannelStateEnum.getByInt(channelState));
		return mapper.findByProductIdChannelIdStateAndChannelState(channelProductMap);
	}

	public List<ChannelProductMap> findChannelProductsMapWithoutConsideringChannelState(ChannelProductMap tempChannelProductMap) {
		return mapper.findChannelProductsMapWithoutConsideringChannelState(tempChannelProductMap);
	}

	public void blockChannelProducts(List<String> productIds) {
		mapper.blockChannelProducts(productIds);
	}

	public List<ChannelProductMap> getBySupplierIdAndChannelId(String supplierId, String channelId) {
		return mapper.getBySupplierIdAndChannelId(supplierId, channelId);

	}

	public ChannelProductMap findByProductIdAndChannelIdAndState(String productId, Integer channelId, boolean open) {
		ChannelProductMap finalChannelProductMap = new ChannelProductMap();

		try {
			ChannelProductMap channelProductMap = new ChannelProductMap();
			channelProductMap.setChannelId(channelId);
			channelProductMap.setProductId(productId);
			channelProductMap.setState(open);
			if (open) {
				channelProductMap.setChannelState(ChannelProductMap.ChannelStateEnum.CHANNEL_STATE_ACTIVE);
			}
			else {
				channelProductMap.setChannelState(ChannelProductMap.ChannelStateEnum.CHANNEL_STATE_SUSPENDED);
			}
			LOG.info("channelId :" + channelId);
			LOG.info("productId :" + productId);
			LOG.info("open :" + open);
			finalChannelProductMap = mapper.findByBPProductIdAndChannelIdAndState(channelProductMap);
			LOG.info("finalChannelProductMap :" + finalChannelProductMap);
		} catch (Throwable x) {
			LOG.error(x);
		}
		return finalChannelProductMap;
	}

	public ChannelProductMap findByChannelProductIdChannelIdAndRateId(ChannelProductMap channelProductMap) {
		return mapper.findByChannelProductIdChannelIdAndRateId(channelProductMap);
	}

	public List<ChannelProductMap> getAllActiveProductByPmAndByChannel(@Param("supplierId") String supplierId, @Param("list") List<Integer> channelIds) {
		return mapper.getAllActiveProductByPmAndByChannel(supplierId, channelIds);
	}
	
	public ChannelProductMap getActiveChannelProductMapByProductByChannel(@Param("productId")String productId,  @Param("channelId")Integer channelId) {
		return mapper.getActiveChannelProductMapByProductByChannel(productId, channelId);
	}

	public List<ChannelProductMap> getAllProductByPmAndByChannel(@Param("supplierId") String supplierId, @Param("list") List<Integer> channelIds) {
		return mapper.getAllProductByPmAndByChannel(supplierId, channelIds);
	}

	public List<Integer> getChannelIdsByProductId(String productId) {
		return mapper.getChannelIdsByProductId(productId);
	}

	public List<Integer> getChannelIdsBySupplierId(String supplierId) {
		return mapper.getChannelIdsBySupplierId(supplierId);
	}

	public List<String> findDistributedProductsOnChannel(String supplierID, String channelId) {
		return mapper.findDistributedProductsOnChannel(supplierID, channelId);
	}

	public List<String> getAllChannelProductIdsForChannelId(Integer channelId) {
		return mapper.getAllChannelProductIdsForChannelId(channelId);
	}

	public List<ChannelProductMap> findByListingAndChannelIDdisregardingState(ChannelProductMap channelProductMap) {
		return mapper.findByListingAndChannelIDdisregardingState(channelProductMap);
	}

	public ChannelProductMap findByBPProductAndChannelForExpedia(ChannelProductMap channelProductMap) {
		return mapper.findByBPProductAndChannelForExpedia(channelProductMap);
	}

	public List<ChannelProductMap> findByChannelIdsWithProductIdChannelProductIdRoomIdRateplanIdNullAndProductIdNotNull(
			List<Integer> channelIds) {
		return mapper.findByChannelIdsWithProductIdChannelProductIdRoomIdRateplanIdNullAndProductIdNotNull(channelIds);
	}

	public List<ChannelProductMap> findByProductIdsChannelIdsWithChannelProductIdNotNullAndRateplanIdNull(List<String> productIds, List<Integer> channelIds) {
		return mapper.findByProductIdsChannelIdsWithChannelProductIdNotNullAndRateplanIdNull(productIds, channelIds);
	}

	public List<ChannelProductMap> findByProductIdsChannelIdsWithChannelProductIdRoomIdNull(List<String> productIds, List<Integer> channelIds) {
		return mapper.findByProductIdsChannelIdsWithChannelProductIdRoomIdNull(productIds, channelIds);
	}

	public List<ChannelProductMap> findByProductIdsChannelIdsWithChannelProductIdRoomIdNotNull(List<String> productIds, List<Integer> channelIds) {
		return mapper.findByProductIdsChannelIdsWithChannelProductIdRoomIdNotNull(productIds, channelIds);
	}

	public List<ChannelProductMap> findByProductIdsChannelIdsWithChannelProductIdRoomIdRateplanIdNotNull(List<String> productIds, List<Integer> channelIds) {
		return mapper.findByProductIdsChannelIdsWithChannelProductIdRoomIdRateplanIdNotNull(productIds, channelIds);
	}

	public List<ChannelProductMap> findAllChannelProductsByChannelIdAndSupplierId(NameId action) {
		return mapper.findAllChannelProductsByChannelIdAndSupplierId(action);
	}

	public List<String> getProductsOnChannelForZipCodes(ProductZipCode9DTO pzc) {
		return mapper.getProductsOnChannelForZipCodes(pzc);
	}

	public List<ChannelProductMap> getAllActiveProductsByChannel(Integer channelId) {
		return mapper.getAllActiveProductsByChannel(channelId);
	}

	public List<String> getActiveSupplierIdsByChannel(Integer channelId) {
		return mapper.getActiveSupplierIdsByChannel(channelId);
	}

	public List<ChannelProductMap> findByChannelIdsProductIds(@Param("productIds") List<String> productIds, @Param("list") List<Integer> channelIds) {
		return mapper.findByChannelIdsProductIds(productIds, channelIds);
	}

	public List<ChannelProductMap> findByBPProductsAndChannelIdAndState(ChannelProductMap channelProductMap) {
		return mapper.findByBPProductsAndChannelIdAndState(channelProductMap);
	}

	public List<ChannelProductMap> findByBPProductAndChannelIdWithoutStates(ChannelProductMap channelProductMap) {
		return mapper.findByBPProductAndChannelIdWithoutStates(channelProductMap);
	}

	public List<String> findAllMLTChannelProductsByChannelIdAndParentList(@Param("ownList") List<String> ownList, @Param("channelId") String channelId) {
		return mapper.findAllMLTChannelProductsByChannelIdAndParentList(ownList, channelId);
	}
	
	public String findReviewStatusByProductID(@Param("productId") Integer productId, @Param("channelId") Integer channelId) {
		return mapper.findReviewStatusByProductID(productId, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelProductMap findByProductIdAndChannelAbbreviation(Integer productId, String abbreviation) {
		return mapper.findByProductIdAndChannelAbbreviation(productId, abbreviation);
	}

	public List<ChannelProductMap> readAllBPProductNoLimitNoStateForSupplier(ChannelProductMap channelProductMap) {
		return mapper.readAllBPProductNoLimitNoStateForSupplier(channelProductMap);
	}


	public List<ChannelProductMap> findActiveRoomsOfHotel(ChannelProductMap channelProductMap){
		return mapper.findActiveRoomsOfHotel(channelProductMap);
	}

	public boolean isLosPriceEnable(String productId, String channelId) {
		return mapper.isLosPriceEnable(productId, channelId);
	}

	public List<ChannelProductMap> findActiveListingByChannelId(ChannelProductMap channelProductMap) {
		return mapper.findActiveListingByChannelId(channelProductMap);
	}

	public Integer findChannelProductIdByProductOwnerIdAndChannelId(ChannelProductMap channelProductMap) {
		return mapper.findChannelProductIdByProductOwnerIdAndChannelId(channelProductMap);
	}

	public void setLosPriceEnable(String productId, String channelId, boolean losPriceEnable) {
		mapper.setLosPriceEnable(productId, channelId, losPriceEnable);
	}

	public List<ChannelProductMap> findAllSwithoverPropertiesBySupplierIDAndChannelId(Integer supplierId,Integer channelId) {
		return mapper.findAllSwithoverPropertiesBySupplierID(supplierId, channelId);
	}

	public List<ChannelProductMap> findByReviewStatusAndChannelIDForSupplierApi(String reviewStatus, Integer channelId) {
		return mapper.findByReviewStatusAndChannelIDForSupplierApi(reviewStatus, channelId);
	}

	public List<ExpediaInactiveReport> allPropertiesOnExpedia() {
		return mapper.allPropertiesOnExpedia();
	}

	public List<IdVersion> getMaxVersions(List<Integer> productIds) {
		return mapper.getMaxVersions(productIds);
	}

	public ChannelProductMap readById(Long id) {
		return mapper.readById(id);
	}

	public ChannelProductMap readByProductIdAndChannelId(Integer productId, Integer channelId) {
		return mapper.readByProductIdAndChannelId(productId, channelId);
	}

	public ChannelProductMap readByChannelProductIdAndChannelId(String channelProductId, Integer channelId) {
		return mapper.readByChannelProductIdAndChannelId(channelProductId, channelId);
	}

	public void delete(ChannelProductMap channelProductMap) {
		mapper.delete(channelProductMap);
	}

	public List<ChannelProductMap> findAllMLTbyParentAndChannelId(Integer parentId, String channelId) {
		return mapper.findAllMLTbyParentAndChannelId(parentId, channelId);
	}

}
