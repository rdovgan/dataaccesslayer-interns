package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.shared.Text;

import java.util.List;

public interface TextDao {

	Text notes(String id, String language);

	Text readByID(String id);

	List<Text> readallbyid(String id);
}
