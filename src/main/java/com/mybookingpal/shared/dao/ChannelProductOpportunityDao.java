package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelProductOpportunityMapper;
import com.mybookingpal.dal.shared.ChannelProductOpportunity;
import com.mybookingpal.shared.dto.ChannelProductOpportunityDto;
import com.mybookingpal.shared.dto.OpportunityFilterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ChannelProductOpportunityDao {

	@Autowired ChannelProductOpportunityMapper mapper;


	public ChannelProductOpportunity readById(Integer id) {
		return mapper.readById(id);
	}

	public List<ChannelProductOpportunity> readByExample(ChannelProductOpportunity example) {
		return mapper.readByExample(example);
	}

	public void create(ChannelProductOpportunity channelProductOpportunity) {
		mapper.create(channelProductOpportunity);
	}

	public void update(ChannelProductOpportunity action) {
		mapper.update(action);
	}

	public List<ChannelProductOpportunity> readByProductId(Integer productId) {
		return mapper.readByProductId(productId);
	}

	public List<ChannelProductOpportunityDto> readByFilters(OpportunityFilterDto opportunityFilter) {
		return mapper.readByFilters(opportunityFilter);
	}

	public List<ChannelProductOpportunityDto> readForCSVByFilters(OpportunityFilterDto opportunityFilter) {
		return mapper.readForCSVByFilters(opportunityFilter);
	}

	public List<ChannelProductOpportunity> readAllByChannelId(Integer channelId){
		return mapper.readAllByChannelId(channelId);
	}

	public void delete(Integer id){
		mapper.deleteChannelProductOpportunity(id);
	}

	public Integer readNumberByFilters(OpportunityFilterDto opportunityFilter) {
		return mapper.readNumberByFilters(opportunityFilter);
	}

	public List<ChannelProductOpportunity> readByOpportunityId (Integer opportunityId){
		return mapper.readByOpportunityId(opportunityId);
	}

	public List<ChannelProductOpportunity> readProductByOpportunityId(Integer opportunityId, List<Integer> productIds, Integer limit, Integer page){
		return mapper.readProductByOpportunityId(opportunityId, productIds, limit, page);
	}

	public List<ChannelProductOpportunity> readPropertiesByOpportunityId(Integer opportunityId){
		return mapper.readPropertiesByOpportunityId(opportunityId);
	}
}
