package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.dao.entities.ReservationModel;
import com.mybookingpal.dal.json.Parameter;
import com.mybookingpal.dal.rest.response.CalendarElement;
import com.mybookingpal.dal.server.api.ReservationMapper;
import com.mybookingpal.dal.shared.IdVersion;
import com.mybookingpal.dal.shared.RoomPeriodChange;
import com.mybookingpal.dal.shared.action.DatesIdAction;
import com.mybookingpal.dal.shared.api.HasProductIdAndDateRange;
import com.mybookingpal.dal.shared.beststayz.BeststayzTransactionReportModel;
import com.mybookingpal.dal.shared.marriott.GuestDecryptedInfoDto;
import com.mybookingpal.dal.shared.marriott.ChannelPortalReservationReport;
import com.mybookingpal.dal.shared.marriott.ChannelPortalAnalyticInfo;
import com.mybookingpal.dal.shared.marriott.ChannelPortalAnalyticRequest;
import com.mybookingpal.dal.shared.marriott.ChannelPortalFilterDto;
import com.mybookingpal.dal.shared.marriott.ChannelPortalGuestFolioInfo;
import com.mybookingpal.dal.shared.marriott.MarriottMedalliaPostStaySurveyInfo;
import com.mybookingpal.dal.shared.marriott.ChannelPortalPayoutReport;
import com.mybookingpal.dal.shared.marriott.ChannelPortalPointsRedemptionReport;
import com.mybookingpal.dal.shared.marriott.ChannelPortalReconciliationReport;
import com.mybookingpal.dal.shared.marriott.ChannelPortalTransactionReport;
import com.mybookingpal.dal.shared.marriott.MedalliaGuestReservationInfo;
import com.mybookingpal.dal.shared.marriott.MedalliaSurveyInfo;
import com.mybookingpal.dal.shared.marriott.PaymentAndStayInfo;
import com.mybookingpal.dal.shared.marriott.PostStayEmailInfoDto;
import com.mybookingpal.dal.shared.marriott.ReservationDatesAvailability;
import com.mybookingpal.shared.dto.BeststayzTransactionReportDto;
import com.mybookingpal.shared.dto.ReservationFilterDto;
import com.mybookingpal.shared.dto.ReservationReport;
import com.mybookingpal.shared.dto.ReservationReportGropedByParam;
import com.mybookingpal.shared.dto.ReservationReportRequestDto;
import com.mybookingpal.shared.dto.ReservationYearsInfo;
import com.mybookingpal.utils.entity.NameId;
import org.apache.commons.collections4.CollectionUtils;
import com.mybookingpal.shared.dto.StripeReconcilationReportDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Repository
public class ReservationDao {

	@Autowired
	protected ReservationMapper reservationMapper;

	public ReservationMapper getReservationMapper() {
		return reservationMapper;
	}

	public void update(ReservationModel reservationModel) {
		reservationMapper.update(reservationModel);
	}

	public List<CalendarElement> calendarelement(Parameter action) {
		return reservationMapper.calendarelement(action);
	}

	public List<ReservationModel> cancelledDatesForPropertyid(ReservationModel action) {
		return reservationMapper.cancelledDatesForPropertyid(action);
	}

	public List<ReservationModel> readCancelledBasedOnTimeBySupplier(ReservationModel tmp) {
		return reservationMapper.readCancelledBasedOnTimeBySupplier(tmp);
	}

	public List<ReservationModel> readInquiryOlderThenTime(ReservationModel tmp) {
		return reservationMapper.readInquiryOlderThenTime(tmp);
	}

	public List<ReservationModel> productActiveReservations(Integer productId) {
		return reservationMapper.productActiveReservations(productId);
	}

	public List<ReservationModel> readActiveBasedOnTimeBySupplier(ReservationModel tmp) {
		return reservationMapper.readActiveBasedOnTimeBySupplier(tmp);
	}

	public List<ReservationModel> reserveredDatesForPropertyid(ReservationModel action) {
		return reservationMapper.reserveredDatesForPropertyid(action);
	}

	public List<ReservationModel> reservedDatesForMultipleProperties(List<Integer> productIdList, Date fromDate, Date toDate) {
		return reservationMapper.reservedDatesForMultipleProperties(productIdList,fromDate,toDate);
	}
	
	public List<ReservationModel> readClosedReservationsForCancelledDates(ReservationModel tmp) {
		return reservationMapper.readClosedReservationsForCancelledDates(tmp);
	}

	public ReservationModel readMaxMinClosedReservationsForCancelledDates(ReservationModel tmp) {
		return reservationMapper.readMaxMinClosedReservationsForCancelledDates(tmp);
	}

	public List<ReservationModel> fetchReservationForAChild(Integer productId) {
		return reservationMapper.getSchedule(productId);
	}

	public List<ReservationModel> readReservationsForADateRange(ReservationModel tmp) {
		return reservationMapper.readReservationsForADateRange(tmp);
	}

	public ReservationModel read(Integer id) {
		return reservationMapper.read(id);
	}

	public List<ReservationModel> readBasedOnTime(String lastFetch) {
		return reservationMapper.readBasedOnTime(lastFetch);
	}

	public List<ReservationModel> listReservationsByPropertyidAndVersion(ReservationModel tmp) {
		return reservationMapper.listReservationsByPropertyidAndVersion(tmp);
	}

	public List<ReservationModel> readReservationByAgentAndAltId(ReservationModel bpReservation) {
		return reservationMapper.readReservationByAgentAndAltId(bpReservation);
	}

	public ReservationModel offlineReservationRead(Integer reservationId) {
		return reservationMapper.offlineReservationRead(reservationId);
	}

	public List<ReservationModel> readActiveBasedOnTimeBySupplierAndChannel(ReservationModel reservation) {
		return reservationMapper.readActiveBasedOnTimeBySupplierAndChannel(reservation);
	}
	
	public List<RoomPeriodChange> readReservationForPeriodForAllStateBySupplierIdByVersion(String supplierId,Date fromDate,Date toDate,Date version,Integer channelId){
		return reservationMapper.readReservationForPeriodForAllStateBySupplierIdByVersion(supplierId,fromDate,toDate,version,channelId);
	}

	public List<ReservationModel> readActiveBasedOnTimeBySupplierAndChannelWithoutProductId(ReservationModel reservation) {
		return reservationMapper.readActiveBasedOnTimeBySupplierAndChannelWithoutProductId(reservation);
	}

	public List<ReservationModel> findAllActiveKeysOfMLTByReservationVersion(String parentId, Date version) {
		return reservationMapper.findAllActiveKeysOfMLTByReservationVersion(parentId, version);
	}

	public ReservationModel collides(Integer productId, LocalDate fromDate, LocalDate toDate) {
		ReservationModel action = new ReservationModel();
		action.setProductId(productId);
		action.setFromDate(fromDate);
		action.setToDate(toDate);
		return reservationMapper.collides(action);
	}

	public List<IdVersion> getMaxAvailabilityVersions(List<Integer> productIds, Set<Integer> channelIds) {
		return reservationMapper.getMaxAvailabilityVersions(productIds, channelIds);
	}

	public List<IdVersion> getMaxAvailabilityVersionsWithoutMlts(List<Integer> productIds, Set<Integer> channelIds) {
		return reservationMapper.getMaxAvailabilityVersionsWithoutMlts(productIds, channelIds);
	}

	public void deleteDate(NameId action) {
		reservationMapper.deleteDate(action);
	}

	public List<DatesIdAction> getCollisionItems(HasProductIdAndDateRange hasProductIdAndDateRange) {
		return reservationMapper.collisionItems(hasProductIdAndDateRange);
	}

	public List<ReservationModel> collisionItemsForDateRange(HasProductIdAndDateRange hasProductIdAndDateRange) {
		return reservationMapper.collisionItemsForDateRange(hasProductIdAndDateRange);
	}

	public void insertList(List<ReservationModel> reservations) {
		reservationMapper.insertList(reservations);
	}

	public void insertList(List<String> reservationIds, Date version) {
		reservationMapper.cancelreservationlist(reservationIds, version);
	}

	public void cancelVersionByDate(ReservationModel action) {
		reservationMapper.cancelversionbydate(action);
	}

	public List<com.mybookingpal.utils.entity.NameId> getCollisions(HasProductIdAndDateRange hasProductIdAndDateRange) {
		return reservationMapper.collisions(hasProductIdAndDateRange);
	}

	public Integer getAvailableRoomsCountForMltKeyRoomsByDay(List<Integer> subProducts, LocalDate day) {
		if (CollectionUtils.isEmpty(subProducts)) {
			return 0;
		}
		return reservationMapper.getAvailableRoomsCountForMltKeyRoomsByDay(subProducts, day);
	}

	public List<ReservationReportRequestDto> getReportDataByDateAndSearchTypeAndProductId(Date fromDate, Date toDate, Integer productId, Integer pmId,
			String searchType) {
		return reservationMapper.getReportDataByDateAndSearchTypeAndProductId(fromDate, toDate, productId, pmId, searchType);
	}

	public List<ReservationReportGropedByParam> getReportDataByDateAndSearchTypeAndProductIdGroupByDateAndChannel(Date fromDate, Date toDate, Integer productId,
			Integer pmId, String searchType, String range) {
		return reservationMapper.getReportDataByDateAndSearchTypeAndProductIdGroupByDateAndChannel(fromDate, toDate, productId, pmId, searchType, range);
	}

	public List<ReservationReportGropedByParam> getReportDataByDateAndSearchTypeAndProductIdGroupByDate(Date fromDate, Date toDate, Integer productId,
			Integer pmId, String searchType, String range) {
		return reservationMapper.getReportDataByDateAndSearchTypeAndProductIdGroupByDate(fromDate, toDate, productId, pmId, searchType, range);
	}

	public List<ReservationReport> readAllReservationByFilter(ReservationFilterDto reservationFilter) {
		return reservationMapper.readAllReservationByFilter(reservationFilter);
	}

	public List<ReservationYearsInfo> getReservationYears(Integer pmId) {
		return reservationMapper.getReservationYears(pmId);
	}

	public List<BeststayzTransactionReportModel> getTransactionReport(BeststayzTransactionReportDto beststayzTransactionReport) {
		return reservationMapper.getTransactionReport(beststayzTransactionReport);
	}

	public List<BeststayzTransactionReportModel> getTransactionReportForExpedia(BeststayzTransactionReportDto beststayzTransactionReport) {
		return reservationMapper.getTransactionReportForExpedia(beststayzTransactionReport);
	}

	public ReservationModel getChannelPortalReservation(Integer reservationId, Integer channelId) {
		return reservationMapper.getChannelPortalReservation(reservationId, channelId);
	}

	public List<ChannelPortalReservationReport> getChannelPortalReservations(ChannelPortalFilterDto filter) {
		return reservationMapper.getChannelPortalReservations(filter);
	}

	public List<ReservationDatesAvailability> getReservationsByProductIdAndDates(Integer productId, Date fromDate, Date toDate) {
		return reservationMapper.getReservationsByProductIdAndDates(productId, fromDate, toDate);
	}

	public List<ChannelPortalTransactionReport> getChannelPortalTransactionReport(ChannelPortalFilterDto filter) {
		return reservationMapper.getChannelPortalTransactionReport(filter);
	}

	public Integer getCountOfChannelPortalTransactionForReport(ChannelPortalFilterDto filter) {
		return reservationMapper.getCountOfChannelPortalTransactionForReport(filter);
	}

	public List<Integer> getChannelPortalReservationForReconciliationReport(Integer channelId) {
		return reservationMapper.getChannelPortalReservationForReconciliationReport(channelId);
	}

	public List<ChannelPortalReconciliationReport> getChannelPortalReconciliationReport(List<Integer> reservationIds, LocalDate startDate, LocalDate endDate, Integer page, Integer limit) {
		return reservationMapper.getChannelPortalReconciliationReport(reservationIds, startDate, endDate, page, limit);
	}

	public Integer getCountOfChannelPortalReconciliationForReport(List<Integer> reservationIds, LocalDate startDate, LocalDate endDate) {
		return reservationMapper.getCountOfChannelPortalReconciliationForReport(reservationIds, startDate, endDate);
	}

	public List<ChannelPortalPayoutReport> getChannelPortalPayoutReport(ChannelPortalFilterDto filter) {
		return reservationMapper.getChannelPortalPayoutReport(filter);
	}

	public List<ChannelPortalPointsRedemptionReport> getChannelPortalPointsRedemptionReport(ChannelPortalFilterDto filter) {
		return reservationMapper.getChannelPortalPointsRedemptionReport(filter);
	}

	public Integer getCountOfChannelPortalPointsRedemptionReport(ChannelPortalFilterDto filter) {
		return reservationMapper.getCountOfChannelPortalPointsRedemptionReport(filter);
	}

	public Integer getCountOfChannelPortalPayoutReport(ChannelPortalFilterDto filter) {
		return reservationMapper.getCountOfChannelPortalPayoutReport(filter);
	}

	public GuestDecryptedInfoDto getPointsRedemptionInfoByReservationId(Integer reservationId) {
		return reservationMapper.getPointsRedemptionInfoByReservationId(reservationId);
	}

	public List<ReservationModel> getAllException(ReservationFilterDto filter) {
		return reservationMapper.readAllReservationsWithException(filter);
	}

	public PaymentAndStayInfo readAllPaymentAndStayInfo(Integer reservationId, Integer productId) {
		return reservationMapper.readAllPaymentAndStayInfo(reservationId, productId);
	}

	public List<Long> checkAndGetReservationIdsByChannel(List<Long> reservationIds, Integer channelId) {
		return reservationMapper.checkAndGetReservationIdsByChannel(reservationIds, channelId);
	}

	public List<PostStayEmailInfoDto> getInfoForPostStayEmails() {
		return reservationMapper.getInfoForPostStayEmails();
	}

	public List<ReservationModel> readReservations(List<Long> reservations) {
		return reservationMapper.readReservations(reservations);
	}

	public MedalliaGuestReservationInfo getGuestReservationDataForMedallia( Integer reservationId) {
		return reservationMapper.getGuestReservationDataForMedallia(reservationId);
	}

	public List<StripeReconcilationReportDto> getAllReservationsPaidViaStripeByDateAndPmId(ReservationFilterDto filter) {
		return reservationMapper.getAllReservationsPaidViaStripeByDateAndPmId(filter);
	}

	public List<ChannelPortalAnalyticInfo> getChannelPortalAnalyticReportInfo(ChannelPortalAnalyticRequest request) {
		return reservationMapper.getChannelPortalAnalyticReportInfo(request);
	}

	public List<MedalliaSurveyInfo> getMedalliaSurveyLinksInfo(List<Integer> ids) {
		return reservationMapper.getMedalliaSurveyLinksInfo(ids);
	}

	public ChannelPortalGuestFolioInfo getChannelPortalGuestFolioInfo(Integer reservationId) {
		return reservationMapper.getChannelPortalGuestFolioInfo(reservationId);
	}

	public List<MarriottMedalliaPostStaySurveyInfo> getReservationsInfoForMedalliaPostStaySurvey() {
		return reservationMapper.getReservationsInfoForMedalliaPostStaySurvey();
	}

	public List<ReservationModel> getChannelReservations(Integer agentId){
		return reservationMapper.getChannelReservations(agentId);
	}

	public void cancelClosedReservations(Integer productId) {
		reservationMapper.cancelClosedReservations(productId);
	}
}
