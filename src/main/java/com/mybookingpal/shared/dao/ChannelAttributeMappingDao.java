package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelAttributeMappingMapper;
import com.mybookingpal.dal.shared.ChannelAttributeMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ChannelAttributeMappingDao {

	@Autowired
	ChannelAttributeMappingMapper channelAttributeMappingMapper;

	public List<ChannelAttributeMapping> readbyCodes(List<String> attributeCodes, int channelId) {
		return channelAttributeMappingMapper.readbyCodes(attributeCodes, channelId);
	}

	public List<ChannelAttributeMapping> readByExample(ChannelAttributeMapping example) {
		return channelAttributeMappingMapper.readByExample(example);
	}

	public List<ChannelAttributeMapping> readExistingListByCodesAndChannelId(List<String> attributeCodes, Integer channelId) {
		return channelAttributeMappingMapper.readExistingListByCodesAndChannelId(attributeCodes, channelId);
	}

	public void create(ChannelAttributeMapping channelAttributeMapping) {
		channelAttributeMappingMapper.create(channelAttributeMapping);
	}

}
