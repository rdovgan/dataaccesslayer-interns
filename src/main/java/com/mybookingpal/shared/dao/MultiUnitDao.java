package com.mybookingpal.shared.dao;

import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.dao.entities.ReservationModel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.PriceMapper;
import com.mybookingpal.dal.server.api.ProductMapper;
import com.mybookingpal.dal.server.api.PropertyMinStayMapper;
import com.mybookingpal.dal.server.api.ReservationMapper;
import com.mybookingpal.dal.server.api.RestrictionMapper;
import com.mybookingpal.dal.server.api.YieldMapper;
import com.mybookingpal.dal.shared.Price;
import com.mybookingpal.dal.shared.Product;
import com.mybookingpal.dal.shared.ProductAction;
import com.mybookingpal.dal.shared.PropertyMinStay;
import com.mybookingpal.dal.shared.Restriction;
import com.mybookingpal.dal.shared.Yield;

@Repository
public class MultiUnitDao {

	@Autowired
	ProductMapper productMapper;

	@Autowired
	PriceMapper priceMapper;

	@Autowired
	YieldMapper yieldMapper;

	@Autowired
	PropertyMinStayMapper propertyMinStayMapper;

	@Autowired
	RestrictionMapper restrictionMapper;
	@Autowired
	ReservationMapper reservationMapper;

	private static final Logger LOG = Logger.getLogger(MultiUnitDao.class.getName());

	public List<Product> fetchAllMasterForASupplier(String supplierId) {
		return productMapper.fetchAllMasterForASupplier(supplierId);
	}

	public List<Product> findAllFinalizedMLTsForASupplier(ProductAction productAction) {
		return productMapper.findAllFinalizedMLTsForASupplierByVersion(productAction);
	}

	public List<Product> fetchAllCreatedMasterForASupplierByVersion(ProductAction productAction) {
		return productMapper.fetchAllCreatedMasterBySupplierIdByVersion(productAction);
	}

	public List<Product> findAllReactivatedParentsForASupplierByVersion(ProductAction action) {
		return productMapper.findAllReactivatedParentsForASupplierByVersion(action);
	}

	public List<Product> findAllActiveKeysOfMLT(String parentId) {
		return productMapper.getProductsByParentId(parentId);
	}

	public List<ReservationModel> findAllActiveKeysOfMLTByReservationVersion(String parentId, Date version) {
		return reservationMapper.findAllActiveKeysOfMLTByReservationVersion(parentId, version);
	}

	public List<Product> fetchParentsBySupplierId(String supplierId) {
		return productMapper.fetchAllParentForASupplier(supplierId);
	}

	public List<Product> fetchAllFinalizedParentsBySupplierId(ProductAction productAction) {
		return productMapper.fetchAllFinalizedParentForASupplierByVersion(productAction);
	}

	public List<Price> fetchAllFinalizedPriceForAParent(Price price) {
		return priceMapper.readByEntityIdAndEntityTypeOnlyFinal(price);
	}

	public List<Price> fetchAllCreatedPriceForAParent(Price price) {
		return priceMapper.readByEntityIdAndEntityType(price);
	}

	public List<Yield> fetchAllFinalizedYieldForAParent(Yield yield) {
		return yieldMapper.readByEntityIdAndEntityTypeOnlyFinal(yield);
	}

	public List<Yield> fetchAllCreatedYieldForAParent(Yield yield) {
		return yieldMapper.readByEntityIdAndEntityType(yield);
	}

	public List<PropertyMinStay> fetchAllFinalizedMinStayForAParent(PropertyMinStay propertyMinStay) {
		return propertyMinStayMapper.readByEntityIdAndEntityTypeOnlyFinal(propertyMinStay);
	}

	public List<PropertyMinStay> fetchAllCreatedMinStayForAParent(PropertyMinStay propertyMinStay) {
		return propertyMinStayMapper.readByEntityIdAndEntityType(propertyMinStay);
	}

	public List<Restriction> fetchAllFinalizedRestrictionForAParent(Restriction restriction) {
		return restrictionMapper.readByEntityIdAndEntityTypeOnlyFinal(restriction);
	}

	public List<Restriction> readByProductIdAndDateRangeOnlyFinal(Restriction restriction) {
		return restrictionMapper.readBySupplierIdAndDateRangeOnlyFinal(restriction);
	}

	public List<Restriction> fetchAllCreatedRestrictionForAParent(Restriction restriction) {
		return restrictionMapper.readByEntityIdAndEntityType(restriction);
	}

	public List<Product> findAllFinalizedKeysForMLT(ProductAction productAction) {
		return productMapper.findAllFinalizedKeysForMLTByVersion(productAction);
	}

	public List<Product> findAllCreatedKeysForAMLT(ProductAction productAction) {
		return productMapper.fetchAllCreatedChildByParentIdByVersion(productAction);
	}

	public List<String> findAllActiveKeysByParentIds(List<String> productIdList) {
		return productMapper.getProductIdsByParentIds(productIdList);
	}

	public List<Product> findAllFinalizedMLTsForOwnList(ProductAction productAction) {

		return productMapper.getFinilizedMlts(productAction);
	}
	
	public List<Product> findAllFinalizedMLTs(ProductAction productAction) {

		return productMapper.getFinilizedMltsByMLTList(productAction);
	}

	public List<Product> fetchAllCreatedMasterForOwnListByVersion(ProductAction productAction) {

		return productMapper.fetchAllCreatedMasterForOwnListByVersion(productAction);

	}
	
	public List<Product> fetchAllCreatedMasterByVersion(ProductAction productAction) {

		return productMapper.fetchAllCreatedMasterByVersion(productAction);

	}
}
