package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.shared.NameIdAction;
import com.mybookingpal.dal.shared.TimeZone;

import java.util.List;

public interface TimeZoneDao {
	/**
	 * Gets all time zones from time_zone table using {@link NameIdAction#getOffsetrows()} and {@link NameIdAction#getNumrows()} for limiting number of rows
	 *
	 * @param limit settings for limiting number of rows
	 * @return {@link NameIdAction#getNumrows()} time zones from time_zone table from {@link NameIdAction#getOffsetrows()}
	 * @throws IllegalArgumentException if sqlSession or limit is null
	 */
	List<TimeZone> readAll(NameIdAction limit);

	/**
	 * Gets all time zones that match example
	 *
	 * @param example example for reading by. Is not null.
	 * @return all time zones that match example
	 * @throws IllegalArgumentException if sqlSession or example is null
	 */
	List<TimeZone> readByExample(TimeZone example);

	/**
	 * Reads time zone by specified id from database
	 *
	 * @param id id of time zone in database
	 * @return time zone if it's found by specified id, otherwise returns null
	 * @throws IllegalArgumentException if sqlSession is null
	 */
	TimeZone read(Integer id);

	/**
	 * Inserts time zone to time_zone table. ID of specified time zone is not using here. This method should only be used by service methods.
	 *
	 * @param timeZone time zone that have to be inserted. Is not null.
	 * @throws IllegalArgumentException if sqlSession or timeZone is null
	 */
	void create(TimeZone timeZone);
}
