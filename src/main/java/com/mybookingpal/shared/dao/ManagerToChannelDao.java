package com.mybookingpal.shared.dao;

import java.util.ArrayList;
import java.util.List;

import com.mybookingpal.dal.shared.ProductAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ManagerToChannelMapper;
import com.mybookingpal.dal.shared.ChannelDistribution;
import com.mybookingpal.dal.shared.ManagerToChannel;
import com.mybookingpal.utils.entity.NameId;

@Repository
public class ManagerToChannelDao {

	@Autowired
	ManagerToChannelMapper managerToChannelMapper;

	public ArrayList<ManagerToChannel> listAllChannelPropertyManagers() {
		return managerToChannelMapper.listAllChannelPropertyManagers();
	}

	public ArrayList<ChannelDistribution> propertyManagersDistributingToChannels() {
		return managerToChannelMapper.propertyManagersDistributingToChannels();
	}

	public ArrayList<ManagerToChannel> listPropertyManagersDistributedChannel(String channel_id) {
		return managerToChannelMapper.listPropertyManagersDistributedChannel(channel_id);
	}

	public ArrayList<String> listPropertyManagersNotDistributedChannel(String channel_id) {
		return managerToChannelMapper.listPropertyManagersNotDistributedChannel(channel_id);
	}

	public List<String> justPropertyManagersDistributedChannel(String channel_id) {
		return managerToChannelMapper.justPropertyManagersDistributedChannel(channel_id);
	}

	public List<String> listPMNotDistributedToChannelString(List<String> supplierIds) {
		return managerToChannelMapper.listPMNotDistributedToChannelString(supplierIds);
	}

	public List<ManagerToChannel> listFeed() {
		return managerToChannelMapper.listFeed();
	}

	public ManagerToChannel getByPmIDAndChannelID(com.mybookingpal.utils.entity.NameId nameId) {
		return managerToChannelMapper.getByPmIDAndChannelID(nameId);
	}

	public ManagerToChannel getByPmIDAndChannel(NameId nameId) {
		return managerToChannelMapper.getByPmIDAndChannelID(nameId);
	}

	public List<ManagerToChannel> readAll() {
		return managerToChannelMapper.readAll();
	}
	
	public List<Integer> getChannelPartnerIdsByPmId(String id){
		return managerToChannelMapper.getChannelPartnerIdsByPmId(id);
	}

	public List<String> justPMCreatedOrNotChangedInAWhile(ProductAction channelId) {
		return managerToChannelMapper.justPMCreatedOrNotChangedInAWhile(channelId);
	}

	@Deprecated
	public void updateFeeNetRateByPropertyManagerAndChannelPartner(Integer pmId, Integer cpId, Boolean isNetRate){
		managerToChannelMapper.updateFeeNetRateByPropertyManagerAndChannelPartner(pmId, cpId, isNetRate);
	}

	public List<ManagerToChannel> findSupplierForExpedia(Integer supplierId) {
		return managerToChannelMapper.findSupplierForExpedia(supplierId);
	}
}
