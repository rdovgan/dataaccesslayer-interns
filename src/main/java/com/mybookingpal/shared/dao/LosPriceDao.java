package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.LosPriceMapper;
import com.mybookingpal.dal.shared.IdDateDto;
import com.mybookingpal.dal.shared.IdVersion;
import com.mybookingpal.dal.shared.LocalDateRange;
import com.mybookingpal.dal.shared.LosPrice;
import com.mybookingpal.dal.shared.ProductToLosPrice;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Repository
public class LosPriceDao {

	@Autowired
	private LosPriceMapper losPriceMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Boolean isSupported(Integer productId) {
		return losPriceMapper.isSupported(productId);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Boolean isSupport(Integer productId) {
		return losPriceMapper.isSupport(productId);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setIsSupported(Integer productId, boolean isSupported) {
		losPriceMapper.setIsSupported(productId, isSupported);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LosPrice> readByProductId(Integer productId) {
		return losPriceMapper.readByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LosPrice> readById(List<Long> listOfId) {
		if(CollectionUtils.isEmpty(listOfId)) {
			return Collections.emptyList();
		}
		return losPriceMapper.readById(listOfId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LosPrice> readByProductIdAndCheckInDate(Integer productId, Date checkInDate) {
		return losPriceMapper.readByProductIdAndCheckInDate(productId, checkInDate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public LosPrice readByParamsAndClosestMaxGuest(Integer productId, Date checkInDate, Integer los, Integer maxGuests) {
		return losPriceMapper.readByParamsAndClosestMaxGuest(productId, checkInDate, los, maxGuests);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LosPrice> readByProductIdAndCheckinAndClosestMaxGuest(Integer productId, LocalDate checkInDate, Integer numberOfQuests) {
		return losPriceMapper.readByProductIdAndCheckinAndClosestMaxGuest(productId, checkInDate, numberOfQuests);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LosPrice> readByProductIdAndCheckInAndMaxGuest(Integer productId, LocalDate checkInDate, Integer maxGuest) {
		return losPriceMapper.readByProductIdAndCheckInAndMaxGuest(productId, checkInDate, maxGuest);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertList( List<LosPrice> losPriceList) {
		if (CollectionUtils.isEmpty(losPriceList)) {
			return;
		}
		losPriceMapper.insertList(losPriceList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(LosPrice losPrice) {
		if (losPrice == null) {
			return;
		}
		losPriceMapper.update(losPrice);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertOrUpdateList(List<LosPrice> losPriceList) {
		if (losPriceList == null) {
			return;
		}
		losPriceMapper.insertOrUpdateList(losPriceList);
	}

	//@Deprecated // use archive() instead
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setToFinal(List<Long> listToFinal) {
		if (CollectionUtils.isEmpty(listToFinal)) {
			return;
		}
		losPriceMapper.setToFinal(listToFinal);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void archive(List<Long> idsToArchive) {
		if (CollectionUtils.isEmpty(idsToArchive)) {
			return;
		}
		losPriceMapper.copyToArchiveTable(idsToArchive);
		losPriceMapper.deleteByIds(idsToArchive);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LosPrice> readByProductIdAndDates(Integer productId, List<LocalDateRange> dates) {
		return losPriceMapper.readByProductIdAndDates(productId, dates);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Integer> readSupportedProductIdsWithActiveLosPricesByLocationId(Integer locationId, LocalDate fromDate, LocalDate toDate) {
		return losPriceMapper.readSupportedProductIdsWithActiveLosPricesByLocationId(locationId, fromDate, toDate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<IdVersion> getMaxVersionProductToLosVersion(List<Integer> productIds) {
		return losPriceMapper.getMaxVersionProductToLosVersion(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public LocalDateTime getProductToLosVersion(Integer productId) {
		return losPriceMapper.getProductToLosVersion(productId);
	}

		@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LosPrice> getByProductIdAndVersion(Integer productId, LocalDateTime version) {
		return losPriceMapper.getByProductIdAndVersion(productId, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LosPrice> getHistoryByProductIdAndVersion(Integer productId, LocalDateTime version) {
		return losPriceMapper.getHistoryByProductIdAndVersion(productId, version);
	}

	public void copyLosPrices(Integer fromProductId, Integer toProductId) {
		losPriceMapper.copyLosPrices(fromProductId, toProductId);
	}

	//@Deprecated // use archiveByProductId() instead
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void makeFinalByProductId(String productId) {
		if (StringUtils.isBlank(productId)) {
			return;
		}
		losPriceMapper.makeFinalByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void archiveByProductId(String productId) {
		if (StringUtils.isBlank(productId)) {
			return;
		}
		losPriceMapper.copyToArchiveTableByProductId(productId);
		losPriceMapper.deleteByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ProductToLosPrice> getProductToLosByProductIds(List<Integer> productIds) {
		return losPriceMapper.getProductToLosByProductIds(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductToLosPrice getProductToLosByProductId(Integer productId) {
		return losPriceMapper.getProductToLosByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<IdVersion> getMaxRateVersions(List<Integer> productIds) {
		return losPriceMapper.getMaxRateVersions(productIds);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<IdVersion> getMaxAvailabilityVersions(List<Integer> productIds, Integer channelId) {
		return losPriceMapper.getMaxAvailabilityVersions(productIds, channelId);
	}


	public void deleteSupportedLosPriceRecord (Integer productId) {
		losPriceMapper.deleteSupportedLosPriceRecord(productId);
	}

	public LosPrice readMinLosPriceForNext90Days(Integer productId) {
		List<LosPrice> prices = losPriceMapper.readMinLosPriceForNext90Days(productId);
		if (CollectionUtils.isEmpty(prices)) {
			return null;
		}
		return prices.get(0);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<LocalDate> countDatesInLosPricesByDateRang(Integer productId, List<LocalDateRange> dates) {
		return losPriceMapper.countDatesInLosPricesByDateRang(productId, dates);
	}

	public List<Integer> checkLosPriceEnableForProduct(List<Integer> productIds) {
		return losPriceMapper.checkLosPriceEnableForProduct(productIds);
	}

	public List<IdDateDto> countDatesInLosPricesByDateRangForProducts(List<Integer> productIds, LocalDateRange range) {
		return losPriceMapper.countDatesInLosPricesByDateRangForProducts(productIds, range);
	}
}
