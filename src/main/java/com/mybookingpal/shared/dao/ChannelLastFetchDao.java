package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.shared.IdVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelLastFetchMapper;
import com.mybookingpal.dal.shared.ChannelLastFetch;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ChannelLastFetchDao {

	@Autowired
	ChannelLastFetchMapper channelLastFetchMapper;

	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
	public void update(ChannelLastFetch action) {
		channelLastFetchMapper.update(action);
	}

	public ChannelLastFetch read(ChannelLastFetch action) {
		return channelLastFetchMapper.read(action);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
	public void create(ChannelLastFetch action) {
		channelLastFetchMapper.create(action);
	}

	public List<IdVersion> getLastfetch(List<Integer> productIds) {
		return channelLastFetchMapper.getLastfetch(productIds);
	}

	public void deleteByProductId(String productId) {
		channelLastFetchMapper.deleteByProductId(productId);
	}

	public List<IdVersion> getMaxRateVersions(List<Integer> productIds) {
		return channelLastFetchMapper.getMaxRateVersions(productIds);
	}

}
