package com.mybookingpal.shared.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.PropertyManagerCancellationRuleMapper;
import com.mybookingpal.dal.shared.cancellation.PropertyManagerCancellationDateRule;
import com.mybookingpal.dal.shared.cancellation.PropertyManagerCancellationNightsRule;
import com.mybookingpal.dal.shared.cancellation.PropertyManagerCancellationRule;

@Repository
public class PropertyManagerCancellationRuleDao {

	@Autowired
	private PropertyManagerCancellationRuleMapper propertyManagerCancellationRuleMapper;

	public final List<PropertyManagerCancellationDateRule> readdatebypmid(Integer propertyManagerId) {
		return propertyManagerCancellationRuleMapper.readdatebypmid(propertyManagerId);
	}

	public List<PropertyManagerCancellationRule> readbypmid(Integer propertyManagerId) {
		return propertyManagerCancellationRuleMapper.readbypmid(propertyManagerId);
	}

	public List<PropertyManagerCancellationRule> readCancellationsByRatePlanId(SqlSession sqlSession, Long ratePlanId) {
		return propertyManagerCancellationRuleMapper.readCancellationsByRatePlanId(ratePlanId);
	}

	public List<PropertyManagerCancellationNightsRule> readByChannelIdSupplierId(Integer supplierId, List<Integer> channelIds) {
		return propertyManagerCancellationRuleMapper.readByChannelIdSupplierId(supplierId, channelIds);
	}

	public List<PropertyManagerCancellationNightsRule> readByChannelIdProductId(Integer productId, List<Integer> channelIds) {
		return propertyManagerCancellationRuleMapper.readByChannelIdProductId(productId, channelIds);
	}

	public void create(PropertyManagerCancellationRule rule) {
		propertyManagerCancellationRuleMapper.create(rule);
	}
	
	public PropertyManagerCancellationRule read(Integer id){
		return propertyManagerCancellationRuleMapper.read(id);
	}
	
	public void update(PropertyManagerCancellationRule rule){
		propertyManagerCancellationRuleMapper.update(rule);
	}

	public List<PropertyManagerCancellationRule> readByPmId(String pmId) {
		return propertyManagerCancellationRuleMapper.readByPmId(pmId);
	}

}
