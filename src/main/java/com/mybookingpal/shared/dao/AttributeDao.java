package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.AttributeMapper;
import com.mybookingpal.dal.shared.Amenities;
import com.mybookingpal.dal.shared.Attribute;

@Repository
public class AttributeDao {

	@Autowired
	private AttributeMapper attributeMapper;

	public List<String> fetchAttributeNames(List<String> relation) {
		List<String> attributeNames = attributeMapper.readAttribute(relation);
		return attributeNames;
	}

	public Attribute read(Attribute action) {
		return attributeMapper.read(action);
	}
	
	public List<Amenities> getAmenitiesById(Integer productId){
		return attributeMapper.getAmenitiesById(productId);
	}

}
