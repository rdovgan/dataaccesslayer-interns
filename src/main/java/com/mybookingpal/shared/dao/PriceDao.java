package com.mybookingpal.shared.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.json.price.PriceWidgetItem;
import com.mybookingpal.dal.shared.IdVersion;
import com.mybookingpal.dal.shared.LocalDateRange;
import com.mybookingpal.dal.shared.PriceTable;
import com.mybookingpal.dal.soap.ota.server.OtaRate;
import com.mybookingpal.dal.utils.GenericList;
import com.mybookingpal.rest.entity.ProductDate;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.PriceMapper;
import com.mybookingpal.dal.shared.Price;
import com.mybookingpal.dal.shared.RoomPeriodChange;
import com.mybookingpal.dal.shared.action.DatesIdAction;
import com.mybookingpal.dal.shared.price.PriceSearch;

@Repository
public class PriceDao {

	@Autowired
	private PriceMapper priceMapper;

	public void create(Price price) {
		priceMapper.create(price);
	}

	public List<Price> readByEntityId(Price price) {
		return priceMapper.readByEntityId(price);
	}

	public List<Price> readByEntityIdAndEntityType(Price price) {
		return priceMapper.readByEntityIdAndEntityType(price);
	}

	public Price getpropertydetailcheckinprice(Price action) {
		return priceMapper.getpropertydetailcheckinprice(action);
	}

	public List<Price> readMandatoryFees(Price price) {
		return priceMapper.readMandatoryFees(price);
	}

	public List<Price> existsPrice(Price price) {
		return priceMapper.existsPrice(price);
	}

	public Price exists(Price action) {
		return priceMapper.exists(action);
	}
	
	public List<Price> findFinalStateByEntityId(String entityId) {
		return priceMapper.findFinalStateByEntityId(entityId);
	}

	public List<Price> readByEntityIdAndEntityTypeOnlyFinal(Price price) {
		return priceMapper.readByEntityIdAndEntityTypeOnlyFinal(price);
	}

	public List<Price> readByEntityIdAndEntityTypeWithFinal(Price price) {
		return priceMapper.readByEntityIdAndEntityTypeWithFinal(price);
	}

	public Price readbydate(Price action) {
		return priceMapper.readbydate(action);
	}

	public List<Price> entityfeature(Price action){
		return priceMapper.entityfeature(action);
	}

	public List<Price> readbydateandids(PriceSearch price) {
		return priceMapper.readbydateandids(price);
	}

	public List<Price> readByProductIdAndDateRange(DatesIdAction action) {
		return priceMapper.readByProductIdAndDateRange(action);
	}

	public List<Price> readByProductIdAndDateRange(Integer productId, LocalDate fromDate, LocalDate toDate) {
		return priceMapper.readPricesByProductIdAndDateRange(productId, fromDate, toDate);
	}

	public Price read(String id) {
		return priceMapper.read(id);
	}

	public Price getContractFees(Price price) {
		return priceMapper.getContractFees(price);
	}

	public List<Price> getFeesAndFeatures(Price price) {
		return priceMapper.getFeesAndFeatures(price);
	}

	public Price getAllByEntityIdAndNameAndRatePlanId(Price price) {
		return priceMapper.getAllByEntityIdAndNameAndRatePlanId(price);
	}

	public Price getAllByEntityIdAndNameAndRatePlanIdAndDate(Price price) {
		return priceMapper.getAllByEntityIdAndNameAndRatePlanIdAndDate(price);
	}

	public List<Price> readByEntityIdAndEntityTypeStateByVersion(Price price) {
		return priceMapper.readByEntityIdAndEntityTypeStateByVersion(price);
	}

	public Double getPriceForRange(Price price) {
		return priceMapper.getPriceForRange(price);
	}

	public List<Price> getAllByEntityIdOrderByDateAsc(Price price) {
		return priceMapper.getAllByEntityIdOrderByDateAsc(price);
	}

	public List<Price> readByEntityIdWithFinal(Price price) {
		return priceMapper.readByEntityIdWithFinal(price);
	}

	public List<Price> readByEntityIdAndState(String entityId) {
		return priceMapper.readByEntityIdAndState(entityId);
	}

	public List<Price> readOptionalFees(Price price) {
		return priceMapper.readOptionalFees(price);
	}

	public Date getLastChangeSinceLastFetch(String productId, String lastFetch) {
		return priceMapper.getLastChangeSinceLastFetch(productId, lastFetch);
	}

	public Price getLowestActivePriceForProduct(Price action) {
		return priceMapper.getLowestActivePriceForProduct(action);
	}

	public List<Price> getProductsForPriceByVersion(Date lastFetch, Date toDate, String productId) {
		Price price = new Price();
		price.setDate(lastFetch);
		price.setTodate(toDate);
		price.setVersion(lastFetch);
		price.setEntityid(productId);
		return priceMapper.getProductsForPriceByVersion(price);
	}

	public List<RoomPeriodChange> readPriceForPeriodForAllStateBySupplierIdByVersion(String supplierId, Date fromDate, Date toDate, Date version,
			Integer channelId) {
		return priceMapper.readPriceForPeriodForAllStateBySupplierIdByVersion(supplierId, null, fromDate, toDate, version, channelId);
	}

	public List<RoomPeriodChange> readPriceForPeriodForAllStateByProductIdByVersion(String supplierId, String productId, Date fromDate, Date toDate,
			Date version, Integer channelId) {
		return priceMapper.readPriceForPeriodForAllStateBySupplierIdByVersion(supplierId, productId, fromDate, toDate, version, channelId);
	}

	public Price readContractFees(Price price) {
		return priceMapper.readContractFees(price);
	}

	public List<Price> readFeesAndFeatures(Price price) {
		return priceMapper.readFeesAndFeatures(price);
	}

	public List<Price> readContractFee(Price price) {
		return priceMapper.readContractFee(price);
	}

	public List<IdVersion> getMaxRateVersions(List<Integer> productIds) {
		return priceMapper.getMaxRateVersions(productIds);
	}

	public Price readPriceByRatePlanId(Long ratePlanId) {
		return priceMapper.readPriceByRatePlanId(ratePlanId);
	}

	public Integer readMaxMinStayByPeriod(Price action) {
		return priceMapper.readMaxMinStayByPeriod(action);
	}

	public void makePriceFinalByRatePlanId(Long ratePlanId) {
		priceMapper.makePriceFinalByRatePlanId(ratePlanId);
	}

	public Price readPriceById(Integer priceId) {
		return priceMapper.readPriceById(priceId);
	}

	public Price readPriceForBaseRatePlan(String productId, LocalDate fromDate, LocalDate toDate) {
		return priceMapper.readPriceForBaseRatePlan(productId, fromDate, toDate);
	}

	public List<Price> readListPricesForBaseRatePlan(String productId, LocalDate fromDate, LocalDate toDate) {
		return priceMapper.readListPricesForBaseRatePlan(productId, fromDate, toDate);
	}

	public List<Price> readAllPricesByProductIdAndDateRange(Integer productId, LocalDate fromDate, LocalDate toDate) {
		return priceMapper.readAllPricesByProductIdAndDateRange(productId, fromDate, toDate);
	}

	public List<Price> readByProductIdWithRatePlanId(String productId) {
		return priceMapper.readByProductIdWithRatePlanId(productId);
	}
	public Price readByEntityIdAndSupplierId(Price priceExample) {
		return priceMapper.readByEntityIdAndSupplierId(priceExample);
	}

	public void insertListPrices(List<Price> prices) {
		priceMapper.insertListPrices(prices);
	}

	public void cancelpricelist(List<Price> prices, Date version) {
		priceMapper.cancelpricelist(new GenericList<>(prices, Price.class), version);
	}

	public List<Price> getPricesByIdList(List<Long> priceIdList) {
		return priceMapper.getPricesByIdList(priceIdList);
	}

	public List<Price> getPricesByProductId(Integer productId) {
		return priceMapper.getPricesByProductId(productId);
	}

	public List<ProductDate> findByVersionAndMonthPeriod(LocalDateTime version, int versionInterval, int priceInterval) {
		return priceMapper.findByVersionAndMonthPeriod(version, versionInterval, priceInterval);
	}

	public void makeFinalByProductId(SqlSession sqlSession, String productId) {
		priceMapper.makeFinalByProductId(productId);
	}

	public void makeFinalByProductId(String productId) {
		priceMapper.makeFinalByProductId(productId);
	}

	public void updateCurrency(Price price) {
		priceMapper.updateCurrency(price);
	}

	public void update(Price price) {
		priceMapper.update(price);
	}

	public List<Price> readPriceDiscrepancyByProductIdAndDateRange(Integer productId, LocalDate fromDate, LocalDate toDate) {
		return priceMapper.readPriceDiscrepancyByProductIdAndDateRange(productId, fromDate, toDate);
	}

	public Price getcheckinprice(List<String> priceids) {
		return priceMapper.getcheckinprice(priceids);
	}

	public void cancelversion(Price action) {
		priceMapper.cancelversion(action);
	}

	public void cancelversionbypartyid(Price action) {
		priceMapper.cancelversionbypartyid(action);
	}

	public List<Price> readsplitpriceperiods(Price action) {
		return priceMapper.readsplitpriceperiods(action);
	}

	public Price readperdiemtax(Price action) {
		return priceMapper.readperdiemtax(action);
	}

	public Price altidfromdate(Price action) {
		return priceMapper.altidfromdate(action);
	}

	public void insertList(List<Price> prices) {
		priceMapper.insertList(prices);
	}

	public Price existsAltID(Price action) {
		return priceMapper.existsAltID(action);
	}

	public Date maxToDate(Price action) {
		return priceMapper.maxToDate(action);
	}

	public List<Price> restreadLimitedRows(Price action) {
		return priceMapper.restreadLimitedRows(action);
	}

	public Integer readArrivalMinStay(Price action) {
		return priceMapper.readArrivalMinStay(action);
	}

	public List<Price> readByEntityIdAndDate(Price price) {
		return priceMapper.readByEntityIdAndDate(price);
	}

	public List<PriceWidgetItem> featurewidget(PriceTable action) {
		return priceMapper.featurewidget(action);
	}

	public List<Price> restread(Price action) {
		return priceMapper.restread(action);
	}

	public void copyPrices(Integer fromProductId, Integer toProductId) {
		priceMapper.copyPrices(fromProductId, toProductId);
	}

	public List<OtaRate> otarates(OtaRate action) {
		return priceMapper.otarates(action);
	}

	public List<PriceWidgetItem> pricewidget(PriceTable action) {
		return priceMapper.pricewidget(action);
	}

	public void deleteByIdList(List<Price> idList) {
		priceMapper.deleteByIdList(idList);
	}

	public void delete(String id) {
		priceMapper.delete(id);
	}

	public void insertListWithUpdate(GenericList<Price> prices) {
		priceMapper.insertListWithUpdate(prices);
	}

	public List<Price> readFinal(int limit) {
		return priceMapper.readFinal(limit);
	}

	public Date maxVersionForProduct(Price action) {
		return priceMapper.maxVersionForProduct(action);
	}

	public List<Price> readByProductIdAndDateRangeForOnePriceRowProducts(DatesIdAction action) {
		return priceMapper.readByProductIdAndDateRangeForOnePriceRowProducts(action);
	}

	public Integer countValidPricesForProduct(Price price) {
		return priceMapper.countValidPricesForProduct(price);
	}

	public Date maxDate(Price action) {
		return priceMapper.maxDate(action);
	}

	public Price readExactMatch(Price action) {
		return priceMapper.readExactMatch(action);
	}

	public List<Price> readbytype(Price action) {
		return priceMapper.readbytype(action);
	}

	public void updateStateOnFinalPerPm(String partyId) {
		priceMapper.updateStateOnFinalPerPm(partyId);
	}

	public List<Price> readRatePlanPricesByDateAndIds(PriceSearch action) {
		return priceMapper.readRatePlanPricesByDateAndIds(action);
	}

	public List<Price> listPriceByEntityAndLastVersion(Integer entityId) {
		return priceMapper.listPriceByEntityAndLastVersion(entityId);
	}

	public List<Price> readPriceByProductIdAndDates(Integer productId, List<LocalDateRange> dates) {
		return priceMapper.readPriceByProductIdAndDates(productId, dates);
	}

	public List<Price> readAllPricesByProductListAndDateRange(List<Integer> list, LocalDate fromDate, LocalDate toDate) {
		return priceMapper.readAllPricesByProductListAndDateRange(list, fromDate, toDate);
	}
}
