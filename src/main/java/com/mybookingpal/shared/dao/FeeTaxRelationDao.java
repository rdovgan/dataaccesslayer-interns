package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.FeeTaxRelationMapper;
import com.mybookingpal.dal.shared.FeeTaxRelation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class FeeTaxRelationDao {

	@Autowired
	private FeeTaxRelationMapper feeTaxRelationMapper;

	public List<FeeTaxRelation> getAllByProductIdAndPartyId(String productId, String partyId) {
		return feeTaxRelationMapper.getAllByProductIdAndPartyId(productId, partyId);
	}

	public List<FeeTaxRelation> readByProductIds(List<String> productIds, Integer state) {
		return feeTaxRelationMapper.readByProductIds(productIds, state);
	}

	public List<FeeTaxRelation> getByFeeId(Integer feeId) {
		return feeTaxRelationMapper.getByFeeId(feeId);
	}

	public List<FeeTaxRelation> getByProductId(String productId) {
		return feeTaxRelationMapper.getByProductId(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<FeeTaxRelation> readByTaxesIds(List<Integer> taxIds) {
		return feeTaxRelationMapper.readByTaxesIds(taxIds);
	}
}
