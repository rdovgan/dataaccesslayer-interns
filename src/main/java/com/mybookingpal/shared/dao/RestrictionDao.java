package com.mybookingpal.shared.dao;

import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.shared.LocalDateRange;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.RestrictionMapper;
import com.mybookingpal.dal.shared.Restriction;
import com.mybookingpal.dal.shared.RoomPeriodChange;

@Repository
public class RestrictionDao {

	@Autowired
	RestrictionMapper restrictionMapper;

	public void create(Restriction restriction) {
		restrictionMapper.create(restriction);
	}

	public Restriction read(int id) {
		return restrictionMapper.read(id);
	}

	public void delete(int id) {
		restrictionMapper.delete(id);
	}

	public void deletebyproductid(int id) {
		restrictionMapper.deletebyproductid(id);
	}

	public List<Restriction> readbyproductandstate(Restriction restriction) {
		return restrictionMapper.readbyproductandstate(restriction);
	}

	public List<Restriction> readByProductIdAndDateRange(Restriction restriction) {
		return restrictionMapper.readByProductIdAndDateRange(restriction);
	}

	public List<Restriction> readByProductIdAndDateRangeAndRuleType(Restriction restriction) {
		return restrictionMapper.readByProductIdAndDateRangeAndRuleType(restriction);
	}

	public List<Restriction> readByEntityIdAndStateAndVersion(Restriction restriction) {
		return restrictionMapper.readByEntityIdAndStateAndVersion(restriction);
	}

	public List<Restriction> readByEntityIdAndEntityTypeAllState(Restriction restriction) {
		return restrictionMapper.readByEntityIdAndEntityTypeAllState(restriction);
	}

	public List<Restriction> readByEntityIdAndEntityType(Restriction restriction) {
		return restrictionMapper.readByEntityIdAndEntityType(restriction);
	}

	public List<Restriction> readByEntityIdAndEntityTypeOnlyFinal(Restriction restriction) {
		return restrictionMapper.readByEntityIdAndEntityTypeOnlyFinal(restriction);
	}

	public Restriction readByPartyIdAndRuleType(@Param("partyId") Integer partyId, @Param("ruleType") Integer ruleType) {
		return restrictionMapper.readByPartyIdAndRuleType(partyId, ruleType);
	}

	public List<Restriction> readCancelledByProductIdAndDateRange(Restriction restriction) {
		return restrictionMapper.readCancelledByProductIdAndDateRange(restriction);
	}

	public Restriction readByProductIdAndRuleType(Integer productId, Integer ruleType) {
		return restrictionMapper.readByProductIdAndRuleType(productId, ruleType);
	}

	public List<Restriction> readByPartyIdAndRuleTypeWithProducts(Integer partyId, Integer ruleType) {
		return restrictionMapper.readByPartyIdAndRuleTypeWithProducts(partyId, ruleType);
	}

	public Restriction getByEntityIdAndNameAndRuleTypeAndRatePlanId(Restriction restriction) {
		return restrictionMapper.getByEntityIdAndNameAndRuleTypeAndRatePlanId(restriction);
	}

	public List<Restriction> getByProductIdOrPartyIdAndMinMaxAdvDaysRuleTypes(Restriction restriction) {
		return restrictionMapper.getByProductIdOrPartyIdAndMinMaxAdvDaysRuleTypes(restriction);
	}

	public List<RoomPeriodChange> readRestrictionForPeriodForAllStateBySupplierIdByVersion(String supplierId, Date fromDate, Date toDate, Date version,
			Integer channelId) {
		return restrictionMapper.readRestrictionForPeriodForAllStateBySupplierIdByVersion(supplierId, fromDate, toDate, version, channelId);
	}

	public List<Restriction> readAllCreatedForProductBetweenDates(Integer productId, Date fromDate, Date toDate) {
		return restrictionMapper.readAllCreatedForProductBetweenDates(productId, fromDate, toDate);
	}

	public List<Restriction> readCreatedRestrictionsByRatePlanIdAndRuleType(Long ratePlanId, List<Integer> rules) {
		return restrictionMapper.readCreatedRestrictionsByRatePlanIdAndRuleType(ratePlanId, rules);
	}

	public List<Restriction> readRestrictionByProductIdAndDates(Integer productId, List<LocalDateRange> dates) {
		return restrictionMapper.readRestrictionByProductIdAndDates(productId, dates);
	}
}
