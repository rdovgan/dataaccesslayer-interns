package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelPartnerMapper;
import com.mybookingpal.dal.shared.ChannelPartner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ChannelPartnerDao {

	@Autowired
	private ChannelPartnerMapper channelPartnerMapper;

	public ChannelPartner read(Integer id) {
		return channelPartnerMapper.read(id);
	}

	public List<ChannelPartner> list() {
		return channelPartnerMapper.list();
	}

	public void delete(Integer id) {
		channelPartnerMapper.delete(id);
	}

	public void create(ChannelPartner action) {
		channelPartnerMapper.create(action);
	}

	public void update(ChannelPartner partner) {
		channelPartnerMapper.update(partner);
	}

	public ChannelPartner readBySupplierId(int supplierId) {
		return channelPartnerMapper.readBySupplierId(supplierId);
	}

	public ChannelPartner readByPartyId(int partyId) {
		return channelPartnerMapper.readByPartyId(partyId);
	}

	public List<String> readRelatedManagersByPartyId(int partyId) {
		return channelPartnerMapper.readRelatedManagersByPartyId(partyId);
	}

	public List<Integer> ManagerByChannelList(int channelID) {
		return channelPartnerMapper.ManagerByChannelList(channelID);
	}

	// XMLs generation 
	public List<ChannelPartner> getListChannelPartnerIdsForXMLs() {
		return channelPartnerMapper.getListChannelPartnerIdsForXMLs();
	}

	public List<Integer> readByAbbreviationChannelIDs(String abbreviation) {
		return channelPartnerMapper.readByAbbreviationChannelIDs(abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ChannelPartner> readListByAbbreviation(String abbreviation) {
		return channelPartnerMapper.readListByAbbreviation(abbreviation);
	}

	public List<Integer> readChannelIdsByAbbreviation(String abbreviation) {
		return channelPartnerMapper.readChannelIdsByAbbreviation(abbreviation);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ChannelPartner readByAbbreviation(String abbreviation) {
		return channelPartnerMapper.readByAbbreviation(abbreviation);
	}

	public ChannelPartner readByProductIdForExpedia(Integer productId) {
		return channelPartnerMapper.readByProductIdForExpedia(productId);
	}
}
