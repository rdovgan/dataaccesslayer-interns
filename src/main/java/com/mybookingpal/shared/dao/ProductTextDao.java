package com.mybookingpal.shared.dao;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.mybookingpal.dal.server.api.ProductTextMapper;
import com.mybookingpal.dal.shared.ProductText;

@Repository
public class ProductTextDao {

	@Autowired
	private ProductTextMapper productTextMapper;

	public List<ProductText> getByProductId(Integer productId) {
		return productTextMapper.getByProductId(productId);
	}
	
	public List<ProductText> getProductTextFromLastTimeStamp(String productId, Date version) {
		return productTextMapper.getProductTextFromLastTimeStamp(productId, version);
	}
}
