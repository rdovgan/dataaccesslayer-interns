package com.mybookingpal.shared.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.PropertyManagerInfoMapper;
import com.mybookingpal.dal.shared.PropertyManagerInfo;

@Repository
public class PropertyManagerInfoDao {

	@Autowired
	private PropertyManagerInfoMapper managerInfoMapper;

	public void create(PropertyManagerInfo propertyManagerInfo) {
		managerInfoMapper.create(propertyManagerInfo);
	}

	public  PropertyManagerInfo readbypmid(Integer pmID) {
		return managerInfoMapper.readbypmid(pmID);
	}
	
	public  PropertyManagerInfo readbyvirtualpmid(Integer pmID) {
		return managerInfoMapper.readbyvirtualpmid(pmID);
	}
	
	public  List<PropertyManagerInfo> listbyids(List<String> partyids) {
		return managerInfoMapper.listbyids(partyids);
	}
	
	public void updateByPMId(PropertyManagerInfo pmi) {
		managerInfoMapper.updatebypmid(pmi);
	}

	public PropertyManagerInfo readByPropertyManagerId(Integer id) {
		return managerInfoMapper.readbypmid(id);
	}

	@Deprecated
	public void updateFeeNetRate(Integer pmId, Boolean isNetRate) {
		managerInfoMapper.updateFeeNetRate(pmId, isNetRate);
	}

    public Date getMaxVersionForHACommission(Integer channelPartnerId, Integer pmId) {
		return managerInfoMapper.getMaxVersionForHACommission(channelPartnerId, pmId);
	}

}
