package com.mybookingpal.shared.dao;

import java.util.List;

import com.mybookingpal.dal.shared.messaging.MailThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.MailMessageMapper;
import com.mybookingpal.dal.shared.messaging.MailMessage;

@Repository
public class MailMessageDao {

	@Autowired
	MailMessageMapper mapper;

	public void create(MailMessage action) {
		mapper.create(action);
	}

	public void update(MailMessage action) {
		mapper.update(action);
	}

	public List<MailMessage> readByThreadId(String threadId) {
		return mapper.readByThreadId(threadId);
	}

	public MailMessage readByThreadIdAndMaxVersion(String threadId) {
		return mapper.readByThreadIdAndMaxVersion(threadId);
	}

	public MailMessage readByMessageId(String messageId) {
		return mapper.readByMessageId(messageId);
	}

	public MailMessage readByChannelMessageId(Long channelMessageId) {
		return mapper.readByChannelMessageId(channelMessageId);
	}

	public List<MailMessage> readByExample(MailMessage example) {
		return mapper.readByExample(example);
	}


}
