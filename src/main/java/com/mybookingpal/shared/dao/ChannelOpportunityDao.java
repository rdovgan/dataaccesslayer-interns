package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelOpportunityMapper;
import com.mybookingpal.dal.shared.ChannelOpportunity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ChannelOpportunityDao {
	@Autowired
	ChannelOpportunityMapper mapper;


	public ChannelOpportunity readById(Integer id) {
		return mapper.readById(id);
	}

	public List<ChannelOpportunity> readByExample(ChannelOpportunity example) {
		return mapper.readByExample(example);
	}

	public void create(ChannelOpportunity channelOpportunity) {
		mapper.create(channelOpportunity);
	}

	public void update(ChannelOpportunity action) {
		mapper.update(action);
	}

	public List<ChannelOpportunity> readByChannelId(Integer channelId) {
		return mapper.readByChannelId(channelId);
	}

	public List<String> readAltIds(){
		return mapper.readAltIds();
	}

	public List<ChannelOpportunity> readDistinctOpportunities(){
		return mapper.readDistinctOpportunities();
	}

}
