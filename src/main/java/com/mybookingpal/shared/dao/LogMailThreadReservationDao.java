package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.LogMailThreadReservationMapper;
import com.mybookingpal.dal.shared.LogMailThreadReservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LogMailThreadReservationDao {

	@Autowired
	LogMailThreadReservationMapper logMailThreadReservationMapper;

	public void create(LogMailThreadReservation logMailThreadReservation) {
		logMailThreadReservationMapper.create(logMailThreadReservation);
	}

	public List<LogMailThreadReservation> readByExample(LogMailThreadReservation example) {
		return logMailThreadReservationMapper.readByExample(example);
	}
}
