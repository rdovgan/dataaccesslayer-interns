package com.mybookingpal.shared.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;
import com.mybookingpal.dal.server.api.ChannelJobStatusMapper;
import com.mybookingpal.dal.shared.ChannelJobStatus;

@Repository
public class ChannelJobStatusDao {
	
	@Autowired
	ChannelJobStatusMapper channelJobStatusMapper;
	
	@Autowired
	ApplicationContext ctx;
	
	public void createChannelJobStatus(ChannelJobStatus channelJobStatus){
		channelJobStatusMapper.createChannelJobStatus(channelJobStatus);
	}
	
	public ChannelJobStatus readChannelJobStatus(ChannelJobStatus channelJobStatus){
		return channelJobStatusMapper.readChannelJobStatus(channelJobStatus);
	}
	
	public void updateChannelJobStatus(ChannelJobStatus channelJobStatus){
		channelJobStatusMapper.updateChannelJobStatus(channelJobStatus);
	}
}
