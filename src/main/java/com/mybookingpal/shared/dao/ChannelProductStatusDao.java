package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelProductStatusMapper;
import com.mybookingpal.dal.shared.ChannelProductStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ChannelProductStatusDao {

    @Autowired
    private ChannelProductStatusMapper channelProductStatusMapper;

    public void create(ChannelProductStatus channelProductStatus) {
        channelProductStatusMapper.create(channelProductStatus);
    }

    public ChannelProductStatus getChannelProductStatusByProductIdAndChannelIdAndFunction(Integer productId, Integer channelId, Integer function) {
        return channelProductStatusMapper.getChannelProductStatusByProductIdAndChannelIdAndFunction(productId, channelId, function);
    }

    public void update(ChannelProductStatus channelProductStatus) {
        channelProductStatusMapper.update(channelProductStatus);
    }

    public List<ChannelProductStatus> getChannelProductStatusByProductIdAndChannelIdAndFunctionList (List<Integer> productIds, Integer channelId,  List<Integer> functions){
        return channelProductStatusMapper.getChannelProductStatusByProductIdAndChannelIdAndFunctionList(productIds,channelId,functions);
    }

    public List<ChannelProductStatus> getInProgressChannelProductStatusByProductIdAndChannelId (List<Integer> productIds, Integer channelId){
        return channelProductStatusMapper.getInProgressChannelProductStatusByProductIdAndChannelId(productIds,channelId);
    }

    public List<ChannelProductStatus> getLatestChannelProductStatusByProductIdAndChannelIdAndFunctions (List<Integer> productIds, Integer channelId,  List<Integer> functions){
        return channelProductStatusMapper.getLatestChannelProductStatusByProductIdAndChannelIdAndFunctions(productIds,channelId,functions);
    }

    public List<ChannelProductStatus> getChannelProductStatusByProductIdsAndChannelIdsAndFunctionList(List<Integer> productIds, List<Integer> channelIds,  List<Integer> functions){
        return channelProductStatusMapper.getChannelProductStatusByProductIdsAndChannelIdsAndFunctionList(productIds,channelIds,functions);
    }

    public List<ChannelProductStatus> getChannelProductStatusForExpedia(Integer productId, Integer function) {
        return channelProductStatusMapper.getChannelProductStatusForExpedia(productId, function);
    }

}
