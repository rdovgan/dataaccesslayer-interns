package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mybookingpal.dal.server.api.PaymentTransactionMapper;
import com.mybookingpal.dal.shared.PaymentTransaction;

/**
 * Layer for processing database operations associated with table PaymentTransaction
 */
@Service("PaymentTransactionDaoImpl")
public class PaymentTransactionDaoImpl /*implements PaymentTransactionDao */{
	
	@Autowired
	PaymentTransactionMapper paymentTransactionMapper;

	public PaymentTransactionDaoImpl(){
	}

	/**
	 * Create new record for payment transaction in table PaymentTransaction
	 *
	 * @param sqlSession to use myBatis mapping
	 * @param paymentTransaction for creating new record in table
	 * @see {@link PaymentTransactionDao}
	 */
	//@Override
	public void create(PaymentTransaction paymentTransaction) {
		paymentTransactionMapper.create(paymentTransaction);
	}

	//@Override
	public List<PaymentTransaction> readByReservationId(Integer reservationId) {
		return paymentTransactionMapper.readByReservationId(reservationId);
	}

	//@Override
	public PaymentTransaction read(Integer id) {
		return paymentTransactionMapper.read(id);
	}

	//@Override
	public PaymentTransaction readLastFailedTransactionForGroupedReservation(Long groupedReservationId) {
		return paymentTransactionMapper.readLastFailedTransactionForGroupedReservation(groupedReservationId);
	}

	//@Override
	public List<PaymentTransaction> readAcceptedTransactionsByIds(List<Long> transactionIds) {
		return paymentTransactionMapper.readTransactionsByIds(transactionIds);
	}

	//@Override
	public List<PaymentTransaction> readAllTransactionsByIds(List<Long> transactionIds) {
		return paymentTransactionMapper.readAllTransactionsByIds(transactionIds);
	}

	//@Override
	public PaymentTransaction getInitialTransactionByReservationId(int reservationId) {
		return paymentTransactionMapper.getInitialTransactionByReservationId(reservationId);
	}

	//@Override
	public void update(PaymentTransaction paymentTransaction) {
		paymentTransactionMapper.update(paymentTransaction);
	}

}