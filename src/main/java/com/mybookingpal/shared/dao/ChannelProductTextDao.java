package com.mybookingpal.shared.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelProductTextMapper;
import com.mybookingpal.dal.shared.ChannelProductText;

/**
 * @author Danijel Blagojevic
 *
 */
@Repository
public class ChannelProductTextDao {

	@Autowired
	ChannelProductTextMapper channelProductTextMapper;

	public List<ChannelProductText> getByProductId(Integer productId) {
		return channelProductTextMapper.getByProductId(productId);
	}

	public List<ChannelProductText> getByProductIdAndChannelId(ChannelProductText channelProductText) {
		return channelProductTextMapper.getByProductIdAndChannelId(channelProductText);
	}

	public void insertList(@Param("list") List<ChannelProductText> channelProductTextList) {
		channelProductTextMapper.insertList(channelProductTextList);
	}

	public void update(ChannelProductText channelProductTextList) {
		channelProductTextMapper.update(channelProductTextList);
	}

	public void deleteList(List<ChannelProductText> idList) {
		channelProductTextMapper.deleteList(idList);
	}

	public String getHeadlineTextByProductIdAndPartyForChannel(ChannelProductText channelProductText) {
		return channelProductTextMapper.getHeadlineTextByProductIdAndPartyForChannel(channelProductText);
	}

	public List<ChannelProductText> getHeadlineTextsByProductIdAndPartyForChannel(ChannelProductText channelProductText) {
		return channelProductTextMapper.getHeadlineTextsByProductIdAndPartyForChannel(channelProductText);
	}
	
	public String getShortDescriptionProductIdAndPartyForChannel(ChannelProductText channelProductText) {
		return channelProductTextMapper.getShortDescriptionProductIdAndPartyForChannel(channelProductText);
	}
	
	public String getDescriptionByProductIdAndPartyForChannel(ChannelProductText channelProductText) {
		return channelProductTextMapper.getDescriptionByProductIdAndPartyForChannel(channelProductText);
	}
	
	public String getDisplayNameByProductIdAndPartyForChannel(ChannelProductText channelProductText) {
		return channelProductTextMapper.getDisplayNameByProductIdAndPartyForChannel(channelProductText);
	}

	public List<ChannelProductText> getShortDescriptionsByProductIdAndPartyForChannel(ChannelProductText channelProductText){
		return channelProductTextMapper.getShortDescriptionsByProductIdAndPartyForChannel(channelProductText);
	}

	public List<ChannelProductText> getNameByTextType(ChannelProductText channelProductText){
		return channelProductTextMapper.getNameByTextType(channelProductText);
	}
}
