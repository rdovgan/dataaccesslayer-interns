package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ProductSpecificMapper;
import com.mybookingpal.dal.shared.ProductSpecific;

@Repository
public class ProductSpecificDao {
	
	@Autowired
	ProductSpecificMapper mapper;

	public void create(ProductSpecific productSpecific) {
		mapper.create(productSpecific);
	}

	public void update(ProductSpecific productSpecific) {
		mapper.update(productSpecific);
	}

	public void createList(List<ProductSpecific> productSpecific) {
		mapper.createList(productSpecific);
	}

	public void updateList(List<ProductSpecific> productSpecific) {
		mapper.updateList(productSpecific);
	}
	
	public List<ProductSpecific> readByProductId(int productId) {
		return mapper.readByProductId(productId);
	}
	
	public List<ProductSpecific> readByPartyId(int partyId) {
		return mapper.readByPartyId(partyId);
	}
}
