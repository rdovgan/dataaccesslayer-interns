package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelProductValidationRulesMapper;
import com.mybookingpal.dal.shared.ChannelProductValidationRules;
import com.mybookingpal.dal.shared.ChannelProductValidationRulesWithName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ChannelProductValidationRulesDao {

    @Autowired
    ChannelProductValidationRulesMapper channelProductValidationRulesMapper;

    public void create (ChannelProductValidationRules channelProductValidationRules) {
        channelProductValidationRulesMapper.create(channelProductValidationRules);
    }

    public List<ChannelProductValidationRulesWithName> getAllRulesForChannel (Integer channelId) {
        return channelProductValidationRulesMapper.getAllRulesForChannel(channelId);
    }

}
