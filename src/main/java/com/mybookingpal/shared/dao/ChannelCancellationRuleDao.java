package com.mybookingpal.shared.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelCancellationRuleMapper;
import com.mybookingpal.dal.shared.cancellation.ChannelCancellationRule;

@Repository
public class ChannelCancellationRuleDao {

	private static final Logger LOG = Logger.getLogger(ChannelCancellationRuleDao.class);

	@Autowired
	private ChannelCancellationRuleMapper mapper;

	public void create(ChannelCancellationRule cancellationRule) {
		mapper.create(cancellationRule);
	}

	public ChannelCancellationRule read(Integer id) {
		return mapper.read(id);
	}

	public void update(ChannelCancellationRule cancellationRule) {
		mapper.update(cancellationRule);
	}

	public void delete(Integer id) {
		mapper.delete(id);
	}

	public List<ChannelCancellationRule> readByChannelId(Integer channelId) {
		return mapper.readByChannelId(channelId);
	}

	public List<ChannelCancellationRule> readByPmId(Integer pmId) {
		return mapper.readByPmId(pmId);
	}

	public void deleteByChannelId(Integer channelId) {
		mapper.deleteByChannelId(channelId);
	}

	public List<ChannelCancellationRule> readCancellationsByPmIdAndChannelPartner(@Param("pmId") Integer pmId,
			@Param("channelPartnerIds") List<Integer> channelPartnerIds) {
		return mapper.readCancellationsByPmIdAndChannelPartner(pmId, channelPartnerIds);
	}

	public void deleteAllByIds(List<Integer> listIds) {
		mapper.deleteAllByIds(listIds);
	}

	public List<ChannelCancellationRule> readCancellationsByProductIdAndChannelPartner(@Param("productId") Integer productId,
			@Param("channelPartnerIds") List<Integer> channelPartnerIds) {
		return mapper.readCancellationsByProductIdAndChannelPartner(productId, channelPartnerIds);
	}

	public List<ChannelCancellationRule> readByProductId(Integer productId) {
		return mapper.readByProductId(productId);
	}

	public List<ChannelCancellationRule> readCancellationsByPmIdOrProductIdAndChannelId(ChannelCancellationRule cancellationRule) {
		return mapper.readCancellationsByPmIdOrProductIdAndChannelId(cancellationRule);
	}

	public List<String> getPropertyIdsByPmIdAndChannelId(Integer pmId, List<Integer> channelIds) {
		return mapper.getPropertyIdsByPmIdAndChannelId(pmId, channelIds);
	}

}
