package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.shared.ProductResidencyCategory;

public interface ProductResidencyCategoryDao {
	
	ProductResidencyCategory readByProductId(Integer productId);

	void create(ProductResidencyCategory productResidencyCategory);

	void update(ProductResidencyCategory productResidencyCategory);

}
