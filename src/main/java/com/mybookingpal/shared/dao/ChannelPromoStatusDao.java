package com.mybookingpal.shared.dao;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.mybookingpal.dal.server.api.ChannelPromoStatusMapper;
import com.mybookingpal.dal.shared.ChannelPromoStatus;

@Repository
public class ChannelPromoStatusDao {

	@Autowired
	private ChannelPromoStatusMapper mapper;

	public ChannelPromoStatus readChannelPromoStatusByPromoId(ChannelPromoStatus channelPromoStatus){
		return mapper.readChannelPromoStatusByPromoId(channelPromoStatus);
	}
	public void create(ChannelPromoStatus channelPromoStatus){
		mapper.create(channelPromoStatus);
	}
	public void update(ChannelPromoStatus channelPromoStatus){
		mapper.update(channelPromoStatus);
	}
	public Date getMaxVersionOfChannelPromoStatus(int channelId){
		return mapper.getMaxVersionOfChannelPromoStatus(channelId);
	}
	public List<ChannelPromoStatus> readChannelPromoStatusByAvailabilityStatus(ChannelPromoStatus channelPromoStatus){
		return mapper.readChannelPromoStatusByAvailabilityStatus(channelPromoStatus);
	}
	public List<ChannelPromoStatus> readChannelPromoStatusByExpiredStatus(ChannelPromoStatus channelPromoStatus){
		return mapper.readChannelPromoStatusByExpiredStatus(channelPromoStatus);
	}
}
