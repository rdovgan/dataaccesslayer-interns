package com.mybookingpal.shared.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.PartyMapper;
import com.mybookingpal.dal.shared.Party;

@Repository
public class PartyDao {

	@Autowired
	 PartyMapper partyMapper;

	public final Party readParty(String id) {
		return partyMapper.read(id);
	}
	
	public final ArrayList<Party> partylistbyids(List<String> partyids) {
		return partyMapper.partylistbyids(partyids);
	}
	public  List<Integer> listChannelID(Integer pmId){
		return partyMapper.listChannelID(pmId);
	}
	
	public  void insertChannelPatner(Integer managerId, Integer channelId){
		partyMapper.insertChannelPatner(managerId,channelId);
	}
	
	public Party read(String id){
		return partyMapper.read(id);
	}
	
	public List<String> getAllPartyIdsPerPMSAndChannel(String partnerId, Integer channelId) {
		return partyMapper.getAllPartyIdsPerPMSAndChannel(partnerId, channelId);
	}
	
	public Party readActiveParty(String id) {
		return partyMapper.readActiveParty(id);
	}
	
	public Integer getPartyIdByEmployerId(Integer id) {
		return partyMapper.getPartyIdByEmployerId(id);
	}

	public List<Party> readPartyIdList(List<Integer> partyIdList) {
		if (partyIdList == null || partyIdList.isEmpty()) {
			return Collections.emptyList();
		}
		List<String> idListString = partyIdList.stream().map(Object::toString).collect(Collectors.toList());
		return partyMapper.partylistbyids(idListString);
	}

	public void create(Party action) {
		partyMapper.create(action);
	}
}
