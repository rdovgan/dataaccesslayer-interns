package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.shared.Image;
import com.mybookingpal.dal.shared.NameIdAction;
import com.mybookingpal.dal.shared.image.ImageSearch;
import com.mybookingpal.dal.shared.image.ImageTag;

import java.util.Date;
import java.util.List;

public interface ImageDao {

	List<Image> imagesbyproductidsortorder(NameIdAction action);

	void update(Image image);

	Image read(String imageId);

	List<ImageSearch> listByProductIds(ImageSearch image);

	List<Image> imagesByProductId(String productId);
	
	List<Image> imagesByProductIdRegardlessOfState(String productId);

	List<ImageTag> readAllImageTags();

	List<ImageTag> readImageTags(Integer id);

	List<Image> getImagesFromLastTimeStamp(String productId, Date version);

	void createImageTag(ImageTag imageTag);

	void updateImageTag(ImageTag imageTag);

	ImageTag readImageTag(String imageTagId);
}
