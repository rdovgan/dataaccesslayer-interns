package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ImageMapper;
import com.mybookingpal.dal.shared.Image;
import com.mybookingpal.dal.shared.NameIdAction;
import com.mybookingpal.dal.shared.image.ImageSearch;
import com.mybookingpal.dal.shared.image.ImageTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class ImageDaoImpl implements ImageDao {

	@Autowired
	private ImageMapper imageMapper;

	public List<Image> imagesbyproductidsortorder(NameIdAction action) {
		return imageMapper.imagesbyproductidsortorder(action);
	}

	public void update(Image image) {
		imageMapper.update(image);
	}

	public Image read(String imageId) {
		return imageMapper.read(imageId);
	}

	public List<ImageSearch> listByProductIds(ImageSearch image) {
		return imageMapper.listByProductIds(image);
	}

	public List<Image> imagesByProductId(String productId) {
		return imageMapper.imagesByProductId(productId);
	}
	
	public List<Image> imagesByProductIdRegardlessOfState(String productId) {
		return imageMapper.imagesByProductIdRegardlessOfState(productId);
	}

	public List<ImageTag> readAllImageTags() {
		return imageMapper.readAllImageTags();
	}

	public List<ImageTag> readImageTags(Integer id) {
		return imageMapper.readImageTags(id);
	}

	public List<Image> getImagesFromLastTimeStamp(String productId, Date version) {
		return imageMapper.getImagesFromLastTimeStamp(productId, version);
	}

	@Override
	public void createImageTag(ImageTag imageTag) {
		imageMapper.createImageTag(imageTag);
	}

	@Override
	public void updateImageTag(ImageTag imageTag) {
		imageMapper.updateImageTag(imageTag);
	}

	@Override
	public ImageTag readImageTag(String imageTagId) {
		return imageMapper.readImageTag(imageTagId);
	}
}
