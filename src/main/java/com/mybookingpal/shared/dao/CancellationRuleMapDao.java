package com.mybookingpal.shared.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.CancellationRuleMapMapper;
import com.mybookingpal.dal.shared.cancellation.ChannelCancellationRule;
import com.mybookingpal.dal.shared.cancellation.ChannelSpecificCancellationRule;

@Repository
public class CancellationRuleMapDao {

	private static final Logger LOG = Logger.getLogger(CancellationRuleMapDao.class);
	@Autowired
	public CancellationRuleMapMapper mapper;

	public void create(ChannelSpecificCancellationRule rule) {
		mapper.create(rule);
	}

	public ChannelSpecificCancellationRule read(Integer id) {
		return mapper.read(id);
	}

	public List<ChannelSpecificCancellationRule> readAll() {
		return mapper.readAll();
	}

	public String readChannelAbbreviation(Integer id) {
		return mapper.readChannelAbbreviation(id);
	}

	public void update(ChannelSpecificCancellationRule rule) {
		mapper.update(rule);
	}

	public List<ChannelSpecificCancellationRule> readByPmName(String name) {
		return mapper.readByPmName(name);
	}

	public List<ChannelSpecificCancellationRule> getAllCancellationRules() {
		return mapper.getAllCancellationRules();
	}

	public List<ChannelSpecificCancellationRule> readChannelSpecificCancellationRulesByIds(List<Integer> cancellationIds) {
		return mapper.readChannelSpecificCancellationRulesByIds(cancellationIds);
	}
	
	public List<ChannelSpecificCancellationRule> readCancellationsByPmIdOrProductIdAndChannelId(ChannelCancellationRule cancellationRule) {
		return mapper.readCancellationsByPmIdOrProductIdAndChannelId(cancellationRule);
	}

	public ChannelSpecificCancellationRule readByChannelCancellationMapId(String channelCancellationMapId) {
		return mapper.readByChannelCancellationMapId(channelCancellationMapId);
	}

	public List<ChannelSpecificCancellationRule> readByChannelCancellationMapPerChannelAbbreviation(String channelAbbreviation) {
		return mapper.readByChannelCancellationMapPerChannelAbbreviation(channelAbbreviation);
	}

}
