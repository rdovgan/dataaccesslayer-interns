package com.mybookingpal.shared.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelPriceMapper;
import com.mybookingpal.dal.shared.ChannelPrice;

@Repository
public class ChannelPriceDao {

	@Autowired
	ChannelPriceMapper channelPriceMapper;
	
	public ChannelPrice create(ChannelPrice action){
		
		channelPriceMapper.create(action);
		return action;
	}

	public void update(ChannelPrice action){
		
	}
	
	public void delete(ChannelPrice action){
		
	}
	
	
}
