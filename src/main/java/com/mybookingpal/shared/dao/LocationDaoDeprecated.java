package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.shared.Area;
import com.mybookingpal.dal.shared.Location;
import com.mybookingpal.dal.utils.LocationConverterUtils;
import com.mybookingpal.dataaccesslayer.entity.FullLocationResult;
import com.mybookingpal.dataaccesslayer.entity.LocationXsl;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.utils.entity.NameIdAction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@Deprecated
public class LocationDaoDeprecated {

	@Autowired
	private com.mybookingpal.dataaccesslayer.dao.LocationDao locationDao;

	@Autowired
	private LocationConverterUtils converter;

	public final Location findLocationById(String id) {
		return converter.convert(locationDao.read(id));
	}

	public Location exists(Location location) {
		return converter.convert(locationDao.exists(converter.convert(location)));
	}

	public void update(Location location) {
		locationDao.update(converter.convert(location));
	}

	public List<Location> readByExample(Location example) {
		return converter.convert(locationDao.readByExample(converter.convert(example)));
	}

	public void create(Location location) {
		locationDao.create(converter.convert(location));
	}

	public Location read(String id) {
		return converter.convert(locationDao.read(id));
	}

	public Location readWithFlushCache(String id) {
		return converter.convert(locationDao.readWithFlushCache(id));
	}

	public FullLocationResult getLocationInformationById(Integer id) {
		return locationDao.getLocationInformationById(id);
	}

	public List<Location> readCreatedLocationsWithoutTimeZoneId(Integer limit) {
		return converter.convert(locationDao.readCreatedLocationsWithoutTimeZoneId(limit));
	}

	public void updateTimeZoneIdForLocations(Integer timeZoneId, List<Integer> locationIds) {
		if (CollectionUtils.isEmpty(locationIds)) {
			return;
		}
		locationDao.updateTimeZoneIdForLocations(timeZoneId, locationIds);
	}

	public List<Location> readByRegionName(String region) {
		return converter.convert(locationDao.readByRegionName(region));
	}

	public List<Location> googleExists(Location action) {
		return converter.convert(locationDao.googleExists(converter.convert(action)));
	}


	public List<Location> getLocations(Integer channelId) {
		return converter.convert(locationDao.getLocations(channelId));
	}

	public List<? extends NameId> countryListByNameId(NameIdAction action) {
		return locationDao.countryListByNameId(action);
	}

	public List<? extends NameId> locationListByCountry(NameIdAction country) {
		return locationDao.locationsByCountryRegion(country);
	}

	public List<? extends NameId> regionNameId(NameIdAction action) {
		return locationDao.regionNameId(action);
	}

	public List<? extends NameId> locationsByCountryRegion(NameIdAction action) {
		return locationDao.locationsByCountryRegion(action);
	}

	public List<? extends NameId> nameIdNearBy(Area action) {
		if (action == null) {
			return new ArrayList<>();
		}
		return locationDao.nameIdNearBy(action.getNelatitude(), action.getSwlatitude(), action.getNelongitude(), action.getSwlongitude());
	}

	public Location name(Location action) {
		return converter.convert(locationDao.name(converter.convert(action)));
	}

	public LocationXsl restread(String locationId) {
		return locationDao.restread(locationId);
	}

	public List<Location> getUnUpdatedEntries() {
		return converter.convert(locationDao.getUnUpdatedEntries());
	}

	public List<Location> getSearchLocations(NameId action) {
		return converter.convert(locationDao.getSearchLocations(action));
	}

	public List<Location> getSearchSublocations(NameId action) {
		return converter.convert(locationDao.getSearchSublocations(action));
	}

	public List<Location> getLocationsWithoutZip(@Param("country") String country, @Param("region") String region, @Param("startIndex") Integer startIndex,
			@Param("limit") Integer limit) {
		return converter.convert(locationDao.getLocationsWithoutZip(country, region, startIndex, limit));
	}

	public Location rentalsUnitedSearch(Location action) {
		return converter.convert(locationDao.rentalsUnitedSearch(converter.convert(action)));
	}

	@Deprecated
	public List<Location> activeLocations() {
		return converter.convert(locationDao.activeLocations());
	}
}