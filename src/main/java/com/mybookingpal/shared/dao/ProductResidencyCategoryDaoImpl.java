package com.mybookingpal.shared.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ProductResidencyCategoryMapper;
import com.mybookingpal.dal.shared.ProductResidencyCategory;

@Repository
public class ProductResidencyCategoryDaoImpl implements ProductResidencyCategoryDao{
	
	@Autowired
	private ProductResidencyCategoryMapper mapper;
	
	public ProductResidencyCategory readByProductId(Integer productId) {
		return mapper.readByProductId(productId);
	}

	public void create(ProductResidencyCategory productResidencyCategory) {
		mapper.create(productResidencyCategory);
	}

	public void update(ProductResidencyCategory productResidencyCategory) {
		mapper.update(productResidencyCategory);
	}

}
