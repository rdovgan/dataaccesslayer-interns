package com.mybookingpal.shared.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.mybookingpal.dal.server.api.ChannelPayoutMapper;
import com.mybookingpal.dal.shared.ChannelPayout;

@Repository
public class ChannelPayoutDao   {
	@Autowired
	ChannelPayoutMapper channelPayoutMapper;
	
	
	public void createChannelPayout( ChannelPayout channelPayout){
		channelPayoutMapper.createChannelPayout(channelPayout);
	}
	public List<ChannelPayout> fetchChannelPayout( ChannelPayout channelPayout){
		return channelPayoutMapper.fetchChannelPayout(channelPayout);
	}
	
	public List<ChannelPayout> fetchChannelPayoutForReport( ChannelPayout channelPayout){
		return channelPayoutMapper.fetchChannelPayoutForDailyReport(channelPayout);
	}
	
	public List<ChannelPayout> getChannelPayoutsByConfirmationCode( ChannelPayout channelPayout){
		return channelPayoutMapper.getChannelPayoutsByConfirmationCode(channelPayout);
	}
	
	public List<ChannelPayout> fetchChannelPayoutForSelectedDates( ChannelPayout channelPayout){
		return channelPayoutMapper.fetchChannelPayoutForSelectedDates(channelPayout);
	}
	
	public List<ChannelPayout> fetchChannelPayoutForLastFetch(ChannelPayout channelPayout){
		return channelPayoutMapper.fetchChannelPayoutForLastFetch(channelPayout);
	}
	
	public Integer getDuplicateChannelPayoutCount(ChannelPayout tempChannelPayout) {
		return channelPayoutMapper.getDuplicateChannelPayoutCount(tempChannelPayout);
	}
	
}
