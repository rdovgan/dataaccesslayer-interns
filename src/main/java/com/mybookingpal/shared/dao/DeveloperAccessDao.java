package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.DeveloperAccessMapper;
import com.mybookingpal.dal.shared.DeveloperAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DeveloperAccessDao {

	@Autowired
	private DeveloperAccessMapper developerAccessMapper;

	public void createDeveloperAccess(DeveloperAccess developerAccess) {
		developerAccessMapper.create(developerAccess);
	}

	public void updateDeveloperAccessById(DeveloperAccess developerAccess) {
		developerAccessMapper.updateById(developerAccess);
	}

	public void updateDeveloperAccessByPartyId(DeveloperAccess developerAccess) {
		developerAccessMapper.updateByPartyId(developerAccess);
	}

	public void deleteById(Integer partyId) {
		developerAccessMapper.deleteByPartyId(partyId);
	}

	public DeveloperAccess readDeveloperAccessByPartyId(Integer partyId) {
		return developerAccessMapper.readByPartyId(partyId);
	}

	public DeveloperAccess readDeveloperAccessByApiKey(String apiKey) {
		return developerAccessMapper.readByApiKey(apiKey);
	}

	public DeveloperAccess readDeveloperAccessByEmail(String email) {
		return developerAccessMapper.readByEmail(email);
	}
}
