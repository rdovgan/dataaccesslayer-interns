package com.mybookingpal.shared.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.shared.LocalDateRange;
import com.mybookingpal.dal.shared.RoomPeriodChange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mybookingpal.dal.server.api.TaxMapper;
import com.mybookingpal.dal.shared.Price;
import com.mybookingpal.dal.shared.Tax;
import com.mybookingpal.dal.shared.api.HasTableService;
import com.mybookingpal.dal.shared.tax.TaxSearch;

@Repository
public class TaxDao {

	@Autowired
	private TaxMapper taxMapper;

	public Tax read(String id) {
		return taxMapper.read(id);
	}

	public Tax exists(Tax action) {
		return taxMapper.exists(action);
	}

	public void cancelversion(Tax action) {
		taxMapper.cancelversion(action);
	}

	public void canceltaxlist(List<String> taxIdList) {
		taxMapper.canceltaxlist(taxIdList);
	}

	public ArrayList<Tax> taxdetail(Price action) {
		return taxMapper.taxdetail(action);
	}

	public ArrayList<Tax> taxbyrelation(Price action) {
		return taxMapper.taxbyrelation(action);
	}

	public Integer count(HasTableService action) {
		return taxMapper.count(action);
	}

	public ArrayList<Tax> list(HasTableService action) {
		return taxMapper.list(action);
	}

	public ArrayList<Tax> readbyproductid(Tax action) {
		return taxMapper.readbyproductid(action);
	}

	public List<Tax> readbyproductidString(String productId) {
		return taxMapper.readbyproductidString(productId);
	}

	public ArrayList<Tax> readAll(int channelPartnerId) {
		return taxMapper.readAll(channelPartnerId);
	}

	public List<Tax> listbyproductids(TaxSearch action) {
		return taxMapper.listbyproductids(action);
	}

	public List<Tax> listbyrelationsproductids(TaxSearch action) {
		return taxMapper.listbyrelationsproductids(action);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
	public void create(Tax action) {
		taxMapper.create(action);
	}

	public List<Tax> readbyAmountAndName(Tax action) {
		return taxMapper.readbyAmountAndName(action);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
	public Tax getActiveChannelChargedTaxByAmountAndName(Tax action) {
		return taxMapper.getActiveChannelChargedTaxByAmountAndName(action);
	}

    public List<RoomPeriodChange> readTaxesForPeriodForAllStateBySupplierIdByVersion(String supplierId, Date fromDate, Date toDate, Date lastFetch, Integer channelId) {
		return taxMapper.readTaxesForPeriodForAllStateBySupplierIdByVersion(supplierId, fromDate, toDate, lastFetch, channelId);
    }

	public List<Tax> getAllTaxesByProductAndVersion(Tax action) {
		return taxMapper.getAllTaxesByProductAndVersion(action);
	}
	
	public List<Tax> getAllTaxesByRelationForProductAndVersion(Price action) {
		return taxMapper.getAllTaxesByRelationForProductAndVersion(action);
	}

	public List<Tax> readByProductIds(List<String> productIds, String state) {
		return taxMapper.readByProductIds(productIds, state);
	}
	
	public List<Tax> readAllActive() {
		return taxMapper.readAllActive();
	}

	public List<Tax> readTaxByProductId(Integer productId) {
		return taxMapper.readTaxByProductId(productId);
	}

	public List<Tax> readTaxByProductIdAndDatesAndNameTemplate(Integer productId, List<TaxSearch> searchTaxes) {
		return taxMapper.readTaxByProductIdAndDatesAndNameTemplate(productId, searchTaxes);
	}
}
