package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.shared.Area;
import com.mybookingpal.dal.utils.LocationConverterUtils;
import com.mybookingpal.dataaccesslayer.entity.FullLocationResult;
import com.mybookingpal.dataaccesslayer.entity.LocationModel;
import com.mybookingpal.dataaccesslayer.entity.LocationXsl;
import com.mybookingpal.utils.entity.NameId;
import com.mybookingpal.utils.entity.NameIdAction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class LocationDaoWrapper {

	@Autowired
	private com.mybookingpal.dataaccesslayer.dao.LocationDao locationDao;

	@Autowired
	private LocationConverterUtils converter;

	public final LocationModel findLocationById(String id) {
		return locationDao.read(id);
	}

	public LocationModel exists(LocationModel location) {
		return locationDao.exists(location);
	}

	public void update(LocationModel location) {
		locationDao.update(location);
	}

	public List<LocationModel> readByExample(LocationModel example) {
		return locationDao.readByExample(example);
	}

	public void create(LocationModel location) {
		locationDao.create(location);
	}

	public LocationModel read(String id) {
		return locationDao.read(id);
	}

	public LocationModel readWithFlushCache(String id) {
		return locationDao.readWithFlushCache(id);
	}

	public FullLocationResult getLocationInformationById(Integer id) {
		return locationDao.getLocationInformationById(id);
	}

	public List<LocationModel> readCreatedLocationsWithoutTimeZoneId(Integer limit) {
		return locationDao.readCreatedLocationsWithoutTimeZoneId(limit);
	}

	public void updateTimeZoneIdForLocations(Integer timeZoneId, List<Integer> locationIds) {
		if (CollectionUtils.isEmpty(locationIds)) {
			return;
		}
		locationDao.updateTimeZoneIdForLocations(timeZoneId, locationIds);
	}

	public List<LocationModel> readByRegionName(String region) {
		return locationDao.readByRegionName(region);
	}

	public List<LocationModel> googleExists(LocationModel action) {
		return locationDao.googleExists(action);
	}


	public List<LocationModel> getLocations(Integer channelId) {
		return locationDao.getLocations(channelId);
	}

	public List<NameId> countryListByNameId(NameIdAction action) {
		return locationDao.countryListByNameId(action);
	}

	public List<NameId> locationListByCountry(NameIdAction country) {
		return locationDao.locationsByCountryRegion(country);
	}

	public List<NameId> regionNameId(NameIdAction action) {
		return locationDao.regionNameId(action);
	}

	public List<NameId> locationsByCountryRegion(NameIdAction action) {
		return locationDao.locationsByCountryRegion(action);
	}

	public List<NameId> nameIdNearBy(Area action) {
		if (action == null) {
			return new ArrayList<>();
		}
		return locationDao.nameIdNearBy(action.getNelatitude(), action.getSwlatitude(), action.getNelongitude(), action.getSwlongitude());
	}

	public LocationModel name(LocationModel action) {
		return locationDao.name(action);
	}

	public LocationXsl restread(String locationId) {
		return locationDao.restread(locationId);
	}

	public List<LocationModel> getUnUpdatedEntries() {
		return locationDao.getUnUpdatedEntries();
	}

	public List<LocationModel> getSearchLocations(NameId action) {
		return locationDao.getSearchLocations(action);
	}

	public List<LocationModel> getSearchSublocations(NameId action) {
		return locationDao.getSearchSublocations(action);
	}

	public List<LocationModel> getLocationsWithoutZip(@Param("country") String country, @Param("region") String region, @Param("startIndex") Integer startIndex,
			@Param("limit") Integer limit) {
		return locationDao.getLocationsWithoutZip(country, region, startIndex, limit);
	}

	public LocationModel rentalsUnitedSearch(LocationModel action) {
		return locationDao.rentalsUnitedSearch(action);
	}


	public LocationModel readByNameLike(LocationModel action){
		return locationDao.readByNameLike(action);
	}

	@Deprecated
	public List<LocationModel> activeLocations() {
		return locationDao.activeLocations();
	}
}