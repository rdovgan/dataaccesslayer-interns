package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.EnumsMapper;
import com.mybookingpal.dal.shared.Enums;

@Repository
public class EnumDao {

	@Autowired
	private EnumsMapper enumsMapper;

	public Integer getIdByValue(String value) {
		return enumsMapper.getIdByValue(value);
	}
	
	public List<Enums> readByField(String field) {
		return enumsMapper.readByField(field);
	}

}
