package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelSpecificTaxGroupMapper;
import com.mybookingpal.dal.shared.ChannelSpecificTaxGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ChannelSpecificTaxGroupDao {

    @Autowired
    private ChannelSpecificTaxGroupMapper channelSpecificTaxGroupMapper;

    public List<ChannelSpecificTaxGroup> getByPropertyManagerId(Integer propertyManagerId) {
        return channelSpecificTaxGroupMapper.getByPropertyManagerId(propertyManagerId);
    }

    public void insertNewPropertyManager(Integer newPropertyManager){
       channelSpecificTaxGroupMapper.insertNewPropertyManager(newPropertyManager);
    }
}
