package com.mybookingpal.shared.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.RazorServer;
import com.mybookingpal.dal.server.api.ChannelReservationPayloadMapper;
import com.mybookingpal.dal.shared.ChannelReservationPayload;

@Repository
public class ChannelReservationPayloadDao {

	@Autowired
	ChannelReservationPayloadMapper channelReservationPayloadMapper;

	private static final Logger LOG = LoggerFactory.getLogger(ChannelReservationPayloadDao.class);

	public void createChannelReservationPayload(ChannelReservationPayload channelReservationPayload) {
		if(channelReservationPayloadMapper == null){
			ApplicationContext ctx = RazorServer.getContext();
			channelReservationPayloadMapper = ctx.getBean(ChannelReservationPayloadMapper.class);
		}
		channelReservationPayloadMapper.createChannelReservationPayload(channelReservationPayload);
	}

	public ChannelReservationPayload readByReservationId(Integer reservationId) {
		ChannelReservationPayload channelReservationPayload = channelReservationPayloadMapper.readChannelReservationPayloadByReservationId(reservationId);
		return channelReservationPayload;
	}

	public ChannelReservationPayload readByConfirmationId(String confirmationId) {
		if(channelReservationPayloadMapper == null){
			ApplicationContext ctx = RazorServer.getContext();
			channelReservationPayloadMapper = ctx.getBean(ChannelReservationPayloadMapper.class);
		}
		ChannelReservationPayload channelReservationPayload = channelReservationPayloadMapper.readChannelReservationPayloadByConfirmationId(confirmationId);
		return channelReservationPayload;
	}

	public void updateChannelReservationPayload(ChannelReservationPayload channelReservationPayload) {
		channelReservationPayloadMapper.updateItem(channelReservationPayload);
	}

	public void updateChannelReservationPayloadByConfirmationCode(ChannelReservationPayload channelReservationPayload) {
		channelReservationPayloadMapper.update(channelReservationPayload);
	}

	public List<ChannelReservationPayload> readChannelReservationPayloadByChannelId(Integer channelId) {
		List<ChannelReservationPayload> channelReservationPayload = channelReservationPayloadMapper.readMissedChannelReservationPayloadByChannelId(channelId);
		return channelReservationPayload;
	}

}
