package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ItemsMapper;
import com.mybookingpal.dal.shared.Items;
import com.mybookingpal.dal.shared.RateItem;

@Repository
public class ItemsDao {
	
	@Autowired
	private ItemsMapper itemsMapper;

	public List<Items> readItemsByRatePlanId(Long ratePlanId) {
		return itemsMapper.readItemsByRatePlanId(ratePlanId);
	}

	public void create(Items item) {
		itemsMapper.create(item);
	}

	public void delete(Items item) {
		itemsMapper.delete(item);
	}

	public void createRateItem(Long ratePlanId, Long itemId) {
		itemsMapper.createRateItem(new RateItem(ratePlanId, itemId));
	}

	public void createRateItemList(Long ratePlanId, List<Items> itemsList) {
		itemsList.stream().forEach(e -> createRateItem(ratePlanId, e.getId()));
	}

	public void deleteRateItem(Long ratePlanId, Long itemId) {
		itemsMapper.deleteRateItem(new RateItem(ratePlanId, itemId));
	}

	public void deleteAllItemsByRatePlanId(Long ratePlanId) {
		itemsMapper.deleteAllItemsByRatePlanId(ratePlanId);
	}

}
