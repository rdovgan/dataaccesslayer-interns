package com.mybookingpal.shared.dao;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.dao.entities.LowInventoryRoom;
import com.mybookingpal.dal.server.api.TimelineMapper;
import com.mybookingpal.dal.shared.availability.AvailabilityRoomDto;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.AvailabilityCalendarMapper;
import com.mybookingpal.dal.shared.AvailabilityCalendarModel;
import com.mybookingpal.dal.shared.AvailabilityCalendarSimple;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class AvailabilityCalendarModelDAO {
	@Autowired
	private AvailabilityCalendarMapper availabilityCalendarMapper;
	@Autowired
	private SqlSessionFactory sqlSessionFactory;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AvailabilityCalendarModel read(Long id) {
		return availabilityCalendarMapper.read(id);
	}

	public void create(AvailabilityCalendarModel availabilityCalendarModel) {
		availabilityCalendarMapper.create(availabilityCalendarModel);
	}

	public void update(AvailabilityCalendarModel availabilityCalendarModel) {
		availabilityCalendarMapper.update(availabilityCalendarModel);
	}

	public void delete(List<Long> idList) {
		availabilityCalendarMapper.delete(idList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityCalendarModel> readForPeriodByProductIds(List<Integer> productIdList, LocalDate fromDate,
			LocalDate toDate) {
		return availabilityCalendarMapper.readForPeriodByProductIds(productIdList, fromDate, toDate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityCalendarModel> readForPeriodForAllStateByProductIds(List<Integer> productIdList, Date fromDate,
			Date toDate) {
		return availabilityCalendarMapper.readForPeriodForAllStateByProductIds(productIdList, fromDate, toDate);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityCalendarModel> readForPeriodByProductIdsByVersion(List<Integer> productIdList, Date fromDate,
			Date toDate, Date version) {
		return availabilityCalendarMapper.readForPeriodByProductIdsByVersion(productIdList, fromDate, toDate, version);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityCalendarSimple> readForPeriodForAllStateBySupplierIdByVersion(String supplierId, Date fromDate, Date toDate, Date version,
			Integer channelId) {
		return availabilityCalendarMapper.readForPeriodForAllStateBySupplierIdByVersion(supplierId, fromDate, toDate, version, channelId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityCalendarModel> readForPeriodForAllStateByProductIdsByVersion(List<Integer> productIdList,
			Date fromDate, Date toDate, Date version) {
		return availabilityCalendarMapper.readForPeriodForAllStateByProductIdsByVersion(productIdList, fromDate, toDate, version);
	}

	public void insertAvailabilityCalendarModels(List<AvailabilityCalendarModel> availabilityCalendarModelList) {
		availabilityCalendarMapper.insertAvailabilityCalendarModels(availabilityCalendarModelList);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AvailabilityCalendarModel readByProductIdAndDate(AvailabilityCalendarModel availabilityCalendarModel) {
		return availabilityCalendarMapper.readByProductIdAndDate(availabilityCalendarModel);
	}

	public void updateVersionInAvilabilityCalenderForProductAndDate(AvailabilityCalendarModel availabilityCalendarModel) {
		availabilityCalendarMapper.updateVersionInAvilabilityCalenderForProductAndDate(availabilityCalendarModel);
	}

	public void saveAvailabilityCalendarHistory(List<AvailabilityCalendarModel> models) {
		availabilityCalendarMapper.saveAvailabilityCalendarHistory(models);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Integer getAvailableCountForProduct(String productId) {
		return availabilityCalendarMapper.getAvailableCountForProduct(productId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AvailabilityCalendarModel getLowestActiveBasePriceForProduct(String productId, Date dateForCheck) {
		return availabilityCalendarMapper.getLowestActiveBasePriceForProduct(productId, dateForCheck);
	}

	public void insertUpdateFromTemplate(AvailabilityCalendarModel templateModel) {
		availabilityCalendarMapper.insertUpdateFromTemplate(templateModel);
	}

	public void insertUpdateFromList(List<AvailabilityCalendarModel> models) {
		availabilityCalendarMapper.insertUpdateFromList(models);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityRoomDto> getCalendarByPropertyManagerForReport(Long pmId) {
		return availabilityCalendarMapper.getCalendarByPropertyManagerForReport(pmId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityRoomDto> readAvailabilityCalendarByRoomConfiguration(List<LowInventoryRoom> rooms, Long configurationId, Integer pmId,
			Integer noUnitCount) {
		return availabilityCalendarMapper.readAvailabilityCalendarByRoomConfiguration(rooms, configurationId, pmId, noUnitCount);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AvailabilityCalendarModel> readCreatedByProductIds(Integer pmId) {
		return availabilityCalendarMapper.readCreatedByProductIds(pmId);
	}

	public void cancelAvailabilityCalendarlist(List<AvailabilityCalendarModel> models) {
		availabilityCalendarMapper.cancelAvailabilityCalendarlist(models);
	}

	public void deleteAvailabilityCalendarlist(List<AvailabilityCalendarModel> models) {
		availabilityCalendarMapper.deleteAvailabilityCalendarlist(models);
	}

	public void insertUpdateCalendarFromTemplate(AvailabilityCalendarModel calendarModel, LocalDate fromDate, LocalDate toDate) {
		try (SqlSession session = sqlSessionFactory.openSession()) {
			session.getMapper(TimelineMapper.class).createDays(fromDate, toDate);
			session.getMapper(AvailabilityCalendarMapper.class).saveAvailabilityCalendarHistory(Collections.singletonList(calendarModel));
			session.getMapper(AvailabilityCalendarMapper.class).insertUpdateFromTemplate(calendarModel);
			session.getMapper(TimelineMapper.class).dropDays();
			session.commit();
		}
	}
	//BP-28512 Change storing process of availability_calendar
	/*public List<AvailabilityCalendarModel> readByAvailabilityCalendarIds(@Param("list") List<Long> ids) {
		return availabilityCalendarMapper.readByAvailabilityCalendarIds(ids);
	}*/

}
