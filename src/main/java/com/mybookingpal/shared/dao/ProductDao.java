package com.mybookingpal.shared.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.entity.IsProduct;
import com.mybookingpal.dal.shared.IdVersion;
import com.mybookingpal.dal.shared.ProductChannel;
import org.apache.ibatis.annotations.Param;

import com.mybookingpal.dal.shared.NameIdAction;
import com.mybookingpal.dal.shared.Product;
import com.mybookingpal.dal.shared.ProductAction;

public interface ProductDao {

	public List<Product> readAllActiveProperties(NameIdAction nameIdAction);

	public List<Product> readAllActiveOrSuspendedProperties(NameIdAction nameIdAction);

	Product read(String id);

	Product readById(Integer productId);

	Product getProductAvoidNPE(Integer id);

	Product readByAltIdAndSupplierId(String altId, String supplierId);

	ArrayList<String> activeMultiUnitSubProductIdListBySupplier(NameIdAction supplierId);

	public ArrayList<String> activeProductIdListBySupplier(NameIdAction supplierId);

	public ArrayList<String> activeSingleUnitProductIdListBySupplier(NameIdAction supplierId);

	public ArrayList<String> activeMultiUnitKeyRootProductIdListBySupplier(NameIdAction supplierId);

	public ArrayList<String> allMultiUnitKeyRootProductIdListBySupplier(NameIdAction supplierId);

	public List<String> activeMultiUnitRepRootProductIdListBySupplier(String supplierId);

	public List<Product> activeMultiUnitRepRootProductListBySupplier(String supplierId);

	List<String> activeMultiUnitProductIdListByParentId(String supplierId, String productId);

	public ArrayList<String> activeProductIdListBySupplierForChannel(ProductAction supplier);
	
	List<String> activeProductIdListBySupplierForChannelWithoutOWN(ProductAction supplier);
	
	public ArrayList<String> activeSGLProductIdListBySupplierForChannel(ProductAction supplier);

	ArrayList<String> allProductIdListBySupplierForChannel(ProductAction supplier);

	public ArrayList<String> activeProductIdListBySupplierForApartmentsApart(NameIdAction supplierId);

	public ArrayList<String> activeProductIdListByOwnerForChannel(ProductAction supplierId);

	public ArrayList<String> inactiveProductIdListBySupplierForChannel(NameIdAction supplierId);

	public ArrayList<String> inactiveProductIdListByOwnerForChannel(NameIdAction supplierId);

	public ArrayList<String> activeProductIdListByPartyForChannel(ProductAction supplierId);

	public ArrayList<String> activeProductIdListByOwnerForChannelByVersion(ProductAction supplierId);

	public ArrayList<String> activeProductIdListByPartyForChannelByVersion(ProductAction supplierId);

	public ArrayList<String> activeProductIdListBySupplierByVersion(ProductAction supplierId);
	
	public ArrayList<String> activeSGLProductIdListBySupplierByVersion(ProductAction supplierId);

	public ArrayList<String> inactiveProductIdListByOwnerForChannelByVersion(ProductAction supplierId);

	public ArrayList<String> inactiveProductIdListByPartyForChannelByVersion(ProductAction supplierId);

	public ArrayList<String> inactiveProductIdListBySupplierByVersion(ProductAction supplierId);
	
	public ArrayList<String> inactiveSGLProductIdListBySupplierByVersion(ProductAction supplierId);

	public ArrayList<String> activeAndInactiveProductIdListBySupplier(NameIdAction supplierId);

	List<String> getAllProductsByPartyForChannelFromChannelParty(NameIdAction supplierId);

	List<String> getAllProductsBySupplierForChannel(NameIdAction supplierId);

	Product getProductFromLastTimeStamp(Product product);

	public List<Product> productlistbyids(List<String> productids);

	List<String> getOWNPropertiesBySupplierId(String supplierId);

	public ArrayList<String> inactiveOrUndistributedProductIdListBySupplier(NameIdAction supplierId);
	
	List<String> inactiveOrUndistributedProductIdListBySupplierWithoutOWN(NameIdAction supplierId);

	List<String> allMultiUnitKeyProductIdListByRootId(NameIdAction supplierId);

	public ArrayList<String> inactiveProductIdListByPartyForChannel(NameIdAction supplierId);

	public List<Product> getProductByName(String hotelName);

	List<String> getProductIdsByParentId(String productId);

	List<Product> getProductsByParentId(String productId);

	void update(Product product);

	public List<String> fetchActiveMultiUnitKeyProductListByIds(@Param("list") List<String> products);

	public List<String> fetchActiveMultiUnitRepProductListByIds(@Param("list") List<String> products);

	public List<Product> readAllSupplier(NameIdAction nameIdAction);

	public List<Product> inactiveProductListBySupplier(NameIdAction supplierId);

	public List<String> activeMuliUnitProductIdListBySupplier(NameIdAction supplierId);
	
	public List<String> allSinglePropertiesIdListBySupplier(NameIdAction nameIdAction);
	
	public List<Product> readAllActivePropertiesByPropertyIds(@Param("list") List<String> productIds);
	
	public List<Product> readAllPropertiesByPropertyIds(@Param("list") List<String> productIds);
	
	public List<Product> readAllActivePropertiesByPmIds(@Param("list") List<String> pmIds);
	
	public List<Integer> getAllNewProductsByVersionForChannelPortal(ProductAction productAction);
	
	void markAsFinal(Integer id);

	List<Product> rootProducts(String supplierId);

	List<Product> allRootProducts(String supplierId, Date version);

	List<Product> allRootProductsFromChannelProductMap(String supplierId, Integer channelId, Date lastFetch);
	
	List<String> allOWNProductsFromChannelProductMap(String supplierId, Integer channelId);
	
	List<String> allMLTProductsFromChannelProductMap(String supplierId, Integer channelId);

	List<Product> distributedRootProducts(String supplierId, Integer channelId);

	List<Product> readRootProducts(List<String> productIds);

	List<String> findKeyProductsForMlt(String parentId);

	List<Product> activeMultiUnitProductsByRootId(String bpProductId);

    List<Product> readByExample(Product productExample);

	List<IdVersion> getMaxVersions(List<Integer> productIds);

    ProductChannel getMbpProductWithMTCCheck(String id, String abbreviation);

	List<Product> getAllAvailableRoomsForRoot(Long rootId);

    List<Product> readProductListByIdList(List<Integer> productIdList);

	List<Integer> getSubProductIds(Integer parentId);
	
	Integer getBoookingSettingForProduct(Integer productId);
	
	List<String> getFlagsForProduct(Integer productId);

	List<String> activePRMAndSUBProductIdListByParentId(ProductAction parentId);

	List<String> unDistributedMLTProductIdListBySupplier(ProductAction productAction);

	List<String> allInactivePRMAndSUBProductIdListByParentId(ProductAction parentId);

	List<Product> readMarriottProductsForGoogleChannel(Integer marriottChannelId);
}
