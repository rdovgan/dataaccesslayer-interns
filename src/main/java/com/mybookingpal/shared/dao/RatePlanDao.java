package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.RatePlanMapper;
import com.mybookingpal.dal.shared.RatePlanModel;

@Repository
public class RatePlanDao {

	@Autowired
	private RatePlanMapper ratePlanMapper;

	public void create(RatePlanModel ratePlan) {
		ratePlanMapper.create(ratePlan);
	}

	public RatePlanModel read(Long ratePlanId) {
		return ratePlanMapper.read(ratePlanId);
	}

	public void update(RatePlanModel ratePlan) {
		ratePlanMapper.update(ratePlan);
	}

	public void deleteById(Long ratePlanId) {
		ratePlanMapper.deleteById(ratePlanId);
	}

	public List<RatePlanModel> listByProductIds(List<Long> productIds) {
		return ratePlanMapper.listByProductIds(productIds);
	}

	public RatePlanModel readByRateCode(String rateCode) {
		return ratePlanMapper.readByRateCode(rateCode);
	}

	public List<RatePlanModel> getRatePlansForProductId(Long productId) {
		return ratePlanMapper.getRatePlansForProductId(productId);
	}
}
