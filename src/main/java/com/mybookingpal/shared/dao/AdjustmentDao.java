package com.mybookingpal.shared.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.AdjustmentMapper;
import com.mybookingpal.dal.shared.Adjustment;
import com.mybookingpal.dal.shared.action.DatesIdAction;
import com.mybookingpal.dal.shared.adjustment.AdjustmentSearch;
import com.mybookingpal.dal.shared.api.HasPrice;

@Repository
public class AdjustmentDao {

	@Autowired
	private AdjustmentMapper adjustmentMapper;

	public void create(Adjustment action) {
		adjustmentMapper.create(action);
	}

	public void update(Adjustment action) {
		adjustmentMapper.update(action);
	}

	public Adjustment exists(Adjustment action) {
		return adjustmentMapper.exists(action);
	}

	public Adjustment read(int id) {
		return adjustmentMapper.read(id);
	}

	public Adjustment readbyexample(Adjustment action) {
		return adjustmentMapper.readbyexample(action);
	}

	public Adjustment readbyreservation(HasPrice action) {
		return adjustmentMapper.readbyreservation(action);
	}

	public void cancelversion(Adjustment action) {
		adjustmentMapper.cancelversion(action);
	}

	public void canceladjustmentlist(List<String> ajustmentIdList) {
		adjustmentMapper.canceladjustmentlist(ajustmentIdList);
	}

	public Double getfixprice(Adjustment action) {
		return adjustmentMapper.getfixprice(action);
	}

	public ArrayList<Adjustment> readbyproductandstate(Adjustment action) {
		return adjustmentMapper.readbyproductandstate(action);
	}

	public ArrayList<Adjustment> listcreatedbyproductids(AdjustmentSearch action) {
		return adjustmentMapper.listcreatedbyproductids(action);
	}

	public List<Adjustment> readByProductIdAndDateRange(DatesIdAction action) {
		return adjustmentMapper.readByProductIdAndDateRange(action);
	}

}
