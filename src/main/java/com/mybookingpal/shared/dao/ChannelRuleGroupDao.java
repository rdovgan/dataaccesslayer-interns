package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelRuleGroupMapper;
import com.mybookingpal.dal.shared.ChannelRuleGroup;

/**
 * @author Danijel Blagojevic
 *
 */
@Repository
public class ChannelRuleGroupDao {
	
	@Autowired
	private ChannelRuleGroupMapper channelRuleGroupMapper;
	
	public void create(ChannelRuleGroup channelRuleGroup) {
		channelRuleGroupMapper.create(channelRuleGroup);
	}
	
	public List<ChannelRuleGroup> getActiveRuleGroupsByProductId(Integer productId) {
		return channelRuleGroupMapper.getActiveRuleGroupsByProductId(productId);
	}
	
	public void update(ChannelRuleGroup channelRuleGroup) {
		channelRuleGroupMapper.update(channelRuleGroup);
	}
	
	public ChannelRuleGroup getByRuleId(String ruleId) {
		return channelRuleGroupMapper.getByRuleId(ruleId);
	}

}
