package com.mybookingpal.shared.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.CommissionMapper;
import com.mybookingpal.dal.shared.Commission;

@Repository
public class CommissionDao {

	@Autowired
	CommissionMapper commissionMapper;

	public void create(Commission action) {
		commissionMapper.create(action);
	}

	public Commission read(Integer id) {
		return commissionMapper.read(id);
	}

	public void update(Commission action) {
		commissionMapper.update(action);
	}

	public void updateCommissionByTypeAndPmId(Commission commission) {
		commissionMapper.updateCommissionByTypeAndPmId(commission);
	}

	public void delete(Integer id) {
		commissionMapper.delete(id);
	}

	public List<Commission> readCommissionsListByPm(Integer pmId) {
		return commissionMapper.readCommissionsListByPm(pmId);
	}

	public ArrayList<Commission> readBpCommissionsForPm(Integer pmId) {
		return commissionMapper.readBpCommissionsForPm(pmId);
	}

	public List<Commission> readBpSeasonalCommissionsForPmInDateRange(Commission commission) {
		return commissionMapper.readBpSeasonalCommissionsForPmInDateRange(commission);
	}

	public List<Commission> readPmIncludedCommissionsForPmInDateRange(Integer partyId, Integer toPartyId, String bookingDate) {
		return commissionMapper.readPmIncludedCommissionsForPmInDateRange(partyId, toPartyId, bookingDate);
	}

	public List<Commission> readPmIncludedCommissionsForPm(Integer partyId, Integer toPartyId) {
		return commissionMapper.readPmIncludedCommissionsForPm(partyId, toPartyId);
	}

	public List<Commission> readCommissionsForPmInDateRange(Integer partyId, Integer toPartyId, Date bookingDate, Integer type) {
		return commissionMapper.readCommissionsForPmInDateRange(partyId, toPartyId, bookingDate, type);
	}

	public List<Commission> readCommissionsForPm(Integer partyId, Integer toPartyId, Integer type) {
		return commissionMapper.readCommissions(partyId, toPartyId, type);
	}

	public List<Commission> readCommissionListByPmAndChannelPartnerPartyId(Integer pmId, Integer channelPartnerPartyId) {
		return commissionMapper.readCommissionListByPmAndChannelPartnerPartyId(pmId, channelPartnerPartyId);
	}
}
