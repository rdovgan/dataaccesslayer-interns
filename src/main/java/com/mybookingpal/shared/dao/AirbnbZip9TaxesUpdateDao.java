/**
 * 
 */
package com.mybookingpal.shared.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.AirbnbZip9TaxesUpdateMapper;


/**
 * @author Filip Pankovic
 *
 */
@Repository
public class AirbnbZip9TaxesUpdateDao {
	
	@Autowired
	private AirbnbZip9TaxesUpdateMapper  airbnbZip9TaxesUpdateMapper;
	
	public void create(String zip9UpdateList) {
		airbnbZip9TaxesUpdateMapper.create(zip9UpdateList);
	}

}
