package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.GroupedReservationToTransactionMapper;
import com.mybookingpal.dal.shared.GroupedReservationToTransaction;

@Repository
public class GroupedReservationToTransactionDao {
	
	@Autowired
	GroupedReservationToTransactionMapper groupedReservationToTransactionMapper;

	private static final Class<GroupedReservationToTransactionMapper> MAPPER_CLASS = GroupedReservationToTransactionMapper.class;

	public void create(GroupedReservationToTransaction groupedReservationToTransaction) {
		groupedReservationToTransactionMapper.create(groupedReservationToTransaction);
	}

	public GroupedReservationToTransaction read(Long id) {
		return groupedReservationToTransactionMapper.read(id);
	}

	public List<Long> readPaymentTransactionIdsByGroupedReservationIdAndType(Long groupedReservationId,
			GroupedReservationToTransaction.TransactionType transactionType) {
		return groupedReservationToTransactionMapper
				.readTransactionIdsByGroupedReservationIdAndType(groupedReservationId, transactionType);
	}

	public String readStateOfLastPaymentTransactionByGroupReservationId(Long groupedReservationId) {
		return groupedReservationToTransactionMapper.readStateOfLastPaymentTransactionByGroupReservationId(groupedReservationId);
	}

	public GroupedReservationToTransaction readGroupedReservationWithPendingTransactionByGroupedReservationId(Long groupedReservationId) {
		return groupedReservationToTransactionMapper.readGroupedReservationWithPendingTransactionByGroupedReservationId(groupedReservationId);
	}

	public GroupedReservationToTransaction readByTransactionIdAndType(Long transactionId, GroupedReservationToTransaction.TransactionType transactionType) {
		return groupedReservationToTransactionMapper.readByTransactionIdAndType(transactionId, transactionType);
	}

	public GroupedReservationToTransaction readLastPaymentGroupedReservationTransaction(String reservationId) {
		return groupedReservationToTransactionMapper.readLastGroupedReservationTransactionByGroupIdAndType(reservationId,
				GroupedReservationToTransaction.TransactionType.PAYMENT_TRANSACTION);
	}

	public GroupedReservationToTransaction readGroupedReservationByTransactionTypeAndTransactionId(Long transactionId,
			GroupedReservationToTransaction.TransactionType transactionType) {
		return groupedReservationToTransactionMapper
				.readGroupedReservationByTransactionTypeAndTransactionId(transactionId, transactionType);
	}

}
