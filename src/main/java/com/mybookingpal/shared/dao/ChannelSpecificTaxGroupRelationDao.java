package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelSpecificTaxGroupRelationMapper;
import com.mybookingpal.dal.shared.ChannelSpecificTaxGroupRelation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ChannelSpecificTaxGroupRelationDao {

    @Autowired
    private ChannelSpecificTaxGroupRelationMapper channelSpecificTaxGroupRelationMapper;

    public List<ChannelSpecificTaxGroupRelation> getByGroupIdsAndChannelId(List<Integer> groupIds, Integer channelId) {
        return channelSpecificTaxGroupRelationMapper.getByGroupIdsAndChannelId(groupIds, channelId);
    }
}
