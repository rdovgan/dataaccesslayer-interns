package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.CustomerMapper;
import com.mybookingpal.dal.shared.Customer;

@Repository
public class CustomerDao {

	@Autowired
	CustomerMapper customerMapper;

	public void createCustomer(Customer customer) {
		customerMapper.createCustomer(customer);
	}

	public void updateCustomer(Customer customer) {
		customerMapper.updateCustomer(customer);
	}

	public Customer readCustomer(Integer id) {
		return customerMapper.readCustomer(id);
	}

	public Customer readCustomerByReservationId(Integer reservationId) {
		return customerMapper.readCustomerByReservationId(reservationId);
	}

	public List<String> readMarriottCustomerNames() {
		return customerMapper.readMarriottCustomerNames();
	}

	public List<Customer> readDecryptedCustomers(List<Integer> ids) {
		return customerMapper.readDecryptedCustomers(ids);
	}
}
