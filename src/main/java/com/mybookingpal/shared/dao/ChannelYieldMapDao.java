package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelYieldMapMapper;
import com.mybookingpal.dal.shared.ChannelYieldMap;

/**
 * @author Danijel Blagojevic
 *
 */
@Repository
public class ChannelYieldMapDao {

	@Autowired
	private ChannelYieldMapMapper channelYieldMapMapper;
	
	public void create(ChannelYieldMap channelYieldMap) {
		channelYieldMapMapper.create(channelYieldMap);
	}
	
	public List<ChannelYieldMap> getActiveRuleGroupsByRuleId(String ruleId) {
		return channelYieldMapMapper.getActiveRuleGroupsByRuleId(ruleId);
	}
	
	public void update(ChannelYieldMap channelYieldMap) {
		channelYieldMapMapper.update(channelYieldMap);
	}
	
	public List<ChannelYieldMap> getActiveByYieldId(Integer yieldId) {
		return channelYieldMapMapper.getActiveByYieldId(yieldId);
	}
	
	public ChannelYieldMap getActiveByYieldIdAndRuleId(Integer yieldId, String ruleId) {
		return channelYieldMapMapper.getActiveByYieldIdAndRuleId(yieldId, ruleId);
	}
}
