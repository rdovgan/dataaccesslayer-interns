package com.mybookingpal.shared.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.PropertyManagerAdditionalContactsMapper;

import com.mybookingpal.dal.shared.PropertyManagerAdditionalContacts;

@Repository
public class PropertyManagerAdditionalContactsDAO {

	private static final Logger LOG = Logger.getLogger(PropertyManagerAdditionalContactsDAO.class);

	@Autowired
	private PropertyManagerAdditionalContactsMapper mapper;

	@Autowired
	ApplicationContext ctx;

	public List<PropertyManagerAdditionalContacts> getByIdAndType(PropertyManagerAdditionalContacts propertyManagerAdditionalContacts) {
		return mapper.getByIdAndType(propertyManagerAdditionalContacts);
	}

	public void getByPmId(int pmId) {
		mapper.getByPmId(pmId);
	}

	public void insertList(List<PropertyManagerAdditionalContacts> additionalContactsList) {
		mapper.insertList(additionalContactsList);
	}

	public void deleteList(List<PropertyManagerAdditionalContacts> additionalContactsList) {
		mapper.deleteList(additionalContactsList);
	}
	
	public void deleteListByPmId(int pmId) {
		mapper.deleteListByPmId(pmId);
	}

	public List<PropertyManagerAdditionalContacts> getByPropertyId(PropertyManagerAdditionalContacts propertyManagerAdditionalContacts) {
		return mapper.getByPropertyId(propertyManagerAdditionalContacts);
	}
	
	public List<PropertyManagerAdditionalContacts> getByPmIdOrPropertyId(PropertyManagerAdditionalContacts propertyManagerAdditionalContacts) {
		return mapper.getByPmIdOrPropertyId(propertyManagerAdditionalContacts);
	}

	public List<PropertyManagerAdditionalContacts> getByPmIdOrPropertyIds(Integer pmId, List<Integer> productIds) {
		return mapper.getByPmIdOrPropertyIds(pmId,productIds);
	}
	
	public List<PropertyManagerAdditionalContacts> getByPropertyIdAndType(PropertyManagerAdditionalContacts propertyManagerAdditionalContacts) {
		return mapper.getByPropertyIdAndType(propertyManagerAdditionalContacts);
	}
	
}
