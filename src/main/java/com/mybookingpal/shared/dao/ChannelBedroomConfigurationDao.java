package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelBedroomConfigurationMapper;
import com.mybookingpal.dal.shared.ChannelBedroomConfiguration;

/**
 * @author Danijel Blagojevic
 *
 */
@Repository
public class ChannelBedroomConfigurationDao {

	@Autowired
	ChannelBedroomConfigurationMapper channelBedroomConfigurationMapper;
	
	public void create(ChannelBedroomConfiguration channelBedroomConfiguration) {
		channelBedroomConfigurationMapper.create(channelBedroomConfiguration);
	}
	
	public void delete(Integer id) {
		channelBedroomConfigurationMapper.delete(id);
	}
	
	public List<ChannelBedroomConfiguration> getByProductId(Integer productId) {
		return channelBedroomConfigurationMapper.getByProductId(productId);
	}
	
	public List<ChannelBedroomConfiguration> getByProductIdAndProductBedroomIds(Integer productId, List<Integer> productBedroomIds) {
		return channelBedroomConfigurationMapper.getByProductIdAndProductBedroomIds(productId, productBedroomIds);	
	}
	
	public List<Integer> getByChannelRoomId(Integer channelRoomId) {
		return channelBedroomConfigurationMapper.getByChannelRoomId(channelRoomId);
	}
}
