package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ProductMapper;
import com.mybookingpal.dal.shared.IdVersion;
import com.mybookingpal.dal.shared.NameIdAction;
import com.mybookingpal.dal.shared.Product;
import com.mybookingpal.dal.shared.ProductAction;
import com.mybookingpal.dal.shared.ProductChannel;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Repository
public class ProductDaoImpl implements ProductDao {

	@Autowired
	private ProductMapper productMapper;

	public List<Product> readAllActiveProperties(NameIdAction nameIdAction) {
		return productMapper.readAllActiveProperties(nameIdAction);
	}

	public List<Product> readAllActiveOrSuspendedProperties(NameIdAction nameIdAction) {
		return productMapper.readAllActiveOrSuspendedProperties(nameIdAction);
	}

	public Product read(String id) {
		return productMapper.read(id);
	}

	public Product readById(Integer id) {
		return productMapper.readById(id);
	}

	public Product getProductAvoidNPE(Integer id) {
		return productMapper.getProductAvoidNPE(id);
	}


	public ArrayList<String> activeMultiUnitSubProductIdListBySupplier(NameIdAction supplierId) {
		return productMapper.activeMultiUnitSubProductIdListBySupplier(supplierId);
	}

	public ArrayList<String> activeProductIdListBySupplier(NameIdAction supplierId) {
		return productMapper.activeProductIdListBySupplier(supplierId);
	}

	public ArrayList<String> activeSingleUnitProductIdListBySupplier(NameIdAction supplierId) {
		return productMapper.activeSingleUnitProductIdListBySupplier(supplierId);
	}

	public ArrayList<String> activeMultiUnitKeyRootProductIdListBySupplier(NameIdAction supplierId) {
		return productMapper.activeMultiUnitKeyRootProductIdListBySupplier(supplierId);
	}

	public ArrayList<String> allMultiUnitKeyRootProductIdListBySupplier(NameIdAction supplierId) {
		return productMapper.allMultiUnitKeyRootProductIdListBySupplier(supplierId);
	}

	public ArrayList<String> activeMultiUnitRepRootProductIdListBySupplier(String supplierId) {
		NameIdAction nameIdAction = new NameIdAction();
		nameIdAction.setSupplierid(supplierId);
		return productMapper.activeMultiUnitRepRootProductIdListBySupplier(nameIdAction);
	}

	public List<Product> activeMultiUnitRepRootProductListBySupplier(String supplierId) {
		NameIdAction nameIdAction = new NameIdAction();
		nameIdAction.setSupplierid(supplierId);
		return productMapper.activeMultiUnitRepRootProductListBySupplier(supplierId);
	}

	public List<String> activeMultiUnitProductIdListByParentId(String supplierId, String productId) {
		NameIdAction nameIdAction = new NameIdAction();
		nameIdAction.setSupplierid(supplierId);
		nameIdAction.setParentId(productId);
		return productMapper.activeMultiUnitProductIdListByParentId(nameIdAction);
	}

	public List<Product> activeMultiUnitProductsByRootId(String hotelId) {
		return productMapper.activeMultiUnitProductsByRootId(hotelId);
	}

	public List<Product> readByExample(Product example) {
		return productMapper.readByExample(example);
	}

	@Override
	public List<IdVersion> getMaxVersions(List<Integer> productIds) {
		return productMapper.getMaxVersions(productIds);
	}

	@Override
	public ProductChannel getMbpProductWithMTCCheck(String id, String abbreviation) {
		return productMapper.getMbpProductWithMTCCheck(id, abbreviation);
	}

	public List<Product> getAllAvailableRoomsForRoot(Long rootId) {
		return productMapper.getAllAvailableRoomsForRoot(rootId);
	}

	@Override
	public List<Product> readProductListByIdList(List<Integer> productIdList) {
		if (productIdList == null || productIdList.isEmpty()) {
			return Collections.emptyList();
		}
		return productMapper.readProductCollectionByIdCollection(productIdList);
	}

	@Override
	public List<Integer> getSubProductIds(Integer parentId) {
		return productMapper.getSubProductIds(parentId);
	}

	public ArrayList<String> activeProductIdListBySupplierForChannel(ProductAction supplier) {
		return productMapper.activeProductIdListBySupplierForChannel(supplier);
	}

	public List<String> activeProductIdListBySupplierForChannelWithoutOWN(ProductAction supplier) {
		return productMapper.activeProductIdListBySupplierForChannelWithoutOWN(supplier);
	}

	public ArrayList<String> activeSGLProductIdListBySupplierForChannel(ProductAction supplier) {
		return productMapper.activeSGLProductIdListBySupplierForChannel(supplier);
	}

	public ArrayList<String> allProductIdListBySupplierForChannel(ProductAction supplier) {
		return productMapper.allProductIdListBySupplierForChannel(supplier);
	}

	public ArrayList<String> activeProductIdListBySupplierForApartmentsApart(NameIdAction supplierId) {
		return productMapper.activeProductIdListBySupplierForApartmentsApart(supplierId);
	}

	public ArrayList<String> activeProductIdListByOwnerForChannel(ProductAction supplierId) {
		return productMapper.activeProductIdListByOwnerForChannel(supplierId);
	}

	public ArrayList<String> inactiveProductIdListBySupplierForChannel(NameIdAction supplierId) {
		return productMapper.inactiveProductIdListBySupplierForChannel(supplierId);
	}

	public ArrayList<String> inactiveProductIdListByOwnerForChannel(NameIdAction supplierId) {
		return productMapper.inactiveProductIdListByOwnerForChannel(supplierId);
	}

	public ArrayList<String> activeProductIdListByPartyForChannel(ProductAction supplierId) {
		return productMapper.activeProductIdListByPartyForChannel(supplierId);
	}

	public ArrayList<String> activeProductIdListByOwnerForChannelByVersion(ProductAction supplierId) {
		return productMapper.activeProductIdListByOwnerForChannelByVersion(supplierId);
	}

	public ArrayList<String> activeProductIdListByPartyForChannelByVersion(ProductAction supplierId) {
		return productMapper.activeProductIdListByPartyForChannelByVersion(supplierId);
	}

	public ArrayList<String> activeProductIdListBySupplierByVersion(ProductAction supplierId) {
		return productMapper.activeProductIdListBySupplierByVersion(supplierId);
	}

	public ArrayList<String> activeSGLProductIdListBySupplierByVersion(ProductAction supplierId) {
		return productMapper.activeSGLProductIdListBySupplierByVersion(supplierId);
	}

	public ArrayList<String> inactiveProductIdListByOwnerForChannelByVersion(ProductAction supplierId) {
		return productMapper.inactiveProductIdListByOwnerForChannelByVersion(supplierId);
	}

	public ArrayList<String> inactiveProductIdListByPartyForChannelByVersion(ProductAction supplierId) {
		return productMapper.inactiveProductIdListByPartyForChannelByVersion(supplierId);
	}

	public ArrayList<String> inactiveProductIdListBySupplierByVersion(ProductAction supplierId) {
		return productMapper.inactiveProductIdListBySupplierByVersion(supplierId);
	}

	public ArrayList<String> inactiveSGLProductIdListBySupplierByVersion(ProductAction supplierId) {
		return productMapper.inactiveSGLProductIdListBySupplierByVersion(supplierId);
	}

	public ArrayList<String> activeAndInactiveProductIdListBySupplier(NameIdAction supplierId) {
		return productMapper.activeAndInactiveProductIdListBySupplier(supplierId);
	}

	public List<String> getAllProductsByPartyForChannelFromChannelParty(NameIdAction supplierId) {
		return productMapper.getAllProductsByPartyForChannelFromChannelParty(supplierId);
	}

	public List<String> getAllProductsBySupplierForChannel(NameIdAction supplierId) {
		return productMapper.getAllProductsBySupplierForChannel(supplierId);
	}

	public Product getProductFromLastTimeStamp(Product product) {
		return productMapper.getProductFromLastTimeStamp(product);
	}

	public List<Product> productlistbyids(List<String> productids) {
		return productMapper.productlistbyids(productids);
	}

	public List<String> getOWNPropertiesBySupplierId(String supplierId) {
		return productMapper.getOWNPropertiesBySupplierId(supplierId);
	}

	public ArrayList<String> inactiveOrUndistributedProductIdListBySupplier(NameIdAction supplierId) {
		return productMapper.inactiveOrUndistributedProductIdListBySupplier(supplierId);
	}

	public List<String> inactiveOrUndistributedProductIdListBySupplierWithoutOWN(NameIdAction supplierId) {
		return productMapper.inactiveOrUndistributedProductIdListBySupplierWithoutOWN(supplierId);
	}

	public ArrayList<String> allMultiUnitKeyProductIdListByRootId(NameIdAction supplierId) {
		return productMapper.allMultiUnitKeyProductIdListByRootId(supplierId);
	}

	public ArrayList<String> inactiveProductIdListByPartyForChannel(NameIdAction supplierId) {
		return productMapper.inactiveProductIdListByPartyForChannel(supplierId);
	}

	public List<Product> getProductByName(String hotelName) {
		return productMapper.getProductByName(hotelName);
	}

	public List<String> getProductIdsByParentId(String productId) {
		return productMapper.getProductIdsByParentIds(Collections.singletonList(productId));
	}

	public List<Product> getProductsByParentId(String productId) {
		return productMapper.getProductsByParentIds(Collections.singletonList(productId));
	}

	public void update(Product product) {
		productMapper.update(product);
	}

	public List<String> fetchActiveMultiUnitKeyProductListByIds(@Param("list") List<String> products) {
		return productMapper.fetchActiveMultiUnitKeyProductListByIds(products);
	}

	public List<String> fetchActiveMultiUnitRepProductListByIds(@Param("list") List<String> products) {
		return productMapper.fetchActiveMultiUnitRepProductListByIds(products);
	}

	public List<Product> readAllSupplier(NameIdAction nameIdAction) {
		return productMapper.readAllSupplier(nameIdAction);
	}

	public List<Product> inactiveProductListBySupplier(NameIdAction supplierId) {
		return productMapper.inactiveProductListBySupplier(supplierId);
	}

	public List<String> activeMuliUnitProductIdListBySupplier(NameIdAction supplierId) {
		return productMapper.activeMuliUnitProductIdListBySupplier(supplierId);
	}

	public ArrayList<String> allSinglePropertiesIdListBySupplier(NameIdAction supplierId) {
		return productMapper.allSinglePropertiesIdListBySupplier(supplierId);
	}

	public List<Product> readAllActivePropertiesByPropertyIds(List<String> productIds) {
		return productMapper.readAllActivePropertiesByPropertyIds(productIds);
	}
	
	public List<Product> readAllPropertiesByPropertyIds(List<String> productIds){
		return productMapper.readAllPropertiesByPropertyIds(productIds);
	}

	public List<Product> readAllActivePropertiesByPmIds(List<String> pmIds) {
		return productMapper.readAllActivePropertiesByPmIds(pmIds);
	}

	public List<String> allOWNProductsFromChannelProductMap(String supplierId, Integer channelId) {
		return productMapper.allOWNProductsFromChannelProductMap(supplierId, channelId);
	}

	public List<String> allMLTProductsFromChannelProductMap(String supplierId, Integer channelId) {
		return productMapper.allMLTProductsFromChannelProductMap(supplierId, channelId);
	}

	@Override
	public List<Product> rootProducts(String supplierId) {
		return productMapper.rootProducts(supplierId);
	}

	@Override
	public List<Product> allRootProducts(String supplierId, Date version) {
		return productMapper.allRootProducts(supplierId, version);
	}

	@Override
	public List<Product> allRootProductsFromChannelProductMap(String supplierId, Integer channelId, Date version) {
		return productMapper.allRootProductsFromChannelProductMap(supplierId, channelId, version);
	}

	@Override
	public List<Product> distributedRootProducts(String supplierId, Integer channelId) {
		return productMapper.distributedRootProducts(supplierId, channelId);
	}

	@Override
	public List<Product> readRootProducts(List<String> productIds) {
		return productMapper.readRootProducts(productIds);
	}

	public void markAsFinal(Integer id) {
		productMapper.markAsFinal(id);
	}

	@Override
	public List<String> findKeyProductsForMlt(String parentId) {
		return productMapper.findKeyProductsForMlt(parentId);
	}

	@Override
	public Integer getBoookingSettingForProduct(Integer productId) {
		return productMapper.getBoookingSettingForProduct(productId);
	}

	@Override
	public List<String> getFlagsForProduct(Integer productId) {
		return productMapper.getFlagsForProduct(productId);
	}

	@Override
	public Product readByAltIdAndSupplierId(String altId, String supplierId) {
		return productMapper.readByAltIdAndSupplierId(altId, supplierId);
	}

	@Override
	public List<Integer> getAllNewProductsByVersionForChannelPortal(ProductAction productAction) {
		return productMapper.getAllNewProductsByVersionForChannelPortal(productAction);

	}

	@Override
	public List<String> activePRMAndSUBProductIdListByParentId(ProductAction parentId) {
		return productMapper.activePRMAndSUBProductIdListByParentId(parentId);
	}

	@Override
	public List<String> unDistributedMLTProductIdListBySupplier(ProductAction productAction) {
		return productMapper.unDistributedMLTProductIdListBySupplier(productAction);
	}

	@Override
	public List<String> allInactivePRMAndSUBProductIdListByParentId(ProductAction parentId) {
		return productMapper.allInactivePRMAndSUBProductIdListByParentId(parentId);
	}

	public List<Product> readMarriottProductsForGoogleChannel(Integer marriottChannelId) {
		return productMapper.readMarriottProductsForGoogleChannel(marriottChannelId);
	}
}
