package com.mybookingpal.shared.dao;

import java.util.List;

import com.mybookingpal.dal.dao.entities.AttributeDisplayModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.dao.entities.AttributeDisplayWithProduct;
import com.mybookingpal.dal.server.api.AttributeDisplayMapper;

@Repository
public class AttributeDisplayDao {

	@Autowired
	private AttributeDisplayMapper mapper;

	public List<AttributeDisplayWithProduct> readAttributesForRoot(Long productId) {
		return mapper.readAttributesForRoot(productId);
	}

	public List<AttributeDisplayWithProduct> readAttributesForRoom(List<Long> productIds, Boolean updateType) {
		return mapper.readAttributesForRoom(productIds, updateType);
	}

	public List<AttributeDisplayWithProduct> readAttributesForResort(List<Long> productIds, Boolean updateType) {
		return mapper.readAttributesForResort(productIds, updateType);
	}

	public List<AttributeDisplayWithProduct> readAttributes(List<Long> productIds, Boolean updateType) {
		return mapper.readAttributes(productIds, updateType);
	}

	public List<AttributeDisplayWithProduct> readProductsAttributesWithCategories(List<Long> productIds, String categoryType) {
		return mapper.readProductsAttributesWithCategories(productIds, categoryType);
	}

	public List<AttributeDisplayModel> readAttributeDisplaysByAttributeCodes (List<String> attributeCodes) {
		return mapper.readAttributeDisplaysByAttributeCodes(attributeCodes);
	}

	public List<AttributeDisplayWithProduct> readAllPossibleAttributes() {
		return mapper.readAllPossibleAttributes();
	}

	public List<AttributeDisplayWithProduct> readDefiningAttributesNotIncludeCurrentForRoot(List<String> activeAttributes) {
		return mapper.readDefiningAttributesNotIncludeCurrentForRoot(activeAttributes);
	}

	public List<AttributeDisplayWithProduct> readDefiningAttributesNotIncludeCurrentForRoom(List<String> activeAttributes) {
		return mapper.readDefiningAttributesNotIncludeCurrentForRoom(activeAttributes);
	}

	public List<AttributeDisplayWithProduct> readDefiningAttributesForRoom() {
		return mapper.readDefiningAttributesForRoom();
	}

	public List<AttributeDisplayWithProduct> getAmenityByDisplayCategory(String categoryType) {
		return mapper.getAmenityByDisplayCategory(categoryType);
	}

	public List<AttributeDisplayWithProduct> readDefiningAttributeForRoot() {
		return mapper.readDefiningAttributeForRoot();
	}

	public List<AttributeDisplayWithProduct> readAllProductAttributes(Long productId) {
		return mapper.readAllProductAttributes(productId);
	}

	public List<AttributeDisplayWithProduct> readUniqueChildrenAttributes(List<Long> productIds, Integer size) {
		return mapper.readUniqueChildrenAttributes(productIds, size);
	}

	public List<AttributeDisplayWithProduct> readBedTypeProductAttributes(Long productId, String categoryType) {
		return mapper.readBedTypeProductAttributes(productId, categoryType);
	}

	public List<AttributeDisplayWithProduct> readAllPossibleBedTypes(String categoryType) {
		return mapper.readAllPossibleBedTypes(categoryType);
	}
}
