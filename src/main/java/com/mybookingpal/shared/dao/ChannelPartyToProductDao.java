package com.mybookingpal.shared.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelPartyToProductMapper;
import com.mybookingpal.dal.shared.ChannelPartyToProduct;

@Repository
public class ChannelPartyToProductDao {

	@Autowired
	private ChannelPartyToProductMapper channelPartyToProductMapper;

	public void createChannelPartyToProduct(ChannelPartyToProduct tempChannelPartyToProduct) {
		channelPartyToProductMapper.createChannelPartyToProduct(tempChannelPartyToProduct);
	}

	public void updateChannelProductId(ChannelPartyToProduct tempChannelPartyToProduct) {
		channelPartyToProductMapper.updateChannelProductId(tempChannelPartyToProduct);
	}

	public ChannelPartyToProduct readByProductId(String productId) {
		return channelPartyToProductMapper.readByProductId(productId);
	}

	public ChannelPartyToProduct readByProductId(ChannelPartyToProduct tempChannelPartyToProduct) {
		return channelPartyToProductMapper.readByProductId(tempChannelPartyToProduct);
	}

	public List<ChannelPartyToProduct> readByPartyId(ChannelPartyToProduct tempChannelPartyToProduct) {
		return channelPartyToProductMapper.readByPartyId(tempChannelPartyToProduct);
	}

	public List<String> readBySupplierId(ChannelPartyToProduct tempChannelPartyToProduct) {
		return channelPartyToProductMapper.readBySupplierId(tempChannelPartyToProduct);
	}

	public List<String> readProductIdByPartyId(ChannelPartyToProduct tempChannelPartyToProduct) {
		return channelPartyToProductMapper.readProductIdByPartyId(tempChannelPartyToProduct);
	}

	public List<String> readByProductIdAndPartyId(ChannelPartyToProduct tempChannelPartyToProduct) {
		return channelPartyToProductMapper.readByProductIdAndPartyId(tempChannelPartyToProduct);
	}
	
	public List<String> readSupplierIdByPartyId(String supplierId) {
		return channelPartyToProductMapper.readSupplierIdByPartyId(supplierId);
	}
	
	public List<String> getVirtualAccountsForPM() {
		return channelPartyToProductMapper.getVirtualAccountsForPM();
	}
	
	public String getVirtualAccountsForPMBySupplierId(ChannelPartyToProduct tempChannelPartyToProduct) {
		String tempSupplierID = tempChannelPartyToProduct.getSupplierId();
		if (StringUtils.isBlank(tempSupplierID)) {
			return null;
		}
		String resultId = channelPartyToProductMapper.getVirtualAccountsForPMBySupplierId(tempChannelPartyToProduct);
		if (StringUtils.isBlank(resultId)) {
			return tempSupplierID;
		}
		return resultId;
	}
}
