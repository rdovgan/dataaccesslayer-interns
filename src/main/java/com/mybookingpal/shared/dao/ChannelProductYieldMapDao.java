
package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelProductYieldMapMapper;
import com.mybookingpal.dal.shared.ChannelProductYieldMap;

@Repository
public class ChannelProductYieldMapDao {

	@Autowired
	private ChannelProductYieldMapMapper mapper;

	public List<ChannelProductYieldMap> getChannelRateId(ChannelProductYieldMap channelProductYieldMap) {

		return mapper.getChannelRateId(channelProductYieldMap);
	}

	public void create(ChannelProductYieldMap action) {
		mapper.create(action);
	}
	
	
	public void update(ChannelProductYieldMap action){
		mapper.update(action);
	}
	
	public void deleteByProductId(ChannelProductYieldMap action){
		mapper.deleteByProductId(action);
	}
	
	public void deleteById(String id){
		mapper.deleteById(id);
	}
	
	public void deleteByChannelIdProductIdChannelRateId(ChannelProductYieldMap channelProductYieldMap){
		mapper.deleteByChannelIdProductIdChannelRateId(channelProductYieldMap);
	}
	
	public ChannelProductYieldMap getChannelRateName(ChannelProductYieldMap channelProductYieldMap) {

		return mapper.getChannelRateName(channelProductYieldMap);
	}

	public ChannelProductYieldMap getYieldRuleNameByChannelRateId(ChannelProductYieldMap channelProductYieldMap){
		return mapper.getYieldRuleNameByChannelRateId(channelProductYieldMap);
	}
	
	public List<ChannelProductYieldMap> getPromoRatesByChannelIdProductId(ChannelProductYieldMap channelProductYieldMap){
		return mapper.getPromoRatesByChannelIdProductId(channelProductYieldMap);
	}
	
	public List<ChannelProductYieldMap> getByProductId(ChannelProductYieldMap channelProductYieldMap){
		return mapper.getByProductId(channelProductYieldMap);
	}
}
