package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelProductSettingsMapper;
import com.mybookingpal.dal.shared.ChannelProductSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ChannelProductSettingsDao {
	@Autowired
	ChannelProductSettingsMapper mapper;

	public ChannelProductSetting readByProductIdAndChannelIdAndType(Integer productId, Integer channelId, Integer type) {
		ChannelProductSetting channelProductSetting = new ChannelProductSetting(productId, channelId, type);
		return mapper.readByProductIdAndChannelIdAndType(channelProductSetting);
	}

	public List<ChannelProductSetting> read(ChannelProductSetting example) {
		return mapper.read(example);
	}

	public void create(ChannelProductSetting channelProductSetting) {
		mapper.create(channelProductSetting);
	}

	public void insertList(List<ChannelProductSetting> channelProductSettings) {
		mapper.insertList(channelProductSettings);
	}

	public void update(ChannelProductSetting action) {
		mapper.update(action);
	}

	public void delete(Integer productId, Integer channelId, Integer type){
		ChannelProductSetting channelProductSetting = new ChannelProductSetting(productId, channelId, type);
		mapper.delete(channelProductSetting);
	}



}
