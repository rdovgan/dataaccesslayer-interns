package com.mybookingpal.shared.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.PartnerMapper;
import com.mybookingpal.dal.shared.Partner;

import java.util.List;

@Repository
public class PartnerDao {

	@Autowired
	PartnerMapper partnerMapper;

	public final Partner fetchPartnerServiceById(String id) {
		//return partnerMapper.read(action.getId());
		return partnerMapper.read(id);
	}
	
	public Partner exists(String partyid){
		return partnerMapper.exists(partyid);
	}
	
	public Partner readByPartyId(Integer partyid){
		return partnerMapper.readByPartyId(partyid);
	}
	
	public Partner getParentByPartyId(String partyid){
		return partnerMapper.getParentByPartyId(partyid);
	}

	public Partner readAllByAbbrevation(String abbrevation){
		return partnerMapper.readAllByAbbrevation(abbrevation);
	}

	public void create(Partner partner) {
		partnerMapper.create(partner);
	}

	public List<Partner> readAllByOrganizationId(Integer organizationid) {
		return partnerMapper.readAllByOrganizationId(organizationid);
	}
}
