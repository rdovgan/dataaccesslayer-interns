package com.mybookingpal.shared.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.RelationMapper;
import com.mybookingpal.dal.shared.Relation;

@Repository
public class RelationDao {

	@Autowired
	private RelationMapper relationMapper;

	public List<String> getListRelationLineIds(Relation relation) {
		return relationMapper.lineids(relation);
	}
	
	public ArrayList<String> lineids(Relation relation){
		return relationMapper.lineids(relation);
	}
	
	public ArrayList<String> headids(Relation relation){
		return relationMapper.headids(relation);
	}
}
