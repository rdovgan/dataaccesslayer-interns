package com.mybookingpal.shared.dao;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelPromoMapMapper;
import com.mybookingpal.dal.shared.ChannelPromoMap;

@Repository
public class ChannelPromoMapDAO {

	private static final Logger LOG = Logger.getLogger(ChannelPromoMapDAO.class);

	@Autowired
	private ChannelPromoMapMapper mapper;

	@Autowired
	ApplicationContext ctx;

	public void createPromoMap(ChannelPromoMap channelPromoMap) {
		mapper.createPromoMap(channelPromoMap);
	}

	public void updatePromoMapByPromoId(ChannelPromoMap channelPromoMap) {
		mapper.updatePromoMapByPromoId(channelPromoMap);
	}
	
	public void update(ChannelPromoMap channelPromoMap) {
		mapper.update(channelPromoMap);
	}
	
	public void delete(String channelPromoMapId) {
		mapper.delete(channelPromoMapId);
	}

	public ChannelPromoMap readPromoMap(Integer yieldId) {
		return mapper.readPromoMap(yieldId);
	}
	
	public  List<ChannelPromoMap> readPromoMapsByYieldId(Integer yieldId) {
		return mapper.readPromoMapsByYieldId(yieldId);
	}

	public ChannelPromoMap readPromoMapByPromoId(String channelPromoId) {
		return mapper.readPromoMapByPromoId(channelPromoId);
	}
	
	public void deleteByProductIdChannelIdAndYieldId(ChannelPromoMap channelPromoMap) {
		mapper.deleteByProductIdChannelIdAndYieldId(channelPromoMap);
	}
	
	public List<ChannelPromoMap>  readByProductIdChannelIdAndYieldId(ChannelPromoMap channelPromoMap) {
		return mapper.readByProductIdChannelIdAndYieldId(channelPromoMap);
	}
	
	public void deleteByChannelIdProductIdChannelRateId(ChannelPromoMap channelPromoMap) {
		 mapper.deleteByChannelIdProductIdChannelRateId(channelPromoMap);
	}
	
	public List<ChannelPromoMap> readChannelPromoMapByYieldId(int yieldId){
		return mapper.readChannelPromoMapByYieldId(yieldId);
	}
	public List<ChannelPromoMap> readChannelPromoMapForAvailabilityStatus(int channelId,
			Date fromdate, Date todate, String entitytype){
		return mapper.readChannelPromoMapForAvailabilityStatus(channelId,fromdate,todate,entitytype);
	}
	public List<ChannelPromoMap> readChannelPromoMapForExpiredStatus(Date fromdate, Date todate, String entitytype){
		return mapper.readChannelPromoMapForExpiredStatus(fromdate,todate,entitytype);
	}
	public List<ChannelPromoMap> readPromoMapByChannelProductIdChannelId(ChannelPromoMap channelPromoMap){
		return mapper.readPromoMapByChannelProductIdChannelId(channelPromoMap);
	}
	public List<ChannelPromoMap> readChannelPromoMapByProductIdWithAvailabilityStatus(int channelId,
			Date fromdate, Date todate, String entitytype, List<String> productIds){
		return mapper.readChannelPromoMapByProductIdWithAvailabilityStatus(channelId,fromdate,todate,entitytype,productIds);
	}
	public List<ChannelPromoMap> readChannelPromoMapByProductIdWithExpiredStatus(Date fromdate, 
			Date todate, String entitytype, List<String> productIds){
		return mapper.readChannelPromoMapByProductIdWithExpiredStatus(fromdate,todate,entitytype,productIds);
	}
	public List<String> readPromotionApplicableChannelPromoMap(int channelId,
			Date fromdate, Date todate, String entitytype){
		return mapper.readPromotionApplicableChannelPromoMap(channelId,fromdate,todate,entitytype);
	}
	
}
