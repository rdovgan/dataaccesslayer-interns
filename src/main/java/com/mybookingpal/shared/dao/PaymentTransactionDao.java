package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.shared.PaymentTransaction;

/**
 * API for processing database operations associated with table PaymentTransaction
 */
@Repository
public interface PaymentTransactionDao {
	
	/**
	 * Insert new record of payment transaction into table PaymentTransaction
	 *
	 * @param sqlSession to use myBatis mapping
	 * @param paymentTransaction for creating new record in table
	 */
	void create(PaymentTransaction paymentTransaction);

	void update(PaymentTransaction paymentTransaction);

	List<PaymentTransaction> readByReservationId(Integer reservationId);

	PaymentTransaction read(Integer id);

	PaymentTransaction readLastFailedTransactionForGroupedReservation(Long groupedReservationId);

	List<PaymentTransaction> readAcceptedTransactionsByIds(List<Long> transactionIds);

	List<PaymentTransaction> readAllTransactionsByIds(List<Long> transactionIds);

	PaymentTransaction getInitialTransactionByReservationId(int reservationId);
}