package com.mybookingpal.shared.dao;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.LogPropertyUpdateMapper;
import com.mybookingpal.dal.shared.LogPropertyUpdate;

/**
 * @author Danijel Blagojevic
 *
 */
@Repository
public class LogPropertyUpdateDao {
	
	@Autowired
	LogPropertyUpdateMapper logPropertyUpdateMapper;
	
	public LogPropertyUpdate getLogPropertyUpdateAttributesFromLastTimeStamp(String productId, Date version) {
		return logPropertyUpdateMapper.getLogPropertyUpdateAttributesFromLastTimeStamp(productId, version);
	}
}
