package com.mybookingpal.shared.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ProductPolicyMapper;
import com.mybookingpal.dal.shared.ProductPolicy;
import com.mybookingpal.dal.shared.ProductPolicyWithName;

/**
 * @author Danijel Blagojevic
 *
 */
@Repository
public class ProductPolicyDao {

	@Autowired
	ProductPolicyMapper productPolicyMapper;
	
	
	public List<ProductPolicy> getByProductId(String productId) {
		return productPolicyMapper.getByProductId(productId);
	}
	
	public List<ProductPolicy> getActiveByProductId(String productId) {
		return productPolicyMapper.getActiveByProductId(productId);
	}
	
	public List<ProductPolicyWithName> getPoliciesWithNameByProductId(String productId) {
		return productPolicyMapper.getByProductIdWithName(productId);
	}
	
	public void insertList(List<ProductPolicy> productPolicies) {
		productPolicyMapper.insertList(productPolicies);
		
	}

    public List<ProductPolicy> getProductPoliciesFromLastTimeStamp(String productId, Date version) {
		return productPolicyMapper.getProductPoliciesFromLastTimeStamp(productId, version);
    }
}
