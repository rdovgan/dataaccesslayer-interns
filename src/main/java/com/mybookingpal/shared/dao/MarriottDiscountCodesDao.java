package com.mybookingpal.shared.dao;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.MarriottDiscountCodesMapper;
import com.mybookingpal.dal.shared.MarriottBlackoutDatesMap;
import com.mybookingpal.dal.shared.MarriottDiscountCode;
import com.mybookingpal.dal.shared.MarriottDiscountCodeInfo;
import com.mybookingpal.dal.shared.MarriottDiscountHmcMap;
import com.mybookingpal.dal.shared.MarriottDiscountMap;

@Repository
public class MarriottDiscountCodesDao {

	@Autowired
	MarriottDiscountCodesMapper marriottDiscountCodesMapper;

	public MarriottDiscountCode readDiscountCode(Integer id) {
		return marriottDiscountCodesMapper.readDiscountCode(id);
	}

	public MarriottDiscountCode readDiscountCodeByName(String discountCode) {
		return marriottDiscountCodesMapper.readDiscountCodeByName(discountCode);
	}

	public void createDiscountCode(MarriottDiscountCode discountCode) {
		marriottDiscountCodesMapper.createDiscountCode(discountCode);
	}

	public void createDiscountCode(SqlSession sqlSession, MarriottDiscountCode discountCode) {
		marriottDiscountCodesMapper.createDiscountCode(discountCode);
	}

	public void insertDiscountList(Integer discountCodeId, List<MarriottDiscountMap> discounts) {
		if (CollectionUtils.isEmpty(discounts)) {
			return;
		}
		marriottDiscountCodesMapper.insertDiscountList(discountCodeId, discounts);
	}

	public void insertHmcList(Integer discountCodeId, List<Integer> hmcs) {
		if (CollectionUtils.isEmpty(hmcs)) {
			return;
		}
		marriottDiscountCodesMapper.insertHmcList(discountCodeId, hmcs);
	}

	public void insertBlackOutDatesList(Integer discountCodeId, List<MarriottBlackoutDatesMap> blackoutDates) {
		if (CollectionUtils.isEmpty(blackoutDates)) {
			return;
		}
		marriottDiscountCodesMapper.insertBlackOutDatesList(discountCodeId, blackoutDates);
	}

	public List<MarriottDiscountMap> readDiscountList(Integer discountCodeId) {
		return marriottDiscountCodesMapper.readDiscountList(discountCodeId);
	}

	public List<MarriottDiscountHmcMap> readHmcList(Integer discountCodeId) {
		return marriottDiscountCodesMapper.readHmcList(discountCodeId);
	}

	public List<MarriottBlackoutDatesMap> readBlackOutDatesList(Integer discountCodeId) {
		return marriottDiscountCodesMapper.readBlackOutDatesList(discountCodeId);
	}

	public void moveToFinal(Integer id) {
		marriottDiscountCodesMapper.moveToFinal(id);
	}
	
	public void updateChannelDiscountId(MarriottDiscountCode discountCode) {
		marriottDiscountCodesMapper.updateChannelDiscountId(discountCode);
	}

	public void moveToFinal(SqlSession sqlSession, Integer id) {
		marriottDiscountCodesMapper.moveToFinal(id);
	}

	public List<MarriottDiscountCodeInfo> getDiscountCodesInfo() {
		return marriottDiscountCodesMapper.getDiscountCodesInfo();
	}
}
