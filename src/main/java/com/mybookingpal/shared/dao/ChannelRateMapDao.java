package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelRateMapMapper;
import com.mybookingpal.dal.shared.ChannelRateMap;

@Repository
public class ChannelRateMapDao {

	@Autowired
	ChannelRateMapMapper channelRateMapMapper;
	
	public List<ChannelRateMap> readChannelRateMap(ChannelRateMap tempChannelRateMap){
		return channelRateMapMapper.readChannelRateMap(tempChannelRateMap);
	}
	public List<ChannelRateMap> readChannelRateMapByYieldId(ChannelRateMap tempChannelRateMap){
		return channelRateMapMapper.readChannelRateMapByYieldId(tempChannelRateMap);
	}
	public ChannelRateMap exists(ChannelRateMap channelRateMap){
		return channelRateMapMapper.exists(channelRateMap);
	}
	public void create(ChannelRateMap action){
		channelRateMapMapper.create(action);
	}
    public void update(ChannelRateMap action){
    	channelRateMapMapper.update(action);
    }
    
}
