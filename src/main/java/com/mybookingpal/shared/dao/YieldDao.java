package com.mybookingpal.shared.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.shared.LocalDateRange;
import com.mybookingpal.dal.shared.NameIdUtils;
import com.mybookingpal.dal.shared.RoomPeriodChange;

import com.mybookingpal.dal.utils.CommonDateUtils;
import com.mybookingpal.utils.entity.NameId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.YieldMapper;
import com.mybookingpal.dal.shared.Yield;
import com.mybookingpal.dal.shared.yield.YieldSearch;

@Repository
public class YieldDao {

	@Autowired
	YieldMapper yieldMapper;

	public void create(Yield action) {
		yieldMapper.create(action);
	}
	
	public Yield read(String id) {
		return yieldMapper.read(id);
	}
	
	public Yield readByIdAndstate(Yield action) {
		return yieldMapper.readByIdAndstate(action);
	}
	

	public void update(Yield action) {
		yieldMapper.update(action);
	}

	public void copy(com.mybookingpal.utils.entity.NameId oldnew) {
		yieldMapper.copy(oldnew);
	}

	public Integer countbyentity(Yield action) {
		return yieldMapper.countbyentity(action);
	}

	public ArrayList<Yield> listbyentity(Yield action) {
		return yieldMapper.listbyentity(action);
	}

	public ArrayList<Yield> maximumgapfillers() {
		return yieldMapper.maximumgapfillers();
	}

	public Yield exists(Yield action) {
		return yieldMapper.exists(action);
	}

	public ArrayList<Yield> readByProductState(Yield action) {
		return yieldMapper.readByProductState(action);
	}

	public void insertList(ArrayList<Yield> yieldList) {
		yieldMapper.insertList(yieldList);
	}

	public void cancelYieldList(List<String> yieldIdList) {
		yieldMapper.cancelYieldList(yieldIdList);
	}

	public void cancelversion(Yield action) {
		yieldMapper.cancelversion(action);
	}

	@Deprecated
	public List<Yield> listByEntityAndDate(String productId, Date fromDate, Date toDate) {
		Yield yieldAction = new Yield();
		yieldAction.setEntitytype(NameIdUtils.Type.Product.name());
		yieldAction.setEntityid(productId);
		yieldAction.setFromDate(CommonDateUtils.toLocalDate(fromDate));
		yieldAction.setToDate(CommonDateUtils.toLocalDate(toDate));
		return listByEntityAndDate(yieldAction);
	}

	public List<Yield> listByEntityAndDate(String productId, LocalDate fromDate, LocalDate toDate) {
		Yield yieldAction = new Yield();
		yieldAction.setEntitytype(NameIdUtils.Type.Product.name());
		yieldAction.setEntityid(productId);
		yieldAction.setFromDate(fromDate);
		yieldAction.setToDate(toDate);
		return listByEntityAndDate(yieldAction);
	}

	public ArrayList<Yield> listByEntityAndDate(Yield action) {
		return yieldMapper.listByEntityAndDate(action);
	}
	
	public List<Yield> getYieldsInInitalState(Yield action) {
		return yieldMapper.getYieldsInInitialState(action);
	}

	public ArrayList<Yield> listByEntityAndDateWithFinal(Yield action) {
		return yieldMapper.listByEntityAndDateWithFinal(action);
	}
	
	public ArrayList<Yield> listByEntityAndDateOnlyFinal(Yield action) {
		return yieldMapper.readByEntityIdAndEntityTypeOnlyFinal(action);
	}

	public ArrayList<Yield> listbyproductids(YieldSearch action) {
		return yieldMapper.listbyproductids(action);
	}
	
	public ArrayList<Yield> readByEntityIdAndEntityTypeStateByVersion(Yield yield){
		return yieldMapper.readByEntityIdAndEntityTypeStateByVersion(yield);
	}
	
	public ArrayList<Yield> readByEntityIdAndEntityTypeOnlyFinal(Yield yield){
		return yieldMapper.readByEntityIdAndEntityTypeOnlyFinal(yield);
	}

	@Deprecated
	public List<Yield> getProductYields(Date fromDate, Date toDate, String productId) {
		Yield yield = new Yield();
		yield.setFromDate(CommonDateUtils.toLocalDate(fromDate));
		yield.setToDate(CommonDateUtils.toLocalDate(toDate));
		yield.setEntityid(productId);
		return yieldMapper.getProductYields(yield);
	}

	public List<Yield> getProductYields(LocalDate fromDate, LocalDate toDate, String productId) {
		Yield yield = new Yield();
		yield.setFromDate(fromDate);
		yield.setToDate(toDate);
		yield.setEntityid(productId);
		return yieldMapper.getProductYields(yield);
	}

	@Deprecated
	public ArrayList<Yield> getProductsForYieldByVersion(Date lastFetch, Date toDate, String productId) {
		Yield yield = new Yield();
		yield.setFromDate(CommonDateUtils.toLocalDate(lastFetch));
		yield.setToDate(CommonDateUtils.toLocalDate(toDate));
		yield.setVersion(lastFetch);
		yield.setEntityid(productId);
		return yieldMapper.getProductsForYieldByVersion(yield);
	}

	public ArrayList<Yield> getProductsForYieldByVersion(LocalDateTime lastFetch, LocalDate toDate, String productId) {
		Yield yield = new Yield();
		yield.setFromDate(lastFetch.toLocalDate());
		yield.setToDate(toDate);
		yield.setVersion(CommonDateUtils.toDate(lastFetch));
		yield.setEntityid(productId);
		return yieldMapper.getProductsForYieldByVersion(yield);
	}

	public List<RoomPeriodChange> readYieldsForPeriodForAllStateBySupplierIdByVersion(String supplierId, Date fromDate, Date toDate, Date lastFetch, Integer channelId) {
		return yieldMapper.readYieldsForPeriodForAllStateBySupplierIdByVersion(supplierId, fromDate, toDate, lastFetch, channelId);
	}

	public List<Yield> readInActiveByEntityAndVersion(Yield action) {
		return yieldMapper.readInActiveByEntityAndVersion(action);
	}

	public List<Yield> readByParamAndEntityAndBookingFromDate(Yield action) {
		return yieldMapper.readByParamAndEntityAndBookingFromDate(action);
	}

	public ArrayList<Yield> getPromoYieldWithinGivenDateRange(Yield yield){
		return yieldMapper.getPromoYieldWithinGivenDateRange(yield);
	}
	public ArrayList<Yield> getPromoYieldByVersionByProduct(Yield yield){
		return yieldMapper.getPromoYieldByVersionByProduct(yield);
	}
	public ArrayList<Yield> getAllPromoYieldsByVersion(Yield yield){
		return yieldMapper.getAllPromoYieldsByVersion(yield);
	}
	public ArrayList<Yield> getAllPromotionYieldsByVersionAndByProduct(Yield yield){
		return yieldMapper.getAllPromotionYieldsByVersionAndByProduct(yield);
	}
	public ArrayList<Yield> getExpiredPromoYield(Yield yield){
		return yieldMapper.getExpiredPromoYield(yield);
	}
	public ArrayList<Yield> getExpiredPromoYieldByVersion(Yield yield){
		return yieldMapper.getExpiredPromoYieldByVersion(yield);
	}
	public ArrayList<Yield> listByEntityVersionAndDateWithoutPromotion(Yield yield){
		return yieldMapper.listByEntityVersionAndDateWithoutPromotion(yield);
	}
	
	public List<Yield> getYieldsWithBookingDates(Yield action) {
		return yieldMapper.getYieldsWithBookingDates(action);
	}

	public ArrayList<Yield> getAllPromoYieldsByVersionAndState(Yield yield){
		return yieldMapper.getAllPromoYieldsByVersionAndState(yield);
	}

	public List<Yield> readForPeriodByProductIds(List<String> productIds, Date bookingFromDate, Date bookingToDate) {
		return yieldMapper.readForPeriodByProductIds(productIds, bookingFromDate, bookingToDate);
	}

	public List<String> getProductsWithNightlyYieldsWithBookingInterval(List<Integer> productIds, List<String> nightlyYieldTypes) {
		return yieldMapper.getProductsWithNightlyYieldsWithBookingInterval(productIds, nightlyYieldTypes);
	}

	public List<Yield> readYieldByEntityIdEntityTypeAndDates(Integer entityId, String entityType, List<LocalDateRange> dates) {
		return yieldMapper.readYieldByEntityIdEntityTypeAndDates(entityId, entityType, dates);
	}
}
