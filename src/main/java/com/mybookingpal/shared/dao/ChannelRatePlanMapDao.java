package com.mybookingpal.shared.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelRatePlanMapMapper;
import com.mybookingpal.dal.shared.ChannelRatePlanMap;

@Repository
public class ChannelRatePlanMapDao {

	@Autowired
	private ChannelRatePlanMapMapper ratePlanMapMapper;

	public void create(ChannelRatePlanMap ratePlanMap) {
		ratePlanMapMapper.create(ratePlanMap);
	}

	public void update(ChannelRatePlanMap ratePlanMap) {
		ratePlanMapMapper.update(ratePlanMap);
	}

	public ChannelRatePlanMap readByChannelIdAndRatePlanIdAndProductIdWithStateNotFinal(ChannelRatePlanMap ratePlanMap) {
		return ratePlanMapMapper.readByChannelIdAndRatePlanIdAndProductIdWithStateNotFinal(ratePlanMap);
	}

	public void updateState(ChannelRatePlanMap ratePlanMap) {
		ratePlanMapMapper.updateState(ratePlanMap);
	}

	public List<ChannelRatePlanMap> readByProductIdChannelId(ChannelRatePlanMap ratePlanMap) {
		return ratePlanMapMapper.readByProductIdChannelId(ratePlanMap);
	}

	public ChannelRatePlanMap findByProductIdAndChannelIdsAndRateplanIdNull(String productId, List<Integer> channelIds) {
		return ratePlanMapMapper.findByProductIdAndChannelIdsAndRateplanIdNull(productId, channelIds);
	}

	public List<ChannelRatePlanMap> getAllProductByPmAndByChannel(String supplierId, List<Integer> channelIds) {
		return ratePlanMapMapper.getAllProductByPmAndByChannel(supplierId, channelIds);
	}

	public List<Integer> getChannelIdsByProductId(String productId) {
		return ratePlanMapMapper.getChannelIdsByProductId(productId);
	}

	public List<Integer> getChannelIdsBySupplierId(String supplierId) {
		return ratePlanMapMapper.getChannelIdsBySupplierId(supplierId);
	}

	public ChannelRatePlanMap findByProductIdsChannelIdsWithChannelProductIdRoomIdNotNullRateplanIdNull(@Param("productIds") List<String> productIds,
			@Param("list") List<Integer> channelIds) {
		return ratePlanMapMapper.findByProductIdsChannelIdsWithChannelProductIdRoomIdNotNullRateplanIdNull(productIds, channelIds);
	}
	
	public List<ChannelRatePlanMap> findByProductIdsChannelIds(@Param("productIds") List<String> productIds, @Param("list") List<Integer> channelIds) {
		return ratePlanMapMapper.findByProductIdsChannelIds(productIds, channelIds);
	}
	
	public ChannelRatePlanMap findByChannelProductIdAndRateId( ChannelRatePlanMap channelRatePlanMap) {
		return ratePlanMapMapper.findByChannelProductIdAndRateId(channelRatePlanMap);
	}

}
