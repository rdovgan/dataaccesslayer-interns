package com.mybookingpal.shared.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.PropertyManagerSupportCCMapper;
import com.mybookingpal.dal.shared.PropertyManagerSupportCC;

@Repository
public class PropertyManagerSupportCCDao {

	@Autowired
	private PropertyManagerSupportCCMapper ccMapper;

	public final PropertyManagerSupportCC readbypartyid(Integer partyId) {
		return ccMapper.readbypartyid(partyId);

	}
}
