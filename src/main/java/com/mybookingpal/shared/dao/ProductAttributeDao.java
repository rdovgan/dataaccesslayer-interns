package com.mybookingpal.shared.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mybookingpal.dal.server.RazorServer;
import com.mybookingpal.dal.shared.IdVersion;
import com.mybookingpal.shared.dto.KeyCollection;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ProductAttributeMapper;
import com.mybookingpal.dal.server.api.ProductAttributeWithName;
import com.mybookingpal.dal.shared.ProductAttribute;

@Repository
public class ProductAttributeDao {

	@Autowired
	private ProductAttributeMapper productAttributeMapper;

	public ArrayList<String> readAttributesByProductId(Integer productId) {
		return productAttributeMapper.readAttributesByProductId(productId);
	}
	
	public Integer getAttributesCountByProductIdAndAttributeId(ProductAttribute productAttribute){
		return productAttributeMapper.getAttributesCountByProductIdAndAttributeId(productAttribute);
	}

	public List<ProductAttribute> getAllByProductId(Integer productId) {
		return productAttributeMapper.getAllByProductId(String.valueOf(productId));
	}
	
	/**
	 * Gets the IDs having the specified product attribute.
	 * If the product ID is set then return the attribute IDs
	 * else if the attribute ID is set then return the product IDs
	 * else return null.
	 * 
	 * @param sqlSession the current SQL session.
	 *@param productAttribute the specified product attribute.
	 * @return list
	 */
	public final List<String> read(ProductAttribute productAttribute) {
		if (productAttribute.noProductId()) {return productAttributeMapper.productIds(productAttribute);}
		else if (productAttribute.noAttributeId()) {return productAttributeMapper.attributeIds(productAttribute);}
		else {return null;}
	}
	
	public ProductAttribute readByProductIdAndAttribute(Integer productId, String attribute) {
		return productAttributeMapper.readByProductIdAndAttribute(productId, attribute);
	}
	
	public List<ProductAttribute> getProductAttributesFromLastTimeStamp(String productId, Date version) {
		return productAttributeMapper.getProductAttributesFromLastTimeStamp(productId, version);
	}
	
	public void insertList(List<ProductAttribute> productAttributeList) {
		productAttributeMapper.insertList(productAttributeList);
	}
	
	public void deleteHotelCodesByProductId(Integer productId) {
		productAttributeMapper.deleteHotelCodesByProductId(productId);
	}
	
	public void deleteRoomCodesByProductId(Integer productId) {
		productAttributeMapper.deleteRoomCodesByProductId(productId);
	}
	
	public List<String> productIds(ProductAttribute productAttribute) {
		return productAttributeMapper.productIds(productAttribute);
	}
	
	public List<String> attributeIds(ProductAttribute productAttribute) {
		return productAttributeMapper.attributeIds(productAttribute);
	}
	
	public void create(ProductAttribute productAttribute) {
		 productAttributeMapper.create(productAttribute);
	}
	
	public void delete(ProductAttribute productAttribute) {
		 productAttributeMapper.delete(productAttribute);
	}
	
	public List<ProductAttributeWithName> getAllWithNamesByProductId(String productId) {
		return productAttributeMapper.getAllWithNamesByProductId(productId);
	}
	
	public List<ProductAttribute> getAllByProductId(String productId) {
		return productAttributeMapper.getAllByProductId(productId);
	}
	
	public void updateQuantity(ProductAttribute productAttribute) {
		 productAttributeMapper.updateQuantity(productAttribute);
	}
	
	public ProductAttribute getByProductIdAndAttributeId(ProductAttribute productAttribute) {
		return productAttributeMapper.getByProductIdAndAttributeId(productAttribute);
	}
	
	public void deleteByProductId(String productId) {
		 productAttributeMapper.deleteByProductId(productId);
	}
	
	public List<String> readAttributeByProductId(Integer id) {
		return productAttributeMapper.readAttributeByProductId(id);
	}

	public void deleteByProductIdAndAttribute(@Param("productId")String productId, @Param("attribute") String attribute) {
		 productAttributeMapper.deleteByProductIdAndAttribute(productId, attribute);
	}

	public List<ProductAttribute> readAttributesByProductIds(List<String> productIds) {
		return productAttributeMapper.readAttributesByProductIds(productIds);
	}
	
	

	public List<ProductAttribute> readAttributesStartWithPctForProducts(List<String> productIds) {
		return productAttributeMapper.readAttributesStartWithPctForProducts(productIds);
	}
	
	public List<String> readAllAttributesBeginWithPrefixForProduct(@Param("productId") Integer productId, @Param("prefix") String prefix) {
		return productAttributeMapper.readAllAttributesBeginWithPrefixForProduct(productId, prefix);
	}
	
	public List<ProductAttributeWithName> readAttributesDisplayNameForProduct(Integer productId) {
		return productAttributeMapper.readAttributesDisplayNameForProduct(productId);
	}

	public KeyCollection getKeyCollection(Integer productId) {
		return productAttributeMapper.getKeyCollection(productId);
	}


	public List<IdVersion> getLastUpdateByProductIds(List<Integer> productIds) {
		return productAttributeMapper.getLastUpdateByProductIds(productIds);
    }
}
