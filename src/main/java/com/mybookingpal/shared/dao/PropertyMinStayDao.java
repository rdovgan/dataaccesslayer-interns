package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.json.minstay.WeeklyMinstay;
import com.mybookingpal.dal.server.api.PropertyMinStayMapper;
import com.mybookingpal.dal.shared.LocalDateRange;
import com.mybookingpal.dal.shared.MinStay;
import com.mybookingpal.dal.shared.PropertyMinStay;
import com.mybookingpal.dal.shared.RoomPeriodChange;
import com.mybookingpal.dal.shared.action.DatesIdAction;
import com.mybookingpal.dal.shared.minstay.MinstaySearch;
import com.mybookingpal.dal.shared.minstay.MinstayWeek;
import com.mybookingpal.dal.utils.CommonDateUtils;
import com.mybookingpal.dal.utils.GenericList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public class PropertyMinStayDao {

	@Autowired
	PropertyMinStayMapper mapper;

	public void create(MinStay action) {
		mapper.create(action);
	}

	public MinStay read(MinStay action) {
		return mapper.read(action);
	}

	public PropertyMinStay read(Long action) {
		return mapper.read(action);
	}

	public MinStay readbyexample(MinStay action) {
		return mapper.readByExample(action);
	}

	public void update(MinStay action) {
		mapper.update(action);
	}

	public void delete(MinStay action) {
		mapper.delete(action);
	}

	public MinStay exists(MinStay action) {
		return mapper.exists(action);
	}

	public List<WeeklyMinstay> getMinstayByWeeksFeed(DatesIdAction action) {
		return mapper.getMinstayByWeeksFeed(action);
	}

	public List<WeeklyMinstay> getMinstayByWeeks(MinstayWeek action) {
		return mapper.getMinstayByWeeks(action);
	}

	public void deleteversion(MinStay action) {
		mapper.delete(action);
	}

	public List<PropertyMinStay> readByProductIdAndDateRange(DatesIdAction action) {
		return mapper.readByProductIdAndDateRange(action);
	}

	public List<PropertyMinStay> readByProductId(int productId) {
		return mapper.readByProductId(productId);
	}

	public List<PropertyMinStay> readListByProductsId(List<Integer> productId) {
		return mapper.readListByProductsId(productId);
	}

	public List<MinStay> selectbyproductids(MinstaySearch minstaySearch) {
		return mapper.selectbyproductids(minstaySearch);
	}

	public List<PropertyMinStay> readByEntityIdAndStateCreatedAndVersion(PropertyMinStay propertyMinStay) {
		return mapper.readByEntityIdAndStateCreatedAndVersion(propertyMinStay);
	}
	
	public List<PropertyMinStay> readByEntityIdAndStateFinalAndVersion(PropertyMinStay propertyMinStay) {
		return mapper.readByEntityIdAndStateFinalAndVersion(propertyMinStay);
	}
	
	public List<PropertyMinStay> readByEntityIdAndEntityType(PropertyMinStay propertyMinStay){
		return mapper.readByEntityIdAndEntityType(propertyMinStay);
	}
	public List<PropertyMinStay> readByEntityIdAndAllStateAndVersion(PropertyMinStay propertyMinStay){
		return mapper.readByEntityIdAndAllStateAndVersion(propertyMinStay);
	}

	public List<PropertyMinStay> getProductsForPropertyMinStayByVersion(LocalDateTime lastFetch, LocalDate toDate, String productId) {
		PropertyMinStay propertyMinStay = new PropertyMinStay();
		propertyMinStay.setFromDate(lastFetch.toLocalDate());
		propertyMinStay.setToDate(toDate);
		propertyMinStay.setVersion(CommonDateUtils.toDate(lastFetch));
		propertyMinStay.setProductID(productId);
		return mapper.getProductsForPropertyMinStayByVersion(propertyMinStay);
	}
	
	public List<RoomPeriodChange> readPropertyMinStayForPeriodForAllStateBySupplierIdByVersion(String supplierId,Date fromDate,Date toDate,Date version,Integer channelId){
		return mapper.readPropertyMinStayForPeriodForAllStateBySupplierIdByVersion(supplierId,fromDate,toDate,version,channelId);
	}

	public List<PropertyMinStay> readCancelledByProductIdAndDateRange(DatesIdAction action) {
		return mapper.readCancelledByProductIdAndDateRange(action);
	}
	
	public List<PropertyMinStay> getAllActiveByProductIdWithoutDateRange(String productId) {
		return mapper.getAllActiveByProductIdWithoutDateRange(productId);
	}


	public List<PropertyMinStay> readMinStayByIdAndDate(MinStay action) {
		return mapper.readMinStayByIdAndDate(action);
	}

	public List<PropertyMinStay> readMinStayByProductIdAndDate(MinStay action) {
		return mapper.readMinStayByProductIdAndDate(action);
	}

	public void cancelPropertyMinStayById(Long propertyMinStayId, Date version) {
		mapper.cancelPropertyMinStayById(propertyMinStayId, version);
	}

	public void updateStateOnFinalPerProduct(Integer productId) {
		mapper.updateStateOnFinalPerProduct(productId);
	}

	public void insertList(GenericList<PropertyMinStay> list) {
		mapper.insertList(list);
	}

	public Integer readMinStay(MinStay action) {
		return mapper.readMinStay(action);
	}

	public List<PropertyMinStay> readByIds(List<Long> minStayIds) {
		return mapper.readByIds(minStayIds);
	}

	public List<PropertyMinStay> readByExamples(List<PropertyMinStay> list) {
		return mapper.readByExamples(list);
	}

	public List<PropertyMinStay> getAllByProductIdAndSupplierId(PropertyMinStay action) {
		return mapper.getAllByProductIdAndSupplierId(action);
	}

	public void deleteByProductId(String productId) {
		mapper.deleteByProductId(productId);
	}

	public void copyPropertyMinStays(Integer fromProductId, Integer toProductId) {
		mapper.copyPropertyMinStays(fromProductId, toProductId);
	}

	public List<PropertyMinStay> getAllActiveByProductId(PropertyMinStay propertyMinStay) {
		return mapper.getAllActiveByProductId(propertyMinStay);
	}

	public void cancelPropertyMinStayList(GenericList<PropertyMinStay> propertyMinStays, Date version) {
		mapper.cancelPropertyMinStayList(propertyMinStays, version);
	}

	public Integer readMaxMinStay(MinStay action) {
		return mapper.readMaxMinStay(action);
	}

	public void updateStateOnFinalPerPm(String partyId) {
		mapper.updateStateOnFinalPerPm(partyId);
	}

	public List<PropertyMinStay> getByProductId(Integer productId) {
		return mapper.getByProductId(productId);
	}

	public List<PropertyMinStay> readMinStayByProductIdAndDates(Integer productId, List<LocalDateRange> dates) {
		return mapper.readMinStayByProductIdAndDates(productId, dates);
	}
}
