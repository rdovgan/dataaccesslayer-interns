package com.mybookingpal.shared.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.CurrencyrateMapper;
import com.mybookingpal.dal.shared.Currencyrate;

@Repository
public class CurrencyrateDao {
	@Autowired
	CurrencyrateMapper currencyrateMapper;

	public Currencyrate readbyexample(Currencyrate currencyrate) {
		return currencyrateMapper.readbyexample(currencyrate);
	}

	public void create(Currencyrate action) {
		currencyrateMapper.create(action);
	}

	public Currencyrate read(String id) {
		return currencyrateMapper.read(id);
	}

	public void update(Currencyrate action) {
		currencyrateMapper.update(action);
	}

	public void write(Currencyrate action) {
		currencyrateMapper.write(action);
	}

}
