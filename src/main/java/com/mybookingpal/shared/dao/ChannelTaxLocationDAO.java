package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mybookingpal.dal.server.api.ChannelTaxLocationMapper;
import com.mybookingpal.dal.shared.ChannelTaxLocation;

@Repository
public class ChannelTaxLocationDAO {

	@Autowired
	private ChannelTaxLocationMapper mapper;

	public List<ChannelTaxLocation> readByLocationId(ChannelTaxLocation channelTaxLocation) {
		return mapper.readByLocationIDAndChannelID(channelTaxLocation);
	}

	public void updateByLocationId(ChannelTaxLocation channelTaxLocation) {
		mapper.updateByLocationIDAndChannelID(channelTaxLocation);
	}

	public void createAirbnbTaxLocation(ChannelTaxLocation channelTaxLocation) {
		mapper.createAirbnbTaxLocation(channelTaxLocation);
	}

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
	public void createAirbnbTaxLocationList(List<ChannelTaxLocation> channelTaxLocationList) {
		mapper.createAirbnbTaxLocationList(channelTaxLocationList);
	}
	
	public List<ChannelTaxLocation> readByLocationIDAndChannelID(ChannelTaxLocation channelTaxLocation) {
		return mapper.readByLocationIDAndChannelID(channelTaxLocation);
	}
	
	public List<ChannelTaxLocation> readByZipCode9AndChannelID(ChannelTaxLocation channelTaxLocation) {
		return mapper.readByZipCode9AndChannelID(channelTaxLocation);
	}

	public ChannelTaxLocation readByZipCode9(String zipcode, Integer channelId) {
		return mapper.readByZipCode9(zipcode,channelId);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
	public void updateAirbnbTaxLocationByID(ChannelTaxLocation channelTaxLocation) {
		mapper.updateAirbnbTaxLocationByID(channelTaxLocation);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
	public List<ChannelTaxLocation> readAllIdWithZipCode9() {
		return mapper.readAllIdWithZipCode9();
	}
	
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
	public void deleteByID(Integer id) {
		mapper.delete(id);
	}
}
