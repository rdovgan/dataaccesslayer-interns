package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelListingInformationMapper;
import com.mybookingpal.dal.shared.ChannelListingInformation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ChannelListingInformationDao {

    @Autowired
    private ChannelListingInformationMapper channelListingInformationMapper;

    public void insertList(List<ChannelListingInformation>channelListingsInformation) {
        channelListingInformationMapper.insertList(channelListingsInformation);
    }

    public List<ChannelListingInformation> getChannelListingInfoBySupplierAndChannel(Integer supplierId, Integer channelId) {
        return channelListingInformationMapper.getChannelListingInfoBySupplierAndChannel(supplierId, channelId);
    }

    public ChannelListingInformation getChannelListingInfoBySupplierAndChannelProduct(Integer supplierId, Integer channelProductId) {
        return channelListingInformationMapper.getChannelListingInfoBySupplierAndChannelProduct(supplierId, channelProductId);
    }

    public void update(ChannelListingInformation channelListingInformation) {
        channelListingInformationMapper.update(channelListingInformation);
    }

    public void delete(Integer supplierId) {
        channelListingInformationMapper.delete(supplierId);
    }
}
