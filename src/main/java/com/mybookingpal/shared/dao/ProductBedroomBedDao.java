package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ProductBedroomBedMapper;
import com.mybookingpal.dal.shared.ProductBedroomBed;
import com.mybookingpal.dal.shared.ProductBedroomBedWithName;

/**
 * @author Danijel Blagojevic
 *
 */
@Repository
public class ProductBedroomBedDao {

	@Autowired
	ProductBedroomBedMapper productBedroomBedMapper;
	
	public List<ProductBedroomBed> getProductBedroomBedByProductId(Integer productId) {
		return productBedroomBedMapper.getAllByProductId(productId);
	}
	
	public Integer readNumberOfBedsForProduct(String productId){
		return productBedroomBedMapper.readNumberOfBedsForProduct(productId);
	}
	
	public List<ProductBedroomBed> getAllByBedroomId(Integer bedroomId) {
		return productBedroomBedMapper.getAllByBedroomId(bedroomId);
	}
	
	public List<ProductBedroomBed> getAllByProductId(Integer productId) {
		return productBedroomBedMapper.getAllByProductId(productId);
	}
	
	public List<ProductBedroomBedWithName> getAllByProductIdGroupByType(Integer productId) {
		return productBedroomBedMapper.getAllByProductIdGroupByType(productId);
	}
	
	public List<ProductBedroomBedWithName> getAllByProductIdGroupByBedroomIDAndType(Integer productId) {
		return productBedroomBedMapper.getAllByProductIdGroupByBedroomIDAndType(productId);
	}
	
	public List<ProductBedroomBedWithName> readListOfBedroomBeds(Integer roomId) {
		return productBedroomBedMapper.readListOfBedroomBeds(roomId);
	}

	public List<ProductBedroomBed> readListByProductIdList(List<Integer> productIdList) {
			return productBedroomBedMapper.readByProductIdList(productIdList);
	}

}
