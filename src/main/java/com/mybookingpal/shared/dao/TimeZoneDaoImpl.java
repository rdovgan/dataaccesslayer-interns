package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.TimeZoneMapper;
import com.mybookingpal.dal.shared.NameIdAction;
import com.mybookingpal.dal.shared.TimeZone;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.mybookingpal.dal.server.util.ExceptionUtils.throwExceptionIfNull;

@Repository
public class TimeZoneDaoImpl implements TimeZoneDao {
	private static final Logger LOG = Logger.getLogger(TimeZoneDaoImpl.class.getName());

	@Autowired
	private TimeZoneMapper timeZoneMapper;

	@Override
	public List<TimeZone> readAll(NameIdAction limit) {
		validateLimit(limit);
		final List<TimeZone> timeZones = timeZoneMapper.readAll(limit);
		getLOG().debug("readAll: read " + timeZones.size() + " rows using offset " + limit.getOffsetrows() + " rows and limit " + limit.getNumrows());
		return timeZones;
	}

	@Override
	public List<TimeZone> readByExample(TimeZone example) {
		validateExample(example);
		final List<TimeZone> timeZones = timeZoneMapper.readByExample(example);
		getLOG().debug("readByExample: read " + timeZones.size() + " rows using example " + example);
		return timeZones;
	}

	@Override
	public TimeZone read(Integer id) {
		final TimeZone timeZone = timeZoneMapper.read(id);
		logResultOfReadingById(timeZone);
		return timeZone;
	}

	@Override
	public void create(TimeZone timeZone) {
		validateTimeZoneForNull(timeZone);
		timeZoneMapper.create(timeZone);
		getLOG().debug("create: time zone [" + timeZone + "] created");
	}

	private void validateTimeZoneForNull(TimeZone timeZone) {
		throwExceptionIfNull(timeZone, "timeZone");
	}

	private static Logger getLOG() {
		return LOG;
	}

	private void logResultOfReadingById(TimeZone timeZone) {
		//getLOG().debug("read: time zone found by id " + timeZone.getId() + ": " + timeZone);
	}

	private void validateLimit(NameIdAction limit) {
		throwExceptionIfNull(limit, "limit");
	}

	private void validateExample(TimeZone example) {
		throwExceptionIfNull(example, "example");
	}
}
