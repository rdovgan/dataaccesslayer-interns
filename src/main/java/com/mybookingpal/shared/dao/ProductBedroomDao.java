package com.mybookingpal.shared.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ProductBedroomMapper;
import com.mybookingpal.dal.shared.ProductBedroom;

/**
 * @author Danijel Blagojevic
 *
 */
@Repository
public class ProductBedroomDao {

	@Autowired
	ProductBedroomMapper productBedroomMapper;
	
	public List<ProductBedroom> listOfBedroomsByProductId(Integer productId) {
		return productBedroomMapper.readListOfBedrooms(productId);
	}

	public List<ProductBedroom> readBedroomProductIdList(List<Integer> productIdList) {
		return productBedroomMapper.readByProductIdList(productIdList);
	}
	
	public Integer getRoomsNumberByProductIdAndType (ProductBedroom action){
		return productBedroomMapper.getRoomsNumberByProductIdAndType(action);
	}
}
