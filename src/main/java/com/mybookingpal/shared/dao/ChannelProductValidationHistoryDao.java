package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelProductValidationHistoryMapper;
import com.mybookingpal.dal.shared.ChannelProductValidationHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ChannelProductValidationHistoryDao {

    @Autowired
    ChannelProductValidationHistoryMapper channelProductValidationHistoryMapper;

    public void create (ChannelProductValidationHistory channelProductValidationHistory) {
        channelProductValidationHistoryMapper.create(channelProductValidationHistory);
    }

    public ChannelProductValidationHistory getChannelProductValidationHistoryByProductIdAndChannelId (Integer productId, Integer channelId) {
        return channelProductValidationHistoryMapper.getChannelProductValidationHistoryByProductIdAndChannelId(productId,channelId);
    }

    public List<ChannelProductValidationHistory> getChannelProductValidationHistoryByProductIdsAndChannelId (List<Integer> productIds, Integer channelId) {
        return channelProductValidationHistoryMapper.getChannelProductValidationHistoryByProductIdsAndChannelId(productIds,channelId);
    }

}
