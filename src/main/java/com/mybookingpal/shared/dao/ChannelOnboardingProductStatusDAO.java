package com.mybookingpal.shared.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ChannelOnboardingProductStatusMapper;
import com.mybookingpal.dal.shared.ChannelOnboardingProductStatus;

@Repository
public class ChannelOnboardingProductStatusDAO {

	@Autowired
	private ChannelOnboardingProductStatusMapper mapper;

	public void createChannelOnboardingProductStatus(ChannelOnboardingProductStatus channelOnboardingProductStatus) {

		if (channelOnboardingProductStatus.getError().length() > 700) {
			channelOnboardingProductStatus.setError(channelOnboardingProductStatus.getError().substring(0, 699));
		}

		mapper.createChannelOnboardingProductStatus(channelOnboardingProductStatus);
	}

	public void finalizeActiveProductByProductIdChannelId(ChannelOnboardingProductStatus channelOnboardingProductStatus) {
		mapper.finalizeActiveProductByProductIdChannelId(channelOnboardingProductStatus);
	}

	public ChannelOnboardingProductStatus getCreatedByProductIdChannelId(ChannelOnboardingProductStatus channelOnboardingProductStatus) {
		return mapper.getCreatedByProductIdChannelId(channelOnboardingProductStatus);
	}

}
