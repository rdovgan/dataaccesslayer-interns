package com.mybookingpal.shared.dao.beststayz;


import com.mybookingpal.dal.shared.messaging.MailThreadAndMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.mybookingpal.dal.server.api.MailThreadMapper;
import com.mybookingpal.dal.shared.messaging.MailThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MailThreadDao {

	@Autowired
	MailThreadMapper mapper;

	public void create(MailThread action) {
		mapper.create(action);
	}

	public MailThread readByThreadId(String threadId) {
		return mapper.readByThreadId(threadId);
	}

	public MailThread readByReservationId(String reservationId) {
		return mapper.readByReservationId(reservationId);
	}

	public MailThread readById(Integer id) {
		return mapper.readById(id);
	}

	public List<MailThreadAndMessage> getThreadAndMessageByPartyId(String partyId, Integer pagination, Integer paginationPage, String type) {
		return mapper.getThreadAndMessageByPartyId(partyId, pagination, paginationPage, type);
	}

	public List<Integer> readAllThreadIdByPartyId(String partyId) {
		return mapper.readAllThreadIdByPartyId(partyId);
	}

	public void update(MailThread mailThread){
		mapper.update(mailThread);
	}

	public MailThread readByThreadIdAndPmId(Integer id, Integer partyId) {
		return mapper.readByThreadIdAndPmId(id, partyId);
	}

	public MailThread readByExample(MailThread mailThread) {
		return mapper.readByExample(mailThread);
	}

	public void updateVersion(MailThread mailThread) {
		mapper.updateVersion(mailThread);
	}
}
