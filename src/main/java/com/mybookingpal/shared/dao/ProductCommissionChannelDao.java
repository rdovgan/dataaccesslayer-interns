package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.shared.IdVersion;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ProductCommissionChannelMapper;
import com.mybookingpal.dal.shared.ProductCommissionChannel;

import java.util.Date;

@Repository
public class ProductCommissionChannelDao {

	@Autowired
	ProductCommissionChannelMapper mapper;
	
	public ProductCommissionChannel readActiveByProductAndChannelPartner(ProductCommissionChannel productCommissionChannel) {
		return mapper.readActiveByProductAndChannelPartner(productCommissionChannel);
	}

    public List<IdVersion> getMaxVersionForHACommissionPerProduct(Integer channelId, Integer channelPartnerId, List<Integer> productIds) {
		return mapper.getMaxVersionForHACommissionPerProduct(channelId, channelPartnerId, productIds);
    }
}
