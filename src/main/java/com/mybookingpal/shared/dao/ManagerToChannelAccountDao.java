package com.mybookingpal.shared.dao;

import java.util.List;

import com.mybookingpal.dal.shared.ProductAction;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mybookingpal.dal.server.api.ManagerToChannelAccountMapper;
import com.mybookingpal.dal.shared.ManagerToChannelAccount;

@Repository
public class ManagerToChannelAccountDao {

	public final Logger LOG = Logger.getLogger(ManagerToChannelAccountDao.class);

	@Autowired
	private ManagerToChannelAccountMapper managerToChannelAccountMapper;

	//@Transactional
	public List<ManagerToChannelAccount> readByPropertyManagerAndChannelPartner(ManagerToChannelAccount managerToChannelAccount) {
		LOG.info("Dao ManagerToChannelAccountDao is called");
		return managerToChannelAccountMapper.readByPropertyManagerAndChannelPartner(managerToChannelAccount);
	}

	public List<ManagerToChannelAccount> findActiveAccountsByChannelPartner(ManagerToChannelAccount managerToChannelAccount) {
		return managerToChannelAccountMapper.findActiveAccountsByChannelPartner(managerToChannelAccount);
	}

	public List<ManagerToChannelAccount> justPMCreatedOrSuspendedNotChangedInAWhile(ProductAction productAction) {
		return managerToChannelAccountMapper.justPMCreatedOrSuspendedNotChangedInAWhile(productAction);
	}

	public List<ManagerToChannelAccount> findActiveAccountsWithMltRepProperties(List<Integer>listOfAllSupplierIds,Integer channelId) {
		return managerToChannelAccountMapper.findActiveAccountsWithMltRepProperties(listOfAllSupplierIds, channelId);
	}

	public void updateState(ManagerToChannelAccount managerToChannelAccount) {
		managerToChannelAccountMapper.updateState(managerToChannelAccount);
	}

	public void create(ManagerToChannelAccount managerToChannelAccount) {
		managerToChannelAccountMapper.create(managerToChannelAccount);
	}

	public List<ManagerToChannelAccount> findByPropertyManagerAndChannelPartnerOrderByVersion(ManagerToChannelAccount managerToChannelAccount) {
		return managerToChannelAccountMapper.findByPropertyManagerAndChannelPartnerOrderByVersion(managerToChannelAccount);
	}

	public List<ManagerToChannelAccount> readByPropertyManagerAndChannelPartnerWithoutState(ManagerToChannelAccount managerToChannelAccount) {
		return managerToChannelAccountMapper.readByPropertyManagerAndChannelPartnerWithoutState(managerToChannelAccount);
	}

	public List<String> getAllActiveAPMIdsForChannel(Integer channelID) {
		return managerToChannelAccountMapper.getAllActiveAPMIdsForChannel(channelID);
	}

	public void update(ManagerToChannelAccount managerToChannelAccount) {
		managerToChannelAccountMapper.update(managerToChannelAccount);
	}

	public List<ManagerToChannelAccount> findByActivePropertyManager(Integer supplierId) {
		return managerToChannelAccountMapper.findByActivePropertyManager(supplierId);
	}

	public List<ManagerToChannelAccount> getlAllActiveVirtualAccountsForSupplier(Integer supplierId) {
		return managerToChannelAccountMapper.getlAllActiveVirtualAccountsForSupplier(supplierId);
		
	}

	public String getPaymentCredentialsBySupplierIdAndChannelId(ManagerToChannelAccount managerToChannelAccount){
		return managerToChannelAccountMapper.getPaymentCredentialsBySupplierIdAndChannelId(managerToChannelAccount);
	}

	public List<ManagerToChannelAccount> findSupplierForExpedia(String supplierId){
		return managerToChannelAccountMapper.findSupplierForExpedia(supplierId);
	}
}
