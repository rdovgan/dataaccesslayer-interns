package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.TextMapper;
import com.mybookingpal.dal.shared.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TextDaoImpl implements TextDao {

	@Autowired
	private TextMapper textMapper;

	public Text notes(String id, String language) {
		return textMapper.readbyexample(new Text(id, language));
	}

	public Text readByID(String id) {
		return textMapper.readByID(id);
	}

	public List<Text> readallbyid(String id) {
		return textMapper.readallbyid(id);
	}
}
