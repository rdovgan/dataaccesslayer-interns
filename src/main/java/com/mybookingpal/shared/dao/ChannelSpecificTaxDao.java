package com.mybookingpal.shared.dao;

import com.mybookingpal.dal.server.api.ChannelSpecificTaxMapper;
import com.mybookingpal.dal.shared.ChannelSpecificTax;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ChannelSpecificTaxDao {

    @Autowired
    private ChannelSpecificTaxMapper channelSpecificTaxMapper;

    public List<ChannelSpecificTax> getByProductId(Integer productId) {
        return channelSpecificTaxMapper.getByProductId(productId);
    }

    public List<ChannelSpecificTax> getByProductIdAndVersion(Integer productId, Date version) {
        return channelSpecificTaxMapper.getByProductIdAndVersion(productId, version);
    }

    public void insertNewTax(ChannelSpecificTax channelSpecificTax){
        channelSpecificTaxMapper.insertNewTax(channelSpecificTax);
    }


}
