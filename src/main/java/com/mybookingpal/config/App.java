package com.mybookingpal.config;

import com.mybookingpal.dal.shared.NameIdAction;
import com.mybookingpal.dataaccesslayer.dao.LocationDao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class App {
	public static void main(String[] args) {

		String[] springConfig = { "com/mybookingpal/dal/config/dao-beans.xml"};

		ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);

		LocationDao locationDao = (LocationDao) context.getBean("locationDao");
		NameIdAction nameIdAction = (NameIdAction) context.getBean("NameIdAction");
		try {

			System.out.println("locationDao "+locationDao);
			locationDao.activeLocations();

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Done");

	}
}
