package com.mybookingpal.config;

import com.mybookingpal.shared.dao.AvailabilityCalendarModelDAO;
import com.mybookingpal.shared.dao.PriceDao;
import com.mybookingpal.shared.dao.PropertyMinStayDao;
import com.mybookingpal.shared.dao.ReservationDao;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringContextBridge implements SpringContextBridgedServices, ApplicationContextAware {

	private static ApplicationContext applicationContext;

	@Autowired
	private AvailabilityCalendarModelDAO availabilityCalendarModelDAO;

	@Autowired
	private PriceDao priceDao;

	@Autowired
	private PropertyMinStayDao propertyMinStayDao;

	@Autowired
	private ReservationDao reservationDao;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	public static SpringContextBridgedServices services() {
		return applicationContext.getBean(SpringContextBridgedServices.class);
	}

	@Override
	public AvailabilityCalendarModelDAO getAvailabilityCalendarDao() {
		return availabilityCalendarModelDAO;
	}

	@Override
	public PriceDao getPriceDao() {
		return priceDao;
	}

	@Override
	public PropertyMinStayDao getPropertyMinStayDao() {
		return propertyMinStayDao;
	}

	@Override
	public ReservationDao getReservationDao() {
		return reservationDao;
	}
}
