package com.mybookingpal.config;

import com.mybookingpal.shared.dao.AvailabilityCalendarModelDAO;
import com.mybookingpal.shared.dao.PriceDao;
import com.mybookingpal.shared.dao.PropertyMinStayDao;
import com.mybookingpal.shared.dao.ReservationDao;

public interface SpringContextBridgedServices {

	AvailabilityCalendarModelDAO getAvailabilityCalendarDao();

	PriceDao getPriceDao();

	PropertyMinStayDao getPropertyMinStayDao();

	ReservationDao getReservationDao();


}
