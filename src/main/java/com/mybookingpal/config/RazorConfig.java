package com.mybookingpal.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mybookingpal.dal.shared.ChannelPartner;
import com.mybookingpal.dal.shared.ManagerToChannelAccount;
import com.mybookingpal.dal.shared.NameIdAction;
import com.mybookingpal.dal.shared.Setting;
import com.mybookingpal.shared.dao.ChannelPartnerDao;
import com.mybookingpal.shared.dao.ManagerToChannelAccountDao;
import com.mybookingpal.shared.dao.ProductDao;
import com.mybookingpal.shared.dao.SettingDao;

@Component
public class RazorConfig {
	private static CompositeConfiguration razorConfig = new CompositeConfiguration();
	private static final String ENVIRONMENT = "bp.environment";
	private static final String DEMO_ENVIRONMENT = "bp.demo.environment";
	public static final String PROD_ENVIRONMENT = "production";
	public static final String DEMO_ENVIRONMENT_VALUE = "demo";
	public static final String TEST_DEMO_ENVIRONMENT = "test_demo";
	public static final String TEST_DEMO_ENVIRONMENT_VALUE = "bp.demo.environment";
	private static final String QA_ENVIRONMENT = "bp.qa.environment";
	public static final String QA_ENVIRONMENT_VALUE = "qa";
	private static final String DB_ENVIRONMENT = "bp.db.environment";
	private static final String MARRIOTT_URL = "bp.marriott.url";
	private static final String FEE_TAX_URL = "bp.feetax.url";
	private static final String BOOKING_ID = "bp.booking.id";
	private static final String INNTOPIA_CHANNEL_ID= "bp.inntopia.channel.id";
	private static final String INNTOPIA_STAGING_USERNAME = "bp.inntopia.staging.username";
	private static final String INNTOPIA_STAGING_PASSWORD = "bp.inntopia.staging.password";
	private static final String INNTOPIA_STAGING_SENDER_ID = "bp.inntopia.staging.senderid";
	private static final String INNTOPIA_STAGING_URL = "bp.inntopia.staging.url";
	private static final String INNTOPIA_USERNAME = "bp.inntopia.username";
	private static final String INNTOPIA_PASSWORD = "bp.inntopia.password";
	private static final String INNTOPIA_SENDER_ID = "bp.inntopia.senderid";
	private static final String INNTOPIA_URL = "bp.inntopia.url";


	private static final String BOOKING_POLLER_TIME = "bp.booking.poller.time";
	private static final Integer BOOKING_ARCHIVE_ID = 1133;
	private static final String S3_IMAGE_URL = "bp.s3.image.url";
	public static final String BOOKING_AUTHORIZATION_EMAIL = "bp.booking.authorization.email";
	public static final String PARTY_BELVILLA_ID = "bp.belvilla";
	private static final String PARTY_NEXTPAX_VACASOL_ID = "bp.np.vacasol";
	private static final String AIRBNB_CHANNEL_ID = "bp.airbnb.channel_id";
	private static final String AIRBNB_ARCHIVE_CHANNEL_ID = "bp.airbnb.archive_channel_id";
	private static final String BOOKING_RESERVATION_RECEPIENTS = "booking.reservation.recepients";
	private static final String SUPPORT_RECEPIENTS = "Support";
	private static final String EXPEDIA_ONBOARDING_PROPERTY_RECEPIENTS = "bp.expedia.onboarding.property.report.recepients";
	private static final String EXPEDIA_ONBOARDING_BULK_FEE_RECEPIENTS = "bp.expedia.onboarding.bulk.fee.recepients";
	private static final String TRIPADVISOR_ID = "bp.tripadvisor.id";
	private static final String MARRIOTT_ID = "bp.marriott.id";
	private static final String HVMI_ID = "bp.hvmi.id";
	public static final String HVMI_AUTH_TOKEN = "bp.hvmi.auth.token";
	private static final String MARRIOTT_ARCHIVAL_ID = "bp.marriott.archival.id";
	private static final String CHANNELAPI_ID = "bp.channelapi.id";
	private static final String TRIPADVISOR_POS_CODE = "tripadvisor.pos.code";
	
	private static final String CHANNELAPI_POS_CODE = "channelapi.pos.code";
	private static final String MARRIOTT_POS_CODE = "marriott.pos.code";
	private static final String MARRIOTT_AUTHORIZATION_CODE = "marriott.authorization.code";
	
	private static final String MARRIOTT_CHANNELPARTNER_ID = "bp.marriott.channelpartner.id";
	private static final String CHANNELAPI_CHANNELPARTNER_ID = "bp.channelapi.channelpartner.id";

	//private static final String EXPEDIA_CHANNEL_ID = "bp.expedia.id";
	//private static final String EXPEDIA_CHANNEL_PM_MOR_ID = "bp.expedia.pm.mor.id";
	private static final String PARTY_AIRBNB_ID = "bp.airbnb";
	private static final String PARTY_AIRBNB_STAGING_ID = "bp.airbnb.staging";
	private static final String POS_CODE = "pos.code";
	private static final String EXPEDIA_CHANNEL_PM_MOR_ID = "bp.expedia.pm.mor.id";
	private static final String EXPEDIA_CHANNEL_ID = "bp.expedia.id";
	private static final String FLATS9_CHANNEL_ID = "bp.9flats.channel_id";
	private static final String GETAROOM_CHANNEL_ID = "bp.getaroom.channel_id";
	private static final String GETAROOM_UAT_STAGING_CHANNEL_ID = "bp.getaroom.uat.channel_id";
	private static final String PARTY_GETAROOM_ID = "bp.getaroom.party_id";

	private static final String AGODA_CHANNEL_ID = "bp.agoda.channel_id";
	private static final String AGODA_CHANNEL_PARTY_ID = "bp.agoda.channel_party.id";
	public static final String AGODA_BULK_FILES_RECIPIENT_EMAILS = "bp.agoda.bulk_file_recipient.emails";

	private static final String EXPEDIA_COLLECT_CHANNELPARTNER_ID = "bp.expedia.collect.channelpartner.id";
	private static final String EXPEDIA_HOTEL_COLLECT_CHANNELPARTNER_ID = "bp.expedia.hotelcollect.channelpartner.id";
	private static final String BOOKING_CHANNELPARTNER_ID = "bp.bookingcom.channelpartner.id";
	private static final String TRIPADVISOR_CHANNELPARTNER_ID = "bp.tripadvisor.channelpartner.id";
	public static final String GETAROOM_BOOKING_POS = "bp.getaroom.pos";
	private static final String BOOKING_PARTY_ID = "bp.bookingcom.partyid";
	private static final String EXPEDIA_STAGING_CHANNEL_ID = "bp.expedia.staging.id";
	private static final String EXPEDIA_STAGING_CHANNEL_PM_MOR_ID = "bp.expedia.staging.pm.mor.id";

	public static final String EXPEDIA_USERNAME = "bp.expedia.username";
	public static final String EXPEDIA_PWD = "bp.expedia.pwd";
	public static final String EXPEDIA_STAGING_USERNAME = "bp.expedia.staging.username";
	public static final String EXPEDIA_STAGING_PWD = "bp.expedia.staging.pwd";

	private static final String FEWO_CHANNEL_ID = "bp.fewo.channel_id";
	private static final String FEWO_PARTY_ID = "bp.fewo.party_id";
	private static final String FEWO_ORGANIZATION_ID = "bp.fewo.organization_id";
	private static final String FEWO_PROD_ORGANIZATION_ID = "bp.fewo.prod.organization_id";
	public static final String AIRBNB_PAYOUT_EMAIL_RECEPIENTS = "bp.airbnb.payout.recepients";
	public static final String AIRBNB_EXCEPTION_PAYOUT_EMAIL_RECEPIENTS = "bp.airbnb.exception.payout.recepients";
	public static final String PMLIST_WITHOUT_AMENTIES_ESSENTIALS = "bp.pmlist.amenities.essentials.off";

	public static final String PARTY_ADRIATIC_ID = "bp.adriatic";

	private static final String EXPEDIA_LOS_PRICING_FLAG = "bp.expedia.los.pricing";
	private static final String EXPEDIA_LOS_PRICING_PMIDS = "bp.expedia.los.pricing.pmids";
	private static final String EXPEDIA_LOS_PRICING_PROPERTYIDS = "bp.expedia.los.pricing.propertyids";

	public static final String EXPEDIA_AVG_LOS_PRICING_PMIDS = "bp.expedia.avg.los.pricing.pmids";
	public static final String EXPEDIA_AVG_LOS_PRICING_LARGE_PMIDS = "bp.expedia.avg.los.pricing.large.pmids";
	private static final String CHANNEL_SUPPORT_RECEPIENTS = "bp.channel.support.recepeints";
	private static final String RAZOR_CONFIG_FILE = "razor-config.properties";

	public static final String GOOGLE_HOTEL_ADS_URL = "bp.google.hotel.ads.url";
	public static final String GOOGLE_HOTEL_ADS_URL_OPMA = "bp.google.hotel.ads.url.opma";
	public static final String GOOGLE_POS_CODE = "bp.google.pos.code";
	private static final String GOOGLE_HOTEL_ADS_CHANNEL_ID = "bp.google.hotel.ads.channel_id";

	private static final String GOOGLE_HOTEL_ADS_SUMMARY_EMAIL_RECIPIENTS = "bp.google.hotel.ads.summary_email_recipients";

	private static final String BP_MULTIUNIT_KEYLEVEL_PMIDS = "bp.multiunit.keylevel.pmids";
	private static final String BP_MULTIUNIT_REPLEVEL_PMIDS = "bp.multiunit.replevel.pmids";
	public static final String EXPEDIA_SPLITJOBS_BATCH1_PROPERTIES = "bp.expedia.splitjobs.batch1.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH2_PROPERTIES = "bp.expedia.splitjobs.batch2.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH3_PROPERTIES = "bp.expedia.splitjobs.batch3.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH4_PROPERTIES = "bp.expedia.splitjobs.batch4.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH5_PROPERTIES = "bp.expedia.splitjobs.batch5.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH6_PROPERTIES = "bp.expedia.splitjobs.batch6.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH7_PROPERTIES = "bp.expedia.splitjobs.batch7.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH8_PROPERTIES = "bp.expedia.splitjobs.batch8.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH9_PROPERTIES = "bp.expedia.splitjobs.batch9.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH10_PROPERTIES = "bp.expedia.splitjobs.batch10.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH11_PROPERTIES = "bp.expedia.splitjobs.batch11.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH12_PROPERTIES = "bp.expedia.splitjobs.batch12.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH13_PROPERTIES = "bp.expedia.splitjobs.batch13.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH14_PROPERTIES = "bp.expedia.splitjobs.batch14.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH15_PROPERTIES = "bp.expedia.splitjobs.batch15.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH16_PROPERTIES = "bp.expedia.splitjobs.batch16.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH17_PROPERTIES = "bp.expedia.splitjobs.batch17.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH18_PROPERTIES = "bp.expedia.splitjobs.batch18.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH19_PROPERTIES = "bp.expedia.splitjobs.batch19.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH20_PROPERTIES = "bp.expedia.splitjobs.batch20.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH21_PROPERTIES = "bp.expedia.splitjobs.batch21.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH22_PROPERTIES = "bp.expedia.splitjobs.batch22.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH23_PROPERTIES = "bp.expedia.splitjobs.batch23.properties";
	public static final String EXPEDIA_SPLITJOBS_BATCH24_PROPERTIES = "bp.expedia.splitjobs.batch24.properties";

	private static final String ADRIATIC_VPM_IDS = "bp.airbnb.adriatic.vpms";
	private static final String VACASA_VPM_IDS = "bp.airbnb.vacasa.vpms";

	public static final String VACASA_US_ACCOUNT = "bp.vacasa.pm.us.account";
	public static final String VACASA_INTERNATIONAL_ACCOUNT = "bp.vacasa.pm.international.account";

	public static final String EMAIL_LIST = "bp.airbnb.email.recepients.report.missing.reservations";
	public static final String MANUALLY_PROCESSED = "bp.airbnb.email.recepients.manually.processed";

	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH1_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch1.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH2_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch2.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH3_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch3.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH4_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch4.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH5_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch5.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH6_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch6.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH7_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch7.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH8_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch8.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH9_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch9.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH10_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch10.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH11_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch11.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH12_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch12.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH13_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch13.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH14_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch14.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH15_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch15.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_BATCH16_PROPERTIES = "bp.expedia.splitjobs.vacasa.batch16.properties";

	public static final String EXPEDIA_SPLITJOBS_VACASA_JOB1_PROPERTIES = "bp.expedia.splitjobs.vacasa.job1.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_JOB2_PROPERTIES = "bp.expedia.splitjobs.vacasa.job2.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_JOB3_PROPERTIES = "bp.expedia.splitjobs.vacasa.job3.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_JOB4_PROPERTIES = "bp.expedia.splitjobs.vacasa.job4.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_JOB5_PROPERTIES = "bp.expedia.splitjobs.vacasa.job5.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_JOB6_PROPERTIES = "bp.expedia.splitjobs.vacasa.job6.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_JOB7_PROPERTIES = "bp.expedia.splitjobs.vacasa.job7.properties";
	public static final String EXPEDIA_SPLITJOBS_VACASA_JOB8_PROPERTIES = "bp.expedia.splitjobs.vacasa.job8.properties";

	public static final String EXPEDIA_SPLITJOBS_HOMEREZ_JOB1_PROPERTIES = "bp.expedia.splitjobs.homerez.job1.properties";
	public static final String EXPEDIA_SPLITJOBS_HOMEREZ_JOB2_PROPERTIES = "bp.expedia.splitjobs.homerez.job2.properties";

	public static final String EXPEDIA_IMAGES_REPORT_EMAIL_RECEPIENTS = "bp.expedia.images.report.recepients";
	private static final String CHANNEL_FAILED_RESERVATION_NOTIFICATION_RECEPIENTS = "bp.channel.failed.reservation.recepients";
	private static final String GETAROOM_RECEPIENTS = "bp.getaroom.recepients";

	// kafka notification property/pm ids per event
	public static final String KAFKA_RATE_NOTIFICATION_PM = "bp.exclude.kafka.rate.pm.notification.ids";
	public static final String KAFKA_RATE_NOTIFICATION_PROPERTIES = "bp.exclude.kafka.rate.property.notification.ids";

	public static final String KAFKA_RESTRICTION_NOTIFICATION_PM = "bp.exclude.kafka.restriction.pm.notification.ids";
	public static final String KAFKA_RESTRICTION_NOTIFICATION_PROPERTIES = "bp.exclude.kafka.restriction.property.notification.ids";

	public static final String KAFKA_CANCELLATION_NOTIFICATION_PM = "bp.exclude.kafka.cancellation.pm.notification.ids";
	public static final String KAFKA_CANCELLATION_NOTIFICATION_PROPERTIES = "bp.exclude.kafka.cancellation.property.notification.ids";

	public static final String KAFKA_TEST_PROPERTY = "bp.kafka.test.property.ids";
	public static final String KAFKA_TEST_PM = "bp.kafka.test.pm.ids";

	public static final String KAFKA_NOTIFICATION_RATE_FULLLOAD_PM = "bp.exclude.kafka.rate.fullload.pm.notification.ids";
	public static final String KAFKA_NOTIFICATION_IMAGE_PM = "bp.kafka.image.pm.notification.ids";
	public static final String KAFKA_NOTIFICATION_IMAGE_PROPERTIES = "bp.kafka.image.property.notification.ids";
	private static final String CANECELATION_PERIOD_60_DAYS = "airbnb.pms.60dayscancelation";

	public static final String KAFKA_NOTIFICATION_RATE_SEASONALCOMMISSION_PM = "bp.exclude.kafka.rate.seasonalcommission.pm.notification.ids";
	public static final String MANUAL_SEASONAL_COMMISSION_CHANGE_RECEPIENTS = "bp.email.manual.seasonal.commission.recepients";
	public static final String CANCELLATION_POLICY_REPORT_RECEPIENTS = "bp.expedia.cancellationpolicy.report.recepients";

	public static final String BDC_WEEKLY_REPORT_LIST = "bp.booking.weeklyreport.emails";
	private static final String DELTA_TEST_PM_IDS = "bp.airbnb.delta_test_pm_ids";
	private static final String IDS_FOR_SUPPLIERS_REPORT = "bp.booking.status.report.ids";
	private static final String EMAILS_FOR_SUPPLIERS_REPORT = "bp.booking.status.report.emails";
	public static final String EXPEDIA_LOS_LARGE_PMID_SPLIT_SIZE = "bp.split.number.expedia.vacasa.los";
	public static final String BOOKING_MULTIUNIT_LARGE_PMID_FOR_SPLITJOBS = "bp.booking.large.pmids.for.splitjobs";
	public static final String BOOKING_MULTIUNIT_LARGE_PMID_SPLIT_SIZE = "bp.split.number.bookingcom.vacasa.multiunit";

	public static final String BOOKING_LOS_PROPERTY_IDS = "bp.booking.los.property.ids";

	public static final String BOOKINGCOM_USERNAME = "bp.bookingcom.username";
	public static final String BOOKINGCOM_PWD = "bp.bookingcom.pwd";

	public static final String KAFKA_PROD_TEST_PROPERTY_IDS = "bp.kafka.prod.test.property.ids";

	private static final String CTRIP_PARTNER_ID = "bp.ctrip.partner.id";//"72547";
	private static final String CTRIP_PARTNER_PASSWORD = "bp.ctrip.partner.password";//"Ctrip!23";
	private static final String CTRIP_CODE_CONTEXT = "bp.ctrip.code.context";//"589";
	public static final String CTRIP_MBP_RESERVATION_ENDPOINT = "bp.ctrip.booking.host";

	public static final String EMAIL_LIST_FOR_FEE_LIMIT = "bp.airbnb.email.recepients.report.fee.limit";
	public static final String ONBOARDING_EMAIL = "bp.airbnb.email.recepients.onboarding";
	public static final String EMAIL_SUPPORT = "bp.airbnb.email.recepients.support";
	public static final String DEFAULT_BOOKING_POLLER_TIME = "45";
	public static final String BP_HOMEAWAY_GUARANTEED_PRICING_PMS_KEY = "bp.homeaway.guaranteed.pricing.pms";
	public static final Integer HOMEAWAY_CHANNELID = 525;
	public static final String BP_HOMEAWAY_PILOT_PMS_42 = "bp.homeaway42.pilot.pms";
	public static final String AIRBNB_EXCLUDE_TAX_ADJUSTMENT = "airbnb.pms.ignore.tax.adjustment";
	public static final String EXPEDIA_UPDATE_FEE_BULK_DOC_USER_EMAIL ="expedia.update.fee.bulk.doc.user.email";


	private static final String CHANNEL_API_CHANNEL_ID = "bp.channelapi.id";
	private static final String CHANNEL_API_STAGING_CHANNEL_ID = "bp.channelapi.staging.id";
	private static final String PARTY_CHANNEL_API_ID = "bp.channelapi.channelpartner.id";
	
	private static final String EXPEDIA_ARCHIVE_CHANNEL_ID = "bp.expedia.archival.id";

	private static final String RHOF_SPLIT_SIZE_BDC = "bp.split.number.RHOF.bookingcom";
	private static final String RHOF_SPLIT_SIZE_EXPEDIA = "bp.split.number.expedia.RHOF";

	private static final String BOOKING_LARGE_PMIDS_GRP1 = "bp.booking.large.pmids.group1";
	private static final String BOOKING_LARGE_PMIDS_GRP2 = "bp.booking.large.pmids.group2";
	private static final String BOOKING_LARGE_PMIDS_GRP3 = "bp.booking.large.pmids.group3";
	private static final String BOOKING_TEST_PMIDS = "bp.booking.test.pmids";
	private static final String BOOKING_LARGE_PMID_FOR_SPLITJOBS = "bp.booking.large.pmids.for.splitjobs";
	private static final String BOOKING_RHOF_SUPPLIER_SPILT= "bp.split.RHOF.bookingcom";
	private static final String BOOKING_RHOF_SPLIT_NUMBER="bp.split.number.RHOF.bookingcom";
	private static final String FAILED_RESERVATION_NOTIFICATION_RECEPIENTS = "bp.failed.reservation.recepients";
	public static final String SUPPLIER_API_ENDPOINT = "bp.supplierApi.url";

	public static final String EMAIL_PM_DISONNECTED_REPORT_LIST = "bp.airbnb.email.recepients.report.disconnected.pm";

	public static final String EMAIL_KAFKA_BROKER_ALERT_LIST = "bp.kafka.brokers.email.recepients";

	@Autowired
	SettingDao settingDao;
	@Autowired
	ProductDao productDao;
	@Autowired
	ChannelPartnerDao channelPartnerDao;
	String environmentId;
	String tripadvisorId;
	String marriottId;
	String hvmiId;
	String hvmiToken;
	String marriottArchivalId;
	String channelApiId;
	String belvillaId;
	String nextpaxVacasolId;
	String bookingcomId;
	String inntopiaChannelId;
	String tripadvisorPosCode;
	String channelApiPosCode;
	String marriottPosCode;
	String marriottAuthorization;
	String channelAuthorization;
	String bookingPollerTime;
	String authorizationBookingRecepients;
	String airBnbChannelId;
	String airBnbStagingChannelId;
	String airBnbArchiveChannelId;
	String bookingcomRecepients;
	String expediaOnboardingPropertyRecepients;
	String expediaOnboardingBulkFeeRecepients;
	String s3ImageURL;
	String airBnbPartyId;
	String expediaCollectPartyId;
	String expediaHotelCollectPartyId;
	String posCode;
	String expediaChannelPartnerId;
	String expediaChannelId;
	String flats9ChannelId;
	String getaroomChannelId;
	String agodaChannelId;
	String getaroomPartyId;
	String bookingChannelPartnerId;
	String tripadvisorChannelPartnerId;
	String channelApiPartnerId;
	String marriottChannelPartnerId;
	String agodaChannelPartyId;
	String bookingPartyId;
	String airbnbChannelId;
	String expediaStagingChannelPartnerId;
	String expediaStagingChannelPartnerIdForPmMor;
	String expediaPwd;
	String expediaUsername;
	String expediaStagingUsername;
	String expediaStagingPwd;
	String fewoChannelId;
	String fewoOrganizationId;
	String fewoPartyId;
	String adriaticId;
	String googleHotelAdsChannelId;
	String pmIdsForStatusEmail;
	String bookingcomUsername;
	String bookingcomPassword;
	String addressesForStatusEmail;
	String ctripChannelPartnerId;
	String ctripChannelPartnerPassword;
	String ctripChannelPartnerCodeContext;
	String expediaProdTestPropertyIds;
	String cancellationPolicyReportRecepients;
	String supportRecipients;
	String inntopiaStagingUsername;
	String inntopiaStagingPassword;
	String inntopiaStagingSenderId;
	String inntopiaStagingUrl;
	String inntopiaUsername;
	String inntopiaPassword;
	String inntopiaSenderId;
	String inntopiaUrl;

	private String channelApiChannelId;
	private String channelApiStagingChannelId;
	private String channelApiPartyId;
	String expediaArchiveChannelId;

	@Autowired
	ManagerToChannelAccountDao managerToChannelAccountDao;

	String expediaImagesReportRecepients;

	public static final Logger LOG = Logger.getLogger(RazorConfig.class);

	@PostConstruct
	public void init() {

		try {
			// If you add Configuration1, and then Configuration2, 
			// any properties shared will mean that the value defined by Configuration1 will be returned.
			razorConfig.addConfiguration(new PropertiesConfiguration(RAZOR_CONFIG_FILE));
			razorConfig.addConfiguration(new SystemConfiguration());
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			LOG.error(e.getMessage());
		}
	}

	public String getBookingcomUsername() {
		if (this.bookingcomUsername == null) {
			bookingcomUsername = settingDao.readbyname(BOOKINGCOM_USERNAME).getValue();
		}
		return bookingcomUsername;
	}

	public String getBookingcomPassword() {
		if (this.bookingcomPassword == null) {
			bookingcomPassword = settingDao.readbyname(BOOKINGCOM_PWD).getValue();
		}
		return bookingcomPassword;
	}

	public static boolean isProduction() {
		LOG.info("Profile of the current context: " + razorConfig.getString(ENVIRONMENT));
		return PROD_ENVIRONMENT.equals(razorConfig.getString(ENVIRONMENT));
	}

	public boolean isDemo() {
		return DEMO_ENVIRONMENT_VALUE.equals(razorConfig.getString(DEMO_ENVIRONMENT));
	}

	public boolean isTestDemo() {
		return TEST_DEMO_ENVIRONMENT_VALUE.equals(razorConfig.getString(TEST_DEMO_ENVIRONMENT));
	}

	public boolean isQa() {
		return QA_ENVIRONMENT_VALUE.equals(razorConfig.getString(QA_ENVIRONMENT));
	}

	public String getBelvillaId() {
		if (this.belvillaId == null) {
			belvillaId = settingDao.readbyname(PARTY_BELVILLA_ID).getValue();
		}
		return belvillaId;
	}

	public int getTripadvisorChannelId() {
		if (tripadvisorId == null) {
			tripadvisorId = settingDao.readbyname(TRIPADVISOR_ID).getValue();
		}
		return Integer.valueOf(tripadvisorId);
	}
	
	public int getMarriottArchivalIdChannelId() {
		if (marriottArchivalId == null) {
			marriottArchivalId = settingDao.readbyname(MARRIOTT_ARCHIVAL_ID).getValue();
		}
		return Integer.valueOf(marriottArchivalId);
	}
	
	public int getMarriottIdChannelId() {
		if (marriottId == null) {
			marriottId = settingDao.readbyname(MARRIOTT_ID).getValue();
		}
		return Integer.valueOf(marriottId);
	}
	
	public int getHvmiIdChannelId() {
		if (hvmiId == null) {
			hvmiId = settingDao.readbyname(HVMI_ID).getValue();
		}
		return Integer.valueOf(hvmiId);
	}
	
	public String getHvmiAuthToken() {
		if (hvmiToken == null) {
			hvmiToken = settingDao.readbyname(HVMI_AUTH_TOKEN).getValue();
		}
		return hvmiToken;
	}
	
	public Integer getChannelApiChannelId() {
		if (channelApiId == null) {
			Setting readbyname = settingDao.readbyname(CHANNELAPI_ID);
			channelApiId = readbyname != null ? readbyname.getValue() : null;
		}
		return channelApiId != null ? Integer.valueOf(channelApiId) : null;
	}

	public String getNextpaxVacasolId() {
		if (this.nextpaxVacasolId == null) {
			nextpaxVacasolId = settingDao.readbyname(PARTY_NEXTPAX_VACASOL_ID).getValue();
		}
		return nextpaxVacasolId;
	}

	public Integer getBookingChannelPartnerId() {
		if (bookingChannelPartnerId == null) {
			bookingChannelPartnerId = settingDao.readbyname(BOOKING_CHANNELPARTNER_ID).getValue();
		}
		return Integer.parseInt(bookingChannelPartnerId);
	}

	public Integer getTripadvisorChannelPartnerId() {
		if (tripadvisorChannelPartnerId == null) {
			tripadvisorChannelPartnerId = settingDao.readbyname(TRIPADVISOR_CHANNELPARTNER_ID).getValue();
		}
		return Integer.parseInt(tripadvisorChannelPartnerId);
	}
	
	public String getChannelApiPosCode() {
		if (channelApiPosCode == null) {
			channelApiPosCode = settingDao.readbyname(CHANNELAPI_POS_CODE).getValue();
		}
		return channelApiPosCode;
	}

	public void setChannelApiPosCode(String channelApiPosCode) {
		this.channelApiPosCode = channelApiPosCode;
	}

	public String getMarriottPosCode() {
		if (marriottPosCode == null) {
			marriottPosCode = settingDao.readbyname(MARRIOTT_POS_CODE).getValue();
		}
		return marriottPosCode;
	}

	public void setMarriottPosCode(String marriottPosCode) {
		this.marriottPosCode = marriottPosCode;
	}
	
	public String getmarriottAuthorization() {
		if (marriottAuthorization == null) {
			marriottAuthorization = settingDao.readbyname(MARRIOTT_AUTHORIZATION_CODE).getValue();
		}
		return marriottAuthorization;
	}

	public void setmarriottAuthorization(String marriottAuthorization) {
		this.marriottAuthorization = marriottAuthorization;
	}
	
	public String getChannelAuthorization(String channelId) {
		if (channelAuthorization == null) {
			channelAuthorization = settingDao.readbyname(channelId+"_authorization").getValue();
		}
		return channelAuthorization;
	}

	public void setChannelAuthorization(String channelAuthorization) {
		this.channelAuthorization = channelAuthorization;
	}
	
	

	public Integer getChannelApiPartnerId() {
		if (channelApiPartnerId == null) {
			channelApiPartnerId = settingDao.readbyname(CHANNELAPI_CHANNELPARTNER_ID).getValue();
		}
		return Integer.parseInt(channelApiPartnerId);
	}

	public void setChannelApiPartnerId(String channelApiPartnerId) {
		this.channelApiPartnerId = channelApiPartnerId;
	}

	public Integer getMarriottChannelPartnerId() {
		if (marriottChannelPartnerId == null) {
			marriottChannelPartnerId = settingDao.readbyname(MARRIOTT_CHANNELPARTNER_ID).getValue();
		}
		return Integer.parseInt(marriottChannelPartnerId);
	}

	public void setMarriottChannelPartnerId(String marriottChannelPartnerId) {
		this.marriottChannelPartnerId = marriottChannelPartnerId;
	}
	
	public int getBookingChannelId() {
		//return razorConfig.getInt(RazorConfig.BOOKING_ID, 276);
		if (bookingcomId == null) {
			bookingcomId = settingDao.readbyname(BOOKING_ID).getValue();
		}
		return Integer.valueOf(bookingcomId);
	}

	public String getTripadvisorPosCode() {
		if (tripadvisorPosCode == null) {
			tripadvisorPosCode = settingDao.readbyname(TRIPADVISOR_POS_CODE).getValue();
		}
		return tripadvisorPosCode;
	}

	public int getInntopiaChannelId() {
		if (inntopiaChannelId == null) {
			inntopiaChannelId = settingDao.readbyname(INNTOPIA_CHANNEL_ID).getValue();
		}
		return Integer.valueOf(inntopiaChannelId);
	}

	public int getBookingPollerTime() {
		bookingPollerTime = settingDao.readbyname(BOOKING_POLLER_TIME).getValue();
		if (bookingPollerTime == null || bookingPollerTime.isEmpty()) {
			bookingPollerTime = DEFAULT_BOOKING_POLLER_TIME;
		}
		return Integer.valueOf(bookingPollerTime);
	}

	public static Integer getBookingArchiveChannelId() {
		return BOOKING_ARCHIVE_ID;
	}

	public String getAuthorizationBookingRecepients() {
		if (authorizationBookingRecepients == null) {
			authorizationBookingRecepients = settingDao.readbyname(BOOKING_AUTHORIZATION_EMAIL).getValue();
		}
		return authorizationBookingRecepients;
	}

	public ArrayList<String> getExpediaImagesReportEmailRecepients() {
		if (expediaImagesReportRecepients == null) {
			expediaImagesReportRecepients = settingDao.readbyname(EXPEDIA_IMAGES_REPORT_EMAIL_RECEPIENTS).getValue();
		}

		ArrayList<String> recepientList = new ArrayList<String>();
		if (expediaImagesReportRecepients != null) {
			StringTokenizer tokens = new StringTokenizer(expediaImagesReportRecepients, ",");
			while (tokens.hasMoreTokens()) {
				recepientList.add(tokens.nextToken());
			}
		}
		return recepientList;
	}

	public ArrayList<String> getRecepientsForChannelErrors() {
		String strPartner = this.getAuthorizationBookingRecepients();
		ArrayList<String> recepientList = new ArrayList<String>();
		if (strPartner != null) {
			StringTokenizer tokens = new StringTokenizer(strPartner, ",");
			while (tokens.hasMoreTokens()) {
				recepientList.add(tokens.nextToken());
			}
		}
		return recepientList;
	}

	public String getS3ImageURL() {
		if (s3ImageURL == null) {
			s3ImageURL = settingDao.readbyname(S3_IMAGE_URL).getValue();
		}
		return s3ImageURL;
	}

	/*	
		public static String getValue(String key, String defaultValue) {
			String value = defaultValue;
			if (razorConfig.getString(key) != null) {
				value = razorConfig.getString(key);
			}
			return value;
		}
	*/

	public static String getValue(String key) {
		return razorConfig.getString(key);
	}

	public static String getValue(String key, String defaultValue) {
		return razorConfig.getString(key, defaultValue);
	}

	public static String getDBEnvironmentId() {
		return razorConfig.getString(DB_ENVIRONMENT);
	}
	
	public String getMarriottUrl() {
		return settingDao.readbyname(MARRIOTT_URL).getValue();
	}
	
	public String getFeeTaxUrlUrl() {
		return settingDao.readbyname(FEE_TAX_URL).getValue();
	}
	
	public String getChannelApiUrl(String channelId) {
		//return settingDao.readbyname(channelId).getValue();
		ChannelPartner channelPartner = channelPartnerDao.read(Integer.valueOf(channelId));
		return channelPartner.getCallbackURL();
	}
	
	public String getCancellationPolicy(String cancellationPolicyId) {
		return settingDao.readbyname(cancellationPolicyId).getValue();
	}

	public String getAirBnbChannelId() {
		if (airBnbChannelId == null) {
			airBnbChannelId = settingDao.readbyname(AIRBNB_CHANNEL_ID).getValue();
		}
		return airBnbChannelId;
	}

	public String getAirBnbArchiveChannelId() {
		if (airBnbStagingChannelId == null) {
			airBnbStagingChannelId = settingDao.readbyname(AIRBNB_ARCHIVE_CHANNEL_ID).getValue();
		}
		return airBnbStagingChannelId;
	}

	public String getAirBnbPartyId() {
		if (airBnbPartyId == null) {
			airBnbPartyId = settingDao.readbyname(PARTY_AIRBNB_ID).getValue();
		}
		return airBnbPartyId;
	}

	public String getAirbnbStaingPartyId() {
		return settingDao.readbyname(PARTY_AIRBNB_STAGING_ID).getValue();
	}
	
	public String getExpediaCollectPartyId() {
		if (expediaCollectPartyId == null) {
			expediaCollectPartyId = settingDao.readbyname(EXPEDIA_COLLECT_CHANNELPARTNER_ID).getValue();
		}
		return expediaCollectPartyId;
	}

	public String getExpediaHotelCollectPartyId() {
		if (expediaHotelCollectPartyId == null) {
			expediaHotelCollectPartyId = settingDao.readbyname(EXPEDIA_HOTEL_COLLECT_CHANNELPARTNER_ID).getValue();
		}
		return expediaHotelCollectPartyId;
	}

	public String getPosCode() {
		if (posCode == null) {
			posCode = settingDao.readbyname(POS_CODE).getValue();
		}
		return posCode;
	}

	public ArrayList<String> getBookingRecepients() {
		if (bookingcomRecepients == null) {
			bookingcomRecepients = settingDao.readbyname(BOOKING_RESERVATION_RECEPIENTS).getValue();
		}
		String[] recepients = bookingcomRecepients.split(",");
		ArrayList<String> recepientList = new ArrayList<String>();
		for (String recepient : recepients) {
			recepientList.add(recepient.trim());
		}
		return recepientList;
	}
	
	public ArrayList<String> getSupportRecepients() {
		if (supportRecipients == null) {
			supportRecipients = settingDao.readbyname(SUPPORT_RECEPIENTS).getValue();
		}
		String[] recepients = supportRecipients.split(",");
		ArrayList<String> recepientList = new ArrayList<String>();
		for (String recepient : recepients) {
			recepientList.add(recepient.trim());
		}
		return recepientList;
	}

	public ArrayList<String> getCancellationPolicyReportRecepients() {
		if (cancellationPolicyReportRecepients == null) {
			cancellationPolicyReportRecepients = settingDao.readbyname(CANCELLATION_POLICY_REPORT_RECEPIENTS).getValue();
		}
		String[] recepients = cancellationPolicyReportRecepients.split(",");
		ArrayList<String> recepientList = new ArrayList<String>();
		for (String recepient : recepients) {
			recepientList.add(recepient.trim());
		}
		return recepientList;
	}

	public ArrayList<String> getExpediaOnboardingPropertyRecepients() {
		if (expediaOnboardingPropertyRecepients == null) {
			expediaOnboardingPropertyRecepients = settingDao.readbyname(EXPEDIA_ONBOARDING_PROPERTY_RECEPIENTS).getValue();
		}
		String[] recepients = expediaOnboardingPropertyRecepients.split(",");
		ArrayList<String> recepientList = new ArrayList<String>();
		for (String recepient : recepients) {
			recepientList.add(recepient.trim());
		}
		return recepientList;
	}
	
	public ArrayList<String> getExpediaUpdateFeeBulkDocUserEmail() {
		String sendEmailExpedia = settingDao.readbyname(EXPEDIA_UPDATE_FEE_BULK_DOC_USER_EMAIL).getValue();
		String[] recepients = sendEmailExpedia.split(",");
		ArrayList<String> recepientList = new ArrayList<String>();
		for (String recepient : recepients) {
			recepientList.add(recepient.trim());
		}
		return recepientList;
	}

	public ArrayList<String> getExpediaOnboardingBulkFeeRecepients() {
		if (expediaOnboardingBulkFeeRecepients == null) {
			expediaOnboardingBulkFeeRecepients = settingDao.readbyname(EXPEDIA_ONBOARDING_BULK_FEE_RECEPIENTS).getValue();
		}
		String[] recepients = expediaOnboardingBulkFeeRecepients.split(",");
		ArrayList<String> recepientList = new ArrayList<String>();
		for (String recepient : recepients) {
			recepientList.add(recepient.trim());
		}
		return recepientList;
	}

	public ArrayList<String> getCSEmailRecepients() {
		String csRecepients = settingDao.readbyname(CHANNEL_SUPPORT_RECEPIENTS).getValue();
		if (StringUtils.isEmpty(csRecepients)) {
			csRecepients = "channelsupport@mybookingpal.com";
		}
		String[] recepients = csRecepients.split(",");
		ArrayList<String> recepientList = new ArrayList<String>();
		for (String recepient : recepients) {
			recepientList.add(recepient.trim());
		}
		return recepientList;
	}

	/**
	 * @return Expedia hotel collect channel id
	 */
	public String getExpediaChannelId_For_PM_MOR() {

		if (expediaChannelPartnerId == null) {
			expediaChannelPartnerId = settingDao.readbyname(EXPEDIA_CHANNEL_PM_MOR_ID).getValue();
		}
		return expediaChannelPartnerId;
	}

	/**
	 * @return Expedia collect channel id
	 */
	public String getExpediaChannelId() {
		if (expediaChannelId == null) {
			expediaChannelId = settingDao.readbyname(EXPEDIA_CHANNEL_ID).getValue();
		}
		return expediaChannelId;
	}

	public List<String> getExpediaProdTestProperties() {
		List<String> propertyIdList = new ArrayList<String>();
		Setting propertyIdsSet = settingDao.readbyname(RazorConfig.KAFKA_PROD_TEST_PROPERTY_IDS);
		String[] propertyIds = null;
		if (propertyIdsSet != null) {
			String propertyList = propertyIdsSet.getValue();
			if (StringUtils.isNotEmpty(propertyList)) {
				propertyIds = StringUtils.split(propertyList, ",");
				if (propertyIds != null) {
					for (String propertyId : propertyIds) {
						propertyIdList.add(propertyId);
					}
				}
			}
		}
		return propertyIdList;
	}

	public boolean isExpediaTestProperty(String productId) {
		if (getExpediaProdTestProperties().contains(productId)) {
			return true;
		} else {
			return false;
		}
	}

	public String getFlats9ChannelPartnerId() {
		if (flats9ChannelId == null) {
			flats9ChannelId = settingDao.readbyname(FLATS9_CHANNEL_ID).getValue();
		}
		return flats9ChannelId;
	}

	public String getGetaroomChannelId() {
		if (getaroomChannelId == null && settingDao.readbyname(GETAROOM_CHANNEL_ID) != null) {
			getaroomChannelId = settingDao.readbyname(GETAROOM_CHANNEL_ID).getValue();
		}
		return getaroomChannelId;
	}

	public String getAgodaChannelId() {
		if (agodaChannelId == null) {
			agodaChannelId = settingDao.readbyname(AGODA_CHANNEL_ID).getValue();
		}
		return agodaChannelId;
	}

	public Integer getAgodaChannelPartyId() {
		if (agodaChannelPartyId == null) {
			agodaChannelPartyId = settingDao.readbyname(AGODA_CHANNEL_PARTY_ID).getValue();
		}
		return Integer.parseInt(agodaChannelPartyId);
	}

	public String getGetaroomUatChannelId() {
		if (getaroomChannelId == null && settingDao.readbyname(GETAROOM_UAT_STAGING_CHANNEL_ID) != null) {
			getaroomChannelId = settingDao.readbyname(GETAROOM_CHANNEL_ID).getValue();
		}
		return getaroomChannelId;
	}

	public String getGetaroomPartyId() {
		if (getaroomPartyId == null) {
			getaroomPartyId = settingDao.readbyname(PARTY_GETAROOM_ID).getValue();
		}
		return getaroomPartyId;
	}

	public String getBookingPartyId() {
		if (bookingPartyId == null) {
			bookingPartyId = settingDao.readbyname(BOOKING_PARTY_ID).getValue();
		}
		return bookingPartyId;
	}

	public String getAirbnbChannelId() {
		if (airbnbChannelId == null) {
			airbnbChannelId = settingDao.readbyname(AIRBNB_CHANNEL_ID).getValue();
		}
		return airbnbChannelId;
	}

	public String getExpediaStagingChannelPartnerId() {
		if (expediaStagingChannelPartnerId == null) {
			expediaStagingChannelPartnerId = settingDao.readbyname(EXPEDIA_STAGING_CHANNEL_ID).getValue();
		}

		return expediaStagingChannelPartnerId;
	}

	public String getExpediaStagingChannelPartnerId_For_PM_MOR() {
		if (expediaStagingChannelPartnerIdForPmMor == null) {
			expediaStagingChannelPartnerIdForPmMor = settingDao.readbyname(EXPEDIA_STAGING_CHANNEL_PM_MOR_ID).getValue();
		}
		return expediaStagingChannelPartnerIdForPmMor;
	}

	public String getExpediaUsername() {
		if (expediaUsername == null) {
			expediaUsername = settingDao.readbyname(EXPEDIA_USERNAME).getValue();
		}
		return expediaUsername;
	}

	public String getExpediaPwd() {
		if (expediaPwd == null) {
			expediaPwd = settingDao.readbyname(EXPEDIA_PWD).getValue();
		}
		return expediaPwd;
	}

	public String getExpediaStagingUsername() {
		if (expediaStagingUsername == null) {
			expediaStagingUsername = settingDao.readbyname(EXPEDIA_STAGING_USERNAME).getValue();
		}
		return expediaStagingUsername;
	}

	public String getInntopiaStaginUserName() {
		if (inntopiaStagingUsername == null) {
			inntopiaStagingUsername = settingDao.readbyname(INNTOPIA_STAGING_USERNAME).getValue();
		}
		return inntopiaStagingUsername;
	}

	public String getInntopiaStagingPassword() {
		if (inntopiaStagingPassword == null) {
			inntopiaStagingPassword = settingDao.readbyname(INNTOPIA_STAGING_PASSWORD).getValue();
		}
		return inntopiaStagingPassword;
	}

	public String getInntopiaStagingSenderId() {
		if (inntopiaStagingSenderId == null) {
			inntopiaStagingSenderId = settingDao.readbyname(INNTOPIA_STAGING_SENDER_ID).getValue();
		}
		return inntopiaStagingSenderId;
	}

	public String getInntopiaStagingUrl() {
		if (inntopiaStagingUrl == null) {
			inntopiaStagingUrl = settingDao.readbyname(INNTOPIA_STAGING_URL).getValue();
		}
		return inntopiaStagingUrl;
	}

	public String getInntopiaUsername() {
		if (inntopiaUsername == null) {
			inntopiaUsername = settingDao.readbyname(INNTOPIA_USERNAME).getValue();
		}
		return inntopiaUsername;
	}

	public String getInntopiaPassword() {
		if (inntopiaPassword == null) {
			inntopiaPassword = settingDao.readbyname(INNTOPIA_PASSWORD).getValue();
		}
		return inntopiaPassword;
	}

	public String getInntopiaSenderId() {
		if (inntopiaSenderId == null) {
			inntopiaSenderId = settingDao.readbyname(INNTOPIA_SENDER_ID).getValue();
		}
		return inntopiaSenderId;
	}

	public String getInntopiaUrl() {
		if (inntopiaUrl == null) {
			inntopiaUrl = settingDao.readbyname(INNTOPIA_URL).getValue();
		}
		return inntopiaUrl;
	}

	public String getExpediaStagingPwd() {
		if (expediaStagingPwd == null) {
			expediaStagingPwd = settingDao.readbyname(EXPEDIA_STAGING_PWD).getValue();
		}
		return expediaStagingPwd;
	}

	public String getFewoChannelId() {
		if (fewoChannelId == null && settingDao.readbyname(FEWO_CHANNEL_ID) != null) {
			fewoChannelId = settingDao.readbyname(FEWO_CHANNEL_ID).getValue();
		}
		return fewoChannelId;
	}

	public String getFewoPartyId() {
		if (fewoPartyId == null && settingDao.readbyname(FEWO_PARTY_ID) != null) {
			fewoPartyId = settingDao.readbyname(FEWO_PARTY_ID).getValue();
		}
		return fewoPartyId;
	}

	public String getFewoOrganizationId() {
		if (fewoOrganizationId == null && settingDao.readbyname(FEWO_ORGANIZATION_ID) != null) {
			fewoOrganizationId = settingDao.readbyname(FEWO_ORGANIZATION_ID).getValue();
		}
		return fewoOrganizationId;
	}
	
	public String getFewoProdOrganizationId() {
		if (fewoOrganizationId == null && settingDao.readbyname(FEWO_PROD_ORGANIZATION_ID) != null) {
			fewoOrganizationId = settingDao.readbyname(FEWO_PROD_ORGANIZATION_ID).getValue();
		}
		return fewoOrganizationId;
	}

	public String getAdriaticId() {
		if (adriaticId == null) {
			adriaticId = settingDao.readbyname(PARTY_ADRIATIC_ID).getValue();
		}
		return adriaticId;
	}

	public static ArrayList<String> getAirbnbPayoutEmailRecepients() {
		String[] recepients = razorConfig.getStringArray(RazorConfig.AIRBNB_PAYOUT_EMAIL_RECEPIENTS);
		if (recepients == null)
			recepients = new String[] { "accountspayable@mybookingpal.com" };
		ArrayList<String> recepientList = new ArrayList<String>();
		for (String recepient : recepients) {
			recepientList.add(recepient);
		}
		return recepientList;
	}

	public static ArrayList<String> getAirbnbExceptionPayoutEmailRecepients() {
		String[] recepients = razorConfig.getStringArray(RazorConfig.AIRBNB_EXCEPTION_PAYOUT_EMAIL_RECEPIENTS);
		ArrayList<String> recepientList = new ArrayList<String>();
		for (String recepient : recepients) {
			recepientList.add(recepient);
		}
		return recepientList;
	}

	public static ArrayList<String> getPMsWithoutEssentialsAmenity() {
		String[] pmsIdsTab = razorConfig.getStringArray(RazorConfig.PMLIST_WITHOUT_AMENTIES_ESSENTIALS);
		ArrayList<String> pmsIds = new ArrayList<String>();
		for (String pmsId : pmsIdsTab) {
			pmsIds.add(pmsId);
		}
		return pmsIds;
	}

	public static boolean getExpediaLOSPricingFlag() {
		// TODO Auto-generated method stub
		return razorConfig.getBoolean(EXPEDIA_LOS_PRICING_FLAG, true);
	}

	public static ArrayList<String> getExpediaLOSPricingPMIDS() {
		String[] pmsIdsTab = razorConfig.getStringArray(RazorConfig.EXPEDIA_LOS_PRICING_PMIDS);
		ArrayList<String> pmsIds = new ArrayList<String>();
		for (String pmsId : pmsIdsTab) {
			pmsIds.add(pmsId);
		}
		return pmsIds;
	}

	public static ArrayList<String> getExpediaLOSPricingPropertyIDS() {
		String[] propertyIdsTab = razorConfig.getStringArray(RazorConfig.EXPEDIA_LOS_PRICING_PROPERTYIDS);
		ArrayList<String> propertyIds = new ArrayList<String>();
		for (String propertyId : propertyIdsTab) {
			propertyIds.add(propertyId);
		}
		return propertyIds;
	}

	public ArrayList<String> getExpediaAverageLOSPmIds() {
		ArrayList<String> pmIdsList = new ArrayList<String>();
		Setting pmsSet = settingDao.readbyname(RazorConfig.EXPEDIA_AVG_LOS_PRICING_PMIDS);
		String[] pmIds = null;
		if (pmsSet != null) {
			String pmsList = pmsSet.getValue();
			if (StringUtils.isNotEmpty(pmsList)) {
				pmIds = StringUtils.split(pmsList, ",");
			}
			for (String pmId : pmIds) {
				pmIdsList.add(pmId);
			}
		}
		return pmIdsList;
	}

	public String getGoogleHotelAdsChannelId() {
		if (googleHotelAdsChannelId == null) {
			Setting googleChannel = settingDao.readbyname(GOOGLE_HOTEL_ADS_CHANNEL_ID);
			if (googleChannel != null) {
				googleHotelAdsChannelId = googleChannel.getValue();
			}
		}
		return googleHotelAdsChannelId;
	}

	public ArrayList<String> getGoogleHotelAdsSummaryEmailRecipients() {
		Setting propertySet = settingDao.readbyname(RazorConfig.GOOGLE_HOTEL_ADS_SUMMARY_EMAIL_RECIPIENTS);
		if (propertySet != null && StringUtils.isNotEmpty(propertySet.getValue())) {
			return new ArrayList<>(Arrays.asList(propertySet.getValue().split(",")));
		}
		return new ArrayList<>();
	}

	public ArrayList<String> getExpediaAverageLOSLargePmIds() {
		ArrayList<String> pmIdsList = new ArrayList<String>();
		Setting pmsSet = settingDao.readbyname(RazorConfig.EXPEDIA_AVG_LOS_PRICING_LARGE_PMIDS);
		String[] pmIds = null;
		if (pmsSet != null) {
			String pmsList = pmsSet.getValue();
			if (StringUtils.isNotEmpty(pmsList)) {
				pmIds = StringUtils.split(pmsList, ",");
			}
			for (String pmId : pmIds) {
				pmIdsList.add(pmId);
			}
		}
		return pmIdsList;
	}

	public ArrayList<String> getMultiKeyLevelPmIds() {
		ArrayList<String> pmIdsList = new ArrayList<String>();
		Setting pmsSet = settingDao.readbyname(RazorConfig.BP_MULTIUNIT_KEYLEVEL_PMIDS);
		String[] pmIds = null;
		if (pmsSet != null) {
			String pmsList = pmsSet.getValue();
			if (StringUtils.isNotEmpty(pmsList)) {
				pmIds = StringUtils.split(pmsList, ",");
			}
			for (String pmId : pmIds) {
				pmIdsList.add(pmId);
			}
		}
		return pmIdsList;
	}

	public ArrayList<String> getMultiRepLevelPmIds() {
		ArrayList<String> pmIdsList = new ArrayList<String>();
		Setting pmsSet = settingDao.readbyname(RazorConfig.BP_MULTIUNIT_REPLEVEL_PMIDS);
		String[] pmIds = null;
		if (pmsSet != null) {
			String pmsList = pmsSet.getValue();
			if (StringUtils.isNotEmpty(pmsList)) {
				pmIds = StringUtils.split(pmsList, ",");
			}
			for (String pmId : pmIds) {
				pmIdsList.add(pmId);
			}
		}
		return pmIdsList;
	}

	public ArrayList<String> getExpediaSplitJobsForHomerezBatchProperties(int batchNumber) {
		ArrayList<String> propertyIdsList = new ArrayList<String>();
		Setting propertySet = new Setting();
		if (batchNumber == 1) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_BATCH1_PROPERTIES);
		} else if (batchNumber == 2) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_BATCH2_PROPERTIES);
		} else if (batchNumber == 3) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_BATCH3_PROPERTIES);
		} else if (batchNumber == 4) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_BATCH4_PROPERTIES);
		}
		String[] propertyIds = null;
		if (propertySet != null) {
			String propertyList = propertySet.getValue();
			if (StringUtils.isNotEmpty(propertyList)) {
				propertyIds = StringUtils.split(propertyList, ",");
			}
			for (String propertyId : propertyIds) {
				propertyIdsList.add(propertyId);
			}
		}
		return propertyIdsList;
	}

	public ArrayList<String> getExpediaSplitJobsForVacasaBatchProperties(int batchNumber) {
		ArrayList<String> propertyIdsList = new ArrayList<String>();
		Setting propertySet = new Setting();
		if (batchNumber == 1) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH1_PROPERTIES);
		} else if (batchNumber == 2) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH2_PROPERTIES);
		} else if (batchNumber == 3) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH3_PROPERTIES);
		} else if (batchNumber == 4) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH4_PROPERTIES);
		} else if (batchNumber == 5) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH5_PROPERTIES);
		} else if (batchNumber == 6) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH6_PROPERTIES);
		} else if (batchNumber == 7) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH7_PROPERTIES);
		} else if (batchNumber == 8) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH8_PROPERTIES);
		} else if (batchNumber == 9) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH9_PROPERTIES);
		} else if (batchNumber == 10) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH10_PROPERTIES);
		} else if (batchNumber == 11) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH11_PROPERTIES);
		} else if (batchNumber == 12) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH12_PROPERTIES);
		} else if (batchNumber == 13) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH13_PROPERTIES);
		} else if (batchNumber == 14) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH14_PROPERTIES);
		} else if (batchNumber == 15) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH15_PROPERTIES);
		} else if (batchNumber == 16) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_BATCH16_PROPERTIES);
		}

		String[] propertyIds = null;
		if (propertySet != null) {
			String propertyList = propertySet.getValue();
			if (StringUtils.isNotEmpty(propertyList)) {
				propertyIds = StringUtils.split(propertyList, ",");
			}
			for (String propertyId : propertyIds) {
				propertyIdsList.add(propertyId);
			}
		}
		return propertyIdsList;
	}

	public static List<String> getStringList(String key) {
		List<Object> values = razorConfig.getList(key);
		List<String> res = new ArrayList<>();
		if (values != null) {
			for (Object value : values) {
				res.add(value.toString());
			}
		}
		return res;
	}

	public List<ManagerToChannelAccount> getAdriaticPMIds() {
		Setting pmsSet = settingDao.readbyname(RazorConfig.ADRIATIC_VPM_IDS);
		List<ManagerToChannelAccount> managerToChannelAccounts = new ArrayList<>();
		String[] adriaticPMs = null;
		if (pmsSet != null) {
			String pmsList = pmsSet.getValue();
			if (StringUtils.isNotEmpty(pmsList)) {
				adriaticPMs = StringUtils.split(pmsList, ",");
			}
		}
		if (adriaticPMs != null && adriaticPMs.length >= 0) {
			List<String> pmsIds = new ArrayList<String>(Arrays.asList(adriaticPMs));
			for (String pmsId : pmsIds) {
				ManagerToChannelAccount action = new ManagerToChannelAccount();
				action.setChannelID(Integer.valueOf(getAirBnbChannelId()));
				action.setSupplierID(Integer.parseInt(pmsId));
				List<ManagerToChannelAccount> managerToChannelAccount = managerToChannelAccountDao.readByPropertyManagerAndChannelPartnerWithoutState(action);
				if (managerToChannelAccount != null) {
					managerToChannelAccounts.addAll(managerToChannelAccount);
				}
			}
		}
		return managerToChannelAccounts;
	}

	public List<ManagerToChannelAccount> getFirstVacasaVPMIds() {
		return getVacasaVPMIdsByBatch("1");
	}

	public List<ManagerToChannelAccount> getSecondVacasaVPMIds() {
		return getVacasaVPMIdsByBatch("2");
	}

	public List<ManagerToChannelAccount> getThirdVacasaVPMIds() {
		return getVacasaVPMIdsByBatch("3");
	}

	public List<ManagerToChannelAccount> getVacasaVPMIdsByBatch(String batchNumber) {
		Setting pmsSet = settingDao.readbyname(RazorConfig.VACASA_VPM_IDS + batchNumber);
		List<ManagerToChannelAccount> managerToChannelAccounts = new ArrayList<ManagerToChannelAccount>();
		String[] vacasaPMs = null;
		if (pmsSet != null) {
			String pmsList = pmsSet.getValue();
			if (StringUtils.isNotEmpty(pmsList)) {
				vacasaPMs = StringUtils.split(pmsList, ",");
			}
		}
		if (vacasaPMs == null || vacasaPMs.length == 0) {
			vacasaPMs = razorConfig.getStringArray(RazorConfig.VACASA_VPM_IDS + batchNumber);
		}
		if (vacasaPMs != null && vacasaPMs.length >= 0) {
			List<String> pmsIds = new ArrayList<String>(Arrays.asList(vacasaPMs));
			for (String pmsId : pmsIds) {
				ManagerToChannelAccount action = new ManagerToChannelAccount();
				action.setChannelID(Integer.valueOf(getAirBnbChannelId()));
				action.setSupplierID(Integer.parseInt(pmsId));
				List<ManagerToChannelAccount> managerToChannelAccount = managerToChannelAccountDao.readByPropertyManagerAndChannelPartnerWithoutState(action);
				if (managerToChannelAccount != null) {
					managerToChannelAccounts.addAll(managerToChannelAccount);
				}
			}
		}
		return managerToChannelAccounts;
	}

	public static String[] getEmailList(String key) {
		return razorConfig.getStringArray(key);
	}

	public ArrayList<String> getExpediaSplitJobsForVacasaProperties(int jobNumber) {
		ArrayList<String> propertyIdsList = new ArrayList<String>();
		Setting propertySet = new Setting();
		if (jobNumber == 1) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_JOB1_PROPERTIES);
		} else if (jobNumber == 2) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_JOB2_PROPERTIES);
		} else if (jobNumber == 3) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_JOB3_PROPERTIES);
		} else if (jobNumber == 4) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_JOB4_PROPERTIES);
		} else if (jobNumber == 5) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_JOB5_PROPERTIES);
		} else if (jobNumber == 6) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_JOB6_PROPERTIES);
		} else if (jobNumber == 7) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_JOB7_PROPERTIES);
		} else if (jobNumber == 8) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_VACASA_JOB8_PROPERTIES);
		}

		String[] propertyIds = null;
		if (propertySet != null) {
			String propertyList = propertySet.getValue();
			if (StringUtils.isNotEmpty(propertyList)) {
				propertyIds = StringUtils.split(propertyList, ",");
			}
			for (String propertyId : propertyIds) {
				propertyIdsList.add(propertyId);
			}
		}
		return propertyIdsList;
	}

	public ArrayList<String> getExpediaSplitJobsForHomerezProperties(int jobNumber) {
		ArrayList<String> propertyIdsList = new ArrayList<String>();
		Setting propertySet = new Setting();
		if (jobNumber == 1) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_HOMEREZ_JOB1_PROPERTIES);
		} else if (jobNumber == 2) {
			propertySet = settingDao.readbyname(RazorConfig.EXPEDIA_SPLITJOBS_HOMEREZ_JOB2_PROPERTIES);
		}

		String[] propertyIds = null;
		if (propertySet != null) {
			String propertyList = propertySet.getValue();
			if (StringUtils.isNotEmpty(propertyList)) {
				propertyIds = StringUtils.split(propertyList, ",");
			}
			
			if(propertyIds != null) {
				for (String propertyId : propertyIds) {
					propertyIdsList.add(propertyId);
				}
			}
		}
		return propertyIdsList;
	}

	public ArrayList<String> getBookingComWeeklyReportEmailList() {
		ArrayList<String> propertyIdsList = new ArrayList<String>();
		Setting propertySet = new Setting();
		propertySet = settingDao.readbyname(RazorConfig.BDC_WEEKLY_REPORT_LIST);

		String[] propertyIds = null;
		if (propertySet != null) {
			String propertyList = propertySet.getValue();
			if (StringUtils.isNotEmpty(propertyList)) {
				propertyIds = StringUtils.split(propertyList, ",");
			}
			for (String propertyId : propertyIds) {
				propertyIdsList.add(propertyId);
			}
		}
		return propertyIdsList;
	}

	public ArrayList<String> getAgodaBulkFileRecipientEmailList() {
		ArrayList<String> emailList = new ArrayList<>();
		Setting propertySet = settingDao.readbyname(RazorConfig.AGODA_BULK_FILES_RECIPIENT_EMAILS);

		if (propertySet != null && StringUtils.isNotEmpty(propertySet.getValue())) {
			emailList.addAll(Arrays.asList(propertySet.getValue().split(",")));
		}
		return emailList;
	}

	public ArrayList<String> getChannelFailedReservationNotificationRecepients() {
		String[] recepients = razorConfig.getStringArray(RazorConfig.CHANNEL_FAILED_RESERVATION_NOTIFICATION_RECEPIENTS);
		if (recepients == null)
			recepients = new String[] {"marko@mybookingpal.com","danijel@mybookingpal.com"};
		ArrayList<String> recepientList = new ArrayList<String>();
		Collections.addAll(recepientList, recepients);
		return recepientList;
	}

	public static ArrayList<String> getGetaroomRecepients() {
		String[] recepients = razorConfig.getStringArray(RazorConfig.GETAROOM_RECEPIENTS);
		if (recepients == null) {
			recepients = new String[] { "ehsan@mybookingpal.com" };
		}
		ArrayList<String> recepientList = new ArrayList<String>();
		Collections.addAll(recepientList, recepients);
		return recepientList;
	}

	public List<String> getPMsWith60daysCancelationPolicy() {
		Setting pmslist = settingDao.readbyname(RazorConfig.CANECELATION_PERIOD_60_DAYS);
		String[] pmIdsList = null;
		if (pmslist != null) {
			String pmTempList = pmslist.getValue();
			if (StringUtils.isNotEmpty(pmTempList)) {
				pmIdsList = StringUtils.split(pmTempList, ",");
			}
		}

		List<String> pmsInstancetList = new ArrayList<String>();
		for (String pm : pmIdsList) {
			pmsInstancetList.add(pm);
		}
		return pmsInstancetList;
	}

	public List<ManagerToChannelAccount> getDeltaTestPMIds() {
		Setting pmsSet = settingDao.readbyname(RazorConfig.DELTA_TEST_PM_IDS);
		List<ManagerToChannelAccount> managerToChannelAccounts = new ArrayList<>();
		String[] deltaTestPMIds = null;
		if (pmsSet != null) {
			String pmList = pmsSet.getValue();
			if (StringUtils.isNotEmpty(pmList)) {
				deltaTestPMIds = StringUtils.split(pmList, ",");
			}
		}
		if (deltaTestPMIds != null && deltaTestPMIds.length >= 0) {
			List<String> pmIds = new ArrayList<String>(Arrays.asList(deltaTestPMIds));
			for (String pmId : pmIds) {
				ManagerToChannelAccount action = new ManagerToChannelAccount();
				action.setChannelID(Integer.valueOf(getAirBnbChannelId()));
				action.setSupplierID(Integer.parseInt(pmId));
				List<ManagerToChannelAccount> managerToChannelAccount = managerToChannelAccountDao.readByPropertyManagerAndChannelPartnerWithoutState(action);
				if (managerToChannelAccount != null) {
					managerToChannelAccounts.addAll(managerToChannelAccount);
				}
			}
		}
		return managerToChannelAccounts;
	}

	public ArrayList<String> getSupplierIdsForStatusReport() {
		if (pmIdsForStatusEmail == null) {
			pmIdsForStatusEmail = settingDao.readbyname(IDS_FOR_SUPPLIERS_REPORT).getValue();
		}
		String[] pmIds = pmIdsForStatusEmail.split(",");
		ArrayList<String> supplierIDs = new ArrayList<String>();
		for (String pmId : pmIds) {
			supplierIDs.add(pmId.trim());
		}
		return supplierIDs;
	}

	public String getBookingMultiUnitLargeSupplierIds() {
		String pm = null;

		Setting pmsSet = settingDao.readbyname(RazorConfig.BOOKING_MULTIUNIT_LARGE_PMID_FOR_SPLITJOBS);
		if (pmsSet != null) {
			pm = pmsSet.getValue();

		}
		return pm;
	}

	public String getBatchSizeForBookingVacasaMultiunit() {
		String size = null;
		Setting pmsSet = settingDao.readbyname(RazorConfig.BOOKING_MULTIUNIT_LARGE_PMID_SPLIT_SIZE);
		if (pmsSet != null) {
			size = pmsSet.getValue();

		}
		return size;
	}

	public List<String> getBookingComLOSPropertyIds() {
		List<String> propertyIdsList = new ArrayList<>();
		try {
			Setting propertySet = settingDao.readbyname(RazorConfig.BOOKING_LOS_PROPERTY_IDS);

			String[] propertyIds = null;
			if (propertySet != null) {
				String propertyList = propertySet.getValue();
				if (StringUtils.isNotEmpty(propertyList)) {
					propertyIds = StringUtils.split(propertyList, ",");
				}
				for (String propertyId : propertyIds) {
					propertyIdsList.add(propertyId);
				}
			}

		} catch (Exception e) {
			LOG.error("Can't fetch data from setting table for booking los property ids property ...");
		}
		return propertyIdsList;

	}


	public String getBatchSizeForExpediaVacasaLos() {
		String size = null;
		Setting pmsSet = settingDao.readbyname(RazorConfig.EXPEDIA_LOS_LARGE_PMID_SPLIT_SIZE);
		if (pmsSet != null) {
			size = pmsSet.getValue();

		}
		return size;
	}

	public List<String> getVacasaProperties(String supplier) {
		NameIdAction nameIdAction = new NameIdAction();
		nameIdAction.setSupplierid(supplier);
		// List<String> productList = productDao.allMultiUnitKeyRootProductIdListBySupplier(nameIdAction);
		List<String> products = productDao.allSinglePropertiesIdListBySupplier(nameIdAction);

		return products;
	}

	public List<String> getVacasaPropertiesForExpediaSplitJobs(List<String> listChannelProductMapProducts, Integer jobNumber, String channelId) {
		String pmsSets = getBatchSizeForExpediaVacasaLos();
		int splitNumber = Integer.parseInt(pmsSets);
		LOG.info("split number" + splitNumber);
		int splitsize = listChannelProductMapProducts.size() / splitNumber;
		if (jobNumber == 7) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(0, splitsize);
		} else if (jobNumber == 8) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(splitsize, 2 * (splitsize));
		} else if (jobNumber == 9) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(2 * (splitsize), 3 * (splitsize));
		} else if (jobNumber == 10) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(3 * (splitsize), 4 * (splitsize));
		} else if (jobNumber == 11) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(4 * (splitsize), 5 * (splitsize));
		} else if (jobNumber == 12) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(5 * (splitsize), 6 * (splitsize));
		} else if (jobNumber == 13) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(6 * (splitsize), 7 * (splitsize));
		} else if (jobNumber == 14) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(7 * (splitsize), 8 * (splitsize));
		} else if (jobNumber == 15) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(8 * (splitsize), 9 * (splitsize));
		} else if (jobNumber == 16) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(9 * (splitsize), 10 * (splitsize));
		} else if (jobNumber == 17) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(10 * (splitsize), 11 * (splitsize));
		} else if (jobNumber == 18) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(11 * (splitsize), 12 * (splitsize));
		} else if (jobNumber == 19) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(12 * (splitsize), 13 * (splitsize));
		} else if (jobNumber == 20) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(13 * (splitsize), 14 * (splitsize));
		} else if (jobNumber == 21) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(14 * (splitsize), 15 * (splitsize));
		} else if (jobNumber == 22) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(15 * (splitsize), 16 * (splitsize));
		} else if (jobNumber == 23) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(16 * (splitsize), 17 * (splitsize));
		} else if (jobNumber == 24) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(17 * (splitsize), 18 * (splitsize));
		} else if (jobNumber == 25) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(18 * (splitsize), 19 * (splitsize));
		} else if (jobNumber == 26) {
			listChannelProductMapProducts = listChannelProductMapProducts.subList(19 * (splitsize), listChannelProductMapProducts.size());
		}

		return listChannelProductMapProducts;
	}

	public ArrayList<String> getEmailsForStatusReport() {
		if (addressesForStatusEmail == null) {
			addressesForStatusEmail = settingDao.readbyname(EMAILS_FOR_SUPPLIERS_REPORT).getValue();
		}
		String[] emailsForStatusReport = addressesForStatusEmail.split(",");
		ArrayList<String> reportEmails = new ArrayList<String>();
		for (String email : emailsForStatusReport) {
			reportEmails.add(email.trim());
		}
		return reportEmails;
	}

	public String getCtripChannelPartnerId() {
		if (ctripChannelPartnerId == null) {
			ctripChannelPartnerId = settingDao.readbyname(CTRIP_PARTNER_ID).getValue();
		}
		return ctripChannelPartnerId;
	}

	public String getCtripChannelPartnerPassword() {
		if (ctripChannelPartnerPassword == null) {
			ctripChannelPartnerPassword = settingDao.readbyname(CTRIP_PARTNER_PASSWORD).getValue();
		}
		return ctripChannelPartnerPassword;
	}

	public String getCtripChannelPartnerCodeContext() {
		if (ctripChannelPartnerCodeContext == null) {
			ctripChannelPartnerCodeContext = settingDao.readbyname(CTRIP_CODE_CONTEXT).getValue();
		}
		return ctripChannelPartnerCodeContext;
	}

	public String getCtripMBPreservationEndPoint() {
		if (ctripChannelPartnerCodeContext == null) {
			ctripChannelPartnerCodeContext = settingDao.readbyname(CTRIP_MBP_RESERVATION_ENDPOINT).getValue();
		}
		return ctripChannelPartnerCodeContext;
	}

	public List<Integer> getGuaranteedPricingPMs() {
		final List<Integer> pmIdsList = new ArrayList<>();
		final Setting guaranteedPricingPms = settingDao.readbyname(BP_HOMEAWAY_GUARANTEED_PRICING_PMS_KEY);
		if(guaranteedPricingPms != null) {
			final String guaranteedPricingPmsValue = guaranteedPricingPms.getValue();
			if(StringUtils.isNotEmpty(guaranteedPricingPmsValue)) {
				final String[] guaranteedPricingPmss = guaranteedPricingPmsValue.split(",");
				for (String pmId : guaranteedPricingPmss) {
					pmIdsList.add(Integer.valueOf(pmId));
				}
			}
		}
		return pmIdsList;
	}

	public List<Integer> getPilot42PMs() {
		final List<Integer> pmIdsList = new ArrayList<>();
		final Setting pilotPms = settingDao.readbyname(BP_HOMEAWAY_PILOT_PMS_42);
		if(pilotPms != null) {
			final String pilotPmsValue = pilotPms.getValue();
			if(StringUtils.isNotEmpty(pilotPmsValue)) {
				final String[] pilotPMs = pilotPmsValue.split(",");
				for (String pmId : pilotPMs) {
					pmIdsList.add(Integer.valueOf(pmId));
				}
			}
		}
		return pmIdsList;
	}

	public Integer getChannelApiStagingChannelId() {
		if (channelApiStagingChannelId == null) {
			Setting readbyname = settingDao.readbyname(CHANNEL_API_STAGING_CHANNEL_ID);
			channelApiStagingChannelId = readbyname != null ? readbyname.getValue() : null;
		}
		return channelApiStagingChannelId != null ? Integer.valueOf(channelApiStagingChannelId) : null;
	}

	public String getChannelApiPartyId() {
		if (channelApiPartyId == null) {
			channelApiPartyId = settingDao.readbyname(PARTY_CHANNEL_API_ID).getValue();
		}
		return channelApiPartyId;
	}
	
	public String getExpediaArchiveChannelId() {
		if (expediaArchiveChannelId == null) {
			expediaArchiveChannelId = settingDao.readbyname(EXPEDIA_ARCHIVE_CHANNEL_ID).getValue();
		}
		return expediaArchiveChannelId;
	}

	public String getRhofSplitSize(String channelId) {
		String size = null;
		Setting pmsSet = null;
		if (getExpediaChannelId().equals(channelId) || getExpediaChannelId_For_PM_MOR().equals(channelId)) {
			pmsSet = settingDao.readbyname(RazorConfig.RHOF_SPLIT_SIZE_EXPEDIA);
		} else if (getBookingChannelId() == Integer.parseInt(channelId)) {
			pmsSet = settingDao.readbyname(RazorConfig.RHOF_SPLIT_SIZE_BDC);
		}
		if (pmsSet != null) {
			size = pmsSet.getValue();
		}
		return size;
	}

	public List<String> getBookingLargePmIdsGroup1() {
		List<String> pmIdsList = new ArrayList<>();
		Setting pmsSet =  settingDao.readbyname(BOOKING_LARGE_PMIDS_GRP1);
		return splitToList(pmIdsList, pmsSet);
	}

	public List<String> getBookingLargePmIdsGroup2() {
		List<String> pmIdsList = new ArrayList<>();
		Setting pmsSet =  settingDao.readbyname(BOOKING_LARGE_PMIDS_GRP2);
		return splitToList(pmIdsList, pmsSet);
	}

	public List<String> getBookingLargePmIdsGroup3() {
		List<String> pmIdsList = new ArrayList<>();
		Setting pmsSet =  settingDao.readbyname(BOOKING_LARGE_PMIDS_GRP3);
		return splitToList(pmIdsList, pmsSet);
	}

	public List<String> getBookingTestPmIds() {
		List<String> pmIdsList = new ArrayList<>();
		Setting pmsSet =  settingDao.readbyname(BOOKING_TEST_PMIDS);
		return splitToList(pmIdsList, pmsSet);
	}

	public List<String> getBookingLargePmIdForSplitJobs() {
		List<String> pmIdsList = new ArrayList<>();
		Setting pmsSet =  settingDao.readbyname(BOOKING_LARGE_PMID_FOR_SPLITJOBS);
		return splitToList(pmIdsList, pmsSet);
	}

	public List<String> getBookingRHOFPMId() {
		List<String> pmIdsList = new ArrayList<>();
		Setting pmsSet = settingDao.readbyname(BOOKING_RHOF_SUPPLIER_SPILT);
		return splitToList(pmIdsList, pmsSet);
	}

	public  String getBookingComSplitNumber() {
		Setting pmsSet = settingDao.readbyname(BOOKING_RHOF_SPLIT_NUMBER);
		return pmsSet.getName();

	}

	private List<String> splitToList(List<String> pmIdsList, Setting pmsSet) {
		String[] pmIds = null;
		if(pmsSet != null) {
			String pmsList = pmsSet.getValue();
			if(StringUtils.isNotEmpty(pmsList)) {
				pmIds = StringUtils.split(pmsList, ",");
			}
			if (pmIds != null) {
				pmIdsList.addAll(Arrays.asList(pmIds));
			}
		}
		return pmIdsList;
	}

	public static ArrayList<String> getFailedReservationNotificationRecepients() {
		String[] recepients = razorConfig.getStringArray(FAILED_RESERVATION_NOTIFICATION_RECEPIENTS);
		if (recepients == null)
			recepients = new String[] { "marko@mybookingpal.com","danijel@mybookingpal.com","filip.pankovic@lightsoft.co.rs"};
		ArrayList<String> recepientList = new ArrayList<String>();
		Collections.addAll(recepientList, recepients);
		return recepientList;
	}
}
