<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.mybookingpal.dal.server.api.AttributeDisplayMapper">

    <resultMap id="ResultMap" type="AttributeDisplay">
        <id column="id" property="id"/>
        <result column="attributeCode" property="attributeCode"/>
        <result column="displayName" property="displayName"/>
        <result column="displayCategory" property="displayCategory"/>
        <result column="displayRootLevel" jdbcType="TINYINT" property="displayRootLevel"/>
        <result column="displayParentLevel" jdbcType="TINYINT" property="displayParentLevel"/>
        <result column="useOnly" jdbcType="TINYINT" property="useOnly"/>
    </resultMap>

    <resultMap id="ResultMapWithProduct" type="com.mybookingpal.dal.dao.entities.AttributeDisplayWithProduct" extends="ResultMap">
        <result column="productId" property="productId"/>
        <result column="deleteAutoUpdate" property="deleteAutoUpdate"/>
        <result column="distance" property="distance"/>
    </resultMap>

    <sql id="columns">
        ID, AttributeCode, DisplayName, DisplayCategory, DisplayRootLevel, DisplayParentLevel, UseOnly
    </sql>

    <select id="readAttributesForRoot" resultMap="ResultMapWithProduct">
        SELECT
                a.DisplayName,
                a.DisplayCategory,
                a.DisplayRootLevel,
                r.product_id AS productId,
                a.AttributeCode
        FROM attribute_display a INNER JOIN product_attribute r ON (a.AttributeCode = r.attribute_id)
        WHERE UseOnly = TRUE
                AND r.product_id = #{productId}
                AND a.DisplayRootLevel = 1
                AND a.DisplayParentLevel = 0
    </select>

    <select id="readDefiningAttributesNotIncludeCurrentForRoot" resultMap="ResultMapWithProduct">
        SELECT <include refid="columns"/>
        FROM attribute_display
        WHERE UseOnly = TRUE
              AND DisplayRootLevel = 1
              AND attribute_display.AttributeCode NOT IN <foreach item="item" index="index" collection="list" open="(" separator="," close=")">
                                                            #{item}
                                                        </foreach>
    </select>

    <select id="readDefiningAttributeForRoot" resultMap="ResultMapWithProduct">
        SELECT <include refid="columns"/>
        FROM attribute_display
        WHERE UseOnly = TRUE
              AND DisplayRootLevel = 1
    </select>

    <select id="readDefiningAttributesNotIncludeCurrentForRoom" resultMap="ResultMapWithProduct">
        SELECT <include refid="columns"/>
        FROM attribute_display
        WHERE UseOnly = TRUE
            AND DisplayParentLevel = 1
            AND attribute_display.AttributeCode NOT IN <foreach item="item" index="index" collection="list" open="(" separator="," close=")">
                                                                #{item}
                                                        </foreach>
    </select>

    <select id = "getAmenityByDisplayCategory" resultMap="ResultMapWithProduct">
        SELECT
        a.ID, a.AttributeCode, a.DisplayName, a.DisplayCategory,
        a.DisplayRootLevel, a.DisplayParentLevel, a.UseOnly
        FROM attribute_display a
        WHERE a.DisplayCategory = #{categoryType}
    </select>

    <select id = "readDefiningAttributesForRoom" resultMap="ResultMapWithProduct">
        SELECT <include refid="columns"/>
        FROM attribute_display
        WHERE UseOnly = TRUE
              AND DisplayParentLevel = 1
    </select>

    <select id="readAttributesForRoom" resultMap="ResultMapWithProduct">
        SELECT
        a.ID, a.AttributeCode, a.DisplayName, a.DisplayCategory,
        a.DisplayRootLevel, a.DisplayParentLevel, a.UseOnly,
        r.product_id AS productId,
        r.update_type AS updateType
        FROM attribute_display a INNER JOIN product_attribute r ON (a.AttributeCode = r.attribute_id)
        WHERE UseOnly = TRUE
        AND r.product_id IN
        <foreach item="item" index="index" collection="productIds" open="(" separator="," close=")">
            #{item}
        </foreach>
        AND a.DisplayRootLevel = 0
        AND a.DisplayParentLevel = 1
        <if test="updateType != null">
            AND r.update_type = #{updateType, jdbcType=TINYINT}
        </if>
    </select>

    <select id="readAttributesForResort" resultMap="ResultMapWithProduct">
        SELECT
        a.DisplayName,
        a.DisplayCategory,
        a.DisplayRootLevel,
        r.product_id AS productId,
        a.AttributeCode
        FROM attribute_display a INNER JOIN product_attribute r ON (a.AttributeCode = r.attribute_id)
        WHERE UseOnly = TRUE
        AND r.product_id IN
        <foreach item="item" index="index" collection="productIds" open="(" separator="," close=")">
            #{item}
        </foreach>
        AND a.DisplayRootLevel = 1
        AND a.DisplayParentLevel = 0
        <if test="updateType != null">
            AND r.update_type = #{updateType, jdbcType=TINYINT}
        </if>
    </select>

    <select id="readAttributes" resultMap="ResultMapWithProduct">
        SELECT
        r.ID, a.AttributeCode, a.DisplayName, a.DisplayCategory,
        a.DisplayRootLevel, a.DisplayParentLevel, a.UseOnly,
        r.product_id AS productId,
        r.update_type AS updateType,
        r.quantity,
        r.delete_auto_update AS deleteAutoUpdate,
        r.distance
        FROM attribute_display a INNER JOIN product_attribute r ON (a.AttributeCode = r.attribute_id)
        WHERE a.UseOnly
        AND r.product_id IN
        <foreach item="item" index="index" collection="productIds" open="(" separator="," close=")">
            #{item}
        </foreach>
        <if test="updateType != null">
            AND r.update_type = #{updateType, jdbcType=TINYINT}
        </if>
        AND r.delete_auto_update = 0
    </select>

    <select id="readProductsAttributesWithCategories" resultMap="ResultMapWithProduct">
        SELECT
        r.ID, a.AttributeCode, a.DisplayName, a.DisplayCategory,
        a.DisplayRootLevel, a.DisplayParentLevel, a.UseOnly,
        r.product_id AS productId,
        r.update_type AS updateType,
        r.quantity,
        r.distance,
        r.delete_auto_update AS deleteAutoUpdate
        FROM attribute_display a INNER JOIN product_attribute r ON (a.AttributeCode = r.attribute_id)
        WHERE r.product_id IN
        <foreach item="item" index="index" collection="productIds" open="(" separator="," close=")">
            #{item}
        </foreach>
        <if test="categoryType != null">
            AND a.DisplayCategory = #{categoryType, jdbcType=VARCHAR}
        </if>
        AND r.delete_auto_update = 0
    </select>

    <select id="readAttributeDisplaysByAttributeCodes" resultMap="ResultMap">
        SELECT
        <include refid="columns"/>
        FROM attribute_display
        WHERE AttributeCode IN
        <foreach item="item" index="index" collection="attributeCodes" open="(" separator="," close=")">
            #{item}
        </foreach>
    </select>

    <select id="readAllPossibleAttributes" resultMap="ResultMapWithProduct">
        SELECT
        a.ID, a.AttributeCode, a.DisplayName, a.DisplayCategory,
        a.DisplayRootLevel, a.DisplayParentLevel, a.UseOnly
        FROM attribute_display a
        WHERE UseOnly = TRUE
    </select>

    <select id="readAllPossibleBedTypes" resultMap="ResultMapWithProduct">
        SELECT
        a.ID, a.AttributeCode, a.DisplayName, a.DisplayCategory,
        a.DisplayRootLevel, a.DisplayParentLevel, a.UseOnly
        FROM attribute_display a
        WHERE a.DisplayCategory = #{categoryType, jdbcType=VARCHAR}
    </select>

    <select id="readAllProductAttributes" resultMap="ResultMapWithProduct">
        SELECT
        r.ID, a.AttributeCode, a.DisplayName, a.DisplayCategory,
        a.DisplayRootLevel, a.DisplayParentLevel, a.UseOnly,
        r.product_id AS productId,
        r.update_type AS updateType,
        r.quantity,
        r.distance,
        r.delete_auto_update AS deleteAutoUpdate
        FROM attribute_display a INNER JOIN product_attribute r ON (a.AttributeCode = r.attribute_id)
        WHERE r.product_id = #{productId}
        AND r.delete_auto_update = 0
    </select>

    <select id="readUniqueChildrenAttributes" resultMap="ResultMapWithProduct">
        SELECT r.ID, a.AttributeCode, a.DisplayName, a.DisplayCategory,
        a.DisplayRootLevel, a.DisplayParentLevel, a.UseOnly,
        r.product_id AS productId,
        r.update_type AS updateType,
        r.distance,
        r.quantity
        FROM attribute_display a INNER JOIN product_attribute r ON (a.AttributeCode = r.attribute_id)
        WHERE a.UseOnly AND r.product_id IN
        <foreach item="item" index="index" collection="ids" open="(" separator="," close=")">
            #{item}
        </foreach>
        GROUP BY r.attribute_id
        HAVING COUNT(r.attribute_id) = #{size}
    </select>

    <select id="readBedTypeProductAttributes" resultMap="ResultMapWithProduct">
        SELECT
        r.ID, a.AttributeCode, a.DisplayName, a.DisplayCategory,
        a.DisplayRootLevel, a.DisplayParentLevel, a.UseOnly,
        r.product_id AS productId,
        r.update_type AS updateType,
        r.distance,
        r.quantity
        FROM attribute_display a INNER JOIN product_attribute r ON (a.AttributeCode = r.attribute_id)
        WHERE r.product_id =  #{productId}
        AND a.DisplayCategory = #{categoryType, jdbcType=VARCHAR}
        AND a.DisplayParentLevel = 1
    </select>

</mapper>